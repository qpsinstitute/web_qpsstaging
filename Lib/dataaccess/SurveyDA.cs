﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using log4net.Config;
using log4net;
using System.Configuration;
using Lib.DataAccess;

namespace Lib.dataaccess
{


    class SurveyDA : DataAccessBase
    {
        #region Constants
        private const CommandType COMMAND_TYPE = CommandType.StoredProcedure;
        private const string SURVEY_TABLE_NAME = "Survey";
      
        private static readonly string logConfig = System.Configuration.ConfigurationManager.AppSettings["Log4Net"];

        #endregion

        #region SqlStatements
        private const string SQL_SURVEY_INSERT = "GetAllSurveyDetail";
        //private const string SQL_CHECK_ADMINUSER_AUTHENTICATED = "";
        //private const string SQL_GET_REGISTERED_USERS = "GetRegisteredUsers";
        //private const string SQL_GET_REGISTERED_Files = "GetRegisteredFile";
        //private const string SQL_GET_TO_EXPORT = "GetToExport";
        //private const string REGISTRATIONFile_TABLE_NAME = "registrationfile";
        private const string SQL_SURVEY_BY_WCLAUSE = "GetAllSurveyDetailById";
        #endregion

        private SqlParameter[] CreateSurveyWClauseParam(string WClause)
        {
            SqlParameter[] param = {
                                    new SqlParameter("@WClause",SqlDbType.VarChar)
                                    };
            param[0].Value = WClause;
            return param;
        }
        public DataTable GetSurveyInfoByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsRegister = new DataSet();
            try
            {
                FillDataSet(dsRegister, SURVEY_TABLE_NAME, SQL_SURVEY_BY_WCLAUSE, CreateSurveyWClauseParam(WClause));
                return dsRegister.Tables[SURVEY_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Survey information failed!!" + e.Message);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }


        public void UpdateSurvey(DataTable Surveydt)
        {
            try
            {
                DataTableUpdate(SQL_SURVEY_INSERT, Surveydt);

                if (log.IsDebugEnabled)
                    log.Debug(string.Format("Performed Table Update"));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
    }
}

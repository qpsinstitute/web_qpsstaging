using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using log4net.Config;
using log4net;
using System.Configuration;

namespace Lib.DataAccess
{
    public class CandidateDA : DataAccessBase
    {
        #region Constants
        private const CommandType COMMAND_TYPE = CommandType.StoredProcedure;
        private const string CANDIDATE_TABLE_NAME = "Candidate";
        private static readonly string logConfig = ConfigurationSettings.AppSettings["Log4Net"];        
        #endregion

        #region SqlStatements
        private const string SQL_CANDIDATE_BY_WCLAUSE = "selectCandidateByWClause";
        private const string SQL_GET_ALL_CANDIDATE = "selectAllCandidate";        
        #endregion

        #region Private Methods
        /// <summary>
        /// 1.Creats a parameter object sets value of that parameter and return it.
        /// </summary>		
        /// <param name="CandidateID">Repersents a String value containing WClause</param>
        /// <returns>array of SqlParameter</returns>    
        private SqlParameter[] CreateCandidateWClauseParam(string WClause)
        {
            SqlParameter[] param = {
                                    new SqlParameter("@WClause",SqlDbType.VarChar)
                                    };
            param[0].Value = WClause;
            return param;
        }

        private SqlParameter[] CreateCandidateIdParam(int CandidateId)
        {
            SqlParameter[] param = {
                                    new SqlParameter("@CandidateId",SqlDbType.VarChar)
                                    };
            param[0].Value = CandidateId;
            return param;
        }
        
        #endregion


        #region Public Methods
        /// <summary>
        /// Get Candidate DataTable By Passing WhereClause
        /// </summary>
        /// <param name="WClause">the value Containing valid WhereClause</param>
        /// <returns>An Instance Of User DataTable</returns>

        public DataTable GetCandidateByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsCandidate = new DataSet();
            try
            {
                FillDataSet(dsCandidate, CANDIDATE_TABLE_NAME, SQL_CANDIDATE_BY_WCLAUSE, CreateCandidateWClauseParam(WClause));
                return dsCandidate.Tables[CANDIDATE_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Candidate Failed!!" + e.Message);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void UpdateCandidate(DataTable Candidt)
        {
            try
            {
                DataTableUpdate(SQL_GET_ALL_CANDIDATE, Candidt);

                if (log.IsDebugEnabled)
                    log.Debug(string.Format("Performed Table Update"));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        public DataTable GetAllCanidate()
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsCandi = new DataSet();
            try
            {
                FillDataSet(dsCandi, CANDIDATE_TABLE_NAME, SQL_GET_ALL_CANDIDATE, null);
                return dsCandi.Tables[CANDIDATE_TABLE_NAME];
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Candidate Table Failed.....!" + ex.ToString());
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }
        #endregion
    }
}


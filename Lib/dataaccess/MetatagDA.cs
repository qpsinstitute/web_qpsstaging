using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using System.Configuration;
using System.Xml;

namespace Lib.DataAccess
{
    class MetaTagDA : DataAccessBase 
    {

        #region "Constant"
        private const string META_TABLE = "MetaTags";
        #endregion

        #region Tables
        private const string SELECT_ALL_METATAGS = "GetAllMetaTags";
        private const string SELECT_METADATA_BY_PATH = "SelectMetaDataByPath";
        #endregion

        #region Constructor
        public MetaTagDA()
		{
		}
		#endregion

        #region Public Method

        public DataTable GetAllMetaTags()
        {
            try
            {
                DataSet dsMeta = new DataSet();
                FillDataSet(dsMeta, META_TABLE, SELECT_ALL_METATAGS, null);
                return dsMeta.Tables[META_TABLE];
            }
            catch (SqlException se)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] MetaTagDA::GetAllMetaTags -  ", se);
                throw se;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void UpdateMetaTags(DataTable dtMeta)
        {
            try
            {
                DataTableUpdate(SELECT_ALL_METATAGS, dtMeta);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] MetaTagDA::UpdateMetaTags -", ex);
                }
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable SelectMetaDataByPath(string Path)
        {
            try
            {
                DataSet dsMetaDetails = new DataSet();
                FillDataSet(dsMetaDetails, META_TABLE, SELECT_METADATA_BY_PATH, CreateRequestIDParam(Path));
                return dsMetaDetails.Tables[META_TABLE];
            }

            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] MetaTagDA:SelectMetaDataByPath: - ", ex);
                throw ex;
            }
            finally
            {
                CloseConnection();
            }

        }
                
        #endregion

        #region Private Method

        private SqlParameter[] CreateRequestIDParam(string Path)
        {
            SqlParameter[] param = {
                                    new SqlParameter("@Path",SqlDbType.VarChar)
                                    };
            param[0].Value = Path;
            return param;
        }

        #endregion

    }
}

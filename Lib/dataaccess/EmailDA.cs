using System;
using System.Data;
using System.Data.SqlClient;  
using System.Collections;  
using log4net;

namespace Lib.DataAccess
{
	/// <summary>
	/// Summary description for EmailDA.
	/// </summary>
    
	public class EmailDA : DataAccessBase 
	{
		#region Constructor
		public EmailDA()
		{
			
		}
		#endregion

		#region Constants
		/// <summary>
		/// Contains a string that specifies the names of the DataTable in the DataSet object.
		/// </summary>
		
		private const CommandType COMMAND_TYPE = CommandType.StoredProcedure;
		#endregion

		#region SQL statements
		/// <summary>
		/// Contains Constants, representing the Stored Procedure Names.
		/// </summary>
		
		private const string SQL_UPDATE_CONTACT_ACTIVE = "UpdateContactActive";
		#endregion

		#region Private Methods

		private SqlParameter[] CreateUpdateContactActiveParam(int ContactID)
		{
			SqlParameter[] param= {
									  new SqlParameter("@ContactID",SqlDbType.Int),	
									 
									  
			};
			param[0].Value = ContactID;		
			return param;
		}
		#endregion 

		#region Public Methods

		public bool UpdateContactActive(int ContactID)
		{
			
			try
			{				
				ExecuteNonQuery(SQL_UPDATE_CONTACT_ACTIVE,CommandType.StoredProcedure,CreateUpdateContactActiveParam(ContactID));
				return true;
			}
			catch(System.Exception e)
			{
				throw e;
			}
			finally
			{
				CloseConnection();
			}
		}
		#endregion
	}

}

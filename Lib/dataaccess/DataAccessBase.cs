using System;
using System.IO;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Xml;
using System.Data.SqlClient;
using log4net;
using log4net.Config;

namespace Lib.DataAccess
{
	/// <summary>
	/// Base class for Data access to Sql Database using ODP.Net
	/// </summary>

	public abstract class DataAccessBase
	{
		#region Constants
		
		protected static string connectionString = ConfigurationSettings.AppSettings["SQLConnectionString"];
        protected static string connectionString_eLearn = ConfigurationSettings.AppSettings["SQLConnectionString_eLearning"];
         protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string logConfig = ConfigurationSettings.AppSettings["Log4Net"];
        
		#endregion			

		#region Attributes

		protected SqlConnection conn = null;
        protected SqlConnection conn_eLearn = null;
		protected SqlTransaction txn = null;
		protected SqlDataReader reader = null;
		
		protected DataSet ds = null;
		protected XmlDataDocument xmlDoc = null;	

		#endregion 

		#region Constructors

        protected DataAccessBase()
        {
            DOMConfigurator.ConfigureAndWatch(new FileInfo(logConfig));
        }

		~DataAccessBase()
		{
			
		}

		#endregion 

		#region properties

		public SqlConnection Connection
		{
			get
			{
				return conn;
			}
		}

		public XmlDataDocument XmlDoc
		{
			get
			{
				return xmlDoc;
			}
		}

		#endregion 

		#region Public Methods

		public void BeginTransaction()
		{
			if (null == conn)
			{
				Connect();
			}

			txn = conn.BeginTransaction();
		}

		public void CommitTransaction()
		{
			txn.Commit();
		}

		public void RollbackTransaction()
		{
			txn.Rollback();
		}

		public void CloseConnection()
		{
			conn.Close(); 
			conn = null;
		}
        public void CloseConnection_eLearn()
        {
            conn_eLearn.Close();
            conn_eLearn = null;
        }

		#endregion 

		#region Protected Methods

		protected SqlCommand GetCommand(string cmdText)
		{
			SqlCommand Cmd=new SqlCommand(cmdText);  
			return Cmd;
		}
		
		protected int ExecuteNonQuery(string cmdText, CommandType cmdType, SqlParameter[] cmdParms) 
		{
			SqlCommand cmd; 
			if (cmdType == CommandType.StoredProcedure)
				cmd=PrepareCommand(cmdText, cmdParms);
			else
				cmd=PrepareCommand(cmdType, cmdText, cmdParms);

			
			int val = cmd.ExecuteNonQuery(); 

			return val;
		}
 
		protected void ExecuteReader(string cmdText, SqlParameter[] cmdParms) 
		{
			if (reader != null)
				reader.Close();
			
			SqlCommand cmd = PrepareCommand(cmdText, cmdParms);
			reader = cmd.ExecuteReader();
		}


		protected void ExecuteReader(string cmdText, CommandType cmdType, CommandBehavior behavior, SqlParameter[] cmdParms) 
		{
			SqlCommand cmd; 
			cmd=PrepareCommand(cmdType, cmdText, cmdParms);
			SqlDataReader rdr = cmd.ExecuteReader(behavior);			
		}
		/**/		
		protected object ExecuteScalar(string cmdText, CommandType cmdType, SqlParameter[] cmdParms) 
		{
			
			SqlCommand cmd = PrepareCommand(cmdType, cmdText, cmdParms);			
			object val = cmd.ExecuteScalar();			
			return val;
		}

		protected object ExecuteScalarWithTransaction(string cmdText, CommandType cmdType, SqlParameter[] cmdParms) 
		{
			
			SqlCommand cmd = PrepareCommand(cmdType, cmdText, cmdParms);
			cmd.Transaction = txn;
			object val = cmd.ExecuteScalar();
			return val;
		}
		
		protected void FillDataSet(DataSet v_ds, string name, string cmdText, SqlParameter[] cmdParms) 
		{
			
			if (null == v_ds)
				v_ds = new DataSet();
			SqlCommand cmd = PrepareCommand(cmdText,cmdParms); 
			SqlDataAdapter adapter = PrepareAdapter(cmd);			
			cmd.ExecuteNonQuery();			
			adapter.Fill(v_ds,name);
		}

        protected void FillDataSet_eLearn(DataSet v_ds, string name, string cmdText, SqlParameter[] cmdParms)
        {

            if (null == v_ds)
                v_ds = new DataSet();
            SqlCommand cmd = PrepareCommand_eLearn(cmdText, cmdParms);
            SqlDataAdapter adapter = PrepareAdapter(cmd);
            cmd.ExecuteNonQuery();
            adapter.Fill(v_ds, name);
        }
        protected void FillDataSet_eLearnonlysp(DataSet v_ds, string cmdText, SqlParameter[] cmdParms)
        {

            if (null == v_ds)
                v_ds = new DataSet();
            SqlCommand cmd = PrepareCommand_eLearn(cmdText, cmdParms);
            SqlDataAdapter adapter = PrepareAdapter(cmd);
            //adapter.
            cmd.ExecuteNonQuery();
            //adapter.Fill(ds, name, (SqlRefCursor) cmd.Parameters["REFC"].Value);
            adapter.Fill(v_ds);

        }

		protected void DataTableUpdate(string cmdText, DataTable v_dt)
		{	
			SqlDataAdapter adapter =  PrepareBuilder(cmdText);	
			adapter.Update(v_dt);
		}

		protected int GetOutputParams(string cmdText)
		{
			return (int) GetCommand(cmdText).Parameters[0].Value;
		}

		protected SqlParameter[] CreateIdOutputParams()
		{
			SqlParameter[] parms = new SqlParameter[] 
			{
				new SqlParameter("@IDVAR", SqlDbType.Int)
			};
			parms[0].Direction =ParameterDirection.Output;
			return parms;
		}

		#endregion 

		#region Private Methods

		protected void Connect()
		{
			conn = new SqlConnection(connectionString);
			conn.Open();
		}
        public void Connect_eLearn()
        {
            conn_eLearn = new SqlConnection(connectionString_eLearn);
            conn_eLearn.Open();
        }

		private void Connect(string connString)
		{
			conn = new SqlConnection(connString);
			conn.Open();
		}

		private SqlDataAdapter PrepareBuilder(string cmdText)
		{
			if (null == conn)
				Connect();

			SqlDataAdapter adapter=PrepareAdapter(cmdText,null);
			SqlCommandBuilder builder = new SqlCommandBuilder(adapter);
			return adapter;
		}		
		
		private SqlCommand  PrepareCommand(string cmdText, SqlParameter[] cmdParms) 
		{
			if (null == conn)
				Connect();

			SqlCommand cmd;			
			cmd = conn.CreateCommand();
			cmd.CommandText = cmdText;
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Transaction = this.txn;
			if (null != cmdParms) 
			{
				foreach (SqlParameter parm in cmdParms)
					cmd.Parameters.Add(parm);
			}
			return cmd;

		}

        private SqlCommand PrepareCommand_eLearn(string cmdText, SqlParameter[] cmdParms)
        {
            if (null == conn_eLearn)
                Connect_eLearn();

            SqlCommand cmd;
            cmd = conn_eLearn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Transaction = this.txn;
            if (null != cmdParms)
            {
                foreach (SqlParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
            return cmd;

        }

		private SqlDataAdapter PrepareAdapter(SqlCommand cmd) 
		{
			SqlDataAdapter adapter;						
			adapter = new SqlDataAdapter(cmd);			

			return adapter;
			
		}

		private SqlDataAdapter PrepareAdapter(string cmdText, SqlParameter[] cmdParms) 
		{
			
			SqlCommand cmd = PrepareCommand(cmdText, cmdParms);

			SqlDataAdapter adapter;			
			adapter = new SqlDataAdapter(cmd);			

			return adapter;
			
		}

		private void PrepareAdapterUpdate(string cmdText, SqlParameter[] cmdParms) 
		{
			
			SqlCommand cmd = PrepareCommand(CommandType.StoredProcedure, cmdText, cmdParms);

			SqlDataAdapter adapter;

			adapter = new SqlDataAdapter();

			adapter.UpdateCommand = cmd;
			
		}
		
		private SqlCommand PrepareCommand(CommandType cmdType, string cmdText, SqlParameter[] cmdParms) 
		{
			if (null == conn)
				Connect();
			
			SqlCommand command = conn.CreateCommand();
			command.CommandText = cmdText;
			command.CommandType = cmdType;
			
			if (null != cmdParms) 
			{
				foreach (SqlParameter parm in cmdParms)
					command.Parameters.Add(parm);
			}

			return command; 

		}

		#endregion 
	
	}

}
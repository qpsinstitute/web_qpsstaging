﻿using System;
using System.Collections.Generic;
using System.Text;
using Lib.DataAccess;
using System.Data;
using log4net;
using System.Data.SqlClient;

namespace Lib.dataaccess
{
    public class InsdataDA : DataAccessBase
    {
        #region Constants
        private const CommandType COMMAND_TYPE = CommandType.StoredProcedure;
        private const string INS_TABLE_NAME = "Instructor";
        private const string INSAdmin_TABLE_NAME = "Instructoradmin";
        private static readonly string logConfig = System.Configuration.ConfigurationManager.AppSettings["Log4Net"];

        #endregion
        #region SqlStatements
        private const string SQL_INS_BY_WCLAUSE = "selectInsInfoByWClause";
        private const string SQL_CHECK_INSAdmin_AUTHENTICATED = "CheckINSAdminUserAuthenticated";
        private const string SQL_GET_INS_Data = "Getinsdata";
        private const string SQL_GET_REGISTERED_Files = "GetRegisteredFile";
        private const string SQL_GET_TO_EXPORT = "GetINSExport";
        private const string REGISTRATIONFile_TABLE_NAME = "registrationfile";
        private const string SQL_REGISTRATIONFile_BY_WCLAUSE = "selectRegistrationfileByWClause";
        #endregion

        public DataTable AdminAuthencation(string userName)
        {
            try
            {
                DataSet dsAdminAuthentication = new DataSet();
                FillDataSet(dsAdminAuthentication, INSAdmin_TABLE_NAME, SQL_CHECK_INSAdmin_AUTHENTICATED, CreateAdminUserAuthenticatParam(userName));
                return dsAdminAuthentication.Tables[INSAdmin_TABLE_NAME];
            }
            finally
            {
                CloseConnection();
            }
        }

        public void UpdateINSUser(DataTable Regdt)
        {
            try
            {
                DataTableUpdate("GetInsUsers", Regdt);

                if (log.IsDebugEnabled)
                    log.Debug(string.Format("Performed Table Update"));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
        public DataTable GetInSdata(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsIns = new DataSet();
            try
            {
                FillDataSet(dsIns, INS_TABLE_NAME, SQL_GET_INS_Data, null);
                return dsIns.Tables[INS_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Display Error!!" + e.Message);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        private SqlParameter[] CreateAdminUserAuthenticatParam(String userName)
        {
            SqlParameter[] param = {
									  new SqlParameter("@Email",SqlDbType.VarChar),		
								  };

            param[0].Value = userName;

            return param;
        }
        private SqlParameter[] CreateINSWClauseParam(string WClause)
        {
            SqlParameter[] param = {
                                    new SqlParameter("@WClause",SqlDbType.VarChar)
                                    };
            param[0].Value = WClause;
            return param;
        }
        public DataTable GetINSInfoByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsRegister = new DataSet();
            try
            {
                FillDataSet(dsRegister, INS_TABLE_NAME, SQL_INS_BY_WCLAUSE, CreateINSWClauseParam(WClause));
                return dsRegister.Tables[INS_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Registartion Failed!!" + e.Message);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }
        public DataTable GetToExport(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsRegister = new DataSet();
            try
            {
                FillDataSet(dsRegister, INS_TABLE_NAME, SQL_GET_TO_EXPORT, null);
                return dsRegister.Tables[INS_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Display Error!!" + e.Message);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }
    }
}

using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using log4net.Config;
using log4net;
using System.Configuration;

namespace Lib.DataAccess
{
    public class JobDA : DataAccessBase
    {
        #region Constants
        private const string JOBPOSTING_TABLE_NAME = "JobPosting";
        
        private static readonly string logConfig = ConfigurationSettings.AppSettings["Log4Net"];        
        #endregion

        #region SQLStatements
        private const string SQL_SELECT_JOBPOSTING_BY_WCLAUSE = "SelectJobPostingByWClause";
        private const string SQL_SELECT_ALL_JOBPOSTING = "SelectAllJobPosting";
        private const string SQL_GET_ALL_JOBPOSTING = "GetAllJobPosting";
        #endregion

        #region Constructor
       
        public JobDA()
        {
            //
            //TODO:
            //
        }
        #endregion

        #region Private Methods

           /// <summary>
           /// Create a Parameter Object sets value of that parameter
           /// </summary>
           /// <param name="WClause"></param>
           /// <returns>array of sqlparameter</returns>
           private SqlParameter[] CreateJobWClauseparam(string WClause)            
           {
               SqlParameter[] param = { 
                                            new SqlParameter("@WClause",SqlDbType.VarChar)
                                        };
               param[0].Value = WClause;
               return param;
           }
        private SqlParameter[] CreateJobIdparam(int JobID)
        {
            SqlParameter[] param = { 
                                        new SqlParameter("@jobID",SqlDbType.Int)
                                    };
            param[0].Value = JobID;
            return param;
        }
        #endregion

        #region public methods

        /// <summary>
        /// Get JobPosting By Passing WhereClause
        /// </summary>
        /// <param name="WClause"></param>
        /// <returns>An Instance of a JobPosting Table</returns>
        public DataTable SelectJobPostingByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsJob = new DataSet();
            try
            {
                FillDataSet(dsJob, JOBPOSTING_TABLE_NAME, SQL_SELECT_JOBPOSTING_BY_WCLAUSE, CreateJobWClauseparam(WClause));   
                return dsJob.Tables[JOBPOSTING_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug(" Job Posting Failed.!!" + e.Message);
                throw e;
            }
            finally
            {
                CloseConnection();
            }            
        }
        /// <summary>
        /// Get Job Posting Data
        /// Added By Dipti
        /// </summary>
        /// 
        public DataSet SelectAllJobPosting()
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsJob = new DataSet();
            try
            {
                FillDataSet(dsJob, JOBPOSTING_TABLE_NAME, SQL_SELECT_ALL_JOBPOSTING,null );
                return dsJob; 
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug(" Job Posting Failed.!!" + e.Message);
                throw e;
            }
            finally
            {
                CloseConnection();

            }     
        }

        //public void UpdateJobPosting(DataTable dtJob)
        //{
        //    try
        //    {
        //        // Update Job Posting
        //        DataTableUpdate(SQL_SELECT_ALL_JOBPOSTING, dtJob);
        //        if (log.IsDebugEnabled)
        //            log.Debug(string.Format("Perform JobPoting Table Update..!"));
        //    }
        //    catch (System.Exception e)
        //    {
        //        throw e;
        //    }
        //}

        public void UpdateJobPosting(DataTable dtJob)
        {
            try
            {
                // Update Job Posting
                DataTableUpdate(SQL_GET_ALL_JOBPOSTING, dtJob);

                if (log.IsDebugEnabled)
                    log.Debug(string.Format("Perform JobPoting Table Update..!"));

            }
            catch (System.Exception e)
            {
                throw e;
            }

        }       
        #endregion
    }
}

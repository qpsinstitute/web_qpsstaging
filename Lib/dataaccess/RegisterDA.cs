using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using log4net.Config;
using log4net;
using System.Configuration;

namespace Lib.DataAccess
{
  public class RegisterDA : DataAccessBase
    {
       #region Constants
            private const CommandType COMMAND_TYPE = CommandType.StoredProcedure;
            private const string REGISTRATION_TABLE_NAME = "Registration";
            private const string ADMINUSER_TABLE_NAME = "Admins";
            private static readonly string logConfig = System.Configuration.ConfigurationManager.AppSettings["Log4Net"];        

       #endregion

       #region SqlStatements
            private const string SQL_REGISTRATION_BY_WCLAUSE = "selectRegisterInfoByWClause";
            private const string SQL_CHECK_ADMINUSER_AUTHENTICATED = "CheckAdminUserAuthenticated";
            private const string SQL_GET_REGISTERED_USERS = "GetRegisteredUsers";
            private const string SQL_GET_REGISTERED_Files = "GetRegisteredFile";
            private const string SQL_GET_TO_EXPORT = "GetToExport";
            private const string REGISTRATIONFile_TABLE_NAME = "registrationfile";
            private const string SQL_REGISTRATIONFile_BY_WCLAUSE = "selectRegistrationfileByWClause";
       #endregion
       
        #region Private Method

            private SqlParameter[] CreateRegistrationWClauseParam(string WClause)
            {
                SqlParameter[] param = {
                                    new SqlParameter("@WClause",SqlDbType.VarChar)
                                    };
                param[0].Value = WClause;
                return param;
            }

      private SqlParameter[] CreateAdminUserAuthenticatParam(String userName)
      {
          SqlParameter[] param = {
									  new SqlParameter("@Email",SqlDbType.VarChar),		
								  };

          param[0].Value = userName;

          return param;
      }
        #endregion

        #region Public Method

        public DataTable GetRegisterInfoByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsRegister = new DataSet();
            try
            {
                FillDataSet(dsRegister, REGISTRATION_TABLE_NAME, SQL_REGISTRATION_BY_WCLAUSE, CreateRegistrationWClauseParam(WClause));
                return dsRegister.Tables[REGISTRATION_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Registartion Failed!!" + e.Message);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }
        public DataTable GetRegisterFileWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsRegister = new DataSet();
            try
            {
                FillDataSet(dsRegister, REGISTRATIONFile_TABLE_NAME, SQL_REGISTRATIONFile_BY_WCLAUSE, CreateRegistrationWClauseParam(WClause));
                return dsRegister.Tables[REGISTRATIONFile_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Registartion Failed!!" + e.Message);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void UpdateRegisterUser(DataTable Regdt)
        {
            try
            {
                DataTableUpdate(SQL_GET_REGISTERED_USERS, Regdt);

                if (log.IsDebugEnabled)
                    log.Debug(string.Format("Performed Table Update"));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
        public void UpdateRegisterFile(DataTable Regdt)
        {
            try
            {
                DataTableUpdate(SQL_GET_REGISTERED_Files, Regdt);

                if (log.IsDebugEnabled)
                    log.Debug(string.Format("Performed Table Update"));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
        // Foe Admin User Login Created By Dipti Swami ////

      public DataTable AdminAuthencation(string userName)
      {
          try
          {
              DataSet dsAdminAuthentication = new DataSet();
              FillDataSet(dsAdminAuthentication, ADMINUSER_TABLE_NAME, SQL_CHECK_ADMINUSER_AUTHENTICATED, CreateAdminUserAuthenticatParam(userName));
              return dsAdminAuthentication.Tables[ADMINUSER_TABLE_NAME];
          }
          finally
          {
              CloseConnection();
          }
      }
      public DataTable GetRegisterUsers(string WClause)
      {
          ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
          DataSet dsRegister = new DataSet();
          try
          {
              FillDataSet(dsRegister, REGISTRATION_TABLE_NAME, SQL_GET_REGISTERED_USERS, null);
              return dsRegister.Tables[REGISTRATION_TABLE_NAME];
          }
          catch (System.Exception e)
          {
              if (log.IsDebugEnabled)
                  log.Debug("Display Error!!" + e.Message);
              throw e;
          }
          finally
          {
              CloseConnection();
          }
      }
      public DataTable GetToExport(string WClause)
      {
          ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
          DataSet dsRegister = new DataSet();
          try
          {
              FillDataSet(dsRegister, REGISTRATION_TABLE_NAME, SQL_GET_TO_EXPORT, null);
              return dsRegister.Tables[REGISTRATION_TABLE_NAME];
          }
          catch (System.Exception e)
          {
              if (log.IsDebugEnabled)
                  log.Debug("Display Error!!" + e.Message);
              throw e;
          }
          finally
          {
              CloseConnection();
          }
      }


       #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lib.DataAccess
{
    class CourseDA:DataAccessBase
    {        
        #region Constructor
        public CourseDA() { }
        #endregion

        #region "Constant(s)"
        private const string COURSE_TABLE = "Course";
        private const string CATEGORY_TABLE = "Category";
        private const string COURSEDETAILS_TABLE = "CategoryDetails";
        #endregion

        #region SQL Statements
        private const string SQL_GET_ALL_CATEGORY = "SelectAllCategories";
        private const string SQL_GET_ALL_COURSE = "GetAllCourse";
        private const string SQL_GET_ALL_COURSE_Active = "GetAllCourse_Active";
        private const string SQL_GET_ALL_COURSE_DETAILS = "GetAllCourseDetails";
        private const string SQL_SELECT_ALL_COURSE_BY_ID = "GetAllCourseByID";
        private const string SQL_SELECT_ALL_COURSE_DETAILS_BY_ID = "GetAllCourseDetailsByID";
        private const string SQL_GET_LAST_COURSE_ID = "GetMAXCourseID";
        private const string SQL_SELECT_ALL_CATEGORY_BY_COURSEID = "SelectAllCategoryByCourseID";
        private const string SQL_SELECT_ALL_COURSEDETAILS_BY_COURSEID = "SelectCourseByID";
        private const string SQL_DELETE_COURSEDETAILS_BY_ID = "DeleteCourseDetailsByID";
        private const string SQL_DELETE_COURSE_BY_ID = "DeleteCourseByID";
        private const string SQL_SELECT_ALL_COURSE_BY_CATEGORYID = "GetAllCourseByCategoryID";
        private const string SQL_SELECT_ALL_LOCATION_BY_COURSEID = "GetAllLoactionByCourseId";
        private const string SQL_SELECT_ALL_DATES_BY_COURSEID = "GetAllDatesByCourseId";
   


        #endregion

        public class DaysData
        {
            private string _Days;
            public string Days
            {
                get { return _Days; }
                set { Days = value; }
            }

            private string _id;
            public string Id
            {
                get { return _id; }
                set { _id = value; }
            }
        }
        #region Public Method(s)

        public DataTable GetAllCategory()
        {
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet_eLearn(dsCourse, CATEGORY_TABLE, SQL_GET_ALL_CATEGORY, null);
                return dsCourse.Tables[CATEGORY_TABLE];
            }

            catch (SqlException se)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::GetAllCategory - ", se);
                throw se;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }
        public DataSet GetAllLocation()
        {
            try
            {
                DataSet dsCourseLocation = new DataSet();
                FillDataSet_eLearnonlysp(dsCourseLocation, "getLocationList", null);
                return dsCourseLocation;
            }

            catch (SqlException se)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::GetAllCategory - ", se);
                throw se;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        public DataTable GetAllCourse()
        {
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet(dsCourse, COURSE_TABLE, SQL_GET_ALL_COURSE, null);
                return dsCourse.Tables[COURSE_TABLE];
            }

            catch (SqlException se)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::GetAllCourse - ", se);
                throw se;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable GetAllCourse_elearn()
        {
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet_eLearn(dsCourse, COURSE_TABLE, SQL_GET_ALL_COURSE_Active, null);
                return dsCourse.Tables[COURSE_TABLE];
            }

            catch (SqlException se)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::GetAllCourse - ", se);
                throw se;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        public DataTable GetCourseByID(int CourseID)
        {            
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet(dsCourse, COURSE_TABLE, SQL_SELECT_ALL_COURSE_BY_ID, CreateCourseParam(CourseID));
                return dsCourse.Tables[COURSE_TABLE];

            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::GetCourseByID - ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable GetAllCourseByCategoryID(int CategoryID)
        {
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet(dsCourse, COURSE_TABLE, SQL_SELECT_ALL_COURSE_BY_CATEGORYID, CreateCategoryParam(CategoryID));
                return dsCourse.Tables[COURSE_TABLE];

            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::GetAllCourseByCategoryID - ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable GetAllLocationByCourseID(int CourseID)
        {
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet_eLearn(dsCourse, COURSE_TABLE, SQL_SELECT_ALL_LOCATION_BY_COURSEID, CreateCourseParam(CourseID));
                return dsCourse.Tables[COURSE_TABLE];

            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::GetAllLocationByCourseID - ", e);
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        public DataTable GetAllDatesByCourseID(int CourseID)
        {
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet_eLearn(dsCourse, COURSE_TABLE, SQL_SELECT_ALL_DATES_BY_COURSEID, CreateCourseParam(CourseID));
                return dsCourse.Tables[COURSE_TABLE];

            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::GetAllDatesByCourseID - ", e);
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }       



        public DataTable GetCourseDetailsByID(int CourseDetailID)
        {
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet(dsCourse, COURSEDETAILS_TABLE, SQL_SELECT_ALL_COURSE_DETAILS_BY_ID, CreateCourseDetailsParam(CourseDetailID));
                return dsCourse.Tables[COURSEDETAILS_TABLE];

            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::GetCourseDetailsByID - ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable SelectAllCategoryByCourseID(int CourseID)
        {
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet(dsCourse, CATEGORY_TABLE, SQL_SELECT_ALL_CATEGORY_BY_COURSEID, CreateCourseParam(CourseID));
                return dsCourse.Tables[CATEGORY_TABLE];

            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::SelectAllCategoryByCourseID - ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }
        public DataTable SelectAlleCourseDetailsByCourseID(int CourseID)
        {
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet_eLearn(dsCourse, COURSEDETAILS_TABLE, SQL_SELECT_ALL_COURSEDETAILS_BY_COURSEID, CreateCourseParam(CourseID));
                return dsCourse.Tables[COURSEDETAILS_TABLE];

            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::SelectAllCategoryByCourseID - ", e);
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        public DataTable SelectAllCourseDetailsByCourseID(int CourseID)
        {
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet(dsCourse, COURSEDETAILS_TABLE, SQL_SELECT_ALL_COURSEDETAILS_BY_COURSEID, CreateCourseParam(CourseID));
                return dsCourse.Tables[COURSEDETAILS_TABLE];

            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::SelectAllCategoryByCourseID - ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void DeleteCourseDetailsByID(int ID)
        {
            try
            {
                DataSet CourseDetails = new DataSet();
                FillDataSet(CourseDetails, COURSEDETAILS_TABLE, SQL_DELETE_COURSEDETAILS_BY_ID, CreateCourseDetailsParam(ID));
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] CourseDA::DeleteCourseDetailsByID -", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void DeleteCourseByID(int ID)
        {
            try
            {
                DataSet dtCourse= new DataSet();
                FillDataSet(dtCourse, COURSE_TABLE, SQL_DELETE_COURSE_BY_ID, CreateCourseParam(ID));
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] CourseDA::DeleteCourseDetailsByID -", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void UpdateCourse(DataTable dtCourse)
        {            
            try
            {
                DataTableUpdate(SQL_GET_ALL_COURSE, dtCourse);
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] CourseDA::UpdateCourse -", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void UpdateCourseDetails(DataTable dtCourseDetails)
        {
            try
            {
                DataTableUpdate(SQL_GET_ALL_COURSE_DETAILS, dtCourseDetails);
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] CourseDA::UpdateCourseDetails -", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable GetLastCourseID()
        {
            try
            {
                DataSet dsCourse = new DataSet();
                FillDataSet(dsCourse, COURSEDETAILS_TABLE, SQL_GET_LAST_COURSE_ID, null);
                return dsCourse.Tables[COURSEDETAILS_TABLE];
            }

            catch (SqlException se)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDA::GetLastCourseID - ", se);
                throw se;
            }
            finally
            {
                CloseConnection();
            }
        }

        #endregion

        #region Private Method(s)

        private SqlParameter[] CreateCourseParam(int CourseID)
        {
            SqlParameter[] param ={
                                    new SqlParameter("@CourseID", SqlDbType.Int)
            };

            param[0].Value = CourseID;

            return param;
        }

        private SqlParameter[] CreateCourseDetailsParam(int ID)
        {
            SqlParameter[] param ={
                                    new SqlParameter("@CourseDetailsID", SqlDbType.Int)
            };

            param[0].Value = ID;

            return param;
        }

        private SqlParameter[] CreateCategoryParam(int CategoryID)
        {
            SqlParameter[] param ={
                                    new SqlParameter("@CategoryID", SqlDbType.VarChar)
            };
                                           
                param[0].Value = "%," + CategoryID.ToString() + ",%";
            return param;
        }

        private SqlParameter[] CreateLocationParam(int CourseID)
        {
            SqlParameter[] param ={
                                    new SqlParameter("@CourseID", SqlDbType.VarChar)
            };

            param[0].Value = "%," + CourseID.ToString() + ",%";
            return param;
        }



        #endregion
    }
}

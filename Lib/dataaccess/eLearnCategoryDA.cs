using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using log4net.Config;
using log4net;


namespace Lib.DataAccess
{
    public class eLearnCategoryDA : DataAccessBase
    {
        #region Constants
        public const string CATEGORY_TABLE_NAME = "Category";
        public const string COURSE_TABLE_NAME = "Course";
        public const string COURSE_LOACTION_TABLE_NAME = "Course_Location";
        private const string GetLoactionsByCourseId = "GetLoactionsByCourseId";


        #endregion

        #region SQL Statements
        private const string SQL_SELECT_CATEGORY_BY_WCLAUASE = "SelectAllCategoryByStatus";
        private const string SQL_SELECT_ALL_COURSE_BY_WCLAUASE = "SelectAllCoursesByCategoryID";
        private const string SQL_SELECT_ALL_COURSE_BY_CATEGORYID_AND_Cityid = "GetCourseByCategory_location";

        private const string SQL_GET_ECOURSE_BY_CATEGORYID = "Get_eCourse_By_CategoryID";
        private const string SQL_GET_ECOURSE_BY_CATEGORYID_AND_COURSETYPEID = "Get_eCourse_By_CategoryID_And_CourseTypeId";
        private const string SQL_SELECT_COURSE_BY_COURSEID = "SelectCourseByID";


        private const string SQL_SELECT_ALL_COURSE_DETAILS_BY_WCLAUSE = "SelectAllCourseDetailsByCourseCode";


        #endregion

        #region public method

        public DataTable SelectAllCategoryByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsCategory = new DataSet();
            try
            {
                // Fill recent Category data table
                FillDataSet_eLearn(dsCategory, CATEGORY_TABLE_NAME, SQL_SELECT_CATEGORY_BY_WCLAUASE, CreateCategoryWClauseParam(WClause));
                return dsCategory.Tables[CATEGORY_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] eLearnCategoryDA::SelectAllCategoryByStatus-", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        public DataTable SelectAllCoursesByCategoryID(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsCategory = new DataSet();
            try
            {
                // Fill recent Category data table
                FillDataSet_eLearn(dsCategory, COURSE_TABLE_NAME, SQL_SELECT_ALL_COURSE_BY_WCLAUASE, CreateCategoryWClauseParam(WClause));
                return dsCategory.Tables[COURSE_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] eLearnCategoryDA::SelectAllCoursesByCategoryID-", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }
        public DataTable SelectAllCoursesByCategoryandlocationID(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsCategory = new DataSet();
            try
            {
                // Fill recent Category data table
                FillDataSet_eLearn(dsCategory, COURSE_TABLE_NAME, SQL_SELECT_ALL_COURSE_BY_CATEGORYID_AND_Cityid, CreateCategoryWClauseParam(WClause));
                return dsCategory.Tables[COURSE_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] eLearnCategoryDA::SelectAllCoursesByCategoryID-", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }
        public DataTable Get_eCourse_By_CategoryID(int CategoryID)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsCategory = new DataSet();
            try
            {
                // Fill recent Category data table
                FillDataSet_eLearn(dsCategory, COURSE_TABLE_NAME, SQL_GET_ECOURSE_BY_CATEGORYID, CreateCourseCategoryWClauseParam(CategoryID));
                return dsCategory.Tables[COURSE_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] eLearnCategoryDA::SelectAllCoursesByCategoryID-", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        public DataTable Get_eCourse_By_CategoryID_TypeID(int CategoryID, int CourseTypeID)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsCategory = new DataSet();
            try
            {
                // Fill recent Category data table
                FillDataSet_eLearn(dsCategory, COURSE_TABLE_NAME, SQL_GET_ECOURSE_BY_CATEGORYID_AND_COURSETYPEID, CreateCourseCategoryWClauseParam(CategoryID, CourseTypeID));
                return dsCategory.Tables[COURSE_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] eLearnCategoryDA::SelectAllCoursesByCategoryID-", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        public DataTable SelectAllCourseDetailsByCourseCode(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsCourse = new DataSet();
            try
            {
                // Fill recent Category data table
                FillDataSet_eLearn(dsCourse, COURSE_TABLE_NAME, SQL_SELECT_ALL_COURSE_DETAILS_BY_WCLAUSE, CreateCategoryWClauseParam(WClause));
                return dsCourse.Tables[COURSE_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] eLearnCategoryDA::SelectAllCourseDetailsByCourseCode-", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        public DataTable Select_Course_By_CourseID(int CourseID)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsCourse = new DataSet();
            try
            {
                // Fill recent Category data table
                FillDataSet_eLearn(dsCourse, COURSE_TABLE_NAME, SQL_SELECT_COURSE_BY_COURSEID, CreateCourseIDParam(CourseID));
                return dsCourse.Tables[COURSE_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] eLearnCategoryDA::Select_Course_By_CourseID-", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        public DataSet GetLocationByCourseID(int CourseID)
        {
            DataSet dsCourse = new DataSet();
            try
            {
                //Fill DataSet
                FillDataSet_eLearn(dsCourse, COURSE_LOACTION_TABLE_NAME, GetLoactionsByCourseId, CreateCourseIDParams(CourseID));
                return dsCourse;
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        #endregion

        #region private method
        private SqlParameter[] CreateCategoryWClauseParam(string WClause)
        {
            SqlParameter[] param = {
                                        new SqlParameter("@WClause",SqlDbType.VarChar)
                                    };
            param[0].Value = WClause;
            return param;
        }

        private SqlParameter[] CreateCategoryandLocationParam(int categoryid, int locationid)
        {
            SqlParameter[] param = {
                                        new SqlParameter("@CategoryID",SqlDbType.Int),
                                        new SqlParameter("@CityID",SqlDbType.Int)
                                    };
            param[0].Value = categoryid;
            param[1].Value = locationid;
            return param;
        }
        private SqlParameter[] CreateCourseCategoryWClauseParam(int CategoryID)
        {
            SqlParameter[] param = { new SqlParameter("@CategoryID", SqlDbType.Int) };
            param[0].Value = CategoryID;
            return param;
        }

        private static SqlParameter[] CreateCourseIDParams(int courseId)
        {
            SqlParameter[] param = {
									  new SqlParameter("@CourseID",SqlDbType.Int)
								  };
            param[0].Value = courseId;
            return param;
        }

        private SqlParameter[] CreateCourseCategoryWClauseParam(int CategoryID, int CourseTypeID)
        {
            SqlParameter[] param = { new SqlParameter("@CategoryID", SqlDbType.Int), new SqlParameter("@CourseTypeID", SqlDbType.Int) };
            param[0].Value = CategoryID;
            param[1].Value = CourseTypeID;
            return param;
        }

        private SqlParameter[] CreateCourseIDParam(int CourseID)
        {
            SqlParameter[] param = { new SqlParameter("@CourseID", SqlDbType.Int) };
            param[0].Value = CourseID;
            return param;
        }


        #endregion
    }
}

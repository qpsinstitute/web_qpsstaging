using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using log4net.Config;
using log4net;
using System.Configuration;
using Lib.DataAccess;

namespace Lib.dataaccess
{
    public class WebinarDA : DataAccessBase
    {
        private const string Webinar_TABLE_NAME = "Webinars";
        private const string SQL_WEBINR_BY_WCLAUSE = "Webinars_GetByWClause";

        private const string SQL_SELECT_Webinar_Webinar_BY_ID = "Webinars_GetByID";

        private Hashtable cmds = Hashtable.Synchronized(new Hashtable());    
        private Hashtable adapters = Hashtable.Synchronized(new Hashtable());

        private const string Webinaropt_TABLE_NAME = "WebinarOptions";
        private const string SQL_WEBINROPTIONS_BY_WCLAUSE = "WebinarOptions_ByWClause";

        public DataTable GetWebinarByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dswebin = new DataSet();
            try
            {
                FillDataSet(dswebin, Webinar_TABLE_NAME, SQL_WEBINR_BY_WCLAUSE, CreateUserWClauseParam(WClause));
                return dswebin.Tables[Webinar_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("User Failed!!" + e.Message);
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        public DataSet GetWebinarByID(int WebID)
        {

            DataSet dswebinr = new DataSet();
            try
            {
                FillDataSet(dswebinr, Webinar_TABLE_NAME, SQL_SELECT_Webinar_Webinar_BY_ID, CreateWebinrParam(WebID));
                return dswebinr;
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        protected void FillDataSet(DataSet v_ds, string name, string cmdText, SqlParameter[] cmdParms)
        {
            PrepareAdapter(cmdText, cmdParms);
            if (null == v_ds)
                v_ds = new DataSet();
            SqlCommand cmd = (SqlCommand)cmds[cmdText];
            SqlDataAdapter adapter = (SqlDataAdapter)adapters[cmdText];
            //adapter.
            cmd.ExecuteNonQuery();
            //adapter.Fill(ds, name, (SqlRefCursor) cmd.Parameters["REFC"].Value);
            adapter.Fill(v_ds, name);
        }

        private void PrepareAdapter(string cmdText, SqlParameter[] cmdParms)
        {
            PrepareCommand(cmdText, cmdParms);
            SqlCommand cmd = (SqlCommand)cmds[cmdText];

            SqlDataAdapter adapter = (SqlDataAdapter)adapters[cmdText];
            if (null == adapter)
            {
                adapter = new SqlDataAdapter(cmd);
                adapters.Add(cmdText, adapter);
            }
            else
            {
            }
        }

        private SqlParameter[] CreateWebinrParam(int WebinID)
        {
            SqlParameter[] pa = {
								   new SqlParameter("@WebinarID",SqlDbType.Int)								  
							   };

            pa[0].Value = WebinID;

            return pa;
        }

        private void PrepareCommand(string cmdText, SqlParameter[] cmdParms)
        {
            if (null == conn)
                Connect_eLearn();

            SqlCommand cmd = (SqlCommand)cmds[cmdText];
            if (null == cmd)
            {
                cmd = conn_eLearn.CreateCommand();
                cmd.CommandText = cmdText;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = this.txn;

                if (null != cmdParms)
                {
                    foreach (SqlParameter parm in cmdParms)
                        cmd.Parameters.Add(parm);
                }

                cmds.Add(cmdText, cmd);
            }
            else
            {
                cmd.Parameters.Clear();

                // Bind the parameters passed in
                if (null != cmdParms)
                {
                    foreach (SqlParameter parm in cmdParms)
                        cmd.Parameters.Add(parm);
                }
            }
        }

        private SqlParameter[] CreateUserWClauseParam(string WClause)
        {
            SqlParameter[] param = {
									  new SqlParameter("@WClause",SqlDbType.VarChar)									  
			                        };
            param[0].Value = WClause;
            return param;
        }


        public DataTable GetWebinarOptionByWClause(string WClause)
        {

            DataSet dsWebinaropt = new DataSet();
            try
            {
                FillDataSet(dsWebinaropt, Webinaropt_TABLE_NAME, SQL_WEBINROPTIONS_BY_WCLAUSE, CreateWebinrWClauseParam(WClause));
                return dsWebinaropt.Tables[Webinaropt_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                CloseConnection_eLearn();
            }
        }

        private SqlParameter[] CreateWebinrWClauseParam(string WClause)
        {
            SqlParameter[] param = {
									  new SqlParameter("@WClause",SqlDbType.VarChar)									  
			                        };
            param[0].Value = WClause;
            return param;
        }
    }
}

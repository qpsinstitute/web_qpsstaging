using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using log4net.Config;
using log4net;


namespace Lib.DataAccess
{
    public class CategoryDA : DataAccessBase
    {
        #region Constants
        public const string JOBCATEGORY_TABLE_NAME = "Job_Category"; 
        #endregion

        #region SQL Statements
        private const string SQL_SELECT_JOB_CATEGORY_BY_WCLAUASE = "SelectJobCategoryByWClause";
        private const string SQL_SELECT_ALL_CATEGORY = "SelectAllJobCategory";        
        #endregion

                 
        #region public method

        public DataTable SelectJobCategoryByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsCategory = new DataSet();
            try
            {
                // Fill recent Job_Category data table
                FillDataSet(dsCategory, JOBCATEGORY_TABLE_NAME, SQL_SELECT_JOB_CATEGORY_BY_WCLAUASE, CreateCategoryWClauseParam(WClause));
                return dsCategory.Tables[JOBCATEGORY_TABLE_NAME];
            }
            catch (System.Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] CategoryDA::SelectCategoryByWClause-", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection();                
            }
        }

        public DataTable GetAllCategory()
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            DataSet dsCat = new DataSet();
            try
            {
                FillDataSet(dsCat, JOBCATEGORY_TABLE_NAME, SQL_SELECT_ALL_CATEGORY, null);
                return dsCat.Tables[JOBCATEGORY_TABLE_NAME];
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Category Table can not retrieved ...!" + ex.ToString());
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void UpdateCategory(DataTable dtCate)
        {
            try
            {
                // Update Job Category Table 
                DataTableUpdate(SQL_SELECT_ALL_CATEGORY, dtCate);

                if (log.IsDebugEnabled)
                    log.Debug(string.Format("Perform Jobcategory Table Update..!"));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region private method
        private SqlParameter[] CreateCategoryWClauseParam(string WClause)
        {
            SqlParameter[] param = {
                                        new SqlParameter("@WClause",SqlDbType.VarChar)
                                    };
            param[0].Value = WClause;
            return param;
        }

        private SqlParameter[] CreateJobCategoryparam(int CatID)
        {
            SqlParameter[] param = { 
                                        new SqlParameter("@CatID",SqlDbType.Int)
                                    };
            param[0].Value = CatID;
            return param;
        }

        #endregion
    }
}

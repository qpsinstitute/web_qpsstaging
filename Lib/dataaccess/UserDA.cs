using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using log4net;
using System.IO;
using log4net.Config;
namespace Lib.DataAccess
{
    /// <summary>
    /// DataAccess Class For Contact.
    /// </summary>
    public class UserDA : DataAccessBase
    {
        #region Constants

        private const string USER_TABLE_NAME = "Users";
        private const string ROLE_TABLE_NAME = "Role";
        private const string CONTENTS_TABLE_NAME = "Contents";
        private const string CANDIDATE_TABLE_NAME = "Candidate";

        #endregion

        #region SQL statements
        private const string SQL_GET_ALL_ROLE = "GetAllRole";
        private const string SQL_GET_ALL_USERS = "GetAllUsers";
        private const string SQL_GET_ALL_USERS_BYWHERECLAUSE = "GetAllUsersByWhereClause";
        private const string SQL_SELECT_USER_BY_WHERECLAUSE = "SelectUsersByWhereClause";
        private const string SQL_GET_ALL_CONTACTUS = "GetContactUs";
        private const string SQL_SELECT_EMPLOYER_BY_WHERECLAUSE = "selectEmployerByWClause";
        private const string SQL_GET_ADMIN_ROLE = "GetAdminRoleByUser";
        /// <summary>
        /// Added By Dinkar Vaja
        /// DC : 5-5-2010
        /// </summary>
        private const string SQL_CHECK_USER_AUTHENTICATED = "CheckUserAuthenticated";
        private const string SQL_CHECK_CANDIDATE_USER_AUTHENTICATED = "CheckCandidateUserAuthenticated";

        #endregion

        #region Constructor
        /// <summary>
        /// Zero Argument Constructor
        /// </summary>
        public UserDA()
        {

        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Bhavin Parmar
        /// DC: 06-3-08
        /// </summary>
        private SqlParameter[] CreateAllUsersByWcParam(string WClause)
        {
            SqlParameter[] param ={
                                    new SqlParameter("@WClause", SqlDbType.VarChar)
            };

            param[0].Value = WClause;

            return param;
        }
        /// <summary>
        /// Hetal Savaliya
        /// DC: 11-3-08
        /// </summary>
        private SqlParameter[] CreateWhereClauseParam(string FieldName,string whereclause, string OrderBy)
        {
            SqlParameter[] param ={ 
                                    new SqlParameter("@OrderBy", SqlDbType.VarChar),                                   
                                    new SqlParameter("@WhereClause", SqlDbType.VarChar),
                                    new SqlParameter("@FieldName", SqlDbType.VarChar)
            };
            param[0].Value = OrderBy;
            param[1].Value = whereclause;
            param[2].Value = FieldName;
            return param;
        }
        /// <summary>
        /// Dinkar Vaja
        /// DC: 5-5-2010
        /// </summary>        
        private SqlParameter[] CreateUserAuthenticatParam(String userName)
        {
            SqlParameter[] param = {
									  new SqlParameter("@Email",SqlDbType.VarChar),		
									  
								  };

            param[0].Value = userName;

            return param;
        }

        private SqlParameter[] CreateCandidateUserAuthenticatParam(String Email)
        {
            SqlParameter[] param = {
									  new SqlParameter("@Email",SqlDbType.VarChar),		
									  
								  };

            param[0].Value = Email;

            return param;
        }

        private SqlParameter[] CreateAdminByRoleParam(string WClause)
        {
            SqlParameter[] param ={
                                    new SqlParameter("@WClause", SqlDbType.VarChar)
            };

            param[0].Value = WClause;

            return param;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sneha Thakker
        /// DC: 06-3-08
        /// </summary>
        public DataTable GetAllUsers()
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            try
            {
                DataSet dsRequest = new DataSet();
                FillDataSet(dsRequest, USER_TABLE_NAME, SQL_GET_ALL_USERS, null);
                return dsRequest.Tables[USER_TABLE_NAME];
            }
            catch (SqlException e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserDA::GetAllUsers -  ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }
      
        /// <summary>
        /// Bhavin Parmar
        /// DC: 10-3-08
        /// </summary>
        public DataTable GetAllUsersByWhereClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            try
            {
                DataSet dsRequest = new DataSet();
                FillDataSet(dsRequest, USER_TABLE_NAME, SQL_GET_ALL_USERS_BYWHERECLAUSE, CreateAllUsersByWcParam(WClause));
                return dsRequest.Tables[USER_TABLE_NAME];
            }
            catch (SqlException e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserDA::GetAllUsersByWhereClause -  ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }       
        public void UpdateUsers(DataTable dtUsers)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            try
            {
                DataTableUpdate(SQL_GET_ALL_USERS, dtUsers);
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] UserDA::UpdateUsers -", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }
        /// <summary>
        /// Hetal savaliya
        /// DC:11-3-08
        /// </summary>
        /// <param name="WhereClause"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public DataTable GetUsersByWhereClause(string FieldName, string WhereClause, string OrderBy)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            try
            {
                DataSet dsRequest = new DataSet();
                FillDataSet(dsRequest, USER_TABLE_NAME, SQL_SELECT_USER_BY_WHERECLAUSE, CreateWhereClauseParam(FieldName,WhereClause, OrderBy));
                return dsRequest.Tables[USER_TABLE_NAME];
            }
            catch (SqlException e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserDA::GetUsersByWhereClause -  ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }
        public DataTable GetContactUs()
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            try
            {
                DataSet dsRequest = new DataSet();
                FillDataSet(dsRequest, CONTENTS_TABLE_NAME, SQL_GET_ALL_CONTACTUS, null);
                return dsRequest.Tables[CONTENTS_TABLE_NAME];
            }
            catch (SqlException e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserDA::GetContactUs -  ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }
        public void UpdateContactUs(DataTable dtContactUs)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            try
            {
                DataTableUpdate(SQL_GET_ALL_CONTACTUS, dtContactUs);
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] UserDA::UpdateContactUs -", e);
                }
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }
        /// <summary>
        /// Dinkar Vaja
        /// DC: 5-5-2010
        /// </summary>        
        public DataRow UserAuthencation(String userName)
        {
            
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            DataSet dsUser = new DataSet();
            try
            {
                FillDataSet(dsUser, USER_TABLE_NAME, SQL_CHECK_USER_AUTHENTICATED, CreateUserAuthenticatParam(userName));

                if (dsUser == null)
                    return null;

                if (dsUser.Tables[USER_TABLE_NAME].Rows.Count <= 0)
                    return null;

                return dsUser.Tables[USER_TABLE_NAME].Rows[0];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("User Failed!!");
                throw e;

            }
            finally
            {
                CloseConnection();
            }
        }


        /// <summary>
        /// Dipti Swami
        /// DC: 5-7-2010
        /// </summary>  
        /// 

        public DataRow CandidateAuthencation(String Email)
        {

            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            DataSet dsUser = new DataSet();
            try
            {
                FillDataSet(dsUser, CANDIDATE_TABLE_NAME, SQL_CHECK_CANDIDATE_USER_AUTHENTICATED, CreateCandidateUserAuthenticatParam(Email));

                if (dsUser == null)
                    return null;

                if (dsUser.Tables[CANDIDATE_TABLE_NAME].Rows.Count <= 0)
                    return null;

                return dsUser.Tables[CANDIDATE_TABLE_NAME].Rows[0];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("User Failed!!");
                throw e;

            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Dipti Swami
        /// DC: 6-8-2010
        /// </summary>  
        /// 
        
        public DataTable GetEmployerByWhereClause(string WhereClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            try
            {
                DataSet dsRequest = new DataSet();
                FillDataSet(dsRequest, USER_TABLE_NAME, SQL_SELECT_EMPLOYER_BY_WHERECLAUSE, CreateAllUsersByWcParam(WhereClause));
                return dsRequest.Tables[USER_TABLE_NAME];
            }
            catch (SqlException e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserDA::GetEmployerByWhereClause -  ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable GetAllRole()
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            try
            {
                DataSet dsRequest = new DataSet();
                FillDataSet(dsRequest, ROLE_TABLE_NAME, SQL_GET_ALL_ROLE, null);
                return dsRequest.Tables[ROLE_TABLE_NAME];
            }
            catch (SqlException e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserDA::GetAllRole -  ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataTable GetAdminRoleByUser(string WhereClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            try
            {
                DataSet dsRequest = new DataSet();
                FillDataSet(dsRequest, USER_TABLE_NAME, SQL_GET_ADMIN_ROLE, CreateAdminByRoleParam(WhereClause));
                return dsRequest.Tables[USER_TABLE_NAME];
            }
            catch (SqlException e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserDA::GetAdminRoleByUser -  ", e);
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using System.Configuration;
using System.Xml;

namespace Lib.DataAccess
{
    public class ContentDA : DataAccessBase 
    {
        

        #region Constructor
        public ContentDA() { }
        #endregion

        #region Constants        
        private static string XMLSettingsPath = ConfigurationManager.AppSettings["RssXMLPath"].ToString();
        #endregion

        #region Public Method(s)

        public DataSet SelectRssContetnFromXMLFile(string FileName)
        {
            try
            {
                DataSet dsXMLText = new DataSet();
                dsXMLText.ReadXml(XMLSettingsPath + FileName);
                return dsXMLText;
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString() + "] ContentDA::SelectContetnFromXMLFile -", ex);               
                throw ex;
            }
        }

        public bool UpdateRSSContetnInXMLFile(string FileName, DataView dtrssContent)
        {
            try
            {
                XmlDocument SiteDocument = new XmlDocument();
                SiteDocument.Load(XMLSettingsPath + FileName);
                XmlNode SiteNode = SiteDocument.DocumentElement;
                XmlNodeList nodeList = SiteNode.SelectNodes("/rss/channel/item");
                //int Numb = SiteList.Count;
                if (dtrssContent.Count > 0)
                {
                    int i = Convert.ToInt32(dtrssContent[0]["item_id"]);
                    nodeList.Item(i).SelectSingleNode("./title").InnerText = dtrssContent[0]["Title"].ToString();
                    nodeList.Item(i).SelectSingleNode("./link").InnerText = dtrssContent[0]["Link"].ToString();
                    nodeList.Item(i).SelectSingleNode("./description").InnerText = dtrssContent[0]["Description"].ToString();
                    nodeList.Item(i).SelectSingleNode("./pubDate").InnerText = DateTime.Now.ToString(); ;
                }
                SiteDocument.Save(XMLSettingsPath + FileName);
                return true;
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString() + "] ContentDA::UpdateContetnInXMLFile -", ex);

                return false;
            }
        }

        public bool AddNewRSSContetnInXMLFile(string FileName, DataView dtrssContent)
        {
            try
            {
                if (dtrssContent.Count > 0)
                {
                    XmlDocument SiteDocument = new XmlDocument();
                    SiteDocument.Load(XMLSettingsPath + FileName);
                    XmlNode SiteNode = SiteDocument.DocumentElement;
                    XmlNode nodeList = SiteNode.SelectSingleNode("/rss/channel");

                    XmlElement itemTag = SiteDocument.CreateElement("item");
                    XmlElement titleTag = SiteDocument.CreateElement("title");
                    XmlElement linkTag = SiteDocument.CreateElement("link");
                    XmlElement descTag = SiteDocument.CreateElement("description");
                    XmlElement dateTag = SiteDocument.CreateElement("pubDate");

                    XmlText titleText = SiteDocument.CreateTextNode(dtrssContent[0]["Title"].ToString());
                    XmlText linkText = SiteDocument.CreateTextNode(dtrssContent[0]["Link"].ToString());
                    XmlText descText = SiteDocument.CreateTextNode(dtrssContent[0]["Description"].ToString());
                    XmlText dateText = SiteDocument.CreateTextNode(Convert.ToString(DateTime.Now));

                    titleTag.AppendChild(titleText);
                    linkTag.AppendChild(linkText);
                    descTag.AppendChild(descText);
                    dateTag.AppendChild(dateText);

                    itemTag.AppendChild(titleTag);
                    itemTag.AppendChild(linkTag);
                    itemTag.AppendChild(descTag);
                    itemTag.AppendChild(dateTag);


                    nodeList.PrependChild(itemTag);
                    SiteDocument.Save(XMLSettingsPath + FileName);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString() + "] ContentDA::UpdateContetnInXMLFile -", ex);
                return false;
            }
        }

        #endregion
    }
}

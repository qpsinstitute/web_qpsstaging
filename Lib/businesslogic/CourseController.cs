using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Lib.DataAccess;
using Lib.Classes;


namespace Lib.BusinessLogic
{
    public class CourseController : BusinessLogicBase
    {
        #region Constructor
        public CourseController()
        {

        }
        #endregion

        #region Public Methods

        public static DataTable GetAllCategory()
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.GetAllCategory();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::GetAllCategory -  ", ex);
                throw ex;
            }
        }
        public static DataSet GetAllLocation()
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.GetAllLocation();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::GetAllCategory -  ", ex);
                throw ex;
            }
        }





        public static DataTable GetAllCourse()
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.GetAllCourse();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::GetAllCourse -  ", ex);
                throw ex;
            }
        }
        public static DataTable GetAllCourse_elearn()
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.GetAllCourse_elearn();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::GetAllCourse -  ", ex);
                throw ex;
            }
        }
        public static DataTable GetAllCourseByID(int CourseID)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.GetCourseByID(CourseID);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::GetAllCourseByID -  ", ex);
                throw ex;
            }
        }

        public static DataTable GetAllLocationByCourseID(int CourseID)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.GetAllLocationByCourseID(CourseID);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::GetAllCourseByCategoryID -  ", ex);
                throw ex;
            }
        }

        public static DataTable GetAllDatesByCourseID(int CourseID)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.GetAllDatesByCourseID(CourseID);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::GetAllCourseByCategoryID -  ", ex);
                throw ex;
            }
        }




        public static DataTable GetAllCourseByCategoryID(int CategoryID)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.GetAllCourseByCategoryID(CategoryID);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::GetAllCourseByCategoryID -  ", ex);
                throw ex;
            }
        }

        static public DataTable SelectAllCategoryByCourseID(int CourseID)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.SelectAllCategoryByCourseID(CourseID);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::SelectAllCategoryByCourseID -  ", ex);
                throw ex;
            }
        }

        public static DataTable GetAllCourseDetailsByID(int CourseID)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.GetCourseDetailsByID(CourseID);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::GetAllCourseDetailsByID -  ", ex);
                throw ex;
            }
        }

        public static DataTable SelectAlleCourseDetailsByCourseID(int CourseID)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.SelectAlleCourseDetailsByCourseID(CourseID);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::SelectAllCourseDetailsByCourseID -  ", ex);
                throw ex;
            }
        }
        public static DataTable SelectAllCourseDetailsByCourseID(int CourseID)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.SelectAllCourseDetailsByCourseID(CourseID);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::SelectAllCourseDetailsByCourseID -  ", ex);
                throw ex;
            }
        }

        public static void DeleteCourseDetailsByID(int ID)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                cDA.DeleteCourseDetailsByID(ID);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::DeleteCourseDetailsByID -  ", ex);
                throw ex;
            }
        }

        public static void DeleteCourseByID(int ID)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                cDA.DeleteCourseByID(ID);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::DeleteCourseByID -  ", ex);
                throw ex;
            }
        }

        public static void UpdateCourse(DataTable dtCourse)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                cDA.UpdateCourse(dtCourse);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::UpdateCourse -  ", ex);
                throw ex;
            }
        }

        public static void UpdateCourseDetails(DataTable dtCourseDetails)
        {
            try
            {
                CourseDA cDA = new CourseDA();
                cDA.UpdateCourseDetails(dtCourseDetails);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::UpdateCourseDetails -  ", ex);
                throw ex;
            }
        }


        public static DataTable GetLastCourseID()
        {
            try
            {
                CourseDA cDA = new CourseDA();
                return cDA.GetLastCourseID();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseController::UpdateCourse -  ", ex);
                throw ex;
            }
        }

        #endregion
        public class DaysData
        {
            private string _Days;
            public string Days
            {
                get { return _Days; }
                set { Days = value; }
            }

            private string _id;
            public string Id
            {
                get { return _id; }
                set { _id = value; }
            }
        }
    }
}

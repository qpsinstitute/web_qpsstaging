using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using Lib.DataAccess;
using Lib.Classes;

namespace Lib.BusinessLogic
{
    public class ContentController : BusinessLogicBase
    {

        #region Constants
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Constructor
        public ContentController() { }
        #endregion

        #region Public Methods

        public static DataTable SelectRssContetnFromXMLFile(string FileName)
        {
            try
            {
                ContentDA objContent = new ContentDA();
                DataSet dsContact = objContent.SelectRssContetnFromXMLFile(FileName);
                DataTable dtItem = dsContact.Tables["item"];
                dtItem.Columns.Add("item_id", System.Type.GetType("System.Int32"));
                int cnt = 0;
                while (cnt != dtItem.Rows.Count)
                {
                    dtItem.Rows[cnt]["item_id"] = cnt;
                    cnt++;
                }
                return dtItem;
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString() + "]  ContentController::SelectRssContetnFromXMLFile -", ex);

                throw ex;
            }
        }

        public static bool UpdateRSSContetnInXMLFile(string FileName, DataView dtContent)
        {
            try
            {
                ContentDA objContent = new ContentDA();
                return objContent.UpdateRSSContetnInXMLFile(FileName, dtContent);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString() + "]  ContentController::UpdateRSSContetnInXMLFile -", ex);

                return false;
            }
        }

        public static bool AddNewRSSContetnInXMLFile(string FileName, DataView dtContent)
        {
            try
            {
                ContentDA objContent = new ContentDA();
                return objContent.AddNewRSSContetnInXMLFile(FileName, dtContent);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString() + "]  ContentController::AddNewRSSContetnInXMLFile -", ex);

                return false;
            }
        }

        public static string GetFilteredXMLData(string XMLText)
        {
            string htmlData = XMLText.Replace("&", "^");
            return htmlData.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&nbsp;", "^nbsp;");
        }

        public static string GetFilteredData(string StringValue)
        {
            return StringValue.Replace("^", "&");
        }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Net;
using System.Net.Mail;
using log4net;
using log4net.Config;
using System.Web;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using Lib.DataAccess;
using Lib.Classes;
using System.Diagnostics;
using System.Configuration;


namespace Lib.BusinessLogic
{
	/// <summary>
	/// Summary description for EmailController.
	/// </summary>
    
	public class EmailController : BusinessLogicBase
	{
		#region Page Level Variable

        private static string SMTP_SERVER = ConfigurationSettings.AppSettings["strSMTPServer"];
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		#endregion

		#region Constructor
		public EmailController()
		{
			
		}
		#endregion


		#region Public Methods

        public static bool SendMail(string from, string to,string to2, string subject, string body, bool isFormatHtml)
        {
            try
            {
                SmtpClient objSMTPClient = new SmtpClient();
                //objSMTPClient.Host = "mail.promactinfo.com";
                objSMTPClient.Host = SMTP_SERVER;
                //objSMTPClient.Credentials = new NetworkCredential("shabina@promactinfo.co.in", "promact");
                string BODY_FORMAT = ConfigurationSettings.AppSettings["EmailBodyContentFormat"];

                MailMessage objMailMessage = new MailMessage(from.Trim(), to.Trim(), subject.Trim(), body.Trim());
                //objMailMessage.CC.Add(ConfigurationSettings.AppSettings["InformationalToMail"]);
                objMailMessage.CC.Add(ConfigurationSettings.AppSettings["InformationalToMail2"]);
                objMailMessage.CC.Add(ConfigurationSettings.AppSettings["InformationalFromMail"]);
                objMailMessage.To.Add(to2);

                if (BODY_FORMAT.ToUpper() == "HTML")
                    objMailMessage.IsBodyHtml = true;
                else if (BODY_FORMAT.ToUpper() == "TEXT")
                {
                    body = StripTags(body);
                    objMailMessage.IsBodyHtml = false;
                    objMailMessage.Body = body.ToString().Trim();
                }
                else
                    return false;


                objSMTPClient.Send(objMailMessage);
                return true;
            }
            catch (Exception eX)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] EmailController::SendMail - ", eX);
                throw eX;
            }
        }

        public static bool SendMail(string from, string to, string subject, string body, bool isFormatHtml)
        {
            try
            {
                SmtpClient objSMTPClient = new SmtpClient();
                //objSMTPClient.Host = "mail.promactinfo.com";
                objSMTPClient.Host = SMTP_SERVER;
                //objSMTPClient.Credentials = new NetworkCredential("shabina@promactinfo.co.in", "promact");
                string BODY_FORMAT = ConfigurationSettings.AppSettings["EmailBodyContentFormat"];

                MailMessage objMailMessage = new MailMessage(from.Trim(), to.Trim(), subject.Trim(), body.Trim());
                objMailMessage.CC.Add(ConfigurationSettings.AppSettings["InformationalToMail"]);
                //objMailMessage.CC.Add(ConfigurationSettings.AppSettings["InformationalToMail2"]);
                //objMailMessage.CC.Add(ConfigurationSettings.AppSettings["InformationalFromMail"]);
                //objMailMessage.To.Add(to);

                if (BODY_FORMAT.ToUpper() == "HTML")
                    objMailMessage.IsBodyHtml = true;
                else if (BODY_FORMAT.ToUpper() == "TEXT")
                {
                    body = StripTags(body);
                    objMailMessage.IsBodyHtml = false;
                    objMailMessage.Body = body.ToString().Trim();
                }
                else
                    return false;


                objSMTPClient.Send(objMailMessage);
                return true;
            }
            catch (Exception eX)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] EmailController::SendMail - ", eX);
                throw eX;
            }
        }
        private static string StripTags(string strHTML)
        {
            if (strHTML.Trim() != "")
            {

                string strText = System.Text.RegularExpressions.Regex.Replace(strHTML, "<[^>]*>", "");
                strText = System.Text.RegularExpressions.Regex.Replace(strText, "\t", "");
                strText = System.Text.RegularExpressions.Regex.Replace(strText, "\r\n:\r\n", ":");
                strText = System.Text.RegularExpressions.Regex.Replace(strText, "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n", "\r\n");
                strText = System.Text.RegularExpressions.Regex.Replace(strText, "\r\n\r\n\r\n", "\t");
                return System.Text.RegularExpressions.Regex.Replace(strText, "&nbsp;", " ");

            }
            else
                return "";
        }
		public static bool UpdateContactActive(int ContactID)
		{
			
			EmailDA caDA = new EmailDA();
			
			try
			{
				caDA.UpdateContactActive(ContactID);
			}
			catch(Exception ce)
			{
				throw ce;
			}
			
			return true;
		}

        public static string GetBodyFromTemplate(string templateFilePath, Hashtable paramValueTable)
        {
            try
            {
                StreamReader srTemplate = null;
                if (System.Web.HttpContext.Current != null)
                    srTemplate = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(templateFilePath));
                else
                    srTemplate = new StreamReader(templateFilePath);
                string sBody = srTemplate.ReadToEnd();

                if (paramValueTable == null)
                    return sBody;

                foreach (string key in paramValueTable.Keys)
                    sBody = sBody.Replace(key, paramValueTable[key].ToString());

                return sBody;
            }
            catch (Exception eX)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] EmailController::GetBodyFromEmailTemplate", eX);
                throw eX;
            }
            finally
            {
                templateFilePath = null;
                paramValueTable = null;
            }
        }
		#endregion
     
    }

}

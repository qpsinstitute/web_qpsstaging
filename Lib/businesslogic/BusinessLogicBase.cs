using System;
using System.Configuration;
using System.IO;
using log4net;
using log4net.Config;

namespace Lib.BusinessLogic
{
	/// <summary>
	/// Base class for all BusinessLogic.
	/// </summary>
    
	public class BusinessLogicBase
	{
		#region Constants

		private static readonly string logConfig = ConfigurationSettings.AppSettings["Log4Net"];
		protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		#endregion

		#region Constructors

		public BusinessLogicBase()
		{
			
		}
		
		#endregion
	}

}

using System;
using System.IO;
using System.Web;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using Lib.DataAccess;
using Lib.Classes;

namespace Lib.BusinessLogic
{
    public class CandidateCotroller : BusinessLogicBase
    {
        #region Constructor
        /// <summary>
        /// Zero argument construtor
        /// </summary>
        public CandidateCotroller()
        {

        }
        #endregion

        #region Constants
        private const string CANDIDATE = "Candidate";        
        #endregion

        #region Public Methods
        ///<summery>
        /// Dinkar Vaja
        /// DC: 27-4-10
        ///</summery>
       
        public static DataTable GetCandidateByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            CandidateDA objCandidateDA = new CandidateDA();
            try
            {
                DataTable dtCandidate = objCandidateDA.GetCandidateByWClause(WClause);
                return dtCandidate;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CandidateController::GetCandidateByWClause -  ", e);
                throw e;
            }
        }

        public static void UpdateCandidate(DataTable Candidt)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            CandidateDA objCandiDA = new CandidateDA();

            // Begin New Transaction 
            objCandiDA.BeginTransaction();

            try
            {
                objCandiDA.UpdateCandidate(Candidt);
                objCandiDA.CommitTransaction();
                objCandiDA.CloseConnection();

                if (log.IsInfoEnabled)
                    log.Info("Candidate Updated Successfully..");
            }
            catch (System.Exception ex)
            {
               // changing by dipti swami
                //RollBack All The Changes on Failure
                objCandiDA.RollbackTransaction();
                objCandiDA.CloseConnection();
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CandidateController::UpdateCandidate -  ", ex);
                throw ex;
            }
        }

        public static DataTable GetAllCanidate()
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            CandidateDA objCandi = new CandidateDA();
            try
            {
                DataTable dtCandi = objCandi.GetAllCanidate();
                return dtCandi;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CandidateController::GetAllCandidate -  ", ex);
                throw ex;
            }
        }
   
        #endregion
    }
}

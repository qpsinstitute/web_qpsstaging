using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using Lib.DataAccess;
using Lib.Classes;

namespace Lib.BusinessLogic
{
    public class CategoryController : BusinessLogicBase
    {

        #region Constructor
        public CategoryController()
        {
           //
           // TODO: Add Contructor Logic here
           //
        }
        #endregion

        #region Public Methods

        public static DataTable SelectJobCategoryByWClause(string WClause)
        {            
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            CategoryDA cDA = new CategoryDA();
            try
            {
                DataTable dtCat = cDA.SelectJobCategoryByWClause(WClause); 
                return dtCat;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Job Category table is not return successfully!!" + ex.ToString());
                throw ex;
            }
        }

        public static DataTable GetAllCategory()
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            CategoryDA objDA = new CategoryDA();
            try
            {
                DataTable dtCat = objDA.GetAllCategory();
                return dtCat;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Category Table Can not return Successfully..!" + ex.ToString());
                throw ex;
            }
        }


        public static void UpdateCategory(DataTable dtCate)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            CategoryDA objCatDA = new CategoryDA();
            // Begin new Transaction 
            objCatDA.BeginTransaction();
            try
            {
                objCatDA.UpdateCategory(dtCate);

                objCatDA.CommitTransaction();

                objCatDA.CloseConnection();

                if (log.IsInfoEnabled)
                    log.Info("Job Category Table Update Successfully!!");
            }
            catch (System.Exception se)
            {
                // Rollback all the changes on failure
                objCatDA.RollbackTransaction();
                objCatDA.CloseConnection();
                if (log.IsDebugEnabled)
                    log.Debug("Update Job Category Failed...");
                throw se;
            }
        }

        #endregion
    }
}

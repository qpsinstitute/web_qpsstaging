using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using log4net;
using Lib.DataAccess;
using Lib.Classes;

namespace Lib.BusinessLogic
{
    /// <summary>
    /// Handle Contact related business logic
    /// </summary>
    public class UserController : BusinessLogicBase
    {
        #region Constructor
        /// <summary>
        /// Zero argument construtor
        /// </summary>
        public UserController()
        {

        }
        #endregion

        #region Constants
        private const string USER = "Users";
        #endregion

        #region Page Level Variables
        #endregion

        #region Public Methods

       
        /// <summary>
        /// Sneha Thakker
        /// DC: 06-3-08
        /// </summary>
        public static DataTable GetAllUsers()
        {
            try
            {
                UserDA uDA = new UserDA();
                return uDA.GetAllUsers();
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserController::GetAllUsers -  ", e);
                throw e;
            }

        }



        /// <summary>
        /// Bhavin Parmar
        /// DC: 10-3-08
        /// </summary>
        public static DataTable GetAllUsersByWhereClause(string WClause)
        {
            try
            {
                UserDA uDA = new UserDA();
                return uDA.GetAllUsersByWhereClause(WClause);
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserController::GetAllUsersByWhereClause -  ", e);
                throw e;
            }
        }       
        /// <summary>
        /// Sneha Thakker
        /// DC: 06-3-08
        /// </summary>
        public static void UpdateUsers(DataTable dtNews)
        {
            try
            {
                UserDA uDA = new UserDA();
                uDA.UpdateUsers(dtNews);

            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] UserController::UpdateUsers -", ex);
                }
                throw ex;
            }
        }        
        /// <summary>
        /// Hetal Savaliya
        /// DC: 11-3-08
        /// </summary>
        public static DataTable GetUsersByWhereClause(string FieldName, string WhereClause, string OrderBy)
        {
            try
            {
                UserDA uDA = new UserDA();
                return uDA.GetUsersByWhereClause(FieldName,WhereClause, OrderBy);

            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] UserController::GetUsersByWhereClause -", ex);
                }
                throw ex;
            }
        }
        public static DataTable GetContactUs()
        {
            try
            {
                UserDA uDA = new UserDA();
                return uDA.GetContactUs();
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserController::GetContactUs -  ", e);
                throw e;
            }

        }
        public static void UpdateContactUs(DataTable dtContactUs)
        {
            try
            {
                UserDA uDA = new UserDA();
                uDA.UpdateContactUs(dtContactUs);

            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] UserController::UpdateContactUs -", ex);
                }
                throw ex;
            }
        }
        /// <summary>
        /// Dinkar Vaja
        /// DC: 5-5-2010
        /// </summary>
       
        public static DataRow CheckUserAuthentication(String userName)
        {
            //string s = System.Web.HttpContext.Current.Request.MapPath(logConfig);            
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            UserDA uDA = new UserDA();
            DataRow drUser = null;

            try
            {
                drUser = uDA.UserAuthencation(userName);
                if (drUser == null)
                    return null;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("User Datarow is not return successfully!!" + e.ToString());
                throw e;
            }
            return drUser;
        }

        /// <summary>
        /// Dipti Swami
        /// DC: 5-7-2010
        /// </summary>  
        /// 
        public static DataRow CheckCandidateUserAuthenticated(String Email)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            UserDA uDA = new UserDA();
            DataRow drUser = null;

            try
            {
                drUser = uDA.CandidateAuthencation(Email);
                if (drUser == null)
                    return null;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("User Datarow is not return successfully!!" + e.ToString());
                throw e;
            }
            return drUser;
        }

        public static DataTable GetEmployerByWhereClause(string WhereClause)
        {
            try
            {
                UserDA uDA = new UserDA();
                return uDA.GetEmployerByWhereClause(WhereClause);
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserController::GetEmployerByWhereClause -  ", e);
                throw e;
            }
        }

        public static DataTable GetAllRole()
        {
            try
            {
                UserDA uDA = new UserDA();
                return uDA.GetAllRole();
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserController::GetAllRole -  ", e);
                throw e;
            }

        }

        public static DataTable GetAdminRoleByUser(string WhereClause)
        {
            try
            {
                UserDA uDA = new UserDA();
                return uDA.GetAdminRoleByUser(WhereClause);
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] UserController::GetAdminRoleByUser -  ", e);
                throw e;
            }
        }

        #endregion

        #region Private Methods

        #endregion

    }
 
}

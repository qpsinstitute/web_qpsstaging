﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Configuration;

namespace Lib.BusinessLogic
{
    public class LogManagerQps
    {
        public LogManagerQps()
        {
            LogFolder = System.Configuration.ConfigurationManager.AppSettings["LogFile"];
            FileNamePrefix = "LogTC";
            EnableLog = true;
        }

        private static readonly LogManagerQps _instance = new LogManagerQps();
        public static LogManagerQps Instance
        {
            get { return _instance; }
        }

        #region Properties
        public string LogFolder { get; set; }

        public bool EnableLog { get; set; }
        public bool LogErrorOnly { get; set; }
        public string FileNamePrefix { get; set; }
        private string FileName { get { return string.Format("{0}Log {1}.txt", FileNamePrefix, DateTime.Today.ToString("MM_dd_yyyy")); } }
        #endregion

        #region Methods
        public bool WriteError(string errorMessage)
        {
#if DEBUG
            Console.WriteLine(string.Format("Error : {0}", errorMessage));
#endif
            return Write(string.Format("Error : {0}", errorMessage));
        }

        public bool WriteError(string infoMessage, params object[] args)
        {
            return WriteError(string.Format(infoMessage, args));
        }
        public bool WriteInfo(string infoMessage, params object[] args)
        {
            return WriteInfo(string.Format(infoMessage, args));
        }

        public bool WriteInfo(string infoMessage)
        {
            if (!LogErrorOnly)
                return Write(string.Format("Info : {0}", infoMessage));
            return true;
        }

        private bool Write(string message)
        {
            if (!EnableLog) { return true; }

            if (string.IsNullOrEmpty(LogFolder))
            {
                return false;
            }
            if (!Directory.Exists(LogFolder))
            {
                try
                {
                    Directory.CreateDirectory(LogFolder);
                }
                catch { return false; }
            }
            StreamWriter streamWriter;
            string FilePath = string.Format("{0}\\{1}", LogFolder, FileName);
            if (!File.Exists(FilePath))
            {
                try
                {
                    streamWriter = File.CreateText(FilePath);
                }
                catch { return false; }
            }
            else
            {
                try
                {
                    streamWriter = new System.IO.StreamWriter(FilePath, true);
                }
                catch { return false; }
            }

            try
            {
                streamWriter.WriteLine(string.Format("{0}\t{1}", DateTime.Now.ToString("MM/dd/yyyy hh:mm ss tt"), message));
                streamWriter.Close();
                streamWriter.Dispose();
                streamWriter = null;
            }
            catch { return false; }

            return true;
        }
        #endregion
    }
}

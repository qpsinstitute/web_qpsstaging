using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using Lib.DataAccess;
using Lib.Classes;

namespace Lib.BusinessLogic
{
    public class eLearnCategoryController : BusinessLogicBase
    {

        #region Constructor
        public eLearnCategoryController()
        {
            //
            // TODO: Add Contructor Logic here
            //
        }
        #endregion

        #region Public Methods

        public static DataTable SelectAllCategoryByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            eLearnCategoryDA eLearnCatDA = new eLearnCategoryDA();
            try
            {
                DataTable dtCat = eLearnCatDA.SelectAllCategoryByWClause(WClause);
                return dtCat;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Category table is not return successfully!!" + ex.ToString());
                throw ex;
            }
        }


        public static DataTable SelectAllCoursesByCategoryID(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            eLearnCategoryDA eLearnCatDA = new eLearnCategoryDA();
            try
            {
                DataTable dtCat = eLearnCatDA.SelectAllCoursesByCategoryID(WClause);
                return dtCat;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Course table is not return successfully!!" + ex.ToString());
                throw ex;
            }
        }

        public static DataTable SelectAllCoursesByCategoryandLocationid(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            eLearnCategoryDA eLearnCatDA = new eLearnCategoryDA();
            try
            {
                DataTable dtCat = eLearnCatDA.SelectAllCoursesByCategoryandlocationID(WClause);
                return dtCat;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Course table is not return successfully!!" + ex.ToString());
                throw ex;
            }
        }

        public static DataSet GetLocationByCourseID(int CourseId)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            eLearnCategoryDA eLearnCatDA = new eLearnCategoryDA();
            try
            {
                DataSet dtCat = eLearnCatDA.GetLocationByCourseID(CourseId);
                return dtCat;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Course table is not return successfully!!" + ex.ToString());
                throw ex;
            }
        }

        public static DataTable Get_eCourse_By_CategoryID(int CategoryID)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            eLearnCategoryDA eLearnCatDA = new eLearnCategoryDA();
            try
            {
                DataTable dtCat = eLearnCatDA.Get_eCourse_By_CategoryID(CategoryID);
                return dtCat;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Course table is not return successfully!!" + ex.ToString());
                throw ex;
            }
        }

        public static DataTable Get_eCourse_By_CategoryID_TypeID(int CategoryID, int CourseTypeID)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            eLearnCategoryDA eLearnCatDA = new eLearnCategoryDA();
            try
            {
                DataTable dtCat = eLearnCatDA.Get_eCourse_By_CategoryID_TypeID(CategoryID, CourseTypeID);
                return dtCat;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Course table is not return successfully!!" + ex.ToString());
                throw ex;
            }
        }
        public static DataTable Select_Course_By_CourseID(int CourseID)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            eLearnCategoryDA eLearnCatDA = new eLearnCategoryDA();
            try
            {
                DataTable dtCourse = eLearnCatDA.Select_Course_By_CourseID(CourseID);
                return dtCourse;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Course table is not return successfully!!" + ex.ToString());
                throw ex;
            }
        }

        public static DataTable SelectAllCourseDetailsByCourseCode(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            eLearnCategoryDA eLearnCatDA = new eLearnCategoryDA();
            try
            {
                DataTable dtCat = eLearnCatDA.SelectAllCourseDetailsByCourseCode(WClause);
                return dtCat;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("CourseDetails table is not return successfully!!" + ex.ToString());
                throw ex;
            }
        }

        #endregion
    }
}

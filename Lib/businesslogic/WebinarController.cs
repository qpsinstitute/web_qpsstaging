using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using log4net.Config;
using System.Data;
using Lib.dataaccess;

namespace Lib.BusinessLogic
{
    public static class WebinarController
    {
        public static DataTable WebinarByID(int WebID)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            WebinarDA WebinDA = new WebinarDA();
            DataSet dswebinr = new DataSet();
            try
            {
                dswebinr = WebinDA.GetWebinarByID(WebID);
                if (log.IsDebugEnabled)
                    log.Debug("Webinars DataTable is return successfully!!");
                return dswebinr.Tables["Webinars"];
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Webinars DataTable is not return!!" + e.ToString());
                throw e;
            }
        }

        public static DataTable GetWebinarOptionByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            WebinarDA objWebinarOptDA = new WebinarDA();
            try
            {
                DataTable dtWebinarOpt = objWebinarOptDA.GetWebinarOptionByWClause(WClause);
                return dtWebinarOpt;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Location Table is not return SuccessFully!!" + e.ToString());
                throw e;
            }
        }

        public static DataTable GetWebinarByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            WebinarDA WebinDA = new WebinarDA();
            try
            {
                DataTable dtWebinar = WebinDA.GetWebinarByWClause(WClause);
                return dtWebinar;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Webinars Table is not return SuccessFully!!" + e.ToString());
                throw e;
            }
        }
    }
}
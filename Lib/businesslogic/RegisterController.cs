using System;
using System.IO;
using System.Web;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using Lib.DataAccess;
using Lib.Classes;
using Lib.BusinessLogic;

namespace Lib.Businesslogic
{
    public class RegisterController : BusinessLogicBase
    {
        #region Constructor
        /// <summary>
        /// Zero argument construtor
        /// </summary>
        public RegisterController()
        {

        }
        #endregion

        #region Puiblic Method

        public static DataTable GetRegisterInfoByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            RegisterDA objRegisterDA = new RegisterDA();

            try
            {
                DataTable dtRegister = objRegisterDA.GetRegisterInfoByWClause(WClause);
                return dtRegister;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] RegisterController::GetRegisterInfoByWClause -  ", e);
                throw e;
            }
        }
        public static DataTable GetRegisterFileByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            RegisterDA objRegisterDA = new RegisterDA();

            try
            {
                DataTable dtRegister = objRegisterDA.GetRegisterFileWClause(WClause);
                return dtRegister;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] RegisterController::GetRegisterInfoByWClause -  ", e);
                throw e;
            }
        }


        public static void UpdateRegisterUser(DataTable Regdt)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            RegisterDA objRegisterDA = new RegisterDA();

            // Begin New Transaction 
            objRegisterDA.BeginTransaction();

            try
            {
                objRegisterDA.UpdateRegisterUser(Regdt);
                objRegisterDA.CommitTransaction();
                objRegisterDA.CloseConnection();

                if (log.IsInfoEnabled)
                    log.Info("User Updated Successfully..");
            }
            catch (System.Exception ex)
            {
                // changing by dipti swami
                //RollBack All The Changes on Failure
                objRegisterDA.RollbackTransaction();
                objRegisterDA.CloseConnection();
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] RegisterController::UpdateRegister -  ", ex);
                throw ex;
            }
        }
        public static void Updateregusterfile(DataTable Regdt)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            RegisterDA objRegisterDA = new RegisterDA();

            // Begin New Transaction 
            objRegisterDA.BeginTransaction();

            try
            {
                objRegisterDA.UpdateRegisterFile(Regdt);
                objRegisterDA.CommitTransaction();
                objRegisterDA.CloseConnection();

                if (log.IsInfoEnabled)
                    log.Info("File Updated Successfully..");
            }
            catch (System.Exception ex)
            {
                // changing by dipti swami
                //RollBack All The Changes on Failure
                objRegisterDA.RollbackTransaction();
                objRegisterDA.CloseConnection();
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] RegisterController::UpdateRegister -  ", ex);
                throw ex;
            }
        }

        // Foe Admin User Login Created By Dipti Swami ////

        public static DataTable CheckAdminUserAuthentication(string userName)
        {
            try
            {
                RegisterDA objAdminDA = new RegisterDA();
                DataTable dtAdmin = objAdminDA.AdminAuthencation(userName);

                return dtAdmin;
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] RegisterController::AdminAuthencation -", ex);
                }
                throw ex;
            }
        }

        public static DataTable GetRegisterUsers(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            RegisterDA objRegisterDA = new RegisterDA();
            try
            {
                DataTable dtRegister = objRegisterDA.GetRegisterUsers(WClause);
                return dtRegister;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] RegisterController::GetRegisterUsers -  ", e);
                throw e;
            }
        }
        public static DataTable GetToExport(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            RegisterDA objRegisterDA = new RegisterDA();
            try
            {
                DataTable dtRegister = objRegisterDA.GetToExport(WClause);
                return dtRegister;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] RegisterController::GetToExport -  ", e);
                throw e;
            }
        }

        #endregion
    
    }
}

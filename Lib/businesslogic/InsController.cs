﻿using System;
using System.Collections.Generic;
using System.Text;
using Lib.BusinessLogic;
using log4net;
using System.Data;
using Lib.DataAccess;
using Lib.Classes;
using Lib.BusinessLogic;
using Lib.dataaccess;

namespace Lib.businesslogic
{
    public class InsController : BusinessLogicBase
    {
          #region Constructor
        /// <summary>
        /// Zero argument construtor
        /// </summary>
        public InsController()
        {

        }
        #endregion

        #region Puiblic Method
      
        // Foe Admin User Login Created By Dipti Swami ////

        public static DataTable CheckAdminUserAuthentication(string userName)
        {
            try
            {
                InsdataDA objAdminDA = new InsdataDA();
                DataTable dtAdmin = objAdminDA.AdminAuthencation(userName);

                return dtAdmin;
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString() + "] RegisterController::AdminAuthencation -", ex);
                }
                throw ex;
            }
        }
        public static DataTable GetINSInfoByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            InsdataDA objInsdataDA = new InsdataDA();

            try
            {
                DataTable dtRegister = objInsdataDA.GetINSInfoByWClause(WClause);
                return dtRegister;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] RegisterController::GetRegisterInfoByWClause -  ", e);
                throw e;
            }
        }

        public static void UpdateINSUser(DataTable Regdt)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            InsdataDA objInsdataDA = new InsdataDA();

            // Begin New Transaction 
            objInsdataDA.BeginTransaction();

            try
            {
                objInsdataDA.UpdateINSUser(Regdt);
                objInsdataDA.CommitTransaction();
                objInsdataDA.CloseConnection();

                if (log.IsInfoEnabled)
                    log.Info("User Updated Successfully..");
            }
            catch (System.Exception ex)
            {
                // changing by dipti swami
                //RollBack All The Changes on Failure
                objInsdataDA.RollbackTransaction();
                objInsdataDA.CloseConnection();
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] InsController::Updateinstructor -  ", ex);
                throw ex;
            }
        }
        public static DataTable GetInsdata(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            InsdataDA objinsDA = new InsdataDA();
            try
            {
                DataTable dtRegister = objinsDA.GetInSdata(WClause);
                return dtRegister;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] RegisterController::GetRegisterUsers -  ", e);
                throw e;
            }
        }
        public static DataTable GetToExport(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            InsdataDA objInsdataDA = new InsdataDA();
            try
            {
                DataTable dtRegister = objInsdataDA.GetToExport(WClause);
                return dtRegister;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] RegisterController::GetToExport -  ", e);
                throw e;
            }
        }
        #endregion
    }
}

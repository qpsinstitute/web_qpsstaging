﻿using System;
using System.IO;
using System.Web;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using Lib.DataAccess;
using Lib.Classes;
using Lib.BusinessLogic;
using Lib.dataaccess;
namespace Lib.businesslogic
{
    public class SurveyController : BusinessLogicBase
    {

        #region Constructor
        /// <summary>
        /// Zero argument construtor
        /// </summary>
        public SurveyController()
        {

        }
        #endregion


        public static DataTable GetSurveyInfoByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
           // RegisterDA objRegisterDA = new RegisterDA();
            SurveyDA objSurveyDA = new SurveyDA();
            try
            {
                DataTable dtRegister = objSurveyDA.GetSurveyInfoByWClause(WClause);
                return dtRegister;
            }
            catch (System.Exception e)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] RegisterController::GetRegisterInfoByWClause -  ", e);
                throw e;
            }
        }

        public static void UpdateSurvey(DataTable Surveydt)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            
            SurveyDA objSurveyDA = new SurveyDA();
            // Begin New Transaction 
            objSurveyDA.BeginTransaction();

            try
            {
                objSurveyDA.UpdateSurvey(Surveydt);
                objSurveyDA.CommitTransaction();
                objSurveyDA.CloseConnection();

                if (log.IsInfoEnabled)
                    log.Info("Survey Updated Successfully..");
            }
            catch (System.Exception ex)
            {
                // changing by dipti swami
                //RollBack All The Changes on Failure
                objSurveyDA.RollbackTransaction();
                objSurveyDA.CloseConnection();
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] RegisterController::UpdateRegister -  ", ex);
                throw ex;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using log4net;
using log4net.Config;
using Lib.DataAccess;

namespace Lib.BusinessLogic
{
    public class MetaTagController : BusinessLogicBase
    {
        #region Constants
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Public Methods
        public static void addMetatagPage(string strPath, HtmlHead Head)
        {
            string MetaTag, MetaDesc, MetaTitle;
            DataTable dtTags = MetaTagController.GetAllMetaTags();
            if (dtTags.Rows.Count > 0)
            {
                dtTags.DefaultView.RowFilter = "path='" + strPath + "'";
                if (dtTags.DefaultView.Count > 0)
                {
                    if (dtTags.DefaultView[0].Row["MetaTags"] != System.DBNull.Value && dtTags.DefaultView[0].Row["MetaTags"] != "")
                    {
                        if (dtTags.DefaultView[0].Row["MetaDescription"] != System.DBNull.Value && dtTags.DefaultView[0].Row["MetaDescription"] != "")
                        {
                            MetaTag = dtTags.DefaultView[0].Row["MetaTags"].ToString();
                            MetaDesc = dtTags.DefaultView[0].Row["MetaDescription"].ToString();                            
                            HtmlMeta meta1 = new HtmlMeta();
                            HtmlMeta meta2 = new HtmlMeta();
                            meta1.Name = "description";
                            meta2.Name = "keywords";
                            meta1.Content = MetaDesc;
                            meta2.Content = MetaTag;
                            Head.Controls.Add(meta1);
                            Head.Controls.Add(meta2);
                        }
                    }
                    if (dtTags.DefaultView[0].Row["PageTitle"] != System.DBNull.Value && dtTags.DefaultView[0].Row["PageTitle"] != "")
                    {
                        MetaTitle = dtTags.DefaultView[0].Row["PageTitle"].ToString();                        
                        Head.Title = MetaTitle;
                    }
                }
                else
                {
                    DataRow drTags = dtTags.NewRow();
                    string PageUrl = strPath;
                    string PageNameText = PageUrl.Substring(PageUrl.LastIndexOf('/'));
                    PageNameText = PageNameText.Substring(1);
                    drTags["PageName"] = PageNameText;
                    drTags["Path"] = PageUrl;
                    dtTags.Rows.Add(drTags);
                    UpdateMetaTags(dtTags);
                }
            }
            else
            {
                DataRow drTags = dtTags.NewRow();
                string PageUrl = strPath;
                string PageNameText = PageUrl.Substring(PageUrl.LastIndexOf('/'));
                PageNameText = PageNameText.Substring(1);
                drTags["PageName"] = PageNameText;
                drTags["Path"] = PageUrl;
                dtTags.Rows.Add(drTags);
                UpdateMetaTags(dtTags);
            }

        }
        public static DataTable GetAllMetaTags()
        {
            try
            {
                MetaTagDA mDA = new MetaTagDA();
                return mDA.GetAllMetaTags();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString() + "]  MetaTagController::GetAllMetaTags -", ex);

                throw ex;
            }
        }

        public static void UpdateMetaTags(DataTable dtMeta)
        {
            try
            {
                MetaTagDA objMetaDA = new MetaTagDA();
                objMetaDA.UpdateMetaTags(dtMeta);

            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                {
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] MetaTagController::UpdateMetaTags -  ", ex);
                }
                throw ex;
            }
        }

        public static DataTable SelectMetaDataByPath(string Path)
        {
            try
            {
                MetaTagDA objMetaDA = new MetaTagDA();
                return objMetaDA.SelectMetaDataByPath(Path);
            }

            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] MetaTagController:SelectMetaDataByPath: - ", ex);
                throw ex;
            }
        }




        #endregion
    }
}

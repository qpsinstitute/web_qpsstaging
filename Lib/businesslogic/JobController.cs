using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using Lib.DataAccess;
using Lib.Classes;
using System.Configuration;

namespace Lib.BusinessLogic
{
    public class JobController : BusinessLogicBase
    {
        #region Constructor
        public JobController()
        {
            //
            //TODO : 
            //
        }
        #endregion

        #region Constants
        private static readonly string logConfig = ConfigurationSettings.AppSettings["Log4Net"];
        #endregion

        #region Public Methods

        /// <summary>
        /// Get Job Posting By WClause 
        /// </summary>
        
        public static DataTable SelectJobPostingByWClause(string WClause)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            JobDA objJobDA = new JobDA();
            try
            {
                DataTable dtJob = objJobDA.SelectJobPostingByWClause(WClause);
                return dtJob;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("JobPosting table is not return successfully!!" + ex.ToString());
                throw ex;
            }
        }
        
        /// <summary>
        /// Get Job Posting Data
        /// Added By Dipti
        /// </summary>
        /// 
        public static DataSet SelectAllJobPosting()
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            JobDA objJobDA = new JobDA();
            DataSet dsJob = new DataSet();
            try
            {
                dsJob = objJobDA.SelectAllJobPosting();
                if (null == dsJob)
                    return null;
            }
            catch (System.Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Debug("JobPosting data is not return successfully!!" + ex.ToString());
                throw ex;
            }

            return dsJob;
        }

        public static void UpdateJobPosting(DataTable dtJob)
        {
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            JobDA objDA = new JobDA();
            objDA.BeginTransaction();
            try
            {
                objDA.UpdateJobPosting(dtJob);

                objDA.CommitTransaction();

                objDA.CloseConnection();

                if (log.IsInfoEnabled)
                    log.Info("Job Posting Table Successfully Updated....");
            }
            catch (System.Exception ex)
            {
                objDA.RollbackTransaction();
                objDA.CloseConnection();
                if (log.IsDebugEnabled)
                    log.Debug("Updated Job Posting Failed...!!!" + ex.ToString());
                throw ex;
            }
        }
        
        #endregion
    }
}

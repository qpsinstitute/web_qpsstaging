<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" Codebehind="WebinarDetails.aspx.cs"
    Inherits="QPS.Webinars.WebinarDetails" Title="Webinars Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">

    <script language="javascript" type="text/javascript" src="../Js/Tooltips/dw_event.js"></script>

    <script language="javascript" type="text/javascript" src="../Js/Tooltips/dw_tooltip.js"></script>

    <script language="javascript" type="text/javascript" src="../Js/Tooltips/dw_viewport.js"></script>

    <style type="text/css">
        div#tipDiv 
        {
          background:transparent url(../Images/P_center.gif) repeat-y scroll 0%;
          position:absolute; visibility:hidden; left:0; top:0; z-index:10000;                                                
          font-size:11px; 
          line-height:1.2;          
        }
    </style>

    <script language="javascript" type="text/javascript">
    function doTooltip(e, msg){
        Tooltip.init();
        if(typeof Tooltip == "undefined" || !Tooltip.ready ) 
            return;            
        Tooltip.show(e, msg);
            }
       
    function hideTip(){
        if ( typeof Tooltip == "undefined" || !Tooltip.ready )
        return;
        Tooltip.hide();
     } 
     
     function Show(){
    document.getElementById('updiv').style.display='block';
    }
function Hide(id)
{
    var id=document.getElementById(id);
    if (!(id == null)) 
    { 
        id.style.visibility="hidden";
        id.style.display='none';
    }
}
    function ShowEmailPanel()
    {
     toggle() ;           
    }
    
    setPage("Webinars");
    function ValidateForm()
    {
    //alert("call");
       
       var flag  = false;
        var ctrlFound = true;
        document.getElementById('WebinarOptionIDs').value = "";
        
        var strIds="0";
        var cnt = 1;
        
        while (ctrlFound == true)
        {
        
        var t = document.getElementById("add_product_id_" + cnt)     ;
        if (t != null)
        {   
            if (t.checked==true)
            {
            strIds = strIds + "," + t.value;
            flag = true;
            }            
            cnt++;
        }
        else
        {
        ctrlFound = false;
        break;
        }
        
       }        
            
        if(flag == false)
        {
            alert('Please select atleast one training option');            
        }
           
           document.getElementById('WebinarOptionIDs').value = strIds;
           
         if(flag)
            return true;
        else
            return false;
         
            
            
    }
    function toggle() {
        
                if (document.getElementById('ctl00_cphContent_tblthanks').style.display == 'block' || document.getElementById('<%= imgname.ClientID %>').value == "thanks") 
			    {
                    if (document.getElementById('ctl00_cphContent_tblthanks').style.display == "none") 
                    {
                        document.getElementById('ctl00_cphContent_tblthanks').style.display = 'block';
                        document.getElementById('<%= imgname.ClientID %>').value = "thanks"
                        document.getElementById('image2').src = "../Images/seluparrownormal.gif";
				        document.getElementById('tellFriend').setAttribute("class", "lock");  
                    }
                    else {
                        document.getElementById('ctl00_cphContent_tblthanks').style.display = 'none'
                        document.getElementById('<%= imgname.ClientID %>').value = "thanks"
                        document.getElementById('image2').src = "../Images/downarrownormal.gif";
				        document.getElementById('tellFriend').setAttribute("class", "");  
                    }
                }
            
                else
		        {
                    if (document.getElementById('ctl00_cphContent_tbldata').style.display == "none") 
                    {
                        document.getElementById('ctl00_cphContent_tbldata').style.display = 'block';
                        document.getElementById('<%= imgname.ClientID %>').value = "";
			            document.getElementById('tellFriend').setAttribute("class", "lock");  
                        document.getElementById('image2').src = "../Images/seluparrownormal.gif";
                    }
                    else 
                    {
                        document.getElementById('<%= imgname.ClientID %>').value = "";
                        document.getElementById('ctl00_cphContent_tbldata').style.display = 'none'
			            document.getElementById('tellFriend').setAttribute("class", ""); 
                        document.getElementById('image2').src = "../Images/downarrownormal.gif";
                     }
                }
        }

        function ChekcFriendsemail(source, args) {
            if (document.getElementById('<%= txtYourEmail.ClientID %>').value == document.getElementById('<%= txtFriendsEmail.ClientID %>').value && document.getElementById('<%= txtYourEmail.ClientID %>').value != "" && document.getElementById('<%= txtFriendsEmail.ClientID %>').value!="")
             {
                args.IsValid = false;
             }

        }
        function Resend() {
		document.getElementById('<%= imgname.ClientID %>').value =""
            document.getElementById('ctl00_cphContent_tbldata').style.display = 'block';
            document.getElementById('ctl00_cphContent_tblthanks').style.display = 'none';
        }
    </script>

    <style type="text/css">
        #ctl00_cphContent_tbldata td {padding:0px;}
         .lock {
                border-left:1px solid #D3D4D4; border-bottom:1px solid #D3D4D4;border-right:1px solid #D3D4D4;border-top:1px solid #D3D4D4;background-color:#F1F1F1;
            }         
    </style>
    <div class="span-24 prepend-1 top highcont">
        <div style="width: 100%; font-family: verdana; font-size: 15px; text-align: left;
            color: #5e5e5e;">
            <h1 class="arial13black" style="font-family: verdana; margin-bottom: 5px; font-size: 14px;
                font-weight: bold; padding-left: 10px;">
                <br />
                <asp:Label ID="lblWebinarHeader" runat="server" Text="Label"></asp:Label>
            </h1>
        </div>
        <p style="padding-left: 12px; color: black; font-family: verdana; font-size: 12px;
            text-align: left; line-height: 20px; color: #5e5e5e; padding-bottom: 10px;">
            <asp:Label ID="lblWebinarDescription" runat="server" Text="Label"></asp:Label>
        </p>
        <table style="width: 918px; margin: 0px">
            <tr>
                <td valign="top" style="width: 612px; vertical-align: top; padding: 0;">
                    <form id="Form1" runat="server">
                        <ajax:ScriptManager ID="SM1" runat="Server">
                        </ajax:ScriptManager>
                        <table style="width: 194px; margin: 5px" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="60px;" style="padding: 0;padding-top:22px;">
                                    <div onclick="doTooltip(event, '<img src=/Images/P_TOP.gif /><div style=padding-left:11px;font-size:12px;color:gray;><img  width=200 height=200 src=/Images/Dan.jpg /> <ul> <li style=list-style-type:none;padding-top:8px;><img src=/Images/greenarr.gif />Dan OLeary</li></ul></div> <img src=/Images/P_BOTTOM.gif />')"
                                        onmouseout="hideTip();" style="cursor: pointer; width: 60px; height: 85px;" title="Dan O'Leary">
                                        <img id="imgInstructorImage" runat="server" src="/Images/Dan.jpg" style="height: 60px;
                                            width: 60px; border-width: 0px;">
                                    </div>
                                </td>
                                <td style="padding: 0" valign="middle">
                                    <table bgcolor="#F1F5FC" cellpadding="0" cellspacing="0" style="margin: 0px;">
                                        <tr>
                                            <td colspan="2" >
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="2" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 10px; padding: 0; padding-left: 10px; white-space: nowrap" colspan="2">
                                                <b>Instructor:</b>&nbsp;&nbsp; <a href="#profile" style="color: #0033CC; font-size: 11px;
                                                    text-decoration: underline; font-weight: bold">
                                                    <asp:Label ID="lblInstructorName" Style="white-space: nowrap; margin-right: 10px;"
                                                        runat="server" Text="Label"></asp:Label>
                                                </a>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td style="font-family: verdana; font-size: 11px; font-weight: bold; padding: 0;
                                                padding-left: 10px;height: 10px;" colspan="2">
                                                Product ID:
                                                <asp:Label ID="lblWebinarId" runat="server" Text="Label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="height: 15px; padding: 0;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="0" cellpadding="0" border="0" style="width: 272px; padding: 0px;
                            margin: 0px;">
                            <tr>
                                <td style="padding: 0px; vertical-align: top; width: 152px" valign="top">
                                    <table cellspacing="0" cellpadding="0" width="" border="0" valign="top" style="width: 115px;
                                        margin: 0px;">
                                        <tr>
                                            <td width="16" style="padding: 0;" valign="top">
                                                <a href="#">
                                                    <img src="../Images/addcalender.gif" border="0"></a></td>
                                            <td colspan="2" style="padding: 0;">
                                            </td>
                                            <td valign="bottom" style="padding: 0;">
                                                &nbsp;<asp:LinkButton ID="addCalender" runat="server" Text="Add To Calendar" Style="font: verdana;
                                                    font-weight: bold; font-size: 12px; color: #0033CC;" OnClick="addCalender_click"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td id="tellFriend" style="padding: 0px; padding-left: 15px; width: 90px" valign="top">
                                    <div style="float: left">
                                        <b>&nbsp;<a href="#" style="font: verdana; font-size: 12px; color: #0033CC;" onclick="ShowEmailPanel();">Tell
                                            a Friend</a></b></div>
                                    <div style="float: right">
                                        <img alt="" src="../Images/downarrownormal.gif" id="image2" style="cursor: pointer;"
                                            onclick="ShowEmailPanel();"><asp:HiddenField ID="imgname" runat="server" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <asp:Panel ID="Panel1" runat="server" CssClass="arial13black" Style="padding-top: 0px;
                            width: 272px;">
                            <ajax:UpdatePanel ID="uppnldata" runat="server">
                                <ContentTemplate>
                                    <table id="tbldata" runat="server" border="0" width="100%" bgcolor="#F1F1F1" cellspacing="0"
                                        cellpadding="0" style="border-left: 1px solid #D3D4D4; border-bottom: 1px solid #D3D4D4;
                                        border-right: 1px solid #D3D4D4; border-top: 1px solid #D3D4D4; display: none;
                                        padding: 0px; margin: 0px;">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <img src="../Images/1px_trans.gif" width="1" height="3"></td>
                                            </tr>
                                            <tr>
                                                <td class="arial13black" valign="middle" style="font-size: 12px; text-align: right"
                                                    align="right">
                                                    Your Name &nbsp;
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtYourName" runat="server" MaxLength="100" Width="120px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="reqyourmail" runat="server" ControlToValidate="txtYourName"
                                                        ErrorMessage="*" ValidationGroup="friend"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="arial13black" valign="middle" style="font-size: 12px; text-align: right"
                                                    align="right">
                                                    Your Email &nbsp;</td>
                                                <td>
                                                    <asp:TextBox ID="txtYourEmail" MaxLength="300" runat="server" Width="120px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="reqemail" runat="server" ControlToValidate="txtYourEmail"
                                                        ErrorMessage="*" ValidationGroup="friend"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regemail" runat="server" ControlToValidate="txtYourEmail"
                                                        ErrorMessage="*" ValidationGroup="friend" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="arial13black" valign="middle" style="font-size: 12px; text-align: right"
                                                    align="right" nowrap="">
                                                    Friend's Name &nbsp;</td>
                                                <td>
                                                    <asp:TextBox ID="txtfriendsname" MaxLength="100" runat="server" Width="120px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="reqfriendsname" runat="server" ControlToValidate="txtfriendsname"
                                                        ErrorMessage="*" ValidationGroup="friend"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="arial13black" maxlength="300" valign="middle" style="font-size: 12px;
                                                    text-align: right" align="right">
                                                    Friend's Email &nbsp;</td>
                                                <td>
                                                    <asp:TextBox ID="txtFriendsEmail" runat="server" Width="120px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="reqfriendemail" runat="server" ControlToValidate="txtFriendsEmail"
                                                        ErrorMessage="*" ValidationGroup="friend"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regfriendemail" runat="server" ControlToValidate="txtFriendsEmail"
                                                        ErrorMessage="*" ValidationGroup="friend" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="arial13black" valign="middle" style="font-size: 12px; text-align: right"
                                                    align="right">
                                                    Message &nbsp;</td>
                                                <td>
                                                    <asp:TextBox ID="txtMessage" runat="server" Width="120px" TextMode="MultiLine" Columns="13"
                                                        Height="50px" Rows="3"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: right;">
                                                    <asp:CustomValidator ID="customemail" runat="server" Text="Yours and your's friend email would not be same "
                                                        ClientValidationFunction="ChekcFriendsemail" ValidationGroup="friend" />
                                                    <asp:Button ID="btnsubmit" ValidationGroup="friend" runat="server" Text="Submit"
                                                        OnClick="btnsubmit_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <img src="../Images/1px_trans.gif" width="1" height="3"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </ContentTemplate>
                            </ajax:UpdatePanel>
                            <ajax:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table id="tblthanks" runat="server" border="0" width="100%" bgcolor="#F1F1F1" cellspacing="0"
                                        cellpadding="0" style="border-left: 1px solid #D3D4D4; border-bottom: 1px solid #D3D4D4;
                                        border-right: 1px solid #D3D4D4; border-top: 1px solid #D3D4D4; display: none;">
                                        <tr>
                                            <td style="width: 100%">
                                                <img src="../Images/1px_trans.gif" width="1" height="3" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; width: 100%;">
                                                <asp:Label ID="lblthanks" runat="server" Font-Bold="true" Font-Size="Medium" Text="Thank You!"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; width: 100%; padding-left: 30px;">
                                                <asp:Label ID="lbldetail" runat="server" Text="The Product link has been sent to your friend."></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; width: 100%; padding-left: 30px;">
                                                <a href="#" onclick="javascript:Resend();">Do you want to send it to another friend?</a>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </ajax:UpdatePanel>
                            <ajax:UpdateProgress ID="uppnl" runat="Server" AssociatedUpdatePanelID="uppnldata"
                                DynamicLayout="false">
                                <ProgressTemplate>
                                    <div id="img" style="background-color: #F1F1F1">
                                        <table align='center' border='0' width='100%'>
                                            <tr>
                                                <td align='center'>
                                                    <b>Processing</b>
                                                    <br>
                                                    <img src='../Images/load.gif' />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ProgressTemplate>
                            </ajax:UpdateProgress>
                        </asp:Panel>
                    </form>
                </td>
                <td align="right" valign="top" style="width: 451px; vertical-align: top; padding: 10px;">
                    <form id="frmWebinars" name="frmWebinars" action="http://localhost:6759/Webinars/ShoppingCart.aspx"
                        method="post">
                        <input type="hidden" name="WebinarOptionIDs" id="WebinarOptionIDs" />
                        <table cellpadding="0" cellspacing="0" bgcolor="#E3EDF9" style="padding: 10px;">
                            <tr>
                                <td style="padding: 0; font-family: verdana;">
                                    <b>Training Options</b>
                                </td>
                                <td>
                                    <b style="color: #5e5e5e; font-family: verdana;">Training Duration =
                                        <asp:Label ID="lblTrainingDuration" runat="server" Text="Label"></asp:Label></b></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-top: 1px solid #CCCCCC;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" style="text-align: left; padding: 0; line-height: 20px;
                                    vertical-align: top; font-size: 12px; color: #5E5E5E; font-family: verdana; padding-left: 14px;">
                                    <b>Attend <span style="color: #D02422;">Live</span> Online Training Only</b><br>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px; color: #5e5e5e; padding: 0; font-weight: normal; padding-left: 14px;
                                    font-family: Verdana, Geneva, sans-serif;" colspan="2" valign="top">
                                    <asp:Label ID="lblTrainingDate" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <asp:Literal ID="ltWebniarOption" runat="server"></asp:Literal>
                            <tr>
                                <td align="left" valign="top" style="width: 18px; padding: 0; padding-left: 10px;
                                    padding-bottom: 15px;" colspan="2">
                                    <span style="font-size: 10px; color: #5E5E5E; padding-left: 28px;">(For multiple locations
                                        contact Customer Care)</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top: 20px; padding-bottom: 10px; text-align: center; border-top: 1px solid #CCCCCC;"
                                    colspan="2" bgcolor="#E3EDF9" align="center" valign="top" style="padding: 2px 0px">
                                    <input type="submit" value="" style="background: url('../Images/buy.gif'); width: 118px;
                                        height: 24px; border: 0; cursor: pointer;" onclick="if(ValidateForm()) { document.frmWebinars.submit();} else { return false;}">
                                </td>
                            </tr>
                        </table>
                    </form>
                </td>
            </tr>
        </table>
        <div style="font-family: verdana; font-size: 12px; color: #5E5E5E; padding: 6px 0px 8px 0px;
            line-height: 18px;">
            <b>Description </b>
            <br>
        </div>
        <div style="background-color: #808080; margin-right: 6px; height: 1px; width: auto;"
            align="left">
        </div>
        <br />
        <div style="font-family: verdana; font-size: 12px; color: #5E5E5E; line-height: 18px;">
            <p style="text-indent: 0;">
                <strong>Why Should You Attend: </strong>
                <br>
                <asp:Label ID="lblWhyShouldYouAttend" runat="server" Text="Label"></asp:Label>
            </p>
            <br />
            <p style="text-indent: 0;">
                <b>Areas Covered in the seminar</b>:
                <br />
                <asp:Literal ID="ltAreaCoverInThisSeminar" runat="server"></asp:Literal>
            </p>
            <br />
            <p style="text-indent: 0;">
                <b>Who will benefit</b>:</p>
            <p style="text-indent: 0;">
                <asp:Label ID="lblWhowillbenefit" runat="server" Text="Label"></asp:Label>
            </p>
            <br />
            <p style="text-indent: 0;">
                <strong><a name="profile"></a>Instructor Profile</strong>:<br>
                <asp:Label ID="lblInstructorProfile" runat="server" Text="Label"></asp:Label>
            </p>
        </div>
    </div>
</asp:Content>

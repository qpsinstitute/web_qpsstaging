using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using log4net.Config;
using System.Text;

namespace QPS.Webinars
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getWebniarTopits();
        }

        private void getWebniarTopits()
        {
            StringBuilder sb = new StringBuilder();

            DataTable tblWebinar = Lib.BusinessLogic.WebinarController.GetWebinarByWClause(" and Status = 1");



            sb.Append("<ul>");
            foreach (DataRow dr in tblWebinar.Rows)
            {
                sb.Append("<li>");
                sb.Append("&nbsp;&nbsp;<a href=\"WebinarDetails.aspx?WebinarId=" + dr["WebinarID"].ToString() + "\"");
                sb.Append("style=\"word-spacing: 0px; font: medium arial, helvetica, sans-serif; text-transform: none;text-indent: 0px; white-space: normal; letter-spacing: normal; background-color: rgb(255,255,255);text-decoration: none; orphans: 2; widows: 2; webkit-text-size-adjust: auto;webkit-text-stroke-width: 0px\">");
                sb.Append("<span class=\"verdana12black\" style=\"font-size: 12px;color: rgb(0,102,255); font-family: Verdana, Arial, Helvetica, sans-serif\">");
                sb.AppendFormat("{0}", dr["Title"]);
                sb.Append("</span>");
                sb.Append("</a>");
                sb.Append("</li>");
                 
            }
            sb.Append("</ul>");
            LtWebinarTopic.Text = sb.ToString();
        }
    }
}

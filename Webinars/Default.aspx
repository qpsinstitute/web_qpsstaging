﻿<%@ Page Language="C#" AutoEventWireup="true" Codebehind="Default.aspx.cs" Inherits="QPS.Webinars.Default"
    MasterPageFile="~/QPSSite.Master" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="contentwebinars" ContentPlaceHolderID="cphContent" runat="server">
   <style type="text/css">
        #ctl00_cphContent_tbldata td {padding:0px;}
         .lock {
                border-left:1px solid #D3D4D4; border-bottom:1px solid #D3D4D4;border-right:1px solid #D3D4D4;border-top:1px solid #D3D4D4;background-color:#F1F1F1;
            }         
    </style>
    <style type="text/css">
        div#tipDiv 
        {
          background:transparent url(../Images/P_center.gif) repeat-y scroll 0%;
          position:absolute; visibility:hidden; left:0; top:0; z-index:10000;                                                
          font-size:11px; 
          line-height:1.2;          
        }
        
        .span-22
        {
        height:33px !important;
        }
        
    </style>
    <br class="clear">
    <div style="padding: 5px">
        <table border="0" cellpadding="0" cellspacing="0" topmargin="0" style="padding-top: 500"
            leftmargin="0" width="100%">
            <%--<tr>
                <td background="http://static.complianceonline.com/images/training/gred_bg.gif">
                    <span class="verdana12black" style="color: #000000; font-size: 11px;" width="100%"><b>
                        Featured Categories</b> </span><span style="color: #ffffff;">.</span>
                </td>
            </tr>--%>
            <tr>
                <td>
                    <a href="" style="word-spacing: 0px; font: 13px arial, helvetica, sans-serif; text-transform: none;
                        color: rgb(0,0,0); text-indent: 0px; white-space: normal; letter-spacing: normal;
                        background-color: rgb(255,255,255); text-decoration: none; orphans: 2; widows: 2;
                        webkit-text-size-adjust: auto; webkit-text-stroke-width: 0px"><b>Webniars</b>
                    </a>
                </td>
            </tr>
            <tr>
                <td>                   
                    <asp:Literal ID="LtWebinarTopic" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

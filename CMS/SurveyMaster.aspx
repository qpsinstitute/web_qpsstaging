﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPS.Master" AutoEventWireup="true"
    CodeBehind="SurveyMaster.aspx.cs" Inherits="Admin_v2.SurveyMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <script type="text/javascript">

        function GetPath(path) {
            Response.Redirect(path);
        }

        function OptionList() {
            var w = '';
            document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value = '';
            var c = document.getElementsByTagName("*");
            for (i = 0; i < c.length; i++) {
                if (c[i].type == 'checkbox' && c[i].title == 'Status') {
                    if (c[i].checked) {
                        if (c[i].value != 'on')
                            w += c[i].value + ',';
                    }
                }
            }
            document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value = w;
        }
        function AllList() {
            var c = document.getElementsByTagName("*");
            for (i = 0; i < c.length; i++) {
                if (c[i].type == 'checkbox' && c[i].title == 'Status') {
                    if (document.getElementById('chkAll').checked) {
                        c[i].checked = true;
                    }
                    else {
                        c[i].checked = false;
                    }
                }
            }
            OptionList();
        }
        function Confirmations() {
            if (document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value != 0) {
                return true;
            }
            else {
                alert("There is no records selected!");
                return false;
            }
        }

     
      
    </script>
    <ajax:ScriptManager ID="SM1" runat="Server">
    </ajax:ScriptManager>
    <ajax:UpdateProgress ID="UPGR1" runat="Server" DisplayAfter="10">
        <ProgressTemplate>
            <div id="loading" style="position: absolute; top: 40%; left: 50%;">
                <img alt="Loading...." src="/images/ajax-loader.gif">
            </div>
        </ProgressTemplate>
    </ajax:UpdateProgress>
    <ajax:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div id="toolbar-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <div class="toolbar" id="toolbar">
                        <table class="toolbar">
                            <tr>
                                <%-- <td id="toolbar-new" class="button">
                                   <a class="toolbar" ><span class="icon-32-new" title="New"></span>
                                New</a>
                                </td>--%>
                                <td class="button" id="toolbar-Delete" style="height: 48px; display:none;" >
                                    <a id="LnkDelete" runat="server" href="#" onclick="OptionList();return Confirmations()"
                                        class="toolbar" onserverclick="Delete_Click"><span class="icon-32-cancel" title="Delete">
                                            <br />
                                        </span>Delete </a>
                                </td>
                                 <td class="button" id="toolbar-unarchive">
                                    <a id="LnkActive" runat="server" href="#" onclick="OptionList();return Confirmations()"
                                        class="toolbar" onserverclick="LnkActive_Click"><span class="icon-32-unarchive" title="Active">
                                        </span>Active </a>
                                </td>
                                <td class="button" id="toolbar-archive">
                                    <a id="LnkInactive" runat="server" href="#" onclick="OptionList();return Confirmations()"
                                        class="toolbar" onserverclick="LnkInactive_Click"><span class="icon-32-archive" title="Inactive">
                                        </span>Inactive </a>
                                </td>
                                <td class="button" id="toolbar-Cancel" style="height: 48px">
                                    <a href="Default.aspx" class="toolbar"><span class="icon-32-cancel" title="Cancel"></span>
                                        Cancel </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="header icon-48-user">
                        &nbsp;Survey Manager</div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div id="element-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <table width="100%">
                        <tr>
                            <td align="left">
                                Filter:
                                <asp:TextBox ID="txtDesc" runat="server" ToolTip="Filter" MaxLength="255" size="20"></asp:TextBox>&nbsp;
                                <button id="btnGo" runat="server" onserverclick="imgGo_Click" title="Go">
                                    Go</button>&nbsp;
                                <button id="btnReset" runat="server" title="Reset" onserverclick="btnReset_Click">
                                    Reset</button>
                            </td>
                            <td align="right" style="display: none;">
                                Resume & Cover Letter Search:
                                <asp:TextBox ID="txtSearch" runat="server" ToolTip="Filter" MaxLength="255" size="20"></asp:TextBox>&nbsp;
                                <button id="btnSearch" runat="server" onserverclick="btnSearch_Click" title="Search">
                                    Search</button>&nbsp;
                            </td>
                       
                            <td align="right">
                           <span style="font-size: 11px;">Select course type:</span>
                                <asp:DropDownList ID="ddlCourseType" runat="server" AutoPostBack="true" DataTextField="CourseName"
                                    DataValueField="CourseId" ToolTip="Select Category" CssClass="inputbox" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="eLearning" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Training" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                 <asp:DropDownList ID="ddlStatus" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                    <asp:ListItem Value="None">- Select status -</asp:ListItem>
                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                    <asp:ListItem Value="0">Inactive</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <div style="color: red">
                        <span id="ErrorMsg" style="font-weight: bold;" runat="server"></span>
                    </div>
                    <asp:GridView ID="gvCandidate" runat="server" AutoGenerateColumns="false" Width="100%"
                        ForeColor="#666666" CellPadding="5" BorderColor="#E7E7E7" PagerStyle-HorizontalAlign="Center"
                        HeaderStyle-BackColor="#F0F0F0" BorderWidth="1px" BorderStyle="Solid" AllowPaging="True"
                        PageSize="10" AllowSorting="true" OnSorting="gvCandidate_Sorting" OnPageIndexChanging="gvCandidate_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="3%">
                                <HeaderTemplate>
                                    <input id="chkAll" name="all" onclick="AllList();" type="checkbox" value="all" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td align="center">
                                                <input id="lnkDelete" runat="server" name='list' title="Status" type="checkbox" value='<%#DataBinder.Eval(Container.DataItem,"SurveyId") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" ItemStyle-VerticalAlign="top" SortExpression="Name"
                                ItemStyle-Width="12%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"Name") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address 1" ItemStyle-VerticalAlign="top" SortExpression="Address"
                                ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "Address")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address 2" ItemStyle-VerticalAlign="top" SortExpression="Address2"
                                ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "Address2")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City" ItemStyle-VerticalAlign="top" SortExpression="City"
                                ItemStyle-Width="7%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"City") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State" ItemStyle-VerticalAlign="top" SortExpression="State"
                                ItemStyle-Width="7%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "State")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email" ItemStyle-VerticalAlign="top" ItemStyle-Width="10%"
                                SortExpression="Email">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <span class="editlinktip hasTip" title="Click Here to View"><a href="AddSurvey.aspx?SurveyId=<%#DataBinder.Eval(Container.DataItem, "surveyId")%>">
                                                    <%#DataBinder.Eval(Container.DataItem,"Email") %>
                                                </a></span>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phone Number" ItemStyle-VerticalAlign="top" SortExpression="Phone"
                                ItemStyle-Width="9%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "Phone")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Course name" ItemStyle-VerticalAlign="top" SortExpression="CourseName"
                                ItemStyle-Width="13%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "CourseName")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Course taken date" ItemStyle-VerticalAlign="top" ItemStyle-Width="15%"
                                SortExpression="Date">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px; padding-right: 15px;">
                                                <%--<%#DataBinder.Eval(Container.DataItem,"Date") %>--%>
                                                <%# Eval("Date", "{0:MM/dd/yyyy}")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle Width="15%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="What did you like?" ItemStyle-VerticalAlign="top" SortExpression="Likes"
                                ItemStyle-Width="13%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "Likes")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Suggested Improvements" ItemStyle-VerticalAlign="Top" 
                                SortExpression="AreaImprovement" ItemStyle-Width="13%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "AreaImprovement")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Comment status" HeaderStyle-Wrap="true" ItemStyle-VerticalAlign="top"
                                ItemStyle-HorizontalAlign="center" ItemStyle-Width="2%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 2px">
                                                <%#DataBinder.Eval(Container.DataItem, "CommentStatus").ToString()=="True" ? "Yes":"No"%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="top" SortExpression="Status"
                                ItemStyle-Width="5%">
                                <ItemTemplate>
                                     <table align="center">
                                        <tr style="background-color: White">
                                            <td align="center">
                                                <span class="editlinktip hasTip">
                                                    <img alt="Published" border="0" height="16" src="../TemplateImages/publish_g.png"
                                                        style="display: <%#DataBinder.Eval(Container.DataItem, "Status").ToString().ToLower() == "true" ? "block" : "none" %>"
                                                        width="16" />
                                                </span><span class="editlinktip hasTip">
                                                    <img alt="Not_Published" border="0" height="16" src="../TemplateImages/publish_x.png"
                                                        style="display: <%#DataBinder.Eval(Container.DataItem, "Status").ToString().ToLower() == "true" ? "none" : "block" %>"
                                                        width="16" />
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <input type="hidden" id="hdnApprover" runat="server" />
                    <tfoot id="PageSize" runat="server" style="background: #F0F0F0 none repeat scroll 0 50%;
                        text-align: center" visible="false">
                        <tr align="center" valign="middle">
                            <td colspan="10">
                                <del class="container">
                                    <div class="pagination" style="background-color: #F0F0F0; text-align: center">
                                        <div class="limit">
                                            Display #
                                            <asp:DropDownList ID="ddlPage" runat="server" AutoPostBack="true" CssClass="inputbox"
                                                OnSelectedIndexChanged="ddlPage_SelectedIndexChanged">
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                <asp:ListItem Value="50">50</asp:ListItem>
                                                <asp:ListItem Value="100">100</asp:ListItem>
                                                <asp:ListItem Value="0">all</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <input name="limitstart" type="hidden" value="0" />
                                    </div>
                                </del>
                            </td>
                        </tr>
                    </tfoot>
                    <div class="clr">
                    </div>
                    <input type="hidden" id="hdnSelectedMessages" runat="server" />
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
        </ContentTemplate>
    </ajax:UpdatePanel>
</asp:Content>

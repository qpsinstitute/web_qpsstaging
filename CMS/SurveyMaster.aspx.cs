﻿using System;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using System.Data.SqlClient;
using Lib.businesslogic;
namespace Admin_v2
{
    public partial class SurveyMaster : System.Web.UI.Page
    {
        
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected static string SortDirection = "DESC";
        protected static string SortExpression = "SurveyId";
        protected static string WClause1 = "";
        protected static int pageno;
        // SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["SQLConnectionString"]);
        SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["SQLConnectionString"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    SortDirection = "ASC";
                    SortExpression = "SurveyId";
                    pageno = 10;
                    BindRepeater();
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] SurveyMaster::Page_Load -  ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        private void BindRepeater()
        {
            try
            {
                string WClause = "";
                if (txtDesc.Text.Trim() != "")
                {
                    WClause += "AND (Name like '%" + txtDesc.Text.Trim() + "%' OR City like '%" + txtDesc.Text.Trim() + "%' OR State like '%" + txtDesc.Text.Trim() + "%' OR address like '%" + txtDesc.Text.Trim() + "%' OR Phone like '%" + txtDesc.Text.Trim() + "%' OR AreaImprovement like '%" + txtDesc.Text.Trim() + "%' OR Likes like '%" + txtDesc.Text.Trim() + "%')";
                }
                if (ddlCourseType.SelectedItem.Value != "None" && int.Parse(ddlCourseType.SelectedValue.ToString()) > 0)
                {
                    WClause += "AND Coursetype = " + ddlCourseType.SelectedValue;
                }
                if (ddlStatus.SelectedItem.Value != "None")
                {
                    WClause += " AND (Status=" + ddlStatus.SelectedItem.Value + ")";
                }
                if (txtSearch.Text.Trim() != "")
                {
                    WClause = WClause1;
                }
                DataTable dtResult = SurveyController.GetSurveyInfoByWClause(WClause);
                if (dtResult.Rows.Count > 0)
                {
                    ErrorMsg.InnerText = "";
                    dtResult.DefaultView.Sort = SortExpression + "  " + SortDirection;
                    gvCandidate.DataSource = dtResult.DefaultView;
                    gvCandidate.DataBind();
                    PageSize.Visible = true;
                }
                else
                {
                    if (ErrorMsg.InnerText != "No Result found")
                    {
                        gvCandidate.DataSource = dtResult;
                        gvCandidate.DataBind();
                    }
                    ErrorMsg.InnerText = "No Result found";
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] SurveyMaster::BindRepeater -  ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void LnkActive_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnSelectedMessages.Value.Trim() != "")
                {
                    string WhereClause = " AND SurveyId in (" + hdnSelectedMessages.Value.Substring(0, hdnSelectedMessages.Value.Length - 1) + ")";
                    DataTable dtResults = SurveyController.GetSurveyInfoByWClause(WhereClause);
                    if (dtResults.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtResults.Rows.Count; i++)
                        {
                            dtResults.Rows[i]["Status"] = true;
                        }
                        SurveyController.UpdateSurvey(dtResults);
                        BindRepeater();
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AdminUserManager::LnkActive_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void LnkInactive_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnSelectedMessages.Value.Trim() != "")
                {
                    string WhereClause = " AND SurveyId in (" + hdnSelectedMessages.Value.Substring(0, hdnSelectedMessages.Value.Length - 1) + ")";
                    DataTable dtResults = SurveyController.GetSurveyInfoByWClause(WhereClause);
                    if (dtResults.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtResults.Rows.Count; i++)
                        {
                            dtResults.Rows[i]["Status"] = false;
                        }
                        SurveyController.UpdateSurvey(dtResults);
                        BindRepeater();
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AdminUserManager::LnkInactive_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void gvCandidate_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortExpression = e.SortExpression;
            if (SortDirection == "ASC")
                SortDirection = "DESC";
            else if (SortDirection == "DESC")
                SortDirection = "ASC";
            BindRepeater();
        }

        protected void imgGo_Click(object sender, EventArgs e)
        {
            try
            {
                SortDirection = "ASC";
                SortExpression = "SurveyId";
                gvCandidate.EditIndex = -1;
                txtSearch.Text = "";
                BindRepeater();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] SurveyMaster::imgGo_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                SortDirection = "ASC";
                SortExpression = "SurveyId";
                gvCandidate.EditIndex = -1;
                txtDesc.Text = "";
                txtSearch.Text = "";
                BindRepeater();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] SurveryMaster::btnSearch_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                pageno = Convert.ToInt32(ddlPage.SelectedValue);
                if (ddlPage.SelectedItem.Value != "0")
                {
                    gvCandidate.AllowPaging = true;
                    gvCandidate.PageSize = int.Parse(ddlPage.SelectedItem.Value);
                    BindRepeater();
                }
                else
                {
                    gvCandidate.AllowPaging = false;
                    BindRepeater();
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] SurveryMaster::ddlPage_SelectedIndexChanged - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void a1_Click(string path)
        {
            Response.TransmitFile(path);
        }

        protected void gvCandidate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCandidate.EditIndex = -1;
            gvCandidate.PageIndex = e.NewPageIndex;
            BindRepeater();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataTable dtTemp = new DataTable();
            try
            {
                DataTable dtResume = CandidateCotroller.GetAllCanidate();
                WClause1 = "AND SurveyId in (";
                bool flag = false;
               
                if (flag)
                {
                    WClause1 = WClause1.Substring(0, WClause1.Length - 1) + ")";
                    BindRepeater();
                }
                else
                {
                    WClause1 += " 0 )";
                    BindRepeater();
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] SurveyMaster::btnSearch_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvCandidate.EditIndex = -1;
            SortDirection = "ASC";
            SortExpression = "SurveyId";
            BindRepeater();
        }
        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvCandidate.EditIndex = -1;
            SortDirection = "ASC";
            SortExpression = "SurveyId";
            BindRepeater();
        }
        protected void Delete_Click(object sender, EventArgs e)
        {
            string temp = hdnSelectedMessages.Value.Substring(0, hdnSelectedMessages.Value.Length - 1);
            string[] ids = temp.Split(',');
            int cnt = 0;
            if (ids.Length != 0)
            {
                gvCandidate.EditIndex = -1;
                while (cnt != ids.Length)
                {
                    try
                    {
                        String Qry = "DELETE FROM Survey WHERE SurveyId=" + Convert.ToInt32(ids[cnt]);
                        SqlCommand cm = new SqlCommand(Qry, cn);
                        cn.Open();
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                    catch (Exception ex)
                    {
                        if (log.IsErrorEnabled)
                            log.Error("[" + System.DateTime.Now.ToString("G") + "]Survey Manager::Delete_Click - ", ex);
                        Response.Redirect("/errorpages/SiteError.aspx", false);
                    }
                    cnt++;
                }
            }
            BindRepeater();
        }
    }
}
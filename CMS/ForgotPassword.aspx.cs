using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Text;
using log4net;
using Lib.BusinessLogic;


namespace Admin_v2
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        DataTable dtUser;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorMsg1.InnerText = "";
            ErrorMsg.InnerText = "";
        }
        protected void lnkFPassword_ServerClick(object sender, EventArgs e)
        {
                try
                {
                        string wClause = "And u.Email ='" + txtUserID.Text.ToString().Trim() + "'";
                        wClause += "And u.Status = 1";
                        dtUser = UserController.GetAllUsersByWhereClause(wClause);
                        if (dtUser.Rows.Count > 0)
                        {
                            string password = dtUser.Rows[0]["Password"].ToString();
                            DataRow drUser = dtUser.Rows[0];
                            UserController.UpdateUsers(dtUser);
                            SendMail(password, dtUser.Rows[0]["Email"].ToString().Trim());
                        }
                        else
                            ErrorMsg1.InnerText = "Unathorized User. Please contact to Administrator.";
                }
                catch (Exception ex)
                {
                    if (log.IsErrorEnabled)
                        log.Error("[" + System.DateTime.Now.ToString("G") + "] ForgotPassword::lnkGetPassword_Click - ", ex);
                    Response.Redirect("/errorpages/SiteError.aspx", false);
                }
            }
        private void SendMail(string Password, string LoginName)
        {
            Hashtable hsUser = new Hashtable();
            string To = "";
            string From = "";
            string Subject = "";
            string Body = "";
            hsUser.Add("$$LOGIN_NAME$$", LoginName);
            hsUser.Add("$$PASSWORD$$", Password);
            To = txtUserID.Text.ToString().Trim();
            From = ConfigurationManager.AppSettings["PasswordResetMail"].ToString();
            Subject = ConfigurationManager.AppSettings["ForgotPasswordSubject"].ToString();
            if (txtUserID.Text.ToString() == dtUser.Rows[0]["Email"].ToString())
            Body = EmailController.GetBodyFromTemplate("/EmailTemplate/ForgotPassword.htm", hsUser);
            if (To.Trim().Length > 0)
            {
                if (EmailController.SendMail(From, To, Subject, Body, false))
                {
                    ErrorMsg.InnerText = "Your password has been emailed to you.";
                }
                else
                {
                    ErrorMsg.InnerText = "You have not successfully reset your password.";
                }
            }
        }

    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Lib.BusinessLogic;

namespace Admin_v2
{
    public partial class QPS : System.Web.UI.MasterPage
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string FullName = "";
        public int role;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["dtLoginUser"] != null)
                {
                    DataTable dtUser = (DataTable)Session["dtLoginUser"];
                    FullName = "Welcome " + dtUser.Rows[0]["FirstName"].ToString().Trim() + " " + dtUser.Rows[0]["LastName"].ToString().Trim() + " ";
                    role = Convert.ToInt32(dtUser.Rows[0]["RoleID"]);

                    if (role == 1)
                    {
                        liadd.Visible = true;
                        liview.Visible = true;
                    }
                    else
                    {
                        liadd.Visible = false;
                        liview.Visible = false;
                        smltitle.InnerText = "Users Profile";
                    }
                }
                else
                    FormsAuthentication.RedirectToLoginPage();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] ELearnAdmin::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["dtLoginUser"] != null)
                {
                    Session.Clear();
                    Session.Remove("dtLoginUser");

                }
                Response.Redirect("~/SignIn.aspx?rmvCookie=yes", false);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] ELearnAdmin::lnkLogout_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }

        }
    }
}

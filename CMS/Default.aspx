<%@ Page Language="C#" MasterPageFile="~/QPS.Master" AutoEventWireup="true" Codebehind="Default.aspx.cs"
    Inherits="Admin_v2._Default" %>

<asp:Content ID="cphHome" runat="Server" ContentPlaceHolderID="ContentPlaceHolder">
    <div class="padding">
        <div id="element-box">
            <div class="t">
                <div class="t">
                    <div class="t">
                    </div>
                </div>
            </div>
            <div class="m">
                <table class="adminform">
                    <tr>
                        <td valign="top" width="100%">
                            <div id="cpanel">
                                <table style="width: 100%">
                                    <tr>
                                        <td align="left" style="width: 100%">
                                            <div style="background-color: #CCCCCC; height: 20px; padding: 2px 2px 0px 2px">
                                                <b style="font-size: 13px">&nbsp;Job Management</b>
                                            </div>
                                            <br />
                                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <div style="float: left;">
                                                            <div class="icon">
                                                                <a href="../Job/JobPostingManager.aspx" title="Job Posting Manager">
                                                                    <img alt="Job Posting Manager" src="TemplateImages/icon-48-category.png" />
                                                                    <span>Job Posting Manager</span> </a>
                                                            </div>
                                                        </div>
                                                        <div style="float: left;">
                                                            <div class="icon">
                                                                <a href="../Job/CategoryManager.aspx" title="Job Category Manager">
                                                                    <img alt="Category Manager" src="TemplateImages/icon-48-article.png" />
                                                                    <span>Job Category Manager</span> </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="background-color: #CCCCCC; height: 20px; padding: 2px 2px 0px 2px">
                                                <b style="font-size: 13px">&nbsp;Candidate Management</b></div>
                                            <br />
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div style="float: left;">
                                                            <div class="icon">
                                                                <a href="../CandidateManager.aspx" title="Candidate Manager">
                                                                    <img alt="Candidate Manager" src="TemplateImages/icon-48-user.png" />
                                                                    <span>Candidate Manager</span> </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="background-color: #CCCCCC; height: 20px; padding: 2px 2px 0px 2px; ">
                                               <span id="smltitle" runat="server" style="font-size: 13px; font-weight:bold; float:left; padding-left:3px;">&nbsp;Admin/Employer Management</span></div>
                                            <br />
                                            <table>
                                                <tr>
                                                    <td id="truser" runat="server" visible="false">
                                                        <div style="float: left;" id="trview">
                                                            <div class="icon">
                                                                <a href="../User/AdminUserManager.aspx" title="View Admin/Employer">
                                                                    <img alt="View Admin/Employer" src="TemplateImages/icon-48-user.png" />
                                                                    <span>Admin/Employer Manager</span> </a>
                                                            </div>
                                                        </div>
                                                         <div style="float: left;" id="tradd">
                                                            <div class="icon">
                                                                <a href="../User/AddAdminUser.aspx" title="Add Admin/Employer">
                                                                    <img alt="Add Admin/Employer" src="TemplateImages/icon-48-user.png" />
                                                                    <span>Add Admin/Employer</span> </a>
                                                            </div>
                                                        </div>
                                                       </td>
                                                       <td>
                                                        <div style="float: left;" id="trprofile">
                                                            <div class="icon">
                                                                <a href="../User/MyProfile.aspx" title="My Profile">
                                                                    <img alt="My Profile" src="TemplateImages/icon-48-user.png" />
                                                                    <span>My Profile</span> </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                      <tr>
                                        <td>
                                            <div style="background-color: #CCCCCC; height: 20px; padding: 2px 2px 0px 2px; ">
                                               <span id="Span1" runat="server" style="font-size: 13px; font-weight:bold; float:left; padding-left:3px;">&nbsp;Testimonial Management</span></div>
                                            <br />
                                            <table>
                                                <tr>
                                                   <td>
                                                        <div style="float: left;" id="Div3">
                                                            <div class="icon">
                                                                <a href="../SurveyMaster.aspx" title="Survey Manager">
                                                                    <img alt="My Profile" src="TemplateImages/icon-48-user.png" />
                                                                    <span>Survey Manager</span> </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="clr">
                </div>
            </div>
            <div class="b">
                <div class="b">
                    <div class="b">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

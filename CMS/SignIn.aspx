<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="ElearnAdmin.SignIn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html dir="ltr" lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>QPS Administration - SignIn</title>
    
    <script src="js/mootools.js" type="text/javascript"></script>
    <link href="css/system.css" rel="stylesheet" type="text/css" />
    <link href="css/rounded.css" rel="stylesheet" type="text/css" />
    <link href="css/login.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        pic1= new Image(); 
        pic1.src="images/Chk_Correct.gif";
        pic2= new Image(); 
        pic2.src="images/Chk.gif";
        pic3= new Image(); 
        pic3.src="images/spinner.gif";    
        function Set()
        {
            if(document.getElementById("chkVal").value == "false")
            {
                document.getElementById("chkImg").src = pic1.src;
                document.getElementById("chkVal").value = "true";
            }
            else
            {
                document.getElementById("chkImg").src = pic2.src;
                document.getElementById("chkVal").value = "false";
            }                
        }
        function UnSet()
        {
          if (document.getElementById("chkImg") != null)
            document.getElementById("chkImg").src = pic2.src;
          if (document.getElementById("chkVal") != null)
            document.getElementById("chkVal").value = "false";      
        }
        function Setfocus()
        {
            if (document.getElementById("txtUserID") != null)
                document.getElementById("txtUserID").focus();  
	        var usercookie=getCookie("ELearnAdminId");
            var pwcookie=getCookie("ELearnAdminPwd");
            usercookie=usercookie!=null?usercookie:"";
            pwcookie=pwcookie!=null?pwcookie:"";   
            if(usercookie == "" && pwcookie == "")
            {
                document.getElementById("txtUserID").style.color = "#808080";     
                document.getElementById("txtPassword").style.color = "#808080";
            }
        }
	    function setCookie(name, value, expires, path, domain, secure)
	    {
		    var curCookie = name + "=" + escape(value) +
			    ((expires) ? "; expires=" + expires.toGMTString() : "") +
			    ((path) ? "; path=" + path : "") +
			    ((domain) ? "; domain=" + domain : "") +
			    ((secure) ? "; secure" : "");
		    document.cookie = curCookie;			
	    }			
	    function getCookie(name)
	    {
		    var dc = document.cookie;
		    var prefix = name + "=";
		    var begin = dc.indexOf("; " + prefix);
		    if (begin == -1)
		    {
			    begin = dc.indexOf(prefix);
			    if (begin != 0) return null;
		    }
		    else
		    {
			    begin += 2;
		    }
		    var end = document.cookie.indexOf(";", begin);
		    if (end == -1)
			    end = dc.length;
		    return unescape(dc.substring(begin + prefix.length, end));
	    }
	    function securityWarning()
	    {		
		    var frm = document.forms[0];
		    var chkStatus;				
		    chkStatus=document.getElementById("chkVal").value;
		    if (chkStatus == false)
		    {					
			    return;
		    }
		    if ( ! AreCookiesEnabled() )
		    {
			    alert('It appears that your browser blocks cookies.\n' +
			    'Please enable cookies if you would like to use the "Remember Me" feature.');
			    document.getElementById("chkVal").value=false;
			    return;
		    }				
		    return;
	    }
	    function AreCookiesEnabled() 
	    {
		    var expires = new Date();
		    expires.setTime(expires.getTime() + 60 * 60 * 1000);
		    setCookie("PerpetuatingAdminCookieTest", "Test whether cookies are enabled", expires);
		    if ( getCookie("PerpetuatingAdminCookieTest") == null )
			    return false;
		    expires = new Date();
		    expires.setTime(expires.getTime() - 1);
		    setCookie("PerpetuatingAdminCookieTest", "Remove test cookie", expires);  
		    return true;
	    }		
	    function doLoginCookie ()
	    {	
	        document.getElementById("Img").style.visibility = "visible";	
	        document.getElementById("Img").style.display = "inline";
	        document.getElementById("Img").src = pic3.src;
		    var frm = document.forms[0];
		    var s = document.getElementById("chkVal").value;
		    if (s != "false")
		    {	    		
			    setRememberIdAndPasswordCookie();
		    }
		    else
		    {
			    unsetRememberIdAndPasswordCookie();
		    }	
		    window.location.href="Default.aspx";
	    }	
	    function doLoginCookie1 (rstr)
	    {	
	        document.getElementById("Img").style.visibility = "visible";	
	        document.getElementById("Img").style.display = "inline";
	        document.getElementById("Img").src = pic3.src;
		    var frm = document.forms[0];
		    var s = document.getElementById("chkVal").value;
		    if (s != "false")
		    {	    		
			    setRememberIdAndPasswordCookie();
		    }
		    else
		    {
			    unsetRememberIdAndPasswordCookie();
		    }	
		    window.location.href=rstr;
	    }	    		
	    function setRememberIdAndPasswordCookie()
	    {			
		    var now = new Date();
		    now.setTime(now.getTime() + 1000 * 60 * 60 * 24 * 14);
		    RememberIdAndPasswordCookie(now);						
	    }
	    function unsetRememberIdAndPasswordCookie()
	    {		
		    var expires = new Date();
		    expires.setTime(expires.getTime() - 1);				
		    RememberIdAndPasswordCookie(expires);					
	    }
	    function RememberIdAndPasswordCookie(expiration)
	    {	
		    setCookie("ELearnAdminId", '<%=UserName%>', expiration);					
		    setCookie("ELearnAdminPwd", '<%=PassWord%>', expiration);					
	    }	            
    </script>

</head>
<body onload="javascript:Setfocus();UnSet();">
    <div id="border-top" class="h_green">
        <div>
            <div>
                <span class="title">QPS Administration </span>
            </div>
        </div>
    </div>
    <div id="content-box">
        <div class="padding">
            <div id="element-box" class="login">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <h1>
                        QPS Administration Login</h1>
                    <div id="section-box">
                        <div class="t">
                            <div class="t">
                                <div class="t">
                                </div>
                            </div>
                        </div>
                        <div class="m">
                            <form id="LoginForm" runat="server" style="clear: both;">
                                <p id="form-login-username">
                                    <label for="modlgn_username">
                                        Username</label>
                                    <input id="txtUserID" runat="server" name="username" size="15" style="margin-left: 10px; width: 120px;" type="text" />
                                </p>
                                <p id="form-login-password">
                                    <label for="modlgn_passwd">
                                        Password</label>
                                    <input id="txtPassword" runat="server" name="passwd" size="15" style="margin-left: 10px; width: 120px;" type="password" />
                                </p>
                                <p style="padding-left: 40px">
                                    <a onclick="Set();" style="text-decoration: none">
                                        <img id="chkImg" alt="" src="" style="height: 16px; vertical-align: middle; width: 16px;" />
                                        <span class="label" style="padding-left: 10px; color: #666666; font-weight: bold">Remember me on this computer.</span></a>
                                </p>
                                <span style="padding-left: 40px">
                                    <asp:Label ID="ErrMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                    <input id="chkVal" runat="server" type="hidden" value="" />
                                    <img id="Img" runat="server" alt="spin" src="" style="display: none; visibility: hidden" />
                                </span>
                                <div class="button_holder">
                                    <div class="button1">
                                        <div class="next">
                                            <a id="lnkLogin" runat="server" href="#" onserverclick="lnkLogin_Click">Login</a>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                 
                                <div class="clr">
                                </div>
                                <p style="padding-left: 40px">
                                        <span class="label" style="padding-left: 10px; color: #666666; font-weight: bold"> <a href="ForgotPassword.aspx">Forgot Password?</a></span>
                                </p>
                                <tr>
                                <td>
                                    <asp:Label ID="lblmsg" runat="server"></asp:Label>
                                </td>
                            </tr>
                            </form>
                            <div class="clr">
                            </div>
                        </div>
                        <div class="b">
                            <div class="b">
                                <div class="b">
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>
                        Use a valid username and password to gain access to the Administrator Back-end.</p>
                    <div id="lock" style="vertical-align: text-top">
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <noscript>
                Warning! JavaScript must be enabled for proper operation of the Administrator back-end.
            </noscript>
            <%if (Request.QueryString["rmvCookie"] == "yes")
          {%>

            <script language="javascript" type="text/javascript">
            unsetRememberIdAndPasswordCookie();
            window.location.href = unescape(window.location.pathname);
            </script>

            <%} %>

            <script language="javascript" type="text/javascript">
            var usercookie=getCookie("ELearnAdminId");
            var pwcookie=getCookie("ELearnAdminPwd");
            usercookie=usercookie!=null?usercookie:"";
            pwcookie=pwcookie!=null?pwcookie:"";		
            var frm = document.forms[0];
//            if ( frm.<%= txtUserID.ClientID%>!=null && usercookie!="" && pwcookie!="")
//	            document.getElementById("chkVal").value = true;					
            if ( frm.<%= txtUserID.ClientID%>!=null && usercookie=="" )
	            frm.<%= txtUserID.ClientID%>.focus();	
            if ( frm.<%= txtUserID.ClientID%> != null )
	            frm.<%= txtUserID.ClientID%>.value=usercookie;		    
            if ( frm.<%= txtPassword.ClientID%> != null )
	            frm.<%= txtPassword.ClientID%>.value=pwcookie;			        	 	 
            if ( usercookie != "" && pwcookie != "" && document.getElementById("txtUserID").className != "Warning" && document.getElementById("txtPassword").className != "Warning")
                document.LoginForm.submit();               
            </script>

            <div class="clr">
            </div>
        </div>
    </div>
    <div id="border-bottom">
        <div>
            <div>
            </div>
        </div>
    </div>
    <div id="footer">
        <p class="copyright">
            � <%= System.DateTime.Now.Year %> Perpetuating
        </p>
    </div>
</body>
</html>

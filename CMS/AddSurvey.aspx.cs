﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using Lib.businesslogic;
namespace Admin_v2
{
    public partial class AddSurvey : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static DataTable dtCandidate = null;
        public static int count = 0;
        public static int CandidateId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["SurveyId"] != null)
                    {
                        smltitle.InnerText = "[ View ]";
                        string WClause = "AND SurveyId =" + Request.QueryString["SurveyId"];
                        DataTable dtCandi = SurveyController.GetSurveyInfoByWClause(WClause);
                        if (dtCandi.Rows.Count > 0)
                        {
                            BindDataEditTime(dtCandi);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AddSurvey::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        private void BindDataEditTime(DataTable dt)
        {
            try
            {
                lblName.Text = dt.Rows[0]["Name"].ToString();
                lblAddress.Text = dt.Rows[0]["Address"].ToString();
                lblAddress2.Text = dt.Rows[0]["Address2"].ToString();
                lblCity.Text = dt.Rows[0]["City"].ToString();
                lblState.Text = dt.Rows[0]["State"].ToString();
                lblLikes.Text = dt.Rows[0]["Likes"].ToString();
                lblAreaImprovement.Text = dt.Rows[0]["AreaImprovement"].ToString();
                lblPhone.Text = dt.Rows[0]["Phone"].ToString();
                lblEmail.Text = dt.Rows[0]["Email"].ToString();
                lblCouseName.Text = dt.Rows[0]["CourseName"].ToString();
                if (dt.Rows[0]["CommentStatus"].ToString() == "True")
                {
                        lblCommentStatus.Text = "Yes";
                }
                else
                {
                        lblCommentStatus.Text = "No";
                }
            
                lblDate.Text = dt.Rows[0]["Date"].ToString();
           
               
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AddSurvey::BindDataEditTime() - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

   

        //protected void btnCancel_Click(object sender, EventArgs e)
        //{
        //    Response.Clear();
        //    Response.Redirect("SurveyMaster.aspx");
        //}

       
    }
}
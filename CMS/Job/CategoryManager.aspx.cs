using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;

namespace Admin_v2.Job
{
    public partial class CategoryManager : System.Web.UI.Page
    {
        protected static string SortDirection = "DESC";
        protected static string SortExpression = "CategoryName";
        protected static int pageno;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SortDirection = "ASC";
                SortExpression = "Number";
                pageno = 10;
                BindData();
            }
        }

        private void BindData()
        {
            string WClause = "";
            if (txtDesc.Text.Trim() != "")
            {
                WClause += " AND CategoryName like '%" + txtDesc.Text.Trim() + "%' ";
            }
            if (ddlUserStatus.SelectedItem.Value != "None")
            {
                WClause += " AND Status=" + ddlUserStatus.SelectedItem.Value;
            }

            DataTable dtResults = CategoryController.SelectJobCategoryByWClause(WClause);
            if (dtResults.Rows.Count > 0)
            {
                ErrorMsg.InnerText = "";
                dtResults.DefaultView.Sort = SortExpression + " " + SortDirection;
                gridCategory.DataSource = dtResults.DefaultView;
                gridCategory.DataBind();
                PageSize.Visible = true;
            }
            else
            {
                if (ErrorMsg.InnerText != "No Result found")
                {
                    gridCategory.DataSource = dtResults;
                    gridCategory.DataBind();
                }
                ErrorMsg.InnerText = "No Result found";
                PageSize.Visible = false;
            }
        }

        protected void LnkActive_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnSelectedMessages.Value.Trim() != "")
                {
                    string WhereClause = " AND CategoryID in (" + hdnSelectedMessages.Value.Substring(0, hdnSelectedMessages.Value.Length - 1) + ")";
                    DataTable dtResults = CategoryController.SelectJobCategoryByWClause(WhereClause);
                    if (dtResults.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtResults.Rows.Count; i++)
                        {
                            dtResults.Rows[i]["Status"] = true;
                            string WClause = "AND JObPosting.CategoryID =" + dtResults.Rows[i]["CategoryID"];                            
                            DataTable dtResult1 = JobController.SelectJobPostingByWClause(WClause);
                            if (dtResult1.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtResult1.Rows.Count; j++)
                                {
                                    dtResult1.Rows[j]["Status"] = true;
                                }
                                JobController.UpdateJobPosting(dtResult1);
                            }
                        }
                        CategoryController.UpdateCategory(dtResults);
                        BindData();
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CategoryManager::LnkActive_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void LnkInactive_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnSelectedMessages.Value.Trim() != "")
                {
                    string WhereClause = " AND CategoryID in (" + hdnSelectedMessages.Value.Substring(0, hdnSelectedMessages.Value.Length - 1) + ")";
                    DataTable dtResults = CategoryController.SelectJobCategoryByWClause(WhereClause);
                    if (dtResults.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtResults.Rows.Count; i++)
                        {
                            dtResults.Rows[i]["Status"] = false;
                            string WClause = "AND JObPosting.CategoryID =" + dtResults.Rows[i]["CategoryID"];
                            DataTable dtResult1 = JobController.SelectJobPostingByWClause(WClause);
                            if (dtResult1.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtResult1.Rows.Count; j++)
                                {
                                    dtResult1.Rows[j]["Status"] = false;
                                }
                                JobController.UpdateJobPosting(dtResult1);
                            }
                        }
                        CategoryController.UpdateCategory(dtResults);
                        BindData();
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CategoryManager::LnkActive_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void gridCategory_Sorting(object sender, GridViewSortEventArgs e)
        {           
            gridCategory.EditIndex = -1;
            SortExpression = e.SortExpression;
            if (SortDirection == "ASC")
                SortDirection = "DESC";
            else if (SortDirection == "DESC")
                SortDirection = "ASC";
            BindData();
        }

        protected void imgGo_Click(object sender, EventArgs e)
        {
            try
            {
                SortDirection = "ASC";
                SortExpression = "Number";
                gridCategory.EditIndex = -1;
                BindData();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CategoryManager::imgGo_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                SortDirection = "ASC";
                SortExpression = "Number";
                gridCategory.EditIndex = -1;                
                txtDesc.Text = "";
                ddlUserStatus.SelectedValue = "None";
                BindData();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CategoryManager::btnReset_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void gridCategory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridCategory.EditIndex = -1;
            gridCategory.PageIndex = e.NewPageIndex;
            BindData();
        }

        protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                pageno = Convert.ToInt32(ddlPage.SelectedValue);
                if (ddlPage.SelectedItem.Value != "0")
                {
                    gridCategory.AllowPaging = true;
                    gridCategory.PageSize = int.Parse(ddlPage.SelectedItem.Value);
                    BindData();
                }
                else
                {
                    gridCategory.AllowPaging = false;
                    BindData();
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Category Manager::ddlPage_SelectedIndexChanged - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void ddlUserStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridCategory.EditIndex = -1;
            SortDirection = "ASC";
            SortExpression = "Number";
            BindData();
        }
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;

namespace Admin_v2.Job
{
    public partial class AddEditJobCategory : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["CatID"] != null)
                {
                    smltitle.InnerText = "[ Edit ]";
                    int CatID = Convert.ToInt32(Request.QueryString["CatID"].ToString());
                    string WClause = "AND CategoryID =" + CatID;                    
                    DataTable dtCat = CategoryController.SelectJobCategoryByWClause(WClause);
                    if (dtCat.Rows.Count > 0)
                    {
                        txtCategoryName.Text = dtCat.Rows[0]["CategoryName"].ToString();
                        chkStatus.Checked = Convert.ToBoolean(dtCat.Rows[0]["Status"].ToString());
                        lblCreated.InnerText = dtCat.Rows[0]["CreatedDate"].ToString();
                        lblCreatedBy.InnerText = dtCat.Rows[0]["CreatedBy1"].ToString();
                        lblModifiedBy.InnerText = dtCat.Rows[0]["ModifiedBy1"].ToString();
                        lblModified.InnerText = dtCat.Rows[0]["ModifiedDate"].ToString() == "" ? "Not Modified" : dtCat.Rows[0]["ModifiedDate"].ToString();
                        lblStatus.InnerText = dtCat.Rows[0]["Status"].ToString().ToLower() == "true" ? "Active" : "Inactive";
                        //if (dtCat.Rows[0]["Status"].ToString().ToLower() == "true")
                        //    ddlCategoryStatus.SelectedValue = "1";
                        //else
                        //    ddlCategoryStatus.SelectedValue = "0";
                    }
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorMsg.Text = "";
               if (ValidateControls())
                {
                    if (Request.QueryString["CatID"] != null)
                    {
                        string WClause = "AND CategoryID =" + Request.QueryString["CatID"];
                        DataTable dtCat = CategoryController.SelectJobCategoryByWClause(WClause);
                        if (dtCat.Rows.Count > 0)
                        {
                            dtCat.DefaultView[0].BeginEdit();
                            dtCat.DefaultView[0].Row["CategoryName"] = txtCategoryName.Text;
                            //if (ddlCategoryStatus.SelectedItem.ToString() == "Active")
                            //    dtCat.DefaultView[0].Row["Status"] = true;
                            //else
                            //    dtCat.DefaultView[0].Row["Status"] = false;
                            dtCat.DefaultView[0].Row["Status"] = chkStatus.Checked;
                            dtCat.DefaultView[0].Row["ModifiedDate"] = System.DateTime.Now;
                            DataTable dtsess = (DataTable)Session["dtLoginUser"];
                            dtCat.DefaultView[0].Row["ModifiedBy"] = dtsess.Rows[0]["UserID"].ToString();
                            CategoryController.UpdateCategory(dtCat);
                        }
                        Response.Redirect("CategoryManager.aspx", false);
                  }
                    else
                    {
                        string WClause = "AND CategoryID =" + 0;
                        DataTable dtCat = CategoryController.SelectJobCategoryByWClause(WClause);
                        DataRow drCat = dtCat.NewRow();
                        drCat["CategoryName"] = txtCategoryName.Text;
                        //if (ddlCategoryStatus.SelectedItem.ToString() == "Active")
                        //    drCat["Status"] = true;
                        //else
                        //    drCat["Status"] = false;
                        drCat["Status"] = chkStatus.Checked;
                        drCat["CreatedDate"] = System.DateTime.Now;
                        DataTable dtsess = (DataTable)Session["dtLoginUser"];
                        drCat["CreatedBy"] = dtsess.Rows[0]["UserID"].ToString();
                        dtCat.Rows.Add(drCat);
                        CategoryController.UpdateCategory(dtCat);
                        Response.Redirect("CategoryManager.aspx", false);
                    }
              }
            }
            catch(Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AddEditCategoryManager::btnSave_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        private bool ValidateControls()
        {
            bool flag = true;
            if(txtCategoryName.Text.Trim() == "")
            {
                ErrorMsg.Text = "Enter Category Name";
                flag = false;
            }
            else
            {
                if (txtCategoryName.Text.Trim() == "")
                {
                    ErrorMsg.Text = "Enter Category Name";
                    flag = false;
                }
            }
            return flag;
        }
    }
}

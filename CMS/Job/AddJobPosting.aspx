<%@ Page Language="C#" MasterPageFile="~/QPS.Master" EnableEventValidation="false"
    AutoEventWireup="true" ValidateRequest="false" CodeBehind="AddJobPosting.aspx.cs"
    Title="Job Posting Manager" Inherits="Admin_v2.Job.AddEditJobPostingManager" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="QPS" TagName="FCKEditor" Src="~/controls/FCKEditor.ascx" %>
<%--<%@ Register Assembly="Moxiecode.TinyMCE" Namespace="Moxiecode.TinyMCE.Web" TagPrefix="tinymce" %>--%>
<asp:Content ID="cphHome" runat="Server" ContentPlaceHolderID="ContentPlaceHolder">
    <style>
  
.MyCalendar .ajax__calendar_container {
    border:1px solid #646464;
    background-color: lemonchiffon;
    color: red;
    z-index:100;
    width:207px;
}
.MyCalendar .ajax__calendar_other .ajax__calendar_day,
.MyCalendar .ajax__calendar_other .ajax__calendar_year {
    color: black;
}
.MyCalendar .ajax__calendar_hover .ajax__calendar_day,
.MyCalendar .ajax__calendar_hover .ajax__calendar_month,
.MyCalendar .ajax__calendar_hover .ajax__calendar_year {
    color: black;
}
.MyCalendar .ajax__calendar_active .ajax__calendar_day,
.MyCalendar .ajax__calendar_active .ajax__calendar_month,
.MyCalendar .ajax__calendar_active .ajax__calendar_year {
    color: black;
    font-weight:bold;
}


.ajax__calendar_body
{
    width:235px;
}
    </style>
    <ajax:ScriptManager ID="SM1" runat="Server">
    </ajax:ScriptManager>
    <ajax:UpdateProgress ID="UPGR1" runat="Server" DisplayAfter="10">
        <ProgressTemplate>
            <div id="loading" style="position: absolute; top: 40%; left: 50%;">
                <img alt="Loading...." src="/images/ajax-loader.gif">
            </div>
        </ProgressTemplate>
    </ajax:UpdateProgress>
    <ajax:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div id="toolbar-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <div id="toolbar" class="toolbar">
                        <table class="toolbar">
                            <tbody>
                                <tr>
                                    <td id="toolbar-save" class="button">
                                        <%--<a id="btnSave" runat="server" class="toolbar" href="#" onserverclick="btnSave_Click">
                                            <span title="Save" class="icon-32-save"></span>Save </a>--%>
                                        <asp:LinkButton ID="btnSave" runat="server" CssClass="toolbar" OnClick="btnSave_Click"
                                            ValidationGroup="user">
                                        <span title="Save" class="icon-32-save"></span>Save</asp:LinkButton>
                                    </td>
                                    <td id="toolbar-cancel" class="button">
                                        <a id="btnClose" runat="server" class="toolbar" href="../Job/JobPostingManager.aspx">
                                            <span title="Cancel" class="icon-32-cancel"></span>Cancel </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="header icon-48-config">
                        Job Posting Manager: <small><small id="smltitle" runat="Server">[ New ]</small></small>
                    </div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div id="element-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <div class="fieldForm" id="fieldForm">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td valign="top">
                                    <table cellspacing="1" class="adminform">
                                        <tr>
                                            <td colspan="2">
                                                <span id="JobErrors" runat="server" style="color: Red; font-size: small;"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Category Name:</strong><span style="color: Red; font-size: small;">*</span>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCategory" DataTextField="CategoryName" DataValueField="CategoryID"
                                                    ToolTip="Select Category" runat="server" CssClass="inputbox" Width="220px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                                <strong>Display Order:</strong>
                                            </td>
                                            <td>
                                              <input type="text" id="txtorder" runat="server" size="50" class="text_area"/>
                                              <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtorder"
                                                    ErrorMessage="Only numeric characters are allowed" ValidationExpression="[^a-zA-Z]*"
                                                    ValidationGroup="user" SetFocusOnError="True" Font-Bold="true"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>
                                                <strong>Job Title:<strong><span style="color: Red; font-size: small;">*</span>
                                            </td>
                                            <td>
                                                <input type="text" id="txtJobTitle" runat="server" size="50" class="text_area" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter Text only"
                                                    ValidationExpression="^([a-zA-Z ]*)$" ControlToValidate="txtJobTitle" Display="Dynamic"
                                                    Font-Bold="true" ValidationGroup="user"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span style="font-size: 11px;">Experience From:</span>
                                            </td>
                                            <td>
                                                <input type="text" id="txtExperienceFrom" runat="server" size="50" class="text_area" />
                                                <asp:RegularExpressionValidator ID="revExperienceFrom" runat="server" ControlToValidate="txtExperienceFrom"
                                                    ErrorMessage="Only numeric characters are allowed" ValidationExpression="[^a-zA-Z]*"
                                                    ValidationGroup="user" Display="Dynamic" SetFocusOnError="True" Font-Bold="true"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Experience To:</strong><span style="color: Red; font-size: small;">*</span>
                                            </td>
                                            <td>
                                                <input type="text" id="txtExperienceTo" runat="server" size="50" class="text_area" />
                                                <asp:RegularExpressionValidator ID="revExperienceTo" runat="server" ControlToValidate="txtExperienceTo"
                                                    ErrorMessage="Only numeric characters are allowed" ValidationExpression="[^a-zA-Z]*"
                                                    ValidationGroup="user" SetFocusOnError="True" Font-Bold="true"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Posted Date:</strong>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPosted" runat="server"></asp:TextBox>&nbsp;<cc1:CalendarExtender
                                                    ID="calender" TargetControlID="txtPosted" Animated="true"
                                                    runat="server" PopupButtonID="img" CssClass="MyCalendar" Format="MMMM d, yyyy" >
                                                </cc1:CalendarExtender>
                                                <img src="../Images/images.jpg" id="img" width="15px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Location:</strong><span style="color: Red; font-size: small;">*</span>
                                            </td>
                                            <td>
                                                <input type="text" id="txtLocation" runat="server" size="50" class="text_area" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Duration:</strong><span style="color: Red; font-size: small;">*</span>
                                            </td>
                                            <td>
                                                <input type="text" id="txtDuration" runat="server" size="50" class="text_area" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Only alphanumeric characters are allowed"
                                                    ControlToValidate="txtDuration" ValidationExpression="[a-zA-Z0-9 ]+$" Font-Bold="true"
                                                    ValidationGroup="user" SetFocusOnError="true"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span style="font-size: 11px;">Salary:</span>
                                            </td>
                                            <td>
                                                <input type="text" id="txtSalary" runat="server" size="50" class="text_area" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <strong>Summary:</strong><span style="color: Red; font-size: small;">*</span>
                                            </td>
                                            <td>
                                                <textarea id="txtSummery" title="Summery" runat="server" cols="55" rows="6" style="width: 700px;"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <strong>Description:</strong><span style="color: Red; font-size: small;">*</span>
                                            </td>
                                            <td>
                                                <QPS:FCKEditor ID="FCKEditer1" runat="server" EnableViewState="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <strong>Status:</strong><span style="color: Red; font-size: small;">*</span>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlStatus" runat="server" ToolTip="Select Status" CssClass="inputbox">
                                                    <asp:ListItem Value="None">- Select Status -</asp:ListItem>
                                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                                    <asp:ListItem Value="0">Inactive</asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="user"
                                        ShowSummary="false" />
                                </td>
                                <td style="padding: 7px 0 0 5px" valign="top" width="360">
                                    <table style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;" width="100%">
                                        <tr>
                                            <td style="padding-left: 7px;">
                                                <strong>Status</strong>
                                            </td>
                                            <td>
                                                <label id="lblStatus" runat="server">
                                                    Active</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 7px;">
                                                <strong>Created By</strong>
                                            </td>
                                            <td>
                                                <label id="lblCreatedBy" runat="server">
                                                    Admin</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 7px;">
                                                <strong>Created Date</strong>
                                            </td>
                                            <td>
                                                <label id="lblCreated" runat="server">
                                                    Not Created</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 7px;">
                                                <strong>Modified By</strong>
                                            </td>
                                            <td>
                                                <label id="lblModifiedBy" runat="server">
                                                    Admin</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 7px;">
                                                <strong>Modified Date</strong>
                                            </td>
                                            <td>
                                                <label id="lblModified" runat="server">
                                                    Not Modified</label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <ajax:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </ajax:UpdatePanel>
</asp:Content>

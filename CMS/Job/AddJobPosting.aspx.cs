using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;

namespace Admin_v2.Job
{
    public partial class AddEditJobPostingManager : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCategory();
                if (Request.QueryString["JobID"] != null)
                {
                    smltitle.InnerText = "[ Edit ]";
                    int JobID = Convert.ToInt32(Request.QueryString["JobID"].ToString());
                    string WClause = "AND JobID=" + JobID;
                    DataTable dtJobPost = JobController.SelectJobPostingByWClause(WClause);
                    if (dtJobPost.Rows.Count > 0)
                    {
                        ddlCategory.SelectedValue = dtJobPost.Rows[0]["CategoryID"].ToString();
                        txtJobTitle.Value = dtJobPost.Rows[0]["JobTitle"].ToString();
                        txtorder.Value = dtJobPost.Rows[0]["DisplayOrderId"].ToString();
                        txtExperienceFrom.Value = dtJobPost.Rows[0]["ExperienceFrom"].ToString();
                        txtExperienceTo.Value = dtJobPost.Rows[0]["ExperienceTo"].ToString();
                        txtLocation.Value = dtJobPost.Rows[0]["Location"].ToString();
                        txtDuration.Value = dtJobPost.Rows[0]["Duration"].ToString();
                        txtSalary.Value = dtJobPost.Rows[0]["Salary"].ToString();
                        txtSummery.Value = dtJobPost.Rows[0]["Summary"].ToString();
                        FCKEditer1.FCKValue = dtJobPost.Rows[0]["Description"].ToString();
                        lblCreated.InnerText = dtJobPost.Rows[0]["PostedDate"].ToString();
                        lblCreatedBy.InnerText = dtJobPost.Rows[0]["PostedBy1"].ToString();
                        lblModifiedBy.InnerText = dtJobPost.Rows[0]["ModifiedBy1"].ToString();
                        lblModified.InnerText = dtJobPost.Rows[0]["ModifiedDate"].ToString() == "" ? "Not Modified" : dtJobPost.Rows[0]["ModifiedDate"].ToString();
                        lblStatus.InnerText = dtJobPost.Rows[0]["Status"].ToString().ToLower() == "true" ? "Active" : "Inactive";
                        txtPosted.Text =DateTime.Parse( dtJobPost.Rows[0]["PostedDate"].ToString()).ToString("MM/dd/yyyy");
                        if (dtJobPost.Rows[0]["Status"].ToString().ToLower() == "true")
                            ddlStatus.SelectedValue = "1";
                        else
                            ddlStatus.SelectedValue = "0";
                    }
                }
            }
        }

        private void BindCategory()
        {
            string WClasue = " AND Status= 1";  
            DataTable dtCat = CategoryController.SelectJobCategoryByWClause(WClasue);
            if (dtCat.Rows.Count > 0)
            {
                ddlCategory.DataSource = dtCat;
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("- Select Category -", "0"));
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateControls())
                {
                    if (Request.QueryString["JobID"] != null)
                    {
                        string WClause = "AND JobID =" + Request.QueryString["JobID"];
                        DataTable dtJob = JobController.SelectJobPostingByWClause(WClause);
                        if (dtJob.DefaultView.Count > 0)
                        {
                            dtJob.DefaultView[0].BeginEdit();
                            dtJob.DefaultView[0].Row["PostedDate"] = Convert.ToDateTime(txtPosted.Text.ToString());
                            dtJob.DefaultView[0].Row["DisplayOrderId"] = Convert.ToInt32(txtorder.Value.Trim());
                            dtJob.DefaultView[0].Row["CategoryID"] = Convert.ToInt32(ddlCategory.SelectedValue.ToString());
                            dtJob.DefaultView[0].Row["JobTitle"] = txtJobTitle.Value.Trim();
                            if (txtExperienceFrom.Value.Trim() == "")
                                dtJob.DefaultView[0].Row["ExperienceFrom"] = DBNull.Value;
                            else
                                dtJob.DefaultView[0].Row["ExperienceFrom"] = Convert.ToInt32(txtExperienceFrom.Value.Trim());
                            dtJob.DefaultView[0].Row["ExperienceTo"] = Convert.ToInt32(txtExperienceTo.Value.Trim());
                            dtJob.DefaultView[0].Row["Location"] = txtLocation.Value.Trim();
                            dtJob.DefaultView[0].Row["Duration"] = txtDuration.Value.Trim();
                            if (txtSalary.Value.Trim() == "")
                                dtJob.DefaultView[0].Row["Salary"] = DBNull.Value;
                            else
                                dtJob.DefaultView[0].Row["Salary"] = txtSalary.Value.ToString();
                            dtJob.DefaultView[0].Row["Summary"] = txtSummery.Value.Trim();
                            dtJob.DefaultView[0].Row["Description"] = FCKEditer1.FCKValue.Trim();
                            if (ddlStatus.SelectedValue == "1")
                                dtJob.DefaultView[0].Row["Status"] = true;
                            else
                                dtJob.DefaultView[0].Row["Status"] = false;
                            dtJob.DefaultView[0].Row["ModifiedDate"] = Convert.ToDateTime(System.DateTime.Now.ToString());
                            DataTable dtsess = (DataTable)Session["dtLoginUser"];
                            dtJob.DefaultView[0].Row["ModifiedBy"] = dtsess.Rows[0]["UserID"].ToString();
                            dtJob.DefaultView[0].EndEdit();
                            JobController.UpdateJobPosting(dtJob);
                            Response.Redirect("JobPOstingManager.aspx", false);
                        }
                    }
                    else
                    {
                        string WClause = "AND JobID =" + 0;
                        DataTable dtJob = JobController.SelectJobPostingByWClause(WClause);
                        DataRow drJob = dtJob.NewRow();
                        drJob["PostedDate"] = Convert.ToDateTime(txtPosted.Text.ToString());
                        drJob["CategoryID"] = Convert.ToInt32(ddlCategory.SelectedValue.ToString());
                        drJob["JobTitle"] = txtJobTitle.Value.Trim();
                        drJob["DisplayOrderId"] = Convert.ToInt32(txtorder.Value.Trim());
                        DataTable dtsess = (DataTable)Session["dtLoginUser"];
                        if (txtExperienceFrom.Value.Trim() == "")
                            drJob["ExperienceFrom"] = DBNull.Value;
                        else
                            drJob["ExperienceFrom"] = Convert.ToInt32(txtExperienceFrom.Value);
                        drJob["ExperienceTo"] = Convert.ToInt32(txtExperienceTo.Value);
                        drJob["Location"] = txtLocation.Value.Trim();
                        drJob["Duration"] = txtDuration.Value.Trim();
                        if (txtSalary.Value.Trim() == "")
                            drJob["Salary"] = DBNull.Value;
                        else
                            drJob["Salary"] = txtSalary.Value.ToString();
                        drJob["Summary"] = txtSummery.Value.ToString();
                        drJob["Description"] = FCKEditer1.FCKValue.Trim();
                        if (ddlStatus.SelectedValue == "1")
                            drJob["Status"] = true;
                        else
                            drJob["Status"] = false;
                        drJob["PostedDate"] = Convert.ToDateTime(System.DateTime.Now.ToString());
                        drJob["PostedBy"] = dtsess.Rows[0]["UserID"].ToString();
                        dtJob.Rows.Add(drJob);
                        JobController.UpdateJobPosting(dtJob);
                        Response.Redirect("JobPOstingManager.aspx", false);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Response.Redirect("/ErrorPages/SiteError.aspx", false);                
            }
        }

        private bool ValidateControls()
        {
            bool flag = true;
            if (ddlCategory.SelectedIndex == 0 || txtJobTitle.Value == "" || txtExperienceTo.Value == "" || txtLocation.Value == "" || txtDuration.Value == "" || txtSummery.Value == "" || FCKEditer1.FCKValue == "" || ddlStatus.SelectedValue == "None")
            {
                JobErrors.InnerText = "* Please enter all required fields.";
                flag = false;
            }
            else
            {

            }
            return flag;
        }
    }
}

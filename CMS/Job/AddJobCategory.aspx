<%@ Page Language="C#" MasterPageFile="~/QPS.Master" AutoEventWireup="true" Codebehind="AddJobCategory.aspx.cs"
    Title="Job Category Manager" Inherits="Admin_v2.Job.AddEditJobCategory" %>

<asp:Content ID="cphHome" runat="Server" ContentPlaceHolderID="ContentPlaceHolder">
    
   <style type="text/css">
        .fieldForm div .fieldItemValue {padding-left:10px;}
        .fieldForm .fieldItemLabel {width:9em;float:left;height:auto;padding-top:1px;text-align:right;}
        .fieldForm .fieldItemLabel label {font-family:verdana;font-size:10px;align:right;}



        .fieldForm .field100Pct, 
        .fieldForm .field50Pct {min-width: 50%;}

        .fieldForm .field100Pct .fieldItemValue, 
        .fieldForm .field50Pct .fieldItemValue {display:inline;}

        .fieldForm .field100Pct .fieldItemValue input {width:80%;min-width:200px;}

        .fieldForm .field50Pct .fieldItemValue input {width:100%;min-width:100px;}

        .fieldForm .field50Pct {float:left;width:90%;}

        .fieldForm div div input {margin-bottom:10px;margin-right:15px;}

        .fieldForm {background-color: #f9f9f9;
        border: solid 1px #d5d5d5;
        width: 100%;
        border-collapse: collapse;
        margin: 8px 0 10px 0;
        margin-bottom: 15px;        
        font-size:9;
        font-family:verdana;}

        .clear {clear:both;}	

        .style3 {font-size: 11px;font-weight:bold;color:#666666;}			
	</style>
	
    <ajax:ScriptManager ID="SM1" runat="Server">
    </ajax:ScriptManager>
    <ajax:UpdateProgress ID="UPGR1" runat="Server" DisplayAfter="10">
        <ProgressTemplate>
            <div id="loading" style="position: absolute; top: 40%; left: 50%;">
                <img alt="Loading...." src="/images/ajax-loader.gif">
            </div>
        </ProgressTemplate>
    </ajax:UpdateProgress>
    <ajax:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div id="toolbar-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <div class="toolbar" id="toolbar">
                        <table class="toolbar">
                            <tr>
                                <td id="toolbar-save" class="button">
                                    <asp:LinkButton ID="btnSave" runat="Server" OnClick="btnSave_Click"><span class="icon-32-save" title="Save"></span>Save</asp:LinkButton>
                                </td>
                                <td id="toolbar-cancel" class="button">
                                     <a href="CategoryManager.aspx" class="toolbar"><span class="icon-32-cancel" title="Cancel">
                                    </span>Cancel </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="header icon-48-addedit">
                        Job Category Manager: <small><small id="smltitle" runat="Server">[ New ]</small></small>
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div id="element-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                       <td valign="top">
                   <div class="fieldForm" id="fieldForm">
                        <table width="100%">
                            <tr>
                                <td colspan="2">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width: 8%;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td width="15%" style="padding-left:5px;">
                                    <strong>
                                        Category Name:<span style="color: #ff0000">*</span></strong>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCategoryName" runat="server" TabIndex="3" Width="160px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Category Name" ControlToValidate="txtCategoryName" Display="Dynamic" Font-Bold="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Enter Text only" ControlToValidate="txtCategoryName" ValidationExpression="^([a-zA-Z ]*)$" Display="Dynamic" Font-Bold="true"></asp:RegularExpressionValidator>
                                    <asp:Label ID="ErrorMsg" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                             <tr>
                                <td></td>
                             </tr>
                            <tr>
                                <td width="15%" style="padding-left:5px;">
                                       Status:
                                </td>            
                                 <td>
                                    <asp:CheckBox ID="chkStatus" runat="server" Checked="True" TabIndex="1" Text="Active" />
                                 </td>      
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                         </table> 
                       <asp:ValidationSummary ID="ValidationSummary2" runat="server"  ShowSummary="false" ValidationGroup="user"/>
                     </div>
                    </td>
                       
                       <td style="padding: 7px 0 0 5px" valign="top" width="360">
                        <table style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;" width="100%">
                            <tr>
                                <td style="padding-left:7px;">
                                    <strong>Status</strong>
                                </td>
                                <td>
                                    <label id="lblStatus" runat="server">Active</label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="padding-left:7px;">
                                    <strong>Created By</strong>
                                </td>
                                <td>
                                    <label id="lblCreatedBy" runat="server">Admin</label>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left:7px;">
                                    <strong>Created Date</strong>
                                </td>
                                <td>                                    
                                    <label id="lblCreated" runat="server">Not Created</label>                                    
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left:7px;">
                                    <strong>Modified By</strong>
                                </td>
                                <td>                                    
                                    <label id="lblModifiedBy" runat="server">Admin</label>                                    
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left:7px;">
                                    <strong>Modified Date</strong>
                                </td>
                                <td>                                    
                                    <label id="lblModified" runat="server">Not Modified</label>                                    
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                     </tr>
                   </table> 
                   
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </ajax:UpdatePanel>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/QPS.Master" AutoEventWireup="true" Codebehind="JobPostingManager.aspx.cs"
    Title="Job Posting Manager" Inherits="Admin_v2.Job.JobPostingManager" %>

<asp:Content ID="cphHome" runat="Server" ContentPlaceHolderID="ContentPlaceHolder">

    <script type="text/javascript">
    
        function OptionList()
        {
            var w='';
            document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value='';
            var c=document.getElementsByTagName("*");
            for(i=0;i < c.length; i++)
            {
               if(c[i].type == 'checkbox' && c[i].title == 'Status')
               {
                if(c[i].checked)
                  {
                     if(c[i].value!='on')
                     w +=c[i].value + ',';
                  }
               }
            }
           document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value=w;           
        }    
        function AllList()
      {            
            var c=document.getElementsByTagName("*");
            for(i=0;i < c.length; i++)
            {
               if(c[i].type == 'checkbox' && c[i].title == 'Status')
               {
                    if(document.getElementById('chkAll').checked)
                    {
                        c[i].checked = true;
                    }
                    else
                    {
                        c[i].checked = false;
                    }
               }
            }
            OptionList();      
      }
       function Confirmations()
      {
        if(document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value != 0)
        {
            return true;
        }
        else
        {
            alert("No records selected!");
            return false;
        }
      }    
    </script>

    <ajax:ScriptManager ID="SM1" runat="Server">
    </ajax:ScriptManager>
    <ajax:UpdateProgress ID="UPGR1" runat="Server" DisplayAfter="10">
        <ProgressTemplate>
            <div id="loading" style="position: absolute; top: 40%; left: 50%;">
                <img alt="Loading...." src="/images/ajax-loader.gif">
            </div>
        </ProgressTemplate>
    </ajax:UpdateProgress>
    <ajax:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div id="toolbar-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <div class="toolbar" id="toolbar">
                        <table class="toolbar">
                            <tr>
                                <td class="button" id="toolbar-new">
                                    <a href="AddJobPosting.aspx" class="toolbar"><span class="icon-32-new"
                                        title="New"></span>New </a>
                                </td>
                                <td class="button" id="toolbar-unarchive">
                                    <a id="LnkActive" runat="server" href="#" onclick="OptionList();return Confirmations()"
                                        class="toolbar" onserverclick="LnkActive_Click"><span class="icon-32-unarchive" title="Active">
                                        </span>Active </a>
                                </td>
                                <td class="button" id="toolbar-archive">
                                    <a id="LnkInactive" runat="server" href="#" onclick="OptionList();return Confirmations()"
                                        class="toolbar" onserverclick="LnkInactive_Click"><span class="icon-32-archive" title="Inactive">
                                        </span>Inactive </a>
                                </td>
                                <td class="button" id="toolbar-Cancel">
                                    <a href="../Default.aspx" class="toolbar"><span class="icon-32-cancel" title="Cancel">
                                    </span>Cancel </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="header icon-48-categories">
                        &nbsp;Job Posting Manager</div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
            <div id="element-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <table>
                        <tr>
                            <td width="100%">
                                Filter:
                                <asp:TextBox ID="txtDesc" runat="server" ToolTip="Filter" MaxLength="255" size="20"></asp:TextBox>&nbsp;
                                <button id="btnGo" runat="server" onserverclick="imgGo_Click" title="Go">
                                    Go</button>&nbsp;
                                <button id="btnReset" runat="server" title="Reset" onserverclick="btnReset_Click">
                                    Reset</button>
                            </td>
                            <td nowrap="nowrap">
                                <span style="font-size: 11px;">Select Category:</span>
                                <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true" DataTextField="CategoryName"
                                    DataValueField="CategoryID" ToolTip="Select Category" CssClass="inputbox" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                </asp:DropDownList>
                                &nbsp;&nbsp; <span style="font-size: 11px;">Select Job Status:</span>
                                <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" ToolTip="Select Status"
                                    CssClass="inputbox" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                    <asp:ListItem Value="None">- Select Status -</asp:ListItem>
                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                    <asp:ListItem Value="0">Inactive</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <div style="color: red">
                        <span id="ErrorMsg" style="font-weight: bold;" runat="server"></span>
                    </div>
                    <asp:GridView ID="gvJobPosting" runat="server" AutoGenerateColumns="False" Width="100%"
                        ForeColor="#666666" CellPadding="1" BorderColor="#E7E7E7" BorderWidth="1px" BorderStyle="Solid"
                        AllowPaging="True" PageSize="10" AllowSorting="true" OnSorting="gvJobPosting_Sorting"
                        OnPageIndexChanging="gvJobPosting_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="#" SortExpression="Number" ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "Number")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="3%">
                                <HeaderTemplate>
                                    <input id="chkAll" name="all" onclick="AllList()" type="checkbox" value="all" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td align="center">
                                                <input id="SelectedMessages" runat="server" name='list' title="Status" type="checkbox"
                                                    value='<%#DataBinder.Eval(Container.DataItem,"JobID") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Job Title" SortExpression="JobTitle" ItemStyle-Width="15%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <span class="editlinktip hasTip" title="Click Here to Edit"><a href="AddJobPosting.aspx?JobID=<%#DataBinder.Eval(Container.DataItem, "JobID")%>">
                                                    <%#DataBinder.Eval(Container.DataItem, "JobTitle")%>
                                                </a></span>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category" SortExpression="CategoryName" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "CategoryName")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Exp. From" SortExpression="ExperienceFrom" ItemStyle-Width="5%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "ExperienceFrom")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Exp. To" SortExpression="ExperienceTo" ItemStyle-Width="5%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "ExperienceTo")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Summary" SortExpression="Summary" ItemStyle-Width="54%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem, "Summary")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="PostedDate" SortExpression="PostedDate">
                        <ItemTemplate>
                            <table>
                                <tr style="background-color: White">
                                    <td style="padding-left: 5px">
                                        <%#DataBinder.Eval(Container.DataItem, "PostedDate")%>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Status" SortExpression="Status" ItemStyle-Width="5%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td align="center">
                                                <span class="editlinktip hasTip">
                                                    <img alt="Published" border="0" height="16" src="../TemplateImages/publish_g.png"
                                                        style="display: <%#DataBinder.Eval(Container.DataItem, "Status").ToString().ToLower() == "true" ? "block" : "none" %>"
                                                        width="16" />
                                                </span><span class="editlinktip hasTip">
                                                    <img alt="Not_Published" border="0" height="16" src="../TemplateImages/publish_x.png"
                                                        style="display: <%#DataBinder.Eval(Container.DataItem, "Status").ToString().ToLower() == "true" ? "none" : "block" %>"
                                                        width="16" />
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="#F0F0F0" Font-Bold="True" ForeColor="#666666" />
                        <PagerStyle CssClass="page" Font-Size="12px" HorizontalAlign="Center" />
                    </asp:GridView>
                    <tfoot id="PageSize" runat="server" style="background: #F0F0F0 none repeat scroll 0 50%;
                        text-align: center" visible="false">
                        <tr align="center" valign="middle">
                            <td colspan="10">
                                <del class="container">
                                    <div class="pagination" style="background-color: #F0F0F0; text-align: center">
                                        <div class="limit">
                                            Display #
                                            <asp:DropDownList ID="ddlPage" runat="server" AutoPostBack="true" CssClass="inputbox"
                                                OnSelectedIndexChanged="ddlPage_SelectedIndexChanged">
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                <asp:ListItem Value="50">50</asp:ListItem>
                                                <asp:ListItem Value="100">100</asp:ListItem>
                                                <asp:ListItem Value="0">all</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <input name="limitstart" type="hidden" value="0" />
                                    </div>
                                </del>
                            </td>
                        </tr>
                    </tfoot>
                    <div class="clr">
                    </div>
                    <input type="hidden" id="hdnSelectedMessages" runat="server" />
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
        </ContentTemplate>
    </ajax:UpdatePanel>
</asp:Content>

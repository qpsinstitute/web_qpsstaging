using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Lib.BusinessLogic;

namespace Admin_v2.Job
{
    public partial class JobPostingManager : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected static string SortDirection = "DESC";
        protected static string SortExpression = "CreatedDate";
        protected static int pageno;
        protected static int roleID;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindCategory();
                    SortDirection = "ASC";
                    SortExpression = "Number";
                    pageno = 10;
                    BindRepeater();
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Job Posting Manager:: Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        private void BindCategory()
        {
            DataTable dtCat = CategoryController.GetAllCategory();
            if (dtCat.Rows.Count > 0)
            {
                ddlCategory.DataSource = dtCat;
                ddlCategory.DataBind();                
            }
            ddlCategory.Items.Insert(0, new ListItem("- Select Category -", "None"));
        }

        private void BindRepeater()
        {
            try
            {
                string WClause = "";
                if (txtDesc.Text.Trim() != "")
                {
                    WClause += "AND (JobPosting.JobTitle like '%" + txtDesc.Text.Trim() + "%'" + "OR JobPosting.Location like '%" + txtDesc.Text.Trim() + "%'" + "OR JobPosting.Summary like '%" + txtDesc.Text.Trim() + "%')";
                }
                if (ddlStatus.SelectedItem.Value != "None")
                {
                    WClause += "AND JobPosting.Status=" + ddlStatus.SelectedItem.Value;
                }
                if (ddlCategory.SelectedItem.Value != "None")
                {
                    WClause += "AND JobPosting.CategoryID=" + ddlCategory.SelectedValue;
                }
                DataTable dtsess = (DataTable)Session["dtLoginUser"];
                roleID = Convert.ToInt32(dtsess.Rows[0]["RoleID"].ToString());
                if (roleID == 1)
                {
                    WClause += " ";
                }
                else
                {
                    WClause += "AND (u.UserID = " + Convert.ToInt32(dtsess.Rows[0]["UserID"].ToString()) + ")";
                    //WClause += "AND (u.RoleID = " + Convert.ToInt32(dtsess.Rows[0]["RoleID"].ToString()) + ")";
                }
                    DataTable dtResult = JobController.SelectJobPostingByWClause(WClause);
                    if (dtResult.Rows.Count > 0)
                    {
                        ErrorMsg.InnerText = "";
                        dtResult.DefaultView.Sort = SortExpression + " " + SortDirection;
                        gvJobPosting.DataSource = dtResult.DefaultView;
                        gvJobPosting.DataBind();
                        PageSize.Visible = true;
                    }
                    else
                    {
                        if (ErrorMsg.InnerText != "No Result found")
                        {
                            gvJobPosting.DataSource = dtResult;
                            gvJobPosting.DataBind();
                        }
                        ErrorMsg.InnerText = "No Result found";
                        PageSize.Visible = false;
                    }
                
            }
            catch(Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Job Posting Manager :: BindRepeater - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                SortDirection = "ASC";
                SortExpression = "Number";
                gvJobPosting.EditIndex = -1;
                txtDesc.Text = "";
                ddlStatus.SelectedValue = "None";
                ddlCategory.SelectedValue = "None";
                BindRepeater();                
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] JobPostingManager::btnReset_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void imgGo_Click(object sender, EventArgs e)
        {
            try
            {
                SortDirection = "ASC";
                SortExpression = "Number";
                gvJobPosting.EditIndex = -1;
                BindRepeater();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] JobPostingManager::imgGo_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void gvJobPosting_Sorting(object sender, GridViewSortEventArgs e)
        {
            gvJobPosting.EditIndex = -1;
            SortExpression = e.SortExpression;
            if (SortDirection == "ASC")
                SortDirection = "DESC";
            else if (SortDirection == "DESC")
                SortDirection = "ASC";
            BindRepeater();
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvJobPosting.EditIndex = -1;
            SortDirection = "ASC";
            SortExpression = "Number";
            BindRepeater();
        }


        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvJobPosting.EditIndex = -1;
            SortDirection = "ASC";
            SortExpression = "Number";
            BindRepeater();
        }
        protected void LnkActive_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnSelectedMessages.Value.Trim() != "")
                {
                    string WhereClause = " AND JobID in (" + hdnSelectedMessages.Value.Substring(0, hdnSelectedMessages.Value.Length - 1) + ")";
                    DataTable dtResults = JobController.SelectJobPostingByWClause(WhereClause);
                    if (dtResults.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtResults.Rows.Count; i++)
                        {
                            dtResults.Rows[i]["Status"] = true;
                        }
                        JobController.UpdateJobPosting(dtResults);
                        BindRepeater();
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "]Job Manager::LnkActive_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void LnkInactive_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnSelectedMessages.Value.Trim() != "")
                {
                    string WhereClause = " AND JobID in (" + hdnSelectedMessages.Value.Substring(0, hdnSelectedMessages.Value.Length - 1) + ")";
                    DataTable dtResults = JobController.SelectJobPostingByWClause(WhereClause);
                    if (dtResults.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtResults.Rows.Count; i++)
                        {
                            dtResults.Rows[i]["Status"] = false;
                        }
                        JobController.UpdateJobPosting(dtResults);
                        BindRepeater();
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "]Job Manager::LnkInactive_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void gvJobPosting_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvJobPosting.EditIndex = -1;
            gvJobPosting.PageIndex = e.NewPageIndex;
            BindRepeater();
        }

        protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                pageno = Convert.ToInt32(ddlPage.SelectedValue);
                if (ddlPage.SelectedItem.Value != "0")
                {
                    gvJobPosting.AllowPaging = true;
                    gvJobPosting.PageSize = int.Parse(ddlPage.SelectedItem.Value);
                    BindRepeater();
                }
                else
                {
                    gvJobPosting.AllowPaging = false;
                    BindRepeater();
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] JobPosting Manager::ddlPage_SelectedIndexChanged - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
    }
}

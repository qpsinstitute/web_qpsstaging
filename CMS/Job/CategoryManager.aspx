<%@ Page Language="C#" MasterPageFile="~/QPS.Master" AutoEventWireup="true" Codebehind="CategoryManager.aspx.cs"
    Title="Job Category Manager" Inherits="Admin_v2.Job.CategoryManager" %>

<asp:Content ID="cphHome" runat="Server" ContentPlaceHolderID="ContentPlaceHolder">

    <script type="text/javascript">
    
        function OptionList()
        {
            var w='';           
                       
            document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value='';
            var c=document.getElementsByTagName("*");
            for(i=0;i < c.length; i++)
            {
               if(c[i].type == 'checkbox' && c[i].title == 'Status')
               {
                if(c[i].checked)
                  {
                     if(c[i].value!='on')
                     w +=c[i].value + ',';
                  }
               }
            }
           document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value=w;           
        }    
        function AllList()
      {            
            var c=document.getElementsByTagName("*");
            for(i=0;i < c.length; i++)
            {
               if(c[i].type == 'checkbox' && c[i].title == 'Status')
               {
                    if(document.getElementById('chkAll').checked)
                    {
                        c[i].checked = true;
                    }
                    else
                    {
                        c[i].checked = false;
                    }
               }
            }
            OptionList();      
      }
       function Confirmations()
      {
        if(document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value != 0)
        {
            return true;
        }
        else
        {
            alert("No records selected!");
            return false;
        }
      }    
    </script>

    <ajax:ScriptManager ID="SM1" runat="Server">
    </ajax:ScriptManager>
    <ajax:UpdateProgress ID="UPGR1" runat="Server" DisplayAfter="10">
        <ProgressTemplate>
            <div id="loading" style="position: absolute; top: 40%; left: 50%;">
                <img alt="Loading...." src="/images/ajax-loader.gif">
            </div>
        </ProgressTemplate>
    </ajax:UpdateProgress>
    <ajax:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div id="toolbar-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <div class="toolbar" id="toolbar">
                        <table class="toolbar">
                            <tr>
                                <td class="button" id="toolbar-new">
                                    <a href="AddJobCategory.aspx" class="toolbar" onclick="NewItem();">
                                        <span class="icon-32-new" title="New"></span>New </a>
                                </td>
                                <td class="button" id="toolbar-unarchive">
                                    <a id="LnkActive" runat="server" href="#" onclick="OptionList();return Confirmations()"
                                        onserverclick="LnkActive_Click" class="toolbar"><span class="icon-32-unarchive" title="Active">
                                        </span>Active </a>
                                </td>
                                <td class="button" id="toolbar-archive">
                                    <a id="LnkInactive" runat="server" href="#" onclick="OptionList();return Confirmations()"
                                        onserverclick="LnkInactive_Click" class="toolbar"><span class="icon-32-archive" title="Inactive">
                                        </span>Inactive </a>
                                </td>
                                <td class="button" id="toolbar-Cancel">
                                    <a href="../Default.aspx" class="toolbar"><span class="icon-32-cancel" title="Cancel">
                                    </span>Cancel </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="header icon-48-addedit">
                        &nbsp;Job Category Manager</div>
                    <div class="clr">
                    </div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div id="element-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <table>
                        <tr>
                            <td width="100%">
                                Filter:
                                <asp:TextBox ID="txtDesc" runat="server" MaxLength="255" size="20"></asp:TextBox>&nbsp;
                                <button id="btnGo" runat="server" onserverclick="imgGo_Click">
                                    Go</button>&nbsp;
                                <button id="btnReset" runat="server" onserverclick="btnReset_Click">
                                    Reset</button>
                            </td>
                            <td nowrap="nowrap">
                                <span style="font-size: 11px;">Select Status:</span>
                                <asp:DropDownList ID="ddlUserStatus" runat="server" AutoPostBack="true" ToolTip="Select Status"
                                    CssClass="inputbox" OnSelectedIndexChanged="ddlUserStatus_SelectedIndexChanged">
                                    <asp:ListItem Value="None">- Select Status -</asp:ListItem>
                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                    <asp:ListItem Value="0">Inactive</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <div style="color: red">
                        <span id="ErrorMsg" style="font-weight: bold;" runat="server"></span>
                    </div>
                    <asp:GridView ID="gridCategory" runat="server" AutoGenerateColumns="false" BorderColor="#E7E7E7"
                        PagerStyle-HorizontalAlign="Center" HeaderStyle-BackColor="#F0F0F0" BorderStyle="Solid"
                        BorderWidth="1px" CellPadding="5" ForeColor="#666666" Width="100%" AllowPaging="true"
                        DataKeyNames="CategoryId" AllowSorting="true" OnSorting="gridCategory_Sorting"
                        OnPageIndexChanging="gridCategory_PageIndexChanging">
                        <Columns>
                            <asp:BoundField DataField="Number" HeaderText="#" ItemStyle-HorizontalAlign="center"
                                SortExpression="Number" ReadOnly="true" ItemStyle-Width="5%" />
                            <asp:TemplateField ItemStyle-Width="5%">
                                <HeaderTemplate>
                                    <input id="chkAll" name="all" onclick="AllList()" type="checkbox" value="all" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td align="center">
                                                <input id="SelectedMessages" runat="server" name='list' title="Status" type="checkbox"
                                                    value='<%#DataBinder.Eval(Container.DataItem,"CategoryID") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category Name" SortExpression="CategoryName" ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "CategoryName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Created By" SortExpression="CreatedBy" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "CreatedBy1")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Created Date" SortExpression="CreatedDate" ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "CreatedDate"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" SortExpression="Status" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td align="center">
                                                <span class="editlinktip hasTip">
                                                    <img alt="Published" border="0" height="16" src="../TemplateImages/publish_g.png"
                                                        style="display: <%#DataBinder.Eval(Container.DataItem, "Status").ToString().ToLower() == "true" ? "block" : "none" %>"
                                                        width="16" />
                                                </span><span class="editlinktip hasTip">
                                                    <img alt="Not_Published" border="0" height="16" src="../TemplateImages/publish_x.png"
                                                        style="display: <%#DataBinder.Eval(Container.DataItem, "Status").ToString().ToLower() == "true" ? "none" : "block" %>"
                                                        width="16" />
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <span class="editlinktip hasTip" title="Click Here to Edit"><a href="AddJobCategory.aspx?CatID=<%#DataBinder.Eval(Container.DataItem, "CategoryID")%>">
                                        Edit </a></span>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <tfoot id="PageSize" runat="server" style="background: #F0F0F0 none repeat scroll 0 50%;
                        text-align: center" visible="false">
                        <tr align="center" valign="middle">
                            <td colspan="10">
                                <del class="container">
                                    <div class="pagination" style="background-color: #F0F0F0; text-align: center">
                                        <div class="limit">
                                            Display #
                                            <asp:DropDownList ID="ddlPage" runat="server" AutoPostBack="true" CssClass="inputbox"
                                                OnSelectedIndexChanged="ddlPage_SelectedIndexChanged">
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                <asp:ListItem Value="50">50</asp:ListItem>
                                                <asp:ListItem Value="100">100</asp:ListItem>
                                                <asp:ListItem Value="0">all</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <input name="limitstart" type="hidden" value="0" />
                                    </div>
                                </del>
                            </td>
                        </tr>
                    </tfoot>
                    <div class="clr">
                    </div>
                    <input type="hidden" id="hdnSelectedMessages" runat="server" />
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </ajax:UpdatePanel>
</asp:Content>

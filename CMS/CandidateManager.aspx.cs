using System;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using System.Data.SqlClient;

namespace Admin_v2.Job
{
    public partial class Candidate_Info : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected static string SortDirection = "DESC";
        protected static string SortExpression = "CreatedDate";
        protected static string WClause1 = "";
        protected static int pageno;
       // SqlConnection cn = new SqlConnection(ConfigurationSettings.AppSettings["SQLConnectionString"]);
        SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["SQLConnectionString"]);
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    SortDirection = "ASC";
                    SortExpression = "Number";
                    pageno = 10;
                    BindRepeater();
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CandidateManager::Page_Load -  ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        private void BindRepeater()
        {
            try
            {
                string WClause = "";
                if (txtDesc.Text.Trim() != "")
                {
                    WClause += "AND (Candidate.FirstName+' '+Candidate.LastName like '%" + txtDesc.Text.Trim() + "%' OR Candidate.City like '%" + txtDesc.Text.Trim() + "%' OR Candidate.State like '%" + txtDesc.Text.Trim() + "%' OR Candidate.JobSkill like '%" + txtDesc.Text.Trim() + "%' OR Candidate.Phone like '%" + txtDesc.Text.Trim() + "%')";
                }
                if (txtSearch.Text.Trim() != "")
                {
                    WClause = WClause1;
                }
                DataTable dtResult = CandidateCotroller.GetCandidateByWClause(WClause);
                if (dtResult.Rows.Count > 0)
                {
                    ErrorMsg.InnerText = "";
                    dtResult.DefaultView.Sort = SortExpression + "  " + SortDirection;
                    gvCandidate.DataSource = dtResult.DefaultView;
                    gvCandidate.DataBind();
                    PageSize.Visible = true;
                }
                else
                {
                    if (ErrorMsg.InnerText != "No Result found")
                    {
                        gvCandidate.DataSource = dtResult;
                        gvCandidate.DataBind();
                    }
                    ErrorMsg.InnerText = "No Result found";
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CandidateManager::BindRepeater -  ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void gvCandidate_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortExpression = e.SortExpression;
            if (SortDirection == "ASC")
                SortDirection = "DESC";
            else if (SortDirection == "DESC")
                SortDirection = "ASC";
            BindRepeater();
        }

        protected void imgGo_Click(object sender, EventArgs e)
        {
            try
            {
                SortDirection = "ASC";
                SortExpression = "Number";
                gvCandidate.EditIndex = -1;
                txtSearch.Text = "";
                BindRepeater();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CandidateManager::imgGo_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                SortDirection = "ASC";
                SortExpression = "Number";
                gvCandidate.EditIndex = -1;
                txtDesc.Text = "";
                txtSearch.Text = "";
                BindRepeater();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CandidateManager::btnSearch_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                pageno = Convert.ToInt32(ddlPage.SelectedValue);
                if (ddlPage.SelectedItem.Value != "0")
                {
                    gvCandidate.AllowPaging = true;
                    gvCandidate.PageSize = int.Parse(ddlPage.SelectedItem.Value);
                    BindRepeater();
                }
                else
                {
                    gvCandidate.AllowPaging = false;
                    BindRepeater();
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CandidateManager::ddlPage_SelectedIndexChanged - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void a1_Click(string path)
        {
            Response.TransmitFile(path);
        }

        protected void gvCandidate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCandidate.EditIndex = -1;
            gvCandidate.PageIndex = e.NewPageIndex;
            BindRepeater();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataTable dtTemp = new DataTable();
            try
            {
                DataTable dtResume = CandidateCotroller.GetAllCanidate();
                WClause1 = "AND CandidateID in (";
                bool flag = false;
                foreach (DataRow drResume in dtResume.Rows)
                {
                    if (drResume["Resume"].ToString() != "")
                    {
                        string path = Server.MapPath("Resume").ToString() + "/" + drResume["ResumeGUID"];
                        if (File.Exists(path))
                        {
                            StreamReader rd = new StreamReader(path);
                            string line;
                            while ((line = rd.ReadLine()) != null)
                            {
                                string str = line.Trim().ToLower();
                                if (str.Contains(txtSearch.Text.ToLower()))
                                {
                                    WClause1 += Convert.ToInt32(drResume["CandidateID"]) + ",";
                                    flag = true;
                                    break;
                                }
                            }
                            rd.Close();
                        }

                    }
                    if (drResume["CoverLetter"].ToString() != "")
                    {
                        string path = Server.MapPath("CoverLetter") + "/" + drResume["CoverLetterGUID"];

                        if (File.Exists(path))
                        {
                            StreamReader rd = new StreamReader(path);
                            string line;
                            while ((line = rd.ReadLine()) != null)
                            {
                                string str = line.Trim().ToLower();
                                if (str.Contains(txtSearch.Text.ToLower()))
                                {
                                    WClause1 += Convert.ToInt32(drResume["CandidateID"]) + ",";
                                    flag = true;
                                    break;
                                }
                            }
                            rd.Close();
                        }
                    }
                }
                if (flag)
                {
                    WClause1 = WClause1.Substring(0, WClause1.Length - 1) + ")";
                    BindRepeater();
                }
                else
                {
                    WClause1 += " 0 )";
                    BindRepeater();
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CandidateManager::btnSearch_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
                string temp = hdnSelectedMessages.Value.Substring(0, hdnSelectedMessages.Value.Length - 1);
                string[] ids = temp.Split(',');
                int cnt = 0;
                if (ids.Length != 0)
                {
                    gvCandidate.EditIndex= -1;
                    while (cnt != ids.Length)
                    {
                      try
                         {
                             String Qry = "DELETE FROM Candidate WHERE CandidateID=" + Convert.ToInt32(ids[cnt]);
                             SqlCommand cm = new SqlCommand(Qry, cn);
                             cn.Open();
                             cm.ExecuteNonQuery();
                             cn.Close();
                         }
                         catch (Exception ex)
                         {
                            if (log.IsErrorEnabled)
                                 log.Error("[" + System.DateTime.Now.ToString("G") + "]Candidate Manager::Delete_Click - ", ex);
                            Response.Redirect("/errorpages/SiteError.aspx", false);
                         }
                         cnt++;
                    }
                 }
                 BindRepeater();
        }
    }
}

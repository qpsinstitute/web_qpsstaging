<%@ Page Language="C#" MasterPageFile="~/QPS.Master" AutoEventWireup="true" Codebehind="CandidateManager.aspx.cs"
    Title="Candidate Manager" Inherits="Admin_v2.Job.Candidate_Info" %>

<asp:Content ID="cphHome" runat="Server" ContentPlaceHolderID="ContentPlaceHolder">

    <script type="text/javascript">
    
        function GetPath(path)
        {
            Response.Redirect(path);
        }
       
        function OptionList()
        {
            var w='';
            document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value='';
            var c=document.getElementsByTagName("*");
            for(i=0;i < c.length; i++)
            {
               if(c[i].type == 'checkbox' && c[i].title == 'Status')
               {
                if(c[i].checked)
                  {
                     if(c[i].value!='on')
                     w +=c[i].value + ',';
                  }
               }
            }
           document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value=w;           
        }    
        function AllList()
      {            
            var c=document.getElementsByTagName("*");
            for(i=0;i < c.length; i++)
            {
               if(c[i].type == 'checkbox' && c[i].title == 'Status')
               {
                    if(document.getElementById('chkAll').checked)
                    {
                        c[i].checked = true;
                    }
                    else
                    {
                        c[i].checked = false;
                    }
               }
            }
            OptionList();      
      }
       function Confirmations()
      {
        if(document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value != 0)
        {
            return true;
        }
        else
        {
            alert("There is no records selected!");
            return false;
        }
      }    
      
      
     
      
    </script>

    <ajax:ScriptManager ID="SM1" runat="Server">
    </ajax:ScriptManager>
    <ajax:UpdateProgress ID="UPGR1" runat="Server" DisplayAfter="10">
        <ProgressTemplate>
            <div id="loading" style="position: absolute; top: 40%; left: 50%;">
                <img alt="Loading...." src="/images/ajax-loader.gif">
            </div>
        </ProgressTemplate>
    </ajax:UpdateProgress>
    <ajax:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div id="toolbar-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <div class="toolbar" id="toolbar">
                        <table class="toolbar">
                            <tr>
                               <%-- <td id="toolbar-new" class="button">
                                   <a class="toolbar" ><span class="icon-32-new" title="New"></span>
                                New</a>
                                </td>--%>
                                <td class="button" id="toolbar-Delete" style="height: 48px">
                                    
                                    <a id="LnkDelete" runat="server" href="#" onclick="OptionList();return Confirmations()"
                                        class="toolbar" onserverclick="Delete_Click"><span class="icon-32-cancel" title="Delete">
                                            <br />
                                        </span>Delete </a>
                                      
                                </td>
                                <td class="button" id="toolbar-Cancel" style="height: 48px">
                                    <a href="Default.aspx" class="toolbar"><span class="icon-32-cancel" title="Cancel"></span>
                                        Cancel </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="header icon-48-user">
                        &nbsp;Candidate Manager</div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div id="element-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <table width="100%">
                        <tr>
                            <td align="left">
                                Filter:
                                <asp:TextBox ID="txtDesc" runat="server" ToolTip="Filter" MaxLength="255" size="20"></asp:TextBox>&nbsp;
                                <button id="btnGo" runat="server" onserverclick="imgGo_Click" title="Go">
                                    Go</button>&nbsp;
                                <button id="btnReset" runat="server" title="Reset" onserverclick="btnReset_Click">
                                    Reset</button>
                            </td>
                            <td align="right">
                                Resume & Cover Letter Search:
                                <asp:TextBox ID="txtSearch" runat="server" ToolTip="Filter" MaxLength="255" size="20"></asp:TextBox>&nbsp;
                                <button id="btnSearch" runat="server" onserverclick="btnSearch_Click" title="Search">
                                    Search</button>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <div style="color: red">
                        <span id="ErrorMsg" style="font-weight: bold;" runat="server"></span>
                    </div>
                    <asp:GridView ID="gvCandidate" runat="server" AutoGenerateColumns="false" Width="100%"
                        ForeColor="#666666" CellPadding="5" BorderColor="#E7E7E7" PagerStyle-HorizontalAlign="Center"
                        HeaderStyle-BackColor="#F0F0F0" BorderWidth="1px" BorderStyle="Solid" AllowPaging="True"
                        PageSize="10" AllowSorting="true" OnSorting="gvCandidate_Sorting" OnPageIndexChanging="gvCandidate_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="#" ItemStyle-VerticalAlign="top" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="3%" SortExpression="Number">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"Number") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                       <asp:TemplateField ItemStyle-Width="3%">
                                <HeaderTemplate>
                                    <input id="chkAll" name="all" onclick="AllList();" type="checkbox" value="all" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td align="center">
                                                <input id="lnkDelete" runat="server" name='list'  title="Status" type="checkbox"
                                                    value='<%#DataBinder.Eval(Container.DataItem,"CandidateID") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        
                            
                            <asp:TemplateField HeaderText="First Name" ItemStyle-VerticalAlign="top" SortExpression="FirstName"
                                ItemStyle-Width="12%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"FirstName") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name" ItemStyle-VerticalAlign="top" SortExpression="LastName"
                                ItemStyle-Width="12%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"LastName") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City" ItemStyle-VerticalAlign="top" SortExpression="City"
                                ItemStyle-Width="7%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"City") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apply for" ItemStyle-VerticalAlign="top" SortExpression="JobSkill"
                                ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"JobSkill") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email" ItemStyle-VerticalAlign="top" ItemStyle-Width="13%"
                                SortExpression="Email">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"Email") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Resume" ItemStyle-VerticalAlign="top" SortExpression="Resume"
                                ItemStyle-Width="13%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <span class="editlinktip hasTip" title="Click here to Get Resume"><a href="/Resume/<%#DataBinder.Eval(Container.DataItem,"ResumeGUID") %>"
                                                    target="_blank">
                                                    <%#DataBinder.Eval(Container.DataItem,"Resume") %>
                                                </a></span>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cover Letter" ItemStyle-VerticalAlign="top" SortExpression="CoverLetter"
                                ItemStyle-Width="13%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <span class="editlinktip hasTip" title="Click here to Get Cover Letter"><a href="/CoverLetter/<%#DataBinder.Eval(Container.DataItem,"CoverLetterGUID") %>"
                                                    target="_blank">
                                                    <%#DataBinder.Eval(Container.DataItem, "CoverLetter")%>
                                                </a></span>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Created Date" ItemStyle-VerticalAlign="top" ItemStyle-Width="12%"
                                SortExpression="CreatedDate">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"CreatedDate") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderText="View" ItemStyle-VerticalAlign="top" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="5%">
                                <ItemTemplate>
                                    <span class="editlinktip hasTip" title="Click Here to View"><a href="AddCandidate.aspx?CandidateId=<%#DataBinder.Eval(Container.DataItem, "CandidateID")%>">
                                        View </a></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <input type="hidden" id="hdnApprover" runat="server" />
                    <tfoot id="PageSize" runat="server" style="background: #F0F0F0 none repeat scroll 0 50%;
                        text-align: center" visible="false">
                        <tr align="center" valign="middle">
                            <td colspan="10">
                                <del class="container">
                                    <div class="pagination" style="background-color: #F0F0F0; text-align: center">
                                        <div class="limit">
                                            Display #
                                            <asp:DropDownList ID="ddlPage" runat="server" AutoPostBack="true" CssClass="inputbox"
                                                OnSelectedIndexChanged="ddlPage_SelectedIndexChanged">
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                <asp:ListItem Value="50">50</asp:ListItem>
                                                <asp:ListItem Value="100">100</asp:ListItem>
                                                <asp:ListItem Value="0">all</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <input name="limitstart" type="hidden" value="0" />
                                    </div>
                                </del>
                            </td>
                        </tr>
                    </tfoot>
                    <div class="clr">
                    </div>
                    <input type="hidden" id="hdnSelectedMessages" runat="server" />
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
        </ContentTemplate>
    </ajax:UpdatePanel>
</asp:Content>

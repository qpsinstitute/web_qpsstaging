<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="Admin_v2.ForgotPassword"  %>
<%--<%@ Register Src ="~/Controls/Message.ascx" TagName ="Message" TagPrefix="DocMgmt" %>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html dir="ltr" lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>QPS Administration </title>
    
    <script src="js/mootools.js" type="text/javascript"></script>
    <link href="css/system.css" rel="stylesheet" type="text/css" />
    <link href="css/rounded.css" rel="stylesheet" type="text/css" />
    <link href="css/login.css" rel="stylesheet" type="text/css" />

</head>
<body >
    <div id="border-top" class="h_green">
        <div>
            <div>
                <span class="title">QPS Administration </span>
            </div>
        </div>
    </div>
    <div id="content-box">
        <div class="padding">
            <div id="element-box" class="login">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <h1>
                        QPS Administration Forgot Password</h1>
                    <div id="section-box">
                        <div class="t">
                            <div class="t">
                                <div class="t">
                                </div>
                            </div>
                        </div>
                        <div class="m">
                            <form id="LoginForm" runat="server" style="clear: both;">
                                <table width="100%">
                                    <tr>
                                        <td colspan="2" style="padding-left: 10px;">
                                            <span id="ErrorMsg" runat="server" style="color: Red; font-weight: bold;"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding-left: 10px;">
                                            <span id="ErrorMsg1" runat="server" style="color: Red; font-weight: bold;"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding-left:50px;">
                                            <asp:RegularExpressionValidator ID="RegularExpressionEmail" runat="server" ControlToValidate="txtUserID"
                                                Display="Dynamic" ErrorMessage="Invalid Username\Email Address" Font-Bold="true"
                                                SetFocusOnError="true" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr>
                                        <td width="50px" valign="top">
                                           <asp:Label ID="lblEmail" runat="server" Font-Bold="true" ForeColor="#666666" Text="Email Address"
                                               Width="90px"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtUserID" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserID" Display="Dynamic"
                                                ErrorMessage="*"></asp:RequiredFieldValidator>
                                            
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2" style="padding-left:115px">
                                           <div class="button1">
                                            <div class="next" >
                                                <a id="lnkFPassword" runat="server" href="#"  onserverclick="lnkFPassword_ServerClick" >Get Password</a>
                                            </div>
                                           </div>
                                        </td>
                                    </tr>
                                </table>
                               <br />
                               <div class="clr">
                               </div>
                            </form>
                            <div class="clr">
                            </div>
                        </div>
                        <div class="b">
                            <div class="b">
                                <div class="b">
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>
                        Use a valid username and password to gain access to the Administrator Back-end.</p>
                    <div id="lock" style="vertical-align: text-top">
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <noscript>
                Warning! JavaScript must be enabled for proper operation of the Administrator back-end.
            </noscript>
            <div class="clr">
            </div>
        </div>
    </div>
    <div id="border-bottom">
        <div>
            <div>
            </div>
        </div>
    </div>
    <div id="footer">
        <p class="copyright">
            � <%= System.DateTime.Now.Year %> Perpetuating
        </p>
    </div>
</body>
</html>

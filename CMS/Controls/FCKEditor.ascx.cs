using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Admin_v2.controls
{
    public partial class FCKEditor : System.Web.UI.UserControl
    {
        private string BASE_PATH = ConfigurationManager.AppSettings["FCKEditorBasePath"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            FCKContent.BasePath = BASE_PATH;
        }

        public string FCKValue
        {
            get { return FCKContent.Value; }

            set { FCKContent.Value = value; }
        }
    }
}
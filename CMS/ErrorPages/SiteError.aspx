<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SiteError.aspx.cs" Inherits="Admin_v2.ErrorPages.SiteError" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html dir="ltr" lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>QPS Administration - SiteError</title>    
    <script src="../js/mootools.js" type="text/javascript"></script>
    <link href="../css/system.css" rel="stylesheet" type="text/css" />
    <link href="../css/rounded.css" rel="stylesheet" type="text/css" />
    <link href="../css/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="border-top" class="h_green">
        <div>
            <div>
                <span class="title">QPS Administration</span>
            </div>
        </div>
    </div>
    <div id="content-box">
        <div class="padding">
            <div id="element-box" style="padding:0px 25px 0px 25px;">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <table class="adminform" width="100%">
                        <tr>                
                        <td valign="top" style="width:100%" >
                            <center>
                            <div id="cpanel">
                            

                            A site error has occurred. We apologize for any inconvenience. Errors are automatically
                            logged for review by the system administrator.
                            <br />
                            If there is anything we can help you with, please <a href="mailto:info@qpsinc.com">
                                contact us</a>.
                            </div>
                            </center>
                        </td>                       
                        </tr>
                    </table>                            
                    <div class="clr">
                    </div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
        </div>
    </div>
    <div id="border-bottom">
        <div>
            <div>
            </div>
        </div>
    </div>
    <div id="footer">
        <p class="copyright">
            � <%= System.DateTime.Now.Year %> Perpetuating
        </p>
    </div>
</body>
</html>
               


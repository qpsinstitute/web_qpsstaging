using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;

namespace Admin_v2.User
{
    public partial class AddAdminUser : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static int UserID = 0;
        public static DataTable dtSession = null;
        public DataTable dtRole = null, dtExist = null;
        public string strExist = "";
        protected string ServerQualifiedPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindRole();
                    txtFirstName.Focus();

                    if (Request.QueryString["UserID"] != null)
                    {
                        if (Request.QueryString["Edit"] != null)
                            smltitle.InnerText = "[ Account Settings ]";
                        else smltitle.InnerText = "Edit";
                        trpass.Visible = false;
                        trpassconf.Visible = false;
                        UserID = Convert.ToInt32(Request.QueryString["UserID"].ToString());
                        string WClause = "AND u.UserID=" + UserID;
                        
                        DataTable dtResult = UserController.GetAllUsersByWhereClause(WClause);
                        if (dtResult.Rows.Count > 0)
                        {
                            txtFirstName.Text = dtResult.Rows[0]["FirstName"].ToString();
                            txtLastName.Text = dtResult.Rows[0]["LastName"].ToString();
                            txtEmail.Text = dtResult.Rows[0]["Email"].ToString();
                            txtPasswordConfirm.Text = dtResult.Rows[0]["Password"].ToString();
                            if (dtResult.Rows[0]["RoleID"] != null)
                                ddlRole.SelectedValue = dtResult.Rows[0]["RoleID"].ToString();
                            txtCompany.Text = dtResult.Rows[0]["Company"].ToString();
                            txtPhone.Text = dtResult.Rows[0]["Phone"].ToString();
                            txtfax.Text = dtResult.Rows[0]["Fax"].ToString();
                            txtposition.Text = dtResult.Rows[0]["Position"].ToString();
                            chkStatus.Checked = Convert.ToBoolean(dtResult.Rows[0]["Status"].ToString());
                            lblCreated.InnerText = dtResult.Rows[0]["CreatedDate"].ToString();
                            lblCreatedBy.InnerText = dtResult.Rows[0]["CreatedBy1"].ToString() == "" ? "Admin" : dtResult.Rows[0]["CreatedBy1"].ToString();
                            lblModifiedBy.InnerText = dtResult.Rows[0]["ModifiedBy1"].ToString();
                            lblModified.InnerText = dtResult.Rows[0]["ModifiedDate"].ToString() == "" ? "Not Modified" : dtResult.Rows[0]["ModifiedDate"].ToString();
                            lblStatus.InnerText = dtResult.Rows[0]["Status"].ToString().ToLower() == "true" ? "Active" : "Inactive";
                        }
                    }
                    else
                    {
                        trpassedit.Style.Add("display", "none");
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AddAdminUser::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        private void BindRole()
        {
            DataTable dtRole = UserController.GetAllRole();
            if (dtRole.Rows.Count > 0)
            {
                ddlRole.DataTextField = "RoleName";
                ddlRole.DataValueField = "RoleID";
                ddlRole.DataSource = dtRole;
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("- Select Role -", "0"));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                if (Request.QueryString["UserID"] != null)
                {
                    string wClause = "And u.UserID =" + Request.QueryString["UserID"];
                    DataTable dtResult = UserController.GetAllUsersByWhereClause(wClause);
                    if (dtResult.DefaultView.Count > 0)
                    {
                        dtResult.DefaultView[0].BeginEdit();
                        dtResult.DefaultView[0].Row["FirstName"] = txtFirstName.Text.ToString().Trim();
                        dtResult.DefaultView[0].Row["LastName"] = txtLastName.Text.ToString().Trim();
                        dtResult.DefaultView[0].Row["Email"] = txtEmail.Text.ToString().Trim();
                        if (txtPasswordEdit.Text.Trim().ToString() != string.Empty)
                            dtResult.DefaultView[0].Row["Password"] = txtPasswordEdit.Text.Trim();
                        dtResult.DefaultView[0].Row["RoleID"] = Convert.ToInt32(ddlRole.SelectedValue.ToString());
                        dtResult.DefaultView[0].Row["Company"] = txtCompany.Text.ToString().Trim();
                        dtResult.DefaultView[0].Row["Phone"] = txtPhone.Text.ToString().Trim();
                        dtResult.DefaultView[0].Row["Fax"] = txtfax.Text.ToString().Trim();
                        dtResult.DefaultView[0].Row["Position"] = txtposition.Text.ToString().Trim();
                        dtResult.DefaultView[0].Row["Status"] = chkStatus.Checked;
                        DataTable dtUser = (DataTable)Session["dtLoginUser"];
                        dtResult.DefaultView[0].Row["ModifiedBy"] = Convert.ToInt32(dtUser.Rows[0]["UserID"].ToString());
                        dtResult.DefaultView[0].Row["ModifiedDate"] = Convert.ToDateTime(System.DateTime.Now);
                        
                        if (PhotoUpload.FileName != "")
                        {
                             string Extention = System.IO.Path.GetExtension(PhotoUpload.PostedFile.FileName);
                             if (Extention != ".jpg" && Extention != ".GIF" && Extention != ".PNG")
                                 lblErrorMsg.Text = "Please upload jpg / PNG / GIF file only.";
                             else
                             {
                                 dtResult.DefaultView[0].Row["PhotoPath"] = UploadPhoto(PhotoUpload);
                                 dtResult.DefaultView[0].EndEdit();
                                 UserController.UpdateUsers(dtResult);
                                 Response.Redirect("AdminUserManager.aspx", false);
                             }
                        }
                        else
                        {
                            dtResult.DefaultView[0].Row["PhotoPath"] = DBNull.Value;
                            dtResult.DefaultView[0].EndEdit();
                            UserController.UpdateUsers(dtResult);
                            Response.Redirect("AdminUserManager.aspx", false);
                        }
                    }
                }
                else
                {
                    string wClause = "And u.UserID =" + 0;
                    DataTable dtResult = UserController.GetAllUsersByWhereClause(wClause);
                    DataRow druser = dtResult.NewRow();
                    druser["FirstName"] = txtFirstName.Text.ToString().Trim();
                    druser["LastName"] = txtLastName.Text.ToString().Trim();
                    druser["Email"] = txtEmail.Text.ToString().Trim();
                    druser["Password"] = txtPassword.Text.ToString().Trim();
                    druser["RoleID"] = ddlRole.SelectedValue.ToString();
                    druser["Company"] = txtCompany.Text.ToString().Trim();
                    druser["Phone"] = txtPhone.Text.ToString().Trim();
                    druser["Fax"] = txtfax.Text.ToString().Trim();
                    druser["Position"] = txtposition.Text.ToString().Trim();
                    druser["CreatedDate"] = Convert.ToDateTime(System.DateTime.Now);
                    DataTable dtUser = (DataTable)Session["dtLoginUser"];
                    druser["CreatedBy"] = Convert.ToInt32(dtUser.Rows[0]["UserID"].ToString());
                    druser["Status"] = chkStatus.Checked;
                    if (PhotoUpload.FileName != "")
                    {
                        string Extention = System.IO.Path.GetExtension(PhotoUpload.PostedFile.FileName);
                        if (Extention != ".jpg" && Extention != ".GIF" && Extention != ".PNG")
                            lblErrorMsg.Text = "Please upload jpg / PNG / GIF file only.";
                        else
                        {
                            druser["PhotoPath"] = UploadPhoto(PhotoUpload);
                            dtResult.Rows.Add(druser);
                            UserController.UpdateUsers(dtResult);
                            Response.Redirect("AdminUserManager.aspx", false);
                        }
                    }
                    else
                    {
                        druser["PhotoPath"] = DBNull.Value;
                        dtResult.Rows.Add(druser);
                        UserController.UpdateUsers(dtResult);
                        Response.Redirect("AdminUserManager.aspx", false);
                    }
                  }
                }
            }
        private string UploadPhoto(FileUpload avatar_file)
        {
            string guidFilename = System.Guid.NewGuid().ToString();
            guidFilename += avatar_file.FileName.Substring(avatar_file.FileName.LastIndexOf(@"."));
            ServerQualifiedPath = ConfigurationManager.AppSettings["UserImagePathAbsolute"] + guidFilename;
            avatar_file.PostedFile.SaveAs(ServerQualifiedPath);
            return guidFilename;
        }
        public bool ValidateControls()
        {
            bool flag = true;
            if (Request.QueryString["UserID"] != null)
            {
                //if (txtPasswordEdit.Text.ToString() != "")
                //{
                //    lblMsg.Text = "Password must be minimum of 6 characters<br/>";
                //    flag = false;
                //}
                strExist = "And u.Email = '" + txtEmail.Text + "' and u.UserID<>" + Request.QueryString["UserID"].ToString();
                dtExist = UserController.GetAllUsersByWhereClause(strExist);
                if (dtExist.Rows.Count > 0)
                {
                    lblMsg.Text = "This username is already registered, enter another username.<br/>";
                    flag = false;
                }
                if (txtEmail.Text.Trim().ToString() != string.Empty)
                {
                    strExist = "And u.Email = '" + txtEmail.Text + "' and u.UserID<>" + Request.QueryString["UserID"].ToString();
                    dtExist = UserController.GetAllUsersByWhereClause(strExist);
                    if (dtExist.Rows.Count > 0)
                    {
                        lblMsg.Text = "This email address is already registered, enter another email address.<br/>";
                        flag = false;
                    }
                }
            }
            else
            {
                if (txtPassword.Text.Trim().Length < 6)
                {
                    lblMsg.Text = "Password must be minimum of 6 characters<br/>";
                    flag = false;
                }
                strExist = "And u.Email = '" + txtEmail.Text + "'";
                dtExist = UserController.GetAllUsersByWhereClause(strExist);
                if (dtExist.Rows.Count > 0)
                {
                    lblMsg.Text = "This username has already been taken, choose another username.<br/>";
                    flag = false;
                }

                if (txtEmail.Text.Trim().ToString() != string.Empty)
                {
                    strExist = "And u.Email = '" + txtEmail.Text + "'";
                    dtExist = UserController.GetAllUsersByWhereClause(strExist);
                    if (dtExist.Rows.Count > 0)
                    {
                        lblMsg.Text = "This email address is already registered, enter another email address.<br/>";
                        flag = false;
                    }
                }
            }
            if (flag == true)
                return true;
            else return false;
        }
    }
}

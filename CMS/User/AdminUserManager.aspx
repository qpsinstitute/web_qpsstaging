<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminUserManager.aspx.cs" Inherits="Admin_v2.User.AdminUserManager" MasterPageFile="~/QPS.Master"%>

<asp:Content ID="cphAdminManager" runat="Server" ContentPlaceHolderID="ContentPlaceHolder">

<script language="javascript" type="text/javascript">
       function OptionList()
       {
            var w='';
            document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value='';
            var c=document.getElementsByTagName("*");
            for(i=0;i < c.length; i++)
            {
               if(c[i].type == 'checkbox' && c[i].title == 'Status')
               {
                if(c[i].checked)
                  {
                     if(c[i].value!='on')
                     w +=c[i].value + ',';
                  }
               }
            }
           document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value=w;
      }
      function AllList()
      {            
            var c=document.getElementsByTagName("*");
            for(i=0;i < c.length; i++)
            {
               if(c[i].type == 'checkbox' && c[i].title == 'Status')
               {
                    if(document.getElementById('chkAll').checked)
                    {
                        c[i].checked = true;
                    }
                    else
                    {
                        c[i].checked = false;
                    }
               }
            }
            OptionList();      
      }
     function Confirmations()
      {
        if(document.getElementById('ctl00_ContentPlaceHolder_hdnSelectedMessages').value != 0)
        {
            return true;
        }
        else
        {
            alert("No records selected!");
            return false;
        }
      }    
 </script>
    
<ajax:ScriptManager ID="SM1" runat="Server">
</ajax:ScriptManager>

<ajax:UpdateProgress ID="UPGR1" runat="Server" DisplayAfter="10">
        <ProgressTemplate>
            <div id="loading" style="position: absolute; top: 40%; left: 50%;">
                <img alt="Loading...." src="/images/ajax-loader.gif">
            </div>
        </ProgressTemplate>
</ajax:UpdateProgress>
        
<ajax:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div id="toolbar-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <div class="toolbar" id="toolbar">
                        <table class="toolbar">
                            <tr>
                              <td id="toolbar-new" class="button">
                                    <a class="toolbar" href="AddAdminUser.aspx"><span class="icon-32-new" title="Add a new admin user"></span>New </a>
                              </td>
                               
                              <td class="button" id="toolbar-unarchive">
                                    <a id="LnkActive" runat="server" href="#" onclick="OptionList();return Confirmations()"
                                        class="toolbar" onserverclick="LnkActive_Click"><span class="icon-32-unarchive" title="Active">
                                        </span>Active </a>
                                </td>
                                <td class="button" id="toolbar-archive">
                                    <a id="LnkInactive" runat="server" href="#" onclick="OptionList();return Confirmations()"
                                        class="toolbar" onserverclick="LnkInactive_Click"><span class="icon-32-archive" title="Inactive">
                                        </span>Inactive </a>
                                </td>
                                
                                <td class="button" id="toolbar-Cancel" style="height: 48px">
                                    <a href="../Default.aspx" class="toolbar"><span class="icon-32-cancel" title="Cancel"></span>
                                        Cancel </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="header icon-48-user">
                        &nbsp;Admin/Employer Manager</div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div id="element-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                   <table width="100%">
                        <tr>
                            <td align="left">
                                 <div>
                                    Admin/Employer Name:
                                    <asp:TextBox ID="txtAdminName" runat="server" MaxLength="50"></asp:TextBox>
                                    &nbsp;Company Name:
                                    <asp:TextBox ID="txtCompanyName" runat="server" MaxLength="50"></asp:TextBox>
                                    &nbsp;
                                    <button id="btnGo" runat="server" onserverclick="imgGo_Click">
                                        Go</button>
                                    <button id="btnReset" runat="server" onserverclick="btnReset_Click">
                                        Reset</button>
                                </div>
                            </td>
                            <td align="right">
                                <asp:DropDownList ID="ddlRole" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged">
                                    <asp:ListItem Value="None">- Select Role -</asp:ListItem>
                                </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddlStatus" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                    <asp:ListItem Value="None">- Select Status -</asp:ListItem>
                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                    <asp:ListItem Value="0">Inactive</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <div style="color: red">
                        <span id="ErrorMsg" style="font-weight: bold;" runat="server"></span>
                    </div>
                    <asp:GridView ID="grdAdminUser" runat="server" AutoGenerateColumns="false" Width="100%"
                        ForeColor="#666666" CellPadding="5" BorderColor="#E7E7E7" PagerStyle-HorizontalAlign="Center"
                        HeaderStyle-BackColor="#F0F0F0" BorderWidth="1px" BorderStyle="Solid" AllowPaging="True"
                        PageSize="10" AllowSorting="true"  OnDataBound="grdAdminUser_DataBound" OnPageIndexChanging="grdAdminUser_PageIndexChanging" OnSorting="grdAdminUser_Sorting" >
                        <Columns>
                            <asp:TemplateField HeaderText="#" ItemStyle-VerticalAlign="top" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="3%" SortExpression="Number">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"Number") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                       <asp:TemplateField ItemStyle-Width="3%">
                                <HeaderTemplate>
                                    <input id="chkAll" name="all" onclick="AllList();" type="checkbox" value="all" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td align="center">
                                                <input id="SelectedMessages" runat="server" name='list'  title="Status" type="checkbox"
                                                    value='<%#DataBinder.Eval(Container.DataItem,"UserID") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="First Name" ItemStyle-VerticalAlign="top" SortExpression="FirstName"
                                ItemStyle-Width="7%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"FirstName") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name" ItemStyle-VerticalAlign="top" SortExpression="LastName"
                                ItemStyle-Width="7%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"LastName") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Admin/Employer Name" ItemStyle-VerticalAlign="top" SortExpression="FirstName"
                                ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                              <a href="AddAdminUser.aspx?UserID=<%#DataBinder.Eval(Container.DataItem, "UserID")%>"> <%#DataBinder.Eval(Container.DataItem,"FirstName") %>  <%#DataBinder.Eval(Container.DataItem,"LastName") %></a>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                             <asp:TemplateField HeaderText="Role" ItemStyle-VerticalAlign="top" SortExpression="RoleName"
                                ItemStyle-Width="7%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"RoleName") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                             <asp:TemplateField HeaderText="Username/Email" ItemStyle-VerticalAlign="top" SortExpression="Email"
                                ItemStyle-Width="12%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <a href="mailto:<%#DataBinder.Eval(Container.DataItem, "Email")%>">
                                                <%#DataBinder.Eval(Container.DataItem,"Email") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Company" ItemStyle-VerticalAlign="top" SortExpression="Company"
                                ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"Company") %> </a>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="top" SortExpression="Status"
                                ItemStyle-Width="5%">
                                <ItemTemplate>
                                     <table align="center">
                                        <tr style="background-color: White">
                                            <td align="center">
                                                <span class="editlinktip hasTip">
                                                    <img alt="Published" border="0" height="16" src="../TemplateImages/publish_g.png"
                                                        style="display: <%#DataBinder.Eval(Container.DataItem, "Status").ToString().ToLower() == "true" ? "block" : "none" %>"
                                                        width="16" />
                                                </span><span class="editlinktip hasTip">
                                                    <img alt="Not_Published" border="0" height="16" src="../TemplateImages/publish_x.png"
                                                        style="display: <%#DataBinder.Eval(Container.DataItem, "Status").ToString().ToLower() == "true" ? "none" : "block" %>"
                                                        width="16" />
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Created Date" ItemStyle-VerticalAlign="top" SortExpression="CreatedDate"
                                ItemStyle-Width="12%">
                                <ItemTemplate>
                                    <table>
                                        <tr style="background-color: White">
                                            <td style="padding-left: 5px">
                                                <%#DataBinder.Eval(Container.DataItem,"CreatedDate") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                           
                        </Columns>
                    </asp:GridView>
                    <tfoot style="background: #F0F0F0 none repeat scroll 0 50%; text-align: center">
                        <tr align="center" valign="middle">
                            <td colspan="10">
                                <del class="container">
                                    <div id="divpagination" runat="Server" class="pagination" style="background-color: #F0F0F0; text-align: center">
                                        <div class="limit">
                                            Display #
                                            <asp:DropDownList ID="ddlPage" runat="server" AutoPostBack="true" CssClass="inputbox" OnSelectedIndexChanged="ddlPage_SelectedIndexChanged">
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                <asp:ListItem Value="50">50</asp:ListItem>
                                                <asp:ListItem Value="100">100</asp:ListItem>
                                                <asp:ListItem Value="0">all</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <input name="limitstart" type="hidden" value="0" />
                                    </div>
                                </del>
                            </td>
                        </tr>
                    </tfoot>
                    <div class="clr">
                    </div>
                    <input type="hidden" id="hdnSelectedMessages" runat="server" />
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
        </ContentTemplate>
    </ajax:UpdatePanel>
    
</asp:Content> 
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;

namespace Admin_v2.User
{
    public partial class MyProfile : System.Web.UI.Page
    {
        public static int UserID = 0;
        public DataTable dtRole = null, dtExist = null;
        public string strExist = "", Extention="" ;
        protected string ServerQualifiedPath = "";
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dtUser = (DataTable)Session["dtLoginUser"];
                UserID = Convert.ToInt32(dtUser.Rows[0]["UserID"].ToString());
                Session["UserID"] = Convert.ToString(UserID);
                if(!IsPostBack)
                {
                   // BindRole(); 
                    txtFirstName.Focus();
                    string wClause = "And u.UserID =" + UserID;
                    DataTable dtResult = UserController.GetAllUsersByWhereClause(wClause);
                    if (dtResult.Rows.Count > 0)
                    {
                        txtFirstName.Text = dtResult.Rows[0]["FirstName"].ToString();
                        txtLastName.Text = dtResult.Rows[0]["LastName"].ToString();
                        txtEmail.Text = dtResult.Rows[0]["Email"].ToString();
                        txtCompany.Text = dtResult.Rows[0]["Company"].ToString();
                        txtPhone.Text = dtResult.Rows[0]["Phone"].ToString();
                        txtfax.Text = dtResult.Rows[0]["Fax"].ToString();
                        txtposition.Text = dtResult.Rows[0]["Position"].ToString();
                        txtPasswordEdit.Text = string.Empty;
                        lblCreated.InnerText = dtResult.Rows[0]["CreatedDate"].ToString();
                        lblCreatedBy.InnerText = dtResult.Rows[0]["CreatedBy1"].ToString() == "" ? "Admin" : dtResult.Rows[0]["CreatedBy1"].ToString();
                        lblModifiedBy.InnerText = dtResult.Rows[0]["ModifiedBy1"].ToString();
                        lblModified.InnerText = dtResult.Rows[0]["ModifiedDate"].ToString() == "" ? "Not Modified" : dtResult.Rows[0]["ModifiedDate"].ToString();
                        lblStatus.InnerText = dtResult.Rows[0]["Status"].ToString().ToLower() == "true" ? "Active" : "Inactive";
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AddAdminUser::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                try
                {
                    if (Session["UserID"] != null)
                    {
                        string wClause = "And u.UserID =" + Convert.ToInt32(Session["UserID"]);
                        DataTable dtResult = UserController.GetAllUsersByWhereClause(wClause);
                        if (dtResult.DefaultView.Count > 0)
                        {
                            dtResult.DefaultView[0].BeginEdit();
                            dtResult.DefaultView[0].Row["FirstName"] = txtFirstName.Text.ToString().Trim();
                            dtResult.DefaultView[0].Row["LastName"] = txtLastName.Text.ToString().Trim();
                            dtResult.DefaultView[0].Row["Email"] = txtEmail.Text.ToString().Trim();
                            if (txtPasswordEdit.Text.Trim().ToString() != string.Empty)
                                dtResult.DefaultView[0].Row["Password"] = txtPasswordEdit.Text.ToString().Trim();
                            dtResult.DefaultView[0].Row["Company"] = txtCompany.Text.ToString().Trim();
                            dtResult.DefaultView[0].Row["Phone"] = txtPhone.Text.ToString().Trim();
                            dtResult.DefaultView[0].Row["Fax"] = txtfax.Text.ToString().Trim();
                            dtResult.DefaultView[0].Row["Position"] = txtposition.Text.ToString().Trim();
                            DataTable dtUser = (DataTable)Session["dtLoginUser"];
                            dtResult.DefaultView[0].Row["ModifiedBy"] = Convert.ToInt32(dtUser.Rows[0]["UserID"].ToString());
                            dtResult.DefaultView[0].Row["ModifiedDate"] = Convert.ToDateTime(System.DateTime.Now);
                            if (PhotoUpload.FileName != "")
                            {
                                string Extention = System.IO.Path.GetExtension(PhotoUpload.PostedFile.FileName);
                                if (Extention != ".jpg" && Extention != ".GIF" && Extention != ".PNG")
                                    lblErrorMsg.Text = "Please upload jpg / PNG / GIF file only.";
                                else
                                    dtResult.DefaultView[0].Row["PhotoPath"] = UploadPhoto(PhotoUpload);
                            }
                            else
                                dtResult.DefaultView[0].Row["PhotoPath"] = DBNull.Value;

                            dtResult.DefaultView[0].EndEdit();
                            UserController.UpdateUsers(dtResult);
                            Response.Redirect("/Default.aspx", false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (log.IsErrorEnabled)
                        log.Error("[" + System.DateTime.Now.ToString("G") + "] AddAdminUser::Page_Load - ", ex);
                    Response.Redirect("/errorpages/SiteError.aspx", false);
                }
            }
        }

        private string UploadPhoto(FileUpload avatar_file)
        {
            string guidFilename = System.Guid.NewGuid().ToString();
            guidFilename += avatar_file.FileName.Substring(avatar_file.FileName.LastIndexOf(@"."));
            ServerQualifiedPath = ConfigurationManager.AppSettings["UserImagePathAbsolute"] + guidFilename;
            avatar_file.PostedFile.SaveAs(ServerQualifiedPath);
            return guidFilename;
        }

        public bool ValidateControls()
        {
            bool flag = true;
            if (Session["UserID"] != null)
            {
                strExist = "And u.Email = '" + txtEmail.Text + "' and u.UserID<>" + Session["UserID"].ToString();
                dtExist = UserController.GetAllUsersByWhereClause(strExist);
                if (dtExist.Rows.Count > 0)
                {
                    lblMsg.Text = "This username is already registered, enter another username<br/>";
                    flag = false;
                }
                if (txtEmail.Text.Trim().ToString() != string.Empty)
                {
                    strExist = "And u.Email = '" + txtEmail.Text + "' and u.UserID<>" + Session["UserID"].ToString();
                    dtExist = UserController.GetAllUsersByWhereClause(strExist);
                    if (dtExist.Rows.Count > 0)
                    {
                        lblMsg.Text = "This email address is already registered, enter another email address<br/>";
                        flag = false;
                    }
                }
            }
            else
            {
                if (txtPasswordEdit.Text.Trim().Length < 6)
                {
                    lblMsg.Text = "Password must be minimum of 6 characters<br/>";
                    flag = false;
                }
                strExist = "And u.Email = '" + txtEmail.Text + "'";
                dtExist = UserController.GetAllUsersByWhereClause(strExist);
                if (dtExist.Rows.Count > 0)
                {
                    lblMsg.Text = "This username has already been taken, choose another username<br/>";
                    flag = false;
                }
                if (txtEmail.Text.Trim().ToString() != string.Empty)
                {
                    strExist = "And u.Email = '" + txtEmail.Text + "'";
                    dtExist = UserController.GetAllUsersByWhereClause(strExist);
                    if (dtExist.Rows.Count > 0)
                    {
                        lblMsg.Text = "This email address is already registered, enter another email address<br/>";
                        flag = false;
                    }
                }
            }
            if (flag == true)
                return true;
            else return false;
        }
    }
}
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;

namespace Admin_v2.User
{
    public partial class AdminUserManager : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string WhereClause = "";
        protected static int pageno;
        protected static string SortDirections = "DESC", SortExpression = "CreatedDate";
        DataTable dtUser=null , dtRole=null ;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pageno = 10;
                dtRole = UserController.GetAllRole();
                if (dtRole.Rows.Count > 0)
                {
                    ddlRole.DataTextField = "RoleName";
                    ddlRole.DataValueField = "RoleID";
                    ddlRole.DataSource = dtRole;
                    ddlRole.DataBind();
                }
                WhereClause = "";
                dtUser = UserController.GetAllUsersByWhereClause(WhereClause);
                grdAdminUser.DataSource = dtUser;
                grdAdminUser.DataBind();
            }
        }
        protected void imgGo_Click(object sender, EventArgs e)
        {
            try
            {
                BindRepeater();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AdminUserManager::imgGo_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ddlRole.SelectedIndex = 0;
                ddlStatus.SelectedIndex = 0;
                txtAdminName.Text = "";
                txtCompanyName.Text = "";
                BindRepeater();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AdminUserManager::btnReset_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void BindRepeater()
        {
            try
            {
                if (txtAdminName.Text.Trim() != "")
                    WhereClause += " AND (u.FirstName + + u.LastName like '" + txtAdminName.Text.Trim() + "%' ) ";
                if (txtCompanyName.Text.Trim() != "")
                    WhereClause += " AND (u.Company like '" + txtCompanyName.Text + "%')";

                if (ddlRole.SelectedItem.Value != "None")
                    WhereClause += " AND (u.RoleID=" + ddlRole.SelectedValue + ")";
                else
                    WhereClause += "";
                if (ddlStatus.SelectedItem.Value != "None")
                    WhereClause += " AND (u.Status=" + ddlStatus.SelectedItem.Value + ")";

                DataTable dtResults = UserController.GetAllUsersByWhereClause(WhereClause);
                dtResults.DefaultView.Sort = SortExpression + " " + SortDirections;
                grdAdminUser.DataSource = dtResults.DefaultView;
                grdAdminUser.DataBind();

                if (dtResults.Rows.Count > 0)
                {
                    divpagination.Visible = true;
                    ErrorMsg.InnerText = "";
                }
                else
                {
                    divpagination.Visible = false;
                    ErrorMsg.InnerText = "No Records found";
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AdminUserManager::BindRepeater - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindRepeater();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AdminUserManager::ddlRole_SelectedIndexChanged - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdAdminUser.EditIndex = -1;
            SortDirections = "ASC";
            SortExpression = "Number";
            BindRepeater();
        }

        protected void grdAdminUser_DataBound(object sender, EventArgs e)
        {
            try
            {
                ddlPage.SelectedValue = pageno.ToString();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AdminUserManager::grdAdminUser_DataBound - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void grdAdminUser_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortExpression = e.SortExpression;
            if (SortDirections == "ASC")
                SortDirections = "DESC";
            else if (SortDirections == "DESC")
                SortDirections = "ASC";
            BindRepeater();
        }

        protected void grdAdminUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAdminUser.PageIndex = e.NewPageIndex;
            BindRepeater();
        }

        protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                pageno = Convert.ToInt32(ddlPage.SelectedValue);
                if (ddlPage.SelectedItem.Value != "0")
                {
                    grdAdminUser.AllowPaging = true;
                    grdAdminUser.PageSize = int.Parse(ddlPage.SelectedItem.Value);
                }
                else
                {
                    grdAdminUser.AllowPaging = false;
                }
                BindRepeater();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AdminUserManager::ddlPage_SelectedIndexChanged - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void LnkActive_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnSelectedMessages.Value.Trim() != "")
                {
                    string WhereClause = " AND u.UserID in (" + hdnSelectedMessages.Value.Substring(0, hdnSelectedMessages.Value.Length - 1) + ")";
                    DataTable dtResults = UserController.GetAllUsersByWhereClause(WhereClause);
                    if (dtResults.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtResults.Rows.Count; i++)
                        {
                            dtResults.Rows[i]["Status"] = true;
                        }
                        UserController.UpdateUsers(dtResults);
                        BindRepeater();
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AdminUserManager::LnkActive_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void LnkInactive_Click(object sender, EventArgs e)
        {
            try
            {
                if(hdnSelectedMessages.Value.Trim()!="")
                {
                    string WhereClause = " AND u.UserID in (" + hdnSelectedMessages.Value.Substring(0, hdnSelectedMessages.Value.Length - 1) + ")";
                    DataTable dtResults = UserController.GetAllUsersByWhereClause(WhereClause);
                    if (dtResults.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtResults.Rows.Count; i++)
                        {
                            dtResults.Rows[i]["Status"] = false;
                        }
                        UserController.UpdateUsers(dtResults);
                        BindRepeater();
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AdminUserManager::LnkInactive_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
    }
}

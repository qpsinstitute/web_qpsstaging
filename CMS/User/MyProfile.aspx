<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyProfile.aspx.cs" Inherits="Admin_v2.User.MyProfile" MasterPageFile="~/QPS.Master" %>

<asp:Content ID="cphMyProfile" runat="Server" ContentPlaceHolderID="ContentPlaceHolder">
    <script language="javascript" type="text/javascript">
        function ShowPassword()
        {
            document.getElementById("ctl00_ContentPlaceHolder_txtPasswordEdit").style.visibility="visible";
            document.getElementById("ctl00_ContentPlaceHolder_txtPasswordEdit").focus();
        }
    </script>
    
    <div id="toolbar-box">
        <div class="t">
            <div class="t">
                <div class="t">
                </div>
            </div>
        </div>
        <div class="m">
            <div id="toolbar" class="toolbar">
               <table class="toolbar">
                    <tr>
                       <td id="toolbar-save" class="button">
                            <asp:LinkButton ID="btnSave" runat="Server" OnClick="btnSave_Click" TabIndex="9" ValidationGroup="user"><span class="icon-32-save" title="Save"></span>Save</asp:LinkButton>
                        </td>
                        <td id="toolbar-cancel" class="button">
                            <a href="../Default.aspx" class="toolbar"><span class="icon-32-cancel" title="Cancel"></span>Cancel</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="header icon-48-user">
               <span id="smltitle" runat="Server" style="color: #0B55C4">My Profile</span>
            </div>
            <div class="clr">
            </div>
        </div>
        <div class="b">
            <div class="b">
                <div class="b">
                </div>
            </div>
        </div>
    </div>
    <div class="clr">
    </div>
    <div id="element-box">
        <div class="t">
            <div class="t">
                <div class="t">
                </div>
            </div>
        </div>
        <div class="m">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top">
                        <table class="adminform">
                            <tr>
                                <td>
                                </td>
                                <td align="left">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="left">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="title">
                                        <strong>
                                        First Name:<span style="color: red">*</span> </strong>
                                    </label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" TabIndex="1" Width="305px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtFirstName" ErrorMessage="RequiredFieldValidator" Font-Bold="True" SetFocusOnError="True" ValidationGroup="user" Display="Dynamic">Enter first name</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Enter Text only" ValidationExpression="[a-zA-Z]+" SetFocusOnError="true" ControlToValidate="txtFirstName" Display="Dynamic" Font-Bold="true" ValidationGroup="user"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                    Last Name:<span style="color: red">*</span></strong></td>
                                <td align="left">
                                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="50" TabIndex="1" Width="305px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtLastName" ErrorMessage="RequiredFieldValidator" Font-Bold="True" SetFocusOnError="True" ValidationGroup="user" Display="Dynamic">Enter last name</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Enter Text only" ValidationExpression="[a-zA-Z]+" SetFocusOnError="true" ControlToValidate="txtLastName" Display="Dynamic" Font-Bold="true" ValidationGroup="user"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Email Address:<span style="color: red">*</span></b></td>
                                <td align="left">
                                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" TabIndex="1" Width="305px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="RequiredFieldValidator" SetFocusOnError="True" ValidationGroup="user" Font-Bold="True" Display="Dynamic">Enter email address</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="RegularExpressionValidator" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="user" Font-Bold="True" Display="Dynamic">Enter valid email address</asp:RegularExpressionValidator></td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <a class="editlinktip hasTip" href="#" onclick="ShowPassword();" tabindex="1" title="Click here to change password">Password:</a>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtPasswordEdit" runat="server" MaxLength="50" Style="visibility:hidden;" TabIndex="1" TextMode="Password" Width="305px"></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td valign="top">
                                   <strong>
                                    Phone:<span style="color: red">*</span></strong></td>
                                <td align="left" nowrap="noWrap">
                                    <asp:TextBox ID="txtPhone" runat="server" Width="305px" MaxLength="50" TabIndex="1"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPhone" SetFocusOnError="true" Font-Bold="true" ValidationGroup="user" Display="Dynamic">Enter Phone Number</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPhone" ErrorMessage="Enter Valid Phone Number" Display="Dynamic"  ValidationExpression="[^a-zA-Z ]*" ValidationGroup="user" Font-Bold="true"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                             <tr>
                                <td valign="top">
                                   <strong>
                                    Fax:<span style="color: red">*</span></strong></td>
                                <td align="left" nowrap="noWrap">
                                    <asp:TextBox ID="txtfax" runat="server" Width="305px" MaxLength="50" TabIndex="1"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtfax" SetFocusOnError="true" Font-Bold="true" ValidationGroup="user" Display="Dynamic" >Enter Fax Number</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtfax" ErrorMessage="Enter Valid Fax Number" ValidationExpression="[^a-zA-Z ]*" Display="Dynamic" ValidationGroup="user" Font-Bold="true"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            
                            <tr>
                                <td valign="top">
                                   <strong>
                                    Position:<span style="color: red">*</span></strong></td>
                                <td align="left" nowrap="noWrap">
                                    <asp:TextBox ID="txtposition" runat="server" Width="305px" MaxLength="50" TabIndex="1"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtposition" SetFocusOnError="true" Font-Bold="true" ValidationGroup="user" Display="Dynamic">Enter Position</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Enter Text only" ControlToValidate="txtposition" ValidationExpression="^([a-zA-Z ]*)$" Display="Dynamic" Font-Bold="true" ValidationGroup="user"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            
                             <tr>
                                <td valign="top">
                                   <strong>
                                    Company:<span style="color: red">*</span></strong></td>
                                <td align="left" nowrap="noWrap">
                                    <asp:TextBox ID="txtCompany" runat="server" Width="305px" MaxLength="50" TabIndex="1"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCompany" SetFocusOnError="true" Font-Bold="true" ValidationGroup="user" Display="Dynamic">Enter Company</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Enter only alphanumeric value" ValidationExpression="^[a-zA-Z0-9 ]+$" ControlToValidate="txtCompany" SetFocusOnError="true" Display="Dynamic" Font-Bold="true" ValidationGroup="user"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td width="150">
                                     Photo:
                                </td>
                                <td align="left">
                                    <asp:FileUpload ID="PhotoUpload" runat="server" />
                                    <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </td> 
                            </tr>
                         
                           <tr>
                                <td>
                                </td>
                                <td align="left">
                                </td>
                            </tr>
                        </table>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False" ValidationGroup="user" />
                    </td>
                    
                    <td style="padding: 7px 0 0 5px" valign="top" width="360">
                        <table style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;" width="100%">
                            <tr>
                                <td style="padding-left:7px;">
                                    <strong>Status</strong>
                                </td>
                                <td>
                                    <label id="lblStatus" runat="server">Active</label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="padding-left:7px;">
                                    <strong>Created By</strong>
                                </td>
                                <td>
                                    <label id="lblCreatedBy" runat="server">Admin</label>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left:7px;">
                                    <strong>Created Date</strong>
                                </td>
                                <td>                                    
                                    <label id="lblCreated" runat="server">Not Created</label>                                    
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left:7px;">
                                    <strong>Modified By</strong>
                                </td>
                                <td>                                    
                                    <label id="lblModifiedBy" runat="server">Admin</label>                                    
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left:7px;">
                                    <strong>Modified Date</strong>
                                </td>
                                <td>                                    
                                    <label id="lblModified" runat="server">Not Modified</label>                                    
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
            </table>
            <div class="clr">
            </div>
        </div>
        <div class="b">
            <div class="b">
                <div class="b">
                </div>
            </div>
        </div>
    </div>
</asp:Content>

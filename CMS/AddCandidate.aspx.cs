using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;

namespace Admin_v2
{
    public partial class AddCandidate : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static DataTable dtCandidate = null;
        public static int count = 0;
        public static int CandidateId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {                
                if (!IsPostBack)
                {
                    if (Request.QueryString["CandidateId"] != null)
                    {
                        smltitle.InnerText = "[ Edit ]";
                        string WClause = "AND CandidateID =" + Request.QueryString["CandidateId"];
                        DataTable dtCandi = CandidateCotroller.GetCandidateByWClause(WClause);
                        if (dtCandi.Rows.Count > 0)
                        {
                            BindDataEditTime(dtCandi);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AddCandidate::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        private void BindDataEditTime(DataTable dt)
        {
            try
            {
                lblFirstName.Text = dt.Rows[0]["FirstName"].ToString();
                lblLastName.Text = dt.Rows[0]["LastName"].ToString();
                //txtStreetNo.Text = dt.Rows[0]["No"].ToString();
                lblStreet.Text = dt.Rows[0]["Street"].ToString();
                lblCity.Text = dt.Rows[0]["City"].ToString();
                lblState.Text = dt.Rows[0]["State"].ToString();
                lblZip.Text = dt.Rows[0]["Zipcode"].ToString();
                //txtCountry.Text = dt.Rows[0]["Country"].ToString();
                lblPhone.Text = dt.Rows[0]["Phone"].ToString();
                lblMobile.Text = dt.Rows[0]["Mobile"].ToString();
                lblJob.Text = dt.Rows[0]["JobSkill"].ToString();
                lblEmail.Text = dt.Rows[0]["Email"].ToString();
                lblResume.Text = dt.Rows[0]["Resume"].ToString();
                lblCover.Text = dt.Rows[0]["CoverLetter"].ToString();                
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AddCandidate::BindDataEditTime() - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
                //if (fuResume.FileName.ToString() != "")
                //    txtReusme.Text = fuResume.FileName.ToString();
                //if (fuCover.FileName.ToString() != "")
                //    txtCover.Text = fuCover.FileName.ToString();
                //if (ValidateControl())
                //{
                //    if (Request.QueryString["CandidateId"] != null)
                //    {
                //        CandidateId = int.Parse(Request.QueryString["CandidateId"].ToString());
                //        string WClause = "AND CandidateID =" + CandidateId;
                //        dtCandidate = CandidateCotroller.GetCandidateByWClause(WClause);

                //        if (dtCandidate.DefaultView.Count > 0)
                //        {
                //            dtCandidate.DefaultView[0].BeginEdit();
                //            #region EditRow
                //            if (txtFirstName.Text.Trim() == "")
                //                dtCandidate.DefaultView[0].Row["FirstName"] = DBNull.Value;
                //            else dtCandidate.DefaultView[0].Row["FirstName"] = txtFirstName.Text.Trim();
                //            if (txtLastName.Text.Trim() == "")
                //                dtCandidate.DefaultView[0].Row["LastName"] = DBNull.Value;
                //            else dtCandidate.DefaultView[0].Row["LastName"] = txtLastName.Text.Trim();
                //            //if (txtStreetNo.Text.Trim() == "")
                //            //    dtCandidate.DefaultView[0].Row["No"] = DBNull.Value;
                //            //else dtCandidate.DefaultView[0].Row["No"] = txtStreetNo.Text.Trim();
                //            if (txtStreet.Text.Trim() == "")
                //                dtCandidate.DefaultView[0].Row["Street"] = DBNull.Value;
                //            else dtCandidate.DefaultView[0].Row["Street"] = txtStreet.Text.Trim();
                //            if (txtCity.Text.Trim() == "")
                //                dtCandidate.DefaultView[0].Row["City"] = DBNull.Value;
                //            else dtCandidate.DefaultView[0].Row["City"] = txtCity.Text.Trim();
                //            if (txtState.Text.Trim() == "")
                //                dtCandidate.DefaultView[0].Row["State"] = DBNull.Value;
                //            else dtCandidate.DefaultView[0].Row["State"] = txtState.Text.Trim();
                //            if (txtZip.Text.Trim() == "")
                //                dtCandidate.DefaultView[0].Row["Zipcode"] = DBNull.Value;
                //            else dtCandidate.DefaultView[0].Row["Zipcode"] = txtZip.Text.Trim();
                //            //if (txtCountry.Text.Trim() == "")
                //            //    dtCandidate.DefaultView[0].Row["Country"] = DBNull.Value;
                //            //else dtCandidate.DefaultView[0].Row["Country"] = txtCountry.Text.Trim();
                //            if (txtPhone.Text.Trim() == "")
                //                dtCandidate.DefaultView[0].Row["Phone"] = DBNull.Value;
                //            else dtCandidate.DefaultView[0].Row["Phone"] = txtPhone.Text.Trim();
                //            if (txtMobile.Text.Trim() == "")
                //                dtCandidate.DefaultView[0].Row["Mobile"] = DBNull.Value;
                //            else dtCandidate.DefaultView[0].Row["Mobile"] = txtMobile.Text.Trim();
                //            if (txtEmail.Text.Trim() == "")
                //                dtCandidate.DefaultView[0].Row["Email"] = DBNull.Value;
                //            else dtCandidate.DefaultView[0].Row["Email"] = txtEmail.Text.Trim();
                //            if (txtReusme.Text.Trim() == "")
                //            {
                //                dtCandidate.DefaultView[0].Row["Resume"] = DBNull.Value;
                //                dtCandidate.DefaultView[0].Row["ResumeGUID"] = DBNull.Value;
                //            }
                //            else
                //            {
                //                dtCandidate.DefaultView[0].Row["Resume"] = txtReusme.Text.Trim();
                //                if (fuResume.FileName.ToString() != "")
                //                    dtCandidate.DefaultView[0].Row["ResumeGUID"] = UploadFiles(fuResume, 1);
                //            }
                //            if (txtCover.Text.Trim() == "")
                //            {
                //                dtCandidate.DefaultView[0].Row["CoverLetter"] = DBNull.Value;
                //                dtCandidate.DefaultView[0].Row["CoverLetterGUID"] = DBNull.Value;
                //            }
                //            else
                //            {
                //                dtCandidate.DefaultView[0].Row["CoverLetter"] = txtCover.Text.Trim();
                //                if(fuCover.FileName.ToString() != "")
                //                    dtCandidate.DefaultView[0].Row["CoverLetterGUID"] = UploadFiles(fuCover,2);
                //            }
                //            if (txtJobSkill.Text.Trim() == "")
                //                dtCandidate.DefaultView[0].Row["JobSkill"] = DBNull.Value;
                //            else dtCandidate.DefaultView[0].Row["JobSkill"] = txtJobSkill.Text.Trim();
                //            dtCandidate.DefaultView[0].Row["ModifiedDate"] = DateTime.Now;
                //            #endregion
                //            dtCandidate.DefaultView[0].EndEdit();
                //            CandidateCotroller.UpdateCandidate(dtCandidate);                            
                //            Response.Redirect("CandidateManager.aspx", false);
                //        }
                //    }
                //    else
                //    {
                //        string WClause = "AND CandidateID =" + 0;
                //        dtCandidate = CandidateCotroller.GetCandidateByWClause(WClause);
                //        DataRow drCandi = dtCandidate.NewRow();

                //        #region AddRow

                //        if (txtFirstName.Text.Trim() == "")
                //            drCandi["FirstName"] = DBNull.Value;
                //        else drCandi["FirstName"] = txtFirstName.Text.Trim();

                //        if (txtLastName.Text.Trim() == "")
                //            drCandi["LastName"] = DBNull.Value;
                //        else drCandi["LastName"] = txtLastName.Text.Trim();

                //        //if (txtStreetNo.Text.Trim() == "")
                //        //    drCandi["No"] = DBNull.Value;
                //        //else drCandi["No"] = Convert.ToInt32(txtStreetNo.Text.Trim());

                //        if (txtStreet.Text.Trim() == "")
                //            drCandi["Street"] = DBNull.Value;
                //        else drCandi["Street"] = txtStreet.Text.Trim();

                //        if (txtCity.Text.Trim() == "")
                //            drCandi["City"] = DBNull.Value;
                //        else drCandi["City"] = txtCity.Text.Trim();

                //        if (txtState.Text.Trim() == "")
                //            drCandi["State"] = DBNull.Value;
                //        else drCandi["State"] = txtState.Text.Trim();

                //        if (txtZip.Text.Trim() == "")
                //            drCandi["Zipcode"] = DBNull.Value;
                //        else drCandi["Zipcode"] = Convert.ToInt32(txtZip.Text.Trim());

                //        //if (txtCountry.Text.Trim() == "")
                //        //    drCandi["Country"] = DBNull.Value;
                //        //else drCandi["Country"] = txtCountry.Text.Trim();

                //        if (txtPhone.Text.Trim() == "")
                //            drCandi["Phone"] = DBNull.Value;
                //        else drCandi["Phone"] = Convert.ToInt64(txtPhone.Text.Trim());

                //        if (txtMobile.Text.Trim() == "")
                //            drCandi["Mobile"] = DBNull.Value;
                //        else drCandi["Mobile"] = Convert.ToInt64(txtMobile.Text.Trim());

                //        if (txtEmail.Text.Trim() == "")
                //            drCandi["Email"] = DBNull.Value;
                //        else drCandi["Email"] = txtEmail.Text.Trim();

                //        if (txtJobSkill.Text.Trim() == "")
                //            drCandi["JobSkill"] = DBNull.Value;
                //        else drCandi["JobSkill"] = txtJobSkill.Text.Trim();

                //        if (txtReusme.Text.Trim() == "")
                //        {
                //            drCandi["Resume"] = DBNull.Value;
                //            drCandi["ResumeGUID"] = DBNull.Value;
                //        }
                //        else
                //        {
                //            drCandi["Resume"] = txtReusme.Text.Trim();
                //            drCandi["ResumeGUID"] = UploadFiles(fuResume,1);
                //        }

                //        if (txtReusme.Text.Trim() == "")
                //        {
                //            drCandi["CoverLetter"] = DBNull.Value;
                //            drCandi["CoverLetterGUID"] = DBNull.Value;
                //        }
                //        else
                //        {
                //            drCandi["CoverLetter"] = txtReusme.Text.Trim();
                //            drCandi["CoverLetterGUID"] = UploadFiles(fuCover,2);
                //        }
                //        drCandi["CreatedDate"] = DateTime.Now;
                //        drCandi["ModifiedDate"] = DateTime.Now;

                //        #endregion
                //        dtCandidate.Rows.Add(drCandi);
                //        CandidateCotroller.UpdateCandidate(dtCandidate);
                //        Response.Redirect("CandidateManager.aspx", false);
                //    }
                //}
        //    }
        //    catch (Exception ex)
        //    {
        //        if (log.IsErrorEnabled)
        //            log.Error("[" + System.DateTime.Now.ToString("G") + "] AddCandidate::btnSave_Click - ", ex);
        //        Response.Redirect("/errorpages/SiteError.aspx", false);
        //    }
        //}

        //private string UploadFiles(FileUpload file1,int No)
        //{
            //string p = file1.FileName;
            //string guidFileName1 = System.Guid.NewGuid().ToString();
            //guidFileName1 += p.Substring(p.LastIndexOf(@"."));
            //string fullpath;
            //if (No == 1)
            //{
            //    fullpath = Server.MapPath("Resume").ToString() + "/"+ guidFileName1;
            //}
            //else
            //{
            //    fullpath = Server.MapPath("CoverLetter") + "/" + guidFileName1;
            //}

            //file1.PostedFile.SaveAs(fullpath);
            //return guidFileName1;
        //}

        //private bool ValidateControl()
        //{
        //    bool flag = true;
        //    if (txtFirstName.Text == "" || txtLastName.Text == "" || txtStreet.Text == "" || txtCity.Text == "" || txtState.Text == "" || txtMobile.Text == "" || txtEmail.Text == "" || txtJobSkill.Text == "")
        //    {
        //        ErrorMsg.InnerText = "* Please Enter All the fields.....!";
        //        flag = false;
        //    }
        //    return flag;
        //}

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CandidateManager.aspx", false);
        }

        //protected void lnkUpdate1_Click(object sender, EventArgs e)
        //{
        //    //div1.Visible = true;
        //}

        //protected void lnkUpdate2_Click(object sender, EventArgs e)
        //{
        //    //div2.Visible = true;
        //}
    }
}

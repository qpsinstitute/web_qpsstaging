<%@ Page Language="C#" MasterPageFile="~/QPS.Master" AutoEventWireup="true" CodeBehind="AddCandidate.aspx.cs"
    Title="Candidate Manager" Inherits="Admin_v2.AddCandidate" %>

<asp:Content ID="cphHome" runat="Server" ContentPlaceHolderID="ContentPlaceHolder">
    <style type="text/css">
        .fieldForm div .fieldItemValue
        {
            padding-left: 10px;
        }
        .fieldForm .fieldItemLabel
        {
            width: 9em;
            float: left;
            height: auto;
            padding-top: 1px;
            text-align: right;
        }
        .fieldForm .fieldItemLabel label
        {
            font-family: verdana;
            font-size: 10px;
            align: right;
        }
        
        
        
        .fieldForm .field100Pct, .fieldForm .field50Pct
        {
            min-width: 50%;
        }
        
        .fieldForm .field100Pct .fieldItemValue, .fieldForm .field50Pct .fieldItemValue
        {
            display: inline;
        }
        
        .fieldForm .field100Pct .fieldItemValue input
        {
            width: 80%;
            min-width: 200px;
        }
        
        .fieldForm .field50Pct .fieldItemValue input
        {
            width: 100%;
            min-width: 100px;
        }
        
        .fieldForm .field50Pct
        {
            float: left;
            width: 90%;
        }
        
        .fieldForm div div input
        {
            margin-bottom: 10px;
            margin-right: 15px;
        }
        
        .fieldForm
        {
            background-color: #f9f9f9;
            border: solid 1px #d5d5d5;
            width: 100%;
            border-collapse: collapse;
            margin: 8px 0 10px 0;
            margin-bottom: 15px;
            font-size: 9;
            font-family: verdana;
        }
        
        .clear
        {
            clear: both;
        }
        
        .style3
        {
            font-size: 11px;
            font-weight: bold;
            color: #666666;
        }
    </style>
    <ajax:ScriptManager ID="SM1" runat="Server">
    </ajax:ScriptManager>
    <ajax:UpdateProgress ID="UPGR1" runat="Server" DisplayAfter="10">
        <ProgressTemplate>
            <div id="loading" style="position: absolute; top: 40%; left: 50%;">
                <img alt="Loading...." src="/images/ajax-loader.gif">
            </div>
        </ProgressTemplate>
    </ajax:UpdateProgress>
    <ajax:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div id="toolbar-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <div class="toolbar" id="toolbar">
                        <table class="toolbar">
                            <tr>
                                <td id="toolbar-save" class="button">
                                    <%--<asp:LinkButton ID="btnSave" runat="Server" OnClick="btnSave_Click"><span class="icon-32-save" title="Save"></span>Save</asp:LinkButton>--%>
                                </td>
                                <td id="toolbar-cancel" class="button">
                                    <asp:LinkButton ID="btnCancel" runat="Server" OnClick="btnCancel_Click"><span class="icon-32-cancel" title="Cancel"></span>Cancel</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="header icon-48-addedit">
                        Candidate Manager: <small><small id="smltitle" runat="Server">[ New ]</small></small>
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
            <div id="element-box">
                <div class="t">
                    <div class="t">
                        <div class="t">
                        </div>
                    </div>
                </div>
                <div class="m">
                    <div class="fieldForm" id="fieldForm">
                        <table width="100%" style="pad">
                            <tr>
                                <td colspan="2">
                                    <div style="color: red">
                                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label><span
                                            id="ErrorMsg" style="font-weight: bold;" runat="server"></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 8%;">
                                </td>
                                <td style="font-family: verdana; font-size: 10px;">
                                    <asp:CheckBoxList ID="ChkTrainingCourse" runat="server" RepeatColumns="2" DataTextField="TrainingName"
                                        DataValueField="TrainingTypeId" TabIndex="1" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="width: 100%; height: 10px;">
                                        <asp:Label runat="server" ID="Label1" CssClass="style3">
                        1. NAME:       
                                        </asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    First Name:</label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%;">
                                                <%--<asp:TextBox ID="txtFirstName" runat="server" TabIndex="3" Width="160px"></asp:TextBox>--%>
                                            </div>
                                            <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                                        </div>
                                        <div class="fieldItemValue" style="width: 10%">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    Last Name:</label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%">
                                                <asp:Label ID="lblLastName" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="width: 100%; height: 10px;">
                                        <asp:Label runat="server" ID="Label2" CssClass="style3">
                        2. ADDRESS:    
                                        </asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    Street:</label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%">
                                                <asp:Label ID="lblStreet" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    City:</label>
                                            </div>
                                            <div class="fieldItemValue">
                                                <%--<asp:TextBox ID="txtCity" runat="server" TabIndex="8" Width="160px"></asp:TextBox>--%>
                                                <asp:Label ID="lblCity" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    State:</label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 25%">
                                                <%--<asp:TextBox ID="txtState" runat="server" TabIndex="9" Width="160px"></asp:TextBox>--%>
                                                <asp:Label ID="lblState" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    Zip:</label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%">
                                                <%--<asp:TextBox ID="txtZip" runat="server" TabIndex="10" Width="160px"></asp:TextBox>--%>
                                                <asp:Label ID="lblZip" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="width: 100%; height: 10px;">
                                        <asp:Label runat="server" ID="Label3" CssClass="style3">
                         3. PHONES:     
                                        </asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    Phone:</label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%">
                                                <%--<asp:TextBox ID="txtPhone" runat="server" TabIndex="12" Width="160px"></asp:TextBox>--%>
                                                <asp:Label ID="lblPhone" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    Mobile:</label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%">
                                                <%--<asp:TextBox ID="txtMobile" runat="server" TabIndex="13" Width="160px"></asp:TextBox>--%>
                                                <asp:Label ID="lblMobile" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    Email:</label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%">
                                                <%--<asp:TextBox ID="txtEmail" runat="server" TabIndex="14" Width="160px"></asp:TextBox>--%>
                                                <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="width: 100%; height: 10px;">
                                        <asp:Label runat="server" ID="Label4" CssClass="style3">
                         3. SKILL AREA:     
                                        </asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    Jobs Looking For:</label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%">
                                                <%--<asp:TextBox ID="txtJobSkill" runat="server" TabIndex="12" Width="160px"></asp:TextBox>--%>
                                                <asp:Label ID="lblJob" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="width: 100%; height: 10px;">
                                        <asp:Label runat="server" ID="Label5" CssClass="style3">
                         4. RESUME:     
                                        </asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    Resume Doc:</label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%">
                                                <%--<asp:TextBox ID="txtReusme" runat="server" TabIndex="12" Width="160px"></asp:TextBox>--%>
                                                <asp:Label ID="lblResume" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <%--<div class="fieldItemLabel" style="width: auto">
                                            <asp:LinkButton ID="lnkUpdate1" runat="server" Visible="false" OnClick="lnkUpdate1_Click">Update</asp:LinkButton>
                                        </div>--%>
                                        <%--<div id="div1" runat="server" class="fieldItemLabel" style="width: auto;" visible="false">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    Upload:<span style="color: #ff0000">*</span></label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%">
                                                <asp:FileUpload ID="fuResume" runat="server" Width="160px" />
                                            </div>
                                        </div>--%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 5px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="field50Pct" style="width: 100%">
                                        <div class="fieldItemLabel" style="width: auto">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    Cover Letter:</label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%">
                                                <%--<asp:TextBox ID="txtCover" runat="server" TabIndex="12" Width="160px"></asp:TextBox>--%>
                                                <asp:Label ID="lblCover" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <%--<div class="fieldItemLabel" style="width: auto">
                                            <asp:LinkButton ID="lnkUpdate2" runat="server" Visible="false" OnClick="lnkUpdate2_Click">Update</asp:LinkButton>
                                        </div>
                                        <div id="div2" runat="server" class="fieldItemLabel" style="width: auto" visible="false">
                                            <div class="fieldItemLabel">
                                                <label for="field3">
                                                    Cover Letter Upload:<span style="color: #ff0000">*</span></label>
                                            </div>
                                            <div class="fieldItemValue" style="width: 10%">
                                                <asp:FileUpload ID="fuCover" runat="server" Width="160px" />
                                            </div>
                                        </div>--%>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="b">
                    <div class="b">
                        <div class="b">
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </ajax:UpdatePanel>
</asp:Content>

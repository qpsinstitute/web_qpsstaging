using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;

namespace Admin_v2
{
    public partial class _Default : System.Web.UI.Page
    {
        public int role;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
                    if (!IsPostBack)
                    {
                        try
                        {
                            if (Session["dtLoginUser"] != null)
                            {
                                DataTable dtsession = (DataTable)Session["dtLoginUser"];
                                role = Convert.ToInt32(dtsession.Rows[0]["RoleID"]);
                                if (role == 1)
                                {
                                    truser.Visible = true;
                                }
                                else
                                {
                                    truser.Visible = false;
                                    smltitle.InnerText = "Users Profile";
                                }
                            }
                            else
                            {
                                FormsAuthentication.RedirectToLoginPage();
                            }
                        }
                        catch (Exception ex)
                        {
                            if (log.IsErrorEnabled)
                                log.Error("[" + System.DateTime.Now.ToString("G") + "] SignIn::lnkLogin_Click - ", ex);
                          //  Response.Redirect("/errorpages/SiteError.aspx", false);
                        }
                    }

                }
            }
        }

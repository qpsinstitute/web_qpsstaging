using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;

namespace ElearnAdmin
{
    public partial class SignIn : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string UserName = "";
        public string PassWord = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtPassword.Attributes.Add("OnKeyDown", "javascript:if (event.keyCode == 13) __doPostBack('" + lnkLogin.UniqueID + "','')");
                if (txtUserID.Value.Trim() != "" && txtPassword.Value.Trim() != "")
                    lnkLogin_Click(sender, e);
            }
            catch (Exception ex)
            {
                
                    if (log.IsErrorEnabled)
                        lblmsg.Text = ex.ToString();
                    //    log.Error("[" + System.DateTime.Now.ToString("G") + "] SignIn::Page_Load - ", ex);
                    //Response.Redirect("/errorpages/SiteError.aspx", false);

                
               // if (log.IsErrorEnabled)
                   // log.Error("[" + System.DateTime.Now.ToString("G") + "] SignIn::Page_Load - ", ex);
                //Response.Redirect("/errorpages/SiteError.aspx", false);

            }
        }

        protected void lnkLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateUserControls())
                {
                    UserName = txtUserID.Value.Trim();
                    PassWord = txtPassword.Value.Trim();
                    DataRow drUser = UserController.CheckUserAuthentication(txtUserID.Value.Trim());
                    DataTable dtUser = drUser.Table.Clone();
                    DataRow drTemp = dtUser.NewRow();
                    drTemp["UserID"] = drUser["UserID"];
                    drTemp["FirstName"] = drUser["FirstName"];
                    drTemp["LastName"] = drUser["LastName"];
                    drTemp["Email"] = drUser["Email"];                    
                    drTemp["Password"] = drUser["Password"];
                    drTemp["Phone"] = drUser["Phone"];
                    drTemp["RoleID"] = drUser["RoleID"];
                    drTemp["Company"] = drUser["Company"];
                    drTemp["CreatedDate"] = drUser["CreatedDate"];
                    drTemp["CreatedBy"] = drUser["CreatedBy"];
                    drTemp["ModifiedDate"] = drUser["ModifiedDate"];
                    drTemp["ModifiedBy"] = drUser["ModifiedBy"];
                    drTemp["PhotoPath"] = drUser["PhotoPath"];
                    drTemp["Status"] = drUser["Status"];
                    dtUser.Rows.Add(drTemp);
                    if (dtUser.Rows.Count > 0)
                    {
                        Session.Add("dtLoginUser", dtUser);
                        if (Request.QueryString["ReturnUrl"] != null)
                        {
                            if (chkVal.Value == "true")
                                Page.RegisterStartupScript("doLogin", "<script>doLoginCookie1('" + Request.QueryString["ReturnUrl"].ToString() + "');</script>");
                            else
                                Response.Redirect(Request.QueryString["ReturnUrl"], false);
                        }
                        else
                        {
                            if (chkVal.Value == "true")
                                Page.RegisterStartupScript("doLogin", "<script>doLoginCookie();</script>");
                            else
                                Response.Redirect("Default.aspx", false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] SignIn::lnkLogin_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        private bool ValidateUserControls()
        {
            bool isValidControls = true;
            if (txtUserID.Value.Trim() == "")
            {
                ErrMessage.Text = "Blank UserName Try Again !!!";
                txtUserID.Attributes.Add("Class", "Warning");
                isValidControls = false;
            }
            else if (txtPassword.Value.Trim() == "")
            {
                ErrMessage.Text = "Blank Password Try Again !!!";
                txtPassword.Attributes.Add("Class", "Warning");
                isValidControls = false;
            }
            else
            {
                DataRow userDR = UserController.CheckUserAuthentication(txtUserID.Value.Trim());
                if (userDR != null)
                {
                    DataTable userDT = userDR.Table.Clone();
                    DataRow drTemp = userDT.NewRow();
                    drTemp["UserID"] = userDR["UserID"];
                    drTemp["Email"] = userDR["Email"];
                    drTemp["Password"] = userDR["Password"];
                    drTemp["Status"] = userDR["Status"];
                    drTemp["RoleID"] = userDR["RoleID"];
                    drTemp["Company"] = userDR["Company"];
                    drTemp["CreatedDate"] = userDR["CreatedDate"];
                    userDT.Rows.Add(drTemp);
                    if (userDT.Rows.Count > 0)
                    {
                        if (txtUserID.Value.ToString().Trim() != userDT.Rows[0]["Email"].ToString().Trim())
                        {
                            ErrMessage.Text = "Invalid UserName Try Again !!!";
                            txtUserID.Attributes.Add("Class", "Warning");
                            isValidControls = false;
                        }
                        else if (txtPassword.Value.ToString().Trim() != userDT.Rows[0]["PassWord"].ToString().Trim())
                        {
                            ErrMessage.Text = "Invalid Password Try Again !!!";
                            txtPassword.Attributes.Add("Class", "Warning");
                            isValidControls = false;
                        }                        
                    }
                    else
                    {
                        ErrMessage.Text = "Invalid UserName/Password Try Again !!!";
                        txtUserID.Attributes.Add("Class", "Warning");
                        txtPassword.Attributes.Add("Class", "Warning");
                        isValidControls = false;
                    }
                }
                else
                {
                    ErrMessage.Text = "Invalid UserName/Password Try Again !!!";
                    txtUserID.Attributes.Add("Class", "Warning");
                    txtPassword.Attributes.Add("Class", "Warning");
                    isValidControls = false;
                }
            }
            return isValidControls;
        }
    }
}

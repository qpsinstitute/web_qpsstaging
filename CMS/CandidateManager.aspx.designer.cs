//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3603
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Admin_v2.Job {
    
    public partial class Candidate_Info {
        protected System.Web.UI.ScriptManager SM1;
        protected System.Web.UI.UpdateProgress UPGR1;
        protected System.Web.UI.UpdatePanel UP1;
        protected System.Web.UI.HtmlControls.HtmlAnchor LnkDelete;
        protected System.Web.UI.WebControls.TextBox txtDesc;
        protected System.Web.UI.HtmlControls.HtmlButton btnGo;
        protected System.Web.UI.HtmlControls.HtmlButton btnReset;
        protected System.Web.UI.WebControls.TextBox txtSearch;
        protected System.Web.UI.HtmlControls.HtmlButton btnSearch;
        protected System.Web.UI.HtmlControls.HtmlGenericControl ErrorMsg;
        protected System.Web.UI.WebControls.GridView gvCandidate;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnApprover;
        protected System.Web.UI.HtmlControls.HtmlGenericControl PageSize;
        protected System.Web.UI.WebControls.DropDownList ddlPage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSelectedMessages;
    }
}

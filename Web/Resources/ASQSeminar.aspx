﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="ASQSeminar.aspx.cs" Inherits="QPS.Resources.ASQSeminar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
<div class="span-19 top" style="float: right">
            <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
    <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                QPS Student with Certifications on CCN!
            </h3>
            <br />
            <p style="font-size: 125%">
               certifications including PMP, Risk Management Professional etc.
 
Mr. Patel served at local and regional levels for the Project Management Institute, Institute of Industrial Engineers and American Society for Quality besides recipient of many awards.
 
He is an ASQ fellow and has been ASQ Worcester Section Chair. Jay has been Chairman of North East Quality Council (NEQC) and Conference \Chair more than 5 times besides NEQC’s prestigious R. Shaw award recipient.



​Dinner Meeting Reservation Process
 
Dinner meeting reservations can be made on the Picatic website.  Click here to register.
 
Important:  
 
If you are paying at the door, please chose the payment option of Invoice.  You will receive a copy of an invoice indicating you will be paying at the door.
 
If you are paying by credit card, please enter the information when prompted.
 
If you have prepaid for the year, please enter Promo Code: prepaid.

If you are the lucky recipient of the free dinner meeting door prize drawn at a prior meeting, please enter Promo Code DoorPrize.

If you have an ASQ Student Membership, please enter Promo Code:  Student

If you are unemployed, please enter Promo Code:  Unemployed
  
Should you need assistance, please contact the Worcester Section at ASQWorcester0110@gmail.com or Patience Miller at pmiller@pipettecal.com.  


​
Click here to sign up for the meeting by 6/15/15.

Click here for directions to Marlboro Courtyard

If you don’t have access or have problems registering using the link above, contact ASQ Worcester at asqworcester0110@gmail.com. or Patience Miller at pmiller@pipettecal.com 


Please let us know at the time of registration if you have dietary restrictions so that proper arrangements can be made for your meal.


For ASQ Worcester news and announcements, visit our website  http://asq.org/sections/mini-sites/0110
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
</asp:Content>

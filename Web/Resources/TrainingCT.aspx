<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrainingCT.aspx.cs" Inherits="QPS.Resources.TrainingCT" MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentTrainingCT" ContentPlaceHolderID="cphContent" runat="server">

<div class="span-24 prepend-1 top highcont">
    <div class="span-19 top" style="float:right">
    <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>
		<div class="span-4 blackbl last"><a  href ="links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
	    <div class="span-4 blackbk last"><a title="Press Releases">Press Releases</a></div>
        <br class="clear"><br class="clear">
		<h3 class="colbright" id="training" >
		    QPS � Now Approved To Conduct Public Training in Connecticut!    
		</h3>
		<br />
		<p style="font-size: 125%">
		    <b class="colbright">Hartford, C.T. � March 31, 2010 �</b>
		    <font style="font-size:90%">
		     QPS is going global, and going local! For years, Quality & Productivity <br />Solutions, Inc. has provided corporate training all over the globe. QPS has also provided public training for <br /> unemployed personnel in the state of Massachusetts. Recently, QPS obtained training provider approval in <br />the state of Rhode Island as well. And, effective today, QPS can now officially conduct public training for<br /> unemployed personnel in the state of Connecticut as well. Newly approved by the Connecticut Department <br /> of Labor, QPS is now on the State of Connecticut Eligible Training Provider list. <a target="_blank" href="http://www1.ctdol.state.ct.us/etpl/index.asp">Click here to see the QPS on <br /> the State of CT�s Eligible Training Provider List website.</a><br />
            As an approved training provider in Connecticut, QPS gives the unemployed an opportunity to obtain funding <br /> from state and federal entities to pay for its Lean, Six Sigma and other well-known programs and courses.<br /> A total of 12 programs are approved in Connecticut; many of these programs offer multiple certifications. <br /> <a target="_blank" href="../Documents/CT Unemployed  Course Matrix July 7, 2010.pdf">Click here to view QPS�s CT approved training.</a><br />
           Interested candidates can easily call their local <a target="_blank" href="../Documents/State of CT Career Centers.pdf"> CTWorks One-Stop Career Center</a> and make an appointment<br /> with their career counselor to start the application process.  Training is conducted at QPS�s own facility, located<br /> at 1224 Mill Street, Building B, East Berlin, Connecticut, located just outside of Hartford.
            </font>
		</p>
		<div></div>
		<p style="font-size: 125%"><b class="colbright">Contact</b><br />
        Jay Patel<br />
        President & CEO<br />
        Toll Free: 1-877-987-3801<br/>
        <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
        </p>
    </div>
    
    <div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>

</asp:Content>


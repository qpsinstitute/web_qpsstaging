﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QpsWebinars.aspx.cs" Inherits="QPS.Resources.QpsWebinars" MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentQpsWebinars" ContentPlaceHolderID="cphContent" runat="server">

<div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
    <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                QPS - Now Offering Webinars!
            </h3>
            <br />
            <p style="font-size: 125%">
                <b class="colbright">February 1, 2012 –</b> <font style="font-size: 90%">
		    Jay P. Patel, President and CEO of Quality & Productivity Solutions, Inc. announced that QPS will be conducting webinars.  Conducting webinars is a special milestone at QPS as it allows anyone around the world to experience the QPS Institute in the luxury of their own location, without the need to travel.  “QPS Webinars on average are anywhere from 75 minutes to 90 minutes, but if the topic requires more time, they can be longer” said Jay Patel, President & CEO of Quality & Productivity Solutions, Inc.  “It’s important that we offer a variety of topics because variety is priority.  It pleases the audience.”Pricing for QPS webinars, although reasonable, vary and include pdf materials of the topic emailed to participants afterward.  Corporate pricing costs more than the individual price as it allows up to 10 participants from 1 company to dial in, see and listen. <a href="http://www.qpsinc.com/Webinars/Default.aspx">Click here to view all QPS Webinars.</a><br />
            </font>
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div></asp:Content>
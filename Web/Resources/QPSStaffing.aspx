﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="QPSStaffing.aspx.cs" Inherits="QPS.Resources.QPSStaffing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">

<div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
    <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                QPS launches new division – Staffing & Recruitment!
            </h3>
            <br />
            <p style="font-size: 125%">
                <b class="colbright">April 16, 2012 –</b> <font style="font-size: 90%">
		    Jay P. Patel, President and CEO of Quality & Productivity Solutions, Inc. announced that QPS will be launching a new division – staffing and recruitment! Staffing and recruitment play a critical role at QPS - to see to the placement of the right candidate at right employer – within the most timely and cost effective manner possible. “QPS wants to help connect unemployed candidates to not just jobs – but to life-long careers” said Jay Patel, President & CEO of Quality & Productivity Solutions, Inc. QPS intends on serving as a bridge between employers and unemployed candidates, and its recruiter, Ms. Shailja Kanjolia, looks forward to making it possible.<br /><br /></font>
               <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
           </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

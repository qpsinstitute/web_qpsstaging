﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="IsoSeminar.aspx.cs" Inherits="QPS.Resources.IsoSeminar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
<div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                QPS Institute - Offering new seminars for ISO 9001:2015
            </h3>
          
            <p style="font-size: 125%">
                <b class="colbright">Marlborough, MA, March 1, 2015 – </b><font style="font-size: 90%">
                    Jay P. Patel, President and CEO of Quality & Productivity Solutions, Inc. announced that QPS will be conducting training at its all locations in Massachusetts, Connecticut, Rhode Island, New Jersey, North Carolina, Florida, Illinois. The new courses are being offered are as follows:</font>
                <br /><br />
                <font style="font-size: 90%">ISO 9001:2015 Understanding changes and Implementation<br /><br />Documenting ISO 9001:2015 system<br /><br />Auditing to ISO 9001;2015 <br /><br />Risk based thinking and Risk Management in ISO 9001:2015<br /></font>
  
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
              <font style="font-size: 90%"><a href="http://www.qpsinc.com/Training/Calendar.aspx?id=1">New Calendar for Spring and Fall will outline dates  and locations.</a></font>
              <br />
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

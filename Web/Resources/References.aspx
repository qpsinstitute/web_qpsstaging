<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="References.aspx.cs" Inherits="QPS.Resources.References" %>

<asp:Content ID="ContentReferences" ContentPlaceHolderID="cphContent" runat="server" >
<script language="javascript" type="text/javascript">
   // setPage("Resources");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	     <%--<div class="span-3 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbk last"><a title="References">References</a></div>--%>
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
		<div class="span-4 blackbl last"><a href="Releases.aspx" title="Press Releases">Press Releases</a></div>

		<br class="clear"><br class="clear">
		<h3 class="colbright" id="1">References</h3>
        <%--<h4 class="colblu mostwrap bold">
            QPS in CNN</h4>--%>
        <%--<ul class="hrefplainclass">
            <li>
                <div class="plainhrefclass">
                    <a href="../documents/QPS in CNN.pdf" target="_blank">QPS student interviewed by CNN</a>
                </div>
            </li>
        </ul>--%>
		<h4 class="colblu mostwrap bold">Six Sigma</h4>
		<ul class="hrefplainclass">
		<li>
            <div class="plainhrefclass">
                Six Sigma Tools &amp; Terminologies&nbsp;
                <a href="../documents/SixSigma_related_tools.pdf" target="_blank">PDF</a>&nbsp;&nbsp;
                <a href="SixSigmaTerminologies.aspx" target="_blank">HTML</a>
            </div>
        </li>
        </ul>
       <%-- <h4 class="colblu mostwrap bold">ISO 9001:2008</h4>
	    <ul class="hrefplainclass">
	        <li><a href="/documents/ISO90012008.pdf" target="_blank">Getting Ready for ISO 9001:2008</a>
	    </ul>--%>
	    <h4 class="colblu mostwrap bold">Lean</h4>
	    <ul class="hrefplainclass">
	        <li><a href="/documents/Lean_Tools.pdf" target="_blank">Lean Tools</a>
	    </ul>
	    <h4 class="colblu mostwrap bold">Lean Six Sigma</h4>
	    <ul class="hrefplainclass">
	        <li><a href="/documents/Sarkis and Roger 11-17-09.pptx" target="_blank">Six Sigma Black Belt project <%--DOE presentation--%></a>
	    </ul>
        <%--<h4 class="colblu mostwrap bold">
            CONFLICT SURVEYS
        </h4>
        <ul class="hrefplainclass">
            This survey is intended for professionals with project management experience. The
            purpose of the survey is to expand knowledge in the project management field by
            identifying best practices in the area of conflict management.<br />
            <br />
            This survey is confidential and anonymous; any data gathered will be aggregated.
            The results of this survey will be published and available on the QPS website.<br /> 
            <br />
            Thank you for taking the time to respond. <br />
               
            <a href="http://www.surveymonkey.com/s/VR2BKZS" target="_blank">www.surveymonkey.com/s/VR2BKZS
            </a>
        </ul>--%>
	</div>
	<div class="span-4">
	    <img src="../images/references-01.gif" class="pull-1" alt="QPS References">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
</asp:Content>
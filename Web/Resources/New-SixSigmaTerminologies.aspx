<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="References.aspx.cs" Inherits="QPS.Resources.References" %>

<asp:Content ID="ContentReferences" ContentPlaceHolderID="cphContent" runat="server" >
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
        <div class="span-3 blackbk last"><a>References</a></div>
		<div class="span-4 blackbl last"><a href="Links.aspx">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx">Newsletters</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">References</h3>
		<h4 class="colblu mostwrap bold">Six Sigma</h4>
        <div class="plainhrefclass">
            Six Sigma Tools &amp; Terminologies&nbsp;
            <a href="../documents/SixSigma_related_tools.pdf">PDF</a>&nbsp;&nbsp;
            <a href="SixSigmaTerminologies.aspx">HTML</a>
        </div>
        <br><br>
        <h4 class="colblu mostwrap bold">ISO 9000</h4>
        <ul class="hrefplainclass">
            <li><a href="GLOSSARY.aspx">Glossary of Key ISO 9000 Terms</a></li>
            <li><a href="../documents/ISO90011994vs2000.pdf">ISO 9001: 1994 vs. ISO 9001: 2000 What Are the Differences? </a></li>
            <li><a href="../documents/ISO9001_overview.pdf">ISO 9001 Overview</a></li>
            <li><a href="../documents/Principles_QMS.pdf">Principles of the Quality Management System</a></li>
        </ul>
	    <h4 class="colblu mostwrap bold">Lean Manufacturing</h4>
	    <ul class="hrefplainclass">
	        <li><a href="/documents/Lean_Tools.pdf">Lean Tools</a>
	    </ul>
	</div>
	<div class="span-4">
	    <img src="../images/references-01.gif" class="pull-1" alt="QPS References">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif">
	        <h3 class="mostwrap"><a href="#" class="nounder">Click here</a></h3> for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
</asp:Content>
<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Links.aspx.cs" Inherits="QPS.Resources.Links" %>
<%--<%@ Register TagPrefix="QPSSidebar" TagName="Resources" Src="~/Controls/Sidebar_Resources.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>--%>

<asp:Content ID="ContentLinks" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("Resources");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	     <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
		<div class="span-4 blackbk last"><a  href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
		<div class="span-4 blackbl last"><a href="Releases.aspx" title="Press Releases">Press Releases</a></div>
        <%--<div class="span-4 blackbl last"><a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>--%>
        <div class="span-4 blackbl last"><a href="../Placement/Aboutus.aspx" title="Jobs">Staffing & Recruitment</a></div>
         	 <%--<div class="span-3 blackbl last"><a href="../Events/Default.aspx" title="Events">Events</a></div>--%>
        <div class="span-3 blackbl last"><a href="Articals.aspx" title="Articles">Articles</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">Links</h3>
		<table cellpadding="0" cellspacing="0" style="width:100%;">
		    <tr>
		    <td style="width:80%;"> 
		    <div class="span-9 colborder" style="width:100%;">
		    <ul class="arrowedbullet">
		        <li>American Association for Lab Accreditation
			        <a href="http://www.a2la.org/">http://www.a2la.org/</a>
		        <li>American Institute of Chemical Engineers
			        <a href="http://www.aiche.org/">http://www.aiche.org/</a>
		        <li>American National Standards Institute
			        <a href="http://www.ansi.org/">http://www.ansi.org/</a>
		        <li>American Productivity & Quality Center
			        <a href="http:www.apqc.org/">http:www.apqc.org/</a>
		        <li>American Society of Mechanical Engineers
			        <a href="http://asme.org/">http://asme.org/</a>
		        <li>American Society for Nondestructive Testing
			        <a href="http://www.asnt.org/">http://www.asnt.org/</a>
		        <li>American Society for Quality
			        <a href="http://www.asq.org/">http://www.asq.org/</a>
		        <li>American Society for Testing and Materials
			        <a href="http://www.astm.org/">http://www.astm.org/</a>
		        <li>American Welding Society
			        <a href="http://www.aws.org/">http://www.aws.org/</a>
		        <li>API (American Petroleum Institute)
			        <a href="http://www.api.org/">http://www.api.org/</a>
		        <li>APICS - Educational Society for Resource Mgmt.
			        <a href="http://www.apics.org/">http://www.apics.org/</a>
		        <li>ASQ Quality Audit Division
			        <a href="http://www.asq.org/qad">http://www.asq.org/qad</a>
		        <li>Association of Information Technology Prof.
			        <a href="http://aitp.org/">http://aitp.org/</a>
		        <li>Association for Quality and Participation
			        <a href="http://www.nhmccd.edu/aqp/index.htm">http://www.nhmccd.edu/aqp/index.htm</a>
		        <li>Automotive Industry Action Group
			        <a href="http://www.aiag.org/">http://www.aiag.org/</a>
		        <li>Baldrige Quality Award
			        <a href="http://www.quality.nist.gov/">http://www.quality.nist.gov/</a>
		        <li>Center for Quality Excellence
			        <a href="http://cqe.spsu.edu/">http://cqe.spsu.edu/</a></li>
		        <li>Environmental Protection Agency
			        <a href="http://cqe.spsu.edu/">http://cqe.spsu.edu/</a>
		        <li>IEEE Computer Society
			        <a href="http://www.computer.org/">http://www.computer.org/</a>
		        <li>Industrial Research Institute
			        <a href="http://www.iriinc.org/">http://www.iriinc.org/</a>
		        <li>Institute of Electrical and Electronics Engineers
			        <a href="http://www.ieee.org/">http://www.ieee.org/</a>
		        <li>Int'l Organization for Standardization
			        <a href="http://www.iso.ch/">http://www.iso.ch/</a>
		        <li>Int'l Quality Federation
			        <a href="http://www.iqfnet.org/">http://www.iqfnet.org/</a>
		        <li>Int'l Society for Measurement and Control
			        <a href="http://www.isa.org/">http://www.isa.org/</a>
		 
		        <li>Los Alamos Quality and Planning Office
			        <a href="http://isoun.lanl.gov:2001/qp/qp.html">http://isoun.lanl.gov:2001/qp/qp.html</a>
		        <li>Medical Education and Training
			        <a href="http://www.topics-eu.com">http://www.topics-eu.com</a>
		        <li>NASA Quality Pages
			        <a href="http://akao.larc.nasa.gov/dfc/qtec.html">http://akao.larc.nasa.gov/dfc/qtec.html</a>
		        <li>NASA Technical Standards Program
			        <a href="http://standards.msfc.nasa.gov/">http://standards.msfc.nasa.gov/</a>
		        <li>National Association of Manufacturers
			        <a href="http://www.nam.org/">http://www.nam.org/</a>
		        <li>National Institute for Standards and Technology
			        <a href="http://www.nist.gov/">http://www.nist.gov/</a>
		        <li>North East Quality Council
			        <a href="http://www.NEQC.org/">http://www.NEQC.org/</a>
		        <li>Quality Assurance Institute
			        <a href="http://www.qaiusa.com/">http://www.qaiusa.com/</a>
		        <li>Quality Digest Magazine
			        <a href="http://www.qualitydigest.com/">http://www.qualitydigest.com/</a>
		        <li>Quality Magazine
			        <a href="http://www.qualitymag.com/">http://www.qualitymag.com/</a>
		        <li>Quality Management Journal
			        <a href="http://qmj.asq.org/">http://qmj.asq.org/</a>
		        <li>Quality Progress Magazine
			        <a href="http://qualityprogress.asq.org/">http://qualityprogress.asq.org/</a>
		        <li>Registrar Accreditation Board
			        <a href="http://www.rabnet.com/">http://www.rabnet.com/</a>
		        <li>i Six Sigma
			        <a href="http://www.isixsigma.com/">http://www.isixsigma.com/</a>
		        <li>Society of Automotive Engineers
			        <a href="http://www.sae.org/">http://www.sae.org/</a>
		        <li>Society for Maintenance and Reliability Prof.
			        <a href="http://www.smrp.org/">http://www.smrp.org/</a>
		        <li>Society of Manufacturing Engineers
			        <a href="http://www.sme.org/">http://www.sme.org/</a>
		        <li>Society of Reliability Engineers
			        <a href="http://www.sre.org/">http://www.sre.org/</a>
		        <li>Software Engineering Laboratory
			        <a href="http://wwwsel.iit.nrc.ca/">http://wwwsel.iit.nrc.ca/</a>
		        <li>Software Productivity Centre
			        <a href="http://www.spc.ca/">http://www.spc.ca/</a>
		        <li>Software Quality Professional
			        <a href="http://sqp.asq.org/">http://sqp.asq.org/</a>
		        <li>The Spice Group (ISO 15504)
			        <a href="http://wwwsel.iit.nrc.ca/spice">http://wwwsel.iit.nrc.ca/spice</a>
		        <li>U.S. Standards Group
			        <a href="http://standardsgroup.asq.org">http://standardsgroup.asq.org</a>
		        <li>Worcester County Business Services
			        <a href="http://www.worcester-spot.com/Massachusetts/">http://www.worcester-spot.com/Massachusetts/</a>
			        <a href="http://Worcester-County/USA/Business-Services/">http://Worcester-County/USA/Business-Services/</a>
			        <a href="http://Worcester-County-Business-Services.htm">http://Worcester-County-Business-Services.htm</a>
		    </ul>
		</div>
		        </td>
	        <td valign="bottom" style="vertical-align:bottom;width:20%;">
	            <p style="float:right;text-align:right;vertical-align:bottom;"><a class="nounder" href="#top" title="Top"><img src="../images/top.gif" title="Top"></a></p>
	        </td>
		    </tr>
		</table>		
	</div>
	<div class="span-4">
	    <img src="../images/links-01.gif" class="pull-1" alt="QPS Links">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
<%--<table width="100%">
<tr>
<td valign="top" style="background-color:#d9d9d9; width:188px">
    <QPSSidebar:Resources id="Sidebar_Resources" runat="server"/>
    <QPSSidebar:Sidebar id="Sidebar" runat="server"/>
</td>
<td>      
<table class="mainText" width='100%' ID="Table1">
    <tr class="Resources" style="HEIGHT: 2px;">
	    <td style="WIDTH: 7px;"><img src='..\Images\Clear.gif' width=7 height=2></td>
	    <td></td>
	    <td></td>
    </tr>
    <tr>
	    <td bgcolor='#1158E7' style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=5></td>
	    <td rowspan="3"><img src='../Images/resources_img.jpg'></td>
	    <td class="Resources" width="100%">Links</td>
    </tr>
    <tr>
	    <td class="ResourcesSpacer" style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=5></td>
	    <td class="ResourcesSpacer" style="padding-left: 20px; padding-right: 20px;">
	    <img src='..\Images\Clear.gif' width=5 height=65>								
	    </td>
    </tr>
    <tr bgcolor="white" style="HEIGHT: 20px;">
	    <td style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=10></td>
	    <td></td>
    </tr>
</table>   
<table class="mainText">
    <tr>
        <td>           
            <%=Link%>            
        </td>
    </tr>
</table>
</td>
</tr></table>--%>
</asp:Content>

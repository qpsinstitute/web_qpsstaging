﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QpsFdaRelated.aspx.cs" Inherits="QPS.Resources.QpsFdaRelated" MasterPageFile="~/QPSSite.Master" %>
<asp:Content ID="ContentQpsFdaRelated" ContentPlaceHolderID="cphContent" runat="server">
<div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
    <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                QPS expanding FDA Related BIOMedical/PharmaTopic Offerings and adding Food Safety!
            </h3>
            <br />
            <p style="font-size: 125%">
                <b class="colbright">February 13, 2012 –</b> <font style="font-size: 90%">
		    Jay P. Patel, President and CEO of Quality & Productivity Solutions, Inc. announced that QPS will be expanding its FDA Related BIOMedical / Pharma related topic offerings, and adding Food Safety too! <br />
           <br /></font>
            <font style="125%" type="bold"><strong>Official listing of FDA Related BIOMedical/Pharma topic offerings consist of:</strong></font>
            
            </p>
            <div style="font-size: 125%; font-style:italic" align="center">
            510(K): Medical Device Process.<br />
            Design Control: Understanding, Implementation & Improvement.<br />
            European Union (EU) Directives, European Conformity (CE) Marking, & Guidance Documentation.<br />
            Biomedical Auditor Certification (ASQ - CBA)<br />
            Pharmaceutical Certification (ASQ - CGMP)<br />
            Corrective & Preventative Action - CAPA.<br />
            GMP / ISO System Documentation.<br />
            QSIT - Quality System Inspection Technique.<br />
                <br />
            </div>
            <p style="font-size: 125%">
                <strong>Official listing of Food Safety topic offerings consist of:
            </strong>
            </p>
            <div style="font-size: 125%; font-style:italic" align="center">
            ISO 22000 Food Safety Management System<br />
            Lead Internal Auditor ISO 22000<br />
            Internal Auditor ISO 22000<br />
            HACCPHazard Analysis Critical Control Point<br />
                <br />
            </div>
            <p style="font-size: 125%">
            <a href="http://www.qpsinc.com/Training/Calendar.aspx?id=3">Click here to view dates/time of all QPS events.</a>
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

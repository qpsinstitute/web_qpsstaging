<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QPSRIMA.aspx.cs" Inherits="QPS.Resources.QPSRIMA"
    MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
            <div class="span-3 blackbl last">
                <a href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <table width="100%">
                <tr>
                    <td style="vertical-align: bottom; padding-right: 7px;" width="75%">
                        <font class="colbright" style="font-size: 1.5em">QPS � A Member of the Rhode Island Manufacturers Association</font>
                    </td>
                    <td align="left">
                        <img id="img1" src="../Images/RIMA.jpg" style="vertical-align: middle" />
                    </td>
                </tr>
            </table>
            <br />
            <p style="margin-left: 5px; font-size: 125%">
                <b class="colbright">November 1, 2010 � </b><font style="font-size: 90%">
                    QPS is now a member of the Rhode Island Manufacturer Association. According to the
                    RIMA�s website, its goal �is to serve as the unified voice of the state�s manufacturing
                    sector�, and to �educate and influence the public, the media, and government officials
                    regarding the important contribution a strong manufacturing base makes to our local,
                    national and global economies.� <a href="https://www.rimanufacturers.com/" target="_blank">Click here to read about the Rhode Island Manufacturer Association.</a> 
                    QPS looks forward to meeting members of the Rhode Island Manufacturer�s
                    Association and providing assistance to RIMA. <a href="https://www.rimanufacturers.com/TheDirectory/" target="_blank"> Click here to view QPS in the membership directory.</a> 
                </font>
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using log4net.Config;

namespace QPS.Resources
{
    public partial class NewsLetter : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected static string NewsLetters = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HtmlHead f = (HtmlHead)Master.FindControl("masterHead");
                MetaTagController.addMetatagPage(Request.Url.AbsolutePath, f);
                if (!IsPostBack)
                {
                    //DataTable dtContactUs = UserController.GetContactUs();
                    //dtContactUs.DefaultView.RowFilter = "Type ='NewsLetters'";
                    //if (dtContactUs.DefaultView.Count > 0)
                    //{
                    //    NewsLetters = dtContactUs.DefaultView[0]["Description"].ToString();
                    //}
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] NewsLetter::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
    }
}

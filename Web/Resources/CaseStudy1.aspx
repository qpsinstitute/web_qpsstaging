<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CaseStudy1.aspx.cs" Inherits="QPS.Resources.CaseStudy1" MasterPageFile="~/QPSSiteHome.Master"  %>

<asp:Content ID="ContentCaseStudy1" ContentPlaceHolderID="cphContent1" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("CaseStudy1");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	    <div class="span-4 blackbk last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a href="References.aspx" title="References">References</a></div>
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
		<div class="span-4 blackbl last"><a href="Releases.aspx" title="Press Releases">Press Releases</a></div>
		
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training" style="margin-left:300px;">Case Study 1
</h3>

<h3 class="colbright" id="H3_1" style="margin-left:250px;">Healthcare Claims Processing

</h3>
	<p>
		    <span style="font-size:125%">
		        In one case we were asked to address the claims processing process, the management thought it was taking too long to process customer claims. They thought this because they really couldn�t pin down the actual time, some people said 30 days and some said 60 days and most other department managers didn�t really know. So we put together a team and started investigating. We trained several black belts and about 50 greenbelts then pulled some of the best people to work on the project. After developing a charter, SIPOC, RACI and other define tools, we moved into the measure phase and decided to do a value stream map of the process. We started in the mail room and tracked the process all the way to sending out a check. 162 days and a value stream map about 20 feet long. WOW!<br />
		                <br />
		    </span>
		    <span style="font-size:125%">
		       Several other projects came out of this project; otherwise we could have spent a year trying to fix everything. During our investigation we stumbled upon several quick fixes, one being in the shipping of the claims from the mail facility to headquarters to another facility for screening and back to headquarters. This was done because the person doing the screening for some special type of claims, left the company 3 years ago, but we still sent the claims to this person; nobody knew what to do with them and sent them back to headquarters. For three years we had been doing this and nobody raised a flag or asked why! STOP! This was taking an average of one week, quick fix, stop doing this!<br />
		                <br />
		    </span>
		    <span style="font-size:125%">
		       After several projects addressing this process, we are down to averaging 30 days. Huge improvement, but we are continuing to address and improve this process. The methodology creates a breakthrough improvement and then we move to continuous improvement to maintain visibility and keep up the improvement approach. In the last 5 years we have averaged $5 million/year in savings. <br />
		                <br />
		    </span>
		</p>
		
	</div>
	<div class="span-4">
	    <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space"><br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
<%--<table>
<tr>
<td valign="top" style="background-color:#d9d9d9;">
    <QPSSidebar:Resources id="Sidebar_Resources" runat="server"/>
    <QPSSidebar:Sidebar id="Sidebar" runat="server"/>
</td>
<td>
<table class="mainText" width='100%' ID="Table1">
    <tr class="Resources" style="HEIGHT: 2px;">
	    <td style="WIDTH: 7px;"><img src='..\Images\Clear.gif' width=7 height=2></td>
	    <td></td>
	    <td></td>
    </tr>
    <tr>
	    <td bgcolor='#1158E7' style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=5></td>
	    <td rowspan="3"><img src="../Images/resources_img.jpg"></td>
	    <td class="Resources" width="100%">Reference Materials</td>
    </tr>
    <tr>
	    <td class="ResourcesSpacer" style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=5></td>
	    <td class="ResourcesSpacer" style="padding-left: 20px; padding-right: 20px;">
	    <img src='..\Images\Clear.gif' width=5 height=65>
	    </td>
    </tr>
    <tr bgcolor="white" style="HEIGHT: 20px;">
	    <td style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=10></td>
	    <td></td>
    </tr>
</table>
<table class="mainText">
    <tr>
        <td>
            <%=Resource%>
        </td>
    </tr>
</table>
</td>
</tr></table>--%>
</asp:Content>
<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="NewsLetter.aspx.cs" Inherits="QPS.Resources.NewsLetter" %>

<asp:Content ID="ContentNewsLetter" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("Resources");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	     
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbk last"><a title="Newsletters">Newsletters</a></div>
		<div class="span-4 blackbl last"><a href="Releases.aspx" title="Press Releases">Press Releases</a></div>
		<%--<div class="span-4 blackbl last"><a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>--%>
        <div class="span-4 blackbl last"><a href="../Placement/Aboutus.aspx" title="Jobs">Staffing & Recruitment</a></div>
		 <%--<div class="span-3 blackbl last"><a href="../Events/Default.aspx" title="Events">Events</a></div>--%>
        <div class="span-3 blackbl last"><a href="Articals.aspx" title="Articles">Articles</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">Newsletters</h3>

		<ul class="arrowedbullet">
        <li><a href="../Documents/April_2015.pdf" target="_blank" title="April 2015">April 2015</a></li>
        <li><a href="../Documents/February 2015.pdf" target="_blank" title="February 2015">February 2015</a></li>

        <%--<li><a href="../Documents/continuity-fall-2013-Printed.pdf" target="_blank" title="July 2013">October 2013</a></li>
        <li><a href="../Documents/Risk Assessment Article revised.pdf" target="_blank" title="July 2013">September 2013</a></li>
        <li><a href="../Documents/Newsletter July 2013 revised.pdf" target="_blank" title="July 2013">July 2013</a></li>

        <li><a href="../Documents/112012 newsletter.pdf" target="_blank" title="November 2012">November 2012</a></li>
        <li><a href="../Documents/QPS Newsletter Feb 2012.pdf" target="_blank" title="February 2012">February 2012</a></li>
		    <li><a href="../Documents/June 2011 Semiannual Focus.pdf" target="_blank" title="June 2011">June 2011</a></li>
            <li><a href="../Documents/2010 October Newsletter QPS.pdf" target="_blank" title="October 2010">October 2010</a></li>
		    <li><a href="../Documents/Newsletter July 2010 official.pdf" target="_blank" title="July 2010">July 2010</a></li>
			<li><a href="../documents/QPS Newsletter April 2010.pdf" target="_blank" title="April 2010">April 2010</a></li>
            <li><a href="../documents/QPS Newsletter January 2010.pdf" target="_blank" title="January 2010">January 2010</a></li>--%>
		</ul>
		<p>
		    The newsletter is currently published in Adobe Acrobat .pdf format.
		    If you can't view this type of file, a free reader can be obtained by clicking
		    <a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank" class="nounder bold" title="Click here for Download Acrobat reader" >here</a>.
		</p>
	</div>
	<div class="span-4">
	    <img src="../images/newsletters-01.gif" class="pull-1" alt="QPS Newsletters">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>

</asp:Content>

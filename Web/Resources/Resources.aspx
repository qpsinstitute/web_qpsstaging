<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Resources.aspx.cs" Inherits="QPS.Resources.Resources" %>
<%--<%@ Register TagPrefix="QPSSidebar" TagName="Resources" Src="~/Controls/Sidebar_Resources.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>--%>

<asp:Content ID="ContentResources" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("Resources");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	    <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a href="References.aspx" title="References">References</a></div>--%>
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
		<div class="span-4 blackbl last"><a href="Releases.aspx" title="Press Releases">Press Releases</a></div>
        <div class="span-4 blackbl last"><a href="../Placement/Aboutus.aspx" title="Jobs">Staffing & Recruitment</a></div>
        <div class="span-3 blackbl last"><a href="Articals.aspx" title="Articles">Articles</a></div>
		<%--<div class="span-4 blackbl last"><a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>--%>
			 <%--<div class="span-3 blackbl last"><a href="../Events/Default.aspx" title="Events">Events</a></div>--%>
		
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">Resources</h3>
		<%--<p>
		    <span style="font-size:125%">
		        Find all that you want to get updated and learned with Tools, Terminologies, Glossary, Links, Newsletters on your desired topics and requirements.
		        You can find handy material and some interesting terminology, some exlusive information on Six Sigma, ISO 9000, Quality System, Lean Tools and many other topics.
		    </span>
		</p>--%>
		<br class="clear">
		<%--<div style="width:180px; float:left">
		    <a href="References.aspx" title="References" class="nounder"><img src="../images/references-02.gif" border=0 alt="QPS References"></a>
		</div>--%>
		<div style="width:180px; float:left">
		    <a href="Links.aspx" title="Links" class="nounder"><img src="../images/links-02.gif" border=0 alt="QPS Links"></a>
		</div>
		<div style="width:180px; float:left">
		    <a href="NewsLetter.aspx" title="Newsletters" class="nounder"><img src="../images/newsletters-02.gif" border=0 alt="QPS Newsletters"></a>
		</div>
		<div style="width:180px; float:left">
		    <a href="Releases.aspx" title="Press Releases" class="nounder"><img src="../Images/press-release.gif" border=0 alt="QPS Newsletters"></a>
		</div>
        <div style="width:180px; float:left">
		    <a href="Articals.aspx" title="Articles" class="nounder"><img src="../Images/articles.png" border=0 alt="Articles"></a>
		</div>
		
	</div>
	<div class="span-4">
	    <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space"><br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
<%--<table>
<tr>
<td valign="top" style="background-color:#d9d9d9;">
    <QPSSidebar:Resources id="Sidebar_Resources" runat="server"/>
    <QPSSidebar:Sidebar id="Sidebar" runat="server"/>
</td>
<td>
<table class="mainText" width='100%' ID="Table1">
    <tr class="Resources" style="HEIGHT: 2px;">
	    <td style="WIDTH: 7px;"><img src='..\Images\Clear.gif' width=7 height=2></td>
	    <td></td>
	    <td></td>
    </tr>
    <tr>
	    <td bgcolor='#1158E7' style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=5></td>
	    <td rowspan="3"><img src="../Images/resources_img.jpg"></td>
	    <td class="Resources" width="100%">Reference Materials</td>
    </tr>
    <tr>
	    <td class="ResourcesSpacer" style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=5></td>
	    <td class="ResourcesSpacer" style="padding-left: 20px; padding-right: 20px;">
	    <img src='..\Images\Clear.gif' width=5 height=65>
	    </td>
    </tr>
    <tr bgcolor="white" style="HEIGHT: 20px;">
	    <td style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=10></td>
	    <td></td>
    </tr>
</table>
<table class="mainText">
    <tr>
        <td>
            <%=Resource%>
        </td>
    </tr>
</table>
</td>
</tr></table>--%>
</asp:Content>
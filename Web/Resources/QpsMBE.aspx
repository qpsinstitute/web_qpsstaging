<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="QpsMBE.aspx.cs"
    Inherits="QPS.Resources.QpsMBE" %>

<asp:Content ID="QpsMBE" ContentPlaceHolderID="cphContent" runat="server">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
    <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <table width="100%">
                <tr>
                    <td style="vertical-align: bottom" width="32%">
                        <font style="font-size: 1.5em" class="colbright">QPS Now a Certified MBE!</font>
                    </td>
                    <td align="left">
                        <img src="../Images/certi.PNG" id="img1" style="vertical-align: middle" />
                    </td>
                </tr>
            </table>
            <p style="font-size: 125%; padding-left: 4px;">
                <b class="colbright">July 14, 2010 �</b> <font style="font-size: 90%">
		    QPS is now a certified MBE, otherwise known as a Minority-owned Business Enterprise, with the certified business description <font style="font-style:italic">Providing Practical and Effective Consulting and Training Support in Improvement of Product, Service, Cost Performance or Systems.</font> QPS is listed in the Supplier Diversity Office Directory of the Massachusetts Central Register. <a href="http://www.somwba.state.ma.us/BusinessDirectory/BusinessDirectoryList.aspx?BusinessTypeID=1&CertTypeMBE=True&CertTypeWBE=False&CertTypePBE=False&CertTypeDBE=False&IndustryType=0&Keywords=quality&ProductNAICS=0&SortedBy=Business_Name&SortedByName=Company+Name&SearchType=SOMWBA&CityOrTown=&RegionNumber=0&ZipCode=" target="_blank">Click here to view QPS�s description.</a> According to former Governor Argeo Paul Cellucci, �minority-and women-owned business provide ever-increasing value to our state's economy�by facilitating easier access to the various phases of state purchasing and contracting, SOMWBA is providing a valuable jumpstart to many of these business(es) which make up our state's fastest-growing business sector.�  Formally known as SOMWBA, the State Office of Minority and Women�s Business Assistance is a professionally recognized entity that has contributed assistance in successful businesses throughout the years. <a href="http://www.somwba.state.ma.us/Content/about/about.aspx" target="_blank">Click here to read about SOMWBA.</a> 
            </font>
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

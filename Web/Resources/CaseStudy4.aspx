<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CaseStudy4.aspx.cs" Inherits="QPS.Resources.CaseStudy4"  MasterPageFile="~/QPSSiteHome.Master" %>

<asp:Content ID="ContentCaseStudy4" ContentPlaceHolderID="cphContent1" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("CaseStudy4");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	    <div class="span-4 blackbk last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a href="References.aspx" title="References">References</a></div>
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
		<div class="span-4 blackbl last"><a href="Releases.aspx" title="Press Releases">Press Releases</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training" style="margin-left:310px;">Case Study 4
</h3>

<h3 class="colbright" id="H3_1" style="margin-left:300px;">Service Industry
</h3>
	<p>
		    <span style="font-size:125%">
		        In a service company that mailed out brochures to their customers discovered that many of the brochures that were mailed out by the marketing department were being returned. This was costing the company $1.00 for every returned piece of mail, it was sent out under bulk rate cost which was very small. Some was forwarded by the post office to the customer�s new address, but after some period of time they stopped forwarding and returned to sender. Nobody ever discovered that a major portion of all the mail was being returned until we found a large box in the receiving area. �What is this box of returned mail?� I asked the guy down in receiving. He replied, �I don�t know, the post office drops them off every Friday and I have asked people what we need to do with them and they said throw them away at the end of the month.�
            <br />            <br />
		    </span>
		    <span style="font-size:125%">
		      This may sound a little crazy, but is very common in a large organization. These boxes represent thousands of dollars and at the end of the year could be worth millions. With 4 million customers, we produce a lot of mail. This problem led us to find out where marketing was getting the addresses to send out this mail. It turns out they had created their own database, because nobody in marketing had authorization to log onto the main database. To solve this problem, they created their own database and every couple of months they would request IT to update the database from the main database. Well this is the first problem; their database is old and not being updated often enough. The second problem was when we went to talk to the people running the main database and asking them about address changes, we discovered a 3 month backlog in their data entry update process. 
            <br />            <br />
		    </span>
		    <span style="font-size:125%">
		      Priority, fix the main process first and then address the marketing address needs. A third party was hired to perform a massive database update. Not only were we three months behind, nobody was very confident of the data that was in the database. The third party called every person in the database and updated the entire database, including the backlog. 30 days later we had a 100% updated database. Now address the marketing database needs, give them access to the main database with read only capability. Now if they want to mail out a brochure, they download all the latest customer addresses and mail out the brochures. A 97% reduction was seen in returned mail. Awesome! 
            <br />          
		    </span>
		</p>
		
	</div>
	<div class="span-4">
	    <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space"><br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
<%--<table>
<tr>
<td valign="top" style="background-color:#d9d9d9;">
    <QPSSidebar:Resources id="Sidebar_Resources" runat="server"/>
    <QPSSidebar:Sidebar id="Sidebar" runat="server"/>
</td>
<td>
<table class="mainText" width='100%' ID="Table1">
    <tr class="Resources" style="HEIGHT: 2px;">
	    <td style="WIDTH: 7px;"><img src='..\Images\Clear.gif' width=7 height=2></td>
	    <td></td>
	    <td></td>
    </tr>
    <tr>
	    <td bgcolor='#1158E7' style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=5></td>
	    <td rowspan="3"><img src="../Images/resources_img.jpg"></td>
	    <td class="Resources" width="100%">Reference Materials</td>
    </tr>
    <tr>
	    <td class="ResourcesSpacer" style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=5></td>
	    <td class="ResourcesSpacer" style="padding-left: 20px; padding-right: 20px;">
	    <img src='..\Images\Clear.gif' width=5 height=65>
	    </td>
    </tr>
    <tr bgcolor="white" style="HEIGHT: 20px;">
	    <td style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=10></td>
	    <td></td>
    </tr>
</table>
<table class="mainText">
    <tr>
        <td>
            <%=Resource%>
        </td>
    </tr>
</table>
</td>
</tr></table>--%>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="NewsLetter.aspx.cs" Inherits="QPS.Resources.NewsLetter" %>
<%--<%@ Register TagPrefix="QPSSidebar" TagName="Resources" Src="~/Controls/Sidebar_Resources.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>--%>

<asp:Content ID="ContentNewsLetter" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("Resources");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	     <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a  href="References.aspx"  title="References">References</a></div>--%>
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbk last"><a title="Newsletters">Newsletters</a></div>
		<div class="span-4 blackbl last"><a href="Releases.aspx" title="Press Releases">Press Releases</a></div>
		<div class="span-4 blackbl last"><a href="../Jobs/JobPosting.aspx" title="Job">Job</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">Newsletters</h3>
		<ul class="arrowedbullet">
            <li><a href="../Documents/2010 October Newsletter QPS.pdf" target="_blank" title="October 2010">October 2010</a></li>
		    <li><a href="../Documents/Newsletter July 2010 official.pdf" target="_blank" title="July 2010">July 2010</a></li>
			<li><a href="../documents/QPS Newsletter April 2010.pdf" target="_blank" title="April 2010">April 2010</a></li>
            <li><a href="../documents/QPS Newsletter January 2010.pdf" target="_blank" title="January 2010">January 2010</a></li>
		</ul>
		<p>
		    The newsletter is currently published in Adobe Acrobat .pdf format.
		    If you can't view this type of file, a free reader can be obtained by clicking
		    <a href="http://www.adobe.com/products/acrobat/readstep2.html" class="nounder bold" title="Click here for Download Acrobat reader" >here</a>.
		</p>
	</div>
	<div class="span-4">
	    <img src="../images/newsletters-01.gif" class="pull-1" alt="QPS Newsletters">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
<%--<table>
<tr>
<td valign="top" style="background-color:#d9d9d9;">
    <QPSSidebar:Resources id="Sidebar_Resources" runat="server"/>
    <QPSSidebar:Sidebar id="Sidebar" runat="server"/>
</td>
<td>
<table class="mainText" width='100%' ID="Table1">
    <tr class="Resources" style="HEIGHT: 2px;">
	    <td style="WIDTH: 7px;"><img src='..\Images\Clear.gif' width="7" height="2"></td>
	    <td></td>
	    <td></td>
    </tr>
    <tr>
	    <td bgcolor='#1158E7' style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width="5" height="5"></td>
	    <td rowspan="3"><img src='../Images/resources_img.jpg'></td>
	    <td class="Resources" width="100%">Newsletters</td>
    </tr>
    <tr>
	    <td class="ResourcesSpacer" style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width="5" height="5"></td>
	    <td class="ResourcesSpacer" style="padding-left: 20px; padding-right: 20px;">
		    <img src='..\Images\Clear.gif' width="5" height="65">
	    </td>
    </tr>
    <tr bgcolor="white" style="HEIGHT: 20px;">
	    <td style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width="5" height="10"></td>
	    <td></td>
    </tr>
</table>
<table class="mainText">
    <tr>
        <td>           
            <%=NewsLetters%>            
        </td>
    </tr>
</table>
</td>
</tr></table>--%>
</asp:Content>

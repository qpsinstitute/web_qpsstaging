<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrainingIl.aspx.cs" Inherits="QPS.Resources.TrainingIl" MasterPageFile="~/QPSSite.Master" %>
<asp:Content ID="ContentTrainingIl" ContentPlaceHolderID="cphContent" runat="server">

<div class="span-24 prepend-1 top highcont">
    <div class="span-19 top" style="float:right">
        <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>
		<div class="span-4 blackbl last"><a  href ="links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
	    <div class="span-4 blackbk last"><a title="Press Releases">Press Releases</a></div>
        <br class="clear"><br class="clear">
		<h3 class="colbright" id="training" >
		    QPS � Coming to Chicago � Newly Approved Training Provider in Illinois!    
		</h3>
		<br />
		<p style="font-size: 125%">
		    <b class="colbright">Chicago, I.L. � June 3, 2010  �</b>
		    <font style="font-size:90%">
		     Since the April 2010 Quarterly Newsletter, QPS has decided to continue its local expansion project to obtain Training Provider approval and make out of state training available to out of state clientele. This initiative has met with great success! For years, QPS has provided corporate training all over the globe, and has offered public training for unemployed personnel in the state of Massachusetts. Earlier this year, QPS became an approved Training Provider in the states of Rhode Island, Connecticut and North Carolina, providing public training for unemployed personnel through the NetworkRI, CTWorks, and NCJobLink, the states� local One-Stop Career Center systems.  Effective today, QPS gained approval as a Training Provider in the state of Illinois.<br />
            As an approved training provider in Illinois, QPS gives the unemployed an opportunity to obtain funding from state and federal entities to pay for Lean, Six Sigma, Project Management, and other well-known programs and courses.  A total of fourteen programs were approved, and of those fourteen programs, close to 75% of them are programs that offer a minimum of two certificates. <a target="_blank" href="../Documents/IL Unemployed  Course Matrix July 7 2010.pdf">Click here to view QPS�s IL approved training.</a><br />
            Interested candidates can easily call their <a target="_blank" href="../Documents/IL One Stop Career Centers List.pdf">Illinois One-Stop Career Center</a> and make an appointment with their career counselor to start the application process.  QPS conducts training at its own facilities in Chicago, both of which are located in Cook County. <a target="_blank" href="http://www.qpsinc.com/Company/Contact.aspx">Click here to view the Park Ridge and Schaumburg locations of QPS training in Chicago. </a> 
            </font> 
		</p>
		<div></div>
		<p style="font-size: 125%"><b class="colbright">Contact</b><br />
        Jay Patel<br />
        President & CEO<br />
        Toll Free: 1-877-987-3801<br/>
        <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
        </p>
    </div>
    
    <div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>

</asp:Content>

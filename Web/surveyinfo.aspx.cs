﻿using System;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lib.BusinessLogic;
using log4net;
using Lib.Businesslogic;
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Xml.XPath;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections;
using System.Net.Mail;
using System.Net;

namespace QPS
{
    
    public partial class surveyinfo : System.Web.UI.Page
    {
        
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static DataTable dtcity = null;
        public static DataTable dtstate = null;
        public static DataTable dtinfo = null;
        DataRow drinfo = null;
        public string WClause = "";
        protected string ServerQualifiedPath = "";
        int year, month;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack == true)
            {

                getCourses();
                BindLists();
                lblmsg.Text = "";

                //rbtusertype.SelectedValue = null;
                rdbCourseType.SelectedValue = null;
                rbtComments.SelectedValue = null;

                if (Request.QueryString["SurveyId"] != "" && Request.QueryString["SurveyId"] != null)
                {
                }
                if (rbtusertype.SelectedItem != null)
                {
                    if (rbtusertype.SelectedItem.ToString() == "Client")
                    {
                        tremployer.Visible = true;
                    }
                    else
                    {
                        tremployer.Visible = false;                        
                    }             
                }
            }          
        }
        
        private void getCourses()
        {
           
            int CourseType = 0;
            DataTable dt = new DataTable();
            dt = (DataTable)CourseController.GetAllCourse_elearn();
            DataView dv = new DataView();
            dv = dt.DefaultView;

            if (rdbCourseType.SelectedValue == "1")
            {
                CourseType = 1;
                dv.RowFilter = "CourseTypeID = 1";
            }
            else
            {
                CourseType = 2;
                dv.RowFilter = "CourseTypeID = 2";
            }
            dt = dv.Table;
           
           // ddlcoursename.DataSource = CourseController.GetAllCourse_elearn();
            ddlcoursename.DataSource = dt;
            ddlcoursename.DataTextField = "CourseTitle";
            ddlcoursename.DataValueField = "CourseID";
            
            
            ddlcoursename.DataBind();
            ddlcoursename.Items.Add(new ListItem("- Select Course -", "0"));
            ddlcoursename.SelectedValue = "0";

        }
        protected void rdbCourseType_OnSelectedIndexChanged(object sender, EventArgs e)
        {
           // getCourses();
        }

        protected void rbtusertype_OnSelectedIndexChanged(object sender, EventArgs e)
        {           
            if (rbtusertype.SelectedValue.ToString() == "Client")
            {
                //tremployer.Visible = true;                            
            }
            else
            {
                //tremployer.Visible = false;                                            
            }            

            displayemp();           
        }
       
        public void displayemp()
        {
            if (rbtusertype.SelectedValue.ToString() == "Client")
            {
                tremployer.Visible = true;
            }
            else
            {
                tremployer.Visible = false;                
            }
        }

        protected void BindLists()
        {
            ListItem l;
            for (int year =2007; year <= DateTime.Now.Year ; year++)
            {
                l = new ListItem(year.ToString(), year.ToString());
                ddlsessionyear.Items.Add(l);
            }

            for (int month = 1; month <= 12; month++)
            {
                string month_name = "";

                

                if (month <= 9)
                {
                    l = new ListItem(month_name + " 0" + month.ToString(), month.ToString());
                    ddlsessionmonth.Items.Add(l);
                }
                else
                {
                    l = new ListItem(month_name + " " + month.ToString(), month.ToString());
                    ddlsessionmonth.Items.Add(l);

                }
            }
            for (int day = 1; day < 32; day++)
            {
                l = new ListItem(day.ToString(), day.ToString());
                ddlsessionday.Items.Add(l);

            }
          
            

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
           
                WClause = "AND SurveyId =" + 0;
                dtinfo = Lib.businesslogic.SurveyController.GetSurveyInfoByWClause(WClause);

                drinfo = dtinfo.NewRow();
                drinfo["Name"] = txtname.Text.ToString().Trim();
                drinfo["Email"] = txtemail.Text.ToString().Trim();
                drinfo["Phone"] = txtphone.Text.ToString().Trim();
                drinfo["status"] = "true";

                if (rbtusertype.SelectedValue != null)
                {
                    if (rbtusertype.SelectedValue.ToString() == "Client")
                    {
                        drinfo["Employer"] = txtEmployer.Text.ToString().Trim();
                    }
                    else
                    {
                        drinfo["Employer"] = null;
                    }
                }
                

                drinfo["Address"] = txtaddress.Text.ToString().Trim();
                drinfo["Address2"] = txtaddress2.Text.ToString().Trim();
                drinfo["City"] = txtCity.Text.ToString().Trim();
                drinfo["Likes"] = txtwhatdidulike.Text.ToString().Trim();
                drinfo["AreaImprovement"] = txtImpprovements.Text.ToString().Trim();

                if (rdbCourseType.SelectedValue == "1")
                    drinfo["CourseType"] = "1";
                else
                    drinfo["CourseType"] = "2";

                if (rbtComments.SelectedValue == "0")
                    drinfo["CommentStatus"] = "false";
                else
                    drinfo["CommentStatus"] = "true";

                if (rbtusertype.SelectedValue == "Student")
                    drinfo["UserType"] = "Student";
                else
                    drinfo["UserType"] = "Client";

                if (ddlState.SelectedItem.Text.ToString() == "Select State")
                {
                    drinfo["state"] = DBNull.Value;                    
                }
                else
                    drinfo["state"] = ddlState.SelectedItem.Text;               


                //if (ddlcoursename.SelectedItem.Text.ToString() == "- Select Course -")
                //    drinfo["CourseId"] = DBNull.Value;
                //else
                //    drinfo["CourseId"] = ddlcoursename.SelectedValue;

                if (txtcoursename != null && txtcoursename.Text.ToString() != "")
                {
                    drinfo["CourseName"] = txtcoursename.Text.ToString();
                }


                if (ddlsessionyear.SelectedIndex > 0 && ddlsessionmonth.SelectedIndex > 0 && ddlsessionday.SelectedIndex > 0)
                {
                    DateTime dateValue;
                    if ((ddlsessionmonth.SelectedIndex > 0 || ddlsessionmonth.SelectedIndex > 0))
                    {
                        dateValue = new DateTime(Int32.Parse(ddlsessionyear.SelectedValue.ToString()), Int32.Parse(ddlsessionmonth.SelectedValue.ToString()), Int32.Parse(ddlsessionday.SelectedValue.ToString()));
                    }

                    {
                        dateValue = new DateTime(Int32.Parse(ddlsessionyear.SelectedValue.ToString()), Int32.Parse(ddlsessionmonth.SelectedValue.ToString()), Int32.Parse(ddlsessionday.SelectedValue.ToString()), 0, 0, 0);
                    }

                    drinfo["Date"] = dateValue.ToString();
                }
                else
                {
                    drinfo["Date"] = DBNull.Value;
                }


                dtinfo.Rows.Add(drinfo);
                Lib.businesslogic.SurveyController.UpdateSurvey(dtinfo);
                btnReset_Click(sender, e);
                lblmsg.Text = "Information has been saved successfully.";

                txtcoursename.Text = "";

                //DataSet ds = new DataSet();

                //ds.Tables.Add(dtinfo);
                //XmlElement xE = (XmlElement)Serialize(dtinfo);
                //string strXml = xE.OuterXml.ToString();
                //XmlDocument xDoc = new XmlDocument();
                //xDoc.LoadXml(strXml);
                //xDoc.Save(Server.MapPath("XML\\myFile.xml"));
               // SendEmail(xDoc);

                


        }

        #region Serialize given object into stream.
        //public XmlElement Serialize(object transformObject)
        //{
        //    XmlElement serializedElement = null;
        //    try
        //    {
        //        MemoryStream memStream = new MemoryStream();
        //        XmlSerializer serializer = new XmlSerializer(transformObject.GetType());
        //        serializer.Serialize(memStream, transformObject);
        //        memStream.Position = 0;
        //        XmlDocument xmlDoc = new XmlDocument();
        //        xmlDoc.Load(memStream);
        //        serializedElement = xmlDoc.DocumentElement;
        //    }
        //    catch (Exception SerializeException)
        //    {

        //    }
        //    return serializedElement;
        //}
        #endregion // End - Serialize given object into stream.
        //private void SendEmail(XmlDocument xmlDoc)
        //{
        //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //    XslCompiledTransform xslt = new XslCompiledTransform();
        //    xslt.Load(Server.MapPath("XML/email.xsl"));
        //    using (XmlTextWriter xtw = new XmlTextWriter(new StringWriter(sb)))
        //    {
        //        xslt.Transform(xmlDoc, xtw);
        //        xtw.Flush();
        //    }
        //    SmtpClient oMail = new SmtpClient();
        //    MailMessage msg = new MailMessage();
        //    try
        //    {
        //        MailAddress Madd = new MailAddress("snldvr9@gmail.com", "sunil.devre@perpetuating.com");
        //        oMail.Host = "smtp.gmail.com";
        //        oMail.Port = 587;
        //        oMail.EnableSsl = true;
        //        oMail.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
        //        oMail.Credentials = new NetworkCredential("snldvr9@gmail.com", "suneeldg");
        //        oMail.Timeout = 20000;
        //        msg.From = Madd;
                
        //        msg.To.Add("sunil.devre@perpetuating.com");
        //        msg.Subject = "TEST";
        //        msg.IsBodyHtml = true;
        //        msg.Body = sb.ToString();
        //        oMail.Send(msg);
        //        Server.Transfer("sent.aspx");
        //    }
        //    catch (Exception ex)
        //    {
        //        //This step is not good practice but seen as though this is just a Demo
        //        //app I thought I would place this here so we can see if any errors occur
               
        //    }
        //}
        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtname.Text = txtemail.Text = txtphone.Text = lblmsg.Text = txtEmployer.Text = txtaddress.Text = txtaddress2.Text = txtCity.Text = txtwhatdidulike.Text = txtImpprovements.Text = txtcoursename.Text = "";
            ddlcoursename.SelectedValue = "0";
            ddlsessionday.SelectedIndex = -1;
            ddlsessionmonth.SelectedIndex = -1;
            ddlsessionyear.SelectedIndex = -1;
            ddlState.SelectedIndex = -1;

            rbtusertype.SelectedValue = null;
            rdbCourseType.SelectedValue = null;
            rbtComments.SelectedValue = null;
        }

        
    }
}
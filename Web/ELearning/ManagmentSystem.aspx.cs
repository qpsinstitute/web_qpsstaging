using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Lib.BusinessLogic;

namespace QPS.ELearning
{
    public partial class ManagmentSystem : System.Web.UI.Page
    {
        //protected string FormSubmitURL = string.Empty;
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    FormSubmitURL = ConfigurationSettings.AppSettings["ElearningURL"];
        //}

        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected static string EcourseLists = "";
        protected string FormSubmitURL = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HtmlHead f = (HtmlHead)Master.FindControl("masterHead");
                MetaTagController.addMetatagPage(Request.Url.AbsolutePath, f);
                FormSubmitURL = ConfigurationSettings.AppSettings["ElearningURL"];
                if (!IsPostBack)
                {
                    DataTable dtContactUs = UserController.GetContactUs();
                    dtContactUs.DefaultView.RowFilter = "Type ='EcourseList'";
                    if (dtContactUs.DefaultView.Count > 0)
                    {
                        EcourseLists = dtContactUs.DefaultView[0]["Description"].ToString();
                    }
                    BindCourseList();
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] EcourseList::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        protected void BindCourseList()
        {
            DataTable dteCourseList = eLearnCategoryController.Get_eCourse_By_CategoryID_TypeID(2, 1);
            rpteCourseList.DataSource = dteCourseList;
            rpteCourseList.DataBind();
        }
    }
}

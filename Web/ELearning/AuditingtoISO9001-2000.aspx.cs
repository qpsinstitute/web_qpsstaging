using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using log4net.Config;

namespace QPS.ELearning
{
    public partial class AuditingtoISO9001_2000 : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected string FormSubmitURL = string.Empty;
        protected static string AuditingISO90012000 = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                FormSubmitURL = "http://online.qpsinc.com/youraccount/CartPage.aspx";
                HtmlHead f = (HtmlHead)Master.FindControl("masterHead");
                MetaTagController.addMetatagPage(Request.Url.AbsolutePath, f);
                if (!IsPostBack)
                {
                    DataTable dtContactUs = UserController.GetContactUs();
                    dtContactUs.DefaultView.RowFilter = "Type ='AuditingISO9001-2000'";
                    if (dtContactUs.DefaultView.Count > 0)
                    {
                        AuditingISO90012000 = dtContactUs.DefaultView[0]["Description"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] AuditingtoISO9001_2000::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            } 
        }
    }
}

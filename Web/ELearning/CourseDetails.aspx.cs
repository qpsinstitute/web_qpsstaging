using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using log4net.Config;

namespace QPS.ELearning
{
    public partial class CourseDetails : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string Title, Code;
        public float Cost;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                    BindDetails();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] CourseDetails::Page_Load - ", ex);
               // Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void BindDetails()
        {
            if(Request.QueryString["CourseID"]!= "" && Request.QueryString["CourseID"]!= null)
            {
                
                int CourseID = Convert.ToInt32(Request.QueryString["CourseID"].ToString());
                DataTable dtCourse = eLearnCategoryController.Select_Course_By_CourseID(CourseID);
                rptCourse.DataSource = dtCourse;
                rptCourse.DataBind();
                foreach (RepeaterItem item in rptCourse.Items)
                {
                    if (dtCourse.Rows[0]["CourseDesc"].ToString() != "")
                        item.FindControl("CourseDesc").Visible = true;
                    if (dtCourse.Rows[0]["CourseBenifit"].ToString() != "")
                        item.FindControl("CourseBenifits").Visible = true;
                    if (dtCourse.Rows[0]["CostDesc"].ToString() != "")
                        item.FindControl("CostDescription").Visible = true;
                    if (dtCourse.Rows[0]["TopicsCovered"].ToString() != "")
                        item.FindControl("topics").Visible = true;
                    if (dtCourse.Rows[0]["MoreInfo"].ToString() != "")
                        item.FindControl("MoreInfo").Visible = true;
                    if (dtCourse.Rows[0]["Instructor"].ToString() != "")
                        item.FindControl("Instructor").Visible = true;
                    if (dtCourse.Rows[0]["AttendDetail"].ToString() != "")
                        item.FindControl("Attend").Visible = true;
                    if (dtCourse.Rows[0]["Prerequisites"].ToString() != "")
                        item.FindControl("Prerequisites").Visible = true;
                    if (dtCourse.Rows[0]["CertificationReq"].ToString() != "")
                        item.FindControl("Certification").Visible = true;
                }

                CostID.InnerHtml = "Cost: $" + dtCourse.Rows[0]["Cost"].ToString();
                Code = dtCourse.Rows[0]["CourseCode"].ToString();
                Title = dtCourse.Rows[0]["CourseTitle"].ToString();
                Cost = float.Parse(dtCourse.Rows[0]["Cost"].ToString());
            }
        }
    }
}

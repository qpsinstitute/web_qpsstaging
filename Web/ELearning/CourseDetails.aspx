<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" Codebehind="CourseDetails.aspx.cs"
    Inherits="QPS.ELearning.CourseDetails" %>

<%@ Register TagPrefix="QPSSidebar" TagName="ELearning" Src="~/Controls/Sidebar_ELearning.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>
<asp:Content ID="ContentSixSigmaOverview" ContentPlaceHolderID="cphContent" runat="server">
    <%--<script language="javascript" type="text/javascript">
    setPage("eLearning");
</script>--%>

    <script type="text/javascript">
    function SetVal()
    {
        var Code = document.getElementById("hdnPassCode").value;
        var Title = document.getElementById("hdnPassTitle").value;
        var Cost = document.getElementById("hdnPassCost").value;
        CheckOut(Code, Title, Cost);
        //alert( Code + ' ' + Title + ' ' + Cost);
    }
    </script>

    <div class="span-24 prepend-1 top highcont">
        <div class="span-5" style="float: right; margin-right: 0px;">
            <form action="https://online.qpsinc.com/youraccount/RegistrationSignIn.aspx" method="post"
                id="FormCheckOut" name="FormCheckOut">
                <table id="Table3" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <!-- Carttable -->
                    <tbody>
                        <tr>
                            <td style="height: 6">
                            </td>
                        </tr>
                        <tr style="height: 25">
                            <td valign="bottom" style="padding-top: 200px; padding-bottom: 1px">
                                <img src="/Images/ImagesBuyNow.gif" align="absbottom" alt="" />
                            </td>
                        </tr>
                        <tr style="height: 40">
                            <!-- 61 -->
                            <td valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 10px solid silver;">
                                    <!-- Cart Content -->
                                    <tbody>
                                        <tr>
                                            <td bgcolor="#ffffff" width="10">
                                            </td>
                                            <td valign="top" width="141">
                                                <table id="Table5" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td id="CostID" runat="server" class="CartContent" valign="middle" style="padding-top: 5px;
                                                                padding-bottom: 20px; width: 105px;">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="bottom" style="width: 105px">
                                                                <a href="#" id="Cart" onclick="javascript:SetVal()">
                                                                    <img alt="Add to cart" src="/Images/addtocart.gif" /></a>
                                                                <%-- <input id="Cart" runat="server" src="/Images/addtocart.gif" onclick="SetVal()" type="image"/>--%>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td bgcolor="#ffffff" width="10">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr style="height: 25">
                            <!-- space -->
                            <td>
                                &nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <input id="hdnCourseID" name="courseId" type="hidden" />
                <input id="hdnCost" name="cost" type="hidden" />
                <input id="hdnTitle" name="courseTitle" type="hidden" />
                <input id="hdnPassCode" value="<%= Code %>" name="hdnPassCode" type="hidden" />
                <input id="hdnPassCost" value="<%= Cost %>" name="hdnPassCost" type="hidden" />
                <input id="hdnPassTitle" value="<%= Title %>" name="hdnPassTitle" type="hidden" />
            </form>
        </div>
        <asp:Repeater ID="rptCourse" runat="server">
            <ItemTemplate>
                <div class="span-14 top" style="float: right; margin-right: 0px;">
                    <table class="mainText" width='100%'>
                        <tr>
                            <td>
                                <h3 class="colbright" id="training" style="padding-top: 10px;">
                                    eLearning</h3>
                                <p>
                                    <span style="font-size: 125%">Learn today�s hottest topics at your own pace and without
                                        taking time away from your job. At work or at home, you can now learn what others
                                        are learning and not have to worry about your job. You will have three months to
                                        complete the program in your free time. Enhance your career by clicking on one of
                                        the courses and follow the instructions. </span>
                                </p>
                                <h3 class="colbright" id="H3_1">
                                    <%#Eval("CourseTitle") %>
                                </h3>
                                <h5 class="colblu">
                                    E Course No.
                                    <%#Eval("CourseNo") %>
                                </h5>
                                <div id="CourseDesc" runat="server" visible="false">
                                    <p>
                                        <%#Eval("CourseDesc") %>
                                    </p>
                                </div>
                                <div id="CourseBenifits" runat="server" visible="false">
                                    <h5 class="colblu">
                                        Course Benefits</h5>
                                    <p>
                                        <%#Eval("CourseBenifit") %>
                                    </p>
                                </div>
                                <div id="Prerequisites" runat="server" visible="false">
                                    <h5 class="colblu">
                                        Prerequisites</h5>
                                    <p>
                                        <%#Eval("Prerequisites")%>
                                    </p>
                                </div>
                                <div id="Certification" runat="server" visible="false">
                                    <h5 class="colblu">
                                        Certification Requirements
                                    </h5>
                                    <p>
                                        <%#Eval("CertificationReq")%>
                                    </p>
                                </div>
                                <h5 class="colblu">
                                    Cost</h5>
                                <p>
                                    <div id="CostDescription" runat="server">
                                        $<%#Eval("Cost") %>
                                        <%#Eval("CostDesc").ToString().Length > 0 ? "- " + Eval("CostDesc") : "" %>
                                    </div>
                                    <br />
                                </p>
                                <div id="topics" runat="server" visible="false">
                                    <h5 class="colblu">
                                        Course Topics:</h5>
                                    <p>
                                        <div style="padding-left: 10PX;">
                                            <%#Eval("TopicsCovered") %>
                                        </div>
                                    </p>
                                </div>
                                <div id="MoreInfo" runat="server" visible="false">
                                    <h5 class="colblu">
                                        List of Partial Tools and Techniques covered:</h5>
                                    <p>
                                        <div style="padding-left: 10PX;">
                                            <asp:Literal ID="ltMore" runat="server" Text='<%#Eval("MoreInfo") %>'>
                                            </asp:Literal>
                                        </div>
                                    </p>
                                </div>
                                <div id="Instructor" runat="server" visible="false">
                                    <h5 class="colblu">
                                        Instructor:</h5>
                                    <p>
                                        <%#Eval("Instructor")%>
                                    </p>
                                </div>
                                <div id="Attend" runat="server" visible="false">
                                    <h5 class="colblu">
                                        Who should take this course?</h5>
                                    <p>
                                        <%#Eval("AttendDetail") %>
                                    </p>
                                </div>
                                <h5 class="colblu">
                                    Contact Information:</h5>
                                <div>
                                    For more Information contact the Training Administrator: <a href="mailto:info@qpsinc.com">
                                        info@qpsinc.com</a>, Telephone: 1-877-987-3801.</div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="span-5">
                    <QPSSidebar:ELearning ID="Sidebar_ELearning" runat="server" />
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>

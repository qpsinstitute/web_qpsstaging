﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectManagement.aspx.cs"
    Inherits="QPS.ELearning.ProjectManagement" MasterPageFile="~/QPSSite.Master" %>

<%@ Register Src="~/Controls/Sidebar_ELearning.ascx" TagName="ELearning" TagPrefix="QPSSidebar" %>
<%@ Register Src="~/Controls/Sidebar.ascx" TagName="Sidebar" TagPrefix="QPSSidebar" %>
<asp:Content ID="ContentProjectManagment" runat="server" ContentPlaceHolderID="cphContent">
    <script language="javascript" type="text/javascript">
        setPage("Training");
        function openPopup(txtURL) {
            window.open(txtURL, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width=600,height=500");
        }
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <div class="span-3 blackbl last" style="width: 130px">
                <a href="../Training/Registration.aspx" title="Corporate Register">Corporate Register</a></div>

                <div class="span-3 blackbl last">
                <a href="../Training/Training.aspx" title="Training Grants">Training Grants</a></div>

                <div class="span-3 blackbk last" style="width: 87px">
                <a href="EcourseList.aspx" title="eLearning">eLearning</a></div>

                <div class="span-3 blackbl last" style="width: 100px">
                <a href="../Training/OnSite.aspx" title="On-Site Training">On-Site Training</a></div>
                <div class="span-3 blackbl last" style="width: 120px">
                <a href="../Training/UnEmployed.aspx" title="Public Training">Public Training</a></div>

            <div class="span-3 blackbl last" style="width: 123px">
                <a href="../Training/Calendar.aspx" title="Corporate Calendar">Corporate Calendar</a></div>
           <%-- <div class="span-3 blackbl last" style="width: 110px">
                <a href="../Instructor/Signin.aspx" title="Instructor Login">Instructor Login</a></div>--%>
            <br class="clear">
            <br class="clear">
            <h3 id="H3_1" class="colbright" style="padding-top: 0px;">
                eLearning</h3>
            <p>
                <span style="font-size: 125%">Learn today’s hottest topics at your own pace and without
                    taking time away from your job. At work or at home, you can now learn what others
                    are learning and not have to worry about your job. You will have three months to
                    complete the program in your free time. Enhance your career by clicking on one of
                    the courses and follow the instructions. </span>
            </p>
            <div class="span-3 blackbl last">
                <a href="EcourseList.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
            <div class="span-3 blackbl last" style="width: 180px;">
                <a href="ManagmentSystem.aspx" title="Management Systems">Management Systems</a></div>
            <div class="span-3 blackbk last" style="width: 180px;">
                <a href="#" title="Project Management">Project Management</a></div>
           <%-- <div class="span-3 blackbl last" style="width: 80px;">
                <a href="Others.aspx" title="Others">Others</a></div>--%>
            <br class="clear">
            <br class="clear">
            <h3 id="training" class="colbright" style="padding-top: 0px;">
                Project Management</h3>
            <p>
                <span style="font-size: 125%">QPS is proud to offer a library of high quality online
                    Project Management courses to help you earn PDU’s today. Please select from the
                    courses below.</span></p>
            <table class="mainText">
                <tr>
                    <td>
                        <table class="mainText" style="border: 1px dotted silver; background: #ffffff;">
                        <tr>
                                <td style="padding-left: 10px; padding-right: 25px; padding-top: 5px; padding-bottom: 15px;"
                                    width="100%">
                                    <asp:Repeater ID="rpteCourseList" runat="server">
                                        <ItemTemplate>
                                            <table id="Table1" cellpadding="0" cellspacing="0" width="410">
                                                <tr>
                                                    <td class="eCourseTitleTD" style="background-color: #eaeaea; width: 388px;">
                                                        <strong>
                                                            <%#Eval("CourseTitle") %></strong>
                                                    </td>
                                                    <td bgcolor="white" width="2">
                                                    </td>
                                                    <td align="center" bgcolor="#eaeaea" valign="middle">
                                                        <a href="#" onclick="CheckOut('<%#Eval("CourseCode")%>','<%#Eval("CourseTitle") %>','<%#Eval("Cost") %>')">
                                                            <img src='/Images/addtocart.gif' title="ADD TO CART"></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class='eCourseCostTD' colspan="2">
                                                        <strong>$<%#Eval("Cost") %></strong>
                                                    </td>
                                                    <td align="right">
                                                        <a href="/ELearning/CourseDetails.aspx?CourseID=<%#Eval("CourseID") %>">
                                                            <img border="0" src='/Images/ViewCourseOutLine.gif'></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="eCourseRow3" colspan="3">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" height="3">
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                            </table>
                      </td>
                      </tr>  
                   
            </table>
            <p style="float: right; text-align: right;">
                <a class="nounder" href="#top" title="Top">
                    <img src="../images/top.gif" title="Top"></a></p>
        </div>
        <div class="span-4">
            <QPSSidebar:ELearning ID="Sidebar_ELearning" runat="server" />
        </div>
    </div>
    <form id="FormCheckOut" action="https://online.qpsinc.com/youraccount/RegistrationSignIn.aspx"
                        method="post" name="FormCheckOut">
                        <input id="hdnCourseID" name="courseId" type="hidden" />
                        <input id="hdnTitle" name="courseTitle" type="hidden" />
                        <input id="hdnCost" name="cost" type="hidden" />
                        </form>
                        <table class="mainText" style="border: 1px dotted silver; background: #ffffff;">
                            
                        </table>
</asp:Content>

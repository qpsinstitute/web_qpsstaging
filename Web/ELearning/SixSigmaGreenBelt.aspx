<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="SixSigmaGreenBelt.aspx.cs" Inherits="QPS.ELearning.SixSigmaGreenBelt" %>
<%@ Register TagPrefix="QPSSidebar" TagName="ELearning" Src="~/Controls/Sidebar_ELearning.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>

<asp:Content ID="ContentSixSigmaGreenBelt" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("eLearning");
    </script>
<div class="span-24 prepend-1 top highcont">
    <div class="span-5" style="float:right; margin-right:0px;">
        <form action="http://online.qpsinc.com/youraccount/CartPage.aspx" method="post">
            <table id="Table1" border="0" cellpadding="0" cellspacing="0"  width="100%"> <!-- Carttable -->
                <tbody>
                <tr><td style="height:6"></td></tr>
                <tr style="height:25">
	                <td valign="bottom" style="padding-top:200px; padding-bottom:1px">
		                <img src="/Images/ImagesBuyNow.gif" align="absbottom" alt="" />													
	                </td>
                </tr>											
                <tr style="height:40" ><!-- 61 -->
	                <td valign="top">
		                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 10px solid silver;"> <!-- Cart Content -->
			                <tbody>
			                    <tr>
				                <td bgcolor="#ffffff" width="10"></td>
				                <td valign="top" width="141">
					                <table id="Table2" cellpadding="0" cellspacing="0" width="100%">
						                <tbody>
						                <tr>
							                <td class="CartContent" valign="middle" style="padding-top:5px; padding-bottom:20px">
							                    Cost:$599
							                </td>											
						                </tr>	
						                <tr>
							                <td valign="bottom"><input src="/Images/addtocart.gif" type="image"></td>
						                </tr>
					                    </tbody>
					                </table>
				                </td>
				                <td bgcolor="#ffffff" width="10"></td>
			                    </tr>
		                    </tbody>
		                </table>
	                </td>
	                <td>&nbsp;</td>
                </tr>
                <tr style="height:25" > <!-- space -->
	                <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <input name="courseId" value="ESSIGBC" type="hidden"> <input name="cost" value="2200" type="hidden">
            <input name="courseTitle" value="Six Sigma Green Belt" type="hidden">
        </form>            
    </div>
    <div class="span-14 top" style="float:right; margin-right:0px;">
        <table class="mainText" width='100%'>
        <tr>
            <td>
    	    <h3 class="colbright" id="training"  style="padding-top:10px;">eLearning</h3>
            <p>
                <span style="font-size:125%">
                    Learn today�s hottest topics at your own pace and without taking time away from your job. At work or at home, you can now learn what others are learning and not have to worry about your job. You will have three months to complete the program in your free time.
                    Enhance your career by clicking on one of the courses and follow the instructions.
                </span>
            </p>
            <h3 class="colbright" id="H3_1">Six Sigma Green Belt Certification</h3>
            <h5 class="colblu">E Course No. 402</h5>
            <p>This course is designed for personnel who desire Six Sigma Green Belt Certification and who support Black Belts. Green Belts work part time on projects, support Black Belts and conduct small projects for improvement.</p>
            <h5 class="colblu">Course Benefits</h5>
            <p>Attendees learn to integrate the methodologies, implement breakthrough improvements, manage small Six Sigma projects and utilize more than 30 tools and techniques to achieve bottom-line success. Overall benefits are improved effectiveness in project support and greater enthusiasm for the implementation of Six Sigma concepts. </p>
            <h5 class="colblu">Prerequisites</h5>
            <p>None </p>
            <h5 class="colblu">Certification Requirements </h5>
            <p>Active class participation and successful completion of a written exam</p>
            <h5 class="colblu">Cost </h5>
            <p>$599 - Certificate of Green Belt Certification upon completion of the course and passing the Final Exam</p>
            <h5 class="colblu">Topics Covered:</h5>
            <p><div style="padding-left:10PX;">
            <ul>
                <li>Module 1: Six Sigma Orientation
                <li>Module 2: Define Phase
                <li>Module 3: Six Sigma Metrics
                <li>Module 4: Project Management
                <li>Module 5: Team Building
                <li>Module 6: Voice of the Customer/QFD
                <li>Module 7: Process Mapping
                <li>Module 8: Probability
                <li>Module 9: Basic Statistics
                <li>Module 10: Process Capability
                <li>Module 11: Measurement Systems Analysis
                <li>Module 12: Basic Quality Tools
                <li>Module 13: Management and Planning Tools
                <li>Module 14: Other Tools and Techniques
                <li>Module 15: Control Charts
            </ul></div>
            </p>
            <h5 class="colblu">List of Partial Tools and Techniques covered:</h5>
            <p><div style="padding-left:10PX;">
            <ul>
                <li>Project Charter
                <li>SIPOC diagram
                <li>DMAIC � Key milestones
                <li>CTQ
                <li>Process Flowchart/Map
                <li>Check sheet/Data collection Forms
                <li>Affinity Diagram
                <li>QFD Matrix
                <li>Gauge R&R
                <li>Process Capability ( Cp, Cpk)
                <li>Histogram
                <li>Pareto chart
                <li>Cause and Effect diagram
                <li>5 Whys
                <li>Process Sigma Calculation
                <li>DPU, DPMO, FTY, RTY
                <li>Box Plot
                <li>Scatter Plot
                <li>Stem and Leaf Plot
                <li>Tree Diagram
                <li>Run charts
                <li>Histogram
                <li>Control Charts		   
            </ul></div>
            </p>	
            <h5 class="colblu">Who would benefit from this course?</h5>
            <p>Anyone interested in supporting Six Sigma efforts should attend this course. This course is also for those who may desire to achieve Black Belt Certification in the future. 
            </p>
            <h5 class="colblu">More Information:</h5>
            <div>For more Information contact the Training Administrator: <a href="mailto:info@qpsinc.com">info@qpsinc.com</a>, 
            Telephone: 1-877-987-3801.</div>
	        </td>
        </tr>
        </table>
    </div>
    <div class="span-5">
        <QPSSidebar:ELearning id="Sidebar_ELearning" runat ="server"/>
    </div>
    </div>    
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="ProcessConceptandMapping.aspx.cs" Inherits="QPS.ELearning.ProcessConceptandMapping" %>
<%@ Register TagPrefix="QPSSidebar" TagName="ELearning" Src="~/Controls/Sidebar_ELearning.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>

<asp:Content ID="ContentSixSigmaGreenBelt" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("eLearning");
</script>
<div class="span-24 prepend-1 top highcont">
    <div class="span-5" style="float:right; margin-right:0px;">
        <form action="http://online.qpsinc.com/youraccount/CartPage.aspx" method="post">
            <table id="Table1" border="0" cellpadding="0" cellspacing="0"  width="100%"> <!-- Carttable -->
                <tbody>
                <tr><td style="height:6"></td></tr>
                <tr style="height:25">
	                <td valign="bottom" style="padding-top:200px; padding-bottom:1px">
		                <img src="/Images/ImagesBuyNow.gif" align="absbottom" alt="" />													
	                </td>
                </tr>											
                <tr style="height:40" ><!-- 61 -->
	                <td valign="top">
		                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 10px solid silver;"> <!-- Cart Content -->
			                <tbody>
			                    <tr>
				                <td bgcolor="#ffffff" width="10"></td>
				                <td valign="top" width="141">
					                <table id="Table2" cellpadding="0" cellspacing="0" width="100%">
						                <tbody>
						                <tr>
							                <td class="CartContent" valign="middle" style="padding-top:5px; padding-bottom:20px">
							                    Cost:$95
							                </td>											
						                </tr>	
						                <tr>
							                <td valign="bottom"><input src="/Images/addtocart.gif" type="image"></td>
						                </tr>
					                    </tbody>
					                </table>
				                </td>
				                <td bgcolor="#ffffff" width="10"></td>
			                    </tr>
		                    </tbody>
		                </table>
	                </td>
	                <td>&nbsp;</td>
                </tr>
                <tr style="height:25" > <!-- space -->
	                <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
		    <input name="courseId" value="EQSIPCM" type="hidden"> <input name="cost" value="2200" type="hidden">
		    <input name="courseTitle" value="Six Sigma Green Belt" type="hidden">
        </form>            
    </div>
    <div class="span-14 top" style="float:right; margin-right:0px;">
        <table class="mainText" width='100%'>
        <tr>
            <td>
            <h3 class="colbright" id="training"  style="padding-top:5px;">eLearning</h3>
	        <p>
	            <span style="font-size:125%">
	                Learn today�s hottest topics at your own pace and without taking time away from your job. At work or at home, you can now learn what others are learning and not have to worry about your job. You will have three months to complete the program in your free time.
                    Enhance your career by clicking on one of the courses and follow the instructions.
	            </span>
	        </p>
	        <h3 class="colbright" id="H3_1">Process Concept and Mapping</h3>
            <h5 class="colblu">E Course No. 416</h5>
	        <p>This course is designed to help you understand, develop and implement Process Mapping techniques. Process mapping enhances Quality Management Systems and its implementation helps meet the process approach requirements of current ISO standards. 	 </p>
            <h5 class="colblu">Course Benefits</h5>
	        <p>Participants learn to develop a Process Map from scratch or from existing procedures. The mapping process enhances the understanding and effectiveness of documents while reducing their size by more than 50%, making them more user friendly. The participant will have clear understanding on improved process documentation and also how to improve the processes. </p>
            <h5 class="colblu">Cost </h5>
	        <p>$95 - The cost includes Certification of Completion of the course upon completion of the course </p>
	        <h5 class="colblu">Topics Covered:</h5>
	        <p><div style="padding-left:10PX;">
	        <ul>
	            <li>Module 1. Understanding the process concept
	            <li>Module 2: Developing a Process Maps
	            <li>Module 3: Continual improvements
	        </ul></div>
	        </p>
	        <h5 class="colblu">Some of the Items are:</h5>
	        <p><div style="padding-left:10PX;">
	        <ul>
	            <li>Defining the Process
	            <li>Process Vs Departments
	            <li>Why process Concept
	            <li>Role of Process Owner
	            <li>Process Organization
	            <li>Process Metrics
                <li>Understanding different types of documentation
                <li>Flow chart Vs Process Map
                <li>Why Process Maps
                <li>How to make flow chart
                <li>How to develop process map
                <li>Understanding value added and non value added activities
                <li>How to identify Non Value added tasks in process maps
                <li>Continual improvement methods
                <li>Tools and Techniques
                <li>Procedure Vs Process Maps �Examples
                <li>Different types of process maps ( examples)   
	        </ul></div>
	        </p>
	        <h5 class="colblu">Who should take this course?</h5>
	        <p>Anyone interested in reducing, simplifying and improving their documentation.  
	        </p>
	        <h5 class="colblu">Contact for More Information:</h5>
	        <div>For more Information contact the Training Administrator: <a href="mailto:info@qpsinc.com">info@qpsinc.com</a>, 
	        Telephone: 1-877-987-3801.</div>
            </td>
    </tr>
    </table>
    </div>
    <div class="span-5">
        <QPSSidebar:ELearning id="Sidebar_ELearning" runat ="server"/>
    </div>
</div>
</asp:Content>

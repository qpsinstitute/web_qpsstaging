<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="AuditingtoISO9001-2000.aspx.cs" Inherits="QPS.ELearning.AuditingtoISO9001_2000" %>
<%@ Register TagPrefix="QPSSidebar" TagName="ELearning" Src="~/Controls/Sidebar_ELearning.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>

<asp:Content ID="ContentSixSigmaGreenBelt" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("eLearning");
</script>
<div class="span-24 prepend-1 top highcont">
    <div class="span-5" style="float:right; margin-right:0px;">
        <form action="http://online.qpsinc.com/youraccount/CartPage.aspx" method="post">
            <table id="Table1" border="0" cellpadding="0" cellspacing="0" width="100%"> <!-- Carttable -->
	            <tbody>
	            <tr><td style="height:6"></td></tr>
	            <tr style="height:25">
		            <td valign="bottom" style="padding-top:200px; padding-bottom:1px">
			            <img src="/Images/ImagesBuyNow.gif" align="absbottom" alt="" />													
		            </td>
	            </tr>											
	            <tr style="height:40" ><!-- 61 -->
		            <td valign="top">
			            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 10px solid silver;"> <!-- Cart Content -->
				            <tbody>
				                <tr>
					            <td bgcolor="#ffffff" width="10"></td>
					            <td valign="top" width="141">
						            <table id="Table2" cellpadding="0" cellspacing="0" width="100%">
							            <tbody>
							            <tr>
								            <td class="CartContent" valign="middle" style="padding-top:5px; padding-bottom:20px">
								                Cost:$195
								            </td>											
							            </tr>	
							            <tr>
								            <td valign="bottom"><input src="/Images/addtocart.gif" type="image"></td>
							            </tr>
						                </tbody>
						            </table>
					            </td>
					            <td bgcolor="#ffffff" width="10"></td>
				                </tr>
			                </tbody>
			            </table>
		            </td>
		            <td>&nbsp;</td>
	            </tr>
	            <tr style="height:25" > <!-- space -->
		            <td>&nbsp;</td>
	            </tr>
                </tbody>
            </table>
		    <input name="courseId" value="EQSBAI9" type="hidden"> <input name="cost" value="2200" type="hidden">
		    <input name="courseTitle" value="Six Sigma Green Belt" type="hidden">
        </form>    
    </div>
    <div class="span-14 top" style="float:right; margin-right:0px;">
        <table class="mainText" width='100%'>
        <tr>
            <td>
	        <h3 class="colbright" id="training"  style="padding-top:5px;">eLearning</h3>
		    <p>
		        <span style="font-size:125%">
		            Learn today�s hottest topics at your own pace and without taking time away from your job. At work or at home, you can now learn what others are learning and not have to worry about your job. You will have three months to complete the program in your free time.
                    Enhance your career by clicking on one of the courses and follow the instructions.
		        </span>
		    </p>
		    <h3 class="colbright" id="H3_1">Auditing to ISO 9001: 2015 (Includes Process Auditing)</h3>
            <h5 class="colblu">E Course No. 417</h5>
		    <p>This course is designed to help you understand the process approach to auditing. It will help you perform an internal or external audit effectively. The participant will be tested and will achieve Internal auditor certificate for successful completion of the course. </p>
            <h5 class="colblu">Course Benefits</h5>
		    <p>Participants will learn the requirements of Quality Management System and how to develop an audit checklist. In addition, it will help to perform process and system audits to realize continual improvements. </p>
            <h5 class="colblu">Prerequisites</h5>
		    <p>Basic auditing background </p>
		    <h5 class="colblu">Certification Requirements</h5>
		    <p>Course attendance and successful completion of the written exam.</p>  
            <h5 class="colblu">Cost</h5>
		    <p>$195 - The cost includes Certification of Completion of the course upon completion of the course </p>
            <h5 class="colblu">Topics Covered:</h5>
		    <p><div style="padding-left:10PX;">
		    <ul>
                <li>Module 1: Principles of Quality Management
                <li>Module 2: Quality System Requirements
                <li>Module 3: Auditing concepts and process
                <li>Module 4: Conducting audits
                <li>Module 5: Audit reporting and Follow up
		    </ul></div>
            </p>	
		    <h5 class="colblu">Who should take this course?</h5>
		    <p>Internal auditors, individuals and managers interested in auditing to the applicable standard and making continual quality improvements. 
		    </p>
		    <h5 class="colblu">Contact Information:</h5>
		    <div>For more Information contact the Training Administrator: <a href="mailto:info@qpsinc.com">info@qpsinc.com</a>, 
		    Telephone: 1-877-987-3801.</div>
            </td>
        </tr>
        </table>
    </div>
	<div class="span-5">
	    <QPSSidebar:ELearning id="Sidebar_ELearning" runat ="server"/>
	</div>
</div>
</asp:Content>

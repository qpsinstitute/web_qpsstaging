<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="SixSigmaOverview.aspx.cs" Inherits="QPS.ELearning.SixSigmaOverview" %>
<%@ Register TagPrefix="QPSSidebar" TagName="ELearning" Src="~/Controls/Sidebar_ELearning.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>

<asp:Content ID="ContentSixSigmaOverview" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("eLearning");
</script>
<div class="span-24 prepend-1 top highcont">
    <div class="span-5" style="float:right; margin-right:0px;">
        <form action="http://online.qpsinc.com/youraccount/CartPage.aspx" method="post">
            <table id="Table3" border="0" cellpadding="0" cellspacing="0" width="100%"> <!-- Carttable -->
	            <tbody>
	            <tr><td style="height:6"></td></tr>
	            <tr style="height:25">
		            <td valign="bottom" style="padding-top:200px; padding-bottom:1px">
			            <img src="/Images/ImagesBuyNow.gif" align="absbottom" alt="" />													
		            </td>
	            </tr>											
	            <tr style="height:40" ><!-- 61 -->
		            <td valign="top">
			            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 10px solid silver;"> <!-- Cart Content -->
				            <tbody>
				                <tr>
					            <td bgcolor="#ffffff" width="10"></td>
					            <td valign="top" width="141">
						            <table id="Table5" cellpadding="0" cellspacing="0" width="100%">
							            <tbody>
							            <tr>
								            <td class="CartContent" valign="middle" style="padding-top:5px; padding-bottom:20px">
								                Cost:$195
								            </td>											
							            </tr>	
							            <tr>
								            <td valign="bottom"><input src="/Images/addtocart.gif" type="image"></td>
							            </tr>
						                </tbody>
						            </table>
					            </td>
					            <td bgcolor="#ffffff" width="10"></td>
				                </tr>
			                </tbody>
			            </table>
		            </td>
		            <td>&nbsp;</td>
	            </tr>
	            <tr style="height:25" > <!-- space -->
		            <td>&nbsp;</td>
	            </tr>
                </tbody>
            </table>
            <input name="courseId" value="ESSIMOV" type="hidden"> <input name="cost" value="2200" type="hidden">
            <input name="courseTitle" value="Six Sigma Green Belt" type="hidden">
        </form>    
    </div>
    <div class="span-14 top" style="float:right; margin-right:0px;">
        <table class="mainText" width='100%'>
        <tr>
            <td>
            <h3 class="colbright" id="training"  style="padding-top:10px;">eLearning</h3>
	        <p>
	            <span style="font-size:125%">
	                Learn today�s hottest topics at your own pace and without taking time away from your job. At work or at home, you can now learn what others are learning and not have to worry about your job. You will have three months to complete the program in your free time.
                    Enhance your career by clicking on one of the courses and follow the instructions.
	            </span>
	        </p>
	        <h3 class="colbright" id="H3_1">Six Sigma Overview</h3>
            <h5 class="colblu">E Course No. 401</h5>
	        <p>This course is designed to provide an overview and clear presentation of the Six Sigma process. It addresses the tools, techniques and methodologies that make it a successful breakthrough program for realizing improvement in quality processes.</p>
            <h5 class="colblu">Course Benefits</h5>
	        <p>At the end of the course, attendees will be able to layout a roadmap to implement the Six Sigma program.</p>
            <h5 class="colblu">Cost</h5>
	        <p>$195 - The cost includes Certification of Completion of the course upon completion of the course</p>
            <h5 class="colblu">Course Topics:</h5>
	        <p><div style="padding-left:10PX;">
	        <ul>
	        <li>Module 1: Six Sigma Background and Concepts
	        <li>Module 2: Six Sigma Process -DMAIC
	        <li>Module 3: Role of Management
	        </ul></div>
            </p>
            <h5 class="colblu">List of Partial Tools and Techniques covered:</h5>
	        <p><div style="padding-left:10PX;">
	        <ul>
	            <li>Project Charter
	            <li>Stakeholder Analysis
	            <li>SIPOC diagram
	            <li>DMAIC � Key milestones
	            <li>CTQ Definitions
	            <li>Process Flowchart/Map
	            <li>Brainstorming
	            <li>Affinity Diagram
	            <li>QFD Matrix
	            <li>Benchmarking plan
	            <li>Gauge R&R
	            <li>Process Capability ( Cp, Cpk)
	            <li>Histogram
	            <li>Pareto chart
	            <li>Cause and Effect diagram
	            <li>5 Whys
	            <li>Process Sigma Calculation
	            <li>DPU, DPMO, FTY, RTY
	            <li>SWOT Analysis
	            <li>Six Sigma Organization
	        </ul></div>
	        </p>        	
	        <h5 class="colblu">Who should take this course?</h5>
	        <p>This course is designed for those who work to improve both production and transactional systems for the benefit of meeting and/or exceeding customer expectations who desire an understanding of Six Sigma. It provides the knowledge to plan a Six Sigma program and to present it to others.
	        </p>
	        <h5 class="colblu">Contact Information:</h5>
	        <div>For more Information contact the Training Administrator: <a href="mailto:info@qpsinc.com">info@qpsinc.com</a>, 
	        Telephone: 1-877-987-3801.</div>
		    </td>
        </tr>
        </table>
    </div>
	<div class="span-5">
	    <QPSSidebar:ELearning id="Sidebar_ELearning" runat ="server"/>
	</div>
</div>
</asp:Content>

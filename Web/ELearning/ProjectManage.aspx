﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectManage.aspx.cs" Inherits="QPS.ELearning.ProjectManage" MasterPageFile="~/QPSSite.Master"%>
<%@ Register Src="~/Controls/Sidebar_ELearning.ascx" TagName="ELearning" TagPrefix="QPSSidebar" %>
<%@ Register Src="~/Controls/Sidebar.ascx" TagName="Sidebar" TagPrefix="QPSSidebar" %>

<asp:Content ID="ContentProjectManagment" runat="server" ContentPlaceHolderID="cphContent">
<script language="javascript" type="text/javascript">
    setPage("eLearning");
 function openPopup(txtURL)
	{
        window.open(txtURL,"","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width=600,height=500");
	}
</script>

<div class="span-24 prepend-1 top highcont">
    <div class="span-18 top" style="float:right">
        <br class="clear">
        <h3 id="H3_1" class="colbright" style="padding-top: 0px;">eLearning</h3>
        <p>
            <span style="font-size: 125%">Learn today’s hottest topics at your own pace and without taking time away from your job. At work or at home, you can now learn what others are learning and not have to worry about your job. You will have three months to complete the program in your free time. Enhance your career by clicking on one of the courses and follow the instructions. </span>
        </p>
        
        <div class="span-3 blackbl last"><a href="EcourseList.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
	    <div class="span-3 blackbl last" style="width:180px;"><a href="#" title="Management Systems">Management Systems</a></div>
        <div class="span-3 blackbk last" style="width:180px;"><a href="#" title="Project Management">Project Management</a></div>
        <br class="clear"><br class="clear">
        <h3 id="training" class="colbright" style="padding-top: 0px;">Project Managment</h3>
      
       
        <table>
            <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong> HealthMax Software Project Management Simulation </strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong>  $359.00<br />
                    PDU: 10 <br />
                    Through this simulation, the learner will play the role of a project manager on a new product development project at HealthMax Software. The goal of the project is to develop HealthRecords, a new medical records software system. For HealthMax, a small company, the success of... <a href="javascript:openPopup(&quot;../documents/HealthMax.htm&quot;)">more</a>
                </td>
            </tr>
           
            <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong> PMP® Exam Prep Course </strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong>  $699.00<br />
                    PDU: 35 <br />
                    This completely online and self-paced thirteen-module project management program builds successful project managers at all levels of the organization. It provides a comprehensive preparation for the PMP® certification exam including exam taking tips, ten comprehensive module quizzes, and two full-length, 200-question practice exams... <a href="javascript:openPopup(&quot;../documents/PMP Exam Prep Course.htm&quot;)">more</a>
                </td>
            </tr>
            
            <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong> Project Management for Information Technology </strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong>   $499.00<br />
                    PDU: 25 <br />
                    Project management refers to the art, or science, of directing projects. This course, Project Management for Information Technology, focuses more specifically on how project management concepts can be applied to IT projects, as well as on project management issues that are unique to IT projects.... <a href="javascript:openPopup(&quot;../documents/Project Management for Information Technology.htm&quot;)">more</a>
                </td>
            </tr>
            
            <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>Project Management Team Leadership</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> 359.00<br />
                    PDU: 18 <br />
                    The vast majority of project work in today's organizations is done in a team setting. In this environment, project management team leaders have a tremendous responsibility and opportunity to develop and exhibit leadership skills. This course first discusses the roles and responsibilities of the project...  <a href="javascript:openPopup(&quot;../documents/Project Management Team Leadership.htm&quot;)">more</a>
                </td>
            </tr>
            
            <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>Project Risk Management: PMI-RMP® Exam Prep</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $599.00<br />
                    PDU: 30 <br />
                    This completely online and self-paced nine-module risk management course helps educate project managers in identifying and responding to project risk. This course provides a comprehensive preparation for the Project Management Institute's PMI-RMP® certification exam including exercises, self-assessments, and case studies along... <a href="javascript:openPopup(&quot;../documents/Project Risk Management  PMI-RMP Exam Prep.htm&quot;)">more</a>
                </td>
            </tr>
            
             <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>PMP 1 - Introduction to Project Management</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $79.00<br />
                    PDU: 3.5 <br />
                    This course was designed as a supplemental study guide for the major content areas of Chapters 1 and 2 of A Guide to the Project Management Body of Knowledge (PMBOK® Guide) , Fourth Edition. In this course, you will consider some of the organizational... <a href="javascript:openPopup(&quot;../documents/PMP1 Introduction to Project Management.htm&quot;)">more</a>
                </td>
            </tr>
            
              <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>PMP 2 - Project Processes and Project Integration Management</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $79.00<br />
                    PDU: 3.5 <br />
                    This course was designed as a supplemental study guide for the major content areas of Chapters 3 and 4 of A Guide to the Project Management Body of Knowledge (PMBOK® Guide) , Fourth Edition. In this course, you will be introduced to the five... <a href="javascript:openPopup(&quot;../documents/PMP 2  Project Processes and Project Integration Management.htm&quot;)">more</a>
                </td>
            </tr>
            
            <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>PMP 3 - Project Scope Management</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $79.00<br />
                    PDU: 3.5 <br />
                    This course was designed as a supplemental study guide for the major content areas of Chapter 5 of A Guide to the Project Management Body of Knowledge (PMBOK® Guide) , Fourth Edition. In this course, you will consider many of the key issues surrounding... <a href="javascript:openPopup(&quot;../documents/PMP 3  Project Scope Management.htm&quot;)">more</a>
                </td>
            </tr>
            
            <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>PMP 4 - Project Time Management</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $79.00<br />
                    PDU: 3.5 <br />
                    This course was designed as a supplemental study guide for the major content areas of Chapter 6 of A Guide to the Project Management Body of Knowledge (PMBOK® Guide) , Fourth Edition. In this course, you will consider how to best plan and schedule... <a href="javascript:openPopup(&quot;../documents/PMP 4  Project Time Management.htm&quot;)">more</a>
                </td>
            </tr>
            
             <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>PMP 5 - Project Cost Management</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $79.00<br />
                    PDU: 3.5 <br />
                    This course was designed as a supplemental study guide for the major content areas of Chapter 7 of A Guide to the Project Management Body of Knowledge (PMBOK® Guide) , Fourth Edition. In this course, you will consider the general approaches to cost management,... <a href="javascript:openPopup(&quot;../documents/PMP 5 Project Cost Management.htm&quot;)">more</a>
                </td>
            </tr>
            
             <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>PMP 6 - Project Quality Management</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $79.00<br />
                    PDU: 3.5 <br />
                    This course was designed as a supplemental study guide for the major content areas of Chapter 8 of A Guide to the Project Management Body of Knowledge (PMBOK® Guide) , Fourth Edition. In this course, you will learn how the project management team ensures... <a href="javascript:openPopup(&quot;../documents/PMP 6 Project Quality Management.htm&quot;)">more</a>
                </td>
            </tr>
            
             <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>PMP 7 - Project Human Resource Management</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $79.00<br />
                    PDU: 3.5 <br />
                    This course was designed as a supplemental study guide for the major content areas of Chapter 9 of A Guide to the Project Management Body of Knowledge (PMBOK® Guide) , Fourth Edition. In this course, you will learn how to acquire and negotiate the... <a href="javascript:openPopup(&quot;../documents/PMP 7 Project Human Resource Management.htm&quot;)">more</a>
                </td>
            </tr>
            
            <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>PMP 8 - Project Communications Management</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $79.00<br />
                    PDU: 3.5 <br />
                    This course was designed as a supplemental study guide for the major content areas of Chapter 10 of A Guide to the Project Management Body of Knowledge (PMBOK® Guide) , Fourth Edition. In this course, you will learn about how project management teams can...<a href="javascript:openPopup(&quot;../documents/PMP 8 Project Communications Management.htm&quot;)">more</a>
                </td>
            </tr>
            
            <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>PMP 9 - Introduction to Project Risk Management</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $79.00<br />
                    PDU: 3.5 <br />
                    This course was designed as a supplemental study guide for the major content areas of Chapter 11 of A Guide to the Project Management Body of Knowledge (PMBOK® Guide) , Fourth Edition. In this course, you will define the core elements of risk management,...<a href="javascript:openPopup(&quot;../documents/PMP 9 Introduction to Project Risk Management.htm&quot;)">more</a>
                </td>
            </tr>
            
             <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>PMP 10 - Project Procurement Management</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $79.00<br />
                    PDU: 3.5 <br />
                    This course was designed as a supplemental study guide for the major content areas of Chapter 12 of A Guide to the Project Management Body of Knowledge (PMBOK® Guide) , Fourth Edition. In this course, you will consider the various aspects of the Project...<a href="javascript:openPopup(&quot;../documents/PMP 10 Project Procurement Management.htm&quot;)">more</a>
                </td>
            </tr>
            
            <tr>
                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                   <strong>PMP 11 - PMP® Practice Exams & Exam Strategies</strong> 
                </td>
            </tr>
            <tr>
                <td><strong> Cost:</strong> $99.00<br />
                    This course contains two 200-question practice exams, which cover the Project Management Institute's standard, A Guide to the Project Management Body of Knowledge (PMBOK® Guide) . The practice exams provide a comprehensive review of the PMBOK® Guide . You will also review key...<a href="javascript:openPopup(&quot;../documents/PMP 11 PMP Practice Exams  Exam Strategies.htm&quot;)">more</a>
                </td>
            </tr>
            
        </table>
       <p style="float:right;text-align:right;"><a class="nounder" href="#top" title="Top"><img src="../images/top.gif" title="Top"></a></p>
    </div>
    
    <div class="span-4">
    <QPSSidebar:ELearning ID="Sidebar_ELearning" runat="server" />
    </div>
</div>
</asp:Content>

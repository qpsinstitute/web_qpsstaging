﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Staffing.aspx.cs" Inherits="QPS.Staffing.WebForm2" %>
<asp:Content ID="Contentstaffing" ContentPlaceHolderID="cphContent" runat="server">
<script language="javascript" type="text/javascript">
    setPage("Staffing");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	    <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a href="References.aspx" title="References">References</a></div>--%>
		<div class="span-5 blackbl last"><a href="Aboutus.aspx" title="About Us">About Us</a></div>
        <div class="span-4 blackbk last"><a href="Staffing.aspx" title="Staffing">Staffing</a></div>
		<div class="span-5 blackbl last"><a href="Recruitment.aspx" title="Recruitment">Recruitment</a></div>
        <div class="span-5 blackbl last"><a href="CareerTransition.aspx" title="Career Transition">Career Transition</a></div>
		
        <br class="clear"><br class="clear">
		
        <h3 class="colbright" id="staff"><u>Staffing</u></h3>
        <div  style="font-size:125%">
        <p>
        Quality Placement Services is New England’s premier staffing firm providing Temp, Temp to hire and Direct Hire placements. We match quality candidates with progressive and dynamic companies in the United States.
       <br />
       <br />
        At QPS, our philosophy is to simplify the career pursuit and transition.Our experienced and compassionate consultants conduct customized career services for corporations and individualsby providing skilled professionals to meet unique hiring requirements and work place environment
        </p></div>

        <h3 class="colbright"><u>Individuals</u></h3>
        <div  style="font-size:125%">
        <p>
         We provide one on one support and enthusiastic guidance with cutting edge expertize toindividuals in their quest for a career. Our consultants are dedicated in helping you positively move forward and pursue your path to success. 
        </p>
        </div>

         <h3 class="colbright"><u>Corporations</u></h3>
        <div  style="font-size:125%">
        <p>
        QPS Staffing displaysits ability to add value to any business objectives by providing a flexible and scalable operating environment that can accommodate dynamic shifts in volume,workload and staffing requirements. With organizational effectiveness, we foster leadership and team strengthening  synergy that enhances productivity and increases retention by hiring exceptional candidates.
        </p>
        </div>

         <h3 class="colbright"><u>Our Mission</u></h3>
        <div  style="font-size:125%">
        <p>
        QPS Staffing strives to maintain its reputation ofhighstandards and business ethics by using multiple communication channels (telephone, web, email, fax, SMS and traditional mail)to leverage knowledgeable,experienced and appropriate talent from across the globe to service our clients’ needs.
        </p>
        </div>

       <br class="clear">
            <strong>For more information, contact <a href="mailto:staffing@qpsinc.com">staffing@qpsinc.com</a>.</strong>
        </div>
        <div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space"><br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
        </div>

</asp:Content>

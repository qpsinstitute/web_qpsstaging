﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Aboutus.aspx.cs" Inherits="QPS.Staffing.Aboutus" %>
<asp:Content ID="Contentabout" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Placement");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	    <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a href="References.aspx" title="References">References</a></div>--%>
		<div class="span-5 blackbk last"><a href="Aboutus.aspx" title="About Us">About Us</a></div>
        <div class="span-4 blackbl last"><a href="Staffing.aspx" title="Staffing">Staffing</a></div>
        <div class="span-5 blackbl last"><a href="Recruitment.aspx" title="Recruitment">Recruitment</a></div>
		<div class="span-5 blackbl last"><a href="CareerTransition.aspx" title="Career Transition">Career Transition</a></div>
		
		
        <br class="clear"><br class="clear">
          <h3 class="colbright" id="about">About Us</h3>
          <br />
          <h3 class="colbright" id="innovative" style="margin-right:20px">INNOVATIVE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
          PROACTIVE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RESULT ORIENTED</h3>
          <div style="font-size:125%;">
          QPS Staffing located in Marlborough MA  introduces innovative ideas and solutions for a successful career path. QPS Staffing is the sister company of QPS Institute, a pioneer in consulting and training Six Sigma ,  Management Systems, ISO , FDA Related , Supply Chain and others. We are a staffing company with innovative training and development program that provides more than 100 courses and certification programs. Our experienced consultants mentor and coach individuals and groups to sharpen their skills for a very progressive career path .We simplify career pursuit with sophisticated recruitment strategy and an excellent human resources team. We provide  staffing and recruiting to many industries.We work with individuals and corporations of all sizes in the United States. focusing on professional, technical and manufacturing staff.
          </div>
          <br />
          <p style="font-size:125%">
          We  bring the tools and techniques for a result oriented career path. <br />
             Staffing <br />
             Recruiting<br />
             Career Transition<br />
             Training & Development<br />
                Human Recourses Consulting<br />

          </p>
        <br class="clear">
            <strong>For more information, contact <a href="mailto:staffing@qpsinc.com">staffing@qpsinc.com</a>.</strong>
        
        </div>
        <div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space"><br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
        </div>
         
</asp:Content>

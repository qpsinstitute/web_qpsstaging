using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using log4net;

namespace QPS.News
{
    public partial class LatestNews : System.Web.UI.Page
    {
        #region Constant(s)
        private static string XMLSettingsPath = ConfigurationManager.AppSettings["RssXMLPath"].ToString();
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected string PAGE_TYPE = "";
        protected string XMLFILE_NAME = ConfigurationManager.AppSettings["RSSFileName"].ToString();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    BindRepeater();
                }
                catch (Exception ex)
                {
                    if (log.IsErrorEnabled)
                        log.Error("[" + System.DateTime.Now.ToString("G") + "] RSSNewsContent::BindData - ", ex);
                    Response.Redirect("/errorpages/SiteError.aspx", false);
                }
            }
        }
        private void BindRepeater()
        {
            DataTable dtItem = SelectRssContetnFromXMLFile(XMLFILE_NAME);           
            rptNews.DataSource = dtItem;
            rptNews.DataBind();
        }
        private DataTable SelectRssContetnFromXMLFile(string FileName)
        {
            DataTable dtItem = null;
            try
            {
                DataSet dsXMLText = new DataSet();
                dsXMLText.ReadXml(XMLSettingsPath + FileName);

                dtItem = dsXMLText.Tables["item"];
                int cnt = 0;

                return dtItem;
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString() + "] SelectRssContetnFromXMLFile -", ex);

                return dtItem;
            }
        }
    }
}

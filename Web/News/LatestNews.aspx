<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="LatestNews.aspx.cs" Inherits="QPS.News.LatestNews" %>

<asp:Content ID="ContentContact" ContentPlaceHolderID="cphContent" runat="server" >
        
    <form name="frmNews" id="frmNews" runat="server">
    <div class="span-24 prepend-1 top highcont">
	    <div class="span-18 top" style="float:right">
	    <br class="clear"><br class="clear">
		    <h3 class="colbright" id="testimonials">Latest News</h3>
		    <hr/>
		    <div class="span-18 last">		 
                <table width="90%" border="0">
                    
                    <tr><td align="right" valign="top" colspan="2" style="text-align:right;"><a href="../js/LatestNews.rss" ><img src="../Images/rss_icon.gif"  width="27px" height="15px" alt="rss" /></a></td></tr>
                        <tr><td colspan="2" style="height:3px;"></td></tr>
                        
		            <asp:Repeater ID="rptNews" runat="server" >                
                        <ItemTemplate>
                            <tr>
                                <td align="left" valign="top" colspan="2"><a href="<%#DataBinder.Eval(Container.DataItem, "link")%>"  style="font-size:13px;font-weight:bold;" target="_blank"><%#DataBinder.Eval(Container.DataItem, "Title")%></a></td>
                             </tr>
                             <tr>                        
                                <td align="left" valign="top" class="GrayText" colspan="2"><%#DataBinder.Eval(Container.DataItem, "pubDate")%></td>                        
                              </tr>
                              <tr><td style="height:8px" colspan="2"></td></tr>
                              <tr>
                                <td align="left" valign="top" class="GrayText" colspan="2"><%#DataBinder.Eval(Container.DataItem, "Description").ToString().Replace("^", "&")%></td>                                                         
                            </tr>
                            <tr><td style="height:8px"  colspan="2"></td></tr>
                        </ItemTemplate>               
                    </asp:Repeater>
                </table>
                <br class="clear">
			    <p style="float:right;text-align:right;">
			        <a class="nounder" href="#top" title="Top"><img src="../images/top.gif" title="Top"></a>
			    </p>
	        </div>
	    </div>
	    <div class="span-4 top">
		    <img src="../images/about_us.gif" class="pull-1 top">
		    <hr class="space">
		    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
		        <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
		    </div>
	    </div>
	    <div class="span-16">
	        &nbsp;
	    </div> 
	</div>   
		
	
	</form>	
</asp:Content>
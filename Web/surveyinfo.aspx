﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true"
    CodeBehind="surveyinfo.aspx.cs" Inherits="QPS.surveyinfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  
    <script language="javascript" type="text/javascript">

        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode >= 48 && charCode <= 57) return false;
            return true;
            //            if (charCode > 31 && (charCode < 48 || charCode > 57 || charCode == 08))
            //                return true;
            //            return false;
        }
        function chksessiondate() {
            var year = document.getElementById('<%=ddlsessionyear.ClientID%>').value;
            var ddlDay = document.getElementById('<%=ddlsessionday.ClientID%>');
            var MonthVal = document.getElementById('<%=ddlsessionmonth.ClientID%>').value;
            checkData(year, ddlDay, MonthVal);
        }


        function LimtCharacters(txtMsg, CharLength, indicator) {

            chars = txtMsg.value.length;
            document.getElementById(indicator).innerHTML = CharLength - chars;
            if (chars > CharLength) {
                txtMsg.value = txtMsg.value.substring(0, CharLength);
            }
        }
        function checkData(year, ddlDay, MonthValue) {
            try {
                if (ddlDay.options.length == 30) {
                    ddlDay.options[30] = new Option('30', '30');
                    ddlDay.options[31] = new Option('31', '31');
                }
                else if (ddlDay.options.length == 29) {
                    ddlDay.options[29] = new Option('29', '29');
                    ddlDay.options[30] = new Option('30', '30');
                    ddlDay.options[31] = new Option('31', '31');
                }

                if (MonthValue == "2") {

                    if (ddlDay.options.length == 32) {
                        if ((year % 4) == 0) {
                            ddlDay.options[31] = null;
                            ddlDay.options[30] = null;
                        }
                        else {
                            ddlDay.options[31] = null;
                            ddlDay.options[30] = null;
                            ddlDay.options[29] = null;
                        }
                    }
                    else {
                        if ((year % 4) == 0) {

                            ddlDay.options[30] = null;
                        }
                        else {
                            ddlDay.options[30] = null;
                            ddlDay.options[29] = null;
                        }

                    }
                }

                else if ((MonthValue == "4") || (MonthValue == "6") || (MonthValue == "9") || (MonthValue == "11")) {
                    ddlDay.options[31] = null;
                }
                else {
                    if (ddlDay.options.length != "32")
                        ddlDay.options[31] = new Option('31', '31');
                }

            } catch (ex) { }
        }

        function chkfeb(id) {
            var year = document.getElementById('<%=ddlsessionyear.ClientID%>').value;
            var ddlDay = document.getElementById('<%=ddlsessionday.ClientID%>');
            var MonthVal = document.getElementById('<%=ddlsessionmonth.ClientID%>').value;
            checkData(year, ddlDay, MonthVal);
        }
        function chksessiondate() {
            var year = document.getElementById('<%=ddlsessionyear.ClientID%>').value;
            var ddlDay = document.getElementById('<%=ddlsessionday.ClientID%>');
            var MonthVal = document.getElementById('<%=ddlsessionmonth.ClientID%>').value;
            checkData(year, ddlDay, MonthVal);
        }
        function chkAppdate() {
            var year = document.getElementById('<%=ddlsessionyear.ClientID%>').value;
            var ddlDay = document.getElementById('<%=ddlsessionday.ClientID%>');
            var MonthVal = document.getElementById('<%=ddlsessionmonth.ClientID%>').value;
            checkData(year, ddlDay, MonthVal);
        }

        //for @ like special Expressions not allowed

        function chkSpecial() {

            var notAllowed = /[\!\@\#\$\%\^\&\*\(\)]/g;
            var txtbox = document.getElementById('<%=txtwhatdidulike.ClientID%>');
            var txtbox1 = document.getElementById('<%=txtImpprovements.ClientID%>');

            txtbox.addEventListener("keyup", function (e) {
                var a = txtbox.value;
                if (notAllowed.test(a)) {
                    txtbox.value = a.replace(notAllowed, "");
                }
            }, false);

            txtbox1.addEventListener("keyup", function (e) {
                var a = txtbox1.value;
                if (notAllowed.test(a)) {
                    txtbox1.value = a.replace(notAllowed, "");
                }
            }, false);
        }        


    </script>
    <style>
        .radiobuttonlist input
        {
            margin-left: -10px;
            float: left;
        }
        .radiobuttonlist label
        {
            font-size: 15px;
            float: left;
            padding-left: 10px;
            line-height: 35px;
        }
    </style>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <br class="clear">
            <h3 class="colbright pagetitle" id="H3_1">
                Feedback Survey Form</h3>
            <form id="form1" runat="server">
            <ajax:ScriptManager ID="ScriptManager1" runat="server">
            </ajax:ScriptManager>
          
            <table width="100%" runat="server" id="tblAdd" class="Empregwrp">
                <tr>
                    <td colspan="2" width="51%;">
                        Fields marked with <span style="color: Red">*</span> are mandatory
                        <br />
                        <asp:Label ID="lblmsg" runat="server" Font-Bold="true" ForeColor="Green" Font-Size="14px"
                            Width="370px"></asp:Label>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="vertical-align: middle">
                        User type <span style="color: red">*</span>
                    </td>
                    <td class="rBtn" align="right" style="vertical-align: bottom; position: relative;">
                        <div id="divusertype" class="rBtnD">
                            <asp:RadioButtonList CssClass="radiobuttonlist" ID="rbtusertype" ValidationGroup="survey"
                                OnSelectedIndexChanged="rbtusertype_OnSelectedIndexChanged" runat="server" RepeatDirection="Horizontal"
                                Font-Size="Medium" AutoPostBack="true">
                                <asp:ListItem Text="Corporate Client" Value="Client"></asp:ListItem>
                                <asp:ListItem Text="Public Training Student" Value="Student"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="Rqusertype" runat="server" ControlToValidate="rbtusertype"
                            ValidationGroup="survey" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Name <span style="color: red">*</span>
                    </td>
                    <td align="left" style="vertical-align: bottom">
                        <asp:TextBox ID="txtname" runat="server" Width="240px" class="mb0" onkeypress="return isNumberKey(event)"></asp:TextBox>
                        <%--  <input id="txtname" runat="server" name="ctl00$cphContent$name" size="34" title="Name" type="text" />--%>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="Rqname" runat="server" ControlToValidate="txtname"
                            ValidationGroup="survey" ErrorMessage="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revName" runat="server" ControlToValidate="txtname"
                            ValidationGroup="survey" ErrorMessage="Enter proper value" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr runat="server" id="tremployer">
                    <td align="right" style="vertical-align: text-top">
                        Employer <span style="color: red">*</span>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtEmployer" runat="server" Width="240px" MaxLength="50" onkeypress="return isNumberKey(event)"></asp:TextBox>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmployer"
                            ValidationGroup="survey" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Email address<span style="color: red">*</span>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtemail" runat="server" Width="240px"></asp:TextBox>
                        <%-- <input id="txtemail" runat="server" name="ctl00$cphContent$emailaddress" size="34"
                        title="Email Address" type="text" />--%>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="Rqemailaddress" runat="server" ControlToValidate="txtemail"
                            ValidationGroup="survey" ErrorMessage="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="Rgemailaddress" runat="server" ValidationGroup="survey"
                            ControlToValidate="txtemail" ErrorMessage="Please enter a valid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Telephone <span style="color: red">*</span>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtphone" runat="server" Width="240px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtphone"
                            ValidationGroup="survey" ErrorMessage="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionPhone"
                            runat="server" ControlToValidate="txtphone" ErrorMessage="Enter phone no (xxx-xxx-xxxx) format"
                            ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" Display="Dynamic"
                            ValidationGroup="survey"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Address 1<span style="color: red">*</span>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtaddress" runat="server" Width="240px"></asp:TextBox>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtaddress"
                            ValidationGroup="survey" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Address 2
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtaddress2" runat="server" Width="240px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Course type <span style="color: red">*</span>
                    </td>
                    <td align="left">
                        <%-- <ajax:UpdatePanel ID="uppnl1" runat="server">
                            <ContentTemplate>--%>
                        <asp:RadioButtonList ID="rdbCourseType" CssClass="radiobuttonlist" runat="server"
                            AutoPostBack="false" OnSelectedIndexChanged="rdbCourseType_OnSelectedIndexChanged"
                            RepeatDirection="Horizontal" Width="250px" Height="16px">
                            <asp:ListItem Text="eLearning" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Training" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                        <%-- </ContentTemplate>
                            <Triggers>
                                <ajax:PostBackTrigger ControlID="rdbCourseType" />
                            </Triggers>
                        </ajax:UpdatePanel>--%>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="rdbCourseType"
                            ValidationGroup="survey" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Course taken <span style="color: red">*</span>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlcoursename" Visible="false" runat="server" Width="240">
                        </asp:DropDownList>
                        <asp:TextBox ID="txtcoursename" runat="server" Width="240px"></asp:TextBox>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator2" runat="server"
                            ControlToValidate="txtcoursename" ErrorMessage="*" Display="Dynamic" ValidationGroup="survey"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Course taken date
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlsessionmonth" runat="server" AppendDataBoundItems="True"
                            onchange="javascript:chksessiondate();">
                            <asp:ListItem Value="0">Month</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlsessionday" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessiondate();">
                            <asp:ListItem Value="0">Day</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlsessionyear" runat="server" AppendDataBoundItems="True"
                            onchange="javascript:chksessiondate();">
                            <asp:ListItem Value="0">Year</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lblerror1" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="ddlsessionmonth"
                            ErrorMessage="*" Display="Dynamic" ValidationGroup="survey" InitialValue="0"
                            EnableClientScript="true"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlsessionday"
                            ErrorMessage="*" Display="Dynamic" ValidationGroup="survey" InitialValue="0"
                            EnableClientScript="true"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="ddlsessionyear"
                            ErrorMessage="*" Display="Dynamic" ValidationGroup="survey" InitialValue="0"
                            EnableClientScript="true"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        City <span style="color: red">*</span>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtCity" runat="server" Width="240px" MaxLength="50" onkeypress="return isNumberKey(event)"></asp:TextBox>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtCity"
                            ValidationGroup="survey" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        State <span style="color: red">*</span>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlState" runat="server" Width="240">
                            <asp:ListItem Value="0">Select State</asp:ListItem>
                            <asp:ListItem Value="AL">Alabama</asp:ListItem>
                            <asp:ListItem Value="AK">Alaska</asp:ListItem>
                            <asp:ListItem Value="AZ">Arizona</asp:ListItem>
                            <asp:ListItem Value="AR">Arkansas</asp:ListItem>
                            <asp:ListItem Value="CA">California</asp:ListItem>
                            <asp:ListItem Value="CO">Colorado</asp:ListItem>
                            <asp:ListItem Value="CT">Connecticut</asp:ListItem>
                            <asp:ListItem Value="DC">District of Columbia</asp:ListItem>
                            <asp:ListItem Value="DE">Delaware</asp:ListItem>
                            <asp:ListItem Value="FL">Florida</asp:ListItem>
                            <asp:ListItem Value="GA">Georgia</asp:ListItem>
                            <asp:ListItem Value="HI">Hawaii</asp:ListItem>
                            <asp:ListItem Value="ID">Idaho</asp:ListItem>
                            <asp:ListItem Value="IL">Illinois</asp:ListItem>
                            <asp:ListItem Value="IN">Indiana</asp:ListItem>
                            <asp:ListItem Value="IA">Iowa</asp:ListItem>
                            <asp:ListItem Value="KS">Kansas</asp:ListItem>
                            <asp:ListItem Value="KY">Kentucky</asp:ListItem>
                            <asp:ListItem Value="LA">Louisiana</asp:ListItem>
                            <asp:ListItem Value="ME">Maine</asp:ListItem>
                            <asp:ListItem Value="MD">Maryland</asp:ListItem>
                            <asp:ListItem Value="MA">Massachusetts</asp:ListItem>
                            <asp:ListItem Value="MI">Michigan</asp:ListItem>
                            <asp:ListItem Value="MN">Minnesota</asp:ListItem>
                            <asp:ListItem Value="MS">Mississippi</asp:ListItem>
                            <asp:ListItem Value="MO">Missouri</asp:ListItem>
                            <asp:ListItem Value="MT">Montana</asp:ListItem>
                            <asp:ListItem Value="NE">Nebraska</asp:ListItem>
                            <asp:ListItem Value="NV">Nevada</asp:ListItem>
                            <asp:ListItem Value="NH">New Hampshire</asp:ListItem>
                            <asp:ListItem Value="NJ">New Jersey</asp:ListItem>
                            <asp:ListItem Value="NM">New Mexico</asp:ListItem>
                            <asp:ListItem Value="NY">New York</asp:ListItem>
                            <asp:ListItem Value="NC">North Carolina</asp:ListItem>
                            <asp:ListItem Value="ND">North Dakota</asp:ListItem>
                            <asp:ListItem Value="OH">Ohio</asp:ListItem>
                            <asp:ListItem Value="OK">Oklahoma</asp:ListItem>
                            <asp:ListItem Value="OR">Oregon</asp:ListItem>
                            <asp:ListItem Value="PA">Pennsylvania</asp:ListItem>
                            <asp:ListItem Value="RI">Rhode Island</asp:ListItem>
                            <asp:ListItem Value="SC">South Carolina</asp:ListItem>
                            <asp:ListItem Value="SD">South Dakota</asp:ListItem>
                            <asp:ListItem Value="TN">Tennessee</asp:ListItem>
                            <asp:ListItem Value="TX">Texas</asp:ListItem>
                            <asp:ListItem Value="UT">Utah</asp:ListItem>
                            <asp:ListItem Value="VT">Vermont</asp:ListItem>
                            <asp:ListItem Value="VA">Virginia</asp:ListItem>
                            <asp:ListItem Value="WA">Washington</asp:ListItem>
                            <asp:ListItem Value="WV">West Virginia</asp:ListItem>
                            <asp:ListItem Value="WI">Wisconsin</asp:ListItem>
                            <asp:ListItem Value="WY">Wyoming</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlState"
                            ValidationGroup="survey" ErrorMessage="*" InitialValue="0" EnableClientScript="true"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="vertical-align: text-top">
                        What did you like? <span style="color: red">*</span>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtwhatdidulike" runat="server" Width="240px" Height="70px" TextMode="multiline"
                            onkeyup="LimtCharacters(this,5000,'ctl00_cphContent_lbldesccount'); javascript:chkSpecial();"></asp:TextBox>
                        <%--<textarea id="txtWhatdidulike" runat="server" name="ctl00$cphContent$position" size="34"
                        title="Position" titls="What did You Like" style="height: 50px;" type="text"></textarea>--%>
                        <span align="right" style="padding-left: 83px; color: gray; font-family: Verdana;
                            font-size: 11px">Remaining characters
                            <label id="lbldesccount" runat="server">
                                5000</label>
                        </span>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtwhatdidulike"
                            ValidationGroup="survey" ErrorMessage="*" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="vertical-align: text-top">
                        Suggested improvements <span style="color: red">*</span>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtImpprovements" runat="server" Width="240px" Height="70px" TextMode="MultiLine"
                            onkeyup="LimtCharacters(this,5000,'ctl00_cphContent_lbldesccount1');  javascript:chkSpecial();"></asp:TextBox>
                        <span align="right" style="padding-left: 83px; color: gray; font-family: Verdana;
                            font-size: 11px">Remaining characters
                            <label id="lbldesccount1" runat="server">
                                5000</label>
                        </span>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtImpprovements"
                            ValidationGroup="survey" ErrorMessage="*" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 300px;">
                        Put under testimonial section? <span style="color: red">*</span>
                    </td>
                    <td align="left" style="vertical-align: text-top">
                        <asp:RadioButtonList ID="rbtComments" CssClass="radiobuttonlist" runat="server" RepeatDirection="Horizontal"
                            Width="180px" Height="16px">
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="rbtComments"
                            ValidationGroup="survey" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td align="center">
                        <asp:Button ID="btnsubmit" runat="server" Text="Submit" ValidationGroup="survey"
                            OnClick="btnsubmit_Click" />&nbsp;
                        <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <div class="span-4">
            <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
    </div>

</asp:Content>

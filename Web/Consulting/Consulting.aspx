<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Consulting.aspx.cs"
    Inherits="QPS.Consulting.Consulting" %>

<asp:Content ID="ContentConsulting" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last" style="width: 102px;">
                <a href="leanSixSigma.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last" style="width: 129px;">
                <a href="QualitySystem.aspx" title="Management System">Management System</a></div>
         <%--   <div class="span-4 blackbl last" style="width: 129px;">
                <a href="../Resources/RiskManagement.aspx" title="Risk Management">Risk Management</a></div>--%>
            <div class="span-4 blackbl last" style="width: 42px;">
                <a href="FDAMatters.aspx" title="FDA">FDA</a></div>
            <div class="span-4 blackbl last" style="width: 132px;">
                <a href="ProjectManagement.aspx" title="Project Management">Project Management</a></div>
            <div class="span-3 blackbl last" style="width: 88px;">
                <a href="sCManagement.aspx" title="Supply Chain">Supply Chain</a></div>
            <div class="span-3 blackbl last" style="width: 78px;">
                <a href="outsourcing.aspx" title="Outsourcing">Outsourcing</a></div>
            <div class="span-4 blackbl last" style="width: 139px">
                <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Consulting</h3>
           <%--<p style="font-size: 125%">--%>
           <p>
                 Today's businesses come across many challenges, competition, demanding trends and requirements quite regularly. From bottom level manpower to top level management, there are tools that improve the company's performance very drastically. We offer those tools. 
                <br>
                <br>
                QPS offers thorough consulting and related services that your organization needs - be it Six Sigma, Lean Techniques, Quality Systems, Business Improvements, and various others. Whether it be for corporate return on investment or just professional development � QPS can help.
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="HSCCP.aspx.cs"
    Inherits="QPS.Consulting.HSCCP" %>

<asp:Content ID="ContentHSCCP" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last">
                <a href="LeanSixSigma.aspx">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="LeanThinking.aspx">Lean Techniques</a></div>
            <div class="span-3 blackbl last">
                <a href="SixSigma.aspx">Six Sigma</a></div>
            <div class="span-4 blackbk last">
                <a>Management System</a></div>
            <div class="span-4 blackbl last">
              <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <div class="span-5" style="float: right">
                <a href="QualitySystem.aspx" class="nounder right">
                    <img src="../images/right.gif" class="right">
                    Return to Management System</a></div>
            <br class="clear">
            <h3 class="colbright" id="training">
                HACCP Services</h3>
            <p>
               <%-- <span style="font-size: 125%">--%>
                    <img src="../images/management.gif" class="top right" style="margin-top: 0">
                    HACCP is an acronym for Hazard Analysis and Critical Control Points. It is a tool
                    that was originally developed for the seafood industry to determine potential hazards
                    in production processes that might critically affect the quality of an end product.
                    Currently required for food safety; the FDA is making new strides to require medical
                    device companies to comply with HACCP as well, with an eye on the pharmaceutical
                    industry to follow. <%--</span>--%>
            </p>
            <%--<br class="clear">--%>
            <p>
                A Critical Control Point (CCP) is defined as "a point, step, or procedure in a food
                process at which control can be applied and, as a result, a food-safety hazard can
                be prevented, eliminated, or reduced to acceptable levels."
            </p>
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                HACCP is a preventive management tool used to assure that the manufacturing process
                addresses all potential hazards of a product. HACCP is not a zero-risk system, but
                it is designed to minimize the risk of potential hazards. Understanding, proper
                implementation and monitoring can only be beneficial to your company. When used
                properly, the HACCP approach of evaluating your products and the production process
                could provide assurance that you have determined the hazards associated with the
                product and its processes. This approach is based on the following seven principles:
            </p>
            <ol>
                <li>
                Conduct Hazard Analysis
                <li>
                Determine the CCPs & Define Control
                <li>
                Establish Critical Limits
                <li>
                Establish Monitoring Procedures
                <li>
                Establish Corrective Actions
                <li>
                Establish Verification Procedures
                <li>
                Establish Record-Keeping & Documentation Procedures
            </ol>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
                Using time-tested techniques, QPS provides the consulting and training that clients
                need to understand hazard analysis and develop the tools needed to create, monitor
                and improve a controlled process for managing HACCP. Our services give clients the
                knowledge they need using a comprehensive, empowering method that teaches teams
                how to manage their HACCP plan in manufacturing through the following courses:
            </p>
            <ul>
                <li>
                HACCP Basic Training
                <li>
                System and Product Hazard Analysis
                <li>
                Determine and Define CCPs
                <li>
                Implementing an HACCP Plan
                <li>
                Auditing an HACCP Plan
                <li>
                Managing HACCP for FDA
            </ul>
            <p>
                PS will review the HACCP Plan and create an operational flow chart to perform a
                Hazard Analysis of your products and processes following the appropriate FDA guidelines.
                We evaluate each product and process for its potential Hazard as it relates to Biological,
                Chemical or Physical Hazard. We will assist in identifying the CRITICAL CONTROL
                POINTS of the process and help establish the critical limits for preventive measures
                associated with each CCP identified. We will then establish procedures to monitor
                and control each CCP, establish corrective actions required when a critical limit
                deviation occurs and create a record keeping system. Once established, the QPS team
                will assist with the HACCP Plan to ensure overall understanding and user-friendly
                implementation. If implemented improperly, the HACCP Plan has no value and could
                be a potential liability.
                <br>
                <br>
                Following successful implementation, QPS will monitor the plan periodically; and
                make recommendations for corrective actions to bring the plan into better compliance.
                Once accepted, the FDA will want to audit the plan. Our staff will be available
                to be present during these audits to assure correct interpretation, address any
                deficiencies on-site, and to ensure a fair and appropriate audit.
                <br>
                <br>
                We are well versed in working with certification companies to meet the needs of
                our clients. QPS takes pride in its attention to detail and ability to provide consistent,
                quality service to help its clients get what they want, and provide a valuable service
                at the same time.
                <br>
                <br>
                Please <a href="../Company/Contact.aspx" title="Contact QPS" class="nounder bold">contact
                    QPS</a> to help you become compliant and at a reasonable cost!
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <div style="padding-top: 20px; margin-left: -20px; width: 180px;" class="hrefnounderclass">
                <h4 class="bold hundredline">
                    Other Systems</h4>
                <a href="FDAMatters.aspx" class="nounder bold">FDA/Regulatory Matters</a><br>
                <a href="NQA.aspx" class="nounder bold">Nuclear Quality Assurance</a><br>
                <a href="Crisis.aspx" class="nounder bold">Crisis Intervention</a><br>
                <a href="CEMark.aspx" class="nounder bold">CE Mark</a><br>
                <a class="grayclass">HACCP</a><br>
            </div>
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

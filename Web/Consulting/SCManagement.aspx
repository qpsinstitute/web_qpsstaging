<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="SCManagement.aspx.cs"
    Inherits="QPS.Consulting.SCManagement" %>

<asp:Content ID="ContentSCManagement" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last" style="width: 102px;">
                <a href="LeanSixSigma.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last" style="width: 129px;">
                <a href="QualitySystem.aspx" title="Management System">Management System</a></div>
            <div class="span-4 blackbl last" style="width: 42px;">
                <a href="FDAMatters.aspx" title="FDA">FDA</a></div>
            <div class="span-4 blackbl last" style="width: 132px;">
                <a href="ProjectManagement.aspx" title="Project Management">Project Management</a></div>
            <div class="span-3 blackbk last" style="width: 88px;">
                <a href="sCManagement.aspx" title="Supply Chain">Supply Chain</a></div>
            <div class="span-3 blackbl last" style="width: 78px;">
                <a href="outsourcing.aspx" title="Outsourcing">Outsourcing</a></div>
            <div class="span-4 blackbl last" style="width: 139px">
               <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Supply Chain Excellence</h3>
            <p>
                <img src="../Images/SCM.jpg" width="132" height="198" class="left top" style="margin-top: 0">
               <%-- <span style="font-size: 125%">--%>Supply chain management (SCM) is a process of planning,
                    implementing and controlling a business supply chain to ensure efficiency and cost-effectiveness.
                    A supply chain is the collection of steps that transform raw components into the
                    final product. SCM is comprised of all movement and storage of raw materials, work-in-process
                    and finished goods from their origin to their consumption, integrating supply and
                    demand management within and across businesses. As a process, it encompasses all
                    the sourcing, procurement, conversion, and logistics management activities; starting
                    and ending with the customer.
                    <br>
                    <br>
                    As organizations strive to focus on their core competencies and becoming more flexible,
                    more organizations are involved in satisfying customer demand while reducing management
                    control of daily logistics. Less control and more supply chain partners led to the
                    creation of SCM concepts. <%--</span>--%>
            </p>
          <br />
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                The first step in supply chain management is known as planning. A strategy must
                be developed to address how a given good or service will meet the customer�s requirements
                and maintain profitability. Strategic activities include building relationships
                with suppliers and customers, and integrating information technology (IT) within
                the supply chain. At this level, company management will be looking to high level
                strategic decisions concerning the whole organization, such as the size and location
                of manufacturing sites, supplier partnerships, products and sales markets.
                <br>
                <br>
                Implementation (or development) takes place next. It involves building strong relationships
                with all necessary raw material suppliers used to make the product the company delivers.
                In this step, there is comprehensive focus on adopting measures that will produce
                cost benefits such as developing a purchasing strategy with the finest suppliers,
                using industry best practices, working to develop cost effect transportation and
                developing strategies to reduce the cost of storing inventory.
                <br>
                <br>
                From here, the process is managed and controlled. It includes the daily management
                of the supply chain�its transactions and relationships, including any rework/recall
                and ongoing continual improvement. Decisions at this level are made daily, and affect
                how the products move along the supply chain. Operational decisions involve making
                schedule changes to production, purchasing agreements with suppliers, taking orders
                from customers and moving products in the warehouse.
            </p>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
                All the activities mentioned have to be coordinated in a finely-tuned process in
                order to achieve the least total logistical cost. Trade-offs exist that increase
                the total cost if only one of the activities is optimized. It is crucial to maintain
                a systems approach when planning logistical activities, determining and developing
                the trade-offs for the most effective and efficient strategy. You need to find a
                way to streamline your business and remain competitive. Supply Chain Management
                is a process-focused, effective cost saving tool that will build better customer
                satisfaction across the board.
                <ul>
                    <li>
                    Are you customer focused? Demand driven?
                    <li>
                    Is your business proactive or reactive to logistical and quality issues?
                    <li>
                    What is happening in the industry and what is the competition doing to satisfy their
                    customers? What is your advantage?
                </ul>
                What you need is clear focus as to the issues your business faces both internally
                and externally�to ensure that your supply chain is operating as effectively as it
                can to generate the highest level of customer satisfaction at the lowest cost to
                you.
            </p>
            <p>
              
            </p>
            <p>
                <h4 class="colblu">
                    What Supply Chain Excellence can do for you?</h4>
                The purpose of supply chain management is to improve trust and collaboration among
                supply chain partners, causing more efficient movement of inventory. Creating a
                demand-driven, process focus for SCM is invaluable to the business focused on quality
                and now is the time to implement techniques to make the job easier. Customer service
                has never been more important! Creating partnerships across industries create new
                stabilities for everyone involved. There have been great advances in e-business
                in the supply chain and software to make SCM user-friendly and more effective than
                ever.
                <br>
                <br>
                QPS works with its clients to create and deliver processes that build high performance
                at a low cost. Whether you need a dynamite SCM process or industry-specific support,
                QPS can help you achieve higher performance for real, bottom-line improvements!
                Let us help you get started on building a more effective and profitable business.
                <br>
                <br>
                QPS has seasoned strategic and process consultants, each with industry-specific
                knowledge who can help you identify what you need to excel in your industry. Our
                consultants will help you unlock the secret to improved supply chain performance�and
                making the process work for you. We strive for business excellence with a team focused
                on your success. <a href="../Company/Contact.aspx" title="Contact QPS" class="nounder bold">
                    Contact us</a>
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="FDAMatters.aspx.cs"
    Inherits="QPS.Consulting.FDAMatters" %>

<asp:Content ID="ContentFDAMatters" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last">
                <a href="LeanSixSigma.aspx">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="LeanThinking.aspx">Lean Techniques</a></div>
            <div class="span-3 blackbl last">
                <a href="SixSigma.aspx">Six Sigma</a></div>
            <div class="span-4 blackbk last">
                <a>Management System</a></div>
            <div class="span-4 blackbl last">
              <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <div class="span-5" style="float: right">
                <a href="QualitySystem.aspx" class="nounder right">
                    <img src="../images/right.gif" class="right">
                    Return to Management System</a></div>
            <br class="clear">
            <h3 class="colbright" id="training">
                FDA/Regulatory Matters</h3>
            <p>
               <%-- <span style="font-size: 125%">--%>
                    <img src="../images/management.gif" class="top right" style="margin-top: 0">
                    Rapid advances in medicine and new technologies make medical device manufacturing
                    a busy and exciting industry. The downsides to that are the considerable challenges
                    that companies face, as market entry requirements for each new product must meet
                    rigorous FDA (Food and Drug Administration) regulatory standards. As a manufacturer,
                    you want to ensure that your goods are effective and safe for consumer use, but
                    you need to minimize the costs and constraints to make your business successful.
              
            </p>
            <%--<br class="clear">--%><br />
                        <p>
            <%--<span style="font-size: 125%">--%>
                21 CFR Part 820 outlines Quality System Regulations (QSR) for medical device manufacturers
                and importers; a requirement to sell product in the US. It also includes a requirement
                for manufacturers to implement an FDA cGMP (current Good Manufacturing Practices)
                compliant quality system for the design, manufacture, packaging, labeling, storage,
                installation and servicing of finished medical devices. ISO 13485 is a (voluntary
                for the US) quality standard that provides the framework for meeting medical device
                requirements in the international marketplace.</p>
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                ISO 13485:2003 specifically covers FDA QSR (21 CFR Part 820) requirements and thus
                complies with both US and international FDA regulations. A 510k is a pre-market
                submission to the FDA. Together, These QSR regulations help assure that medical
                devices are safe and effective for their intended use; to reduce, if not prevent,
                injuries and deaths. The FDA monitors device problem data and inspects the operations
                and records of device developers and manufacturers to determine compliance with
                the GMP requirements in the QS regulation.</p>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
                Our FDA compliance program enables you to get your products to market quickly and
                to maintain your FDA compliance status. As professional consultants to the healthcare
                industry, QPS helps companies to achieve and maintain their FDA compliance.
                <br>
                <br>
                Using our established, practical training techniques, QPS provides the consulting
                and training that clients need to understand the federal requirements as well as
                develop the appropriate system through the following courses:
            </p>
            <ul>
                <li>
                Understanding ISO 13485:2003 and FDA QSR (21 CFR Part 820) Reqs.
                <li>
                cGMP (section 520 of the Food, Drug and Cosmetic (FD&C) Act)
                <li>
                Determining Which Requirements Apply to Your Business
                <li>
                Rapidly Bringing Product to Market
                <li>
                How to Minimize Validation Costs
                <li>
                Premarket Notification Submissions (501(k)
                <li>
                Quality System Development & Implementation
                <li>
                Quality System Audits & Inspections
                <li>
                Preparing for FDA Inspections & Compliance Audits
                <li>
                Responding to 483�s and Warning Letters
                <li>
                Medical Device Reporting & Product Recalls
                <li>
                Requirements for Import/Export of Medical Devices
                <li>
                We also provide special registration-specific services for:
                <li>
                Writing 501 (k) applications
                <li>
                Process Validation
                <li>
                Design Control
                <li>
                Documentation
            </ul>
            <h4 class="colblu">
                What FDA/Regulatory Compliance can do for you?</h4>
            <p>
                Whether you need FDA QSR (21 CFR Part 820) and cGMP for regulatory compliance to
                sell medical devices or want to implement an ISO 13485 system to export, implementing
                a quality system will cause increased teamwork, employee involvement, continuous
                improvement, and communication. Your business success is a function of the use of
                these techniques and makes preparations for compliance easier now and for the future.
                <br>
                <br>
                Our staff has more than 30 years experience in the implementation of quality systems
                and managing compliance issues. Our experience includes numerous ISO/QS audits and
                inspections for compliance registration for a wide range of products and devices.
                We know what FDA expects from your quality system and can help you develop a quality
                system that is simple, practical, and effective in maintaining your quality objectives
                to comply with FDA requirements.
                <br>
                <br>
                The QPS FDA/Regulatory Compliance Program is designed to create and build your success,
                specific to your requirements. The QPS team provides:
            </p>
            <ul>
                <li>
                    <p>
                        Coaching and hands-on guidance to team leaders and team members through successful
                        registration.</p>
                    <li>
                        <p>
                            Structured classroom training in practical, hands-on techniques needed to understand
                            and manage a successful and efficient quality system.</p>
                        <li>
                            <p>
                                Assistance in developing an appropriate implementation plan for your quality system.</p>
                            <li>
                                <p>
                                    Enhanced participant skill sets within the organization to drive quality system
                                    improvements, remedy urgent problems and deliver effective corrective action</p>
            </ul>
            <p>
                Our clients are achieving their goals. From strategy to plan, training to implementation,
                we are committed to deliver, with support unrivaled by others. <a href="../Company/Contact.aspx"
                    title="Contact QPS" class="nounder bold">Contact us</a> today and find out why
                so many businesses trust QPS for their success.
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <div style="padding-top: 20px; margin-left: -20px; width: 180px;" class="hrefnounderclass">
                <h4 class="bold hundredline">
                    Other Systems</h4>
                <a class="grayclass">FDA/Regulatory Matters</a><br>
                <a href="NQA.aspx" class="nounder bold">Nuclear Quality Assurance</a><br>
                <a href="Crisis.aspx" class="nounder bold">Crisis Intervention</a><br>
                <a href="CEMark.aspx" class="nounder bold">CE Mark</a><br>
                <a href="HSCCP.aspx" class="nounder bold">HACCP</a><br>
            </div>
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Crisis.aspx.cs"
    Inherits="QPS.Consulting.Crisis" %>

<asp:Content ID="ContentCrisis" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last">
                <a href="LeanSixSigma.aspx">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="LeanThinking.aspx">Lean Techniques</a></div>
            <div class="span-3 blackbl last">
                <a href="SixSigma.aspx">Six Sigma</a></div>
            <div class="span-4 blackbk last">
                <a>Management System</a></div>
            <div class="span-4 blackbl last">
             <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <div class="span-5" style="float: right">
                <a href="QualitySystem.aspx" class="nounder right">
                    <img src="../images/right.gif" class="right">
                    Return to Management System</a></div>
            <br class="clear">
            <h3 class="colbright" id="training">
                Crisis Intervention</h3>
            <p>
               <%-- <span style="font-size: 125%">--%>
                    <img src="../images/management.gif" class="top right" style="margin-top: 0">
                    What is Crisis Intervention? Has your business ever been a potentially dire or damaging
                    situation? Failure to properly act in these situations could possibly result in
                    your business being shut down, criminal/civil actions being filed against you or
                    your company, and/or huge financial losses. Some of the following situations fit
                    the need for intervention: <%--</span>--%>
            </p>
           <%-- <br class="clear">--%>
            <ul>
                <li>
                Recently received a notice of a forthcoming surprise audit?
                <li>
                Received an FDA Warning Letter?
                <li>
                Received non-conforming observations from the government or registrar?
                <li>
                Thinking about implementing a Corrective Action or Product Recall?
            </ul>
            <p>
                Careful, thoughtful, response is important in order to address these and other critical
                situations. Too many companies proceed without guidance and have ended up wasting
                a lot of time and money by not involving expert assistance.
            </p>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
                QPS is a recognized industry leader in assisting various types of companies out
                of crisis situations. Time is critical and we are on call to provide you some initial
                assistance in understanding what is going on and how to proceed. We help create
                an effective approach in the event of a crisis�an action plan to manage it and proceed
                in business accordingly.
                <br>
                <br>
                An initial consultation is at no charge to you�if you are not satisfied with QPS�s
                services, you may cancel at any time. <a href="../Company/Contact.aspx" title="Contact QPS"
                    class="nounder bold">Contact QPS</a> to manage any business crisis; we work
                together with you and for you to handle with care that which matters most.
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <div style="padding-top: 20px; margin-left: -20px; width: 180px;" class="hrefnounderclass">
                <h4 class="bold hundredline">
                    Other Systems</h4>
                <a href="FDAMatters.aspx" class="nounder bold">FDA/Regulatory Matters</a><br>
                <a href="NQA.aspx" class="nounder bold">Nuclear Quality Assurance</a><br>
                <a class="grayclass">Crisis Intervention</a><br>
                <a href="CEMark.aspx" class="nounder bold">CE Mark</a><br>
                <a href="HSCCP.aspx" class="nounder bold">HACCP</a><br>
            </div>
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

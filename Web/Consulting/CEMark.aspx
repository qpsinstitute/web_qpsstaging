<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="CEMark.aspx.cs"
    Inherits="QPS.Consulting.CEMark" %>

<asp:Content ID="ContentCEMark" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last">
                <a href="LeanSixSigma.aspx">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="LeanThinking.aspx">Lean Techniques</a></div>
            <div class="span-3 blackbl last">
                <a href="SixSigma.aspx">Six Sigma</a></div>
            <div class="span-4 blackbk last">
                <a>Management System</a></div>
            <div class="span-4 blackbl last">
              <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <div class="span-5" style="float: right">
                <a href="QualitySystem.aspx" class="nounder right">
                    <img src="../images/right.gif" class="right">
                    Return to Management System</a></div>
            <br class="clear">
            <h3 class="colbright" id="training">
                CE Mark / MDD / IVDD / AIMDD</h3>
            <p>
              <%--  <span style="font-size: 125%">--%>
                    <img src="../images/management.gif" class="top right" style="margin-top: 0">
                    The European Community (EC) has implemented a system to protect the health of consumers
                    and users of manufactured products. EC Directives and harmonized standards were
                    created to ensure conformity to safety and quality standards throughout all the
                    member states of the EC. The directives are voted into laws by the respective countries.
                    If you do not comply with these laws, it is the equivalent to breaking federal law
                    in the US. A review of your product's certification can be requested by customs,
                    competitors, or because of an accident. If you want to export your products to anyone
                    of the following countries: <%--</span>--%>
            </p>
            <br class="clear">
            <div class="span-5 colborder">
                <ul>
                    <li>
                    Austria
                    <li>
                    Belgium
                    <li>
                    Denmark
                    <li>
                    Finland
                    <li>
                    France
                    <li>
                    Germany
                    <li>
                    Greece
                    <li>
                    Iceland
                    <li>
                    Ireland
                    <li>
                    Italy
                    <li>
                    Luxembourg
                </ul>
            </div>
            <div class="span-5 last">
                <ul>
                    <li>
                    The Netherlands
                    <li>
                    Norway
                    <li>
                    Portugal
                    <li>
                    Spain
                    <li>
                    Sweden
                    <li>
                    United Kingdom
                    <li>
                    Poland
                    <li>
                    Romania
                    <li>
                    Czech Republic
                    <li>
                    Slovenia
                    <li>
                    Bulgaria
                </ul>
            </div>
            <br class="clear">
            <p>
                Your products must comply with the applicable EC Directives, and bear a "CE Marking"
                as a mark of conformity and compliance. It is a requirement for selling devices
                within Europe and the European distributors will typically request the CE Marking
                before carrying your product.
            </p>
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                The CE Mark represents compliance with a specific device directive that can be achieved
                through self-assessment or a certification body depending upon the device classification.
                Following are the steps to CE Marking:
            </p>
            <ol>
                <li>
                Properly classify the device
                <li>
                Determine the applicable harmonized standards and essential health and safety requirements
                <li>
                Plan self certification or Identify and choose a notified body
                <li>
                Determine and perform the appropriate conformity assessment procedure
                <li>
                Incorporate the requirements into the device/system design
                <li>
                Assemble the Technical File
                <li>
                Prepare the Declaration of Conformity
                <li>
                Affix the CE Marking
            </ol>
            <p>
                For medical devices, the European Community requires compliance with the three special
                directives (Medical Device Directive (MDD), In-Vitro Diagnostic Directive (IVDD)
                and the Active Implantable Medical Device Directive (AIMDD)) in order to put the
                CE Mark on your device. With the CE marking shown on your product or, if not possible,
                on the package, it can circulate freely throughout the member countries. CE certification
                will help you obtain authorizations, if required, for medical products for non-CE
                countries.
            </p>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
                The QPS team are experts at guiding our clients to CE Mark�providing a roadmap and
                assisting them through the entire process of securing their CE Mark. Our services
                include:
            </p>
            <ul>
                <li>
                Device Classification
                <li>
                Preparation of Technical File & Process
                <li>
                Determination of Certifying Body
                <li>
                Assistance in ISO Certification (as needed)
            </ul>
            <p>
                Our fast-track approach will save time and resources, decreasing your time to market.
                We expedite the process and minimize the costs involved by managing the process
                steps with you, keeping you informed all along the way. We are well versed in working
                with certification companies to meet the needs of our clients. QPS takes pride in
                its attention to detail and ability to provide consistent, quality service to help
                its clients get what they want, and provide a valuable service at the same time.
                <br>
                <br>
                Please <a href="../Company/Contact.aspx" title="Contact QPS" class="nounder bold">contact
                    QPS</a> to help you obtain CE Marking quickly and at a reasonable cost!
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <div style="padding-top: 20px; margin-left: -20px; width: 180px;" class="hrefnounderclass">
                <h4 class="bold hundredline">
                    Other Systems</h4>
                <a href="FDAMatters.aspx" class="nounder bold">FDA/Regulatory Matters</a><br>
                <a href="NQA.aspx" class="nounder bold">Nuclear Quality Assurance</a><br>
                <a href="Crisis.aspx" class="nounder bold">Crisis Intervention</a><br>
                <a class="grayclass">CE Mark</a><br>
                <a href="HSCCP.aspx" class="nounder bold">HACCP</a><br>
            </div>
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

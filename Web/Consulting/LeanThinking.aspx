<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="LeanThinking.aspx.cs"
    Inherits="QPS.Consulting.LeanThinking" %>

<asp:Content ID="ContentLeanThinking" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last">
                <a href="LeanSixSigma.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
            <div class="span-4 blackbk last">
                <a title="Lean Techniques">Lean Techniques</a></div>
            <div class="span-3 blackbl last">
                <a href="SixSigma.aspx" title="Six Sigma">Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="QualitySystem.aspx" title="Management System">Management System</a></div>
            <div class="span-4 blackbl last">
               <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Lean Techniques</h3>
            <p>
               <%-- <span style="font-size: 125%">--%>Lean is a methodology that maximizes shareholder value
                    by achieving improvement in cost, cycle time, product and process quality, invested
                    capital & profit. Lean and Six Sigma are often implemented together because neither
                    alone improves process capability, speed and workflow, but together they do all
                    this while reducing capital investment and empowering your workforce. What a great
                    team! 
                <br>
                <br>
                <span class="colblu" style="font-size: 125%">Are you looking to improve quality, cost,
                    lead-time & profit?</span><br>
                <img src="../images/lean2.gif" class="top right">
                Quality & Productivity Solutions, Inc. is also a well-qualified team to assist in
                implementing Lean Six Sigma because of their expertise on both topics. We have a
                unique method for selecting and deploying improvement projects that maximize break-through
                results. Experts in the field of quality, production and inventory control, our
                consultants have trained and implemented all types of business improvement practices
                and have managed several successful lean projects from start to finish. We customize
                this training for each of our clients, doing all that is needed to set up a successful
                program for your business. With over 20 years experience in the areas of quality
                and operations, our knowledge, understanding and service are unmatched in the industry.
                <br>
                <br>
                Our training provides a practical approach to understanding lean principles for
                all levels in any type of businesses. Learn how Lean thinking stacks up to other
                concepts and why it works hand-in-hand with Six Sigma and ISO 9000 to take you to
                the top of your industry and keep that competitive edge. We share our knowledge,
                experience and skills with you to improve:
            </p>
            <ul>
                <li>
                Cost
                <li>
                Quality
                <li>
                Delivery
                <li>
                Flexibility
                <li>
                Throughput
                <li>
                Productivity
                <li>
                Market Share
                <li>
                Customer Satisfaction
            </ul>
            <p>
                For Public Lean training, see our training <a href="../Training/Calendar.aspx" class="nounder bold"
                    title="Training calender">calender</a><br>
                For list of the courses, download the Lean Brochure.
                <br>
                <br>
                <strong>For more details, please download the <a target="_blank" href="../documents/LeanBrochures.pdf"
                    class="nounder bold" title="Lean Brochures">Lean Brochures</a>, which outlines all
                    of our training programs.</strong>
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="LeanSixSigma.aspx.cs"
    Inherits="QPS.Consulting.LeanSixSigma" %>

<asp:Content ID="ContentLeanSixSigma" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbk last" style="width: 102px;">
                <a href="leanSixSigma.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last" style="width: 129px;">
                <a href="QualitySystem.aspx" title="Management System">Management System</a></div>
            <div class="span-4 blackbl last" style="width: 42px;">
                <a href="FDAMatters.aspx" title="FDA">FDA</a></div>
            <div class="span-4 blackbl last" style="width: 132px;">
                <a href="ProjectManagement.aspx" title="Project Management">Project Management</a></div>
            <div class="span-3 blackbl last" style="width: 88px;">
                <a href="sCManagement.aspx" title="Supply Chain">Supply Chain</a></div>
            <div class="span-3 blackbl last" style="width: 78px;">
                <a href="outsourcing.aspx" title="Outsourcing">Outsourcing</a></div>
            <div class="span-4 blackbl last" style="width: 139px">
               <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <h3 class="colbright" id="H3_1">
                <br class="clear">
                Lean Six Sigma</h3>
            <p>
                <%--<span style="font-size: 125%">--%>
                    <img src="../images/leansixsigma2.gif" class="top right" style="margin-top: 0">
                   Lean Six Sigma has become very popular in the professional world, it is a 
                   combination of Lean and the Six Sigma methodology and tools. Although 
                   most of this innovation was focused on manufacturing operations, it can be 
                  learned from different case studies and success stories that transferring Lean 
                   from the shop floor to the office, as well as providing a deployment model 
                   that integrates Lean, Six Sigma and process management, delivers wonderful 
                   results uniformly.  
            </p>
            <br class="clear">
            <div class="span-5 colborder">
                <h4>
                    <span class="colblu">Lean Six Sigma for<br>
                        Healthcare</span></h4>
                <hr class="space">
                Lean Six Sigma is a business strategy that combines the methodologies of Lean Enterprise and Six Sigma into one seamlessly-integrated strategy for excellence. The combination of these two powerful performance improvement programs is changing the face of modern healthcare delivery... <a href="LeanSixSigmaHealthcare.aspx" class="nounder bold">Read
                    more.</a>
            </div>
            <div class="span-5 colborder">
                <h4>
                    <span class="colblu">Lean Six Sigma for<br>
                        Manufacturing</span></h4>
                <hr class="space">
                Lean Six Sigma is the combination of two methodologies�Lean Enterprise and Six Sigma�it is today�s fastest and most effective approach to business improvement. By combining the lead-time reduction and rapid waste elimination of Lean with Six Sigma�s spotlight on quality and yield development, Lean Six Sigma gives process manufacturers a combination punch for faster results than Six Sigma alone... <a href="LeanSixSigmaManufacturing.aspx"
                    class="nounder bold">Read more.</a>
            </div>
            <div class="span-6 last">
                <h4>
                    <span class="colblu">Lean Six Sigma for<br>
                        Service</span></h4>
                <hr class="space">
                Lean Six Sigma is a business strategy that combines the strength of today's two most powerful improvement initiatives - Lean Enterprise and Six Sigma - into one integrated strategy for excellence. Although both Six Sigma and Lean Enterprise have their roots in manufacturing, they work just as effectively in service industries. Service businesses face the same relentless demands that manufacturers do � to reduce waste and increase speed, all while improving quality, delivery and customer satisfaction... <a href="LeanSixSigmaService.aspx"
                    class="nounder bold">Read more.</a>
            </div>
            <br class="clear">
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <div style="padding-top: 20px" class="hrefnounderclass">
                <h4 class="bold hundredline">
                    Lean Six Sigma for:</h4>
                <a href="LeanSixSigmaHealthcare.aspx" class="nounder bold" title="Healthcare">Healthcare</a><br>
                <a href="LeanSixSigmaManufacturing.aspx" class="nounder bold" title="Manufacturing">
                    Manufacturing</a><br>
                <a href="LeanSixSigmaService.aspx" class="nounder bold" title="Service">Service</a><br>
            </div>
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

<%@ Master Language="C#" AutoEventWireup="true" Codebehind="QPSPopUp.Master.cs" Inherits="QPS.QPSPopUp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="masterHead" runat="server">
    <meta name="MSSmartTagsPreventParsing" content="TRUE" />
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
    <meta name="keywords" content="total quality management, tqm, six sigma, methodology, consulting, training, lean, ISO, ISO 9000, ISO 14000, 16949, ISO/TS, six-sigma employment, 6 sigma" />
    <meta name="description" content="A committed consulting and training partner for Six Sigma, Lean, and ISO. Customized training, on-site consulting, and results oriented work." />
    <title>Quality & Productivity Solutions, Inc. -- Dedicated provider of Six Sigma, Lean,
        and ISO Consulting and Training</title>
    <style>

 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"Arial Unicode MS";
	panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
	{font-family:"Bookman Old Style";
	panose-1:2 5 6 4 5 5 5 2 2 4;}
@font-face
	{font-family:"Calisto MT";
	panose-1:2 4 6 3 5 5 5 3 3 4;}
@font-face
	{font-family:AGaramond;
	panose-1:0 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"Arial Black";
	panose-1:2 11 10 4 2 1 2 2 2 4;}
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:"\@Arial Unicode MS";
	panose-1:2 11 6 4 2 2 2 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman";}
h1
	{margin:0in;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman";
	font-weight:normal;}
h2
	{margin:0in;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman";
	font-weight:bold;}
h3
	{margin-top:0in;
	margin-right:4.0pt;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	text-align:right;
	page-break-after:avoid;
	font-size:10.0pt;
	font-family:"Times New Roman";
	font-weight:bold;}
h4
	{margin:0in;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:10.0pt;
	font-family:Arial;
	font-weight:bold;}
h5
	{margin-right:.5in;
	margin-left:0in;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:Arial;
	font-weight:normal;}
h6
	{margin:0in;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:Arial;
	color:#FF9900;
	font-weight:bold;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman";}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{margin:0in;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:14.0pt;
	font-family:"Times New Roman";
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:16.0pt;
	font-family:AGaramond;
	font-style:italic;}
p.MsoBodyTextIndent, li.MsoBodyTextIndent, div.MsoBodyTextIndent
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:9.0pt;
	margin-bottom:.0001pt;
	font-size:16.0pt;
	font-family:"Times New Roman";}
p.MsoDate, li.MsoDate, div.MsoDate
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman";}
p.MsoBodyText2, li.MsoBodyText2, div.MsoBodyText2
	{margin:0in;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:14.0pt;
	font-family:AGaramond;
	font-style:italic;}
p.MsoBodyText3, li.MsoBodyText3, div.MsoBodyText3
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:Arial;}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
p
	{margin-right:0in;
	margin-left:0in;
	font-size:12.0pt;
	font-family:"Arial Unicode MS";}
p.BlockQuotationLast, li.BlockQuotationLast, div.BlockQuotationLast
	{margin-top:0in;
	margin-right:.5in;
	margin-bottom:12.0pt;
	margin-left:.5in;
	font-size:10.0pt;
	font-family:"Times New Roman";
	font-style:italic;}
p.ChapterLabel, li.ChapterLabel, div.ChapterLabel
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:12.0pt;
	margin-left:0in;
	page-break-after:avoid;
	border:none;
	padding:0in;
	font-size:7.5pt;
	font-family:"Arial Black";
	text-transform:uppercase;
	letter-spacing:3.5pt;}
p.SectionLabel, li.SectionLabel, div.SectionLabel
	{margin-top:102.0pt;
	margin-right:0in;
	margin-bottom:.25in;
	margin-left:0in;
	line-height:24.0pt;
	font-size:24.0pt;
	font-family:"Arial Black";
	color:gray;
	letter-spacing:-1.75pt;}
span.cccontent1
	{font-family:Verdana;
	font-variant:normal !important;
	letter-spacing:3288.8pt;
	font-weight:normal;
	font-style:normal;
	text-decoration:none none;}
span.bodytext1
	{font-family:Verdana;}
p.bodytext, li.bodytext, div.bodytext
	{margin-right:0in;
	margin-left:0in;
	font-size:10.0pt;
	font-family:Verdana;}
 /* Page Definitions */
 @page Section1
	{size:8.5in 11.0in;
	margin:.8in 67.7pt .7in 1.0in;}
div.Section1
	{page:Section1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}  </style>
</head>
<body>
    <div ud="popupheader">
        <p class="MsoDate" align="center" style='text-align: center'>
            <img width="576" height="37" src="Images/QPSTitle.jpg"></p>
        <p class="MsoDate" align="center" style='text-align: center'>
            One Sunny Hill Drive, Oxford, MA 01540&nbsp; Telephone: 1-877-987-3801</p>
        <p class="MsoDate" align="center" style='text-align: center'>
            Email:&nbsp; <a href="mailto:info@qpsinc.com">info@qpsinc.com</a>&nbsp; Website:
            <a href="http://www.qpsinc.com">www.qpsinc.com </a>
        </p>
        <p class="MsoNormal">
            &nbsp;</p>
        <p class="MsoBodyText" align="center" style='margin-left: .25in; text-align: center;
            line-height: 50%'>
            <b><span style='font-family: "Bookman Old Style"'>&nbsp;</span></b>&nbsp;</p>
        <h1 align="center" style='text-align: center'>
            <span style='font-size: 22.0pt; font-family: Arial; color: #B30000'>
                <asp:Label ID="lblCourseTitle" runat="server"></asp:Label>
            </span>
        </h1>
        <div class="MsoNormal" align="center" style='text-align: center'>
            <span style='color: #B30000'>
                <hr size="2" width="100%" align="center" />
            </span>
        </div>
    </div>
    <div>
        <asp:ContentPlaceHolder ID="cphContent" runat="server">
        </asp:ContentPlaceHolder>
    </div>
    <div id="popupfooter">
        <p class="MsoNormal">
            <span style='font-size: 12.0pt; font-family: Arial'>&nbsp;</span>&nbsp;</p>
        <p class="MsoBodyText3">
            <b><span style='font-size: 3.0pt'>&nbsp;</span></b>&nbsp;</p>
        <p class="MsoBodyText3">
            <b><span style='font-size: 3.0pt'>&nbsp;</span></b>&nbsp;</p>
        <p class="MsoBodyText3">
            <b><span style='font-size: 3.0pt'>&nbsp;</span></b>&nbsp;</p>
        <p align="center" class="MsoDate" style='text-align: center; font-weight: bold; font-size: 14pt'>
            Business Solutions Providers for Quality, Productivity and Performance Excellence</p>
        <p align="center" class="MsoDate" style='text-align: center; font-size: 14pt; font-style: italic'>
            Experts in Lean Six Sigma, Management System, Supply Chain, Project
            <br />
            Management and Professional Development</p>
    </div>
</body>
</html>

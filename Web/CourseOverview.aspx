<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CourseOverview.aspx.cs" Inherits="QPS.CourseOverview" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Course Overview</title>
    <style>
        .TopHeader
        {
            margin:0in;
	        margin-bottom:.0001pt;
	        font-size:10.0pt;
	        font-family:Times New Roman
        }
        .CourseTitle
        {
            font-size: 22.0pt; 
            font-family: Arial;
            color: #B30000;
            text-align: center;
            font-weight: bold;
            margin-top:26px            
        }
        .FieldDetail
        {
            margin:0in;
	        margin-bottom:19px;
	        font-size:12.0pt;
	        font-family:"Arial"
	        
        }
        .FieldNormal
        {
            margin:0in;
	        margin-bottom:19px;
	        font-size:10.0pt;
	        font-family:"Arial"
	        
        }
        .FieldTitle
        {
           margin:0in;
	       margin-bottom:.0001pt;
	       page-break-after:avoid;
	       font-size:12.0pt;
	       font-family:"Arial";
	       font-weight:bold;
        }
        .FooterBold
        {
            font-size: 19px; 
            font-family: Times New Roman;
            text-align: center;
            font-weight: bold;
            margin-top:76px    
        }
        .FooterNormal
        {
            font-size: 19px; 
            font-family: Times New Roman;
            text-align: center;
            font-style:italic
              
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="popupheader">
            <p align="center" class="TopHeader" style='text-align: center'>
                <img height="37" src="Images/QPSTitle.jpg" width="576" alt="" /></p>
            <p align="center" class="TopHeader" style='text-align: center'>
                One Sunny Hill Drive, Oxford, MA 01540&nbsp; Telephone: (508) 987-3800</p>
            <p align="center" class="TopHeader" style='text-align: center'>
                Email:&nbsp; <a href="mailto:info@qpsinc.com">info@qpsinc.com</a>&nbsp; Website:
                <a href="http://www.qpsinc.com">www.qpsinc.com </a>
            </p>
                <div class="CourseTitle">
                    <asp:Label ID="lblCourseTitle" runat="server" Text="Data"></asp:Label>
                </div>
                <hr align="center" class="FieldDetail" size="2" width="100%" />
        </div>
    </div>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:Repeater ID="rptCourseDetails" runat="server">
                        <ItemTemplate>
                            <div>
                                <div class="FieldTitle">Course No.&nbsp;<%#Eval("CourseNo") %></div>
                                <div class="FieldDetail"><%#Eval("CourseDesc") %></div>
                               
                                
                            </div>
                            <div id="divcoursebenefit" runat="server" visible="false">
                                <div class="FieldTitle">Course Benefits:</div>
                                <div class="FieldDetail"><%#Eval("CourseBenifit") %></div>
                                
                            </div>
                            <div id="divPrerequisite" runat="server" visible="false">
                                <div class="FieldTitle">Prerequisites:</div>
                                <div class="FieldDetail"><%#Eval("Prerequisites") %></div>
                            </div>
                            <div id="divCertification" runat="server" visible="false">
                                <div class="FieldTitle">Certification Requirements:</div>
                                <div class="FieldDetail"><%#Eval("CertificationReq") %></div>
                            </div>
                            <div id="divDuration" runat="server" visible="false">
                                <div class="FieldTitle">Duration:</div>
                                <div class="FieldDetail"><%#Eval("Duration") %></div>
                                
                            </div>
                            <div id="divCost" runat="server" visible="false">
                                <div class="FieldTitle">Cost:</div>
                                <div class="FieldDetail">$<%#Eval("Cost")%>&nbsp;<%#Eval("CostDesc")%> </div>
                            </div>
                            <div id="divTopicCovered" runat="server" visible="false">
                                <div class="FieldTitle">Topics Covered:</div>
                                <div class="FieldDetail"><%#Eval("TopicsCovered") %></div>
                            </div>
                            <div id="divMoreInfo" runat="server" visible="false">
                                <div class="FieldDetail"><%#Eval("MoreInfo") %></div>
                            </div>
                            <div id="divInstructor" runat="server" visible="false">
                                <div class="FieldTitle">Instructor:</div>
                                <div class="FieldDetail"><%#Eval("Instructor")%> </div>
                                
                            </div>
                            <div id="divAttendies" runat="server" visible="false">
                                <div class="FieldTitle">Who Should Attend ?</div>
                                <div class="FieldDetail"><%#Eval("Attenddetail")%></div>
                             
                            </div>
                            <div>
                                <div class="FieldTitle">For More Information:</div>
                                <div class="FieldNormal">
                                    Contact the Training Administrator, Pam Martel <a href="mailto:info@qpsinc.com">info@qpsinc.com</a>,
                                    Telephone: (508) 987-3800 or Jay Patel at 508-579-1006.
                                </div>
                                    
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
        <div id="popupfooterHeader">
            <div id="divfooterbold" class="FooterBold">
                Business Solutions Providers for Quality, Productivity and Performance Excellence
            </div>
            <div id="divfooternormal" class="FooterNormal">
                Experts in Lean Six Sigma, Management System, Supply Chain, Project
            </div>
            <div id="div1" class="FooterNormal" style="margin-bottom:10px;">
               Management and Professional Development
            </div>
            
        </div>
    </form>
</body>
</html>

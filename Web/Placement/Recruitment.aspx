﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Recruitment.aspx.cs" Inherits="QPS.Placement.Recruitment" %>
<asp:Content ID="Contentrecruitment" ContentPlaceHolderID="cphContent" runat="server">
<script language="javascript" type="text/javascript">
    setPage("Placement");      
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	    <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a href="References.aspx" title="References">References</a></div>--%>
		<div class="span-5 blackbl last"><a href="Aboutus.aspx" title="About Us">About Us</a></div>
        <div class="span-4 blackbl last"><a href="Staffing.aspx" title="Staffing">Staffing</a></div>
       <div class="span-5 blackbk last"><a href="Recruitment.aspx" title="Recruitment">Recruitment</a></div>
		<div class="span-5 blackbl last"><a href="CareerTransition.aspx" title="Career Transition">Career Transition</a></div>
	
		
        <br class="clear"><br class="clear">
		
        <h3 class="colbright" id="recruitment">Recruitment</h3>
        <div style="font-size:125%">
        <p >
       QPS Staffing specializes in placing highly skilled professionals with leading companies of all sizes in any geographical area or location in the United States. Our Recruiting Consultants provide cutting edge services to match your requirements. QPS Staffing has a large pool of qualified candidates that can match with careers in a variety of areas.</p>
       </div>
       <div style="font-size:125%">
       <p>
       QPS Staffing believes in its candidates and has an integral list of subject matter experts with an immediate start. QPS Staffing is extremely comprehensive  in preparing contracts, paperwork, payroll and other compensations for clients. We maintain a very high and ethical standard in acquiring background checks, references, drug testing and work status documentation. 
       </p>
       </div>
       <div style="font-size:125%">
       <p>
       QPS Staffing promotes reinvigorating referrals, employment branding and CRM model for hiring. We provide a friendly and welcoming orientation and work amicably with individuals or groups in the relocation process. Our Outplacement Services enhance workforce planning for a successful transition internally or externally.
       </p>
       </div>
        <div style="font-size:125%">
       <p>
       At QPS Staffing, we have built a pipeline of qualified and diverse candidates and have developed targeted recruitment campaigns. Our very knowledgeable consultants remain up to date on recruiting news, laws, industry trends and technology. We ensure that all regulatory aspects of recruitment processes are in full   compliance with federal, state, local and international law.
       </p></div>
       <div style="font-size:125%">
       <p>
       Our consultants regularly participate in networking organizations and events in local and national   forums. At QPS Staffing, with our sophisticated recruiting we constantly and proactively focus on turning the best talent into hires. 
       </p></div>
         <br class="clear">
            <strong>For more information, contact <a href="mailto:info@qpsinc.com">info@qpsinc.com</a>.</strong>
        
		</div>
        <div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space"><br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
</asp:Content>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="QPS.Controls.Header" %>
<div class="headerHM">
    <ul id="menuHM">
        <li id="mnhome-top" class="activeHM"><a id="home-top" href="../default.aspx" title="Home">
            Home</a></li>
        <li id="mncompany-top"><a id="company-top" href="../Company/about.aspx" title="Company">
            Company</a></li>
        <li id="mntraining-top"><a id="training-top" href="../Training/Calendar.aspx" title="Training">
            Training</a></li>
        <li id="mnconsulting-top"><a id="consulting-top" href="../Consulting/Consulting.aspx"
            title="Consulting">Consulting</a></li>
      <%--  <li id="mnplacement-top"><a id="placement-top" href="../Placement/Aboutus.aspx" title="Staffing & Recruitment">
            Staffing & Recruitment</a></li>--%>
        <li id="mnresources-top"><a id="resources-top" href="../Resources/Resources.aspx"
            title="Resources">Resources</a></li>
        <li id="mnevents-top"><a id="events-top" href="../Events/Default.aspx" title="Free Events">
           Free Events</a></li>
        <li id="mnwebinars-top"><a id="webinars-top" href="../Webinars/Default.aspx" title="Webinars">
            Webinars</a></li>
        <li id="mncontactus-top"><a id="contactus-top" href="../Company/Contact.aspx" title="Contact Us">
            Contact Us</a></li>
        <li class="homeIconHM"><a href="../cart.asp" style="font-size: 16px;" title="Add To Cart">
            <img src="../Images/cart_icon.png" title="ADD TO CART" />
        </a></li>
    </ul>
    <div class="newLogoWrp">
        <div class="qpsNewLogo">
            <a href="../Welcome.aspx" title="The QPS Institute">
                <img src="../Images/qps_logo.png" />
                <h1>
                    Quality & Productivity Solutions, Inc.
                </h1>
            </a>
        </div>
        <div class="headerTopRightWrp">
            <div class="headerSearchWrpHM">
                <form action="" id="searchbox_016124679326840018074:zth8bogqxfo">
                <div style="padding-top: 0px">
                    <input id="q" name="q" onblur="setImage()" onclick="removeImg()" size="20" style="background: #FFFFFF url(http://qpsinc.com/images/qps_watermark.gif) left no-repeat;"
                        title="Search" type="text" />
                    <input title="Search" type="submit" value="Search" onclick="OnLoad();" />
                </div>
                </form>
                <script src="../Js/brand.js" type="text/javascript"></script>
                <div id="results_016124679326840018074:zth8bogqxfo" style="display: none">
                    <div id="ANS1" class="cse-closeResults">
                        <a>&times; Close</a>
                    </div>
                    <div id="res" class="cse-resultsContainer">
                    </div>
                </div>
                <link href="../Css/overlay.css" rel="stylesheet" type="text/css" />
                <script src="http://www.google.com/uds/api? file=uds.js&v=1.0&key=ABQIAAAAR4CiGksj9ilcLyKw0v9fNBQfuHxrG_OBhPfTgMNXqY1zIg08ZBSJQZMdFYSEpm8BD5G4Gbe4ZBBUYg&hl=en"
                    type="text/javascript"></script>
                <script src="http://www.google.com/cse/api/overlay.js"></script>
                <script type="text/javascript">
                    function OnLoad() {
                        var cseo = new CSEOverlay("016124679326840018074:zth8bogqxfo", document.getElementById("searchbox_016124679326840018074:zth8bogqxfo"), document.getElementById("results_016124679326840018074:zth8bogqxfo"));
                        cseo.searchControl.setLinkTarget(GSearch.LINK_TARGET_PARENT);
                    }
                    GSearch.setOnLoadCallback(OnLoad);
                    //GSearch.setLinkTarget(google.search.Search.LINK_TARGET_PARENT);
                    document.getElementById("q").style.fontSize = "11pt";
                </script>
            </div>
            <div style="clear: both">
            </div>
            <div class="socialLinksWrpHM">
                <span style="color: White; vertical-align: super; font-size: 11px;">Follow Us:</span>
                &nbsp; <a target="_blank" href="http://www.blogger.com/profile/02032638210555019174"
                    style="text-decoration: none;">
                    <img src="../Images/1360240082_blogger.png" border="0" title="Blogger" style="" width="25px"
                        height="25px" /></a> <%--<a target="_blank" href="http://www.facebook.com/pages/The-QPS-Institute/189052384471963"
                            style="text-decoration: none;">
                            <img src="../Images/Facebook_icon_4.png" border="0" title="Facebook" style="" /></a>&nbsp;--%>
                <a target="_blank" href="http://www.linkedin.com/groups/QPS-Institute-3887066?itemaction=mclk&anetid=3887066&impid=3887066-3085229&pgkey=anet_about_guest&actpref=anet_about-gbm"
                    style="text-decoration: none;">
                    <img src="../Images/linkedin.png" border="0" title="Linkedin" style="" /></a>&nbsp;
                <a target="_blank" href="https://twitter.com/#!/TheQPSInstitute" style="text-decoration: none;">
                    <img src="../Images/twitter.png" border="0" title="Twitter" style="" /></a>
                <a target="_blank" href="http://www.eventbrite.com/org/3223954808" style="text-decoration: none;
                    padding-left: 2px;">
                    <img src="../Images/eventbrite.png" border="0" title="Eventbrite" width="85px" height="25px" /></a>
            </div>
        </div>
        <div style="clear: both;">
        </div>
    </div>
</div>
<script language="javascript" type="text/javascript">
    function removeImg() {
        document.getElementById("q").style.background = "#FFFFFF";
    }

    function setImage() {
        if (document.getElementById("q").value == "")
            document.getElementById("q").style.background = "#FFFFFF url(http://qpsinc.com/images/qps_watermark.gif) left no-repeat";
    }
</script>

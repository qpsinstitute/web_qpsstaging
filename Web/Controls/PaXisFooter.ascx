<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaXisFooter.ascx.cs" Inherits="PerpetSiteWebv2.controls.PaXisFooter" %>
<%@Register TagPrefix="PTI" TagName="BreadCrumbTrail" src="BreadCrumbTrail.ascx" %>
<%if (PerpetSiteWebv2.PerpetSiteMaster.page.IndexOf("ASP.paxis_default_aspx") < 0) {%>
<tr id="">
            <td colspan="2">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                    <tr align="center">
                        <td width="25%" class="ad"><a href="#"><img src="/images/global/OnlineVideo.JPG" /><br /><span><u>Take an online video tour</u></span></a></td>
                        <td width="25%" class="ad"><a href="/Paxis/Information.aspx"><img src="/images/global/live_demo.png" /><br /><span>Contact us for a live demo</span></a></td>
                        <td width="25%" class="ad"><a href="/Paxis/DownloadPaper.aspx"><img src="/images/global/WhitePaper.JPG" /><br /><span>Download a white paper</span></a></td>
                        <td width="25%" class="ad"><a href="http://itexec.meetup.com/29/?gj=sj2" target="_blank"><img src="/images/global/Meetup.png" /><br /><span>Join our meetup group</span></a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="2" height="10"></td></tr>
        <tr>
            <td colspan="2" align="center">
<% }%>

<table class="DataTable" style="width:900px;">
    <tr><td colspan="4" align="left"><PTI:BreadCrumbTrail ID="ucBreadCrumbTrail" Runat="Server" ></PTI:BreadCrumbTrail></td></tr>
    <tr>
        <th>Paxis Benefits</th>
        <th>Community</th>
        <th>News & Press</th>
        <th>Contact</th>
        
    </tr>
    <tr>
        <td align="left">
            <table>
                <tr><td style="padding-left:0; padding-right:0;"><a href="/Paxis/QualityManagement.aspx" ><span class="GrayText">Quality Management</span></a></td></tr>
                <tr><td style="padding-left:0; padding-right:0;"><a href="/Paxis/FDACompliance.aspx" ><span class="GrayText">FDA Compliance</span></a></td></tr>
                <tr><td style="padding-left:0; padding-right:0;"><a href="/Paxis/GovernanceAndCompliance.aspx" ><span class="GrayText">Governance & Compliance</span></a></td></tr>
                <tr><td style="padding-left:0; padding-right:0;"><a href="/Paxis/DocumentManagement.aspx" ><span class="GrayText">Document Management</span></a></td></tr>
            </table>
        </td>
        <td align="left" valign="top">
            <table>
                <tr><td style="padding-left:0; padding-right:0;"><a href="http://itexec.meetup.com/29/?gj=sj2" target="_blank"><span class="GrayText">Boston Meetup Group</span></a></td></tr>
                <tr><td style="padding-left:0; padding-right:0;"><a href="/Paxis/DownloadPaper.aspx?signup=''" ><span class="GrayText">Webinars & Podcasts</span></a></td></tr>
            </table>
        </td>
        <td align="left" valign="top">
           <table>
                <tr><td style="padding-left:0; padding-right:0;"><a href="http://itexec.meetup.com/29/?gj=sj2" target="_blank"><span class="GrayText">Join our Meetup Group</span></a></td></tr>
                <tr><td style="padding-left:0; padding-right:0;"><span class="GrayText">Paxis DocuSoft v2.0 Announced</span></td></tr>
            </table>
           
        </td>
        <td align="left" valign="top">
            <table>
                <tr><td style="padding-left:0; padding-right:0;"><a href="/Paxis/DownloadPaper.aspx" ><span class="GrayText">Download Whitepapers</span></a></td></tr>
                <tr><td style="padding-left:0; padding-right:0;"><a href="/Paxis/Information.aspx" ><span class="GrayText">Instant Demo</span></a></td></tr>
                <tr><td style="padding-left:0; padding-right:0;"><span class="GrayText">Phone: 617.734.3978</span></td></tr>
                <tr><td style="padding-left:0; padding-right:0;"><span class="GrayText">Email: <a href="mailto:info@perpetuating.com" class="GrayText">info@perpetuating.com</a></span></td></tr>
            </table>
        </td>
    </tr>    
</table>
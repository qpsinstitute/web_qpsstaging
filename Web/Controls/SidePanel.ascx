<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidePanel.ascx.cs" Inherits="PerpetSiteWebv2.controls.SidePanel" %>
    <div id="SectionNav" class="VerticalNav">
		<h3>About Us</h3>
		<ul>
		<li><a href="AboutUs.aspx">About Us</a>
		<li><a href="ContactUs.aspx">Contact Us</a>
			</li>
			<li><a href=TrustedPartners.aspx>Trusted Partners</a>
				<ul>
					<li><a <%if (PerpetSiteWebv2.PerpetSiteMaster.page != "ASP.aboutus_trustedpartners_aspx") { Response.Write("href='TrustedPartners.aspx#ApolloHealthStreet'"); } else { Response.Write("href='#ApolloHealthStreet'"); }%>>Apollo Health Street</a></li>
					<li><a <%if (PerpetSiteWebv2.PerpetSiteMaster.page != "ASP.aboutus_trustedpartners_aspx") { Response.Write("href='TrustedPartners.aspx#Ektron'"); } else { Response.Write("href='#Ektron'"); }%>>Ektron</a></li>
					<li><a <%if (PerpetSiteWebv2.PerpetSiteMaster.page != "ASP.aboutus_trustedpartners_aspx") { Response.Write("href='TrustedPartners.aspx#MNET'"); } else { Response.Write("href='#MNET'"); }%>>MaintainNet</a></li>
					<li><a <%if (PerpetSiteWebv2.PerpetSiteMaster.page != "ASP.aboutus_trustedpartners_aspx") { Response.Write("href='TrustedPartners.aspx#OspreyCommunications'"); } else { Response.Write("href='#OspreyCommunications'"); }%>>Osprey Communications</a></li>
					<li><a <%if (PerpetSiteWebv2.PerpetSiteMaster.page != "ASP.aboutus_trustedpartners_aspx") { Response.Write("href='TrustedPartners.aspx#QualityProductivitySolutions'"); } else { Response.Write("href='#QualityProductivitySolutions'"); }%>>Quality & Productivity Solutions</a></li>
					<li><a <%if (PerpetSiteWebv2.PerpetSiteMaster.page != "ASP.aboutus_trustedpartners_aspx") { Response.Write("href='TrustedPartners.aspx#Rackspace'"); } else { Response.Write("href='#Rackspace'"); }%>>Rackspace</a></li>
				</ul>
			</li>
			<li><a href="BoardofDirectors.aspx">Board of Directors</a>
			</li>
	    </ul>
	</div>
	
	
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="footer_New.ascx.cs" Inherits="QPS.Controls.footer_New" %>
<!-- RSS javascripts -->
    <meta http-equiv="pragma" content="no-cache"/>	
    <link rel="alternate" type="application/rss+xml" title="RSS" href="../js/LatestNews.rss" />
    <script src="../js/prototype.js" type="text/javascript" charset="utf-8"></script>
    <script src="../js/scriptaculous.js" type="text/javascript" charset="utf-8"></script>
    <script src="../js/ticker.js" type="text/javascript" charset="utf-8"></script>

<%--<style type="text/css">
body#libra {
  margin: 0 5px 0 3px;
  padding:0px;
  font:80.5% Arial,Helvetica,sans-serif; /* Sets base */
  color:#000;
  background-color:#fff;
  min-width:902px;
  }
  html body#libra {
    font:80% Arial,Helvetica,sans-serif; /* IE6 reset */
    width: expression((documentElement.clientWidth < 910) ? "902px" : "auto" ); /* IE scroll shift */
    }
pre { font-size:124.5%; } /* Reverse default PRE resizing (smaller) in Mozilla-based browsers */
  * html pre { font-size:100%; }
/* End migration from legacy.css */

img { border:0; }
a:link { color:#039; }
a:visited { color:#639; }

/***
-----------------------------------
Web Hierarchical Navigation - hinav
-----------------------------------
. defines classes: parent, peer, selected, & child.
. legend:
     li    : bottom
     li li : all others
***/

.hinav {
  position:relative;
  padding:9px 8px 21px 0;
  }
  * html .hinav { width:182px; }
.hinav h3 { display:none; }
.hinav ul {
  margin:0;
  padding:0;
  }
.hinav li {
  display:inline;
  margin:0;
  padding:0;
  list-style:none;
  }
.hinav li a, .hinav li a:visited, .hinav span {
  position:relative;
  display:block;
  margin-bottom:-1px;
  color:#2f6681;
  background-color:transparent;
  font-size:86%;
  text-decoration:none;
  border-bottom:1px solid #acc2cd;
  }

/* Bring selected to front */
.hinav li a.selected, .hinav li a.selected:visited, .hinav span.selected {
  z-index:10;
  background-color:#dde6eb;
  border-top:1px solid #c8cacc;
  border-bottom:1px solid #acc2cd;
  }

/* Cancel topmost border */
.hinav li li a, .hinav li li a:visited,  .hinav li li span {
  border-top:1px solid #d5e0e6;
  }
.hinav a, .hinav a:visited, .hinav span {
  padding:3px 6px 3px 7px;
  width:169px; /* IE redraw */
  }
.hinav a.peer, .hinav a.peer:visited,
.hinav a.selected, .hinav a.selected:visited,
.hinav span.peer, .hinav span.selected {
  padding-left:15px;
  width:161px; /* IE redraw */
  font-weight:bold;
  }
.hinav a.child, .hinav a.child:visited, .hinav span.child {
  padding-left:28px;
  width:147px; /* IE redraw */
  }

/* hinav special case for peers of parent */
.hinav.show_parent_peers a.parent_peer {
   padding-left: 15px !important;
   width: 162px !important;
   }
.hinav.show_parent_peers li a.selected {
  padding-left: 28px !important;
  width: 149px !important;
  }
</style>--%>

<br class="clear"/>
<div>
    <%--<table id="framework-wrapper" cellspacing="0" cellpadding="0" border="0" summary="">
        <tr>
            <td>
               
                <table id="framework-base-main" cellspacing="0" cellpadding="0" border="0" summary="">
                    <tr>
                        <td id="framework-column-left" valign="top">
                            <a name="hinav"></a>
                            <!-- CDC-DM: Hinav Start -->
                            <div class="hinav">                                
                                <ul class="outer">
                                    <li>
                                        <ul>
                                            <li>
                                                <ul>
                                                    <li>
                                                        <ul>
                                                            <li><span class="selected">Select Topic To See Our Training Programs</span> </li>
                                                            <ul>
                                                                <li><a href="../Training/Calendar.aspx?id=2" title="AS 9100C" class="child">AS 9100C</a></li>
                                                                <li><a href="../ELearning/EcourseList.aspx" title="E Learning" class="child">E Learning</a></li>
                                                                <li><a href="../Training/Calendar.aspx?id=4" title="FDA Related" class="child">FDA Related</a></li>
                                                                <li><a href="../Training/Calendar.aspx?id=6" title="Human Resources" class="child">Human
                                                                    Resources</a></li>
                                                                <li><a href="../Training/Calendar.aspx?id=2" title="ISO 9001:2008" class="child">ISO
                                                                    9001:2008</a></li>
                                                                <li><a href="../Training/Calendar.aspx?id=3" title="ISO 14001" class="child">ISO 14001</a></li>
                                                                <li><a href="../Training/Calendar.aspx?id=4" title="ISO 13485" class="child">ISO 13485</a></li>
                                                                <li><a href="../Training/Calendar.aspx?id=2" title="ISO/TS 16949" class="child">ISO/TS
                                                                    16949</a></li>
                                                                <li><a href="../Training/Calendar.aspx" title="Lean Six Sigma" class="child">Lean Six
                                                                    Sigma</a></li>
                                                                <li><a href="../Training/Onsite.aspx" title="Onsite Training" class="child">Onsite Training</a></li>
                                                                <li><a href="../Training/Calendar.aspx?id=9" title="Project Mgmt. Certification"
                                                                    class="child">Project Mgmt. Certification</a></li>
                                                                <li><a href="../Training/Training.aspx" title="Public Training" class="child">Public
                                                                    Training</a></li>
                                                                <li><a href="../Training/Calendar.aspx?id=6" title="Service" class="child">Service</a></li>
                                                                <li><a href="../Training/Calendar.aspx?id=8" title="Software" class="child">Software</a></li>
                                                                <li><a href="../Training/Calendar.aspx?id=5" title="Statistics" class="child">Statistics</a></li>
                                                            </ul>
                                                    </li>
                                                    <li><strong><span class="selected">Quick Connect >></span></strong>
                                                        <ul>
                                                            <li><a href="../Company/about.aspx" title="About Us" class="child">About Us</a></li>
                                                            <li><a href="../Consulting/QualitySystem.aspx" title="AS 9100" class="child">AS 9100</a></li>
                                                            <li><a href="../Training/Calendar.aspx" title="Calendar" class="child">Calendar</a></li>
                                                            <li><a href="../Company/Clients.aspx" title="Clients" class="child">Clients</a></li>
                                                            <li><a href="../Consulting/Consulting.aspx" title="Consulting" class="child">Consulting</a></li>
                                                            <li><a href="../Company/Contact.aspx" title="Contact Us" class="child">Contact Us</a></li>
                                                            <li><a href="../Consulting/QualitySystem.aspx" title="FDA" class="child">FDA</a></li>
                                                            <li><a href="../Consulting/QualitySystem.aspx" title="ISO 14000" class="child">ISO 14000</a></li>
                                                            <li><a href="../Consulting/QualitySystem.aspx" title="ISO 9001:2008" class="child">ISO
                                                                9001:2008</a></li>
                                                            <li><a href="../Consulting/QualitySystem.aspx" title="ISO 13485" class="child">ISO 13485</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=2" title="ISO/TS 16949" class="child">ISO/TS
                                                                16949</a></li>
                                                            <li><a href="../Consulting/QualitySystem.aspx" title="ISO 27001" class="child">ISO 27001</a></li>
                                                            <li><a href="../Consulting/BusinessImprovement.aspx" title="Leadership" class="child">
                                                                Leadership</a></li>
                                                            <li><a href="../Resources/Links.aspx" title="Links" class="child">Links</a></li>
                                                            <li><a href="../Training/Calendar.aspx" title="Map & Directions" class="child">Map &
                                                                Directions</a></li>
                                                            <li><a href="../Consulting/QualitySystem.aspx" title="NQA" class="child">NQA</a></li>
                                                            <li><a href="../Consulting/BusinessImprovement.aspx" title="Outsourcing" class="child">
                                                                Outsourcing</a></li>
                                                            <li><a href="../Consulting/BusinessImprovement.aspx" title="Project Management" class="child">
                                                                Project Management</a></li>
                                                            <li><a href="../Consulting/QualitySystem.aspx" title="Quality System" class="child">
                                                                Quality System</a></li>
                                                            <li><a href="../Training/Registration.aspx" title="Register" class="child">Register</a></li>
                                                            <li><a href="../Consulting/BusinessImprovement.aspx" title="Strategic Planning" class="child">
                                                                Strategic Planning</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=6" title="Supply Chain/Mfg" class="child">
                                                                Supply Chain/Mfg</a></li>
                                                            <li><a href="../Consulting/BusinessImprovement.aspx" title="Teams" class="child">Teams</a></li>
                                                            <li><a href="../Company/Testimonials.aspx" title="Testimonials" class="child">Testimonials</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>                                    
                            </div>
                            <!-- CDC-DM: Hinav End -->
                        </td>                     
                    </tr>
                </table>
            </td>
        </tr>
    </table>--%>



	<br/>
	<%--<div class="span-5 prepend-1 colborder">
	    <h3 style="line-height:140%;"><span style="color:rgb(0,96,165);">Select topic</span> to see our training programs</h3>
		<h5>&nbsp;</h5><h5>&nbsp;</h5><h6>&nbsp;</h6><br/>
	</div>
	<div class="span-4 colborder upp">
	    <h4><a href="../Training/Calendar.aspx?id=2" title="AS 9100">AS 9100</a></h4> 	    
	    <h4><a href="../ELearning/EcourseList.aspx" title="E Learning">E Learning</a></h4> 
	    <h4><a href="../Training/Calendar.aspx?id=4" title="FDA Related">FDA Related</a></h4>
	    <h4><a href="../Training/Calendar.aspx?id=6" title="Human Resources">Human Resources</a></h4>
		<h4><a href="../Training/Calendar.aspx?id=2" title="ISO 9001:2008">ISO 9001:2008</a></h4>
  	</div>
	<div class="span-4 colborder upp">  	    
  	    <h4><a href="../Training/Calendar.aspx?id=3" title="ISO 14001">ISO 14001</a></h4> 
  	    <h4><a href="../Training/Calendar.aspx?id=4" title="ISO 13485">ISO 13485</a></h4>
        <h4><a href="../Training/Calendar.aspx?id=2" title="ISO/TS 16949">ISO/TS 16949</a></h4>         
        <h4><a href="../Training/Calendar.aspx" title="Lean Six Sigma">Lean Six Sigma</a></h4>
        <h4><a href="../Training/Onsite.aspx" title="Onsite Training">Onsite Training</a></h4>  
	   	    
  	</div>
	<div class="span-6 colborder upp">
	   <h4><a href="../Training/Calendar.aspx?id=9" title="Project Mgmt. Certification">Project Mgmt. Certification</a></h4>	     		
        <h4><a href="../Training/Training.aspx" title="Public Training">Public Training</a></h4>
        <h4><a href="../Training/Calendar.aspx?id=6" title="Service">Service</a></h4>
        <h4><a href="../Training/Calendar.aspx?id=8" title="Software">Software</a></h4>
        <h4><a href="../Training/Calendar.aspx?id=5" title="Statistics">Statistics</a></h4>  	    
  	</div>--%>
<br class="clear">
	<div class="span-24 last" style="width:963px;padding:12px;background:#C8C8C8;height:22px;">
	    <div class="span-4 colborder prepend-1"><h5 style="margin-bottom:0;padding-bottom:0;"><b>Hot News Headlines</b></h5></div>
	    <div class="span-17"><div id="ticker"><a href="/News/LatestNews1.aspx" id="news-link">Read the latest news and information.</a></div></div>
	</div>
	<br class="clear">
	<%--<div class="span-24 last" style="width:980px;padding:5px;background:#fafafa;">
	    <div class="span-5 prepend-1">
	        <h3 class="splfn" style="margin-bottom:5px;">2009 Course Calendar released</h3>
	        <p>
	            We are proud to release our 2009 public training program with over 80 courses. <a style="font-size:15px;" href="../Documents/calender-Training 2009.pdf" target="_blank" title="Download">Download</a> our course program for more details.
	        </p>
	    </div>
	    <div class="span-18 last" style="padding:5px;border:1px solid silver;line-height:2em">
		    <h4  style="border-bottom:1px solid #dadada;margin-bottom:3px;padding:5px;color:#3A5087;"><b>Quick Connect &raquo;</b></h4>		    
			<div class="span-3 colborder">
			    <a href="../Company/about.aspx" title="About Us">About Us</a><br>
			    <a href="../Consulting/QualitySystem.aspx" title="AS 9100">AS 9100</a><br>
			    <a href="../Training/Calendar.aspx" title="Calendar">Calendar</a><br>
			    <a href="../Company/Clients.aspx" title="Clients">Clients</a><br>
			    <a href="../Consulting/Consulting.aspx" title="Consulting">Consulting</a><br>
			    <a href="../Company/Contact.aspx" title="Contact Us">Contact Us</a><br>			    			    
			</div>			
			<div class="span-4 colborder">			    
			    <a href="../Consulting/QualitySystem.aspx" title="FDA">FDA</a><br>
			    <a href="../Consulting/QualitySystem.aspx" title="ISO 14000">ISO 14000</a><br>			    
			    <a href="../Consulting/QualitySystem.aspx" title="ISO 9001:2008">ISO 9001:2008</a><br>
			    <a href="../Consulting/QualitySystem.aspx" title="ISO 13485">ISO 13485</a><br>
			    <a href="../Training/Calendar.aspx?id=2" title="ISO/TS 16949">ISO/TS 16949</a><br>
			    <a href="../Consulting/QualitySystem.aspx" title="ISO 27001">ISO 27001</a><br>
			</div>			
			<div class="span-4 colborder">
			    <a href="../Consulting/BusinessImprovement.aspx" title="Leadership">Leadership</a><br>
			    <a href="../Resources/Links.aspx" title="Links">Links</a><br>
			    <a href="../Training/Calendar.aspx" title="Map & Directions">Map & Directions</a><br>			
			    <a href="../Consulting/QualitySystem.aspx" title="NQA">NQA</a><br>
			    <a href="../Consulting/BusinessImprovement.aspx" title="Outsourcing">Outsourcing</a><br />
			    <a href="../Consulting/BusinessImprovement.aspx" title="Project Management">Project Management</a><br />
			</div>			
			<div class="span-4 last">
			    <a href="../Consulting/QualitySystem.aspx" title="Quality System">Quality System</a><br>
			    <a href="../Training/Registration.aspx" title="Register">Register</a><br />
				<a href="../Consulting/BusinessImprovement.aspx" title="Strategic Planning">Strategic Planning</a><br />			
			    <a href="../Consulting/BusinessImprovement.aspx" title="Supply Chain">Supply Chain</a><br />				
			    <a href="../Consulting/BusinessImprovement.aspx" title="Teams">Teams</a><br />
			    <a href="../Company/Testimonials.aspx" title="Testimonials">Testimonials</a><br>
								
			</div>
	    </div>
	</div>--%>
</div>
<div class="span-24 last">
    <div style="width: 972px; padding: 8px; height: 15px; text-align: left; font-size: 8pt;
        color: white; background: gray; border-bottom: 15px solid #eaeaea">
        <div style="float: left;">
            � <%= System.DateTime.Now.Year %> Quality & Productivity Solutions, Inc. All Rights Reserved</div>
    </div>
</div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
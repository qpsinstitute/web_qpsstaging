<%@ Page AutoEventWireup="true" Codebehind="EcourseList.aspx.cs" Inherits="QPS.ELearning.EcourseList" Language="C#" MasterPageFile="~/QPSSite.Master" %>

<%@ Register Src="~/Controls/Sidebar_ELearning.ascx" TagName="ELearning" TagPrefix="QPSSidebar" %>
<%@ Register Src="~/Controls/Sidebar.ascx" TagName="Sidebar" TagPrefix="QPSSidebar" %>
<asp:Content ID="ContentEcourseList" runat="server" ContentPlaceHolderID="cphContent">
 <script language="javascript" type="text/javascript">
    setPage("eLearning");
    </script>

    <div class="span-24 prepend-1 top highcont">

    <div class="span-18 top" style="float:right">

                <h3 id="training" class="colbright" style="padding-top: 0px;">
                    eLearning</h3>
                <p>
                    <span style="font-size: 125%">Learn today�s hottest topics at your own pace and without taking time away from your job. At work or at home, you can now learn what others are learning and not have to worry about your job. You will have three months to complete the program in your free time. Enhance your career by clicking on one of the courses and follow the instructions. </span>
                </p>

                <table class="mainText">
                    <tr>
                        <td>
                            <%-- <%=EcourseLists%>   --%>

                            <table class="mainText" style="border:1px dotted silver;background:#ffffff;">
                                <tr>
                                    <td style="padding-left: 10px; padding-right: 25px; padding-top: 5px; padding-bottom: 15px;" width="100%">
                                        <table id="Table1" cellpadding="0" cellspacing="0" width="410">
                                            <tr>
                                                <form id="FormESSIMOV" action="<%= FormSubmitURL %>" method="post" name="FormESSIMOV">
                                                    <input id="Hidden1" name="courseId" type="hidden" value="ESSIMOV">
                                                    <input id="Hidden2" name="cost" type="hidden" value="2200">
                                                    <input id="Hidden3" name="courseTitle" type="hidden" value="Six sigma green belt certification">
                                                </form>
                                                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                                                    <strong>Six Sigma Overview</strong>
                                                </td>
                                                <td bgcolor="white" width="2">
                                                </td>
                                                <td align="center" bgcolor="#eaeaea" valign="middle">
                                                   <a href="#" onclick="document.FormESSIMOV.submit(); return false;">
                                                        <img src='/Images/addtocart.gif'></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class='eCourseCostTD' colspan="2">
                                                    <strong>$295.00</strong>
                                                </td>
                                                <td align="right">
                                                    <a href="/ELearning/SixSigmaOverview.aspx">
                                                        <img border="0" src='/Images/ViewCourseOutLine.gif'></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="eCourseRow3" colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="3">
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellpadding="0" cellspacing="0" width="410">
                                            <tr>
                                                <form id="FormESSIGBC" action="<%= FormSubmitURL %>" method="post" name="FormESSIGBC">
                                                    <input name="courseId" type="hidden" value="ESSIGBC">
                                                    <input name="cost" type="hidden" value="2200">
                                                    <input name="courseTitle" type="hidden" value="Six sigma green belt certification">
                                                </form>
                                                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                                                    <strong>Sigma Green Belt Certification</strong>
                                                </td>
                                                <td bgcolor="white" width="2">
                                                </td>
                                                <td align="center" bgcolor="#eaeaea" valign="middle">
                                                    <a href="#" onclick="document.FormESSIGBC.submit(); return false;">
                                                        <img src='/Images/addtocart.gif'></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class='eCourseCostTD' colspan="2">
                                                    <strong>$1195.00</strong>
                                                </td>
                                                <td align="right">
                                                    <a href="/ELearning/SixSigmaGreenBelt.aspx">
                                                        <img border="0" src='/Images/ViewCourseOutLine.gif'></a></td>
                                            </tr>
                                            <tr>
                                                <td class="eCourseRow3" colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="3">
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellpadding="0" cellspacing="0" width="410">
                                            <tr>
                                                <form id="FormESSIBBC" action="<%= FormSubmitURL %>" method="post" name="FormESSIBBC">
                                                    <input name="courseId" type="hidden" value="EQSIPCM">
                                                    <input name="cost" type="hidden" value="2200">
                                                    <input name="courseTitle" type="hidden" value="Six sigma black black certification">
                                                </form>
                                                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                                                    <strong>Process Concept and Mapping</strong></td>
                                                <td bgcolor="white" width="2">
                                                </td>
                                                <td align="center" bgcolor="#eaeaea" valign="middle">
                                                    <a href="#" onclick="document.FormESSIBBC.submit(); return false;">
                                                        <img src='/Images/addtocart.gif'></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class='eCourseCostTD' colspan="2">
                                                    <strong>$195.00</strong></td>
                                                <td align="right">
                                                    <a href="/ELearning/ProcessConceptandMapping.aspx">
                                                        <img border="0" src='/Images/ViewCourseOutLine.gif'></a></td>
                                            </tr>
                                            <tr>
                                                <td class="eCourseRow3" colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="3">
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellpadding="0" cellspacing="0" width="410">
                                            <tr>
                                                <form id="FormESSIFCH" action="<%= FormSubmitURL %>" method="post" name="FormESSIFCH">
                                                    <input name="courseId" type="hidden" value="EQSBAI9">
                                                    <input name="cost" type="hidden" value="600">
                                                    <input name="courseTitle" type="hidden" value="Lean">
                                                </form>
                                                <td class="eCourseTitleTD" style="background-color:#eaeaea; width:388px;">
                                                    <strong>Auditing to ISO 9001-2000</strong></td>
                                                <td bgcolor="white" width="2">
                                                </td>
                                                <td align="center" bgcolor="#eaeaea" valign="middle">
                                                    <a href="#" onclick="document.FormESSIFCH.submit(); return false;">
                                                        <img src='/Images/addtocart.gif'></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class='eCourseCostTD' colspan="2">
                                                    <strong>$295.00</strong></td>
                                                <td align="right" style="padding-right:10px;">
                                                    <a href="/ELearning/AuditingtoISO9001-2000.aspx">
                                                        <img border="0" src='/Images/ViewCourseOutLine.gif'></a></td>
                                            </tr>
                                            <tr>
                                                <td class="eCourseRow3" colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="3">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

    </div>

		<div class="span-4">
		<QPSSidebar:ELearning ID="Sidebar_ELearning" runat="server" />
		</div>

 </div>

</asp:Content>

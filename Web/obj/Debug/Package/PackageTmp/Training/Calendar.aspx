<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Calendar.aspx.cs"
    Inherits="QPS.Training.Calendar" %>

<asp:Content ID="ContentCalendar" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Training");
    </script>
    <style type="text/css">
        :-moz-any-link:focus
        {
            outline: none;
        }
        a
        {
            outline: none;
        }
        a:active
        {
            outline: none;
            -moz-outline-style: none;
        }
        a:focus
        {
            outline: none;
            -moz-outline-style: none;
        }
    </style>
    <script language="javascript" type="text/javascript">

        var lastSelectedCategoryID = 0;
        function SetCurrentTab(CategoryID) {
            if (lastSelectedCategoryID == 0) {
                document.getElementById("lstTabs").childNodes[1].className = "";
                document.getElementById("lstTabs").childNodes[1].style.fontWeight = "normal";
            }

            document.getElementById("li" + CategoryID).className = "Current";
            document.getElementById("li" + CategoryID).style.fontWeight = "bold";

            document.getElementById("li" + lastSelectedCategoryID).className = "";
            document.getElementById("li" + lastSelectedCategoryID).style.fontWeight = "normal";

            lastSelectedCategoryID = CategoryID;
        }

        function CheckOut(CourseCode, Title, Cost) {
        
            document.getElementById("hdnCourseCode").value = CourseCode;
            document.getElementById("hdnTitle").value = Title;
            document.getElementById("hdnCost").value = Cost;
            document.FormCheckOut.submit();
        }

        function OpenPopupWindow(CourseCode) {
            window.open("CourseOverview.aspx?CourseCode=" + CourseCode, "_blank", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width=600,height=500");
            return false;
        }  
      
    </script>
    <script type="text/javascript" language="javascript1.2" src="../js/qfunctions.js"></script>
    <%--  <form id="FormCheckOut" action="https://elearningweb.staging2.perpetuating.com/youraccount/RegistrationSignIn.aspx"
    method="post" name="FormCheckOut">--%>
      <form id="FormCheckOut" action="https://online.qpsinc.com/youraccount/RegistrationSignIn.aspx"
    method="post" name="FormCheckOut">
    <%-- <form id="Form1" action="http://localhost:6598/youraccount/RegistrationSignIn.aspx"
    method="post" name="FormCheckOut">--%>
    <input id="hdnCourseCode" name="courseId" type="hidden" />
    <input id="hdnTitle" name="courseTitle" type="hidden" />
    <input id="hdnCost" name="cost" type="hidden" />
    </form>
    <form id="frm11" runat="server">
    <ajax:ScriptManager ID="ScriptManager1" runat="server">
    </ajax:ScriptManager>
    <ajax:UpdateProgress ID="UPGR1" runat="Server">
        <ProgressTemplate>
            <div id="loading" style="position: absolute; top: 70%; left: 40%;">
                <img alt="Loading...." src="/images/ajax-loader.gif">
            </div>
        </ProgressTemplate>
    </ajax:UpdateProgress>
    <div class="span-24 prepend-1 highcont top">
        <div class="span-18 top" style="float: right">
            <%--<div class="span-3 blackbl last" style="width:80px"><a href="Institute.aspx" title="QPS Institute">QPS Institute</a></div>--%>
               <div class="span-3 blackbl last" style="width: 130px">
                <a href="Registration.aspx" title="Corporate Register">Corporate Register</a></div>
         <div class="span-3 blackbl last">
                <a href="Training.aspx" title="Training Grants">Training Grants</a></div>
                <div class="span-3 blackbl last" style="width: 87px">
                <a href="/ELearning/EcourseList.aspx" title="eLearning">eLearning</a></div>
                <div class="span-3 blackbl last" style="width: 100px">
                <a href="OnSite.aspx" title="On-Site Training">On-Site Training</a></div>
                <div class="span-3 blackbl last" style="width: 120px">
                <a href="UnEmployed.aspx" title="Public Training">Public Training</a></div>
            <div class="span-3 blackbk last" style="width: 123px">
                <a title="Corporate Calendar">Corporate Calendar</a></div>
            
            
            
            
            <%--  <div class="span-3 blackbl last" style="width: 110px">
                <a href="../Instructor/Signin.aspx" title="Instructor Login">Instructor Login</a></div>--%>

            <br class="clear" />
            <br class="clear" />
            <h3 class="colbright" id="calendar">
                Corporate Training Calendar</h3>
            <h4>
                QPS is pleased to offer the following Training Courses for Winter/Spring
                2014. Course brochures can be viewed and printed by clicking the name of the course.
                Additional courses and locations will be scheduled upon request. Please <a class="nounder"
                    href="../Documents/corporate calender Jan-June 2016.pdf" target="_blank" title="Click here to view our Public Training Course Schedule">
                    <b>Click here</b></a> to view our Public Training Course Schedule. For any questions
                or information regarding training grants, please <a href="mailto:info@qpsinc.com">contact us</a>.
            </h4>
            <br />
            <br class="clear" />
            <br class="clear" />
            <div class="span-3 last" style="background: gray;">
                <h2 class="colwhi" style="margin: 0; <%--padding-top: 5px;--%> text-align: center; font-size: 18px; line-height: 19px; padding-bottom: 2px;">
                    Corporate Register</h2>
            </div>
            <div class="span-12">
                <div class="span-12" style="border: 1px solid silver; padding: 2px; padding-left: 5px;
                    padding-bottom: 1px; margin-bottom: 7px;">
                    1. To use Credit Card click 'add to cart' at the end of the course line.<br />
                    2. To pay by check or purchase order use <b><a href="../documents/QPS Registration form.doc" class="nounder"
                        title="Registration Form">Registration Form</a></b>.
                </div>
            </div>
            <br class="clear" />
            <h4 style="margin-top: 5px;" class="rightarrowed">
                <a href="/Company/Contact.aspx" class="nounder" title="Course Location and Direction">
                    Course Location and Direction</a>
                    </h4>
                       <h4 style="margin-top: 5px;" class="rightarrowed">
                     <a href="Notification.aspx" class="nounder" title="Click to get notification for selected course">
                    Click to get notification for selected course</a>
                      </h4>
                       <h4 style="margin-top: 5px;" class="rightarrowed">
                     <a href="../Documents/ISO 2001 - 2015 Flyer.pdf" target="_blank" class="nounder" title="ISO consulting and training">
                   ISO consulting and training</a>
                      </h4>
        </div>
        <div class="span-5">
            <img alt="" src="../images/training.gif" border="0" class="pull-1 top" />
        </div>
        <div class="span-24 pull-1 last">
            <ajax:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="Tabs">
                        <asp:Repeater ID="rptCategory" runat="server">
                            <HeaderTemplate>
                                <ul id="lstTabs">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li id="li<%# Eval("CategoryID").ToString() %>">
                                    <asp:LinkButton ID="lnkCategory" runat="server" CommandArgument='<%# Eval("CategoryID").ToString() %>'
                                        OnClick="lnkCategory_Click" onmouseout="style.textDecoration='none'" onmouseover="style.textDecoration='underline'"
                                        Text='<%# Eval("Name") %>'></asp:LinkButton>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul></FooterTemplate>
                        </asp:Repeater>
                        <script language="javascript">
                            document.getElementById("lstTabs").childNodes[1].className = "Current";
                            document.getElementById("lstTabs").childNodes[1].style.fontWeight = "bold";
                        </script>
                    </div>
                    <div style="border-left: 1px solid silver; border-bottom: 1px solid silver; padding: 10px;">
                        <asp:Repeater ID="rptDetails" runat="server">
                            <HeaderTemplate>
                                <table style="width: 100%;" class="CourseTab">
                                    <thead>
                                        <tr>
                                            <th width="325px;">
                                                Course
                                            </th>
                                            <th>
                                                Course No.
                                            </th>
                                            <th>
                                                Days
                                            </th>
                                            <th>
                                                Dates
                                            </th>
                                            <th>
                                                Location
                                            </th>
                                            <th>
                                                Add
                                            </th>
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="oddRow">
                                    <td style="width: 325px;">
                                        <a href="#" onmouseover="style.color='black'" onmouseout="style.color='#004080'"
                                            forecolor="#004080" width="325px" font-bold="true" font-underline="false" onclick="return OpenPopupWindow('<%# Eval("CourseCode")%>')">
                                            <%# Eval("CourseTitle")%>
                                        </a>
                                    </td>
                                    <td style="width: 100px; text-align: left; padding-left: 30px;">
                                        <%# Eval("CourseNo")%>
                                    </td>
                                    <td style="width: 58px; text-align: left; padding-left: 20px;">
                                        <%# Eval("Days") %>
                                    </td>
                                    <td style="width: 170px; text-align: center;">
                                        <%# Eval("Dates").ToString().Replace("\n","<br/>")%>
                                    </td>
                                    <td style="width: 140px; text-align: center;">
                                        <%# Eval("Locations").ToString().Replace("\n", "<br/>")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="#" onclick="CheckOut('<%#Eval("CourseCode")%>','<%# Eval("CourseTitle") %>','<%#Eval("Cost") %>')">
                                            <img alt="Add to Cart" src='/Images/addtocart.gif' title="ADD TO CART"></a>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </ContentTemplate>
            </ajax:UpdatePanel>
            <br />
            <div id="coursefooter">
                <span class="small colblu" style="color:#004080">Please Contact us for 
                additional courses not listed, details and different location schedules as we 
                have the ability to add new courses, locations and times.</span>
                    <br />
                
                <span class="small colblu" style="color:#004080">* All courses will be held from </span><font style="font-weight: bold;
                    color: #004080;"><%= ConfigurationManager.AppSettings["seminarTime"]%> </font>
                <span class="small colblu" style="color:#004080">unless stated otherwise. QPS has 
                the right to change times based
                    on customer request.
                    <br />
                    For any questions, please feel free to contact our customer service at <a href="mailto:info@qpsinc.com">
                        info@qpsinc.com</a>.
                    <br />
                </span>
            </div>
            <div style="float: right; text-align: right; padding-right: 10px; padding-bottom: 5px;">
                <a class="nounder" href="#top" title="Top">
                    <img alt="" src="../images/top.gif" title="Top" style="margin: 0; padding: 0;" /></a></div>
        </div>
    </div>
    <script type="text/javascript">
        function getParameterValue(ParameterName) {
        
            ParameterName = ParameterName.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + ParameterName + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.href);
            if (results == null)
                return 1;
            else
                return results[1];
        }

        var CurrentTabValue = getParameterValue("id");

        
        SetCurrentTab(CurrentTabValue);
    </script>
    </form>
</asp:Content>

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/QPSSite.Master" CodeBehind="UnEmployed.aspx.cs"
    Inherits="QPS.Training.UnEmployed" %>

<asp:Content ID="ContentReg" runat="server" ContentPlaceHolderID="cphContent">
    <style>
        .Grid
        {
            border-left: 1px solid silver;
            border-bottom: 1px solid silver;
            padding: 10px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        setPage("Training");

        function SelectDiv(no) {
            if (no == 1) {
                document.getElementById("Massachusetts").style.display = 'block';
                document.getElementById("Rhode").style.display = 'none';
                document.getElementById("Connecticut").style.display = 'none';
                document.getElementById("New Jersey").style.display = 'none';
                document.getElementById("North Carolina").style.display = 'none';
                document.getElementById("Illinois").style.display = 'none';
                document.getElementById("New York").style.display = 'none';
                document.getElementById("Massachusetts").style.height = 'auto';
                document.getElementById("Rhode").style.height = '0px';
                document.getElementById("Connecticut").style.height = '0px';
                document.getElementById("New Jersey").style.height = '0px';
                document.getElementById("North Carolina").style.height = '0px';
                document.getElementById("Illinois").style.height = '0px';
                document.getElementById("New York").style.height = '0px';

                //                document.getElementById("tab1").className = 'span-3 blackbk last';
                //                document.getElementById("tab2").className = 'span-3 blackbl last';
                //                document.getElementById("tab3").className = 'span-3 blackbl last';
                //                document.getElementById("tab4").className = 'span-3 blackbl last';
                //                document.getElementById("tab5").className = 'span-3 blackbl last';
                //                document.getElementById("tab6").className = 'span-3 blackbl last';
                //                document.getElementById("tab7").className = 'span-3 blackbl last';
            }
            else if (no == 2) {
                document.getElementById("Massachusetts").style.display = 'none';
                document.getElementById("Rhode").style.display = 'block';
                document.getElementById("Connecticut").style.display = 'none';
                document.getElementById("North Carolina").style.display = 'none';
                document.getElementById("Illinois").style.display = 'none';
                document.getElementById("New York").style.display = 'none';
                document.getElementById("New Jersey").style.display = 'none';
                document.getElementById("Massachusetts").style.height = '0px';
                document.getElementById("Rhode").style.height = 'auto';
                document.getElementById("Connecticut").style.height = '0px';
                document.getElementById("North Carolina").style.height = '0px';
                document.getElementById("Illinois").style.height = '0px';
                document.getElementById("New York").style.height = '0px';

                //                document.getElementById("tab1").className = 'span-3 blackbl last';
                //                document.getElementById("tab2").className = 'span-3 blackbk last';
                //                document.getElementById("tab3").className = 'span-3 blackbl last';
                //                document.getElementById("tab4").className = 'span-3 blackbl last';
                //                document.getElementById("tab5").className = 'span-3 blackbl last';
                //                document.getElementById("tab6").className = 'span-3 blackbl last';
                //                document.getElementById("tab7").className = 'span-3 blackbl last';
            }
            else if (no == 3) {
                document.getElementById("Massachusetts").style.display = 'none';
                document.getElementById("Rhode").style.display = 'none';
                document.getElementById("Connecticut").style.display = 'block';
                document.getElementById("New Jersey").style.display = 'none';
                document.getElementById("North Carolina").style.display = 'none';
                document.getElementById("Illinois").style.display = 'none';
                document.getElementById("New York").style.display = 'none';
                document.getElementById("Massachusetts").style.height = '0px';
                document.getElementById("Rhode").style.height = '0px';
                document.getElementById("Connecticut").style.height = 'auto';
                document.getElementById("New Jersey").style.height = '0px';
                document.getElementById("North Carolina").style.height = '0px';
                document.getElementById("Illinois").style.height = '0px';
                document.getElementById("New York").style.height = '0px';

                //                document.getElementById("tab1").className = 'span-3 blackbl last';
                //                document.getElementById("tab2").className = 'span-3 blackbl last';
                //                document.getElementById("tab3").className = 'span-3 blackbk last';
                //                document.getElementById("tab4").className = 'span-3 blackbl last';
                //                document.getElementById("tab5").className = 'span-3 blackbl last';
                //                document.getElementById("tab6").className = 'span-3 blackbl last';
                //                document.getElementById("tab7").className = 'span-3 blackbl last';
            }
            else if (no == 4) {
                document.getElementById("Massachusetts").style.display = 'none';
                document.getElementById("Rhode").style.display = 'none';
                document.getElementById("Connecticut").style.display = 'none';
                document.getElementById("New Jersey").style.display = 'none';
                document.getElementById("North Carolina").style.display = 'block';
                document.getElementById("Illinois").style.display = 'none';
                document.getElementById("New York").style.display = 'none';
                document.getElementById("Massachusetts").style.height = '0px';
                document.getElementById("Rhode").style.height = '0px';
                document.getElementById("Connecticut").style.height = '0px';
                document.getElementById("New Jersey").style.height = '0px';
                document.getElementById("North Carolina").style.height = 'auto';
                document.getElementById("Illinois").style.height = '0px';
                document.getElementById("New York").style.height = '0px';

                //                document.getElementById("tab1").className = 'span-3 blackbl last';
                //                document.getElementById("tab2").className = 'span-3 blackbl last';
                //                document.getElementById("tab3").className = 'span-3 blackbl last';
                //                document.getElementById("tab4").className = 'span-3 blackbk last';
                //                document.getElementById("tab5").className = 'span-3 blackbl last';
                //                document.getElementById("tab6").className = 'span-3 blackbl last';
                //                document.getElementById("tab7").className = 'span-3 blackbl last';
            }
            else if (no == 5) {
                document.getElementById("Massachusetts").style.display = 'none';
                document.getElementById("Rhode").style.display = 'none';
                document.getElementById("Connecticut").style.display = 'none';
                document.getElementById("New Jersey").style.display = 'none';
                document.getElementById("North Carolina").style.display = 'none';
                document.getElementById("Illinois").style.display = 'block';
                document.getElementById("New York").style.display = 'none';
                document.getElementById("Massachusetts").style.height = '0px';
                document.getElementById("Rhode").style.height = '0px';
                document.getElementById("Connecticut").style.height = '0px';
                document.getElementById("New Jersey").style.height = '0px';
                document.getElementById("North Carolina").style.height = '0px';
                document.getElementById("Illinois").style.height = 'auto';
                document.getElementById("New York").style.height = '0px';

                //                document.getElementById("tab1").className = 'span-3 blackbl last';
                //                document.getElementById("tab2").className = 'span-3 blackbl last';
                //                document.getElementById("tab3").className = 'span-3 blackbl last';
                //                document.getElementById("tab4").className = 'span-3 blackbl last';
                //                document.getElementById("tab5").className = 'span-3 blackbk last';
                //                document.getElementById("tab6").className = 'span-3 blackbl last';
                //                document.getElementById("tab7").className = 'span-3 blackbl last';
            }

            //Added New Jersey
            else if (no == 7) {
                document.getElementById("Massachusetts").style.display = 'none';
                document.getElementById("Rhode").style.display = 'none';
                document.getElementById("Connecticut").style.display = 'none';
                document.getElementById("New Jersey").style.display = 'block';
                document.getElementById("North Carolina").style.display = 'none';
                document.getElementById("Illinois").style.display = 'none';
                document.getElementById("New York").style.display = 'none';
                document.getElementById("Massachusetts").style.height = '0px';
                document.getElementById("Rhode").style.height = '0px';
                document.getElementById("Connecticut").style.height = '0px';
                document.getElementById("New Jersey").style.height = 'auto';
                document.getElementById("North Carolina").style.height = '0px';
                document.getElementById("Illinois").style.height = '0px';
                document.getElementById("New York").style.height = '0px';

                //                document.getElementById("tab1").className = 'span-3 blackbl last';
                //                document.getElementById("tab2").className = 'span-3 blackbl last';
                //                document.getElementById("tab3").className = 'span-3 blackbl last';
                //                document.getElementById("tab4").className = 'span-3 blackbl last';
                //                document.getElementById("tab5").className = 'span-3 blackbl last';
                //                document.getElementById("tab6").className = 'span-3 blackbl last';
                //                document.getElementById("tab7").className = 'span-3 blackbk last';
            }
            //// over ///
            else {
                document.getElementById("Massachusetts").style.display = 'none';
                document.getElementById("Rhode").style.display = 'none';
                document.getElementById("Connecticut").style.display = 'none';
                document.getElementById("New Jersey").style.display = 'none';
                document.getElementById("North Carolina").style.display = 'none';
                document.getElementById("Illinois").style.display = 'none';
                document.getElementById("New York").style.display = 'block';
                document.getElementById("Massachusetts").style.height = '0px';
                document.getElementById("Rhode").style.height = '0px';
                document.getElementById("Connecticut").style.height = '0px';
                document.getElementById("New Jersey").style.height = '0px';
                document.getElementById("North Carolina").style.height = '0px';
                document.getElementById("Illinois").style.height = '0px';
                document.getElementById("New York").style.height = 'auto';

                //                document.getElementById("tab1").className = 'span-3 blackbl last';
                //                document.getElementById("tab2").className = 'span-3 blackbl last';
                //                document.getElementById("tab3").className = 'span-3 blackbl last';
                //                document.getElementById("tab4").className = 'span-3 blackbl last';
                //                document.getElementById("tab5").className = 'span-3 blackbl last';
                //                document.getElementById("tab6").className = 'span-3 blackbk last';
                //                document.getElementById("tab7").className = 'span-3 blackbl last';

            }
        }
        function DIV1_onclick() { }
    </script>
    <script type="text/javascript">
        var lastSelectedCategoryID = 1;
        function getParameterValue(ParameterName) {
            ParameterName = ParameterName.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + ParameterName + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.href);
            if (results == null)
                return 1;
            else
                return results[1];
        }

        var CurrentTabValue = getParameterValue("id");

        SetCurrentTab(CurrentTabValue);

        function SetCurrentTab(CategoryID) {
            //            if (CategoryID == 1) {
            //                document.getElementById("lstTabs").childNodes[1].className = "";
            //                document.getElementById("lstTabs").childNodes[1].style.fontWeight = "normal";

            //            }
            //            else {
            //                alert(document.getElementById("li" + CategoryID) + lastSelectedCategoryID);
            document.getElementById("li" + CategoryID).className = "Current";
            document.getElementById("li" + CategoryID).style.fontWeight = "bold";

            document.getElementById("li" + lastSelectedCategoryID).className = "";
            document.getElementById("li" + lastSelectedCategoryID).style.fontWeight = "normal";


            //            }
            lastSelectedCategoryID = CategoryID;
        }
    </script>
    <div class="span-24 prepend-1 highcont top">
        <div class="span-18 top" style="float: right">
            <div class="span-3 blackbl last" style="width: 130px">
                <a href="Registration.aspx" title="Corporate Register">Corporate Register</a></div>
            <div class="span-3 blackbl last">
                <a href="Training.aspx" title="Training Grants">Training Grants</a></div>
            <div class="span-3 blackbl last" style="width: 87px">
                <a href="/ELearning/EcourseList.aspx" title="eLearning">eLearning</a></div>
            <div class="span-3 blackbl last" style="width: 100px">
                <a href="OnSite.aspx" title="On-Site Training">On-Site Training</a></div>
            <div class="span-3 blackbk last" style="width: 120px">
                <a title="Public Training">Public Training</a></div>
            <div class="span-3 blackbl last" style="width: 123px">
                <a href="Calendar.aspx" title="Corporate Calendar">Corporate Calendar</a></div>
            <%--    <div class="span-3 blackbl last" style="width: 110px">
                <a href="../Instructor/Signin.aspx" title="Instructor Login">Instructor Login</a></div>--%>
            <br class="clear">
            <br class="clear">
            <%-- <h3 class="colbright" id="training">
                Training Grants</h3>--%>
            <h3 id="registration" class="colbright">
                QPS � An Approved WIA Training Provider
            </h3>
            <p style="padding-right: 25px;">
                Quality & Productivity Solutions, Inc. is an approved training provider in several
                states that works with each state�s Department of Labor�s Division of Unemployment
                & Training�s Career Center Systems.
            </p>
            <h3 id="H3_1" class="colbright">
                QPS Information Packet</h3>
            <p>
                If you are unemployed, you may qualify for free training. To request an information
                packet, please send an email with your name and course of interest to the Training
                Administrator, at <a class="nounder" href="mailto:info@qpsinc.com" target="" title="">
                    <u>info@qpsinc.com</u></a>and an Information Packet regarding the application
                process will be sent to you in a timely fashion.
            </p>
            <div>
                Please complete <a href="../Registration/Registration.aspx">inquiry information</a>
                or call 1-877-987-3801.
            </div>
        </div>
        <div class="span-5" style="" style="float: left">
            <img alt="QPS Onsite Training" class="pull-1" src="../images/training.gif">
            <hr class="space">
        </div>
        <br />
        <div class="span-24 pull-1 last">
            <form id="frm11" runat="server">
            <ajax:ScriptManager ID="ScriptManager1" runat="server">
            </ajax:ScriptManager>
            <ajax:UpdateProgress ID="UPGR1" runat="Server">
                <ProgressTemplate>
                    <div id="loading" style="position: absolute; top: 70%; left: 40%;">
                        <img alt="Loading...." src="/images/ajax-loader.gif">
                    </div>
                </ProgressTemplate>
            </ajax:UpdateProgress>
            <ajax:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="Tabs">
                        <ul id="lstTabs">
                            <li id="li1" onclick="SetCurrentTab(1)"><a href="#" title="Massachusetts" onmouseout="style.textDecoration='none'"
                                onmouseover="style.textDecoration='underline'" onclick="SelectDiv(1)">Massachusetts</a>
                            </li>
                            <li id="li2" onclick="SetCurrentTab(2)"><a href="#" title="Rhode Island" onmouseout="style.textDecoration='none'"
                                onmouseover="style.textDecoration='underline'" onclick="SelectDiv(2)">Rhode Island</a></li>
                            <li id="li3" onclick="SetCurrentTab(3)"><a href="#" title="Connecticut" onmouseout="style.textDecoration='none'"
                                onmouseover="style.textDecoration='underline'" onclick="SelectDiv(3)">Connecticut</a></li>
                            <li id="li4" onclick="SetCurrentTab(4)"><a href="#" title="New Jersey" onmouseout="style.textDecoration='none'"
                                onmouseover="style.textDecoration='underline'" onclick="SelectDiv(7)">New Jersey</a></li>
                            <li id="li5" style="display: none;" onclick="SetCurrentTab(5)"><a href="#" title="North Carolina"
                                onmouseout="style.textDecoration='none'" onmouseover="style.textDecoration='underline'"
                                onclick="SelectDiv(4)">North Carolina</a></li>
                            <li id="li6" style="display: none;" onclick="SetCurrentTab(6)"><a href="#" title="Illinois"
                                onmouseout="style.textDecoration='none'" onmouseover="style.textDecoration='underline'"
                                onclick="SelectDiv(5)">Illinois</a></li>
                            <li id="li7" style="display: none;" onclick="SetCurrentTab(7)"><a href="#" title="New York"
                                onmouseout="style.textDecoration='none'" onmouseover="style.textDecoration='underline'"
                                onclick="SelectDiv(6)">New York</a></li>
                        </ul>
                    </div>
                    <script type="text/javascript" language="javascript">
                        document.getElementById("lstTabs").childNodes[1].className = "Current";
                        document.getElementById("lstTabs").childNodes[1].style.fontWeight = "bold";
                    </script>
                    <div id="Massachusetts" style="visibility: visible">
                        <div class="Grid">
                            <table class="CourseTab" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>
                                            Course Id.
                                        </th>
                                        <th>
                                            Course No.
                                        </th>
                                        <th width="325px;">
                                            Course
                                        </th>
                                        <th width="120px;">
                                            Days
                                        </th>
                                        <th width="90px;">
                                            Approved For
                                        </th>
                                        <th width="105px;">
                                            Fees
                                        </th>
                                        <th width="180px;">
                                            Dates
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1011457</span>
                                        </td>
                                        <td>
                                            <span>323A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/323A Six Sigma Green Belt and PMP 2015.pdf">
                                                Six Sigma Green Belt Certification Plus PMP (or CAPM) Certification Prep. (Two certifications;
                                                Six Sigma Green Belt & PMP Training)</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 22 hrs/Wk
                                                <br />
                                                4 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,055 (With PMI<sup>�</sup> exam fee of $555)</span>
                                        </td>
                                        <td>
                                            <span>Jan 11 - Feb 05, 2016<br />
                                                Feb 22 - Mar 20, 2016<br />
                                                Apr 04 - May 01, 2016<br />
                                                May 16 - June 11, 2016<br />
                                                June 20 - July 15, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1019331</span>
                                        </td>
                                        <td>
                                            <span>112</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/112 Lean Six Sigma Champion Training 2015.pdf">
                                                Lean Six Sigma Champion for Leading and Managing Teams</a></span>
                                        </td>
                                        <td>
                                            <span>4 Days</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$2,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 11 - Jan 14, 2016<br />
                                                Feb 22 - Feb 25, 2016<br />
                                                Apr 04 - Apr 07, 2016<br />
                                                June 20 - June 24, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1025927</span>
                                        </td>
                                        <td>
                                            <span>331</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/331 Master Expert certification 2015.pdf">
                                                Master Expert Certification (Customized for your needs) � Five courses/ certifications
                                                E .g. LSSGB, Lean Expert, SSBB, PMP, Supply chain, CPIM, ISO, ASQ certification)</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                12 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$15,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Mar 25, 2016<br />
                                                Jan 18 -Apr 08, 2016<br />
                                                Feb 15 - May 06, 2016<br />
                                                Mar 28 - June 11, 2016<br />
                                                Apr 11 - July 01, 2016<br />
                                                May 09 - Aug 05, 2016<br />
                                                May 23 - Aug 19, 2016<br />
                                                June 13 - Sep 01, 2016<br />
                                                June 27 - Sep 16, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1028067</span>
                                        </td>
                                        <td>
                                            <span>114A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/114A Six Sigma  Black Belt Certification 2015.pdf">
                                                Six Sigma Black Belt Certification (Includes LSSGB) - Two certifications</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                6 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$7,500</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 12, 2016<br />
                                                Feb 15 - Mar 27, 2016<br />
                                                May 12 - June 19, 2016<br />
                                                June 23 - July 31, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1036847</span>
                                        </td>
                                        <td>
                                            <span>124A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/124A Lean Expert Certification 2015.pdf">
                                                Lean Expert Certification (Includes LSSGB) � Two certifications</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 27 hrs/Wk
                                                <br />
                                                3 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$5,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Jan 22, 2016<br />
                                                Feb 15 - Mar 05, 2016<br />
                                                Mar 28 - Apr 15, 2016<br />
                                                May 12 - May 29, 2016<br />
                                                June 22 - July 08, 2016</span> </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1037639</span>
                                        </td>
                                        <td>
                                            <span>118</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/118 Lean Six Sigma Black Belt Certification 2015.pdf">
                                                Lean Six Sigma Black Belt Certification (2 certifications: LSSGB, SSBB 1036847,
                                                1028067)</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                8 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$8,500</span>
                                        </td>
                                        <td>
                                            <span>Jan 06 - Feb 27, 2016<br />
                                                Feb 17 - Apr 10, 2016<br />
                                                Mar 31 - May 08, 2016<br />
                                                May 12 - July 03, 2016<br />
                                                June 23 - Aug 14, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1052375</span>
                                        </td>
                                        <td>
                                            <span>113A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/113A%20Lean%20Six%20Sigma%20Green%20Belt%20Certification%202015.pdf">
                                                Lean Six Sigma Green Belt Certification (ASQ Body of Knowledge)</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 28 hrs/Wk
                                                <br />
                                                2 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$3000</span>
                                        </td>
                                        <td>
                                            <span>July 07 � July 17, 2015<br />
                                                Aug 18 � Aug 28, 2015<br />
                                                Sep 29 � Oct 09, 2015<br />
                                                Nov 10 � Nov 20, 2015<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1054679</span>
                                        </td>
                                        <td>
                                            <span>221</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/221%20Project%20Management%20Certification%20Training%202015.pdf">
                                                Project Management (PMP) Certification Training</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                3 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$3,550 (with PMI<sup>�</sup> exam fee)</span>
                                        </td>
                                        <td>
                                            <span>July 20 � Aug 7, 2015<br />
                                                Aug 31 � Sep 18, 2015<br />
                                                Oct 12 � Oct 30, 2015<br />
                                                Nov 23 - Dec 11, 2015</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1054680</span>
                                        </td>
                                        <td>
                                            <span>323</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/323%20Management%20Certification%202015.pdf">
                                                Management Certification (2 Certifications; Course LSSGB, PMP, MS Project)</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 22 hrs/Wk
                                                <br />
                                                5 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,500</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1054681</span>
                                        </td>
                                        <td>
                                            <span>227</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/227%20Certified%20Inspector%20and%20Technican%202015.pdf">
                                                Inspector and Technician Certification (ASQ BOK)</a> </span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                5 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$5,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04- Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1071971</span>
                                        </td>
                                        <td>
                                            <span>228</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/228%20Production%20and%20Supervisory%20Skills%202015.pdf">
                                                Production & Supervisory Skills </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                5 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$5,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1073267</span>
                                        </td>
                                        <td>
                                            <span>324</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/324 Management Certification for Mfg Professionals 2015.pdf">
                                                Management Certification for Manufacturing Professionals (Lean Six Sigma GB, Lean
                                                Expert or AutoCAD, Supply Chain Cert. or ASQ CQE/CQA/CQT, and ISO 9001 or SolidWorks)</a>
                                            </span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                7 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$7,500</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 19, 2016<br />
                                                Feb 15 - Apr 03, 2016<br />
                                                Mar 28 - May 13, 2016<br />
                                                May 09 - June 24, 2016<br />
                                                June 27 - Aug 06, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1073537</span>
                                        </td>
                                        <td>
                                            <span>325</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/325 Bio Medical Certification 2015.pdf">
                                                BIO Medical/Pharm Certificate Program � Any four courses (Lean Six Sigma GB, Bio
                                                Medical or Pharm. Courses) </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                7 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$7,500</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 19, 2016<br />
                                                Feb 15 - Apr 03, 2016<br />
                                                Mar 28 - May 13, 2016<br />
                                                May 09 - June 24, 2016<br />
                                                June 27 - Aug 06, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1075480</span>
                                        </td>
                                        <td>
                                            <span>177</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/177 Sales and Customer Service 2015.pdf">
                                                Sales, Marketing & Customer Service</a> </span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                5 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$5,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1075481</span>
                                        </td>
                                        <td>
                                            <span>223</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/223 ASQ Certification Preparation 2015.pdf">
                                                ASQ Certifications Training (any 3 ASQ Certifications except Black Belt) </a>
                                            </span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                5 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$5,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1077049</span>
                                        </td>
                                        <td>
                                            <span>245</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/245 Office Technology 2015.pdf">Office Technology
                                                - Software & Accounting (MS office, QuickBooks, Internet & Visio)</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                5 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$5,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 05 - Feb 05, 2016<br />
                                                Feb 16 - Mar 20, 2016<br />
                                                Mar 29 - May 01, 2016<br />
                                                May 17 - June 10, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1087069</span>
                                        </td>
                                        <td>
                                            <span>332</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/332 Leadership & Management Development 2015.pdf">
                                                Leadership & Management Development Certificate Program 5 Certifications! Customized</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                13 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$15,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Mar 25, 2016<br />
                                                Feb 15 - May 06, 2016<br />
                                                Apr 11 - July 01, 2016<br />
                                                May 23 - Aug 19, 2016<br />
                                                June 13 - Sept 01, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1087353</span>
                                        </td>
                                        <td>
                                            <span>334</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/334 Advanced Project Management Leadership 2015.pdf">
                                                Advanced Project Management Leadership Program: Customized! Choose any four Certifications
                                                plus MS Project : PMP, RMP-Risk Management, Agile ACP, Lean Six Sigma GB or PgMP
                                                or scheduling</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 22 hrs/Wk
                                                <br />
                                                7 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$10,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 19, 2016<br />
                                                Feb 15 - Apr 03, 2016<br />
                                                Mar 28 - May 13, 2016<br />
                                                May 09 - June 24, 2016<br />
                                                June 27 - Aug 06, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1087354</span>
                                        </td>
                                        <td>
                                            <span>333</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/333 Software Professional Development 2015.pdf">
                                                Software Professional Development: three plus MS Project: PMP, Lean Six Sigma Green
                                                Belt or RMP - three Certifications plus MS Project</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                7 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$7,500</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 19, 2016<br />
                                                Feb 15 - Apr 03, 2016<br />
                                                Mar 28 - May 13, 2016<br />
                                                May 09 - June 24, 2016<br />
                                                June 27 - Aug 06, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1109085</span>
                                        </td>
                                        <td>
                                            <span>342</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="">Any Three Certifications from APICS (CPIM or CSCP)
                                                , ASQ, or SCRUM </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                5 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1090244</span>
                                        </td>
                                        <td>
                                            <span>340</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/340 Autocad and Solidworks Basics & Intermediate 2015.pdf">
                                                AutoCAD and Solid works: Basics & Intermediate</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                5 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1091724</span>
                                        </td>
                                        <td>
                                            <span>340A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/340A Solidworks Basics & Intermediate 2015.pdf">
                                                Solid Works: Basics & Intermediate</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                3 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$3,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 18 - Feb 05, 2016<br />
                                                Feb 29 - Mar 20, 2016<br />
                                                Apr 11 - May 01, 2016<br />
                                                May 30 - June 10, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1090245</span>
                                        </td>
                                        <td>
                                            <span>341</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/341 Project Management Modern 2015.pdf">
                                                Project Management Modern: Three cert: PMP Risk Management, Agile Certified Professional
                                                or other</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                5 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,555 (with PMI� exam fee of $555) </span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td style="text-align: center" colspan="4">
                                            <span><a target="_blank" href="../Documents/MA_Unemployed_Course_Matrix_Jan-June 2016.pdf">
                                                Click here</a> to download Massachusetts Public Training calendar</span>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="Rhode" style="display: none; height: 0px">
                        <div class="Grid">
                            <table class="CourseTab" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>
                                            Course Id.
                                        </th>
                                        <th>
                                            Course No.
                                        </th>
                                        <th width="325px;">
                                            Course
                                        </th>
                                        <th width="120px;">
                                            Days
                                        </th>
                                        <th width="90px;">
                                            Approved For
                                        </th>
                                        <th width="105px;">
                                            Fees
                                        </th>
                                        <th width="180px;">
                                            Dates
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1011457</span>
                                        </td>
                                        <td>
                                            <span>323A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/Course 323A.pdf">Six Sigma Green Belt Certification
                                                Plus PMP (CAPM) Certification Prep. (Two certifications; Six Sigma Green Belt &
                                                PMP Training)</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk<br />
                                                5 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,055 (with PMI� exam fee of $555) </span>
                                        </td>
                                        <td>
                                            <span>Jan 11 - Feb 05, 2016<br />
                                                Feb 22 - Mar 20, 2016<br />
                                                Apr 04 - May 01, 2016<br />
                                                May 16 - June 11, 2016<br />
                                                June 20 - July 15, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1028067</span>
                                        </td>
                                        <td>
                                            <span>114A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 114A.pdf">Six Sigma Black Belt Certification
                                                (Includes LSSGB)- two certifications </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 28 hrs/Wk<br />
                                                6 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,900</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 12, 2016<br />
                                                Feb 15 - Mar 27, 2016<br />
                                                May 12 - June 19, 2016<br />
                                                June 23 - July 31, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1036847</span>
                                        </td>
                                        <td>
                                            <span>124A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 124A.pdf">Lean Expert Certification
                                                (Includes LSS GB)- two certifications </a></span>
                                        </td>
                                        <td>
                                            <span>11 days in
                                                <br />
                                                3 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$4,500</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Jan 22, 2016<br />
                                                Feb 15 - Mar 05, 2016<br />
                                                Mar 28 - Apr 15, 2016<br />
                                                May 12 - May 29, 2016<br />
                                                June 22 - July 08, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1037639</span>
                                        </td>
                                        <td>
                                            <span>118</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 118.pdf">Lean Six Sigma Black Belt
                                                Certification (2 Certifications: LSSGB, SSBB 1036847, 1028067) and Lean Expert</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 24 hrs/Wk<br />
                                                7 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$8,500</span>
                                        </td>
                                        <td>
                                            <span>Jan 06 - Feb 27, 2016<br />
                                                Feb 17 - Apr 10, 2016<br />
                                                Mar 31 - May 08, 2016<br />
                                                May 12 - July 03, 2016<br />
                                                June 23 - Aug 14, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1052375</span>
                                        </td>
                                        <td>
                                            <span>113A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 113A.pdf">Lean Six Sigma Green Belt
                                                Certification (ASQ Body of Knowledge) </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 36 hrs/Wk
                                                <br />
                                                2 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$3,950</span>
                                        </td>
                                        <td>
                                            <span>Jan 06 - Jan 16, 2016<br />
                                                Feb 17 - Feb 27, 2016<br />
                                                Mar 31 - Apr 10, 2016<br />
                                                May 12 - May 22, 2016<br />
                                                June 23 - july 03, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1054679</span>
                                        </td>
                                        <td>
                                            <span>221</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 221.pdf">Project Management (PMP)
                                                Certification Training</a> </span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk
                                                <br />
                                                3 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$3,000*</span>
                                        </td>
                                        <td>
                                            <span>Jan 18 - Feb 05, 2016<br />
                                                Mar 03 - Mar 19, 2016<br />
                                                Apr 11 - Apr 29, 2016<br />
                                                May 23 - June 12, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1075481</span>
                                        </td>
                                        <td>
                                            <span>223</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 223.pdf">ASQ Certifications Training
                                                (any ASQ Certifications except Black Belt) </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk<br />
                                                5 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$5,000 </span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1087069</span>
                                        </td>
                                        <td>
                                            <span>332</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 332.pdf">Leadership & Management
                                                Development Certificate Program 5 Certifications! Customized </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 26 hrs/Wk<br />
                                                13 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$15,000*</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Mar 25, 2016<br />
                                                Feb 15 - May 06, 2016<br />
                                                Apr 11 - July 01, 2016<br />
                                                May 23 - Aug 19, 2016<br />
                                                June 13 - Sep 01, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1087353</span>
                                        </td>
                                        <td>
                                            <span>334</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 334.pdf">Advanced Project Management
                                                Leadership Program: Customized! Choose any four Certifications plus MS Project :
                                                PMP, RMP - Risk Management, Agile ACP, Lean Six Sigma GB or PgMP or scheduling </a>
                                            </span>
                                        </td>
                                        <td>
                                            <span>Avg. 26 hrs/Wk
                                                <br />
                                                7 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$10,000*</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 19, 2016<br />
                                                Feb 15 - Apr 03, 2016<br />
                                                Mar 28 - May 13, 2016<br />
                                                May 09 - June 24, 2016<br />
                                                June 27 - Aug 06, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1087354</span>
                                        </td>
                                        <td>
                                            <span>333</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 333.pdf">Software Professional Development
                                                : three plus MS Project : PMP, Lean Six Sigma Green Belt or RMP - three Certifications
                                                plus MS Project</a> </span>
                                        </td>
                                        <td>
                                            <span>Avg. 26 hrs/Wk
                                                <br />
                                                7 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$7,500*</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 19, 2016<br />
                                                Feb 15 - Apr 03, 2016<br />
                                                Mar 28 - May 13, 2016<br />
                                                May 09 - June 24, 2016<br />
                                                June 27 - Aug 06, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1090244</span>
                                        </td>
                                        <td>
                                            <span>340</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 340.pdf">AutoCAD and Solid works:
                                                Basics & Intermediate </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk<br />
                                                5 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$5,000 </span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1090245</span>
                                        </td>
                                        <td>
                                            <span>341</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 341.pdf">Project Management Modern:
                                                Three cert: PMP Risk Management, Agile Certified Professional or other </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 24 hrs/Wk<br />
                                                5 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,055*(with PMI� exam fee of $555)</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>1090242</span>
                                        </td>
                                        <td>
                                            <span>342</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 341.pdf">Any Three Certifications
                                                from APICS (CPIM or CSCP) , ASQ, or SCRUM</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 20 hrs/Wk<br />
                                                5 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>N/A</span>
                                        </td>
                                        <td>
                                            <span>237</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 237 APICS Supply Chain, CSCP Prep.pdf">
                                                APICS Supply Chain Professional / Production & Inventory Management</a> </span>
                                        </td>
                                        <td>
                                            <span>Avg. 24 hrs/Wk<br />
                                                5 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>N/A</span>
                                        </td>
                                        <td>
                                            <span>343</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/course 343.pdf">Quality, Project Management
                                                & Supply Professional</a> </span>
                                        </td>
                                        <td>
                                            <span>Avg. 32 hrs/Wk<br />
                                                5 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$7,500*</span>
                                        </td>
                                        <td>
                                            <span>Jan 04 - Feb 05, 2016<br />
                                                Feb 15 - Mar 20, 2016<br />
                                                Mar 28 - May 01, 2016<br />
                                                May 16 - June 10, 2016<br />
                                                June 20 - July 22, 2016</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td style="text-align: center" colspan="4">
                                            <span><a target="_blank" href="../Documents/RI Unemployed  Course Matrix Jan-June 2016.pdf">
                                                Click here</a> to download Rhode Island Public Training calendar</span>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td style="text-align: center" colspan="4">
                                            <span>*$450 Certification Fee is not included</span>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="Connecticut" style="display: none; height: 0px">
                        <div class="Grid">
                            <table class="CourseTab" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>
                                            Course Id.
                                        </th>
                                        <th>
                                            Course No.
                                        </th>
                                        <th width="335px;">
                                            Course
                                        </th>
                                        <th width="120px;">
                                            Days
                                        </th>
                                        <th width="90px;">
                                            Approved For
                                        </th>
                                        <th width="105px;">
                                            Fees
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09014</span>
                                        </td>
                                        <td>
                                            <span>113A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/113A Lean Six Sigma Green Belt CT.pdf">Lean
                                                Six Sigma Green Belt Certification</a> </span>
                                        </td>
                                        <td>
                                            <span>3 days/Wk<br />
                                                2 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$3,950 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09015</span>
                                        </td>
                                        <td>
                                            <span>114A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/114A Six Sigma  Black Belt Certification CT.pdf">
                                                Six Sigma Black Belt Certification (Includes LSSGB)</a> </span>
                                        </td>
                                        <td>
                                            <span>Avg. 24 hrs/Wk<br />
                                                4 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$4,000 *</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09016</span>
                                        </td>
                                        <td>
                                            <span>118</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/118 Lean Six Sigma Black Belt Certification CT.pdf">
                                                Lean Six Sigma Black Belt Certification (3 certifications: LSSGB, SSBB, Lean Expert)
                                            </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 24 hrs/Wk<br />
                                                6 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,000 *</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09017</span>
                                        </td>
                                        <td>
                                            <span>124A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/124 Lean Six Sigma Green Belt & Lean Expert CT.pdf">
                                                Lean Expert Certification (2 certifications: Lean Expert, LSSGB) </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 34 hrs/Wk<br />
                                                2 Weeks</span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$4,000 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09018</span>
                                        </td>
                                        <td>
                                            <span>177</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/177A Sales Customer Svs & Lean Six Sigma Green Belt CT.pdf">
                                                Sales, Marketing & Customer Service </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 24 hrs/Wk
                                                <br />
                                                3 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$4,000 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09019</span>
                                        </td>
                                        <td>
                                            <span>221</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/CT 221A Agile, PMP, RMP.pdf">Agile<sup>SM</sup>
                                                + Project Management (PMP<sup>�</sup>) Certification Training (2 Certificates: Agile
                                                & PMP<sup>�</sup>) </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 24 hrs/Wk<br />
                                                3 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$4,555 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09020</span>
                                        </td>
                                        <td>
                                            <span>322</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/322 Certified Inspector and Technican CT.pdf">
                                                Operator & Technician Certification</a> </span>
                                        </td>
                                        <td>
                                            <span>Avg. 24 hrs/Wk<br />
                                                2 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$4,000 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09021</span>
                                        </td>
                                        <td>
                                            <span>321</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/321 Certified Production Operator & Technician CT.pdf">
                                                Inspector & Technician</a> </span>
                                        </td>
                                        <td>
                                            <span>Avg. 24 hrs/Wk<br />
                                                3 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$4,000 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09022</span>
                                        </td>
                                        <td>
                                            <span>323</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/323 Management Certification CT.pdf">Management
                                                Certification (2 Certifications; LSSGB, PMP<sup>�</sup>) </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 24 hrs/Wk<br />
                                                3 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$5,000 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09023</span>
                                        </td>
                                        <td>
                                            <span>324</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/324 Management Certification for Mfg Professionals CT.pdf">
                                                Management Certification for Manufacturing Professionals (Lean Six Sigma GB, Lean
                                                Expert, Supply Chain Cert., ISO 9001 or CQA)</a> </span>
                                        </td>
                                        <td>
                                            <span>Avg. 24 hrs/Wk<br />
                                                5 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$6,000</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09024</span>
                                        </td>
                                        <td>
                                            <span>325 </span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/325 Bio Medical Certification Training CT.pdf">
                                                BIO Medical/Pharm. Certification � Any 4 (Lean Six Sigma GB, Bio Medical or Pharm.
                                                Courses) </a></span>
                                        </td>
                                        <td>
                                            <span>4 Certifications<br />
                                                Avg. 26 hrs/Wk<br />
                                                7 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$7,500*</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>09025</span>
                                        </td>
                                        <td>
                                            <span>331</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/331 Master Expert Certification CT.pdf">
                                                Master Expert Certification (Customized for your needs) - 5 certifications LSSGB,
                                                Lean Expert, SSBB, PMP or Supply chain, ISO or any ASQ certifications </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 26 hrs/Wk<br />
                                                12 Weeks </span>
                                        </td>
                                        <td>
                                            <span>ITA Trade Section 30</span>
                                        </td>
                                        <td>
                                            <span>$15,000 * </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td style="text-align: center" colspan="6">
                                            <span><a target="_blank" href="../Documents/CT Unemployed  Course Matrix Jan-June 2016.pdf">
                                                Click here</a> to download Connecticut Public Training calendar</span>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td style="text-align: center" colspan="6">
                                            <b>Federal No. 043325229</b>
                                            <br />
                                            <b>School licensed by Dept. of Education, Commonwealth of Mass-13100007 � Sept. 2016
                                            </b>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="New Jersey" style="display: none; height: 0px">
                        <div class="Grid">
                            <%-- <b> Available every two weeks. Please <a href="mailto:info@qpsinc.com">contact us</a>.</b>--%>
                            <table class="CourseTab" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>
                                            Course No.
                                        </th>
                                        <th>
                                            Course
                                        </th>
                                        <th style="width: 130px">
                                            Days
                                        </th>
                                        <th style="width: 130px">
                                            Fees
                                        </th>
                                        <th width="170px;">
                                            Start Dates
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="oddRow">
                                        <td>
                                            <span>237</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 237 APICS Supply Chain CSCP Prep.pdf">
                                                APICS � Certified Supply Chain Professional (CSCP) or Certified in Production & Inventory Management (CPIM) Preparation </a></span>
                                        </td>
                                        <td>
                                            <span>56 Hours </span>
                                        </td>
                                        <td>
                                            <span>$4,000</span>
                                        </td>
                                        <td>
                                            <span>January 04, 2016<br />
                                                February 15, 2016<br />
                                                March 28, 2016<br />
                                                May 09, 2016<br />
                                                June 27, 2016<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>343B</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 343B Agile & PMP Prep Project Management ACP Prep.pdf">
                                                Agile & PMP<sup>�</sup> Preparation (Agile can be PMI or SCRUM)</a></span>
                                        </td>
                                        <td>
                                            <span>56 Hours </span>
                                        </td>
                                        <td>
                                            <span>$4,000</span>
                                        </td>
                                        <td>
                                            <span>January 04, 2016<br />
                                                February 15, 2016<br />
                                                March 28, 2016<br />
                                                May 16, 2016<br />
                                                June 20, 2016<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>343A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 343A Any 1 PMI Project Management Certification.pdf">
                                                Any (1) PMI Project Management Certification (PMP<sup>�</sup>, CAPM<sup>�</sup>,
                                                RMP<sup>�</sup>, SP<sup>�</sup>, ACP<sup>SM</sup>) </a></span>
                                        </td>
                                        <td>
                                            <span>35 Hours </span>
                                        </td>
                                        <td>
                                            <span>$3,155</span>
                                        </td>
                                        <td>
                                            <span>January 04, 2016<br />
                                                February 15, 2016<br />
                                                March 28, 2016<br />
                                                May 16, 2016<br />
                                                June 20, 2016<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>343</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 343 Any 3 Cer Prep ASQ Quality, PMI Project Mgmt & Supply Chain.pdf">
                                                Any (3) Certifications Prep: Any course of PMI, ASQ, APICS, Scrum certifications (Except Black Belt)</a>
                                            </span>
                                        </td>
                                        <td>
                                            <span>77 Hours </span>
                                        </td>
                                        <td>
                                            <span>$6,500</span>
                                        </td>
                                        <td>
                                            <span>January 04, 2016<br />
                                                February 15, 2016<br />
                                                March 28, 2016<br />
                                                May 09, 2016<br />
                                                June 27, 2016<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>325</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 325 Bio Medical Certification.pdf">BIO
                                                Medical Auditor, ISO 9001/13485, Six Sigma Green Belt (Any three courses) </a></span>
                                        </td>
                                        <td>
                                            <span>42 Hours </span>
                                        </td>
                                        <td>
                                            <span>$4,000</span>
                                        </td>
                                        <td>
                                            <span>January 04, 2016<br />
                                                February 15, 2016<br />
                                                March 28, 2016<br />
                                                May 09, 2016<br />
                                                June 27, 2016<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>124A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 124A Lean Expert & Lean Six Sigma Green Belt.pdf">
                                                Lean Expert & Six Sigma Green Belt </a></span>
                                        </td>
                                        <td>
                                            <span>56 Hours </span>
                                        </td>
                                        <td>
                                            <span>$5,000</span>
                                        </td>
                                        <td>
                                            <span>January 04, 2016<br />
                                                February 15, 2016<br />
                                                March 28, 2016<br />
                                                May 12, 2016<br />
                                                June 22, 2016<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>113A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 113A Lean Six Sigma Green Belt.pdf">Lean
                                                Six Sigma Green Belt </a></span>
                                        </td>
                                        <td>
                                            <span>42 Hours </span>
                                        </td>
                                        <td>
                                            <span>$3,950</span>
                                        </td>
                                        <td>
                                            <span>January 06, 2016<br />
                                                February 17, 2016<br />
                                                March 31, 2016<br />
                                                May 12, 2016<br />
                                                June 23, 2016<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>118</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 118 Lean Six Sigma Green, Black Belt & Lean Expert.pdf">
                                                Lean Six Sigma Green Belt, Black Belt & Lean Expert (BB Certification-Prep)</a></span>
                                        </td>
                                        <td>
                                            <span>98 Hours</span>
                                        </td>
                                        <td>
                                            <span>$6,500</span>
                                        </td>
                                        <td>
                                            <span>January 06, 2016<br />
                                                February 17, 2016<br />
                                                March 31, 2016<br />
                                                May 12, 2016<br />
                                                June 23, 2016<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>221</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 221 PMP & CAPM Certified Project Mgmt Professional.pdf">
                                                PMP<sup>�</sup> or CAPM<sup>�</sup> Prep or any Scrum certification </a></span>
                                        </td>
                                        <td>
                                            <span>35 Hours</span>
                                        </td>
                                        <td>
                                            <span>$3,055</span>
                                        </td>
                                        <td>
                                            <span>January 18, 2016<br />
                                                March 03, 2016<br />
                                                April 11, 2016<br />
                                                May 23, 2016<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr style="display: none;" class="oddRow">
                                        <td>
                                            <span>221a</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/221a PMI- Certified Project Mgmt Professional_UPDATE.pdf">
                                                PMI � Project Management Professional Certification � (PMP<sup>�</sup>) Preparation</a></span>
                                        </td>
                                        <td>
                                            <span>5 days</span>
                                        </td>
                                        <td>
                                            <span>$2,500</span>
                                        </td>
                                        <td>
                                            <span>Oct 19, 2015<br />
                                                Nov 16, 2015<br />
                                                Dec 14, 2015</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>323A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 323A Six Sigma Green Belt & PMI PMP_ or CAPM_ Preparation.pdf">
                                                PMP<sup>�</sup> or CAPM<sup>�</sup> Prep Project Management & Six Sigma Green Belt</a></span>
                                        </td>
                                        <td>
                                            <span>70 Hours </span>
                                        </td>
                                        <td>
                                            <span>$4,000 </span>
                                        </td>
                                        <td>
                                            <span>January 11, 2016<br />
                                                February 22, 2016<br />
                                                April 04, 2016<br />
                                                May 16, 2016<br />
                                                June 20, 2016<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>114</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 114 Six Sigma  Black Belt Certification.pdf">
                                                Six Sigma Black Belt Certification Preparation</a></span>
                                        </td>
                                        <td>
                                            <span>56 Hours</span>
                                        </td>
                                        <td>
                                            <span>$5,000</span>
                                        </td>
                                        <td>
                                            <span>January 04, 2016<br />
                                                February 15, 2016<br />
                                                May 12, 2016<br />
                                                June 23, 2016<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <%--<tr class="oddRow">
                                        <td>
                                            <span>340</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NJ 340 Autocad or solidworks.pdf">AutoCAD
                                                or SolidWorks </a></span>
                                        </td>
                                        <td>
                                            <span>07 days</span>
                                        </td>
                                        <td>
                                            <span>$4,000</span>
                                        </td>
                                        <td>
                                            <span>TBD</span>
                                        </td>
                                    </tr>--%>
                                    <tr class="oddRow">
                                        <td>
                                        </td>
                                        <td style="text-align: center" colspan="4">
                                            <span><a target="_blank" href="../Documents/NJ Unemployed Course Matrix January-June 2016.pdf">
                                                Click here</a> to download New Jersey Public Training calendar</span>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <p style="text-align: center; padding: 0 40px;">
                                                <b style="text-decoration: underline">Note: Course Duration will be 4-11 weeks depending
                                                    on the course and clocked hours student
                                                    <br />
                                                    can attend (1-3 days per week)
                                                    <br />
                                                    Evenings and weekends courses are available.</b><br />
                                                <br />
                                                State of New Jersey Department of Education & Department of Labor & Workforce Development
                                                <br />
                                                approved, certified & licensed Private Vocational School<br />
                                                <br />
                                                For appointments & information, you may contact the NJ Director, Edwin May at <a
                                                    target="_blank" href="mailto:edm@qpsinc.com">edm@qpsinc.com.</a>
                                                <br />
                                                * Schedule will be developed after consultation with the student.
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br />
                        <p class="mb0">
                            <b>Note:</b> Usually course runs 2 days per week unless specifically agreed with
                            student.
                        </p>
                    </div>
                    <div id="North Carolina" style="display: none; height: 0px;">
                        <div class="Grid">
                            <%--  <b> Available every month. Please <a href="mailto:info@qpsinc.com">contact us</a>.</b>--%>
                            <table class="CourseTab" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>
                                            Course Id.
                                        </th>
                                        <th>
                                            Course No.
                                        </th>
                                        <th width="400px;">
                                            Course
                                        </th>
                                        <th width="140px;">
                                            Days
                                        </th>
                                        <th width="100px;">
                                            Fees
                                        </th>
                                        <th width="155px;">
                                            Dates
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18953 </span>
                                        </td>
                                        <td>
                                            <span>331</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 331 Master Expert Certification.pdf">
                                                Master Expert Certification (Customized for your needs) - 5 certifications </a>
                                            </span>
                                        </td>
                                        <td>
                                            <span>Avg. 32 hrs/Wk<br />
                                                10 Weeks </span>
                                        </td>
                                        <td>
                                            <span>$12,000 * </span>
                                        </td>
                                        <td>
                                            <span>July 28, 2015<br />
                                                Sept 08, 2015<br />
                                                Oct 20, 2015<br />
                                                Dec 01, 2015<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18954</span>
                                        </td>
                                        <td>
                                            <span>118</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 118 Lean Six Sigma Black Belt Certification 6.11.13.pdf">
                                                Lean Six Sigma Black Belt Certification (2 certifications)</a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 26 hrs/Wk<br />
                                                6 Weeks </span>
                                        </td>
                                        <td>
                                            <span>$8,000 * </span>
                                        </td>
                                        <td>
                                            <span>Aug 11, 2015<br />
                                                Sept 22, 2015<br />
                                                Nov 03, 2015<br />
                                                Dec 15, 2015<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18956 </span>
                                        </td>
                                        <td>
                                            <span>323</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 323 Management Certification.pdf">Management
                                                Certification (2 Certifications) </a></span>
                                        </td>
                                        <td>
                                            <span>2 certifications:<br />
                                                Avg. 32 hrs/Wk<br />
                                                4 Weeks </span>
                                        </td>
                                        <td>
                                            <span>$6,500 </span>
                                        </td>
                                        <td>
                                            <span>July 28, 2015<br />
                                                Sept 08, 2015<br />
                                                Oct 20, 2015<br />
                                                Dec 01, 2015<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18958 </span>
                                        </td>
                                        <td>
                                            <span>324</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 324 Management Certification for Mfg Professionals.pdf">
                                                Management Certification for Manufacturing Professionals (Lean Six Sigma GB, Lean
                                                Expert, Supply Chain, ISO 9001) </a></span>
                                        </td>
                                        <td>
                                            <span>4 Certifications<br />
                                                Avg. 32 hrs/Wk<br />
                                                4 Weeks </span>
                                        </td>
                                        <td>
                                            <span>$6,000 * </span>
                                        </td>
                                        <td>
                                            <span>Aug 11, 2015<br />
                                                Sept 22,2015<br />
                                                Nov 03, 2015<br />
                                                Dec 15, 2015<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18959 </span>
                                        </td>
                                        <td>
                                            <span>325</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 325 Bio Medical Certification.pdf">BIO
                                                Medical Certification (Llssb, ISO 13485 BIO Med Auditor Certification) </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 26 hrs/Wk<br />
                                                7 Weeks </span>
                                        </td>
                                        <td>
                                            <span>$7,500 * </span>
                                        </td>
                                        <td>
                                            <span>July 28, 2015<br />
                                                Sept 08, 2015<br />
                                                Oct 20, 2015<br />
                                                Dec 01, 2015<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18960 </span>
                                        </td>
                                        <td>
                                            <span>124A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 124A Lean Expert Certification 6.6.14.pdf">
                                                Lean Expert Certification (Includes LSS GB) </a></span>
                                        </td>
                                        <td>
                                            <span>2 certifications<br />
                                                Avg. 32 hrs/Wk<br />
                                                3 Weeks </span>
                                        </td>
                                        <td>
                                            <span>$4,000 </span>
                                        </td>
                                        <td>
                                            <span>July 28, 2015<br />
                                                Sept 08, 2015<br />
                                                Oct 20, 2015<br />
                                                Dec 01, 2015<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18961 </span>
                                        </td>
                                        <td>
                                            <span>221</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 221 PMI- Certified Project Mgmt Professional.pdf">
                                                Project Management (PMP) Certification Prep Training (QPS certification provided
                                                also) </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 30 hrs/Wk<br />
                                                2 Weeks</span>
                                        </td>
                                        <td>
                                            <span>$3,000 </span>
                                        </td>
                                        <td>
                                            <span>Aug 11, 2015<br />
                                                Sept 22,2015<br />
                                                Nov 03, 2015<br />
                                                Dec 15, 2015</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18962 </span>
                                        </td>
                                        <td>
                                            <span>114A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 114A Six Sigma  Black Belt.pdf">Six Sigma
                                                Black Belt Certification (Includes LSSGB) </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 32 hrs/Wk<br />
                                                6 Weeks </span>
                                        </td>
                                        <td>
                                            <span>$6,900 </span>
                                        </td>
                                        <td>
                                            <span>Aug 11, 2015<br />
                                                Sept 22, 2015<br />
                                                Nov 03, 2015<br />
                                                Dec 15, 2015<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18963 </span>
                                        </td>
                                        <td>
                                            <span>113A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 113A Lean Six Sigma Green Belt.pdf">Lean
                                                Six Sigma Green Belt Certification (LSSGB Certification) </a></span>
                                        </td>
                                        <td>
                                            <span>4 days & 5 days<br />
                                                Avg. 36 hrs/Wk<br />
                                                2 Weeks </span>
                                        </td>
                                        <td>
                                            <span>$3,500 </span>
                                        </td>
                                        <td>
                                            <span>Aug 11, 2015<br />
                                                Sept 22, 2015<br />
                                                Nov 03, 2015<br />
                                                Dec 15, 2015<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18964 </span>
                                        </td>
                                        <td>
                                            <span>321</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 321 Certified Production Operator and Technican 6.6.14.pdf">
                                                Certified Production Operator and Technician (Certification provided) </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 32 hrs/Wk<br />
                                                2 Weeks </span>
                                        </td>
                                        <td>
                                            <span>$3,000 </span>
                                        </td>
                                        <td>
                                            <span>Sept 22, 2015<br />
                                                Nov 03, 2015<br />
                                                Dec 22, 2015</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18965 </span>
                                        </td>
                                        <td>
                                            <span>322</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 322 Certified Inspector and Technican.pdf">
                                                Inspector and Technician Certification (Certification Provided)</a> </span>
                                        </td>
                                        <td>
                                            <span>Avg. 32 hrs/Wk<br />
                                                2 Weeks </span>
                                        </td>
                                        <td>
                                            <span>$3,000 </span>
                                        </td>
                                        <td>
                                            <span>Sept 22, 2015<br />
                                                Nov 03, 2015<br />
                                                Dec 22, 2015</span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>18966 </span>
                                        </td>
                                        <td>
                                            <span>177</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="../Documents/NC 177 Sales and Customer Service Analyst 6.6.14.pdf">
                                                Sales & Customer Service Analyst</a> </span>
                                        </td>
                                        <td>
                                            <span>2 certifications<br />
                                                Avg. 32 hrs/Wk<br />
                                                2 Weeks </span>
                                        </td>
                                        <td>
                                            <span>$3,000 </span>
                                        </td>
                                        <td>
                                            <span>Aug 11, 2015<br />
                                                Sept 22, 2015<br />
                                                Nov 03, 2015<br />
                                                Dec 15 2015<br />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                        </td>
                                        <td style="text-align: center" colspan="4">
                                            <span><a target="_blank" href="../Documents/NC Unemployed  Course Matrix July-Dec 2015.pdf">
                                                Click here</a> to download North Carolina Public Training calendar</span>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="Illinois" style="display: none; height: 0px; padding-right: 40px">
                        <div class="Grid">
                            <b>Available every month. Please <a href="mailto:info@qpsinc.com">contact us</a>.</b>
                            <%--  <table class="CourseTab" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>
                                            Couse No.
                                        </th>
                                        <th width="420px;">
                                            Course
                                        </th>
                                        <th width="120px;">
                                            Days
                                        </th>
                                        <th width="80px;">
                                            Fees
                                        </th>
                                        <th width="145px;">
                                            Dates
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="oddRow">
                                        <td>
                                            <span>331</span>
                                        </td>
                                        <td>
                                            <span><a href="http://qpsinc.com/Documents/IL 331 Master Expert Certification.pdf">Master
                                                Expert Certification (Customized for your needs) - 5 certifications LSSGB, Lean
                                                Expert, SSBB, PMP or Supply chain, ISO or t CQA </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 26 hrs/Wk, 12 weeks </span>
                                        </td>
                                        <td>
                                            <span>$12,000 * </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Mar 30, 2013
                                                <br />
                                                Feb 18 - May 11, 2013
                                                <br />
                                                Apr 1 - June 22, 2013
                                                <br />
                                                May 13 - Aug 3, 2013
                                                <br />
                                                June 24 - Sep 14, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>118</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 118 Lean Six Sigma Black Belt Certification.pdf">
                                                Lean Six Sigma Black Belt Certification (2 certifications: LSSGB, SSBB 1036847,
                                                1028067) </a></span>
                                        </td>
                                        <td>
                                            <span>Avg. 26 hrs/Wk, 8 weeks </span>
                                        </td>
                                        <td>
                                            <span>$8,000 * </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Mar 1, 2013
                                                <br />
                                                Feb 19 - Mar 30, 2013
                                                <br />
                                                Apr 1 - July 1, 2013
                                                <br />
                                                May 13 - Aug 12, 2013
                                                <br />
                                                June 24 - Sep 2, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>323</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 323 Management Certification.pdf">Management
                                                Certification (2 Certifications; Course 1052375 & 1054679) LSSGB, PMP</a> </span>
                                        </td>
                                        <td>
                                            <span>(2 certifications: 26 hrs per week, 5 weeks </span>
                                        </td>
                                        <td>
                                            <span>$6,000 * </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 9, 2013
                                                <br />
                                                Feb 18 - Mar 23, 2013
                                                <br />
                                                Apr 1 - Apr 20, 2013
                                                <br />
                                                May 13 - June 15, 2013
                                                <br />
                                                June 24 - July 27, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>324</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 324 Management Certification for Mfg Professionals.pdf">
                                                Management Certification for Manufacturing Professionals (Lean Six Sigma GB, Lean
                                                Expert, Supply Chain Cert., ISO 9001 or CQA) </a></span>
                                        </td>
                                        <td>
                                            <span>(4 Certifications, Avg. 26 hrs per week, 7 weeks </span>
                                        </td>
                                        <td>
                                            <span>$5,000 * </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 23, 2013
                                                <br />
                                                Feb 19 - Mar 23, 2013
                                                <br />
                                                Apr 1 - June 22, 2013
                                                <br />
                                                May 13 - Aug 5, 2013
                                                <br />
                                                June 24 - July 27, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>325</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 325 Bio Medical Certification.pdf">BIO
                                                Medical/Pharm Certification � Any 4 (Lean Six Sigma GB, Bio Medical or Pharm. Courses)
                                            </a></span>
                                        </td>
                                        <td>
                                            <span>(4 Certifications, Avg. 26 hrs per week, 7 weeks) </span>
                                        </td>
                                        <td>
                                            <span>$5,000 * </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 23, 2013
                                                <br />
                                                Feb 19 - Mar 23, 2013
                                                <br />
                                                Apr 1 - June 22, 2013
                                                <br />
                                                May 13 - Aug 5, 2013
                                                <br />
                                                June 24 - July 27, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>124A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 124 Lean Expert Certification.pdf">Lean
                                                Expert Certification (Includes LSSGB) </a></span>
                                        </td>
                                        <td>
                                            <span>2 certifications, Avg. 34 hours per week, 3 Weeks) </span>
                                        </td>
                                        <td>
                                            <span>$5,000 </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Jan 26, 2013<br />
                                                Feb 19 - Mar 9, 2013
                                                <br />
                                                Apr 1 - Apr 20, 2013
                                                <br />
                                                May 13 - June 1, 2013
                                                <br />
                                                June 24 - July 13, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>221</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 221 PMI- Certified Project Mgmt Professional.pdf">
                                                Project Management (PMP) Certification Training</a> </span>
                                        </td>
                                        <td>
                                            <span>(20 hrs per week, 3 weeks) </span>
                                        </td>
                                        <td>
                                            <span>$3,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 22 - Feb 9, 2013
                                                <br />
                                                Mar 4 - Mar 23, 2013
                                                <br />
                                                Apr 15 - May 4, 2013
                                                <br />
                                                May 28 - June 15, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>114A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 114A Six Sigma Black Belt.pdf">Six Sigma
                                                Black Belt Certification (Includes LSSGB) </a></span>
                                        </td>
                                        <td>
                                            <span>(Avg. 28 hrs/Wk, 6 weeks) </span>
                                        </td>
                                        <td>
                                            <span>$6,900 * </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 16, 2013
                                                <br />
                                                Feb 18 - Mar 30, 2013
                                                <br />
                                                Apr 1 - May 11, 2013
                                                <br />
                                                May 13 - Aug 12, 2013
                                                <br />
                                                June 24 - Sep 23, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>113A</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 113A Lean Six Sigma Green Belt.pdf">Lean
                                                Six Sigma Green Belt Certification </a></span>
                                        </td>
                                        <td>
                                            <span>(4 days & 5 days, Avg. 36 hours per week, 2 weeks) </span>
                                        </td>
                                        <td>
                                            <span>$3,500 </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Jan 19, 2013
                                                <br />
                                                Feb 19 - Mar 2, 2013
                                                <br />
                                                Apr 1 - Apr 13, 2013
                                                <br />
                                                May 13 - May 25, 2013
                                                <br />
                                                June 24 - July 5, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>322</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 322 Certified Inspector and Technican.pdf">
                                                Inspector and Technician Certification </a></span>
                                        </td>
                                        <td>
                                            <span>(Avg. 24 hrs per week, 3 weeks) </span>
                                        </td>
                                        <td>
                                            <span>$2,000 </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Jan 19, 2013
                                                <br />
                                                Feb 19 - Mar 2, 2013
                                                <br />
                                                Apr 1 - Apr 13, 2013
                                                <br />
                                                May 13 - May 25, 2013
                                                <br />
                                                June 24 - July 5, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>177</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 177 Sales and Customer Service Analyst.pdf">
                                                Sales, Marketing & Customer Service </a></span>
                                        </td>
                                        <td>
                                            <span>2 certificates 24 hrs/Wk for 3 weeks </span>
                                        </td>
                                        <td>
                                            <span>$2,000 </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Jan 19, 2013
                                                <br />
                                                Feb 19 - Mar 2, 2013
                                                <br />
                                                Apr 1 - Apr 13, 2013
                                                <br />
                                                May 13 - May 25, 2013
                                                <br />
                                                June 24 - July 5, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>223</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 223 ASQ Certification Preparation courses.pdf">
                                                ASQ Certifications Training (any 3 ASQ Certifications) </a></span>
                                        </td>
                                        <td>
                                            <span>3 certificates 20 hrs/Wk average, 5 weeks </span>
                                        </td>
                                        <td>
                                            <span>$3,000 </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 16, 2013
                                                <br />
                                                Feb 18 - Mar 30, 2013
                                                <br />
                                                Apr 1 - May 11, 2013
                                                <br />
                                                May 13 - Aug 12, 2013
                                                <br />
                                                June 24 - Sep 23, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>245</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 245 Computer and Software Basics.pdf">
                                                Computer, Software & Accounting (MS Office, Quickbooks, Internet) </a></span>
                                        </td>
                                        <td>
                                            <span>20 hrs/Wk Avg.<br />5 weeks </span>
                                        </td>
                                        <td>
                                            <span>$2,000 </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 16, 2013
                                                <br />
                                                Feb 18 - Mar 30, 2013
                                                <br />
                                                Apr 1 - May 11, 2013
                                                <br />
                                                May 13 - Aug 12, 2013
                                                <br />
                                                June 24 - Sep 23, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>321</span>
                                        </td>
                                        <td>
                                            <span><a target="_blank" href="http://qpsinc.com/Documents/IL 321 Certified Production Operator and Technican.pdf">
                                                Production & Supervisory Skills </a></span>
                                        </td>
                                        <td>
                                            <span>(20 hrs/Wk for 5 weeks) </span>
                                        </td>
                                        <td>
                                            <span>$2,000 </span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 16, 2013
                                                <br />
                                                Feb 18 - Mar 30, 2013
                                                <br />
                                                Apr 1 - May 11, 2013
                                                <br />
                                                May 13 - Aug 12, 2013
                                                <br />
                                                June 24 - Sep 23, 2013 </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>--%>
                        </div>
                    </div>
                    <div id="New York" style="display: none; height: 0px; padding-right: 40px">
                        <div class="Grid">
                            <b>Available every month. Please <a href="mailto:info@qpsinc.com">contact us</a>.</b>
                            <%--   <table class="CourseTab" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>
                                            Course No.
                                        </th>
                                        <th width="370px;">
                                            Course
                                        </th>
                                        <th width="155px;">
                                            Days
                                        </th>
                                        <th>
                                            Approved For
                                        </th>
                                        <th width="95px;">
                                            Fees
                                        </th>
                                        <th width="165px;">
                                            Dates
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="oddRow">
                                        <td>
                                            <span>331</span>
                                        </td>
                                        <td>
                                            <span>Master Expert Certification (Customized for your needs) - 5 certifications
                                            </span>
                                        </td>
                                        <td>
                                            <span>Avg. 26 hrs/Wk, 12 weeks </span>
                                        </td>
                                        <td>
                                            <span>NEG </span>
                                        </td>
                                        <td>
                                            <span>$15,000 *</span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Mar 30, 2013<br />
                                                Feb 18 - May 11, 2013<br />
                                                Apr 1 - June 22, 2013<br />
                                                May 13 - Aug 3, 2013<br />
                                                June 24 - Sep 14, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>118</span>
                                        </td>
                                        <td>
                                            <span>Lean Six Sigma Black Belt Certification (2 certifications: 1036847,1028067)
                                            </span>
                                        </td>
                                        <td>
                                            <span>Avg. 26 hrs/Wk, 8 weeks </span>
                                        </td>
                                        <td>
                                            <span>NEG </span>
                                        </td>
                                        <td>
                                            <span>$10,000 *</span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Mar 1, 2013<br />
                                                Feb 19 - Mar 30, 2013<br />
                                                Apr 1 - July 1, 2013<br />
                                                May 13 - Aug 12, 2013<br />
                                                June 24 - Sep 2, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>324</span>
                                        </td>
                                        <td>
                                            <span>Management Certification for Manufacturing Professionals (Lean Six Sigma GB, Le
                                                an Expert, Supply Chain Cert., ISO 9001) </span>
                                        </td>
                                        <td>
                                            <span>(4 Certifications, Avg. 26 hrs per week, 7 weeks </span>
                                        </td>
                                        <td>
                                            <span>NEG </span>
                                        </td>
                                        <td>
                                            <span>$7,500 *</span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 23, 2013<br />
                                                Feb 19 - Mar 23, 2013<br />
                                                Apr 1 - June 22, 2013<br />
                                                May 13 - Aug 5, 2013<br />
                                                June 24 - July 27, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>325</span>
                                        </td>
                                        <td>
                                            <span>BIO Medical Certification (Lean Six Sigma GB, Bio Medical Courses, ISO 13485 Auditing,
                                                BIO Med Auditor Certification) </span>
                                        </td>
                                        <td>
                                            <span>(4 Certifications, Avg. 26 hrs per week, 7 weeks) </span>
                                        </td>
                                        <td>
                                            <span>NEG </span>
                                        </td>
                                        <td>
                                            <span>$7,500 *</span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 23, 2013<br />
                                                Feb 19 - Mar 23, 2013<br />
                                                Apr 1 - June 22, 2013<br />
                                                May 13 - Aug 5, 2013<br />
                                                June 24 - July 27, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>221</span>
                                        </td>
                                        <td>
                                            <span>Project Management (PMP) Certification Prep Training (QPS certification provided
                                                also) </span>
                                        </td>
                                        <td>
                                            <span>(20 hrs per week, 3 weeks) </span>
                                        </td>
                                        <td>
                                            <span>NEG </span>
                                        </td>
                                        <td>
                                            <span>$3,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 22 - Feb 9, 2013<br />
                                                Mar 4 - Mar 23, 2013<br />
                                                Apr 15 - May 4, 2013<br />
                                                May 28 - June 15, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>114A</span>
                                        </td>
                                        <td>
                                            <span>Six Sigma Black Belt Certification (Includes LSSGB) </span>
                                        </td>
                                        <td>
                                            <span>(Avg. 29 hrs/Wk, 7 weeks) </span>
                                        </td>
                                        <td>
                                            <span>NEG </span>
                                        </td>
                                        <td>
                                            <span>$6,900</span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 16, 2013<br />
                                                Feb 18 - Mar 30, 2013<br />
                                                Apr 1 - May 11, 2013<br />
                                                May 13 - Aug 12, 2013<br />
                                                June 24 - Sep 23, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>321</span>
                                        </td>
                                        <td>
                                            <span>Certified Production Operator and Technician (Certification provided) </span>
                                        </td>
                                        <td>
                                            <span>(Avg. 26 hrs per week, 5 weeks) </span>
                                        </td>
                                        <td>
                                            <span>NEG </span>
                                        </td>
                                        <td>
                                            <span>$3,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 9, 2013<br />
                                                Feb 18 - Mar 23, 2013<br />
                                                Apr 1 - Apr 20, 2013<br />
                                                May 13 - June 15, 2013<br />
                                                June 24 - July 2, 2013 </span>
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            <span>322</span>
                                        </td>
                                        <td>
                                            <span>Inspector and Technician Certification (Certification Provided) </span>
                                        </td>
                                        <td>
                                            <span>(Ave 26 hrs per week, 5 weeks) </span>
                                        </td>
                                        <td>
                                            <span>NEG </span>
                                        </td>
                                        <td>
                                            <span>$3,000</span>
                                        </td>
                                        <td>
                                            <span>Jan 7 - Feb 9, 2013<br />
                                                Feb 18 - Mar 23, 2013<br />
                                                Apr 1 - Apr 20, 2013<br />
                                                May 13 - June 15, 2013<br />
                                                June 24 - July 2, 2013 </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>--%>
                        </div>
                    </div>
                    <br />
                    <span style="color: Gray">&nbsp;Upon request, the details of each class are provided.
                        *some assistance may be available if DET does not approve full amount.</span>
                </ContentTemplate>
            </ajax:UpdatePanel>
            </form>
        </div>
    </div>
    <p style="float: right; text-align: right;">
        <a class="nounder" href="#top" title="Top">
            <img src="../images/top.gif" title="Top"></a></p>
</asp:Content>

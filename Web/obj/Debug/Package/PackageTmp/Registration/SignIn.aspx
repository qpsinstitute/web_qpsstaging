<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="QPS.Registration.SignIn" MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentSignIn" ContentPlaceHolderID="cphContent" runat="server">
<script language="javascript" type="text/javascript">
setPage("Training");
</script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <br class="clear">
            <h3 class="colbright" id="H3_1">
                Administrator Login</h3>
                 <div style="position:absolute; left:900px;top:196px;">  <asp:HyperLink ID="Link" runat="server" Text="Back to Registration Form" NavigateUrl="~/Registration/Registration.aspx"></asp:HyperLink></div>
            <form id="fRegister" runat="server">
                <div id="dreg" runat="server">
                    <table width="100%" style="padding-left:35px;">
                        <tr>
                            <td colspan="2">
                            </td>
                            
                        </tr>
                        <tr>
                            <td style="width: 102px">
                            </td>
                            <td>
                                <br />
                                <asp:Label ID="lblmsg" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="right" style="width: 102px">
                                Email Address
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtemail" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvemail" runat="server" ControlToValidate="txtemail" ErrorMessage="*" Display="Dynamic" ValidationGroup="SignIn"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionEmail" runat="server" ErrorMessage="Invalid email address" ControlToValidate="txtemail" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="SignIn"></asp:RegularExpressionValidator>&nbsp;&nbsp;
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="right" style="width: 102px">
                               Password
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtPassword" runat="server" Width="200px" MaxLength="50" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword" ErrorMessage="*" Display="Dynamic" ValidationGroup="SignIn"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 102px">
                            </td>
                            <td>
                                <br />
                                 <input id="chkVal" runat="server" type="hidden" value="" />
                                <asp:Button ID="btnsignin" runat="server" Text="Login" ValidationGroup="SignIn" OnClick="btnLogin_Click" />&nbsp;
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 102px">
                            </td>
                            <td>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False" ValidationGroup="SignIn" />
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
        <div class="span-4">
            <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
    </div>


</asp:Content>

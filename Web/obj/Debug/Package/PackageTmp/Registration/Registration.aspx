<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="QPS.Registration.Registration"
    MasterPageFile="~/QPSSite.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>
<asp:Content ID="ContentRegister" ContentPlaceHolderID="cphContent" runat="server">
 
    <script language="javascript" type="text/javascript">
        setPage("Training");
        function setFocus() {
            document.getElementById("txtFname").focus();
        }
        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode >= 48 && charCode <= 57) return false;
            return true;
            //            if (charCode > 31 && (charCode < 48 || charCode > 57 || charCode == 08))
            //                return true;
            //            return false;
        }
        function autoIframe(frameId) {
            frame = document.getElementById(frameId);
            innerDoc = (frame.contentDocument) ? frame.contentDocument : frame.contentWindow.document;
            objToResize = (frame.style) ? frame.style : frame;
            objToResize.height = (innerDoc.body.scrollHeight) + 'px';
            objToResize.width = (innerDoc.body.scrollWidth + 20) + 'px';
        }

        function istxt(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode

            if (charCode > 31 && (charCode < 48 || charCode > 57 || charCode == 08) && charCode != 45)
                return false;
            else
                return true;
        }



        function hidewait() {
            //            debugger;
            //            frameid = document.getElementById('iframe1');
            //            var d = document.getElementById('repdiv');

            //            d.innerHTML = "";
            //  frameid.src = "";

            var iframe = document.getElementById('iframe1');
            var innerDoc = iframe.contentDocument || iframe.contentWindow.document;

            var d = innerDoc.getElementById('repdiv');
            d.innerHTML = '';
        }


        function checkData(year, ddlDay, MonthValue) {
            try {
                if (ddlDay.options.length == 30) {
                    ddlDay.options[30] = new Option('30', '30');
                    ddlDay.options[31] = new Option('31', '31');
                }
                else if (ddlDay.options.length == 29) {
                    ddlDay.options[29] = new Option('29', '29');
                    ddlDay.options[30] = new Option('30', '30');
                    ddlDay.options[31] = new Option('31', '31');
                }

                if (MonthValue == "2") {

                    if (ddlDay.options.length == 32) {
                        if ((year % 4) == 0) {
                            ddlDay.options[31] = null;
                            ddlDay.options[30] = null;
                        }
                        else {
                            ddlDay.options[31] = null;
                            ddlDay.options[30] = null;
                            ddlDay.options[29] = null;
                        }
                    }
                    else {
                        if ((year % 4) == 0) {

                            ddlDay.options[30] = null;
                        }
                        else {
                            ddlDay.options[30] = null;
                            ddlDay.options[29] = null;
                        }

                    }
                }

                else if ((MonthValue == "4") || (MonthValue == "6") || (MonthValue == "9") || (MonthValue == "11")) {
                    ddlDay.options[31] = null;
                }
                else {
                    if (ddlDay.options.length != "32")
                        ddlDay.options[31] = new Option('31', '31');
                }

            } catch (ex) { }
        }

        function chkfeb(id) {
            var year = document.getElementById('<%=ddlYear.ClientID%>').value;
            var ddlDay = document.getElementById('<%=ddlDay.ClientID%>');
            var MonthVal = document.getElementById('<%=ddlMonth.ClientID%>').value;
            checkData(year, ddlDay, MonthVal);
        }
        function chksessiondate() {
            var year = document.getElementById('<%=ddlsessionyear.ClientID%>').value;
            var ddlDay = document.getElementById('<%=ddlsessionday.ClientID%>');
            var MonthVal = document.getElementById('<%=ddlsessionmonth.ClientID%>').value;
            checkData(year, ddlDay, MonthVal);
        }
        function chkAppdate() {
            var year = document.getElementById('<%=ddlappyear.ClientID%>').value;
            var ddlDay = document.getElementById('<%=ddlappday.ClientID%>');
            var MonthVal = document.getElementById('<%=ddlappmonth.ClientID%>').value;
            checkData(year, ddlDay, MonthVal);
        }
        function validateAppdate(sender, args) {
            if (document.getElementById('<%=ddlappmonth.ClientID%>').selectedIndex > 0 && document.getElementById('<%=ddlappyear.ClientID%>').selectedIndex > 0 && document.getElementById('<%=ddlappday.ClientID%>').selectedIndex > 0)
                args.IsValid = true;
            else
                args.IsValid = false;

        }

        function textboxMultilineMaxNumber(txt, maxLen) {
            try {
                if (txt.value.length > (maxLen - 1)) return false;
            } catch (e) {
            }
        }
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <br class="clear">
            <h3 class="colbright" id="H3_1">
                Registration Form</h3>
            <form id="fRegister" runat="server">
            <ajax:ScriptManager ID="sm1" runat="server">
            </ajax:ScriptManager>
            <%-- <ajax:UpdatePanel ID="UP1" runat="server">
                <ContentTemplate>--%>
            <div id="dreg" runat="server">
                <table width="100%" runat="server" id="tblAdd" style="display: none;">
                    <tr>
                        <td colspan="2" width="55%;">
                            Fields marked with <span style="color: Red">*</span> are mandatory
                            <br />
                        </td>
                        <td style="padding: 1; border: 1px solid silver;">
                            <table>
                                <tr>
                                    <td align="center" style="color: #141D6B; padding: 0 80px; font-size: 1.1em;">
                                        <b>For Admin Use</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0;">
                                        To view registrations, Please
                                        <asp:HyperLink ID="Link" runat="server" NavigateUrl="~/Registration/ViewRegistration.aspx"
                                            Text="click here"></asp:HyperLink>
                                        to login.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <br />
                            <asp:Label ID="lblmsg" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="200px">
                            First Name <span style="color: red">*</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFname" runat="server" Width="200px" onkeypress="return isNumberKey(event);"
                                MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvFirstName" runat="server"
                                ControlToValidate="txtFname" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rgvFname" runat="server" Display="Dynamic" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"
                                ControlToValidate="txtFname" ValidationGroup="register" SetFocusOnError="true"
                                ErrorMessage="Enter proper value "></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Last Name <span style="color: red">*</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtLname" runat="server" Width="200px" onkeypress="return isNumberKey(event);"
                                MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvLastName" runat="server"
                                ControlToValidate="txtLname" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rgvLname" runat="server" Display="Dynamic" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"
                                ControlToValidate="txtLname" ValidationGroup="register" SetFocusOnError="true"
                                ErrorMessage="Enter proper value"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Email Address <span style="color: red">*</span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtemail" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtemail"
                                ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionEmail"
                                runat="server" ErrorMessage="Invalid email address" ControlToValidate="txtemail"
                                Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="register"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Cell Phone <span style="color: red">*</span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtmob" onkeypress="return istxt(event);" runat="server" Width="200px"
                                MaxLength="12"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvcellPhone" runat="server" ControlToValidate="txtmob"
                                ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionPhone"
                                runat="server" ControlToValidate="txtmob" ErrorMessage="Enter cell phone number (xxx-xxx-xxxx) format"
                                ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" Display="Dynamic"
                                ValidationGroup="register"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Career Center State
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlccstate">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="1">CT</asp:ListItem>
                                <asp:ListItem Value="2">IL</asp:ListItem>
                                <asp:ListItem Value="3">MA</asp:ListItem>
                                <asp:ListItem Value="4">NC</asp:ListItem>
                                <asp:ListItem Value="5">NJ</asp:ListItem>
                                <asp:ListItem Value="6">RI</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Career Center City
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtccenter" runat="server" onkeypress="return isNumberKey(event);"
                                Width="200px" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Career Counselor Name
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtccounselor" onkeypress="return isNumberKey(event);" runat="server"
                                Width="200px" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Career Counselor Email
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtccemail" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                            <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionValidator1"
                                runat="server" ErrorMessage="Invalid email address" ControlToValidate="txtccemail"
                                Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="register"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Course Intrested In <span style="color: red">*</span>
                        </td>
                        <td align="left" colspan="2">
                            <%--<asp:TextBox ID="txtcourse" runat="server" Width="200px" MaxLength="50"></asp:TextBox>--%>
                            <asp:DropDownList ID="ddlcoursename" runat="server" Width="280">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator2" runat="server"
                                ControlToValidate="ddlcoursename" ErrorMessage="*" Display="Dynamic" InitialValue="0"
                                ValidationGroup="register"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Session Start Date
                        </td>
                        <td colspan="2" align="left">
                         <asp:DropDownList ID="ddlsessionmonth" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessiondate();">
                                <asp:ListItem Value="0">Month</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlsessionday" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessiondate();">
                                <asp:ListItem Value="0">Day</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlsessionyear" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessiondate();">
                                <asp:ListItem Value="0">Year</asp:ListItem>
                            </asp:DropDownList>
                             <asp:Label ID="lblerror1" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Session Start Time
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlsessionhour" runat="server">
                                <asp:ListItem Value="-1">Hour</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlsessionminute" runat="server">
                                <asp:ListItem Value="-1">Minute</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            QPS Staffing & Recruitment Intrested In
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtstaff" runat="server" Width="200px" onkeypress="return isNumberKey(event);"
                                MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Work Experience Background
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtwexp" runat="server" Width="200px" Height="80px" MaxLength="20"
                                onkeypress="return textboxMultilineMaxNumber(this,200)" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Academic Background
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtaback" runat="server" Width="200px" Height="80px" MaxLength="10"
                                onkeypress="return textboxMultilineMaxNumber(this,200)" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Current Certifications
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtccerti" runat="server" Width="200px" Height="80px" MaxLength="14"
                                onkeypress="return textboxMultilineMaxNumber(this,200)" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Resume
                        </td>
                        <td  >
                            <asp:FileUpload ID="ResumeUpload" runat="server" Width="200px" />
                          
                        </td>
                        <td align="left">
                              <asp:Label ID="lblresume" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Other Attachments
                        </td>
                        <td align="left" colspan="2">
                            <%
                                System.Data.DataTable dtTemp = null;
                                if (Session["Session_Table"] != null)
                                    dtTemp = (System.Data.DataTable)Session["Session_Table"];
                                if (Session["Session_Table"] == null || dtTemp.Rows.Count == 0)
                                {%>
                            <iframe id="iframeupload" src="FileData.aspx" allowtransparency="true" style="width: 500px;
                                overflow: hidden; clear: both; height: 40px;" frameborder="0" marginheight="0px"
                                scrolling="auto" onload="if (window.parent && window.parent.autoIframe) {window.parent.autoIframe('iframeupload');}">
                            </iframe>
                            <%}
                                else
                                { %>
                            <iframe id="iframe1" src="FileData.aspx" allowtransparency="true" style="display: block;
                                width: 500px; overflow: hidden;" frameborder="0" marginheight="0px" scrolling="no"
                                onload="if (window.parent && window.parent.autoIframe) {window.parent.autoIframe('iframe1');}">
                            </iframe>
                            <%} %>
                        </td>
                    </tr>
                    <tr>
                     <td align="right">
                     Verification Code <span style="color: red">*</span>
                     </td>
                        <td>
                         <asp:TextBox ID="txtVerify" style="vertical-align:top" width="70px" Height="24px" Font-Size="Large"  runat="server"></asp:TextBox>&nbsp;
                         <asp:Image ID="imCaptcha" ImageUrl="~/Controls/Captcha.ashx" ImageAlign="AbsMiddle"  runat="server" />
                         <asp:RequiredFieldValidator ID="reqcaptcha" runat="server" Display="Dynamic" ValidationGroup="register" ControlToValidate="txtVerify"  style="vertical-align:super;" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                    <td align="right">
                     </td>
                    <td>
                      <asp:CustomValidator ID="CustomValidator2" runat="server" ValidationGroup="register" ControlToValidate="txtVerify" Display="Dynamic"
                         ErrorMessage="You have Entered a Wrong Verification Code!Please Re-enter!!!" OnServerValidate="CAPTCHAValidate"></asp:CustomValidator>  
                    </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <br />
                            <asp:Button ID="btnsubmit" runat="server" Text="Submit" ValidationGroup="register"
                                OnClick="btnsubmit_Click" />&nbsp;
                            <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False"
                                ValidationGroup="register" />
                        </td>
                    </tr>
                </table>
                <table width="100%" id="tbledit" runat="server" style="display: none;">
                    <tr>
                        <td colspan="2">
                            <b>Edit Registration Details</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="200px">
                            First Name
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Fname" onkeypress="return isNumberKey(event);" runat="server" Width="200px"
                                MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqfname" runat="server" ControlToValidate="Fname"
                                ErrorMessage="*" Display="Dynamic" ValidationGroup="editor"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regfname" runat="server" Display="Dynamic" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"
                                ControlToValidate="Fname" ValidationGroup="editor" SetFocusOnError="true" ErrorMessage="Enter proper value "></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Last Name
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Lname" onkeypress="return isNumberKey(event);" runat="server" Width="200px"
                                MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqlname" runat="server" ControlToValidate="Lname"
                                ErrorMessage="*" Display="Dynamic" ValidationGroup="editor"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="reglname" runat="server" Display="Dynamic" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"
                                ControlToValidate="Lname" ValidationGroup="editor" SetFocusOnError="true" ErrorMessage="Enter proper value"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Email Address
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Email" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqemail" runat="server" ControlToValidate="Email"
                                ErrorMessage="*" Display="Dynamic" ValidationGroup="editor"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator SetFocusOnError="true" ID="regemail" runat="server"
                                ErrorMessage="Invalid email address" ControlToValidate="Email" Display="Dynamic"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="editor"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Cell Phone
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="Phone" onkeypress="return istxt(event);" runat="server" Width="200px"
                                MaxLength="12"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqPhone" runat="server" ControlToValidate="Phone"
                                ErrorMessage="*" Display="Dynamic" ValidationGroup="editor"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator SetFocusOnError="true" ID="regPhone" runat="server"
                                ControlToValidate="Phone" ErrorMessage="Enter cell phone number (xxx-xxx-xxxx) format"
                                ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" Display="Dynamic"
                                ValidationGroup="editor"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Career Center State
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlviewCCenterState">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="CT">CT</asp:ListItem>
                                <asp:ListItem Value="IL">IL</asp:ListItem>
                                <asp:ListItem Value="MA">MA</asp:ListItem>
                                <asp:ListItem Value="NC">NC</asp:ListItem>
                                <asp:ListItem Value="NJ">NJ</asp:ListItem>
                                <asp:ListItem Value="RI">RI</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Career Center City
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Center" onkeypress="return isNumberKey(event);" runat="server" Width="200px"
                                MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Career Counselor Name
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Counselor" onkeypress="return isNumberKey(event);" runat="server"
                                Width="200px" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Career Counselor Email
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtviewccemail" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Course Intrested In
                        </td>
                        <td align="left">
                            <%--  <asp:TextBox ID="Course" runat="server" Width="200px" Enabled="false" MaxLength="50"></asp:TextBox>--%>
                            <asp:DropDownList ID="ddleditcourse" runat="server" Width="280">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator3" runat="server"
                                ControlToValidate="ddleditcourse" ErrorMessage="*" Display="Dynamic" InitialValue="0"
                                ValidationGroup="editor"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Session Start Date
                        </td>
                        <td colspan="2" align="left">
                            <asp:DropDownList ID="ddlMonth" runat="server" AppendDataBoundItems="True" onchange="javascript:chkfeb(this.id);">
                                <asp:ListItem Value="0">Month</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlDay" runat="server" AppendDataBoundItems="True" onchange="javascript:chkfeb(this.id);">
                                <asp:ListItem Value="0">Day</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlYear" runat="server" AppendDataBoundItems="True" onchange="javascript:chkfeb(this.id);">
                                <asp:ListItem Value="0">Year</asp:ListItem>
                            </asp:DropDownList>
                             <asp:Label ID="lblsessionval" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Session Start Time
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlviewhour" runat="server">
                                <asp:ListItem Value="-1">Hour</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlviewminute" runat="server">
                                <asp:ListItem Value="-1">Minute</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            QPS Staffing & Recruitment Intrested In
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtviewStaffing" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Work Experience Background
                        </td>
                        <td align="left">
                            <asp:TextBox ID="WorkExp" runat="server" Width="200px" Height="80px" MaxLength="10"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Academic Background
                        </td>
                        <td align="left">
                            <asp:TextBox ID="AcedemicBG" runat="server" Width="200px" Height="80px" MaxLength="200"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Current Certifications
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Certification" runat="server" Width="200px" Height="80px" MaxLength="200"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 142px">
                            Information Sent
                        </td>
                        <td align="left" style="width: 647px">
                            <asp:TextBox ID="txtSentInfo" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 142px">
                            Course Matrix
                        </td>
                        <td align="left" style="width: 647px">
                            <asp:TextBox ID="txtMartix" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 142px">
                            Course Brochure
                        </td>
                        <td align="left" style="width: 647px">
                            <asp:TextBox ID="txtBrochure" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 142px">
                            Appointment Date 
                        </td>
                        <td colspan="2" align="left" style="width: 647px">
                             <asp:DropDownList ID="ddlappmonth" runat="server" AppendDataBoundItems="True" onchange="javascript:chkAppdate();">
                                <asp:ListItem Value="0">Month</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlappday" runat="server" AppendDataBoundItems="True" onchange="javascript:chkAppdate();">
                                <asp:ListItem Value="0">Day</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlappyear" runat="server" AppendDataBoundItems="True" onchange="javascript:chkAppdate();">
                                <asp:ListItem Value="0">Year</asp:ListItem>
                            </asp:DropDownList>
                            <%--<asp:CustomValidator style="padding-left :6px;" runat="server"  Display="Dynamic"  Font-Bold="true"  id="customeapp" SetFocusOnError="true"   ValidationGroup="editor"  ErrorMessage="*" ClientValidationFunction="validateAppdate" />--%>
                            <asp:Label ID="lblError" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>

                            <asp:CheckBox ID="chkfund" runat="server" Text="Denied Funding" Checked="true" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 142px; height: 24px;">
                            Time 
                        </td>
                        <td align="left" colspan="2" style="height: 24px; width: 647px;">
                            <asp:DropDownList ID="ddlHour" runat="server">
                                <asp:ListItem Value="-1">Hour</asp:ListItem>
                            </asp:DropDownList>
                            <%--<asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvTime" runat="server" ControlToValidate="ddlHour"
                                ErrorMessage="*" Display="Dynamic" InitialValue="-1" ValidationGroup="editor"></asp:RequiredFieldValidator>--%>
                            &nbsp; &nbsp;
                            <asp:DropDownList ID="ddlMinute" runat="server">
                                <asp:ListItem Value="-1">Minute</asp:ListItem>
                            </asp:DropDownList>
                            <%--<asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="ddlMinute" ErrorMessage="*" Display="Dynamic" InitialValue="-1"
                                ValidationGroup="editor"></asp:RequiredFieldValidator>--%>
                            &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr runat="server" id="trcity" visible="false">
                        <td align="right" style="width: 142px">
                            City <span id="city" runat="server" style="color: Red;">*</span>
                        </td>
                        <td style="width: 647px">
                            <asp:TextBox ID="txtCity" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvCity" runat="server" ControlToValidate="txtCity"
                                ErrorMessage="*" Display="Dynamic" ValidationGroup="editor"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr runat="server" id="trstate" visible="false">
                        <td align="left" style="width: 142px">
                            State: <span id="state" runat="server" style="color: Red;">*</span>
                        </td>
                        <td align="left" style="width: 647px">
                            <%--<asp:TextBox ID="txtState" runat="server" Width="200px" MaxLength="50"></asp:TextBox>--%>
                            <asp:DropDownList ID="ddlState" runat="server">
                                <asp:ListItem Value="0">Select State</asp:ListItem>
                                <asp:ListItem Value="AL">Alabama</asp:ListItem>
                                <asp:ListItem Value="AK">Alaska</asp:ListItem>
                                <asp:ListItem Value="AZ">Arizona</asp:ListItem>
                                <asp:ListItem Value="AR">Arkansas</asp:ListItem>
                                <asp:ListItem Value="CA">California</asp:ListItem>
                                <asp:ListItem Value="CO">Colorado</asp:ListItem>
                                <asp:ListItem Value="CT">Connecticut</asp:ListItem>
                                <asp:ListItem Value="DC">District of Columbia</asp:ListItem>
                                <asp:ListItem Value="DE">Delaware</asp:ListItem>
                                <asp:ListItem Value="FL">Florida</asp:ListItem>
                                <asp:ListItem Value="GA">Georgia</asp:ListItem>
                                <asp:ListItem Value="HI">Hawaii</asp:ListItem>
                                <asp:ListItem Value="ID">Idaho</asp:ListItem>
                                <asp:ListItem Value="IL">Illinois</asp:ListItem>
                                <asp:ListItem Value="IN">Indiana</asp:ListItem>
                                <asp:ListItem Value="IA">Iowa</asp:ListItem>
                                <asp:ListItem Value="KS">Kansas</asp:ListItem>
                                <asp:ListItem Value="KY">Kentucky</asp:ListItem>
                                <asp:ListItem Value="LA">Louisiana</asp:ListItem>
                                <asp:ListItem Value="ME">Maine</asp:ListItem>
                                <asp:ListItem Value="MD">Maryland</asp:ListItem>
                                <asp:ListItem Value="MA">Massachusetts</asp:ListItem>
                                <asp:ListItem Value="MI">Michigan</asp:ListItem>
                                <asp:ListItem Value="MN">Minnesota</asp:ListItem>
                                <asp:ListItem Value="MS">Mississippi</asp:ListItem>
                                <asp:ListItem Value="MO">Missouri</asp:ListItem>
                                <asp:ListItem Value="MT">Montana</asp:ListItem>
                                <asp:ListItem Value="NE">Nebraska</asp:ListItem>
                                <asp:ListItem Value="NV">Nevada</asp:ListItem>
                                <asp:ListItem Value="NH">New Hampshire</asp:ListItem>
                                <asp:ListItem Value="NJ">New Jersey</asp:ListItem>
                                <asp:ListItem Value="NM">New Mexico</asp:ListItem>
                                <asp:ListItem Value="NY">New York</asp:ListItem>
                                <asp:ListItem Value="NC">North Carolina</asp:ListItem>
                                <asp:ListItem Value="ND">North Dakota</asp:ListItem>
                                <asp:ListItem Value="OH">Ohio</asp:ListItem>
                                <asp:ListItem Value="OK">Oklahoma</asp:ListItem>
                                <asp:ListItem Value="OR">Oregon</asp:ListItem>
                                <asp:ListItem Value="PA">Pennsylvania</asp:ListItem>
                                <asp:ListItem Value="RI">Rhode Island</asp:ListItem>
                                <asp:ListItem Value="SC">South Carolina</asp:ListItem>
                                <asp:ListItem Value="SD">South Dakota</asp:ListItem>
                                <asp:ListItem Value="TN">Tennessee</asp:ListItem>
                                <asp:ListItem Value="TX">Texas</asp:ListItem>
                                <asp:ListItem Value="UT">Utah</asp:ListItem>
                                <asp:ListItem Value="VT">Vermont</asp:ListItem>
                                <asp:ListItem Value="VA">Virginia</asp:ListItem>
                                <asp:ListItem Value="WA">Washington</asp:ListItem>
                                <asp:ListItem Value="WV">West Virginia</asp:ListItem>
                                <asp:ListItem Value="WI">Wisconsin</asp:ListItem>
                                <asp:ListItem Value="WY">Wyoming</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvState" runat="server" ControlToValidate="ddlState"
                                ErrorMessage="select state" Display="Dynamic" InitialValue="0" ValidationGroup="editor"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Other Attachments
                        </td>
                        <td align="left" colspan="2">
                            <%
                                System.Data.DataTable dtTemp = null;
                                if (Session["Session_Table"] != null)
                                    dtTemp = (System.Data.DataTable)Session["Session_Table"];
                                if (Session["Session_Table"] == null || dtTemp.Rows.Count == 0)
                                {%>
                            <iframe id="iframe2" src="FileData.aspx" allowtransparency="true" style="width: 500px;
                                overflow: hidden; clear: both; height: 40px;" frameborder="0" marginheight="0px"
                                scrolling="auto" onload="if (window.parent && window.parent.autoIframe) {window.parent.autoIframe('iframe2');}">
                            </iframe>
                            <%}
                                else
                                { %>
                            <iframe id="iframe3" src="FileData.aspx" allowtransparency="true" style="display: block;
                                width: 500px; overflow: hidden;" frameborder="0" marginheight="0px" scrolling="no"
                                onload="if (window.parent && window.parent.autoIframe) {window.parent.autoIframe('iframe2');}">
                            </iframe>
                            <%} %>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 142px">
                        </td>
                        <td style="width: 647px">
                            <br />
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" ValidationGroup="editor"
                                OnClick="btnUpdate_Click" />&nbsp;
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 142px">
                        </td>
                        <td style="width: 647px">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="false"
                                ValidationGroup="editor" />
                        </td>
                    </tr>
                </table>
            </div>
            <%--</ContentTemplate>
                <Triggers>
                    <ajax:PostBackTrigger ControlID="btnsubmit" />
                </Triggers>
            </ajax:UpdatePanel>--%>
            </form>
        </div>
        <div class="span-4">
            <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
    </div>


</asp:Content>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="masterHead" runat="server">
    <meta name="MSSmartTagsPreventParsing" content="TRUE" />
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
    <meta name="keywords" content="total quality management, tqm, six sigma, methodology, consulting, training, lean, ISO, ISO 9000, ISO 14000, 16949, ISO/TS, six-sigma employment, 6 sigma" />
    <meta name="description" content="A committed consulting and training partner for Six Sigma, Lean, and ISO. Customized training, on-site consulting, and results oriented work." />
    <title>Quality & Productivity Solutions, Inc. -- Dedicated provider of Six Sigma, Lean, and ISO Consulting and Training</title>
    <link rel="stylesheet" type="text/css" href="css/domestic.css" />
    <link rel="stylesheet" href="css/extended/screen.css" type="text/css" media="screen, projection">
    <link rel="stylesheet" href="css/extended/print.css" type="text/css" media="print">
    <!--[if IE]><link rel="stylesheet" href="css/extended/ie.css" type="text/css" media="screen, projection"><![endif]-->
    <link rel="stylesheet" href="css/extended/plugins/fancy-type/screen.css" type="text/css" media="screen, projection">
    <link rel="stylesheet" href="css/extended/src/typography.css" type="text/css" media="screen, projection">
    <script src="Js/javaja.js" type="text/javascript"></script>
    <script type="text/javascript" language="JavaScript1.2" src="Js/stmenu.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="Js/qfunctions.js"></script>
    <!-- RSS javascripts -->
    <meta http-equiv="pragma" content="no-cache">	
    <link rel="alternate" type="application/rss+xml" title="RSS" href="js/LatestNews.rss">
    <script src="js/prototype.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/scriptaculous.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/ticker.js" type="text/javascript" charset="utf-8"></script>
    <script src="../Js/jquery.js" type="text/javascript"></script>
<script src="../Js/jquery.innerfade.js" type="text/javascript"></script>
</head>
<body>
<div class="shadowBg">
    <div class="maincont" id="top">
	<div class="container">
	<!-- HEADER -->
     
	<div class="headerHM">
     <script language="javascript" type="text/javascript">

       
         var Brsr = navigator.appName;
         if (Brsr == "Microsoft Internet Explorer") {
             document.getElementById("MainDivForDesign").className = "topbar tops span-25 prepend-1 last;";
         }
         else {
             document.getElementById("MainDivForDesign").className = "topbar tops span-24 prepend-1 last;";
         }
	
      
</script>
    <ul id="menuHM">
        <li id="mnhome-top" ><a id="home-top" href="../default.aspx" title="Home">
            Home</a></li>
        <li id="mncompany-top"><a id="company-top" href="../Company/about.aspx" title="Company">
            Company</a></li>
        <li id="mntraining-top" class="activeHM"><a id="training-top" href="../Training/Calendar.aspx" title="Training" >
            Training</a></li>
        <li id="mnconsulting-top"><a id="consulting-top" href="../Consulting/Consulting.aspx"
            title="Consulting">Consulting</a></li>
        <li id="mnplacement-top"><a id="placement-top" href="../Placement/Aboutus.aspx" title="Staffing & Recruitment">
            Staffing & Recruitment</a></li>
        <li id="mnresources-top"><a id="resources-top" href="../Resources/Resources.aspx"
            title="Resources">Resources</a></li>
        <li id="mnevents-top"><a id="events-top" href="../Events/Default.aspx" title="Events">
            Events</a></li>
        <li id="mnwebinars-top"><a id="webinars-top" href="../Webinars/Default.aspx" title="Webinars">
            Webinars</a></li>
        <li id="mncontactus-top"><a id="contactus-top" href="../Company/Contact.aspx" title="Contact Us">
            Contact Us</a></li>
        <li class="homeIconHM"><a href="../cart.asp" style="font-size: 16px;" title="Add To Cart">
            <img src="../Images/cart_icon.png" title="ADD TO CART" />
        </a></li>
    </ul>
    <div class="newLogoWrp">
        <div class="qpsNewLogo">
            <a href="../Welcome.aspx" title="The QPS Institute">
                <img src="../Images/qps_logo.png" />
                <h1>
                    Quality & Productivity Solutions, Inc.
                </h1>
            </a>
        </div>
        <div class="headerTopRightWrp">
            <div class="headerSearchWrpHM">
                <form action="" id="searchbox_016124679326840018074:zth8bogqxfo">
                <div style="padding-top: 0px">
                    <input id="q" name="q" onblur="setImage()" onclick="removeImg()" size="20" style="background: #FFFFFF url(http://qpsinc.com/images/qps_watermark.gif) left no-repeat;"
                        title="Search" type="text" />
                    <input title="Search" type="submit" value="Search" onclick="OnLoad();" />
                </div>
                </form>
                <script src="../Js/brand.js" type="text/javascript"></script>
                <div id="results_016124679326840018074:zth8bogqxfo" style="display: none">
                    <div id="ANS1" class="cse-closeResults">
                        <a>&times; Close</a>
                    </div>
                    <div id="res" class="cse-resultsContainer">
                    </div>
                </div>
                <link href="../Css/overlay.css" rel="stylesheet" type="text/css" />
                <script src="http://www.google.com/uds/api? file=uds.js&v=1.0&key=ABQIAAAAR4CiGksj9ilcLyKw0v9fNBQfuHxrG_OBhPfTgMNXqY1zIg08ZBSJQZMdFYSEpm8BD5G4Gbe4ZBBUYg&hl=en"
                    type="text/javascript"></script>
                <script src="http://www.google.com/cse/api/overlay.js"></script>
                <script type="text/javascript">
                    function OnLoad() {
                        var cseo = new CSEOverlay("016124679326840018074:zth8bogqxfo", document.getElementById("searchbox_016124679326840018074:zth8bogqxfo"), document.getElementById("results_016124679326840018074:zth8bogqxfo"));
                        cseo.searchControl.setLinkTarget(GSearch.LINK_TARGET_PARENT);
                    }
                    GSearch.setOnLoadCallback(OnLoad);

                    document.getElementById("q").style.fontSize = "11pt";
                </script>
            </div>
            <div style="clear: both">
            </div>
            <div class="socialLinksWrpHM">
                <span style="color: White; vertical-align: super; font-size: 11px;">Follow Us:</span>
                &nbsp; <a target="_blank" href="http://www.blogger.com/profile/02032638210555019174"
                    style="text-decoration: none;">
                    <img src="../Images/1360240082_blogger.png" border="0" title="Blogger" style="" width="25px"
                        height="25px" /></a> <!--<a target="_blank" href="http://www.facebook.com/pages/The-QPS-Institute/189052384471963"
                            style="text-decoration: none;">
                            <img src="../Images/Facebook_icon_4.png" border="0" title="Facebook" style="" /></a>&nbsp;-->
                <a target="_blank" href="http://www.linkedin.com/groups/QPS-Institute-3887066?itemaction=mclk&anetid=3887066&impid=3887066-3085229&pgkey=anet_about_guest&actpref=anet_about-gbm"
                    style="text-decoration: none;">
                    <img src="../Images/linkedin.png" border="0" title="Linkedin" style="" /></a>&nbsp;
                <a target="_blank" href="https://twitter.com/#!/TheQPSInstitute" style="text-decoration: none;">
                    <img src="../Images/twitter.png" border="0" title="Twitter" style="" /></a>
                <a target="_blank" href="http://www.eventbrite.com/org/3223954808" style="text-decoration: none;
                    padding-left: 2px;">
                    <img src="../Images/eventbrite.png" border="0" title="Eventbrite" width="85px" height="25px" /></a>
            </div>
        </div>
        <div style="clear: both;">
        </div>
    </div>
</div>
<script language="javascript" type="text/javascript">
function removeImg()
{    
    document.getElementById("q").style.background = "#FFFFFF";
}

function setImage()
{ 
    if(document.getElementById("q").value == "")
        document.getElementById("q").style.background = "#FFFFFF url(http://qpsinc.com/images/qps_watermark.gif) left no-repeat";
}
function setPage(Pagename)
{
    if(Pagename == "Company")
    {
        document.getElementById('company-top').className = "topsact";
    }
//    else if(Pagename == "Training")
//    {
//        document.getElementById('training-top').className = "topsact";
//    }
    else if(Pagename == "Consulting")
    {
        document.getElementById('consulting-top').className = "topsact";
    }
    else if(Pagename == "Resources")
    {
        document.getElementById('resources-top').className = "topsact";
    }
    else if (Pagename == "eLearning")
    {
        document.getElementById('elearning-top').className = "topsact"
    }
}
</script>
<!-- Middle -->
<script language="javascript" type="text/javascript">
    setPage("Training");
</script>
<form>
<div id="test"  class="span-24 prepend-1 top highcont">
    <script language="javascript" type="text/javascript">
	    var Brsr=navigator.appName;
        if(Brsr=="Microsoft Internet Explorer")
        {
            document.getElementById("test").className="span-25 prepend-1 top highcont";
        }
        else
        {
            document.getElementById("test").className="span-24 prepend-1 top highcont";
        }
    </script>
	<div class="span-18 top" style="float:right">
  
               <div class="span-3 blackbk last" style="width: 115px">
                <a href="Training/Calendar.aspx" title="Corporate Calendar">Corporate Calendar</a></div>
            <div class="span-3 blackbl last" style="width: 120px">
                <a href="Training/UnEmployed.aspx" title="Public Training">Public Training</a></div>
            <div class="span-3 blackbl last" style="width: 100px">
                <a href="Training/OnSite.aspx" title="On-Site Training">On-Site Training</a></div>
            <div class="span-3 blackbl last" style="width: 87px">
                <a href="/ELearning/EcourseList.aspx" title="eLearning">eLearning</a></div>
            <div class="span-3 blackbl last">
                <a href="Training/Training.aspx" title="Training Grants">Training Grants</a></div>
            <div class="span-3 blackbl last" style="width: 130px">
                <a href="Training/Registration.aspx" title="Corporate Register">Corporate Register</a></div>
		
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">Your Cart</h3>
		<br class="clear">
		<h4>You added:</h4>
		<p>Please review the contents of your cart. You may continue adding courses or proceed to checkout.</p>
		<br class="clear">
		<div class="span-12">
		<input style="display:none;" onclick='location.href="https://online.qpsinc.com/youraccount/RegistrationSignIn.aspx"' type="button" id="btnCheckout" value="Proceed to Checkout >>" >
		<iframe width="400" height="400" scrolling="no" frameborder="0" src="http://online.qpsinc.com/youraccount/CartPage.aspx"></iframe>
		</div>
		<br class="clear">
	</div>
	<div class="span-4">
	    <img src="images/addtocart-big.gif" border=0 class="pull-1 top">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="images/check.gif">
	        <h3 class="mostwrap"><a href="/Training/Calendar.aspx
" class="nounder">Click here</a></h3> for our Upcoming Corporate Training Programs
	    </div>
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
    </div>
<script language="javascript" type="text/javascript">
	var Brsr=navigator.appName;
	if(Brsr=="Microsoft Internet Explorer")
	{
		document.getElementById("MiddleLeft").className="span-7 prepend-1 colborder";
	}
	else
	{
		document.getElementById("MiddleLeft").className="span-5 prepend-1 colborder";
	}
</script>

<!-- Footer -->
<script type="text/javascript">
    $(document).ready(
		function () {
		    $('#news').innerfade({
		        animationtype: 'slide',
		        speed: 750,
		        timeout: 5000,
		        type: 'random',
		        containerheight: '1em'
		    });

		}
	);
</script>

   <script language="javascript" type="text/javascript">
       function openPopup(txtURL) {
           window.open(txtURL, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width=600,height=500");
       }
        </script>
<br class="clear">
<div id="bottlist">	
   
    <div class="newsTicketWrpHM">
        <span>Hot News Headlines</span>
        <div id="ticker" style="padding-left: 120px" class="ticketTxt">
            <ul id="news">
                                <li><a href="#">Lean six sigma Green Belt certification is starting next month, reserve your seat now </a></li>
                <li><a href="#">PMP certification Prep courses starting every 6 weeks, guaranteed passing at first attempt </a></li>
                <li><a href="#">Agile Certification classes conducted every 6 weeks, guaranteed passing at first attempt </a> </li>
                <li><a href="#">Lean Expert classes are starting every 6 weeks, get our certification and make breakthrough improvements</a> </li>
                <li><a href="#">Black Belt classes are starting every month, reserve your seat now </a> </li>
                <li><a href="#">FDA related courses are available in Boston and NJ </a> </li>
                            </ul>
        </div>
    </div>
    
	
	<br class="clear"/>
	<div class="span-24 last" style="width: 980px; padding: 5px; background: #fafafa;">
	   
	    <div class="span-18 last" style="width: 470px;  padding: 5px; border: 1px solid silver;
            line-height: 2em">
            <h4 style="text-align: center; border-bottom: 1px solid #dadada; margin-bottom: 3px;
                padding: 5px; color: rgb(0,96,165);">
                <b>Training Programs</b>
            </h4>
              <div class="span-4 colborder" style="width: 180px; margin-left: 25px; font-weight: bold;">
                                <a href="../Training/calendar.aspx?id=2" title="Aerospace">Aerospace</a><br>
                                <a href="../Training/Calendar.aspx?id=8" title="Agile">Agile</a><br>
                                <a href="../Training/Calendar.aspx?id=6" title="APICS/SME Certifications">APICS/SME
                                    Certifications</a><br>
                              <a href="../Training/calendar.aspx?id=7" title="ASQ/IT Certifications">ASQ/IT Certifications</a><br>
                                <a href="../Training/calendar.aspx?id=3" title="FDA Related">FDA Related</a><br>
                                <a href="../Training/calendar.aspx?id=5" title="HR Related">HR Related</a><br>
                                <a href="../Training/Calendar.aspx?id=2" title="ISO Related">ISO Related</a><br>
                            </div>
                            <div class="span-4 colborder" style="border-right: 0px solid #EEE; font-weight: bold;">
                           
                                <a href="../Training/calendar.aspx?id=1" title="Lean Six Sigma">Lean Six Sigma</a><br>
                            
                                <a href="../Training/calendar.aspx?id=8" title="PMP/Agile/SCRUM">PMP/Agile/SCRUM</a><br>
                                <a href="../Training/calendar.aspx?id=5" title="Service Related">Service Related</a><br>
                                     <a href="../Training/calendar.aspx?id=4" title="Statistics">Statistics</a><br>
                                <a href="../Training/calendar.aspx?id=7" title="Software Related">Software Related</a><br>
                                <a href="../Training/Calendar.aspx?id=6" title="Supply Chain/Mfg">Supply Chain/Mfg</a><br>
                            </div>
			
	    </div>
	    
	    
	      <div class="span-18 last" style="width: 470px; margin-left: 15px; padding: 5px; border: 1px solid silver;
            line-height: 2em">
            <h4 style="text-align: center; border-bottom: 1px solid #dadada; margin-bottom: 3px;
                padding: 5px; color: rgb(0,96,165);">
               <b>Consulting </b>
            </h4>
             <div class="span-4 colborder" style="width: 180px; margin-left: 25px; font-weight: bold;">
                                <a href="Consulting/QualitySystem.aspx" title="AS 9100C">AS 9100C</a><br>
                                <a href="../Consulting/BusinessImprovement.aspx" title="Business Improvement">Business
                                    Improvement</a><br>
                                <a href="../Consulting/FDAMatters.aspx" title="FDA Related">FDA Related</a><br>
                                <a href="../Consulting/LeanThinking.aspx" title="Lean">Lean</a><br>
                                <a href="../Consulting/leanSixSigma.aspx" title="Lean Six Sigma">Lean Six Sigma</a><br>
                                <a href="../Consulting/QualitySystem.aspx" title="Management Systems">Management Systems</a><br />
                                <a href="../Consulting/FDAMatters.aspx" title="Medical Devices">Medical
                                    Devices</a><br>
                            </div>
                            <div class="span-4 colborder" style="border-right: 0px solid #EEE; font-weight: bold;">
                                 <a href="javascript:openPopup('../documents/ISO 9001 Quality Management System .htm')"" title="ISO 9001">ISO 9001</a><br />
                <a href="../Consulting/LeaderShipTeams.aspx" title="Leadership/Teams">Leadership/Teams</a><br />
                                <a href="../Consulting/NQA.aspx" title="Nuclear">Nuclear</a><br>
                            
                                <a href="../Consulting/ProjectManagement.aspx" title="Project Management">Project Management</a><br>
                                <a href="../Consulting/SixSigma.aspx" title="Six Sigma">Six Sigma </a>
                                <br>
                                <a href="../Consulting/StrategicPlanning.aspx" title="Strategic Planning">Strategic
                                    Planning</a><br>
                                <a href="../Consulting/sCManagement.aspx" title="Supply chain">Supply chain</a><br>
                            </div>
			
	    </div>
	</div>
	
</div>
<div class="span-25 last">
    <div style="width:972px;padding:8px;text-align:left;font-size:8pt;color:white;background:gray;border-bottom:15px solid #eaeaea">
        � <script type="text/javascript">              var year = new Date(); document.write(year.getFullYear());</script> Quality & Productivity Solutions, Inc. All Rights Reserved�
    </div>
</div>
</form>
    </div>
<!-- Other -->
		<div class="footnote span-25 last" style="background-color:white;z-index:100;">
		<div style="float:right;padding-right:15px;">
		<p class="grayref">
		<a href="mailto:webmaster@qpsinc.com">Feedback</a>&nbsp;&nbsp;
		<a href="/Company/Contact.aspx">Contact</a>&nbsp;&nbsp;
		<a href="/Company/PrivacyPolicy.aspx">Privacy Policy</a></p>
		</div>
		</div>
</div>
</div>
<br class="clear">
</body>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4685318-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</html>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
﻿<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" Codebehind="Welcome.aspx.cs"
    Inherits="QPS.Welcome" Title="Welcome to THE QPS INSTITUTE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  
    <div class="span-24 prepend-1 top highcont">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="vertical-align: top;">
                    <div class="span-4 top">
                        <div style="padding-top: 26px; text-align: left; padding-left: 0px;" class="pull-1">
                            <ul style="list-style: none;">
                                <li id="clon1">
                                    <div id="divforclon1" class="span-3 blackbl last" style="
                                        background-color: #948A54; width: 150%; border-bottom: solid 1px white; text-align: left;">
                                        <a href="javascript:onsite12('clon1');" title="Our Commitment to Education & Training"
                                            style="color: Silver;">Our Commitment to Education & Training</a></div>
                                </li>
                                <li id="clon2">
                                    <div id="divforclon2" class="span-3 blackbl last" style="
                                        background-color: #948A54; width: 150%; border-bottom: solid 1px white; text-align: left;">
                                        <a href="javascript:onsite12('clon2');" title="Admissions & Financial Services" style="color: Silver;">
                                            Admissions & Financial Services</a></div>
                                </li>
                                <li id="clon3">
                                    <div id="divforclon3" class="span-3 blackbl last" style="
                                        background-color: #948A54; width: 150%; border-bottom: solid 1px white; text-align: left;">
                                        <a href="javascript:onsite12('clon3');" title="Career Assistance" style="color: Silver;">
                                            Career Assistance</a></div>
                                </li>
                                <li id="clon4">
                                    <div id="divforclon4" class="span-3 blackbl last" style="
                                        background-color: #948A54; width: 150%; border-bottom: solid 1px white; text-align: left;">
                                        <a href="javascript:onsite12('clon4');" title="Tour" style="color: Silver;">Tour</a></div>
                                </li>
                                <li id="clon5">
                                    <div id="divforclon5" class="span-3 blackbl last" style="
                                        background-color: #948A54; width: 150%; border-bottom: solid 1px white; text-align: left;">
                                        <a href="javascript:onsite12('clon5');" title="PDUs" style="color: Silver;">PDUs</a></div>
                                </li>
                                <%--  <li id="clon6">
                                    <div id="divforclon6" class="span-3 blackbl last" style="font-family: Times New Roman; background-color:#948A54; width: 150%; border-bottom: solid 1px white;text-align:left;">
                                        <a href="javascript:onsite12('clon6');" title="Alumni" style="color: Silver;">Alumni</a></div>
                                </li>--%>
                            </ul>
                        </div>
                        <br class="clear">
                        <br class="clear">
                    </div>
                </td>
                <td style="vertical-align: top; text-align: left;">
                    <div>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding-left: 60px;">
                                    <div class="span-11 spacehedline" id="welcome1" style="width: 99%;">
                                        <table id="tblwelcome" cellpadding="0" cellspacing="0" border="0" style="padding-top: 8px;">
                                            <tr>
                                                <td colspan="2" style="color: #948A54; font-size: xx-large;">
                                                    Director’s Welcome
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style=" font-size: large; font-weight: bold;">
                                                    By Jay Patel
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify">
                                                    <p>
                                                        Greetings,</p>
                                                    <p>
                                                        On behalf of QPS, a most sincere thanks to all of you for allowing us to work with
                                                        you. QPS was formed in 1996 by President & CEO Jay P. Patel, to provide consulting
                                                        and training services in the areas of quality and productivity in addition to performance
                                                        excellence business solutions. Our goal is to continuously strive to deliver quality
                                                        consulting services as well as educational opportunities. QPS, who has been expanding
                                                        its training and holds state licensure to operate as a Private</p>
                                                    <p>
                                                        Occupational School though the Commonwealth of Massachusetts Department of Education,
                                                        opened the QPS Institute. Located in the vicinity of several major metropolitan
                                                        areas – Providence, Hartford and Boston - the QPS Institute provides an opportunity
                                                        to experience world-class training within driving distance. With new programs being
                                                        created, new out of state training locations added and eLearning available, the
                                                        QPS Institute continues to grow. Whether your interest is in Lean Six Sigma, Project
                                                        Management, ISO or Supply Chain---we trust you will find your experience at the
                                                        QPS Institute to be successful, beneficial and memorable!</p>
                                                    <p>
                                                        QPS is also and approved Training Provider for several states, providing public
                                                        training for those who are unemployed, and works with the One Stop Career Center
                                                        Systems under the Workforce Investment Office’s Department of Labor & Training.
                                                        By being an approved Training Provider, QPS can give those that are unemployed an
                                                        opportunity to obtain certifications for Lean, Six Sigma, Project Management, and
                                                        others through funding from state and federal entities. Those interested can easily
                                                        call the local career center and make an appointment with their career counselor
                                                        to start the application process for funding, and career centers are scattered generously
                                                        throughout each state. QPS is also the lead training provider for many companies
                                                        seeking training for their employees. Our extensive client list can be found on
                                                        our website. QPS also provides grant writing services and training needs assessments
                                                        for those companies seeking funding through the State of Massachusetts Workforce
                                                        Training Grant.</p>
                                                    <p>
                                                        At QPS, we lawayd add new programs, locations and events.  We have strong interest and passion in delivering the training to meet company needs. We always are interested to know what are your needs. Our passion for educational opportunities and customer satisfaction foster strong ties that we believe are essential to not just our success, but to yours as well !</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="span-11 spaceheadline" id="clonsite1" style="display: none; width: 99%">
                                        <table id="tblcommit" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="color: #948A54;  font-size: xx-large;">
                                                    Our Commitment to Education & Training
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify">
                                                    <p>
                                                        The QPS Institute is committed to providing the highest quality of education and
                                                        training for our clients. Our commitment to the goal of providing quality-driven
                                                        training and education to our clients is strongly anchored in our history of client
                                                        success and fully focused in our future mission to continue client success initiatives.
                                                    </p>
                                                    <p>
                                                        As global providers of business solutions, quality, productivity and performance
                                                        excellence, our expertise in the areas of Lean, Six Sigma, Management System, Supply
                                                        Chain, Project Management and Professional Development demands adherence to the
                                                        highest of standards which we continuously administer. Our aim to provide world-class
                                                        training is accomplished daily in our newly opened corporate training center, through
                                                        using state of the art technology, and with expert leaders mentoring every step
                                                        of the way.
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="span-11 spaceheadline" id="clonsite2" style="display: none; width: 99%">
                                        <table id="tblabout" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="color: #948A54;  font-size: xx-large;">
                                                    Admissions & Financial Services
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify">
                                                    <p style="font-style: italic; font-size: large;">
                                                        Corporate</p>
                                                    <p>
                                                        Registration for QPS programs, seminars, workshops and other related events is simple.
                                                        For corporate training, please use the <a href="http://www.qpsinc.com/documents/QPS Registration form.doc"
                                                            target="_blank">Corporate Registration Form</a> and submit with payment to <a href="mailto:info@qpsinc.com">
                                                                info@qpsinc.com.</a> Should your company have several training participants,
                                                        special discount rates may apply and onsite training at your company may be possible.
                                                        Custom proposals with quotations are provided upon request.
                                                    </p>
                                                    <p>
                                                        Funding is possible through the Workforce Training Grant
                                                        for companies seeking financial assistance on training their employees. QPS is very
                                                        familiar with this process and provides grant writing services with a training needs
                                                        assessment at no charge!
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify">
                                                    <p style="font-style: italic; font-size: large;">
                                                        Public
                                                    </p>
                                                    <p>
                                                        For public training, please contact us for an information packet that is custom to
                                                        your state, as well as with any questions on state/federal funding. Select programs
                                                        apply.
                                                    </p>
                                                    <p>
                                                        Scholarships may be available for potential out-of-pocket difference should the
                                                        funding source not cover the entire amount of the program.
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify;">
                                                    <p style="font-style: italic;">
                                                        Contact<br />
                                                        <span style="padding-left: 50px; font-style: normal;">Any questions regarding Admissions
                                                            and Financial Services can be sent to <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a>:
                                                        </span>
                                                    </p>
                                                    <p style="text-align: center;">
                                                    <br/>
                                                        Jay Patel
                                                        <br />
                                                        Founder<br />
                                                        The QPS Institute<br />
                                                        225 Cedar Hill Street<br />
                                                        Marlborough, MA 01752 USA<br />
                                                        toll-free: 1-877-987-3801<br />
                                                        <a href="http://www.qpsinc.com">www.qpsinc.com</a>
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="span-11 spaceheadline" id="clonsite3" style="display: none; width: 99%">
                                        <table id="Table1" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="color: #948A54;font-size: xx-large;">
                                                    Career Assistance
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" text-align: justify">
                                                    <p style="font-style: italic; font-size: large">
                                                        Jobs Information Session</p>
                                                    <p>
                                                        Are you looking for a job? Come to our Jobs Information Session where you can obtain
                                                        information on:</p>
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td style="text-align: center">
                                                                <span style="text-align: center; text-decoration: underline; font-size: large">Positions</span>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 33%; vertical-align: top;">
                                                                <ul>
                                                                    <li>Inspectors & Technicians</li><li>Supervisors</li><li>Customer Service Analysts</li><li>
                                                                        IT & Software Professionals</li><li>Director of Procurement</li><li>Senior Quality Engineer
                                                                            - Quality Control</li><li>Industrial Engineer</li><li>Operations Engineer</li><li>Lean
                                                                                Project Manager</li><li>Sr Design Quality Engineer - Medical Devices </li>
                                                                    <li>Quality Engineer</li><li>Quality Assurance Manager / Supervisor</li></ul>
                                                            </td>
                                                            <td style="width: 33%; vertical-align: top;">
                                                                <ul>
                                                                    <li>Quality Systems Manager</li><li>Packaging Quality Engineer</li><li>Healthcare Quality
                                                                        Engineer</li><li>Metrologist / Calibration Lab Supervisor</li><li>Director of Manufacturing
                                                                            Engineers</li><li>Design Assurance Engineer</li><li>Director of Total Quality Management</li><li>
                                                                                Sourcing Specialist – Research Outsourcing Procurement</li><li>Director, Strategic Sourcing</li><li>
                                                                                    Director, Supply Chain</li></ul>
                                                            </td>
                                                            <td style="width: 33%; vertical-align: top;">
                                                                <ul>
                                                                    <li>Buyer / Planner</li><li>Sr Quality Systems Engineer</li><li>Contract Manufacturing
                                                                        Commodity Manager</li><li>Product Engineer – Informatics</li><li>Engagement Leader</li><li>
                                                                            Principal Project Manager</li><li>Project Manager Senior</li><li>Lean / Six Sigma Engineer</li></ul>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td colspan="2" style="text-align: center;">
                                                                            <span style="text-align: center; text-decoration: underline; font-size: large;">Categories
                                                                                / Industries Served </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 50%; vertical-align: top;">
                                                                            <ul style="list-style-image: url(Images/checkMarkSmall.gif)">
                                                                                <li>Automotive & Parts Manufacturing</li>
                                                                                <li>Biotechnology & Pharmaceutical</li><li>Consumer Packaged Goods Manufacturing</li><li>
                                                                                    Electronics, Components & Semiconductor Manufacturing</li><li>Energy & Utilities</li></ul>
                                                                        </td>
                                                                        <td style="width: 50%; vertical-align: top;">
                                                                            <ul style="list-style-image: url(Images/checkMarkSmall.gif)">
                                                                                <li>Industrial & Manufacturing Engineering</li>
                                                                                <li>IT & Software Development</li>
                                                                                <li>Logistics & Transportation</li>
                                                                                <li>Medical Devices & Supplies</li><li>Product Development</li><li>Telecommunications
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <p style="font-style: italic;">
                                                        Resume Refinement</p>
                                                    <p>
                                                        Looking to improve your current resume? QPS offers a Resume Refinement Workshop
                                                        every month that is hands-on and will provide you with real-world advice on organizational
                                                        format and content!
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify;">
                                                    <p style="font-style: italic;">
                                                        Video Interview</p>
                                                    <p>
                                                        Want to know what you look like and sound during an interview process? Come and
                                                        attend our video interview session! This one-on-one session will allow you to be
                                                        interviewed via video so you can watch it, replay, critique and improve on your
                                                        interview skills!
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" text-align: justify;">
                                                    <p style="font-style: italic;">
                                                        Contact<br />
                                                        <span style="padding-left: 50px; font-style: normal;">Any questions regarding Career
                                                            Assistance can be sent to <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a>:
                                                        </span>
                                                    </p>
                                                    <p style="text-align: center;">
                                                       <br/>
                                                        Jay Patel
                                                        <br />
                                                        Founder<br />
                                                        The QPS Institute<br />
                                                        225 Cedar Hill Street<br />
                                                        Marlborough, MA 01752 USA<br />
                                                        toll-free: 1-877-987-3801<br />
                                                        <a href="http://www.qpsinc.com">www.qpsinc.com</a>
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="span-11 spaceheadline" id="clonsite4" style="display: none; width: 99%">
                                        <table id="tbltour" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-size: xx-large; color: #948A54;">
                                                    Tour
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" text-align: justify">
                                                    <p style="font-style: italic; font-size: large;">
                                                        Walk-in Visits</p>
                                                    <p>
                                                        Within normal business hours during the week, visit us any time and observe our
                                                        training!
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" text-align: justify">
                                                    <p style="font-style: italic; font-size: large;">
                                                        Appointment
                                                    </p>
                                                    <p>
                                                        Should you need to meet with our consultants or trainers, contact QPS for an appointment.
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify">
                                                    <p style="font-style: italic;  font-size: large;">
                                                        Directions
                                                    </p>
                                                    <p>
                                                        <span style="text-decoration: underline">Massachusetts</span> - Take 495N, drive 6 miles, take exit 24B (RT 20 west), first right into Felton street, first right at light on ELM street and then right on Lord, Road, park on back of the building Go to  #205 is Suite. 
                                                    </p>
                                                    <p>
                                                        <span style="text-decoration: underline">Rhode Island</span> -  Take 95 North, take exit 6B (495N). drive 6 miles, take exit 24B (RT 20 west), first right into Felton street, first right at light on ELM street and then right on Lord, Road, park on back of the building Go to  #205 is Suite.
                                                    </p>
                                                    <p>
                                                        <span style="text-decoration: underline">Connecticut</span> - Take 84 east toward Boston (portions toll) crossing into Massachusetts, Take 90 East toward Worcester / Boston (portions toll), take exit 11A (495N). Take 495N, drive 6 miles, take exit 24B (RT 20 west), first right into Felton street, first right at light on ELM street and then right on Lord, Road, park on back of the building Go to  #205 is Suite.
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: justify;">
                                                    <p style="font-style: italic; font-size: large;">
                                                        Contact</p>
                                                    <p>
                                                        <span style="padding-left: 50px; font-style: normal;">Any questions regarding Career
                                                            Assistance can be sent to <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a>:
                                                        </span>
                                                    </p>
                                                    <p style="text-align: center;">
                                                    <br />
                                                        Jay Patel
                                                        <br />
                                                        Founder<br />
                                                        The QPS Institute<br />
                                                        225 Cedar Hill Street<br />
                                                        Marlborough, MA 01752 USA<br />
                                                        toll-free: 1-877-987-3801<br />
                                                        <a href="http://www.qpsinc.com">www.qpsinc.com</a>
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="span-11 spaceheadline" id="clonsite5" style="display: none; width: 99%">
                                        <table id="tblpdus" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style=" font-size: xx-large; color: #948A54;">
                                                    PDUs
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" text-align: justify">
                                                    <p>
                                                        <span style="padding-left: 80px;"></span>QPS is a Registered Education Provider
                                                        (R.E.P) for the Project Management Institute (PMI). Being a R.E.P. allows QPS to
                                                        issue PDUs for its project management related training because its content is formally
                                                        recognized and consistent with PMI’s global standards. QPS is also listed in the
                                                        PMI Directory.
                                                    </p>
                                                    <p>
                                                        <span style="padding-left: 50px;"></span>According to the PMI website, “placement
                                                        in the directory confirms an organization's good standing” and that “it is approved
                                                        to issue Professional Development Units (PDUs) for training courses and events.”
                                                        R.E.P. status reflects that QPS met high criteria that PMI requires from all potential
                                                        R.E.P. applicants.
                                                    </p>
                                                    <p>
                                                        <span style="padding-left: 50px;"></span>QPS exemplifies organizational maturity
                                                        with it 15+ years experience of being in the training and consulting business, accompanied
                                                        by its extensive client satisfaction track record. Its curriculum design process
                                                        and content, in addition to is delivery methods, contain quality-driven characteristics
                                                        that result in its high success rates of QPS participants passing PMI examinations.
                                                    </p>
                                                    <p>
                                                        <span style="padding-left: 50px;"></span>QPS’s commitment to education and training
                                                        is highly considered by those seeking professional development when selecting a
                                                        training provider and QPS’s client satisfaction is the determining factor when they
                                                        choose to train with QPS.
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="span-11 spaceheadline" id="clonsite6" style="display: none; width: 99%;
                                        text-align: left;">
                                        <table id="tblalumni" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style=" width: 99%; font-size: xx-large; text-align: left;
                                                    color: #948A54;">
                                                    Alumni
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" text-align: justify">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>


</asp:Content>

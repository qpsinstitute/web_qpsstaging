<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="BusinessImprovement.aspx.cs"
    Inherits="QPS.Consulting.BusinessImprovement" %>

<asp:Content ID="ContentBusinessImprovement" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last" style="width: 102px;">
                <a href="LeanSixSigma.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last" style="width: 129px;">
                <a href="QualitySystem.aspx" title="Management System">Management System</a></div>
            <div class="span-4 blackbl last" style="width: 42px;">
                <a href="FDAMatters.aspx" title="FDA">FDA</a></div>
            <div class="span-4 blackbl last" style="width: 132px;">
                <a href="ProjectManagement.aspx" title="Project Management">Project Management</a></div>
            <div class="span-3 blackbl last" style="width: 88px;">
                <a href="sCManagement.aspx" title="Supply Chain">Supply Chain</a></div>
            <div class="span-3 blackbl last" style="width: 78px;">
                <a href="outsourcing.aspx" title="Outsourcing">Outsourcing</a></div>
            <div class="span-4 blackbk last" style="width: 139px">
               <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Business Improvement</h3>
            <p>
                <%--<span style="font-size: 125%">--%>
                    <img src="../images/BusinessImprovement.gif" class="top right">
                    To be successful in business, you need more than people, knowledge and skills; you
                    need a competitive edge. With QPS, we teach the teams to master the skills needed
                    for any size project implementation as well as the comprehensive training and consulting
                    support from project selection to completion. Our team is experienced in assessing
                    business plans and developing strategies. Using the needs assessment, we help our
                    clients select the appropriate project to improve their business, create an effective
                    implementation plan, and provide our incomparable coaching and support. In addition
                    to specific training in Six Sigma, Lean Techniques and Quality Systems, we also
                    provide services for: <%--</span>--%>
            </p>
            <h4 class="colblu rightarrowed">
                <a href="LeaderShipTeams.aspx" class="colblu">Leadership / Teams</a></h4>
            <ul>
                <li>
                Leading & Managing Change
                <li>
                Team Building
                <li>
                Developing Team Culture
                <li>
                Kaizen Deployment
                <li>
                Team Recognition
            </ul>
            <h4 class="colblu rightarrowed">
                <a href="StrategicPlanning.aspx" class="colblu">Strategic Planning</a></h4>
            <ul>
                <li>
                Strategy for Success
                <li>
                Process-Based Business
                <li>
                Business Assessment for Malcolm Baldridge Award Criteria
            </ul>
            <p>
                <span>Our satisfaction comes with your triumph. If you want unrivaled assistance in
                    creating a world-class business, you need QPS. Call us for an assessment and consultation.
                    We want to be your partner in success! For more information, please contact us <a
                        class="nounder" href="mailto:info@qpsinc.com">info@qpsinc.com</a> </span>
            </p>
            <p style="float: right; text-align: right;">
                <a class="nounder" href="#top" title="Top">
                    <img src="../images/top.gif" title="Top"></a></p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

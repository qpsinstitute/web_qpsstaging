<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="QualitySystem.aspx.cs"
    Inherits="QPS.Consulting.QualitySystem" %>

<asp:Content ID="ContentQualitySystem" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");
    </script>
    <script language="javascript" type="text/javascript">
        function openPopup(txtURL) {
            window.open(txtURL, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width=600,height=500");
        }
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="../Js/qfunctions.js"></script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last" style="width: 102px;">
                <a href="leanSixSigma.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
            <div class="span-4 blackbk last" style="width: 129px;">
                <a href="QualitySystem.aspx" title="Management System">Management System</a></div>
            <div class="span-4 blackbl last" style="width: 42px;">
                <a href="FDAMatters.aspx" title="FDA">FDA</a></div>
            <div class="span-4 blackbl last" style="width: 132px;">
                <a href="ProjectManagement.aspx" title="Project Management">Project Management</a></div>
            <div class="span-3 blackbl last" style="width: 88px;">
                <a href="sCManagement.aspx" title="Supply Chain">Supply Chain</a></div>
            <div class="span-3 blackbl last" style="width: 78px;">
                <a href="outsourcing.aspx" title="Outsourcing">Outsourcing</a></div>
            <div class="span-4 blackbl last" style="width: 139px">
               <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Management System</h3>
            <p>
               <%-- <span style="font-size: 125%">--%>
                    <img src="../images/management.gif" class="top right" style="margin-top: 0">
                    Management System is a requirement of every organization to conduct business worldwide
                    and to do effectively. The ISO set of standards have become an important mechanism
                    to allow companies to inform their customers and competitors that they have implemented
                    an externally-accepted quality system in addition to enabling companies to increase
                    conformity and efficiency in their operations. It poses the opportunity for continued
                    improvement, thereby creating a forward-looking mindset for the organization. ISO
                    is continually increasing its global presence and becoming a regulatory requirement
                    for many industries. 
            </p>
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                There are many different ISO standards designed to meet the needs of the various
                industries. The systems are designed to help businesses standardize their quality
                by providing a balanced set of core processes that maintain a foundation for management
                to operate, employees to perform, and products to be made properly in an efficient
                and effective manner. This in turn, allows for widespread visibility of problems
                and offers ability to manage those issues at their root, changing processes to continue
                improved outcomes. Though it can be designed to be simply a process management system,
                it has the ability to bring out the best in people, process and performance if used
                well.</p>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
                QPS has assisted hundreds of companies to develop Management system for many ISO
                standards. We have guided many companies across the following industries: automotive,
                medical device, pharmaceutical, health and human services, food and other manufacturing
                (e.g. consumer goods, aerospace, defense). Our customer testimonials offer you a
                sampling of the experience they have had with QPS on ISO. We guarantee your satisfaction,
                including first attempt registration for:</p>
            <ul>
                <li><a href="javascript:openPopup('../documents/Aerospace AS 9100.htm')" title="AS 9100 Aerospace QMS">
                    AS 9100 Aerospace QMS</a></li><li><a href="javascript:openPopup('../documents/ISO 9001 Quality Management System .htm')"
                        title="ISO 9001 QMS">ISO 9001 QMS</a></li><li><a href="javascript:openPopup('../documents/13485 Brochure.htm')"
                            title="ISO 13485 Medical Device QMS">ISO 13485 Medical Device QMS</a></li><li><a
                                href="javascript:openPopup('../documents/ISO 14001 EMS.htm')" title="ISO 14001 EMS">
                                ISO 14001 EMS</a></li><li><a href="javascript:openPopup('../documents/ISOTS16949 Automotive Quality.htm')"
                                    title="ISO/TS 16949 Automotive QMS">ISO/TS 16949 Automotive QMS</a></li><li><a href="javascript:openPopup('../documents/ISO 17025  Brochure.htm')"
                                        title="ISO 17025 Laboratories QMS">ISO 17025 Laboratories QMS</a></li><li><a href="javascript:openPopup('../documents/ISO 20001.htm')"
                                            title="ISO 20001 Service Management System">ISO 20001 Service Management System</a></li><li>
                                                <a href="javascript:openPopup('../documents/ISO 27001 Information Security system.htm')"
                                                    title="ISO 27001 Software Security">ISO 27001 Software Security</a></li><li><a href="javascript:openPopup('../documents/TL 9000.htm')"
                                                        title="TL 9000 Telecommunication QMS">TL 9000 Telecommunication QMS</a></li><li><a
                                                            href="javascript:openPopup('../documents/Baldrige Requirements.htm')" title="Malcolm Baldrige">
                                                            Malcolm Baldrige</a></li></ul>
            <h4 class="colblu">
                Other Systems:</h4>
            <ul>
                <li><a href="NQA.aspx" class="nounder bold" title="Nuclear Quality Assurance">Nuclear
                    Quality Assurance</a>
                <li><a href="Crisis.aspx" class="nounder bold" title="Crisis Intervention">Crisis Intervention
                </a>
                <li><a href="CEMark.aspx" class="nounder bold" title="CE Mark">CE Mark</a>
                <li><a href="HSCCP.aspx" class="nounder bold" title="HACCP">HACCP</a>
            </ul>
            <p>
                Using proven techniques, <span style="font-weight: bold;">QPS</span> provides the
                consulting and training that clients need to understand what is required to implement
                the quality system appropriate for your business, acquire registration, and realize
                its potential to improve the workplace. Our services give providers the knowledge
                they need using a comprehensive, empowering method that teaches teams in every function
                how an ISO system can make gains for everyone�including all the shareholders! <span
                    style="font-weight: bold;">QPS</span> offers many courses in ISO standards and
                systems. <span class="bold">For a complete list of all the courses we offer, please
                    <a href="../Training/Onsite.aspx" class="nounder" title="click here">click here</a>.</span>
                <br>
                <br>
                <span style="font-weight: bold;">QPS</span> will help you take your business where
                you want it to be�whether it be upgrading a current system, an initiative to streamline
                efficiency and/or costs, or the implementation of a new system, we can help. With
                <span style="font-weight: bold;">QPS</span>, our clients get �the works�. From gap
                analysis and planning to implementation and registration, we stand alongside you,
                training employees and auditors to be team players. We help manage culture change,
                build confidence in the system and support management as well as all the process
                owners to make your system a success.
                <br>
                <br>
                We start at the beginning, doing a gap analysis to determine how and if the current
                system in use can be used or developed, into what is required for registration.
                This planning helps determine Master plan for resources. <span style="font-weight: bold;">
                    QPS</span> helps to set up work plans and process maps for the preparation of
                documenting processes and procedures. We create a plan with you to meet your needs,
                keeping resources and staffing in mind. We provide all training to all levels of
                staff from management to the production line employee, providing course materials
                and certificates to all attendees. Internal Auditors are trained and certified to
                maintain the system to the standard using both classroom sessions and hands-on auditing.
                <br>
                <br>
                Our consultants will manage the entire project for you. Where there are multiple
                sites or business units involved, we maintain the schedule and integrity of the
                project to meet the target dates and keep costs at a minimum. We have spent over
                12 years creating systems to better manage businesses and prepare them to succeed.
                <a href="../Company/Contact.aspx" class="nounder bold" title="Contact us">Contact us</a>
                to find out how <span style="font-weight: bold;">QPS</span> can help you!
            </p>
            <p style="float: right; text-align: right;">
                <a class="nounder" href="#top" title="Top">
                    <img src="../images/top.gif" title="Top"></a></p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a href="/Training/Calendar.aspx" class="nounder" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

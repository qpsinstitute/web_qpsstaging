<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="LeanSixSigmaHealthcare.aspx.cs"
    Inherits="QPS.Consulting.LeanSixSigmaHealthcare" %>

<asp:Content ID="ContentSixSigmaHealthcare" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbk last">
                <a>Lean Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="LeanThinking.aspx">Lean Techniques</a></div>
            <div class="span-3 blackbl last">
                <a href="SixSigma.aspx">Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="QualitySystem.aspx">Management System</a></div>
            <div class="span-4 blackbl last">
               <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Lean Six Sigma for Healthcare</h3>
            <p>
                <%--<span style="font-size: 125%">--%>
                    <img src="../images/leansixsigma2.gif" class="top right" style="margin-top: 0">
                    Lean Six Sigma is a business strategy that combines the methodologies of Lean Enterprise and Six Sigma into one seamlessly-integrated strategy for excellence. The combination of these two powerful performance improvement programs is changing the face of modern healthcare delivery. From the patient to the provider, this program can reduce variability and waste, translating to more efficient processes, better team productivity, and higher patient satisfaction. Lean Six Sigma is helping businesses make breakthrough improvements in quality while lowering costs, offering vast potential to health care providers.  
            </p>
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                Healthcare businesses face liability and compliance risk because of daily errors, while labor efficiencies suffer as a result of repetitious inspection and rework. Errors that may seem minor on the surface can eventually cause frustration for both internal and external customers involved. Overall customer satisfaction goes hand in hand with profit. 
                <br>
                <br>
               Lean Six Sigma is a powerful tool designed to improve the patient experience while imparting financial results. It provides a persistent organizational focus on the elimination of non-value-added resource consumption�those things for which the customer would choose not to pay. Any errors or their resulting consequences are by definition not value added. Though those in healthcare understand they are in business for the patient, when we look beyond the surface, many activities are done as a result of regular business constraints set in place by earlier people and processes. Some examples may be 1) too many people involved in a process, or 2) reprocessing for lack of clear communication/responsibility. Why are these things occurring? Lean Six Sigma teaches to research the data rather than resort to finger-pointing or relying on intuition. Improvement teams will trust a statistical method to identify and address the real source of the problem, driving continual focus to eliminate errors and improve the overall process.
                <br>
                <br>
               Today's health care organizations have to reduce their costs, improve the quality of care and meet stringent guidelines to remain competitive and stay in business. Lean Six Sigma provides the tools to evaluate performance, identify organizational strengths and align patient care strategies with the business, using best-in-class solutions for healthcare quality. Sound awesome? It is! 
            </p>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
               QPS provides the consulting and training that clients need to understand their potential to improve, develop a vision of excellence and implement a dynamic plan to realize it. Our services give providers the tools they need to reduce costs while improving their quality using a comprehensive, empowering method that gives teams in every function the tools and support they need to attain quality breakthroughs, enhance client service and solve costly problems. 
                <br>
                <br>
                QPS tackles the issues of non-value-added resource consumption and errors by providing your associates with the knowledge and understanding to address both. The  <b>Lean Six Sigma Program</b> integrates process improvement tools and statistically - based methods to identify inefficiencies, whether they are driven by variation, process flow or error - related issues. It provides practical training and mentoring support for all levels of the organization through the following courses: 
            </p>
            <ul>
                <li>
                Lean Six Sigma Management Overview
                <li>
                Lean Six Sigma for Champions
                <li>
                Lean Six Sigma Yellow Belt 
                <li>
                Lean Six Sigma Green Belt Certification 
                <li>
                Lean Six Sigma Black Belt Certification 
                <li>
                Lean Six Sigma Master Black Belt Certification
                <li>
                Design for Six Sigma Overview
                <li>
                Design for Six Sigma Certification 
                <li>
                Strategic Planning 
                <li>
                Coaching and Mentoring 
            </ul>
            <h4 class="colblu">
                What Lean Six Sigma can do for you?</h4>
            <p>
            Health care organizations are turning to these Lean Six Sigma programs to improve patient care, counteract rising costs, enhance employee satisfaction and retention, and boost financial performance. Some of the organization-changing results that our <b>Lean Six Sigma Program</b> will deliver are: 
  
            </p>
            <ul>
                <li>
                Improved patient care and satisfaction
                <li>
                Improved physician satisfaction and retention
                <li>
                Cost reduction and improved financials
                <li>
                Stronger, more efficient organizations 
            </ul>
            <p>

            The <strong>Lean Six Sigma Program</strong> and our support guides facilitators, team leaders and team members from their first meeting through the completion of a successful improvement project. Teams gain the knowledge, practical skills and tools they need to remedy the urgent quality problems while enhancing client service and support significantly, enhancing the skill set in the organization, while driving results via the project work. 
                
                <br>
                <br>
                Our clients are achieving their goals; from strategy to plan, training to implementation, we are committed to deliver. <a href="../Company/Contact.aspx" title="Contact QPS"
                    class="nounder bold">Contact us</a> today and find out why so many businesses trust QPS for their success. 

               
            </p>
            <strong>For more details, please download the <a target="_blank" href="../documents/LSS4HealthCare.pdf"
                class="nounder bold">Lean Six Sigma for Healthcare</a>.</strong>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <div style="padding-top: 20px" class="hrefnounderclass">
                <h4 class="bold hundredline">
                    Lean Six Sigma for:</h4>
                <a class="grayclass">Healthcare</a><br>
                <a href="LeanSixSigmaManufacturing.aspx" class="nounder bold">Manufacturing</a><br>
                <a href="LeanSixSigmaService.aspx" class="nounder bold">Service</a><br>
            </div>
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Outsourcing.aspx.cs"
    Inherits="QPS.Consulting.Outsourcing" %>

<asp:Content ID="ContentOutsourcing" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last" style="width: 102px;">
                <a href="LeanSixSigma.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last" style="width: 129px;">
                <a href="QualitySystem.aspx" title="Management System">Management System</a></div>
            <div class="span-4 blackbl last" style="width: 42px;">
                <a href="FDAMatters.aspx" title="FDA">FDA</a></div>
            <div class="span-4 blackbl last" style="width: 132px;">
                <a href="ProjectManagement.aspx" title="Project Management">Project Management</a></div>
            <div class="span-3 blackbl last" style="width: 88px;">
                <a href="sCManagement.aspx" title="Supply Chain">Supply Chain</a></div>
            <div class="span-3 blackbk last" style="width: 78px;">
                <a href="outsourcing.aspx" title="Outsourcing">Outsourcing</a></div>
            <div class="span-4 blackbl last" style="width: 139px">
                <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Outsourcing</h3>
            <p>
                <img src="../Images/Outsourcing.jpg" width="252" height="149" class="left top" style="margin-top: 0">
               <%-- <span style="font-size: 125%">--%>To outsource, is to subcontract a process, such as software
                    development, product design or manufacturing, to a third-party company. It refers
                    to a company that contracts with another company to provide services currently performed
                    by in-house employees. These separate companies specialize in each process or service,
                    which allows the established business to focus on other issues while having the
                    details taken care of by outside experts.
                    <br>
                    <br>
                    The specialized company that handles the outsourced work is often streamlined, with
                    world-class capabilities and access to new technology that many companies cannot
                    afford on their own, or may not want to manage. It also allows some insight and
                    analysis into building foundations in other countries for expansion. <%--</span>--%>
            </p>
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                There are many reasons that companies outsource various jobs, but the most prominent
                advantage is the fact that it is an effective, cost-saving strategy when used properly.
                Many of the companies that provide outsourcing services are able to do the work
                for considerably less money--with different pay and benefit scales--and stable processes
                to complete the work more efficiently. An example of outsourcing might be an insurance
                company setting up a call center in India to handle automated quotes in order to
                save on the cost of maintaining customer service staff on-site.
            </p>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
                While outsourcing may prove highly beneficial for many companies, it does have its
                drawbacks. It is crucial that each company accurately evaluates their needs to determine
                if outsourcing is a practical option. It is all about adding value and maintaining
                control.
                <ul>
                    <li>
                    What is it that sets your business apart from the rest?
                    <li>
                    Are there new, high tech ways to automate or upgrade that you can�t afford?
                    <li>
                    What are the greatest costs your business faces in the global market?
                    <li>
                    Are you prepared for dealing with other business climates and cultures?
                </ul>
                What you need is a clear understanding of what can be outsourced without losing
                control of what you consider most important�your customers and your business. Our
                experience in process and outsourcing is your confidence.
            </p>
            <p>
              
            </p>
            <p>
                <h4 class="colblu">
                    What Outsourcing can do for you?</h4>
                The decision to outsource is often made in the interest of lowering fixed costs,
                making better use of time and energy costs, or to make more efficient use of (information)
                technology, land, labor, capital, and resources. You want a partner completely synchronized
                with your overall business objectives and process excellence that exceeds what your
                business can do on its own. You want a competitive edge.
                <br>
                <br>
                QPS works with its clients to create and deliver process innovations that transform
                businesses and bring high performance at a lower cost. Whether you need software
                development,business process outsourcing for vital functions, industry-specific
                solutions, flexible and advanced IT or bundled services, QPS can help you achieve
                higher performance to not only save money, but MAKE more, too! Let us help you get
                started on building a more effective and profitable business.
                <br>
                <br>
                QPS has many seasoned strategic consultants to choose from, each with industry-specific
                knowledge who can help you determine external market dynamics and identify the key
                strategies to help you succeed in your industry. Our Senior Consultant will help
                you determine where outsourcing can benefit your business and help you find the
                companies that best fit your objectives. We have offices in India, and partners
                in China and Europe to give you the edge you need in the locations that make the
                most sense. Business excellence is what we strive to accomplish with a team focused
                on successful implementation at minimal cost to you. <a href="../Company/Contact.aspx"
                    title="Contact QPS" class="nounder bold">Contact us</a>
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="LeanSixSigmaManufacturing.aspx.cs"
    Inherits="QPS.Consulting.LeanSixSigmaManufacturing" %>

<asp:Content ID="ContentSixSigmaManufacturing" ContentPlaceHolderID="cphContent"
    runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbk last">
                <a>Lean Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="LeanThinking.aspx">Lean Techniques</a></div>
            <div class="span-3 blackbl last">
                <a href="SixSigma.aspx">Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="QualitySystem.aspx">Management System</a></div>
            <div class="span-4 blackbl last">
               <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Lean Six Sigma for Manufacturing</h3>
            <p>
                <%--<span style="font-size: 125%">--%>
                    <img src="../images/leansixsigma2.gif" class="top right" style="margin-top: 0">
                    Lean Six Sigma is the combination of two methodologies�Lean Enterprise and Six Sigma�it is today�s fastest and most effective approach to business improvement. By combining the lead-time reduction and rapid waste elimination of Lean with Six Sigma�s spotlight on quality and yield development, Lean Six Sigma gives process manufacturers a combination punch for faster results than Six Sigma alone.
            </p><br />
         <%--   <br class="clear">--%>
            <p> <%--<span style="font-size: 125%">--%>
               Lean enables Six Sigma to optimize process flow while eliminating defects using the advanced statistical tools of Six Sigma to achieve true 'lean' processes. Results come faster because baseline performance levels are determined and statistical tools are focused where the most impact will be achieved. Initially, most companies use basic lean techniques to standardize work and eliminate waste. After the more obvious wastes are eliminated, more advanced methods are needed to uncover root cause of the deviation. For example, inventory reduction requires reducing batch sizes and linking operations using Lean tools, and demands minimizing process variation by utilizing Six Sigma tools. Together, these methodologies maximize the potential of an organization, creating an integrated system and roadmap for driving continual improvement and the ability to achieve the best competitive position.
       
            </p>
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                Today, manufacturing organizations have to become faster and more responsive while operating at world-class cost and capability in order to remain in business. 'Minor' daily errors, repetitious inspection and rework cause reduced labor efficiencies, frustration and cost in every department. Beyond liability, risk, and eroding the bottom line, the final outcome is reduced customer satisfaction�the driver for success in every business. To obtain maximum speed, you need quality. To improve quality, you need to cut waste and become more efficient. It becomes a game of determining how to create value and increase output while reducing cost! 
                <br>
                <br>
               In any process, Lean Six Sigma creates a map of the process identifying value-added and non-value add costs, and captures the 'Voice of the Customer' to define their critical issues. Projects within that process are then prioritized based on the delay time they cause. This prioritization pinpoints activities with high defect rates (using Six Sigma tools) or long setups, downtime (using Lean tools). For manufacturing, an added benefit results from a reduction in working capital and capital expenditure. 
                <br>
                <br>
                Lean Six Sigma is being used to drive game-changing competitive advantage for process industries across the globe. It provides a persistent organizational focus on the elimination of cost and waste. It requires the hard work of focused action, based on learning and data, to achieve bottom line savings. 
            </p>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
               Whether you are new to Lean Six Sigma, or are in the midst of using lean tools for improving a current business system, QPS can help. Success in Lean Six Sigma, as with any other major change effort, relies heavily on detailed planning. We will assess your current situation and work with you to design your roadmap to breakthrough improvement. 
                <br>
                <br>
               QPS provides the consulting and training that clients need to understand their potential to improve, develop a vision of excellence and implement a dynamic plan to realize it. Our services give providers the tools and techniques they need to reduce costs while improving their quality using a comprehensive, empowering method that gives teams in every function the tools and support they need to attain quality breakthroughs, enhance client service and solve costly problems. Our integrated program is designed to increase customer satisfaction, shorten process cycle times, improve overall quality, and create shareholder value in all areas of your organization while changing the culture for team-driven success. 
                <br>
                <br>
                QPS tackles the issues of non-value added resource consumption and errors by providing your associates with the knowledge and understanding to address both. The <strong>Lean Six Sigma Program</strong> integrates process improvement tools and statistically based methods to identify inefficiencies, whether they are driven by variation, process flow or error related issues. It provides practical training and mentoring support for all levels of the organization, through the following courses: 

            </p>
            <ul>
            <li>
                Lean Six Sigma Management Overview 
                <li>
                Lean Six Sigma for Champions
                <li>
                Lean Six Sigma Yellow Belt
                <li>
                Lean Six Sigma Green Belt Certification
                <li>
                Lean Six Sigma Black Belt Certification
                <li>
                Lean Six Sigma Master Black Belt Certification
                <li>
                Design for Six Sigma Overview
                <li>
                Design for Six Sigma Certification
                <li>
                Strategic Planning
                <li>
                Coaching and Mentoring
            </ul>
            <h4 class="colblu">
                What Lean Six Sigma can do for you?</h4>
            <p>
            Manufacturing organizations are turning to Lean Six Sigma to improve production efficiencies, reduce costs, enhance customer and employee satisfaction and retention, and boost financial performance. Powerful competitive advantages are right on your plant floor. Using time-based principles and hands-on experience, we help transform manufacturers into engines for real sustained growth, without downsizing. Some of the organization-changing results that our <strong>Lean Six Sigma Program</strong> will deliver are: 

            </p>
            <ul>
                <li>
                Improved cross-functional teamwork and communications
                <li>
                Higher quality, more efficient processes and yields
                <li>
                Increased flexibility to match production to actual demand 
                <li>
                More efficient supplier and distribution networks 
                <li>
                Improved cash flow with less inventory and streamlined distribution
                <li>
                Lower costs and better financials
                <li>
                Responsiveness to business opportunity and change; create advantages 
                <li>
                Improved stakeholder value (employees, partners, customers, etc.)
                <li>
                Stronger, more effective organizations 
            </ul>
            <p>
            The QPS <strong>Lean Six Sigma Program</strong> and our support are designed to create and build your success. We guide facilitators, team leaders and team members from their first meeting through the completion of a successful improvement project, with support unrivaled by other programs. Teams gain the knowledge, practical skills and tools they need to remedy the urgent quality problems while enhancing the skill set in the organization; driving results via the project work. We provide full service Lean Six Sigma consulting; from custom program design, training, and implementation, with on-going support and maintenance. 

                
                <br>
                <br>
                Our clients are achieving their goals; from strategy to plan, training to implementation, we are committed to deliver. <a href="../Company/Contact.aspx" title="Contact QPS"
                    class="nounder bold">Contact us</a> today and find out why so many businesses trust QPS for their success. 

               
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <div style="padding-top: 20px" class="hrefnounderclass">
                <h4 class="bold hundredline">
                    Lean Six Sigma for:</h4>
                <a href="LeanSixSigmaHealthcare.aspx" class="nounder bold">Healthcare</a><br>
                <a class="grayclass">Manufacturing</a><br>
                <a href="LeanSixSigmaService.aspx" class="nounder bold">Service</a><br>
            </div>
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

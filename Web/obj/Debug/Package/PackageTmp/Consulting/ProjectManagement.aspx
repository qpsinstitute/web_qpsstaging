<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="ProjectManagement.aspx.cs"
    Inherits="QPS.Consulting.ProjectManagement" %>

<asp:Content ID="ContentProjectManagement" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last" style="width: 102px;">
                <a href="leanSixSigma.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last" style="width: 129px;">
                <a href="QualitySystem.aspx" title="Management System">Management System</a></div>
            <div class="span-4 blackbl last" style="width: 42px;">
                <a href="FDAMatters.aspx" title="FDA">FDA</a></div>
            <div class="span-4 blackbk last" style="width: 132px;">
                <a href="ProjectManagement.aspx" title="Project Management">Project Management</a></div>
            <div class="span-3 blackbl last" style="width: 88px;">
                <a href="sCManagement.aspx" title="Supply Chain">Supply Chain</a></div>
            <div class="span-3 blackbl last" style="width: 78px;">
                <a href="outsourcing.aspx" title="Outsourcing">Outsourcing</a></div>
            <div class="span-4 blackbl last" style="width: 139px">
               <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Project Management</h3>
            <p>
                <img src="../Images/Project Management.jpg" class="left top" width="213" height="213"
                    style="margin-top: 0">
                <img style="float: right; margin-top: 0" src="../Images/REPsmall.jpg" />
                <%--<span style="font-size: 125%">--%>Project Management is the use of knowledge and skill applied
                    to initiate, plan, execute, monitor and control a project. It encompasses all necessary
                    resources for the successful completion of specific project goals and objectives.
                    It is a finite endeavor undertaken to create value in a unique product or service�contrary
                    to a process that is permanent or semi-permanent. Management of a project is quite
                    different than a process and requires specific technical skills and philosophy.
                    <br>
                    <br>
                    A project management professional (PMP�) understands issues such as risk management,
                    stakeholder analysis, resource management, team performance analysis, and project
                    performance analysis. They know how (and take pride) to deliver cost effective projects
                    on time. Strategic planning ability, team facilitation, resourceful problem solving,
                    and an understanding of concepts such as statistics and analysis, are just some
                    of the skills required by a project management professional. An ability to quickly
                    understand various project software is crucial for a PMP� as a project could consist
                    of designing new products, improving logistics management, or restructuring company
                    operations. <%--</span>--%>
            </p>
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                A project is a carefully defined set of activities that use resources (money, people,
                materials, energy, space, provisions, communication, motivation, etc.) to achieve
                the project goals and objectives. The primary challenge in project management is
                to achieve each of these project goals and objectives while adhering to classic
                project constraints�scope, quality, time and budget. The secondary challenge is
                to optimize the inputs needed to meet the final objectives.
                <br>
                <br>
                In today's economy, all business lines and sectors have incorporated Project Management
                to aid them in accomplishing their pre-defined objectives within the defined time
                limits. It has also brought profitability by providing:
            </p>
            <ul>
                <li>
                Clear, communicable plans for achieving specific project goals and in turn, business
                goals.
                <li>
                Development that is phased in systematically with ongoing, measurable progress.
                <li>
                A team approach at managing tasks and issues based on skills required for each task.
                <li>
                A built-in mechanism for ongoing communication, review and analysis of a project
                � assessing requirements and matching appropriate resources as needed.
                <li>
                A way to incorporate Quality Assurance within the project life cycle thus producing
                true, meaningful output on time.
            </ul>
            <h4 class="colblu">
                How QPS Can Help?</h4>
            <p>
                Project Management is an in-demand skill set and one of the fastest growing professional
                disciplines in North America. It breaks down the chaos of an overwhelming workload
                into manageable elements - scope, cost, quality, time, human resources, communication,
                procurement, integration, and risk. How? By using a phased approach to executing
                a project per a defined life cycle using educated teams and quality assurance tools.
                <br>
                <br>
                QPS brings certified Project Management professionals with decades experience in
                successful project management for manufacturing and service industries. We pride
                ourselves on getting the job done right; determining the project and objectives
                for the goal, incorporating the right resources and tools, creating the proper communication
                channels, and educating the team�on time and within budget. It comes down to making
                a project successful at the lowest cost to you.
            </p>
            <ul>
                <li>
                What are your business objectives?
                <li>
                How can you best allocate resources to avoid waste?
                <li>
                How can make your goals and remain competitive at the same time?
            </ul>
            <h4 style="margin-bottom: 25px" class="colblu">
                What Project Management Can Do For You?</h4>
            <p>
                Managing a project is extremely time consuming for those keeping one eye on today
                and another on tomorrow. The professionals we dedicate, the tools and skills we
                teach, and the project-specific focus we provide are value-added to your business.
                QPS starts at the business plan and determines your needs based on your goals. We
                work along side your business to deliver project success at a lower cost than if
                you did it alone, and we will find ways to help grow your business.
                <br>
                <br>
                QPS has many seasoned strategic consultants to choose from, each with industry-specific
                knowledge who can help you plan your project and complete it on your schedule. Our
                Project Management Professional (PMP�) will help you determine which project will
                benefit your business and how to get started. Business excellence is what we strive
                to accomplish with a team focused on successful project completion at minimal cost
                to you.
                <br>
                <br>
                QPS works with its clients to create to deliver project performance at its best!
                We offer PMP� certification, on-site and public training opportunities as well as
                coaching. Let us help you get started on building a more effective and profitable
                business today. <a href="../Company/Contact.aspx" title="Contact QPS" class="nounder bold">
                    Contact us</a>
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

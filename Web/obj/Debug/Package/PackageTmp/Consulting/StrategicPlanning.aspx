<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="StrategicPlanning.aspx.cs"
    Inherits="QPS.Consulting.StrategicPlanning" %>

<asp:Content ID="ContentStrategicPlanning" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last">
                <a href="LeanSixSigma.aspx">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="LeanThinking.aspx">Lean Techniques</a></div>
            <div class="span-3 blackbl last">
                <a href="SixSigma.aspx">Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="QualitySystem.aspx">Management System</a></div>
            <div class="span-4 blackbk last">
                <a>Business Improvement</a></div>
            <br class="clear">
            <br class="clear">
            <div class="span-6" style="float: right">
                <a href="BusinessImprovement.aspx" class="nounder right">
                    <img src="../images/right.gif" class="right">
                    Return to Business Improvement</a></div>
            <br class="clear">
            <h3 class="colbright" id="training">
                Strategic Planning</h3>
            <p>
                <img src="../Images/Strategic Planning.jpg" class="left top" style="margin-top: 0">
                <%--<span style="font-size: 125%">--%>Every organization is different, but the principles of
                    winning are the same. Each business has its own definition of strategy; a plan to
                    grow based on how, where, and when they compete. It is what sets your business apart
                    from the competition based on the barriers and competencies you have and the market
                    forces you face. The strategic plan of an organization has two stages in its life
                    cycle�a dormant phase and a development phase. Due to time constraints, leadership
                    personalities, and process/communication issues, many businesses spend the majority
                    of their time in the dormant phase. <%--</span>--%>
            </p>
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                When a business has a product or service in demand that is worth developing, then
                there is justification for investment in that company's future. This creates a need
                for a process that causes an ongoing appraisal of a business� direction and generates
                a plan for sustainable and improved profitability growing forward. Use of this process
                avoids dormancy of a plan, and creates ongoing excitement due to the continued focus
                and vision of the organization
                <br>
                <br>
                Defining strategy allows businesses to understand how to serve the right customers
                in ways that profitably differentiate themselves from the competition. It defines
                the core competencies and the market forces faced externally, while examining the
                internal barriers to growth that must be overcome. This is hard work and requires
                due diligence, patience and determination to develop the future vision and its structure
                for an entire organization. Business owners often find it difficult to be fully
                objective; thus causing strategic planning from the inside-out. More and more businesses
                are looking to employ the part time services of external professional Strategy Consultants
                that have the ability to work from the outside-in.
            </p>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
                What you need is a credible plan for growth; a strategy that is clear, sound, and
                compelling, developed by YOUR team. It becomes the roadmap for growth and fundamental
                change�one that you own, believe in and can get excited about!
            </p>
            <ul>
                <li>
                What sets your business apart from competitors?
                <li>
                What are your core competencies?
                <li>
                What are the external market forces you face?
                <li>
                What are the internal barriers to growth within your organization?
            </ul>
            <p style="margin-bottom: 25px" class="colblu">
                Answering each of these questions require carefully-thought out fact-based analyses
            that require research, knowledge and an understanding of strategy. Our Strategic
            Development consulting services includes courses in:<p>
                <ul>
                    <li>
                    Business Strategy Basics
                    <li>
                    Core Analysis; SWOT & MOST Analysis Techniques
                    <li>
                    Strategy Team Training
                    <li>
                    Market Definition & Competitive Analysis
                    <li>
                    Determining Growth Opportunities; Benchmarking & Best Practices
                    <li>
                    Strategic Planning Process
                    <li>
                    Strategy Development & Implementation
                </ul>
                <h4 class="colblu">
                    What Strategy Planning can do for you?</h4>
                <p style="margin-bottom: 5px">
                    Getting started on developing a business strategy can be overwhelming. One of the
                    key tools used in Strategic Planning and Implementation is the M.O.S.T. Analysis:
                    <ul>
                        <li>
                        Mission: the direction you intend the business to go
                        <li>
                        Objectives: the key goals needed to achieve your mission
                        <li>
                        Strategies: the options open to you to achieve your goals
                        <li>
                        Tactics: putting the strategies into action
                    </ul>
                    Key to the process is its ability to work top-down AND bottom-up; where the mission
                    drives the objectives, which creates the strategic options, which then force tactical
                    actions that make the strategies work, helping to achieve the objectives set and
                    take the business toward its mission
                    <br>
                    <br>
                    QPS has many seasoned strategic consultants to choose from, each with industry-specific
                    knowledge who can help you determine external market dynamics and identify the key
                    strategies to help you succeed in your industry. Our Senior Consultant will train
                    your top people how to apply the methodologies of strategic planning and to develop
                    their own plan for growth using proven, world-class best practices. This will promote
                    buy-in, true understanding, and successful implementation at minimal cost to you.
                    <br>
                    <br>
                    QPS will become your coach; providing the processes needed to ensure your growth.
                    Understand the fundamentals of strategy like few executives ever do and use them
                    to take your business to new heights�tackling the most critical issues facing your
                    organization.
                    <br>
                    <a href="../Company/Contact.aspx" title="Contact QPS" class="nounder bold">Contact us</a>
                </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

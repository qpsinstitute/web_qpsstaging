<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="SixSigma.aspx.cs"
    Inherits="QPS.Consulting.SixSigma" %>

<asp:Content ID="ContentSixSigma" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last">
                <a href="LeanSixSigma.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="LeanThinking.aspx" title="Lean Techniques">Lean Techniques</a></div>
            <div class="span-3 blackbk last">
                <a title="Six Sigma">Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="QualitySystem.aspx" title="Management System">Management System</a></div>
            <div class="span-4 blackbl last">
               <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>s
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Six Sigma</h3>
            <p>
               <%-- <span style="font-size: 125%">--%>
                    <img src="../images/Six_sigma3.gif">
                    Six Sigma is a comprehensive, focused and highly effective implementation of proven
                    quality principles and techniques that can help any business to save money, increase
                    efficiencies and make more money. Incorporating elements from the work of many quality
                    experts, the <strong>QPS Six Sigma Program</strong> focuses on improving quality--defect
                    prevention, cycle time reduction, and cost savings--by helping organizations produce
                    products and services with better quality. faster and cheaper. Tying business improvement
                    to the bottom line by reducing and controlling quality is the essence of Six Sigma.
              <%--  </span>--%>
                <br>
                <br>
                <%-- <span style="font-size: 125%">--%>
                <img src="../images/sixsigma2.gif" class="top right">
                <strong>Today�s businesses are experiencing breakthrough improvements by 25% each year
                    as a result of implementing Six Sigma!</strong> By reducing process variation,
                Six Sigma allows the organization to focus on improving process capability and as
                sigma levels increase, the cost of poor quality falls�and profitability grows. This
                is why Six Sigma is linked to the delivery of consistent world-class quality. Implementing
                the <strong>QPS Six Sigma Program</strong> quickly pays for itself. <strong>QPS</strong>
                has designed a unique curriculum to provide the tools and knowledge needed to prioritize,
                optimize and maximize Return on Investment at a cost most businesses can afford.
                Our program can help any size or business type to deploy and manage a successful
                Six Sigma improvement program.<%--</span>--%>
            </p>
            <h4 class="colblu">
                Implementation Benefits:</h4>
            <ul>
                <li><strong>Improve customer satisfaction; creating more value to your customer and
                    you</strong>
                <li><strong>Increase product quality & decrease variation </strong>
                <li><strong>Reduce Waste! </strong>
            </ul>
            <h4 class="colblu">
                Certification Training Programs:</h4>
            <p>
                The following Certification Training Programs are available. Candidates will participate
                in class discussion, exercises & workshops to implement their company�s process
                improvements.</p>
            <ul>
                <li><strong>Six Sigma Green Belt </strong>
                <li><strong>Six Sigma Black Belt </strong>
                <li><strong>Lean Six Sigma Black Belt </strong>
                <li><strong>Six Sigma Master Black Belt </strong>
                <li><strong>Design for Six Sigma </strong>
            </ul>
            <h5 class="bold">
                Contact us today to design a program that fits your needs!</h5>
            <p>
                Certified Six Sigma professionals with extensive experience in tools, improvement,
                and methodologies teach all <b>QPS</b> courses. Our instructors are qualified on
                the basis of their experience and expertise in the field and are available for project
                assistance. <b>QPS</b> emphasizes teamwork, behavior modification, change management
                and mentored deployment together with the skills to enable breakthrough improvements.
                <b>Research has shown that firms that successfully implement Six Sigma perform better
                    <span style="text-decoration: underline">in virtually every business category</span></b>,
                including employment growth, return on sales, return on investment, and increase
                in share prices. Learn to turn real world problems into real time solutions with
                this systematic methodology focused on results. You have learned about quality tools
                before, but it is people and processes that must change to achieve world-class improvement.
                <b>Are you ready to join the Six Sigma revolutions?</b>
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="LeanSixSigmaService.aspx.cs"
    Inherits="QPS.Consulting.LeanSixSigmaService" %>

<asp:Content ID="ContentSixSigmaService" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbk last">
                <a>Lean Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="LeanThinking.aspx">Lean Techniques</a></div>
            <div class="span-3 blackbl last">
                <a href="SixSigma.aspx">Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="QualitySystem.aspx">Management System</a></div>
            <div class="span-4 blackbl last">
               <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Lean Six Sigma for Service</h3>
            <p>
               <%-- <span style="font-size: 125%">--%>
                    <img src="../images/leansixsigma2.gif" class="top right" style="margin-top: 0">
                   Lean Six Sigma is a business strategy that combines the strength of today's two most powerful improvement initiatives - Lean Enterprise and Six Sigma - into one integrated strategy for excellence. Although both Six Sigma and Lean Enterprise have their roots in manufacturing, they work just as effectively in service industries. Service businesses face the same relentless demands that manufacturers do � to reduce waste and increase speed, all while improving quality, delivery and customer satisfaction. 
               
            </p><br />
           <%-- <br class="clear">--%>
            <p>
          <%--  <span style="font-size: 125%">--%>
                Six Sigma is a disciplined, data-driven methodology for eliminating defects and reducing variation in any process. Lean focuses on improving process speed and the elimination of non-value added steps. Lean Six Sigma combines the speed improvements that originate from using Lean principles with the quality improvements derived from Six Sigma tools, thus creating higher quality and faster services at a lower cost--important to the customer, to the business stakeholders and to the welfare of the business.
            </p>
            <h4 class="colblu">
               Applying the Strategy</h4>
            <p>
               Business processes in service organizations are often more difficult to visualize and control than manufacturing processes, outwardly more difficult to improve. The good news is that Lean Six Sigma forces the same rapid and dramatic improvement in services and business processes as they do on the plant floor and just as effectively as in manufacturing, with even faster results! 
                <br>
                <br>
               Surprised? Lean Six Sigma has far more potential in the service industry, compared to traditional product/manufacturing companies. Gains can be multiplied in the service industries by replicating improvements across a large volume of customers (millions of insurance subscribers, banking customers, etc). Lean Six Sigma can revolutionize performance efficiencies in virtually any and every area of an organization; in all aspects of the service provided. 
                <br>
                <br>
               These are some examples of what happens in many companies � frequently tracking down information to complete a task, attempting to combine infrequent duties with regular duties, batching work items because it seems more convenient and efficient that way. Although these are common practices in service functions �accepted as "normal" � they each represent process waste: non-value-added work that increases delays and extends progress. In doing so, they also delay the quick completion of value-added work. 
                <br>
                <br>
                In any process, there will be some hindrances or interruptive factors that are deeply woven into a process. A Lean Six Sigma team will find ways to either eliminate or drastically reduce the amount of delays that these tasks add. 
            </p>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
               QPS provides the consulting and training that clients need to understand their potential to improve, develop a vision of excellence and implement a dynamic plan to realize it. Our services give providers the tools and techniques they need to reduce costs while improving their quality using a comprehensive, empowering method that gives teams the tools and support they need to attain quality breakthroughs, enhance client service and solve costly problems. Our integrated program is designed to increase customer satisfaction, shorten process cycle times, improve overall quality, and create shareholder value in all areas of your organization while changing the culture for team-driven success. 
                <br>
                <br>
                The application of a <strong>Lean Six Sigma Program</strong> transforms an organization. It enables businesses to better identify and meet customer needs by emphasizing creative problem solving and teamwork. QPS tackles the issues of non-value added resource consumption and errors by providing your associates with the knowledge and understanding to address both. Our <strong>Lean Six Sigma Program</strong> integrates process improvement tools and statistically based methods to identify inefficiencies, whether they are driven by variation, process flow or error related issues. It provides practical training and mentoring support for all levels of the organization, through the following courses: 

            </p>
            <ul>
                <li>
                Lean Six Sigma Management Overview
                <li>
                Lean Six Sigma for Champions
                <li>
                Lean Six Sigma Yellow Belt 
                <li>
                Lean Six Sigma Green Belt Certification 
                <li>
                Lean Six Sigma Black Belt Certification 
                <li>
                Lean Six Sigma Master Black Belt Certification 
                <li>
                Design for Six Sigma Overview 
                <li>
                Design for Six Sigma Certification 
                <li>
                Strategic Planning 
                <li>
                Coaching and Mentoring
            </ul>
            <p>
            Our curriculum builds on the strengths of our Lean Six Sigma expertise to deliver truly unique and unsurpassed training in organizational approaches to world-class asset management, performance measurement, and productivity improvement. Our training is specifically tailored to your program. All of our courses can be delivered at your site, and many of our courses are offered as public training at locations worldwide. Through careful study of existing processes, goal identification and Lean Six Sigma training, dramatic results are possible. 
               
            </p>
            <h4 class="colblu">
               What Lean Six Sigma can do for you?</h4>
            <p>
                Delivering products and services with speed, customer satisfaction and lower cost through operational excellence is essential to achieve and sustain superior shareholder returns in businesses and governmental enterprises. Operating excellence is becoming an even bigger priority in service segments like insurance, retail, banking, and government because so much of their costs are tied into operations. Eliminating this waste cannot only reduce costs, but more importantly allows a business to become faster and much more responsive to its customers, driving revenue growth. In our experience, as much as 40% of the cost in a service organization is tied up in delays, mistakes and rework done to solve problems for customers. Our unique Lean Six Sigma approach attacks these issues to: 
            </p>
            <ul>
                <li>
                Improve response times 
                <li>
                Build customer loyalty; targeting customer-driven improvement 
                <li>
                Better processing and delivery speeds 
                <li>
                Increase customer satisfaction and retention 
                <li>
                More efficient departments; increasing capacity without adding staff
                <li>
                Create new and innovative services and marketing capacities; driving competitive advantage
                <li>
                Less defects, errors, rework and waste
                <li>
                Better financials and higher annual savings
            </ul>
            <p>

            The QPS <strong>Lean Six Sigma Program</strong> and our support guides facilitators, team leaders and team members from their first meeting through the completion of a successful improvement project. Teams gain the knowledge, practical skills and tools they need to remedy the urgent quality problems while enhancing client service and support significantly, enhancing the skill set in the organization, while driving results via the project work. Transforming an organization according to the Lean Six Sigma model is a process of evaluation, definition, implementation and measurement. Working closely with the organization, we enable you to create and execute a program that fits your business and strategies for real-time operational improvements; facilitating transformation and creating a valuable, sustainable business. 
               
                <br>
                <br>
                Our experience gives us insight into successful execution strategies in a wide range of environments and cultures. Lean Six Sigma is a journey� QPS is your partner, resource and counsel. Our clients are achieving their goals; contact us today by e mail <a href="mailto:info@qpsinc.com" class="nounder bold">info@qpsinc.com</a> or calling 1-877-987-3801 and find out why so many businesses are using QPS for their success. 
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <div style="padding-top: 20px" class="hrefnounderclass">
                <h4 class="bold hundredline">
                    Lean Six Sigma for:</h4>
                <a href="LeanSixSigmaHealthcare.aspx" class="nounder bold">Healthcare</a><br>
                <a href="LeanSixSigmaManufacturing.aspx" class="nounder bold">Manufacturing</a><br>
                <a class="grayclass">Service</a><br>
            </div>
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

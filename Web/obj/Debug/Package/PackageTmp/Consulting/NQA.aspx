<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="NQA.aspx.cs"
    Inherits="QPS.Consulting.NQA" %>

<asp:Content ID="ContentNQA" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last">
                <a href="LeanSixSigma.aspx">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="LeanThinking.aspx">Lean Techniques</a></div>
            <div class="span-3 blackbl last">
                <a href="SixSigma.aspx">Six Sigma</a></div>
            <div class="span-4 blackbk last">
                <a>Management System</a></div>
            <div class="span-4 blackbl last">
              <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <div class="span-5" style="float: right">
                <a href="QualitySystem.aspx" class="nounder right">
                    <img src="../images/right.gif" class="right">
                    Return to Management System</a></div>
            <br class="clear">
            <h3 class="colbright" id="training">
                NQA</h3>
            <p>
               <%-- <span style="font-size: 125%">--%>
                    <img src="../images/management.gif" class="top right" style="margin-top: 0">
                    As America comes to embrace its need to rely on less fossil fuels and reduced toxic
                    emissions, the nuclear energy industry is taking off. Nuclear power offers affordable,
                    secure, and climate-friendly electricity to an energy-hungry world. As the demand
                    grows for products and services that meet the industry's unique requirements, QPS
                    is here to help suppliers meet those needs and provide the expertise to meet the
                    requirements of ASME NQA-1, 10 CFR 50 Appendix B, and ANSI N45.2 to compete in this
                    growing market. <%--</span>--%>
            </p>
            <br class="clear">
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                This standard sets forth requirements for the establishment and execution of quality
                assurance programs for the siting, design, construction, operation, and decommissioning
                of nuclear facilities. It reflects industry experience and current understanding
                of the quality assurance requirements necessary to achieve safe, reliable, and efficient
                utilization of nuclear energy, and management and processing of radioactive materials.
                The focus is on the achievement of results; emphasizing the role of the individual
                and line management in the achievement of quality, and fostering the consistent
                application of these requirements. There are 18 Sections in the criteria:
            </p>
            <ul>
                <li>
                Organization
                <li>
                Quality System
                <li>
                Design Control
                <li>
                Procurement Document Control
                <li>
                Instructions, Procedures, and Drawings
                <li>
                Control of Purchased Items and Services
                <li>
                Identification and Control of Items
                <li>
                Control of Special Processes
                <li>
                Control of Nonconforming Items
                <li>
                10 CFR Part 21 Reporting of Noncompliance
                <li>
                Inspection & Test Control
                <li>
                Control of Measurement and Test Equipment
                <li>
                Handling Storage and Shipping
                <li>
                Document Control
                <li>
                Inspection, Test, and Operating Status
                <li>
                Corrective Actions
                <li>
                Quality Assurance Records
                <li>
                Audits
            </ul>
            <h4 class="colblu">
                How QPS can help?</h4>
            <p>
                QPS can help your company get its quality system compliant with the requirements
                of the nuclear industry to ASME NQA-1 and 10 CFR 50 Appendix B. We dedicate ourselves
                to helping our clients implement a compliant quality system that gives them a competitive
                edge in obtaining more contracts and achieving operational excellence in the nuclear
                industry.
            </p>
            <p>
                <br />
                Our services include:
                <ul>
                    <li>
                    Implementation of NQA-1 into ISO 9001 Systems
                    <li>
                    Development of Quality System Documentation
                    <li>
                    Technical Expertise in Testing
                    <li>
                    Determination of Certifying Body
                    <li>
                    Assistance in NQA-1Certification (as needed)
                </ul>
            </p>
            <p>
                Our fast-track approach will save time and resources, decreasing your time to market.
                We expedite the process and minimize the costs involved by managing the process
                steps with you, providing you ownership of the system at your pace. QPS takes pride
                in its attention to detail and ability to provide consistent, quality service to
                help its clients get what they want, and provide a valuable service at the same
                time.
                <br />
                <br />
                <a href="../Company/Contact.aspx" title="Contact QPS" class="nounder bold">Contact QPS</a>
                to help you meet your nuclear system needs at a reasonable cost!
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <div style="padding-top: 20px; margin-left: -20px; width: 180px;" class="hrefnounderclass">
                <h4 class="bold hundredline">
                    Other Systems</h4>
                <a href="FDAMatters.aspx" class="nounder bold">FDA/Regulatory Matters</a><br>
                <a class="grayclass">Nuclear Quality Assurance</a><br>
                <a href="Crisis.aspx" class="nounder bold">Crisis Intervention</a><br>
                <a href="CEMark.aspx" class="nounder bold">CE Mark</a><br>
                <a href="HSCCP.aspx" class="nounder bold">HACCP</a><br>
            </div>
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

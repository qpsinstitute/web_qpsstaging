<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="LeaderShipTeams.aspx.cs"
    Inherits="QPS.Consulting.LeaderShipTeams" %>

<asp:Content ID="ContentLeaderShipTeams" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Consulting");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-3 blackbl last">
                <a href="LeanSixSigma.aspx">Lean Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="LeanThinking.aspx">Lean Techniques</a></div>
            <div class="span-3 blackbl last">
                <a href="SixSigma.aspx">Six Sigma</a></div>
            <div class="span-4 blackbl last">
                <a href="QualitySystem.aspx">Management System</a></div>
            <div class="span-4 blackbk last">
               <a href="LeaderShipTeams.aspx" title="Leadership/teams">Leadership/teams</a></div>
            <br class="clear">
            <br class="clear">
            <div class="span-6" style="float: right">
                <a href="BusinessImprovement.aspx" class="nounder right">
                    <img src="../images/right.gif" class="right">
                    Return to Business Improvement</a></div>
            <br class="clear">
            <h3 class="colbright" id="training">
                Leadership & Teams</h3>
            <p>
                <img src="../Images/LTeams.jpg" width="168" height="143" class="left top" style="margin-top: 0">
                <%--<span style="font-size: 125%">--%>Developing leadership skills can be easy if you concentrate
                    on being a good leader to yourself first. Those with good leadership skills aren't
                    perfect and still make mistakes like everyone else. The difference is that good
                    leaders learn from their mistakes and stay committed to the goals they set, inspiring
                    others as they go.
                    <br>
                    <br>
                    Like leadership, teamwork is a method that aligns the employee mindset in a cooperative
                    manner, toward a specific business purpose. The right mix of skill, motivation and
                    ability to manage a project without compromising its quality determines team effectiveness.
                    Team building is a process that develops cooperation and teamwork within a work
                    unit. Teamwork is the concept of people working together cooperatively as a team
                    in order to accomplish those goals/objectives. 
            </p>
            <h4 class="colblu">
                Applying the Strategy</h4>
            <p>
                To lead is to influence a group of people to progress towards a goal or achievement.
                In developing leadership skills, one needs to find the style that best suits their
                personality. Effective leaders generate higher productivity, lower costs, and find
                more opportunities than ineffective leaders. They create real results and attain
                goals, inspiring and motivating others with more lasting results.
                <br>
                <br>
                Great leaders utilize teams. Current corporate philosophy stresses that each member
                of a team plays an integral part in the success of the company. Through activities
                known as team building exercises, individuals can learn and practice trust, cooperation,
                brainstorming and feedback. These skills are invaluable to a business, in that
                they tap potential from within and create the ability to generate excitement. Whether
                it is for planning a meeting, managing a project, or giving a presentation, this
                enthusiasm is contagious, and makes the difference between being successful and
                being extraordinary.
            </p>
            <h4 class="colblu">
                How QPS Can Help?</h4>
            <p>
                With understaffing, outsourcing, and other morale-defeating activities on the rise,
                corporations recognize they must cultivate better internal communication. Being
                part of a team usually provides a sense of loyalty and ownership because each member
                of the team has a purpose and a function. Many businesses form teams, or committees,
                for varying purposes to allow cross-team work. Overall success depends on the team
                dynamic and how the leaders can build upon it.
                <ul>
                    <li>
                    Is there a cooperative culture within your organization?
                    <li>
                    What drives your associates to achieve success?
                </ul>
                Creating a dynamic, exciting culture within an organization, you need leaders and
                teams that work together to make great things happen. You need the right mix of
                skill, abilities and drive.
            </p>
            <p>
               
            </p>
            <p>
                <h4 class="colblu">
                    What Leadership and Teams can do for you?</h4>
                For an effective team, a leader needs to be established. Good leadership skills
                stem from being both self-aware and socially aware. Good leaders know themselves,
                while striving to learn about others through listening, observation and a compassionate,
                open attitude. True leaders are a part of the team they lead.
                <br>
                <br>
                Bringing together the right people whose skill and knowledge complement one another
                is key to team success. Members must share a common goal, respect one another, and
                be motivated to use their individual strengths to achieve the objectives.
                <br>
                <br>
                QPS works with its clients to determine their needs and helps develop mature teams
                by understanding strengths, building on weaknesses and finding potential. We know
                what it takes to create great corporate culture, and we help make it happen. Whether
                you need leadership coaches, team trainers, or facilitators, QPS can help you cultivate
                greatness, while becoming more efficient!
                <br>
                <br>
                QPS has leadership and team consultants, each with industry-specific knowledge who
                can help you identify the people, skills, and knowledge to build great teams. Changing
                culture can be difficult, but the payoff is huge. We give you the edge you need
                to accomplish great things as a unified whole. <a href="../Company/Contact.aspx"
                    title="Contact QPS" class="nounder bold">Contact us</a>
            </p>
        </div>
        <div class="span-4">
            <img src="../images/consulting.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="CareerTransition.aspx.cs" Inherits="QPS.Placement.WebForm1" %>
<asp:Content ID="ContentCarrer" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
    setPage("Placement");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	    <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a href="References.aspx" title="References">References</a></div>--%>
		<div class="span-5 blackbl last"><a href="Aboutus.aspx" title="About Us">About Us</a></div>
        <div class="span-4 blackbl last"><a href="Staffing.aspx" title="Staffing">Staffing</a></div>
        <div class="span-5 blackbl last"><a href="Recruitment.aspx" title="Recruitment">Recruitment</a></div>
		<div class="span-5 blackbk last"><a href="CareerTransition.aspx" title="Career Transition">Career Transition</a></div>
        <br class="clear"><br class="clear">
		
        <h3 class="colbright" id="staff">Career Transition</h3>
        <p style="font-size:125%">
        Our compassionate Career Transition Consultants at QPS are highly trained to work with candidates that are facing a career change or job redundancy.  In today's  market when most companies  are organizing and re-engineering their staff, our  consultants identify  your skills  and interests and provide tools and support for your specific needs and goals. We mentor individuals/groups to find a new career direction, while sharpening ones expertise.  We believe change is good and change starts at QPS Staffing. 
        </p>
        <div style="font-size:125%">
        <b>QPS consultants provide training in:</b> <br /><br />
        Career Counseling and Career Path Planning<br />
        Resume/Cover Letter Writing Technique<br />
        Career Search Methods such as Internet and Social Media<br />
        Time Management<br />
        Networking and Communication Strategies<br />
        Career Leverage and Career Integration<br />
        Strengthening your people skills in the workplace<br /> 
        Coaching, mentoring and team building skills<br />  
        Interviewing etiquette<br /> 
        Behavioral Skills Training<br />
        Assessment Testing<br /> 
        Computer Skills Testing<br />
        </div>
         <br class="clear">
            <strong>For more information, contact <a href="mailto:info@qpsinc.com">info@qpsinc.com</a>.</strong>
        
		</div>
        <div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space"><br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
      
</asp:Content>

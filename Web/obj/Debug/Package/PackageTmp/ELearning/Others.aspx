<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Others.aspx.cs"
    Inherits="QPS.ELearning.Others" %>

<%@ Register Src="~/Controls/Sidebar_ELearning.ascx" TagName="ELearning" TagPrefix="QPSSidebar" %>
<%@ Register Src="~/Controls/Sidebar.ascx" TagName="Sidebar" TagPrefix="QPSSidebar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Training");
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <div class="span-3 blackbl last" style="width: 123px">
                <a href="../Training/Calendar.aspx" title="Corporate Calendar">Corporate Calendar</a></div>
            <div class="span-3 blackbl last" style="width: 120px">
                <a href="../Training/UnEmployed.aspx" title="Public Training">Public Training</a></div>
            <div class="span-3 blackbl last" style="width: 100px">
                <a href="../Training/OnSite.aspx" title="On-Site Training">On-Site Training</a></div>
            <div class="span-3 blackbk last" style="width: 87px">
                <a href="EcourseList.aspx" title="eLearning">eLearning</a></div>
            <div class="span-3 blackbl last">
                <a href="../Training/Training.aspx" title="Training Grants">Training Grants</a></div>
            <div class="span-3 blackbl last" style="width: 130px">
                <a href="../Training/Registration.aspx" title="Corporate Register">Corporate Register</a></div>
            <br class="clear">
            <br class="clear">
            <h3 id="training" class="colbright" style="padding-top: 0px;">
                eLearning</h3>
            <p>
                <span style="font-size: 125%">Learn today�s hottest topics at your own pace and without
                    taking time away from your job. At work or at home, you can now learn what others
                    are learning and not have to worry about your job. You will have three months to
                    complete the program in your free time. Enhance your career by clicking on one of
                    the courses and follow the instructions. </span>
            </p>
            <div class="span-3 blackbl last">
                <a href="EcourseList.aspx" title="Lean Six Sigma">Lean Six Sigma</a></div>
            <div class="span-3 blackbl last" style="width: 180px;">
                <a href="ManagmentSystem.aspx" title="Management Systems">Management Systems</a></div>
            <div class="span-3 blackbl last" style="width: 180px;">
                <a href="ProjectManagement.aspx" title="Project Management">Project Management</a></div>
            <div class="span-3 blackbk last" style="width: 80px;">
                <a href="#" title="Others">Others</a></div>
            <br class="clear">
            <br class="clear">
            <h3 id="H3_1" class="colbright" style="padding-top: 0px;">
                Others</h3>
            <table class="mainText">
                <tr>
                    <td>
                        <table class="mainText" style="border: 1px dotted silver; background: #ffffff;">
                            <tr>
                                <td style="padding-left: 5px; padding-right: 25px; padding-top: 5px; padding-bottom: 15px;"
                                    width="100%">
                                    <div id="ad" runat="server">
                                        <style>
                                            .programTitle
                                            {
                                                font-family: inherit;
                                                background-color: #EAEAEA;
                                                font-size: 100%;
                                                color: black;
                                                font-weight: bold;
                                                height: 20px;
                                                margin-top: 15px;
                                                text-decoration: none;
                                            }
                                            .program
                                            {
                                                font-family: inherit;
                                                border: 1px;
                                                font-size: 100%;
                                                text-decoration: none;
                                            }
                                            span.programDescription
                                            {
                                                font-family: inherit;
                                                font-size: 100%;
                                                text-decoration: none;
                                            }
                                            a.programCartLink, a.programCartLink:active, a.programCartLink:visited
                                            {
                                                font-family: Verdana, Arial, Geneva, Helvetica, sans-serif;
                                                font-size: 12px;
                                                text-decoration: none;
                                            }
                                            a.programCartLink:hover
                                            {
                                                font-family: Verdana, Arial, Geneva, Helvetica, sans-serif;
                                                font-size: 12px;
                                                color: #838327;
                                                text-decoration: none;
                                            }
                                            #programCategory
                                            {
                                                color: #141D6B;
                                                width: 250px;
                                                margin-top: 5px;
                                                background-color: white;
                                                margin-bottom: 15px;
                                                font-family: inherit;
                                                font-size: 1.5em;
                                                font-weight: normal;
                                                display: block;
                                            }
                                            #categoryLinksDIV
                                            {
                                                font-family: Verdana, Arial, Geneva, Helvetica, sans-serif;
                                                font-size: 12px;
                                                color: black;
                                                border: 1px solid black;
                                                padding: 10px;
                                                width: 300px;
                                                background-color: #f0f8ff;
                                                margin-bottom: 15px;
                                                display: none;
                                            }
                                            a.categoryLink
                                            {
                                                text-decoration: none;
                                                color: #9400d3;
                                                font-family: Verdana, Arial, Geneva, Helvetica, sans-serif;
                                                font-size: 12px;
                                            }
                                        </style>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <script>                                                        var showCategoryLinks = true; </script>
                                                    <script src="http://www.mindedgeonline.com/jscripts/findDOM.js" type="text/javascript"></script>
                                                    <script src="http://www.mindedgeonline.com/jscripts/cookie.js" type="text/javascript"></script>
                                                    <script src="http://www.mindedgeonline.com/jscripts/cart.js" type="text/javascript"></script>
                                                    <script src="http://www.mindedgeonline.com/jscripts/remotePrograms_js.php?d=44&ref=1141&regp=qps&showsuites=true&showcats=true&sortcats=true&cats=Auxilium,Career_Support_Programs,Communication,Compliance,Conflict_Resolution_Programs,Creativity,Customer_Service,Demo_Courses,Entrepreneurship,Estrin,Financial_Resources_Management,Gender/Diversity,Health_Metrics,Human_Resource_Management,Information_Technology,Introductory_Courses,Job_Search,Leadership,Mainstream,Management,Nonprofit,Team-Based_Workshops&hidecart=false"
                                                        type="text/javascript"></script>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <p style="float: right; text-align: right;">
                <a class="nounder" href="#top" title="Top">
                    <img src="../images/top.gif" title="Top"></a></p>
        </div>
        <div class="span-4">
            <QPSSidebar:ELearning ID="Sidebar_ELearning" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>

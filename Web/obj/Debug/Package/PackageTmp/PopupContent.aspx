<%@ Page Language="C#" MasterPageFile="~/QPSPopUp.Master" AutoEventWireup="true"
    Codebehind="PopupContent.aspx.cs" Inherits="QPS.PopupContent" %>

<asp:Content ID="ContentCalendar" ContentPlaceHolderID="cphContent" runat="server">
    <form id="frm11" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:Repeater ID="rptCourseDetails" runat="server">
                        <ItemTemplate>
                            <p class="MsoBodyText3">
                                &nbsp;</p>
                            <h2>
                                <span style='font-family: Arial'>Course No.&nbsp;
                                    <%#Eval("CourseNo") %>
                            </h2>
                            <p class="MsoBodyText3" id="lblCourseDesc" runat="server">
                                <%#Eval("CourseDesc") %>
                            </p>
                            <div id="divcoursebenefit" runat="server" visible="false">
                                <p class="MsoBodyText3">
                                    &nbsp;</p>
                                <h2>
                                    <span style='font-family: Arial'>Course Benefits: </span>
                                </h2>
                                <p class="MsoNormal" style='text-align: justify'>
                                    <span style='font-size: 12.0pt; font-family: Arial'>
                                        <%#Eval("CourseBenifit") %>
                                    </span>
                                </p>
                            </div>
                            <div id="divPrerequisite" runat="server" visible="false">
                                <p class="MsoBodyText3">
                                    &nbsp;</p>
                                <h2>
                                    <span style='font-family: Arial'>Prerequisites: </span>
                                </h2>
                                <p class="MsoNormal" style='text-align: justify'>
                                    <span style='font-size: 12.0pt; font-family: Arial'>
                                        <%#Eval("Prerequisites") %>
                                    </span>
                                </p>
                            </div>
                            <div id="divCertification" runat="server" visible="false">
                                <p class="MsoBodyText3">
                                    &nbsp;</p>
                                <h2>
                                    <span style='font-family: Arial'>Certification Requirements: </span>
                                </h2>
                                <p class="MsoNormal" style='text-align: justify'>
                                    <span style='font-size: 12.0pt; font-family: Arial'>
                                        <%#Eval("CertificationReq") %>
                                    </span>
                                </p>
                            </div>
                            <div id="divDuration" runat="server" visible="false">
                                <p class="MsoBodyText3">
                                    &nbsp;</p>
                                <h2>
                                    <span style='font-family: Arial'>Duration: </span>
                                </h2>
                                <p class="MsoNormal" style='text-align: justify'>
                                    <span style='font-size: 12.0pt; font-family: Arial'>
                                        <%#Eval("Duration") %>
                                    </span>
                                </p>
                            </div>
                            <div id="divCost" runat="server" visible="false">
                                <p class="MsoBodyText3">
                                    &nbsp;</p>
                                <h2>
                                    <span style='font-family: Arial'>Cost: </span>
                                </h2>
                                <p class="MsoNormal" style='text-align: justify'>
                                    <span style='font-size: 12.0pt; font-family: Arial'>$<%#Eval("Cost") %>
                                    </span>
                                    <span style='font-size: 12.0pt; font-family: Arial'><%#Eval("CostDesc")%>
                                    </span>
                                </p>
                            </div>
                            <div id="divTopicCovered" runat="server" visible="false">
                                <p class="MsoBodyText3">
                                    &nbsp;</p>
                                <h2>
                                    <span style='font-family: Arial'>Topics Covered: </span>
                                </h2>
                                <p class="MsoNormal" style='text-align: justify'>
                                    <span style='font-size: 12.0pt; font-family: Arial'>
                                        <%#Eval("TopicsCovered") %>
                                    </span>
                                </p>
                            </div>
                            <div id="divMoreInfo" runat="server" visible="false">
                                <p class="MsoBodyText3">
                                    &nbsp;</p>
                               
                                <p class="MsoNormal" style='text-align: justify'>
                                    <span style='font-size: 12.0pt; font-family: Arial'>
                                        <%#Eval("MoreInfo") %>
                                    </span>
                                </p>
                            </div>
                            <div id="divInstructor" runat="server" visible="false">
                                <p class="MsoBodyText3">
                                    &nbsp;</p>
                                <h2>
                                    <span style='font-family: Arial'>Instructor: </span>
                                </h2>
                                <p class="MsoNormal" style='text-align: justify'>
                                    <span style='font-size: 12.0pt; font-family: Arial'>
                                        <%#Eval("Instructor") %>
                                    </span>
                                </p>
                            </div>
                            <div id="divAttendies" runat="server" visible="false">
                                <p class="MsoBodyText3">
                                    &nbsp;</p>
                                <h2>
                                    <span style='font-family: Arial'>Who Should Attend? </span>
                                </h2>
                                <p class="MsoNormal" style='text-align: justify'>
                                    <span style='font-size: 12.0pt; font-family: Arial'>
                                        <%#Eval("Attenddetail") %>
                                    </span>
                                </p>
                            </div>
                            <p class="MsoBodyText3">
                                &nbsp;</p>
                            <h2>
                                <span style="font-family: Arial; mso-bidi-font-weight: bold">For More Information:</span><span
                                    style="font-weight: normal; font-size: 11pt; font-family: Arial; mso-bidi-font-size: 10.0pt;
                                    mso-bidi-font-weight: bold"><o:p></o:p></span></h2>
                            <p class="MsoNormal">
                                <span style="font-family: Arial">Contact the Training Administrator, Customer Service <a
                                    href="mailto:info@qpsinc.com">info@qpsinc.com</a>, Telephone: 1-877-987-3801 or
                                    Jay Patel at 508-579-1006. </span>
                            </p>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </form>
</asp:Content>

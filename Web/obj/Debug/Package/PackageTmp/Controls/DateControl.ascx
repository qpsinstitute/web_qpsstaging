﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateControl.ascx.cs" Inherits="QPS.Controls.DateControl" %>
<%@ Register TagPrefix="DateControl" Namespace="PeterBlum.PetersDatePackage" Assembly="PetersDatePackage" %>

<link rel="stylesheet" type="text/css" media="screen" href="DatePackage/Appearance/StyleSheet1.css"" />

<DateControl:DateTextBox id="DTB2B" runat="server" xShowPopupB="True" xErrorForeColor="Red" xErrorBackColor="255, 192, 192" 
				xEnableContextMenuB="False" xLicensing="01-5542100833" AutoSharedCalendarB="true" >
				
				<xPopupCalendar xToggleImageUrl="/controls/DatePackage/Appearance/calendar.gif" >
					<xCalendar ID="XCalendar1" CssClass ="CSCCalendarNOFilter" runat="server">
						
					</xCalendar>
				</xPopupCalendar> 
			</DateControl:DateTextBox>
		
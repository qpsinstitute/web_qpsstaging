<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="QPS.Controls.Footer" %>
<!-- RSS javascripts -->
<meta http-equiv="pragma" content="no-cache" />
<link rel="alternate" type="application/rss+xml" title="RSS" href="http://www.qpsinc.com/js/latestnews.rss" />
<script src="../js/prototype.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/scriptaculous.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/ticker.js" type="text/javascript" charset="utf-8"></script>

 <script src="../Js/jquery.js" type="text/javascript"></script>
    <script src="../Js/jquery.innerfade.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(
		function () {
		    $('#news').innerfade({
		        animationtype: 'slide',
		        speed: 750,
		        timeout: 5000,
		        type: 'random',
		        containerheight: '1em'
		    });

		}
	);
    </script>
<script language="javascript" type="text/javascript">
        function openPopup(txtURL) {
            window.open(txtURL, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width=600,height=500");
        }
        </script>

<br class="clear" />

   
<div id="bottlist">
    <div class="newsTicketWrpHM">
        <span>Hot News Headlines</span>
        <div id="ticker" style="padding-left: 120px" class="ticketTxt">
             <ul id="news">
                <li><a href="#">Lean six sigma Green Belt certification is starting next month, reserve your seat now </a></li>
                <li><a href="#">PMP certification Prep courses starting every 6 weeks, guaranteed passing at first attempt </a></li>
                <li><a href="#">Agile Certification classes conducted every 6 weeks, guaranteed passing at first attempt </a> </li>
                <li><a href="#">Lean Expert classes are starting every 6 weeks, get our certification and make breakthrough improvements</a> </li>
                <li><a href="#">Black Belt classes are starting every month, reserve your seat now </a> </li>
                <li><a href="#">FDA related courses are available in Boston and NJ </a> </li>
            </ul>
        </div>
   


    </div>
    <br class="clear">
    <div class="span-24 last" style="width: 980px; padding: 5px; background: #fafafa;">
        <div class="span-18 last" style="width: 470px; padding: 5px; border: 1px solid silver;
            line-height: 2em">
            <h4 style="text-align: center; border-bottom: 1px solid #dadada; margin-bottom: 3px;
                padding: 5px; color: rgb(0,96,165);">
                <b>Training Programs</b>
            </h4>
            <div class="span-4 colborder" style="width: 180px; margin-left: 25px; font-weight: bold;">
                <a href="../Training/calendar.aspx?id=2" title="Aerospace">Aerospace</a><br>
                <a href="../Training/Calendar.aspx?id=8" title="Agile">Agile</a><br>
                <a href="../Training/Calendar.aspx?id=7" title="APICS/SME Certifications">APICS/SME
                    Certifications</a><br>
                  <a href="../Training/calendar.aspx?id=7" title="ASQ/IT Certifications">ASQ/IT Certifications</a><br>
                <a href="../Training/calendar.aspx?id=3" title="FDA Related">FDA Related</a><br>
                <a href="../Training/calendar.aspx?id=5" title="HR Related">HR Related</a><br>
                <a href="../Training/Calendar.aspx?id=2" title="ISO Related">ISO Related</a><br>
            </div>
            <div class="span-4 colborder" style="border-right: 0px solid #EEE; font-weight: bold;">
              
                <a href="../Training/calendar.aspx?id=1" title="Lean Six Sigma">Lean Six Sigma</a><br>
               <%-- <a href="../Training/Calendar.aspx?id=8" title="PMI Certifications">PMI Certifications</a><br>--%>
                <a href="../Training/calendar.aspx?id=8" title="PMP/Agile">PMP/Agile/SCRUM</a><br>
                <a href="../Training/calendar.aspx?id=5" title="Service Related">Service Related</a><br>
     <a href="../Training/calendar.aspx?id=4" title="Statistics">Statistics</a><br>
                <a href="../Training/calendar.aspx?id=7" title="Software Related">Software Related</a><br>
                <a href="../Training/Calendar.aspx?id=6" title="Supply Chain/Mfg">Supply Chain/Mfg</a><br>
            </div>
        </div>
        <%--end first two column--%>
        <div class="span-18 last" style="width: 470px; margin-left: 15px; padding: 5px; border: 1px solid silver;
            line-height: 2em">
            <h4 style="text-align: center; border-bottom: 1px solid #dadada; margin-bottom: 3px;
                padding: 5px; color: rgb(0,96,165);">
                <b>Consulting </b>
            </h4>
            <div class="span-4 colborder" style="width: 180px; margin-left: 25px; font-weight: bold;">
                <a href="../Consulting/QualitySystem.aspx" title="AS 9100C">AS 9100C</a><br>
                <a href="../Consulting/BusinessImprovement.aspx" title="Business Improvement">Business
                    Improvement</a><br>
                <a href="../Consulting/FDAMatters.aspx" title="FDA Related">FDA Related</a><br>
                <a href="../Consulting/LeanThinking.aspx" title="Lean">Lean</a><br>
                <a href="../Consulting/leanSixSigma.aspx" title="Lean Six Sigma">Lean Six Sigma</a><br>
                <a href="../Consulting/QualitySystem.aspx" title="Management Systems">Management Systems</a><br />
                <a href="../Consulting/FDAMatters.aspx" title="Medical Devices">Medical
                    Devices</a><br>
            </div>
            <div class="span-4 colborder" style="border-right: 0px solid #EEE; font-weight: bold;">
       <a href="javascript:openPopup('../documents/ISO 9001 Quality Management System .htm')"" title="ISO 9001">ISO 9001</a><br />
                <a href="../Consulting/LeaderShipTeams.aspx" title="Leadership/Teams">Leadership/Teams</a><br />

                <a href="../Consulting/NQA.aspx" title="Nuclear">Nuclear</a><br>
               
                <a href="../Consulting/ProjectManagement.aspx" title="Project Management">Project Management</a><br>
                <a href="../Consulting/SixSigma.aspx" title="Six Sigma">Six Sigma </a>
                <br>
                <a href="../Consulting/StrategicPlanning.aspx" title="Strategic Planning">Strategic
                    Planning</a><br>
                <a href="../Consulting/sCManagement.aspx" title="Supply chain">Supply chain</a><br>
            </div>
        </div>
    </div>
</div>
<div class="span-24 last">
    <div style="width: 972px; padding: 8px; height: 15px; text-align: left; font-size: 8pt;
        color: white; background: gray; border-bottom: 15px solid #eaeaea">
        <div style="float: left;">
            � <%= System.DateTime.Now.Year %> Quality & Productivity Solutions, Inc. All Rights Reserved</div>
    </div>
</div>

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" Codebehind="JobPosting.aspx.cs"
    Inherits="QPS.Jobs.JobPosting" %>

<asp:Content ID="ContentJobs" ContentPlaceHolderID="cphContent" runat="server">

<script language="javascript">
function Discription(id)
{
    document.getElementById (id).style.visibility ="visible";
    return false;
}
setPage("Resources");
    </script>

    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
           
            <div class="span-3 blackbk last">
                <a title="Jobs">Jobs</a></div>
            <div class="span-4 blackbl last">
                <a href="Register.aspx" title="Candidate Register">Candidate Registration</a></div>
            <div class="span-4 blackbl last">
                <a title="Employer Register" href="EmployerRegister.aspx">Employer Registration</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Current Job Openings</h3>
            <div id="div1">
                <form id="form1" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Repeater ID="rptjobs" runat="server">
                                    <HeaderTemplate>
                                        <div style="height:400px;">
                                            <table style="width: 580px;" rules="none" align="left" cellpadding="5" cellspacing="0"
                                                border="2px">
                                                <tr style="background-color: Gray; color: White;">
                                                    <td width="280px;" style="font-weight: bolder;">
                                                        JobTitle / Description</td>
                                                    <td width="80px;" style="font-weight: bolder;">
                                                        Location</td>
                                                    <td width="80px;" style="font-weight: bolder;">
                                                        Posted by</td>
                                                          <td width="80px;" valign="middle" style="font-weight: bolder;">
                                                        Posted Date</td>
                                                 
                                                      
                                                </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="vertical-align:top;">
                                                <asp:Label ID="txttitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"JobTitle")%>' Font-Bold="true" Font-Size="Small" Style="color: rgb(0,96,165);"></asp:Label>
                                                &nbsp; (<%# DataBinder.Eval(Container.DataItem, "ExperienceFrom")%>-&nbsp;<%# DataBinder.Eval(Container.DataItem, "ExperienceTo")%>yrs.)
                                                <br />
                                                <asp:Label ID="lsummary" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Summary")%>'></asp:Label>
                                            </td>
                                            <td style="vertical-align:top;">
                                                <asp:Label ID="llocation" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Location")%>'> </asp:Label>
                                            </td>
                                            <td style="vertical-align:top;">
                                                <asp:Label ID="lcompany" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Company")%>'></asp:Label>
                                            </td>
                                              <td style="vertical-align:top;">
                                                <asp:Label ID="lposted" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "PostedDate")).ToShortDateString()%>'></asp:Label>
                                            </td>
                                       
                                        </tr>
                                        <tr>
                                            <td>                                                
                                                <a href="JobDescription.aspx?JobID=<%# Eval("JobID")%>" class="nounder" style="text-decoration: underline">View details</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <div style="background-color: Gray; height: 1px;">
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                     </table> </div>
                                   </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td style="color: Red">
                                <asp:Label ID="lbldata" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div class="span-4">
            <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
    </div>
</asp:Content>

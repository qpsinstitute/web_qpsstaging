<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" Codebehind="Register.aspx.cs"
    Inherits="QPS.Jobs.Register" %>

<asp:Content ID="ContentRegister" ContentPlaceHolderID="cphContent" runat="server">

 

    <script language="javascript">
        setPage("Resources");
    </script>

    <script language="javascript" type="text/javascript">
        function setFocus() {
            document.getElementById("txtFname").focus();
        }    
    </script>

    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <div class="span-3 blackbl last">
                <a href="JobPosting.aspx" title="Jobs">Jobs</a></div>
            <div class="span-4 blackbk last">
                <a title="Candidate Register">Candidate Registration</a></div>
            <div class="span-4 blackbl last">
                <a title="Employer Register" href="EmployerRegister.aspx">Employer Registration</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="H3_1">
                Candidate Registration Form</h3>
            <form id="fRegister" runat="server">
                <div id="dreg" runat="server">
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                Fields marked with * are mandatory
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <br />
                                <asp:Label ID="lblmsg" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"  width="200px">
                                First Name
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtFname" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvFirstName" runat="server"
                                    ControlToValidate="txtFname" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Last Name
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtLname" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator2" runat="server"
                                    ControlToValidate="txtLname" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="vertical-align: top;">
                                Address
                            </td>
                            <td align="left" valign="middle">
                                <asp:TextBox ID="txtAdd" runat="server" TextMode="MultiLine" Width="190px" Height="60px"
                                    MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator3" runat="server"
                                    ControlToValidate="txtAdd" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                City
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtcity" runat="server" Width="200px" MaxLength="20"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvCity" runat="server" ControlToValidate="txtcity"
                                    ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                State</td>
                            <td align="left">
                                <asp:DropDownList ID="dpstate" runat="server" Width="203px" AutoPostBack="false">
                                    <asp:ListItem Value="none">Select State</asp:ListItem>
                                    <asp:ListItem Value="Alabama">Alabama</asp:ListItem>
                                    <asp:ListItem Value="Alaska">Alaska</asp:ListItem>
                                    <asp:ListItem Value="Arizona">Arizona </asp:ListItem>
                                    <asp:ListItem Value="Arkansas">Arkansas</asp:ListItem>
                                    <asp:ListItem Value="California">California </asp:ListItem>
                                    <asp:ListItem Value="Colorado">Colorado</asp:ListItem>
                                    <asp:ListItem Value="Connecticut">Connecticut</asp:ListItem>
                                    <asp:ListItem Value="Delaware">Delaware</asp:ListItem>
                                    <asp:ListItem Value="District of Columbia">District of Columbia</asp:ListItem>
                                    <asp:ListItem Value="Florida">Florida</asp:ListItem>
                                    <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
                                    <asp:ListItem Value="Hawaii">Hawaii</asp:ListItem>
                                    <asp:ListItem Value="Idaho">Idaho</asp:ListItem>
                                    <asp:ListItem Value="Illinois">Illinois</asp:ListItem>
                                    <asp:ListItem Value="Indiana">Indiana</asp:ListItem>
                                    <asp:ListItem Value="Iowa">Iowa</asp:ListItem>
                                    <asp:ListItem Value="Kansas">Kansas</asp:ListItem>
                                    <asp:ListItem Value="Kentucky">Kentucky</asp:ListItem>
                                    <asp:ListItem Value="Louisiana">Louisiana</asp:ListItem>
                                    <asp:ListItem Value="Maine">Maine</asp:ListItem>
                                    <asp:ListItem Value="Maryland">Maryland</asp:ListItem>
                                    <asp:ListItem Value="Massachusetts">Massachusetts</asp:ListItem>
                                    <asp:ListItem Value="Michigan">Michigan</asp:ListItem>
                                    <asp:ListItem Value="Minnesota">Minnesota</asp:ListItem>
                                    <asp:ListItem Value="Mississippi">Mississippi</asp:ListItem>
                                    <asp:ListItem Value="Missouri">Missouri</asp:ListItem>
                                    <asp:ListItem Value="Montana">Montana</asp:ListItem>
                                    <asp:ListItem Value="Nebraska">Nebraska</asp:ListItem>
                                    <asp:ListItem Value="Nevada">Nevada</asp:ListItem>
                                    <asp:ListItem Value="New Hampshire">New Hampshire</asp:ListItem>
                                    <asp:ListItem Value="New Jersey">New Jersey</asp:ListItem>
                                    <asp:ListItem Value="New Mexico">New Mexico</asp:ListItem>
                                    <asp:ListItem Value="New York">New York</asp:ListItem>
                                    <asp:ListItem Value="North Carolina">North Carolina</asp:ListItem>
                                    <asp:ListItem Value="North Dakota">North Dakota</asp:ListItem>
                                    <asp:ListItem Value="Ohio">Ohio</asp:ListItem>
                                    <asp:ListItem Value="Oklahoma">Oklahoma</asp:ListItem>
                                    <asp:ListItem Value="Oregon">Oregon</asp:ListItem>
                                    <asp:ListItem Value="Pennsylvania">Pennsylvania</asp:ListItem>
                                    <asp:ListItem Value="Rhode Island">Rhode Island</asp:ListItem>
                                    <asp:ListItem Value="South Carolina">South Carolina</asp:ListItem>
                                    <asp:ListItem Value="South Dakota">South Dakota</asp:ListItem>
                                    <asp:ListItem Value="Tennessee">Tennessee</asp:ListItem>
                                    <asp:ListItem Value="Texas">Texas</asp:ListItem>
                                    <asp:ListItem Value="Utah">Utah</asp:ListItem>
                                    <asp:ListItem Value="Vermont">Vermont</asp:ListItem>
                                    <asp:ListItem Value="Virginia">Virginia </asp:ListItem>
                                    <asp:ListItem Value="Washington">Washington</asp:ListItem>
                                    <asp:ListItem Value="West Virginia">West Virginia</asp:ListItem>
                                    <asp:ListItem Value="Wisconsin">Wisconsin</asp:ListItem>
                                    <asp:ListItem Value="Wyoming">Wyoming</asp:ListItem>
                                    <asp:ListItem Value="Armed Forces Africa">Armed Forces Africa</asp:ListItem>
                                    <asp:ListItem Value="Armed Forces Americas">Armed Forces Americas</asp:ListItem>
                                    <asp:ListItem Value="Armed Forces Canada">Armed Forces Canada</asp:ListItem>
                                    <asp:ListItem Value="Armed Forces Europe">Armed Forces Europe</asp:ListItem>
                                    <asp:ListItem Value="Armed Forces Middle East">Armed Forces Middle East</asp:ListItem>
                                    <asp:ListItem Value="Armed Forces Pacific">Armed Forces Pacific </asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvState" runat="server" ControlToValidate="dpstate"
                                    ErrorMessage="*" Display="Dynamic" InitialValue="none" ValidationGroup="register"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td align="right">
                                Zip Code</td>
                            <td align="left">
                                <asp:TextBox ID="txtzip" runat="server" Width="200px" MaxLength="10"></asp:TextBox>
                                <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionValidator1"
                                    runat="server" ErrorMessage="*" ControlToValidate="txtzip" ValidationExpression="[^a-zA-Z ]*"
                                    Display="Dynamic" ValidationGroup="register"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Phone
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtphone" runat="server" Width="200px" MaxLength="14"></asp:TextBox>
                                <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionValidator2"
                                    runat="server" ControlToValidate="txtphone" ErrorMessage="*" ValidationExpression="[^a-zA-Z ]*"
                                    Display="Dynamic" ValidationGroup="register"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Mobile
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtmob" runat="server" Width="200px" MaxLength="14"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator6" runat="server"
                                    ControlToValidate="txtmob" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionValidator3"
                                    runat="server" ControlToValidate="txtmob" ErrorMessage="Invalid Mobile Number" ValidationExpression="[^a-zA-Z ]*"
                                    Display="Dynamic" ValidationGroup="register"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Jobs Looking for
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtjob" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator7" runat="server"
                                    ControlToValidate="txtjob" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Email
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtemail" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtemail"
                                    ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionEmail"
                                    runat="server" ErrorMessage="Invalid Email Address" ControlToValidate="txtemail" Display="Dynamic"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="register"></asp:RegularExpressionValidator>&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <br />
                                Cover Letter
                            </td>
                            <td align="left">
                                <br />
                                <asp:FileUpload ID="CletterUpload1" runat="server" />
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvCover" runat="server" ControlToValidate="CletterUpload1"
                                    ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                                <asp:Label ID="lblCoverLetter" runat="server" ForeColor="Red"></asp:Label>
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Resume
                            </td>
                            <td align="left">
                                <asp:FileUpload ID="ResumeUpload" runat="server" />
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvResume" runat="server"
                                    ControlToValidate="ResumeUpload" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                                <asp:Label ID="lblresume" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <br />
                                <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnsubmit_Click"
                                    ValidationGroup="register" />&nbsp;
                                <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False"
                                    ValidationGroup="register" />
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
        <div class="span-4">
            <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
    </div>


</asp:Content>

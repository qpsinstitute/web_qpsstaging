﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="ViewInstructor.aspx.cs" Inherits="QPS.Instructor.ViewInstructor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
<script language="javascript" type="text/javascript">
    setPage("Training");       
    </script>
<div class="span-24 prepend-1 top highcont">
        <div class="span-18 top">
            <br class="clear"/>
            <h3 class="colbright pagetitle" id="H3_1">
              View Instructor</h3>
            <form id="fRegister" runat="server">
                <div id="dreg" runat="server">
                    <div id="logout" style="position:relative; left:115%; top:-35px; width:0px;">
                        <asp:LinkButton ID="lnkLogout" runat="server"  OnClick="lnkLogout_Click">Logout</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkExport" runat="server"  OnClick="btnExport_click">Export</asp:LinkButton>
                     </div>
                    <table width="100%">
                        <tr>
                            <td>
                            <asp:Repeater ID="rptRegistration" runat="server"  >
                                    <HeaderTemplate>
                                        <div style="height: 400px;">
                                            <table style="width:900px;" rules="none" align="left" cellpadding="5" cellspacing="0"
                                                border="2px">
                                                <tr style="background-color: Gray; color: White;">
                                                <td width="100px;" style="font-weight: bolder;"><asp:LinkButton ID="lnkdate" Runat="server" style="white-space:nowrap;color:White;" CommandArgument="Submitdate"  OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click"  >Submit Date</asp:LinkButton>
                                                      </td>
                                                    <td width="100px;" style="font-weight: bolder;"><asp:LinkButton ID="lnkfname" Runat="server" style="white-space:nowrap;color:White;"  CommandArgument="FirstName"  OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click" >First Name</asp:LinkButton>
                                                        </td>
                                                    <td width="100px;" style="font-weight: bolder;white-space:nowrap;"><asp:LinkButton ID="lnklname" Runat="server" style="white-space:nowrap;color:White;" CommandArgument="LastName"  OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click"  >Last Name</asp:LinkButton>
                                                        </td>
                                                    <td width="200px;" style="font-weight: bolder;"><asp:LinkButton ID="lnkemail" Runat="server" style="white-space:nowrap;color:White;" CommandArgument="Email"  OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click"  >Submitter Email Id</asp:LinkButton>
                                                      </td>
                                                    <td style="font-weight: bolder; width:200px;"><asp:LinkButton ID="lnkcell" Runat="server" style="white-space:nowrap;color:White;" CommandArgument="CellPhone"  OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click"  >Submitter Cell Phone</asp:LinkButton>
                                                        </td>
                                                        <td style="font-weight: bolder; width:200px;"><asp:LinkButton ID="lnkevedate" Runat="server" style="white-space:nowrap;color:White;" CommandArgument="Eventdate"  OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click"  >Event Date</asp:LinkButton>
                                                        </td>
                                                   
                                                </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                        <td   width="100px;" style="vertical-align:top;">
                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Submitdate") %>'> </asp:Label>
                                                
                                            </td>
                                            <td width="100px;" style="vertical-align:top;">
                                                <asp:Label ID="lblfrstname" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FirstName")%>'> </asp:Label>
                                                
                                            </td>
                                            <td  width="100px;" style="vertical-align:top;">
                                                <asp:Label ID="lbllstname" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"LastName")%>'></asp:Label>
                                            </td>
                                            <td style="vertical-align:top;">
                                                <a href="Instructor_Registration.aspx?InsID=<%#Eval("Insid")%>"><asp:Label ID="lblemail" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Email")%>'></asp:Label></a> 
                                            </td>
                                            <td style="vertical-align:top;">
                                                <asp:Label ID="lblPhone" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CellPhone")%>'></asp:Label>
                                            </td>
                                            
                                             <td style="vertical-align:top;">
                                                <asp:Label ID="lblevent" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Eventdate")%>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8">
                                                <div style="background-color: Gray; height: 1px;">
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                    
                                     </table> </div>
                                   </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </div>
                <div align="center" style="padding-left:50%;">
                    <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnPrev" runat="server" Text=" << Prev " OnClick="btnPrev_Click"/> &nbsp; <asp:Button ID="btnNext" runat="server" Text=" Next >> " OnClick="btnNext_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:40px;">
                            <asp:label id="lblCurrentPage" Font-Bold="true" runat="server"></asp:label>
                        </td>
                    </tr>
                </table>
                </div>
            </form>
        </div>
     
    </div>
</asp:Content>

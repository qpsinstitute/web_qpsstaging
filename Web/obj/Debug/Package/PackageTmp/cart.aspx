<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cart.aspx.cs" Inherits="QPS.Cart" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="masterHead" runat="server">
    <meta name="MSSmartTagsPreventParsing" content="TRUE" />
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
    <meta name="keywords" content="total quality management, tqm, six sigma, methodology, consulting, training, lean, ISO, ISO 9000, ISO 14000, 16949, ISO/TS, six-sigma employment, 6 sigma" />
    <meta name="description" content="A committed consulting and training partner for Six Sigma, Lean, and ISO. Customized training, on-site consulting, and results oriented work." />
    <title>Quality & Productivity Solutions, Inc. -- Dedicated provider of Six Sigma, Lean, and ISO Consulting and Training</title>
    <link rel="stylesheet" type="text/css" href="css/domestic.css" />
    <link rel="stylesheet" href="css/extended/screen.css" type="text/css" media="screen, projection">
    <link rel="stylesheet" href="css/extended/print.css" type="text/css" media="print">
    <!--[if IE]><link rel="stylesheet" href="css/extended/ie.css" type="text/css" media="screen, projection"><![endif]-->
    <link rel="stylesheet" href="css/extended/plugins/fancy-type/screen.css" type="text/css" media="screen, projection">
    <link rel="stylesheet" href="css/extended/src/typography.css" type="text/css" media="screen, projection">
    <script src="Js/javaja.js" type="text/javascript"></script>
    <script type="text/javascript" language="JavaScript1.2" src="Js/stmenu.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="Js/qfunctions.js"></script>
    <!-- RSS javascripts -->
    <meta http-equiv="pragma" content="no-cache">	
    <link rel="alternate" type="application/rss+xml" title="RSS" href="js/LatestNews.rss">
    <script src="js/prototype.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/scriptaculous.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/ticker.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>

    <div class="maincont" id="top">
	<div class="container">
	<!-- HEADER -->
	<div id="MainDivForDesign" class="topbar tops span-25 prepend-1 last" style="margin-bottom:0;border-top:15px solid #eaeaea">
    <script language="javascript" type="text/javascript">
	var Brsr=navigator.appName;
    if(Brsr=="Microsoft Internet Explorer")
    {
        document.getElementById("MainDivForDesign").className="topbar tops span-25 prepend-1 last;";
    }
    else
    {
        document.getElementById("MainDivForDesign").className="topbar tops span-24 prepend-1 last;";
    }
	</script>
    <input type="hidden" id="pagehead" value="" />
 	<div class="span-4 last">
        <a id="home-top" href="../default.aspx">Home</a>
    </div>
    <div class="span-4 last">
        <a id="company-top" href="../Company/about.aspx">Company</a>
    </div>
    <div class="span-4 last">
        <a id="training-top" href="../Training/Training.aspx">Training</a>
    </div>
    <div class="span-4 last">
        <a id="consulting-top" href="../Consulting/Consulting.aspx">Consulting</a>
    </div>
    <div class="span-4 last">
        <a id="resources-top" href="../Resources/Resources.aspx">Resources</a>
    </div>
    <div class="span-4 last">
        <a id="elearning-top" href="/ELearning/EcourseList.aspx">eLearning</a>
    </div>
</div>
<div class="span-25 last" style="margin-top:0;background:black">
    <div class="span-22" style="height:37px;background-position:left;background-repeat:no-repeat;background-color:black;padding:0px;margin:0px;">
    <div class="span-1 last"></div>
    <div id="submenu01" class="topsub">
        <div class="leftop">&nbsp;</div>
        <div class="centop">
            <div class="incentop" style="margin-left:13px">
                <a href="../Company/about.aspx">About QPS</a>&nbsp;|&nbsp;<a href="../Company/Clients.aspx">Clients</a>&nbsp;|&nbsp;<a href="../Company/Testimonials.aspx">Testimonials</a>&nbsp;|&nbsp;<a href="../Company/Contact.aspx">Contact us</a>
            </div>
        </div>
        <div class="rightop">&nbsp;</div>
    </div>
    <div id="submenu02" class="topsub">
        <div class="leftop">&nbsp;</div>
        <div class="centop">
            <div class="incentop" style="margin-left:151px">
                <a href="../Training/Training.aspx">Training</a>&nbsp;|&nbsp;<a href="../Training/OnSite.aspx">Onsite Training</a>&nbsp;|&nbsp;<a href="../Training/Calendar.aspx">Calendar</a>&nbsp;|&nbsp;<a href="../Training/Registration.aspx">Register</a>
            </div>
        </div>
        <div class="rightop">&nbsp;</div>
    </div>
    <div id="submenu03" class="topsub">
        <div class="leftop">&nbsp;</div>
        <div class="centop">
            <div class="incentop" style="margin-left:213px">
                <a href="../Consulting/SixSigma.aspx">Six Sigma</a>&nbsp;|&nbsp;<a href="../Consulting/LeanThinking.aspx">Lean Techniques</a>&nbsp;|&nbsp;<a href="../Consulting/BusinessImprovement.aspx">Biz Improvement</a>&nbsp;|&nbsp;<a href="../Consulting/QualitySystem.aspx">Management System</a>&nbsp;|&nbsp;<a href="../Consulting/LeanSixSigma.aspx">Lean Six Sigma</a>
            </div>
        </div>
        <div class="rightop">&nbsp;</div>
    </div>
    <div id="submenu04" class="topsub">
        <div class="leftop">&nbsp;</div>
        <div class="centop">
            <div class="incentop" style="margin-left:442px">
                <a href="references.html">Reference</a>&nbsp;|&nbsp;<a href="links.html">Links</a>&nbsp;|&nbsp;<a href="newsletters.html">Newsletters</a>
            </div>
        </div>
        <div class="rightop">&nbsp;</div>
    </div>
    </div>
    <div class="span-4">
        <img src="../images/finalv1.gif" class="top" alt="QPS Testimonial">
        <h1 style="display:none">QPS</h1>
    </div>
    <div class="span-9" style="margin-top:5px;width:800px;">
        <h2 class="alt bottom pull-1" style="color:silver;font-family:helvetica;">Quality and Productivity Solutions, Inc.</h2>        
        <div class="span-1">&nbsp;</div>
        <div style="padding-top:6px">
        <form action="" id="searchbox_016124679326840018074:zth8bogqxfo" onsubmit="return false;">
          <div>
            <input type="text" id="q" name="q" size="20" style="background:#FFFFFF url(http://qps.staging.perpetuating.com/images/qps_watermark.gif) left no-repeat;" onblur="setImage()" onclick="removeImg()" />
            <input type="submit" value="Search" />            
            <a href="../Company/Contact.aspx" class="plain" style="font-size:16px;padding-left:10px" >
                <img src="../images/email.gif" border=0 >&nbsp;
                <span style="color:#DADADA;font-size:16px;font-family:arial;font-weight:bold" >Contact Us</span>
            </a>            
          </div>
        </form>
        <script type="text/javascript" src="../Js/brand.js"></script>
        <div id="results_016124679326840018074:zth8bogqxfo" style="display:none">
          <div id="ANS1" class="cse-closeResults" > 
            <a >&times; Close</a>
          </div>          
          <div class="cse-resultsContainer" id="res"></div>
        </div>
        
        <link href="../Css/overlay.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../Js/api.js"></script>
        <script src="../Js/overlay.js" type="text/javascript"></script>
        <script type="text/javascript">
        function OnLoad() 
        {
          var cseo=new CSEOverlay("016124679326840018074:zth8bogqxfo", document.getElementById("searchbox_016124679326840018074:zth8bogqxfo"), document.getElementById("results_016124679326840018074:zth8bogqxfo"));
          cseo.searchControl.setLinkTarget(GSearch.LINK_TARGET_PARENT);  
        }

        GSearch.setOnLoadCallback(OnLoad);    
       // GSearch.setLinkTarget(google.search.Search.LINK_TARGET_PARENT);
        document.getElementById("q").style.fontSize = "11pt";        
        </script>
        </div>
    </div>
<br class="clear">
<div width=100% style="background:black;">&nbsp;</div>
</div>
<script language="javascript" type="text/javascript">
function removeImg()
{    
    document.getElementById("q").style.background = "#FFFFFF";
}

function setImage()
{ 
    if(document.getElementById("q").value == "")
        document.getElementById("q").style.background = "#FFFFFF url(http://qps.staging.perpetuating.com/images/qps_watermark.gif) left no-repeat";
}
function setPage(Pagename)
{
    if(Pagename == "Company")
    {
        document.getElementById('company-top').className = "topsact";
    }
    else if(Pagename == "Training")
    {
        document.getElementById('training-top').className = "topsact";
    }
    else if(Pagename == "Consulting")
    {
        document.getElementById('consulting-top').className = "topsact";
    }
    else if(Pagename == "Resources")
    {
        document.getElementById('resources-top').className = "topsact";
    }
    else if (Pagename == "eLearning")
    {
        document.getElementById('elearning-top').className = "topsact"
    }
}
</script>
<!-- Middle -->
<script language="javascript" type="text/javascript">
    setPage("Training");
</script>
<form>
<div id="test"  class="span-24 prepend-1 top highcont">
    <script language="javascript" type="text/javascript">
	    var Brsr=navigator.appName;
        if(Brsr=="Microsoft Internet Explorer")
        {
            document.getElementById("test").className="span-25 prepend-1 top highcont";
        }
        else
        {
            document.getElementById("test").className="span-24 prepend-1 top highcont";
        }
    </script>
	<div class="span-18 top" style="float:right">
		<div class="span-3 blackbk last"><a>Training</a></div>
		<div class="span-3 blackbl last"><a href="Training/Calendar.aspx">Calendar</a></div>
		<div class="span-3 blackbl last"><a href="Training/OnSite.aspx">Onsite Training</a></div>
		<div class="span-3 blackbl last"><a href="Training/Registration.aspx">Register</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright pagetitle" id="training">Your Cart</h3>
		<br class="clear">
		<h4 class="headtitle">You added:</h4>
		<p>Please review the contents of your cart. You may continue adding courses or proceed to checkout.</p>
		<br class="clear">
		<div class="span-12">
		<input onclick='location.href="https://localhost:39273/youraccount/RegistrationSignIn.aspx"' type="button" id="btnCheckout" value="Proceed to Checkout >>" >
		<iframe width="400" height="400" scrolling="no" frameborder="0" src="http://localhost:39273/youraccount/CartPage.aspx"></iframe>
		</div>
		<br class="clear">
	</div>
	<div class="span-4">
	    <img src="images/addtocart-big.gif" border=0 class="pull-1 top">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="images/check.gif">
	        <h3 class="mostwrap"><a href="/Training/Calendar.aspx" class="nounder">Click here</a></h3> for our Upcoming Corporate Training Programs
	    </div>
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
<script language="javascript" type="text/javascript">
	var Brsr=navigator.appName;
	if(Brsr=="Microsoft Internet Explorer")
	{
		document.getElementById("MiddleLeft").className="span-7 prepend-1 colborder";
	}
	else
	{
		document.getElementById("MiddleLeft").className="span-5 prepend-1 colborder";
	}
</script>

<!-- Footer -->
<br class="clear">
<div id="bottlist">	
    <br>
	<div id="divselect" class="span-5 prepend-1 colborder">
	    <h3 style="line-height:140%;"><span style="color:rgb(0,96,165);">Select a topic</span> to see our training programs and consulting services</h3>
		<h5>&nbsp;</h5><h5>&nbsp;</h5><br>
	</div>
	<div id="div1" class="span-4 colborder upp">
	    <h4><a href="../Training/Calendar.aspx?id=2">AS 9100</a></h4> 	    
	    <h4><a href="../ELearning/EcourseList.aspx">E Learning</a></h4> 
	    <h4><a href="../Training/Calendar.aspx?id=4">FDA Related</a></h4>
	    <h4><a href="../Training/Calendar.aspx?id=6">Human Resources</a></h4>
		<h4><a href="../Training/Calendar.aspx?id=2">ISO 9001:2008</a></h4>
  	</div>
	<div class="span-4 colborder upp">  	    
  	    <h4><a href="../Training/Calendar.aspx?id=3">ISO 14001</a></h4> 
        <h4><a href="../Training/Calendar.aspx?id=2">ISO/TS 16949</a></h4>         
        <h4><a href="../Training/Calendar.aspx">Lean Six Sigma</a></h4>
        <h4><a href="../Training/Onsite.aspx">Onsite Training</a></h4>  
	   <h4><a href="../Training/Calendar.aspx?id=9">Project Mgmt.</a></h4>	    
  	</div>
	<div class="span-6 colborder upp">
	   <h4><a href="../Training/Calendar.aspx?id=9">Project Mgmt. Certification</a></h4>	     		
        <h4><a href="../Training/Training.aspx">Public Training</a></h4>
        <h4><a href="../Training/Calendar.aspx?id=6">Service</a></h4>
        <h4><a href="../Training/Calendar.aspx?id=8">Software</a></h4>
        <h4><a href="../Training/Calendar.aspx?id=5">Statistics</a></h4>  	    
  	</div>
    <br class="clear">    
	<div id="divup" class="span-25 last"  style="padding-top:12px;padding-left:12px;background:#C8C8C8;">
	    <div style="height:36px;">
	        <div class="span-5 colborder prepend-1"><h5 style="margin-bottom:0;padding-bottom:0;"><b>Upcoming Courses</b></h5></div>
	        <div class="span-17"><div id="ticker"><a href="News/LatestNews1.aspx" id="news-link">Read the latest news and information.</a></div></div>
	    </div>
	</div>
	<br class="clear"/>
	<div class="span-24 last" id="LastLeftDesc1" style="padding:5px;background-color:#fafafa;">
	    <div class="span-5 prepend-1" Id="LastLeftDesc2" style="background-color:#fafafa;">
	        <h3 class="splfn" style="margin-bottom:5px;">2009 Course Calendar released</h3>
	        <p>
	            We are proud to release our 2009 public training program with over 80 courses. <a href="../Documents/calender-Training 2009.pdf" target="_blank">Download</a> our course program for more details.
	        </p>
	    </div>
	    <div class="span-17 last" style="padding:5px;border:1px solid silver;line-height:2em;background-color:#fafafa;width:698px">
		    <h4  style="border-bottom:1px solid #dadada;margin-bottom:3px;padding:5px;color:#3A5087;"><b>Quick Connect &raquo;</b></h4>
			<div class="span-3 colborder">
			    <a href="../Company/about.aspx">About Us</a><br>
			    <a href="../Consulting/QualitySystem.aspx">AS 9100</a><br>
			    <a href="../Training/Calendar.aspx">Calendar</a><br>
			    <a href="../Company/Clients.aspx">Clients</a><br>
			    <a href="../Consulting/Consulting.aspx">Consulting</a><br>
			    <a href="../Company/Contact.aspx">Contact Us</a><br>
			</div>			
			<div class="span-4 colborder">			    
			    <a href="../Consulting/BusinessImprovement.aspx">ISO 14000</a><br>
			    <a href="../Consulting/QualitySystem.aspx">ISO 9000</a><br>
			    <a href="../Consulting/QualitySystem.aspx">ISO 13485</a><br>
			    <a href="../Consulting/BusinessImprovement.aspx">Leadership</a><br>
			    <a href="../Resources/Links.aspx">Links</a><br>
			    <a href="../Training/Calendar.aspx">Map & Directions</a><br>
			</div>			
			<div class="span-4 colborder">
			    <a href="../Consulting/QualitySystem.aspx">NQA</a><br>
			    <a href="../Consulting/BusinessImprovement.aspx">Outsourcing</a><br />
			    <a href="../Consulting/BusinessImprovement.aspx">Project Management</a><br />
			    <a href="../Consulting/QualitySystem.aspx">Quality System</a><br>
			    <a href="../Training/Registration.aspx">Register</a><br />
				<a href="../Consulting/BusinessImprovement.aspx">Strategic Planning</a><br />
			</div>			
			<div class="span-3 last">
			    <a href="../Consulting/BusinessImprovement.aspx">Supply Chain</a><br />
				<a href="../Company/Contact.aspx">Support</a><br>
			    <a href="../Consulting/BusinessImprovement.aspx">Teams</a><br />
			    <a href="../Company/Testimonials.aspx">Testimonials</a><br>
			</div>
	    </div>
	</div>
	<script language="javascript" type="text/javascript">
	    var Brsr=navigator.appName;
		if(Brsr=="Microsoft Internet Explorer")
		{
			document.getElementById("LastLeftDesc2").className="span-6 prepend-1";						
  			document.getElementById("LastLeftDesc1").className="span-25 last";
		}
		else
		{
			document.getElementById("LastLeftDesc1").className="span-24 last";
		}
	</script>
	<script language="javascript" type="text/javascript">
	    var Brsr=navigator.appName;
		if(Brsr=="Microsoft Internet Explorer")
		{
			document.getElementById("divselect").className="span-67 prepend-1 colborder";
			document.getElementById("div1").className="span-5 colborder upp";
			
		}
		else
		{
			document.getElementById("divselect").className="span-5 prepend-1 colborder";
			document.getElementById("div1").className="span-4 colborder upp";
			document.getElementById("divup").style.width="976px;";
		}
</script>
</div>
<div class="span-25 last">
    <div style="width:972px;padding:8px;text-align:left;font-size:8pt;color:white;background:gray;border-bottom:15px solid #eaeaea">
        � 2009 Quality & Productivity Solutions, Inc. All Rights Reserved�
    </div>
</div>
</form>
    </div>
<!-- Other -->
		<div class="footnote span-25 last" style="background-color:white;z-index:100;">
		<div style="float:right;padding-right:15px;">
		<p class="grayref">
		<a href="mailto:webmaster@qpsinc.com">Feedback</a>&nbsp;&nbsp;
		<a href="/Company/Contact.aspx">Contact</a>&nbsp;&nbsp;
		<a href="/Company/PrivacyPolicy.aspx">Privacy Policy</a></p>
		</div>
		</div>
</div>

<br class="clear">
</body>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4685318-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</html>

<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="SeminarforPMPExamInMA.aspx.cs" Inherits="QPS.Events.SeminarforPMPExamInMA" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">

 <script language ="javascript" type ="text/javascript" >
     setPage("Events");
    </script>
      <div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">

     <br class="clear"/><br class="clear"/>
	           <div>  <h1 class="arial13black eititle">
                <span>How to Pass the PMP� exam on the 1st Attempt!</span>
                </h1><br />
                <p>
                Before you register for that famous PMP� exam, register for this seminar! Understand fundamentals, processes, tools/techniques in Project Management and the PMI Body of Knowledge, as specified by Project Management Institute. Increase your competency in Project Management and learn secrets on how to pass the PMP� or CAPM� Certification Exam the first time.
<br /><br />

 <a href="Register.aspx?sub=MA - How to Pass the PMP� exam on the 1st Attempt!" target="_blank">
                REGISTRATION IS REQUIRED!</a><br /><br />
<b class="fontfamily">To register: email contact information (name, email, phone#, etc.) to <a href="mailto:jayp@qpsinc.com" style="color:#5E5E5E">jayp@qpsinc.com</a> and <a href="mailto:info@qpsinc.com" style="color:#5E5E5E">info@qpsinc.com</a> with �STATE of MA - PMP� Event� in subject line.</b>
 <br />Cost: $195 - FREE if registration received by October 31, 2012!

                </p>
                </div>
                <div class="eidtitle">
                <%--<% if (Request.QueryString["Date"] == "true")
                   {%>--%>
                  <%-- <strong>Date :</strong> Monday, October 15, 2012<br />--%>
              <%--  <% }%>
                <%else
                   { %>--%>
                   <strong>Date :</strong> Monday, November 12, 2012<br /> 
                <%--<%} %>--%>
                <strong>Time :</strong> 01:00 PM � 03:30 PM<br /><br />
                 <strong>Location : </strong> THE QPS INSTITUTE, 225 Cedar Hill Street, 3rd floor, Marlborough, MA 01752<br />
                 &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp(Phone) 1-877-987-3801<br />
                </div><br />    
                 <div class="eidtitle">
                <strong>Instructor Profile:</strong><br />
                <span>&nbsp;</span><p>
                About Jay P. Patel<br />
                President & CEO of Quality & Productivity Solutions, Inc.<br />
                Founder, The QPS Institute<br />
                Master Black Belt, PMP, Risk Management Professional, Supply Chain Professional & Biomedical Auditor<br /><br />
                </p>
                <p>
              	Jay possesses over 25 years of professional work experience in management and quality assignments. He has provided consulting to many Fortune 500 clients and trained thousands of people. He is an ASQ fellow and holds 10 ASQ Certifications including Six Sigma Certified Black Belt and Certified Biomedical Auditor. He is a RAB-Quality System Lead Auditor and Environmental Auditor. Jay holds PMI certifications. He is a certified Project Management Professional (PMP�), a certified Risk Management Professional (RMP�) and a certified Scheduling Professional (SP�). Jay has served as Chapter President of the Project Management Institute and of the Institute of Industrial Engineers. He has held a number of positions at Section and Region levels within ASQ.
                </p>
               
                </div> 
                </div>
                <div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
</div>
</div>





</asp:Content>
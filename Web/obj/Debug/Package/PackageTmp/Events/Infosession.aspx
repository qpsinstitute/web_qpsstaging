<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Infosession.aspx.cs" Inherits="QPS.Events.Infosession"
    MasterPageFile="~/QPSSiteHome.Master" %>

<asp:Content ID="ContentInfosession" runat="server" ContentPlaceHolderID="cphContent1">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                <asp:Label ID="lbl1" runat="server">Open House
                </asp:Label>
            </h3>
            <p style="font-size: 12px">
                <font style="font-size: 12px">
		    Whether you're just starting your new job out or seeking professional development in your career, the
            QPS Institute can help you achieve your goals. Join us at our open house! Observe a training program
            from 11 am- 12 noon then take a tour the QPS Institute where you can meet QPS Staff, present your
            resume to discuss training opportunities/career goals and enjoy refreshments.<br />
            </font>
                <div style="font-size: 12px; vertical-align: top;">
                    <font style="background-color: Yellow;"><b> Wednesday, January 12, 2011 at 11 am-The QPS Institute 225 Cedar Hill Street Marlborough, MA 01752</b></font>
                </div>
                <br />
                <b style="font-size: 12px; font-weight: bold;">Visit us daily! Directions are easy as
                    1- 2- 3!</b><br />
                <font style="font-size: 12px">
		   1. Take 495,<br />
           2. Take Exit 23 C (Simarano Drive Exit toward Marlborough / Southboro & keep left at fork in the ramp),<br />
           3. Take left onto Simarano Drive then turn right onto Cedar Hill.  <br />
            </font>
                <br />
                <div style="font-size: 12px; vertical-align: top;">
                    <font style="background-color: Yellow; font-style: italic;"><b> Register via email:</b></font>
                    <a href="mailto:info@qpsinc.com" target="_blank">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com"
                        target="_blank">jayp@qpsinc.com</a> <font style="background-color: Yellow; font-style: italic;"><b>- Seating is limited so call 1-877-987-3801 today!</b></font>
                </div>
                <br />
                <br />
                <a href="../Events/Register.aspx?sub=Open House" style="text-decoration: none; color: #0060A5;"
                    target="_blank">Click Here to Register</a>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

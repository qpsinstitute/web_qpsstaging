<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="FreeSeminarforSupplyChain.aspx.cs" Inherits="QPS.Events.FreeSeminarforSupplyChain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">

 <script language ="javascript" type ="text/javascript" >
     setPage("Events");
    </script>
      <div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">

     <br class="clear"/><br class="clear"/>
	           <div>  <h1 class="arial13black eititle">
                <span>Lean Supply Chain Overview</span>
                </h1><br />
                <p>
                Designed for individuals that are charged with improving supply chain and material management, this seminar provides an overview on how process teams and business leaders can learn from the best in class case studies to implement the best practices to translate strategy into action plans. It provides an overview of understanding the supply chain & unlocking the secret to improved supply chain performance, reducing waste and making supply chain lean! 
                
               
                Register if you are interested in:
<ul>
<li>Supply chain �waste</li>
<li>Evolution of supplier relationships</li> 
<li> Tools for supply chain excellence</li>
<li>Adding flexibility</li>
<li>Process design- current problems</li>
<li>Process design- volatility amplification in the supply chain</li>
<li>Just In Time, demand flow directed processes</li>
<li>Designing supply chain</li>
<li>Supplier management</li>
<li>Sourcing strategies</li>
</ul>

<br />
 <a href="Register.aspx?sub=NJ - Lean Supply Chain Overview" target="_blank">
                REGISTRATION IS REQUIRED!</a><br /><br />



<b class="fontfamily">To register: email contact information (name, email, phone#, etc.) to <a href="mailto:jayp@qpsinc.com" style="color:#5E5E5E">jayp@qpsinc.com</a> and <a href="mailto:info@qpsinc.com" style="color:#5E5E5E">info@qpsinc.com</a> with �SC Event� in subject line.</b>&nbsp;
 <br />Cost: $195 - FREE if registration received by October 23, 2012!<br />

 <br />Please arrive at 15 minutes prior to check in.



                </p>
                </div>
               <div class="eidtitle">
                <%--<% if (Request.QueryString["Date"] == "true")
                   {%>--%>
                  <%-- <strong>Date :</strong> Monday, October 15, 2012<br />--%>
              <%--  <% }%>
                <%else
                   { %>--%>
                   <strong>Date :</strong> Tuesday, December 4, 2012<br /> 
                <%--<%} %>--%>
                <strong>Time :</strong> 1:00 PM � 3:00 PM<br /><br />
                <strong>Location : </strong> THE QPS INSTITUTE, 33 Wood Avenue South, 6th floor, Iselin, NJ, 08830<br />
                 &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp(Phone) 1-877-987-3801<br />
                </div><br />    
                 <div class="eidtitle">
                <p></p>
                <strong>Instructor Profile:</strong><br />
                <span>&nbsp;</span><p>
                About Jay P. Patel<br />
                President & CEO of Quality & Productivity Solutions, Inc.<br />
                Founder, The QPS Institute<br />
                Master Black Belt, PMP, Risk Management Professional, Supply Chain Professional & Biomedical Auditor<br /><br />
                </p>
                <p>
              	Jay possesses over 25 years of professional work experience in management and quality assignments. He has provided consulting to many Fortune 500 clients and trained thousands of people. He is an ASQ fellow and holds 10 ASQ Certifications including Six Sigma Certified Black Belt and Certified Biomedical Auditor. He is a RAB-Quality System Lead Auditor and Environmental Auditor. Jay holds PMI certifications. He is a certified Project Management Professional (PMP�), a certified Risk Management Professional (RMP�) and a certified Scheduling Professional (SP�). Jay has served as Chapter President of the Project Management Institute and of the Institute of Industrial Engineers. He has held a number of positions at Section and Region levels within ASQ.
                </p>


                   <span>&nbsp;</span>
                <p>
                <strong>About QPS</strong>
                <br />
                 <p>Quality & Productivity Solutions, Inc. provides consulting, training and auditing for ISO, Six Sigma, Lean, Quality Management System, Project Management, Supply Chain and other related business improvements. Visit: <a href="http://www.qpsinc.com/" style="color:#5E5E5E">http://www.qpsinc.com/</a> </p>
                
                
                 </p>


                </div> 
                </div>
                <div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
</div>
</div>





</asp:Content>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventInfo.aspx.cs" Inherits="QPS.Events.EventInfo" MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentEventInfo" runat="server" ContentPlaceHolderID="cphContent">
    <div class="span-24 prepend-1 top highcont">
    <div class="span-19 top" style="float:right">
  
        <br class="clear"><br class="clear">
		<h3 class="colbright" id="training" >
		<asp:label id="lbl1" runat="server">Free ISO 14001 / OHSAS 18001 Workshop!
		</asp:label>
		</h3>
		<br />
		<p style="font-size: 12px">
		    <b class="colbright">Warwick, R.I. � February 17, 2011 � </b>
		    <font style="font-size:12px">
		    On Thursday, February 17, 2011, Quality & Productivity Solutions with the QPS Institute will be hosting a free workshop on ISO 14001 / OHSAS 18001.<br />
            </font>
            <div style="font-size:12px; margin-left:280px;">
	            Thursday, February 17, 2011<br />
                <font style="margin-left:38px;">9 AM � 11:30 AM</font><br />
                 <font style="margin-left:30px; vertical-align:middle;">Crowne Plaza Hotel</font><br />
                 <font style="margin-left:20px;">801 Greenwich Avenue</font><br />
                 <font style="margin-left:32px;">Warwick, RI 02886</font><br />
	        </div>
		 
                <BR />
                <p style="font-size:12px;">This workshop has been designed to provide an overview about ISO 14001 standard for Environmental Management System and OHSAS 18001 standard for Safety Management System.  It will provide assistance on how to implement them in the organization including the auditing program.  Participants will learn the requirements of the Environmental Management System and Safety Management System as well as a basic understanding of how to implement both systems.  The seminar leader will be RAB Certified Auditor with implementation experience in both systems.  </p>
            </font>
            <b style=" font-size:12px;">Who Should Attend? Auditors, managers, engineers and people interested in learning how to implement ISO 14001 or OHSAS 18001.  NO CONSULTANTS PLEASE.</b>    
	    </p>
	    <br />
            <br />
	    <div style="font-size: 12px; width:540px; vertical-align:top; margin-left:50px;">
           <font style="background-color:Yellow;"><b> REGISTRATION REQUIRED: 1</b><font style="font-size:8px; vertical-align:text-top;">ST</font> <b>COME, 1</b><font style="font-size:8px; vertical-align:text-top;">ST</font><b> SERVE � SEATING IS LIMITED!</b></font>
        </div> 
        <br />
            <br />
	      <a href="../Events/Register.aspx?sub=Free ISO 14001 / OHSAS 18001 Workshop" style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a> 
	        
		<div></div>
		
    </div>
    
    <div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
</asp:Content>
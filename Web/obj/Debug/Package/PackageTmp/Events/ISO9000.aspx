﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="ISO9000.aspx.cs" Inherits="QPS.Events.ISO9000" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
<script language="javascript" type="text/javascript">
    setPage("Events");
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <br class="clear" />
            <br class="clear" />
            <div>
                <h1 class="arial13black" style="font-family: verdana; margin-bottom: 5px; font-size: 14px;
                    font-weight: bold; color: #a68d0d;">
                     <% if (Request.QueryString["State"] == "NJ")
                   {%>
                <span>ISO 9001:2008 transition to ISO 9001:2015 - Woodbridge</span>
                <% }%>
                <% else if (Request.QueryString["State"] == "CT")
                   {%>
                <span>ISO 9001:2008 transition to ISO 9001:2015- East Berlin</span>
                <%} %>
                <%else
                   { %>
                <span>ISO 9001:2008 transition to ISO 9001:2015 - Marlborough</span>
                <%} %>
                    
                </h1>
                <br />
            </div>
            <div style="font-family: verdana; font-size: 12px; color: #5E5E5E; line-height: 18px;">
                <% if (Request.QueryString["State"] == "NJ")
                   {%>
                <strong>Date :</strong> Wednesday, February 10, 2016<br />
                <% }%>
                <% else if (Request.QueryString["State"] == "CT")
                   { %>
                <strong>Date :</strong> Tuesday, February 09, 2016<br />
                <%} %>
                <%else
                   { %>
                <strong>Date :</strong> Monday, February 08, 2016<br />
                <%} %>
                <strong>Time :</strong> 5:00 PM – 6:00 PM<br />
                <br />
                <% if (Request.QueryString["State"] == "NJ")
                   {%>
                <strong>Location : </strong>The QPS Institute, 581 Main Street Suite 640 Woodbridge,
                NJ 07095<br />
                <% }%>
                <% else if (Request.QueryString["State"] == "CT")
                   {%>
                <strong>Location : </strong>The QPS Institute, 1224 Mill Street, Building B, East
                Berlin, CT 06023<br />
                <%} %>
                <%else
                   { %>
                <strong>Location : </strong>The QPS Institute, 28 Lord Road, Suite 205, Marlborough,
                MA 01752<br />
                <%} %>
                <br />
                Refreshments provided.<br />
                <br />
                <% if (Request.QueryString["State"] == "NJ")
                   {%>
                <a href="Register.aspx?sub=ISO 9001:2008 transition to ISO 9001:2015 - Woodbridge" target="_blank">
                    REGISTRATION IS REQUIRED!</a>
                <% }%>
                <% else if (Request.QueryString["State"] == "CT")
                   {%>
                <a href="Register.aspx?sub=ISO 9001:2008 transition to ISO 9001:2015 - East Berlin" target="_blank">
                    REGISTRATION IS REQUIRED!</a>
                <%} %>
                <%else
                   { %>
                <a href="Register.aspx?sub=ISO 9001:2008 transition to ISO 9001:2015 - Marlborough" target="_blank">
                    REGISTRATION IS REQUIRED!</a>
                <%} %>
                <br />
                <br />
                Contact Customer Service 1-877-987-3801, Email <a href="mailto:info@qpsinc.com" style="color: #5E5E5E">
                    info@qpsinc.com</a> with your name, phone and email address so I can respond
                with your registration confirmation!
            </div>
            <br />
            <div style="font-family: verdana; font-size: 12px; color: #5E5E5E; line-height: 18px;">
                <strong>Instructor :</strong><br />
                <% if (Request.QueryString["State"] == "NJ")
                   {%>
                Edwin May
                <% }%>
                <% else if (Request.QueryString["State"] == "CT")
                   {%>
                Jack Reardon
                <%} %>
                <%else
                   { %>
                Jay P. Patel
                <%} %>
                <br />
            </div>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

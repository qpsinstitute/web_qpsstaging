<%@ Page AutoEventWireup="true" Codebehind="Register.aspx.cs" Inherits="QPS.InformationSession.Register"
    Language="C#" MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentRegister" runat="server" ContentPlaceHolderID="cphContent">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <br class="clear">
            <br class="clear">
            <h3 id="training" class="colbright">
               <asp:Label ID="lblhead" runat="server" Text=""></asp:Label> - Registration
            </h3>
           <!-- <font style="font-size: 120%">
                <b class="colbright" style="font-size: 12px;">Come to the next free informational session at the QPS Institute where we can assist you with: </b> 
                <br />
                <br />
                <ul style="padding-left:30px; font-size:13px;">
                    <li>Training Opportunities </li>
                    <li>Jobs & Career Goals</li>
                    <li>Review of QPS Institute�s Key Courses & Benefits to You</li>
                    <li>Specific Candidate Consideration</li>
                    <li>Why Lean Six Sigma & PMP</li>
                    <li>Instructors � Qualifications</li>
                    <li>Possible Funding/Scholarships - Application Assistance</li>
                    <li>Refreshments Provided</li>
                </ul>
            </font>-->
           <br />
            <form id="frmContact" runat="server" style="vertical-align: top;">
                <table width="100%">
                    <tr>
                        <td class="first" colspan="2">
                            <h4>
                                <b style="background-color:Yellow;">Contact Form</b>
                            </h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Please feel free to ask any questions on our "Consulting & Training" programs and
                            inquiries. Appropriate experts will answer your questions with no obligation to
                            you!<br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                            <b>Note: Required fields marked with </b>
                            <img src="../images/req.gif"><br>
                            <span id="ErrorMsg" runat="server" style="color: #C00;"></span>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="third">
                            Name
                            <img src="../images/req.gif"><br>
                            <input id="name" runat="server" name="ctl00$cphContent$name" size="34" title="Name"
                                type="text" />
                            <asp:RequiredFieldValidator ID="Rqname" runat="server" ControlToValidate="name" ErrorMessage="*"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revName" runat="server" ControlToValidate="name"
                                ErrorMessage="Only alphanumeric characters are allowed" ValidationExpression="[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="third">
                            Email Address
                            <img src="../images/req.gif"><br>
                            <input id="emailaddress" runat="server" name="ctl00$cphContent$emailaddress" size="34"
                                title="Email Address" type="text" />
                            <asp:RequiredFieldValidator ID="Rqemailaddress" runat="server" ControlToValidate="emailaddress"
                                ErrorMessage="*"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="Rgemailaddress" runat="server" ControlToValidate="emailaddress" 
                                ErrorMessage="Please enter a valid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="third">
                            Company
                            <%--<img src="../images/req.gif">--%>
                            <br>
                            <input id="company" runat="server" name="ctl00$cphContent$company" size="34" title="Company" type="text" />
                            <%--<asp:RequiredFieldValidator ID="Rqcompany" runat="server" ControlToValidate="company" ErrorMessage="*"></asp:RequiredFieldValidator>
			                             <asp:RegularExpressionValidator ID="revCompany" runat="server" ControlToValidate="company" ErrorMessage="Only alphanumeric characters allowed" ValidationExpression="[a-zA-Z0-9 ]*" ></asp:RegularExpressionValidator>--%>
                        </td>
                    </tr>
                   <!-- <tr>
                        <td class="third">
                            Subject
                            
                            <br>
                            <asp:DropDownList runat="server" ID="reglist" Width="245px">
                            <asp:ListItem Value="0">Free Informational Session</asp:ListItem>
                             <asp:ListItem Value="1">Free ISO 14001 / OHSAS 18001 Workshop</asp:ListItem>
                            </asp:DropDownList>
                            
                        </td>
                    </tr>-->
                    <tr>
                        <td class="third">
                            Position<br>
                            <input id="position" runat="server" name="ctl00$cphContent$position" size="34" title="Position" type="text" /></td>
                    </tr>
                    <tr>
                        <td class="third">
                            Telephone #
                            <img src="../images/req.gif">
                            <br>
                            <input id="telephone" runat="server" maxlength="50" name="ctl00$cphContent$telephone" size="34" title="Telephone" type="text" />
                            <asp:RequiredFieldValidator ID="Rqtelephone" runat="server" ControlToValidate="telephone" ErrorMessage="*"></asp:RequiredFieldValidator><br />
                            <asp:RegularExpressionValidator ID="revTelephone" runat="server" ControlToValidate="telephone" ErrorMessage="Only numeric characters are allowed" ValidationExpression="[^a-zA-Z ]*"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="third">
                            Questions/Comments
                            <img src="../images/req.gif">
                            <asp:RequiredFieldValidator ID="Rqcomments" runat="server" ControlToValidate="comments" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="comments" runat="server" cols="26" name="ctl00$cphContent$comments" rows="4" title="Questions/Comments"></textarea>
                        </td>
                    </tr>
                     <tr>
                        <td class="third">
                           Verification Code
                            <img src="../images/req.gif">
                            
                        </td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>
                        <asp:TextBox ID="txtVerify" style="vertical-align:top" width="70px" Height="24px" runat="server"></asp:TextBox>&nbsp;
                        <asp:Image ID="imCaptcha" ImageUrl="~/Controls/Captcha.ashx" ImageAlign="AbsMiddle"  runat="server" />
                        <asp:RequiredFieldValidator ID="reqcaptcha" runat="server" ControlToValidate="txtVerify"  style="vertical-align:super;" ErrorMessage="*"></asp:RequiredFieldValidator>
                         
                        </td>
                    </tr>
                    <tr>
                    <td>
                      <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtVerify" Display="Dynamic"
                         ErrorMessage="You have Entered a Wrong Verification Code!Please Re-enter!!!" OnServerValidate="CAPTCHAValidate"></asp:CustomValidator>  
                    </td>
                    </tr>
                    <tr>
                        <td>
                            <input id="Submit" runat="server" class="third" name="ctl00$cphContent$Submit" onserverclick="btnSubmit_Click" title="Submit" type="submit" value="Submit" /><br>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

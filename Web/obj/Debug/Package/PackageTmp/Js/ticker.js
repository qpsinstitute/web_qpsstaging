Effect.DefaultOptions.duration = 0.3;
NewsTicker = Class.create();
Object.extend(NewsTicker.prototype, {
	feedURL:"../js/LatestNews.rss",
	tickerDiv: "ticker", 
	tickerTitle: "news-link",
	tickerLink: "../News/LatestNews1.aspx",	
	pauseLength: 3500,
	timer: 0,
	currentTitle: 0,
	currentLink: 0,
	items: null,
	initialize: function() {
		
		this.items = [];
		
		new Ajax.Request(
			this.feedURL, {
				method: "get",
				onSuccess: function(response) { //alert(response.responseXML);
					this.parseXML(response.responseXML);
					this.buildTicker();
				}.bind(this),
				onFailure: function() {
					console.log("Please visit http://qpsinc.com/News/LatestNews.aspx for the latest news and information.");
				},
				onException: function(req, err) { //alert(err);
					// throw(err);
				}
			}
		);
	},
	
	buildTicker: function() {
		// replace the placeholder content with the first news title
		if (this.items[this.currentTitle]) {
			$(this.tickerTitle).innerHTML = this.items[this.currentTitle]['title'];
			this.start();// start the timer if we have valid headlines
		}
		if (this.items[this.currentLink]) {
			$(this.tickerLink).innerHTML = this.items[this.currentLink]['link'];
			this.start();// start the timer if we have valid headlines
		}
	},
	
	parseXML: function(xml) {
		// build the array of news titles		
		//alert(xml.getElementsByTagName("item").length);
		$A(xml.getElementsByTagName("item")).each(function(item) {
			title = item.getElementsByTagName("title")[0].childNodes[0].nodeValue;
			//var link = NewsTicker.tickerLink;
			link = item.getElementsByTagName("link")[0].childNodes[0].nodeValue;
			this.items.push({title: title, link: link});
		}.bind(this));
	},
	
	start: function() {
		this.interval = setInterval(this.showNext.bind(this), this.pauseLength);
	},
	
	stop: function() {
		clearInterval(this.interval)
	},
	
	showNext: function() {
		
		//determine next headline
		if ( this.currentTitle < this.items.length-1 ) {
			this.currentTitle = this.currentTitle+1;
		} else {
			this.currentTitle = 0;
		}
		
		if ( this.currentLink < this.items.length-1 ) {
			this.currentLink = this.currentLink+1;
		} else {
			this.currentLink = 0;
		}
		
		new Effect.Fade('news-link', {
			afterFinish: function() {
				this.switchData();
				new Effect.Appear('news-link'); }.bind(this)});

	},
	
    switchData: function() {
        this.tickerLink = this.items[this.currentLink]['link'];
		$(this.tickerTitle).setAttribute("href", this.tickerLink);
		if (this.items[this.currentTitle]) {
			$(this.tickerTitle).innerHTML = this.items[this.currentTitle]['title'];
		}
		
			 
		
	}
});

Event.observe(window, 'load', function() {
	var ticker = new NewsTicker();
});


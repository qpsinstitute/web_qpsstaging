//    // meta's

//    document.write('<meta name="MSSmartTagsPreventParsing" content="TRUE" />');
//    document.write('<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />');
//    document.write('<meta name="keywords" content="total quality management, tqm, six sigma, methodology, consulting, training, lean, ISO, ISO 9000, ISO 14000, 16949, ISO/TS, six-sigma employment, 6 sigma" />');
//    document.write('<meta name="description" content="A committed consulting and training partner for Six Sigma, Lean, and ISO. Customized training, on-site consulting, and results oriented work." />');
//    document.write('<title>Quality & Productivity Solutions, Inc. -- Dedicated provider of Six Sigma, Lean, and ISO Consulting and Training</title>');

//   	// styles

//   	document.write('<link rel="stylesheet" type="text/css" href="css/domestic.css" />');
//  	document.write('<link rel="stylesheet" href="css/extended/screen.css" type="text/css" media="screen, projection">');
//	document.write('<link rel="stylesheet" href="css/extended/print.css" type="text/css" media="print">');
//	document.write('<!--[if IE]><link rel="stylesheet" href="css/extended/ie.css" type="text/css" media="screen, projection"><![endif]-->');
//	document.write('<link rel="stylesheet" href="css/extended/plugins/fancy-type/screen.css" type="text/css" media="screen, projection">');
//	document.write('<link rel="stylesheet" href="css/extended/src/typography.css" type="text/css" media="screen, projection">');


//	// js ajax

//	document.write('<script src="javaja.js" type="text/javascript"></script>');


//	// dynamic menu lib

//	document.write('<script type="text/javascript" language="JavaScript1.2" src="stmenu.js"></script>');



	// domestic functions

	function clch(wdv)
  	{

  	if ( wdv.substr(0,1) == "x")
		{
			for (ctr=1;ctr<=5;ctr++)
			{

    		var vdv = "x"+ctr.toString();


    		if (vdv == wdv)
    			{
    			document.getElementById(vdv).style.backgroundImage="url(images/tab-02.gif)";
    			document.getElementById(vdv).style.color="black";
    			}
    		else
    			{
       			document.getElementById(vdv).style.backgroundImage="";
    			document.getElementById(vdv).style.color="";
    			}
			}
		}

		switch (wdv)
		{
		case 'x1':
  		ajaxpage('pop-company.html','deadmen');
  		break;
		case 'x2':
  		ajaxpage('pop-training.html','deadmen');
  		break;
		case 'x3':
  		ajaxpage('pop-consult.html','deadmen');
  		break;
  		case 'x4':
  		ajaxpage('pop-resource.html','deadmen');
  		break;
  		case 'x5':
  		ajaxpage('pop-elearn.html','deadmen');
  		break;
		default:
  		ajaxpage('pop-null.html','deadmen');
		}

	}


	function popmen(popit)
	{

	document.getElementById('deadmen').style.visibility='hidden';
	document.getElementById('lefmen').style.visibility='visible';
	document.getElementById('lefmen').style.opacity='0.5';
	document.getElementById('lefmen').style.filter="alpha(opacity='50')";

		switch (popit)
		{
		case 'x1':
  		ajaxpage('pop-company.html','lefmen');
  		break;
		case 'x2':
  		ajaxpage('pop-training.html','lefmen');
  		break;
		case 'x3':
  		ajaxpage('pop-consult.html','lefmen');
  		break;
  		case 'x4':
  		ajaxpage('pop-resource.html','lefmen');
  		break;
  		case 'x5':
  		ajaxpage('pop-elearn.html','lefmen');
  		break;
		default:
  		ajaxpage('pop-null.html','lefmen');
		}
	}



	function popdown()
	{
	document.getElementById('lefmen').style.visibility='hidden';
	document.getElementById('deadmen').style.visibility='visible';
	}

	function ocb(idz)
	{
	document.getElementById(idz).style.opacity='1.0';
	return;
	}

	function ocd(idz)
	{
	document.getElementById(idz).style.opacity='0.7';
	return;
	}



	function EnableDisable(chk)
    {

	for (cnt=1;cnt<=10;cnt++)
    {
    var dvid = "Cont"+cnt.toString();
    if (cnt==chk)
    	{
    	document.getElementById(dvid).scrollTop=0;
    	document.getElementById(dvid).style.display='block';
    	}
    else
    	{
    		document.getElementById(dvid).style.display='none';
    	}
    }
    return;
    }



    function toggle_visibility(id,queid) {
    if(document.getElementById(id+queid).style.display=='none')
    {
        document.getElementById("faq"+queid).style.display = 'none';

    }
    var e = document.getElementById(id+queid);

    e.scrollTop=0;

    if(e.style.display == 'none')
        e.style.display = 'block';
    else
        e.style.display = 'none';
    }



    function tch(aid)
    {        for (ltr=1;ltr<=9;ltr++)
    	{
    	var lid = "lw"+ltr.toString();
    	srid = parseInt(aid.substring(1));
    	if(ltr==srid){if(document.getElementById(lid).className=="")document.getElementById(lid).className="Current";}
    	else{document.getElementById(lid).className="";}
    	}
        return;
    }

    function onsite(oid)
    {

        for (otr=1;otr<=23;otr++)
    	{
    	var onid = "on"+otr.toString();
    	var onst = "onsite"+otr.toString();
    	onsrid = parseInt(oid.substring(2));
    	document.getElementById(onst).scrollTop=0;

    	if(otr==onsrid){
    	    if(document.getElementById(onid).className=="")
    	    document.getElementById(onid).className="clicked";
    	    document.getElementById(onst).style.display='block';
    	    }
    	else{
    	    document.getElementById(onid).className="";
    	    document.getElementById(onst).style.display='none';
    	    }
    	}

    return;
    }

    function onsite12(oid)
    {

        for (otr=1;otr<=6;otr++)
    	{
    	var onid = "clon"+otr.toString();
    	var onst = "clonsite"+otr.toString();
   
    	onsrid = parseInt(oid.substring(4));
    
      	document.getElementById(onst).scrollTop=0;
        
    	if(otr==onsrid)
    	{
    	    if(document.getElementById(onid).className=="")
    	    document.getElementById(onid).className="clicked";
    	    element = document.getElementById('divfor' + onid);
            element.style.background = 'black';
            document.getElementById(onst).style.display='block';
             document.getElementById('welcome1').style.display='none';

    	}
    	else{
    	    document.getElementById(onid).className="";
    	    element = document.getElementById('divfor' + onid);
            element.style.background = '#948A54';
            document.getElementById(onst).style.display='none';}
    	}

    return;
    }

    function noScr(anid)
    {
    //document.getElementById(contid).scrollTop=0;
    return false;
    }



		function removeBreaks(){

		var para = document.getElementById("paragraphs").checked;
		var nopara = document.getElementById("noparagraphs").checked;
		var noBreaksText = document.getElementById("oldText").value;

		noBreaksText = noBreaksText.replace(/(\n\r|\n|\r)/gm,"<1br />");

		re1 = /<1br \/><1br \/>/gi;
		re1a = /<1br \/><1br \/><1br \/>/gi;

		if(nopara == 1 || nopara ==  true){
		noBreaksText = noBreaksText.replace(re1," ");
		}else{
		noBreaksText = noBreaksText.replace(re1a,"<1br /><2br />");
		noBreaksText = noBreaksText.replace(re1,"<2br />");
		}

		re2 = /\<1br \/>/gi;
		noBreaksText = noBreaksText.replace(re2, " ");

		re3 = /\s+/g;
		noBreaksText = noBreaksText.replace(re3," ");

		re4 = /<2br \/>/gi;
		noBreaksText = noBreaksText.replace(re4,"\n\n");
		document.getElementById("newText").value = noBreaksText;
		}



		function showhidepops(takeid)
		{		for(str=1;str<=7;str++)
			{
			fulltake = "standard"+str.toString();

			document.getElementById(fulltake).scrollTop=0;

			if(str==takeid){document.getElementById(fulltake).style.display='block';}
			else{document.getElementById(fulltake).style.display='none';}
			}
		return false;
		}

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Glossary.aspx.cs" Inherits="QPS.Resources.Glossary" %>
<%--<%@ Register TagPrefix="QPSSidebar" TagName="Resources" Src="~/Controls/Sidebar_Resources.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>--%>

<asp:Content ID="ContentGlossary" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("Resources");
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
        <div class="span-3 blackbk last"><a href="references.aspx" title="References">References</a></div>
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">References</h3>

<h4>Glossary of Key ISO 9000 Terms</h4>
<style type="text/css">
table.lightgrid
{
font-size:11pt;
color:#444444;
cell-padding:0;
cell-spacing:0;
border-top:1px solid #eaeaea;
border-right:1px solid #eaeaea;
}

table.lightgrid td
{
padding:5px;
border-bottom:1px solid #eaeaea;
border-left:1px solid #eaeaea;
}

table.lightgrid td.lft
{
color:#004080;
font-size:10pt;
}
</style>
<table class="lightgrid">
<tbody><tr><td class="lft">
  <p>Audit</p>
  </td>
  <td>
  <p>A systematic, independent and documented
  process for obtaining audit evidence and evaluating it objectively to
  determine the extent to which audit criteria are fulfilled. </p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Capability</p>
  </td>
  <td>
  <p>The ability of an organization, system or
  process to realize/produce a product that will fulfill its requirements.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Characteristic</p>
  </td>
  <td>
  <p>A distinguishing feature.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Competence</p>
  </td>
  <td>
  <p>A demonstrated ability to apply knowledge
  and skills.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Continual Improvement</p>
  </td>
  <td>
  <p>A recurring activity to increase the ability
  to fulfill requirements.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Corrective Action</p>
  </td>
  <td>
  <p>An action to eliminate the cause of a
  detected nonconformity or other undesirable situation.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Customer Satisfaction</p>
  </td>
  <td>
  <p>Customerís perception of the degree to which
  his/her expectations have been met.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Grade</p>
  </td>
  <td>
  <p>A category or rank given to different
  quality requirements for products, processes or systems having the same
  functional use.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Infrastructure</p>
  </td>
  <td>
  <p>System of facilities, equipment and services
  needed for the operation of an organization.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Interested Party</p>
  </td>
  <td>
  <p>A person or group having an interest in the
  performance or success of an organization.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Management System</p>
  </td>
  <td>
  <p>A set of interactive elements (system) to
  establish a policy and objectives and to achieve them. </p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Preventive Action</p>
  </td>
  <td>
  <p>An action to eliminate the cause of a potential
  nonconformity or other undesirable potential situation.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Procedure</p>
  </td>
  <td>
  <p>A specified way to carry out an activity or
  a process.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Process</p>
  </td>
  <td>
  <p>A set of interrelated or interacting
  activities, which transforms inputs into outputs.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Product</p>
  </td>
  <td>
  <p>The result of a process.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Qualification</p>
  </td>
  <td>
  <p>Demonstrating the ability to fulfill
  specified requirements. </p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Quality</p>
  </td>
  <td>
  <p>The degree to which a set of inherent
  characteristics meets requirements.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Quality Assurance</p>
  </td>
  <td>
  <p>The part of management that is focused on
  providing confidence that quality requirements will be fulfilled.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Quality Control</p>
  </td>
  <td>
  <p>Part of management that is focused on
  fulfilling the quality requirements.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Quality Manual</p>
  </td>
  <td>
  <p>A document specifying the quality management
  system of an organization.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Quality Objective</p>
  </td>
  <td>
  <p>Something sought or aimed for, related to
  quality. </p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Quality Plan</p>
  </td>
  <td>
  <p>A document specifying which procedures and
  associated resources shall be applied by whom and when to a specific project,
  process, product or contract.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Quality Policy</p>
  </td>
  <td>
  <p>The overall intentions and direction of an
  organization related to quality and is expressed by top management.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Record</p>
  </td>
  <td>
  <p>A document stating results achieved or
  providing evidence of activities performed. </p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Requirement</p>
  </td>
  <td>
  <p>A need or expectation that is stated,
  implied or obligatory.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Review</p>
  </td>
  <td>
  <p>An activity undertaken to determine the
  suitability, adequacy and effectiveness of the subject matter to achieve
  established objectives.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Specification</p>
  </td>
  <td>
  <p>A document stating requirements.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Traceability</p>
  </td>
  <td>
  <p>The ability to trace the history,
  application or location of the item/process under consideration.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Validation</p>
  </td>
  <td>
  <p>Confirmation, through the provision of
  objective evidence that the requirements for a specific intended use or
  application have been fulfilled.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Verification</p>
  </td>
  <td>
  <p>Confirmed, through the provision of
  objective evidence that specified requirements have been fulfilled.</p>
  </td>
 </tr>

 <tr><td class="lft">
  <p>Work Environment</p>
  </td>
  <td>
  <p>A set of conditions under which work is
  performed.</p>
  </td>

 </tr>
</tbody>
</table>
</div>

	<div class="span-4">
	    <img src="../images/references-01.gif" class="pull-1" alt="QPS References">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>

</div>

<%--<table>
<tr>
<td valign="top" style="background-color:#d9d9d9;">
    <QPSSidebar:Resources id="Sidebar_Resources" runat="server"/>
    <QPSSidebar:Sidebar id="Sidebar" runat="server"/>
</td>
<td>
<table class="mainText">
    <tr>
        <td>
            <%=KeyISO9000Terms%>
        </td>
    </tr>
</table>
</td>
</tr></table>--%>

</asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="QPSNewJersey.aspx.cs" Inherits="QPS.Resources.QPSNewJersey" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">

<div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
    <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="QPSNewJersey">
                QPS Institute - also in New Jersey!
            </h3>
            <br />
            <p style="font-size: 125%">
                <b class="colbright">Trenton, NJ – November 1, 2012 –</b> <font style="font-size: 90%">
		    Jay P. Patel, President and CEO of Quality & Productivity Solutions, Inc. announced that QPS will be conducting training at its newly opened location in <b>Metro Park, Woodbridge, NJ</b>. Not far from New York, the Iselin location is now open to enroll training participants and is licensed as a Private Vocational School with the New Jersey State Department of Education. This out of state school approval is another special milestone at QPS because it allows students within the area to experience the QPS Institute, just as they would in Massachusetts, outside of Boston, where the QPS Institute is headquartered.
            <br />QPS is committed to providing world class training, consulting and other services to meet individual and organization needs.<br /><br /></font>
               <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                508-987-3800<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <%--<a href="http://www.qpsinc.com">www.qpsinc.com</a><br />--%>
           </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>














</asp:Content>

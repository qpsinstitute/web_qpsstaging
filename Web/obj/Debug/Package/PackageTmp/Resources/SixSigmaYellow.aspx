﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true"
    CodeBehind="SixSigmaYellow.aspx.cs" Inherits="QPS.Resources.SixSigmaYellow" %>

<asp:Content ID="ContentQpsAgile" ContentPlaceHolderID="cphContent" runat="server">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Congratulations to the New Six Sigma Yellow Belts at Dunkin’ Donuts!
            </h3>
          
            <img src="../Images/NewSixSigmaYellow.jpg" runat="server" alt="" height=300 width=450 style="vertical-align:middle;" />
            <br /><br />
            <p style="font-size: 125%">
                <b class="colbright">Marlborough, MA – April 2013 – </b><font style="font-size: 90%">
                    Jay Patel, President and CEO of Quality and Productivity Solutions, Inc. awarded
                    certificates to the new Yellow Belts as part of a Lean & Six Sigma Initiative training
                    program for Dunkin’ Donuts. Yellow Belt, Green Belt, Black Belt, and Master Black
                    Belt trainings are popular programs at QPS. Training is offered year-round through
                    monthly open enrollment.</font>
                <br /><br />
                <font style="125%" type="bold"><strong><u>About our Six Sigma Certification:</u></strong>
                </font>
                <br />
                <font style="font-size: 90%">Be ready to take new challenges in manufacturing, software
                    or service. Six Sigma prepares students to be experts in Quality, Business Improvement
                    and Project Management. Jay P. Patel, President and CEO of Quality and Productivity
                    Solutions, Inc., serves as instructor at the QPS Institute. A Master Black Belt,
                    he is founder of the QPS Institute and one of the contributing authors to the famous
                    <u>Quality Council of Indiana’s Six Sigma Black Belt and Green Belt Primers recognized
                    nationally.</u> </font>
                  
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
              <font style="font-size: 90%"><a href="http://www.qpsinc.com/Training/Calendar.aspx?id=1">Click here to view dates/time of all QPS events.</a></font>
              <br />
                <b class="colbright">Contact</b><br />
                Marketing and Public Relations Division <br />
                Toll Free: 1 (877) 987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> 
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

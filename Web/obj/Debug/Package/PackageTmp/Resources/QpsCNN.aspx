<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QpsCNN.aspx.cs" Inherits="QPS.Resources.QpsCNN"
    MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentQpsCNN" ContentPlaceHolderID="cphContent" runat="server">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
    <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                QPS Student with Certifications on CCN!
            </h3>
            <br />
            <p style="font-size: 125%">
                <b class="colbright">April 13, 2010 �</b> <font style="font-size: 90%">
		     Stephen Udden, who recently completed six training certificates from Quality & Productivity Solutions, Inc., was interviewed by CNN for their news piece entitled China: Opportunity or Threat. <a target="_blank" href="http://amfix.blogs.cnn.com/2010/04/12/assessing-chinas-impact-on-america/?iref=obinsite">To view the original CNN video, click here.</a> The interview was aired on April 12th, 2010 on the American Morning show. <a target="_blank" href="../Documents/QPS on CNN photos.pdf">To view CNN�s photos of Steve and QPS�s certificates, click here.</a> QPS spoke with Steve, and he shared his story with us. <a target="_blank" href="../Documents/QPS News Flash on CNN.pdf">To read the QPS News Flash on Steve Udden, click here.</a><br />
            </font><br /><b class="colbright">About Training at Quality & Productivity Solutions, Inc.</b>
                <br />
                <font style="font-size: 90%">
		        QPS offers many Corporate Training Courses in the areas of Lean Six Sigma, Project Management, ISO, Supply Chain as well as other related disciplines. The courses are conducted at various locations; complete schedules are available upon request. QPS is also an approved training provider in the states of MA, RI, NY, NJ, RI CT, providing public training for unemployed personnel through the  One-Stop Career Center systems. As an approved training provider in these states, QPS gives the unemployed an opportunity to obtain funding from state and federal entities to pay for its courses and programs. Candidates that are interested in training programs can easily call their local career center and make an appointment with their career counselor to start the application process. Career centers are scattered generously throughout each state, and a significant amount of information regarding state and federal funding for education is also available online.<br />
            </font>
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

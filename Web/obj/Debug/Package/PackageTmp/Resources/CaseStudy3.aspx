<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CaseStudy3.aspx.cs" Inherits="QPS.Resources.CaseStudy3" MasterPageFile="~/QPSSiteHome.Master" %>

<asp:Content ID="ContentCaseStudy3" ContentPlaceHolderID="cphContent1" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("CaseStudy3");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	    <div class="span-4 blackbk last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a href="References.aspx" title="References">References</a></div>
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
		<div class="span-4 blackbl last"><a href="Releases.aspx" title="Press Releases">Press Releases</a></div>
		
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training" style="margin-left:280px;">Case Study 3
</h3>

<h3 class="colbright" id="H3_1" style="margin-left:250px;">Process Improvement


</h3>
	<p>
	    <div>
		    <span style="font-size:125%">
		        In manufacturing we struggle to improve our manufacturing process to improve our process capability. An example of process improvement is in the manufacturing of transmission gears. We started out with a pretty poor process with a Cpk .79 x 3 = 2.37 Sigma Level.
            <br /><br />
            <img src="../Images/chart1.JPG" />
            <br />
		    </span>
		</div>
		<div>
		    <span style="font-size:125%">
		       This process was not capable of meeting the customer specification and we had to have 100% inspection because this process will produce defects and we need to make sure the customer does not receive any defects. Which we did a pretty good job of screening out the defects. However even with 100% inspection some were sneaking through the inspection. 100% inspection is about 85% effective. So this project was to find out how to improve this process to a 6 sigma level so we could eliminate customer defects and the cost of the 100% inspection. Because of all the defects we found in inspection we spend a lot of money in scrap and rework. 
            <br /><br />
		    </span>
		    <span style="font-size:125%">
		       We started by looking at all the variables in the process and finding out why defects were happening. Part of this was have the machine manufacture come in and analyze the machine, and calibrating the adjustments and also providing us with some better machine training. 
            <br /><br />
                <img src="../Images/chart2.JPG" />
                <br />
		    </span>
		</div>
		<div>
		    <span style="font-size:125%">
		       Now we have a Cpk 1.25 or 3.75 Sigma. A good first step in our improvement. Next we did a study on some new inserts that the supplier said would give us better control and improve the performance of the machine. We did an experiment with 3 new type of inserts and saw some nice improvements, we picked the best insert from the study and performed a new analysis.
            <br /><br />
            <img src="../Images/chart3.JPG" />
            <br />
		    </span>
		</div>
		    <span style="font-size:125%">
		      The new process after improving the machine performance and after selecting a better insert now has a Cpk 1.99 or 5.97 or about 6 sigma. Now we can remove our 100% inspection and start using an Xbar & R control chart to keep it in control. The cost of the machine fixing and the new, more expensive, inserts is more than covered by the reduction in cost of the old process.
            <br />
		    </span>
		</p>
		
	</div>
	<div class="span-4">
	    <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space"><br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
<%--<table>
<tr>
<td valign="top" style="background-color:#d9d9d9;">
    <QPSSidebar:Resources id="Sidebar_Resources" runat="server"/>
    <QPSSidebar:Sidebar id="Sidebar" runat="server"/>
</td>
<td>
<table class="mainText" width='100%' ID="Table1">
    <tr class="Resources" style="HEIGHT: 2px;">
	    <td style="WIDTH: 7px;"><img src='..\Images\Clear.gif' width=7 height=2></td>
	    <td></td>
	    <td></td>
    </tr>
    <tr>
	    <td bgcolor='#1158E7' style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=5></td>
	    <td rowspan="3"><img src="../Images/resources_img.jpg"></td>
	    <td class="Resources" width="100%">Reference Materials</td>
    </tr>
    <tr>
	    <td class="ResourcesSpacer" style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=5></td>
	    <td class="ResourcesSpacer" style="padding-left: 20px; padding-right: 20px;">
	    <img src='..\Images\Clear.gif' width=5 height=65>
	    </td>
    </tr>
    <tr bgcolor="white" style="HEIGHT: 20px;">
	    <td style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=10></td>
	    <td></td>
    </tr>
</table>
<table class="mainText">
    <tr>
        <td>
            <%=Resource%>
        </td>
    </tr>
</table>
</td>
</tr></table>--%>
</asp:Content>

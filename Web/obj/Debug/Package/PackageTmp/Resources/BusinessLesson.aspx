<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BusinessLesson.aspx.cs"
    Inherits="QPS.Resources.BusinessLesson" MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentBusinessLesson" ContentPlaceHolderID="cphContent" runat="server">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%-- <div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Jay P. Patel, President & CEO, featured in Boston Business Journal�s Lessons Learned
                on <font style="font-style: italic">�For Entrepreneurs, Novel Advice Has a Lasting Impact.�</font>
            </h3>
            <p style="font-size: 125%; padding-left: 3px;">
                <b class="colbright">July 2, 2010 �</b> <font style="font-size: 90%">
		        Jay P. Patel, President and CEO of Quality & Productivity Solutions, Inc. was featured in Boston Business Journal�s Lessons Learned on �For Entrepreneurs, Novel Advice Has a Lasting Impact.�  The brief commentary featured a few entrepreneurs and their advice on maintaining a successful business. Mr. Patel�s advice focused on customer satisfaction and market needs. Moreover, he expressed that customer satisfaction is the most important part of running a successful business. <a target="_blank" href="http://www.bizjournals.com/boston/stories/2010/07/05/focus3.html">Click here to view the full article.</a><br />
            </font><br /><b class="colbright">About Jay P. Patel, President & CEO of Quality & Productivity
                Solutions, Inc.</b><br />
                <font style="font-size: 90%">
		        Jay P. Patel, President & CEO of Quality & Productivity Solutions, Inc. possesses over 25 years of professional work experience in management and quality assignments, including holding past positions as Project Engineer, Program Manager, Plant Manager, and Director of Corporate Quality. His professional work experience includes working at General Electric, Allied Signal-Bendix, United Technologies-Carrier, and Cabot Safety Corporation. He has provided consulting to many Fortune 500 clients and trained thousands of people. Currently, he is the President of Quality & Productivity Solutions, Inc. The company provides consulting, training and auditing services for ISO, Six Sigma, Lean, Quality Management System, Project Management, Supply Chain and other related topics.<br />
		        Jay has been the National Malcolm Baldrige Quality Award Examiner. He has also taught on site courses to several public and private companies. Courses that Jay has taught include Six Sigma, Lean, ISO, TS, and Project Management. Jay has presented speeches and seminars to various organizations such as Project Management Institute, American Production and Inventory Control, Institute of Industrial Engineers, American Society for Quality, and Society of Manufacturing Engineers. In addition, Jay has 10 ASQ Certifications including Six Sigma Certified Black Belt, Certified Quality Auditor. Jay�s educational achievements include a Bachelor in Engineering and two Master Degrees (one in Engineering from Fairleigh Dickinson University and one in Business Administration from the University of Bridgeport). He is a RAB-Quality System Lead Auditor and has taught an RAB-approved course for ISO 9001: 2000 Transition and Auditing. He has served as Chapter President of the Project Management Institute and is also a certified Project Management Professional. He has also served as Chapter President of Institute of Industrial Engineers.<br />
		        Jay has held number of positions at Section and Region levels within ASQ, including Worcester Section Chairman, Education and Certification Chair. Currently, Jay is the Chairman of ASQ North East Quality Council.
            </font>
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrainingRI.aspx.cs" Inherits="QPS.Resources.TrainingRI"  MasterPageFile="~/QPSSite.Master"%>

<asp:Content ID="ContentTrainingRI" ContentPlaceHolderID="cphContent" runat="server">

<div class="span-24 prepend-1 top highcont">
    <div class="span-19 top" style="float:right">
    <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>
		<div class="span-4 blackbl last"><a  href ="links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
	    <div class="span-4 blackbk last"><a title="Press Releases">Press Releases</a></div>
        <br class="clear"><br class="clear">
		<h3 class="colbright" id="training" >
		    QPS � Newly Approved Training Provider in Rhode Island!  
		</h3>
		<br />
		<p style="font-size:125%">
		    <b class="colbright">Providence, R.I. � January 27, 2010 �</b>
		    <font style="font-size:90%">
		     For years, Quality & Productivity Solutions, Inc. has provided corporate training all over the globe in addition to public training for unemployed personnel in the state of Massachusetts.<br /> As of today, QPS can now officially conduct public training for unemployed personnel in the state of Rhode Island.  Newly approved by the State Workforce Investment Office, QPS is now on the State of Rhode Island Department <br /> of Labor and Training�s Workforce Investment Act Eligible Training Provider list. <a target="_blank" href="http://www.dlt.ri.gov/wio/programs.htm">Click here to see the QPS on the <br /> State of RI�s Eligible Training Provider List website.</a>
		     <br />
            As an approved training provider in Rhode Island, QPS gives the unemployed an opportunity to obtain funding <br />from state and federal entities to pay for its Lean, Six Sigma and other well-known programs and courses.<br /> A total of 12 programs are approved in Rhode Island; many of these programs offer multiple certifications.<br /><a target="_blank" href="../Documents/RI Unemployed Course Matrix.pdf">Click here to view QPS�s RI approved training.</a><br />
           Interested candidates can easily call their local <a target="_blank" href="../Documents/State of RI Career Centers.pdf">NetworkRI One-Stop Career Center</a> and make an appointment with <br /> their career counselor to start the application process. Training is conducted in Warwick, Rhode Island at the <br /> Crowne Plaza Hotel which is located near T.F. Green Airport.
            </font> 
		</p>
		<div></div>
		<p style="font-size: 125%"><b class="colbright">Contact</b><br />
        Jay Patel<br />
        President & CEO<br />
        Toll Free: 1-877-987-3801<br/>
        <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
        </p>
    </div>
    
    <div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
</asp:Content>
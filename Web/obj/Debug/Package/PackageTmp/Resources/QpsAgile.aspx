<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QpsAgile.aspx.cs" Inherits="QPS.Resources.QpsAgile"
    MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentQpsAgile" ContentPlaceHolderID="cphContent" runat="server">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
    <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                QPS � Now Offering Agile!
            </h3>
            <br />
            <p style="font-size: 125%">
                <b class="colbright">June 1, 2011 �</b> <font style="font-size: 90%">
		    QPS is now offering Agile Certification Training.  This course follows PMI�s Body of Knowledge and has been designed to understand the Agile fundamentals, processes, tools/techniques.  It also prepares you for the PMI Agile Certified Practitioner (PMI-ACP)SM Exam.  Participants will be able to describe agile methodology and its practices, how to implement it successfully, apply its methods to rapidly changing requirements and priorities, recognize ways to increase team performance through better communication and close involvement of customers, use its techniques to estimate project work more accurately, identify and prevent common pitfalls that organizations encounter when adopting its practices and more.  Agile training is very popular and a course brochure can be viewed on the QPS website under Project Management Training.<br />
            </font>
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

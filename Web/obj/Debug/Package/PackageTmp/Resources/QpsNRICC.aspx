<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="QpsNRICC.aspx.cs"
    Inherits="QPS.Resources.QpsNRICC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%-- <div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
            <div class="span-3 blackbl last">
                <a href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <table width="100%">
                <tr>
                    <td style="vertical-align: bottom; padding-right: 7px;" width="75%">
                        <font class="colbright" style="font-size: 1.5em">QPS Now a Northern Rhode Island Chamber of Commerce Member</font>
                    </td>
                    <td align="left">
                        <img id="img1" src="../Images/MemberChamber.png" style="vertical-align: middle" />
                    </td>
                </tr>
            </table>
            <br />
            <p style="margin-left: 5px; font-size: 125%">
                <b class="colbright">August 26, 2010 </b> <font style="font-size: 90%">
                    QPS is now a member of the Northern Rhode Island Chamber of Commerce. 
                    <a href="http://norhodeislandricoc.weblinkconnect.com/chamber/Strategic-Goals.aspx" target="_blank">Click here to read about the Northern Rhode Island Chamber of Commerce.</a> 
                    Its mission is to strengthen the economic climate through business leadership that fosters member
                    and community prosperity. <a href="http://www.nrichamber.com/search/" target="_blank">Click here to view QPS in the membership directory.</a>
                </font>
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

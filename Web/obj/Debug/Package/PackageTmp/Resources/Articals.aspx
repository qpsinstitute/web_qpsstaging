﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Articals.aspx.cs" Inherits="QPS.Resources.Articals" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
<script language="javascript" type="text/javascript">
    setPage("Resources");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	     
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
		<div class="span-4 blackbl last"><a href="Releases.aspx" title="Press Releases">Press Releases</a></div>
		<%--<div class="span-4 blackbl last"><a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>--%>
        <div class="span-4 blackbl last"><a href="../Placement/Aboutus.aspx" title="Jobs">Staffing & Recruitment</a></div>
		<div class="span-3 blackbk last"><a title="Articals">Articals</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">Articals</h3>

		<ul class="arrowedbullet">
        <li><a href="../Documents/Article 7 principles of ISO 90012015.pdf" target="_blank" title="Seven principles of ISO 90012015">Seven principles of ISO 90012015</a></li>
        <li><a href="../Documents/February 2015.pdf" target="_blank" title="February 2015">February 2015</a></li>

        
		</ul>
		<p>
		    The articals is currently published in Adobe Acrobat .pdf format.
		    If you can't view this type of file, a free reader can be obtained by clicking
		    <a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank" class="nounder bold" title="Click here for Download Acrobat reader" >here</a>.
		</p>
	</div>
	<div class="span-4">
	    <img src="../images/articles.png" class="pull-1" alt="QPS Newsletters">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
</asp:Content>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QpsWorldCenter.aspx.cs"
    Inherits="QPS.Resources.QpsWorldCenter" MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentQpsCenter" ContentPlaceHolderID="cphContent" runat="server">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
    <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                QPS opens World Class Corporate Training Center, <font style="font-style: italic;">QPS Institute</font>
            </h3>
            <br />
            <p style="font-size: 125%">
                <b class="colbright">June 8, 2010 </b><font style="font-size: 90%">- Quality & Productivity Solutions  opens the Quality & Productivity Institute, a world class corporate training center located outside of Boston. The announcement came because for years QPS always conducted its training at the Royal Plaza Hotel.�QPS has been growing and I am so pleased to offer training world class style in such a beautiful environment,� 
                said Jay P. Patel, QPS President & CEO. <a href="../Documents/World Class Training Center Official Press Release.pdf" target="_blank">Click here to view the Original Press Release.</a> 
                <br />
                The Quality & Productivity Institute includes rooms with state of the art technology.There are three large rooms: two of 
                the rooms have lightweight and modern tables and chairs that can be arranged in conference lecture style or u-shape  
                layout; the third room, located adjacent to the entrance, is a conference-type room that is ideal for  meetings and small 
                corporate gatherings.The conference room features a beautiful, largely extended mahogany table with chairs arranged in formal boardroom style seating. <a target="_blank" href="../Documents/World Class Training Center Photos.pdf">Click here to view the Photo Gallery.</a>
                QPS invites  you to join them as they celebrate <br /> an Open House at the newly opened Quality & Productivity Institute, located at 225 Cedar Hill, Marlborough, MA 01572. <a target="_blank" href="../Documents/World Class Training Center Open House Invite.pdf">Click here to view the Open House Invite.</a><br />
            </font><br /><b class="colbright">About Quality & Productivity Solutions, Inc.</b>
                <br />
                Quality & Productivity Solutions, Inc. is an international firm dedicated to providing
                practical and effective consulting
                <br />
                and training to achieve its client's objectives. QPS partners with its customers
                to understand and assess their needs, developing effective strategies and solutions
                for them. Quality & Productivity Solutions training has grown tremendously over
                the years, and with the newly opened Quality & Productivity Institute, there is
                an even greater opportunity to experience sophisticated training, world class style.
                The Quality & Productivity Institute is officially licensed in the state of Massachusetts
                as a Private Occupational School, offering certifications to hundreds of people
                in the areas of Lean, Six Sigma, Project Management, Supply Chain, ISO, and other
                related topics.
                <br />
                <a target="_blank" href="http://www.qpsinc.com/Training/Calendar.aspx">Click here to
                    view QPS Training Certifications.</a>
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

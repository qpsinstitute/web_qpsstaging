<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="casestudy.aspx.cs" Inherits="QPS.Resources.casestudy" MasterPageFile="~/QPSSiteHome.Master" %>

<asp:Content ID="Contentcasestudy" ContentPlaceHolderID="cphContent1" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("casestudy");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	    <div class="span-4 blackbk last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a href="References.aspx" title="References">References</a></div>
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
		<div class="span-4 blackbl last"><a href="Releases.aspx" title="Press Releases">Press Releases</a></div>
		
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">Case Studies</h3>
		<table border="0">
            
                <tr style="vertical-align: top;">
                    <td style="vertical-align: top; font-weight:bold; width:100px;">Case Study 1</td>
                    <td valign="top"><a href="../Resources/CaseStudy1.aspx" style="text-decoration: none; font-style:italic;" target="_blank"> Healthcare Claims Processing</a>
                    </td>
                </tr>
                 <tr style="vertical-align: top;">
                    <td style="vertical-align: top; font-weight:bold;">Case Study 2</td>
                    <td valign="top"><a href="../Resources/CaseStudy2.aspx" style="text-decoration: none; font-style:italic;" target="_blank"> Insurance Application Process</a>
                    </td>
                </tr>
                  <tr style="vertical-align: top;">
                    <td style="vertical-align: top; font-weight:bold;">Case Study 3</td>
                    <td valign="top"><a href="../Resources/CaseStudy3.aspx" style="text-decoration: none; font-style:italic;" target="_blank"> Process Improvement</a>
                    </td>
                </tr>
                  <tr style="vertical-align: top;">
                    <td style="vertical-align: top; font-weight:bold;">Case Study 4</td>
                    <td valign="top"><a href="../Resources/CaseStudy4.aspx" style="text-decoration: none; font-style:italic;" target="_blank"> Service Industry</a>
                    </td>
                </tr>
		</table>
		
	</div>
	<div class="span-4">
	    <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space"><br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
<%--<table>
<tr>
<td valign="top" style="background-color:#d9d9d9;">
    <QPSSidebar:Resources id="Sidebar_Resources" runat="server"/>
    <QPSSidebar:Sidebar id="Sidebar" runat="server"/>
</td>
<td>
<table class="mainText" width='100%' ID="Table1">
    <tr class="Resources" style="HEIGHT: 2px;">
	    <td style="WIDTH: 7px;"><img src='..\Images\Clear.gif' width=7 height=2></td>
	    <td></td>
	    <td></td>
    </tr>
    <tr>
	    <td bgcolor='#1158E7' style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=5></td>
	    <td rowspan="3"><img src="../Images/resources_img.jpg"></td>
	    <td class="Resources" width="100%">Reference Materials</td>
    </tr>
    <tr>
	    <td class="ResourcesSpacer" style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=5></td>
	    <td class="ResourcesSpacer" style="padding-left: 20px; padding-right: 20px;">
	    <img src='..\Images\Clear.gif' width=5 height=65>
	    </td>
    </tr>
    <tr bgcolor="white" style="HEIGHT: 20px;">
	    <td style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=10></td>
	    <td></td>
    </tr>
</table>
<table class="mainText">
    <tr>
        <td>
            <%=Resource%>
        </td>
    </tr>
</table>
</td>
</tr></table>--%>
</asp:Content>
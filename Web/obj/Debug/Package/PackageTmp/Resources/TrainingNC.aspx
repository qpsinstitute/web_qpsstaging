<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrainingNC.aspx.cs" Inherits="QPS.Resources.TrainingNC" MasterPageFile="~/QPSSite.Master" %>
<asp:Content ID="ContentTrainingNC" ContentPlaceHolderID="cphContent" runat="server">

<div class="span-24 prepend-1 top highcont">
    <div class="span-19 top" style="float:right">
        <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>
		<div class="span-4 blackbl last"><a  href ="links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
	    <div class="span-4 blackbk last"><a title="Press Releases">Press Releases</a></div>
        <br class="clear"><br class="clear">
		<h3 class="colbright" id="training" >
		    QPS � Recently Gained Training Provider Approval in North Carolina!        
		</h3>
		<br />
		<p style="font-size: 125%">
		    <b class="colbright">Charlotte, N.C. � May 1, 2010 �</b>
		    <font style="font-size:90%">
		     Since the April 2010 Quarterly Newsletter, QPS has decided to continue its <br />local expansion project to obtain Training Provider approval and make out of state training available to out of<br /> state clientele. This initiative has met with great success! For years, QPS has provided corporate training all<br /> over the globe, and has offered public training for unemployed personnel in the state of Massachusetts. Earlier<br /> this year, QPS became an approved Training Provider in the states of Rhode Island and Connecticut, providing <br /> public training for unemployed personnel through the NetworkRI and CTWorks, the states� local One-Stop Career Center systems. Effective today, QPS gained approval as a Training Provider in the state of North Carolina,<br /> where it is listed on the North Carolina State Training Accountability and Reporting System, also known as the NCSTARS database. <a target="_blank" href="http://www.ncstars.org/">Click here to view QPS on the NC Training List. </a><br />
            As an approved training provider in North Carolina, QPS gives the unemployed an opportunity to obtain funding<br /> from state and federal entities to pay for Lean, Six Sigma, Project Management, and other well-known programs <br />and courses. A total of twenty-five were approved, and of that twenty five, 1/3 or more are programs that offer <br /> at least two certificates. <a target="_blank" href="../Documents/NC Unemployed  Course Matrix July 12 2010.pdf">Click here to view QPS�s NC approved training.</a><br />
            Interested candidates can easily call their <a target="_blank" href="../Documents/NC Career Centers.pdf">NCJobLink One-Stop Career Center</a> and make an appointment with<br /> their career counselor to start the application process. Training is conducted at the Suburban Extended Stay<br /> Hotel, 10225 Feld Farm Lane in Charlotte, located near the Charlotte Douglas International Airport.
            </font> 
		</p>
		<div></div>
		<p style="font-size: 125%"><b class="colbright">Contact</b><br />
        Jay Patel<br />
        President & CEO<br />
        Toll Free: 1-877-987-3801<br/>
        <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
        </p>
    </div>
    
    <div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>

</asp:Content>



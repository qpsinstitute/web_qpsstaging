<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QpsRating.aspx.cs" Inherits="QPS.Resources.QpsRating"
    MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentQpsRating" ContentPlaceHolderID="cphContent" runat="server">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <%--<div class="span-4 blackbl last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a  href="References.aspx" title="References">References</a></div>--%>
            <div class="span-4 blackbl last">
                <a href="links.aspx" title="Links">Links</a></div>
            <div class="span-4 blackbl last">
                <a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
            <div class="span-4 blackbk last">
                <a title="Press Releases">Press Releases</a></div>
            <div class="span-4 blackbl last">
                <a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training" style="height: 50px;">
                <table width="100%">
                    <tr>
                        <td style="vertical-align: bottom" width="69%">
                            QPS Accredited with A+ Rating by the Better Business Bureau!
                        </td>
                        <%--<td align="left">
                            <img src="../Images/bbb1.jpg" id="img1" style="vertical-align: middle" />
                        </td>--%>
                    </tr>
                </table>
            </h3>
            <p style="font-size: 125%; padding-left: 3px;">
                <b class="colbright">June 29, 2010 �</b> <font style="font-size: 90%">Effective today, QPS has met the standards of the Better Business Bureau and earned an A+ rating! QPS is now listed in the BBB�s Accredited Business Directory. The BBB�s directory is a trusted source of business listings that have met BBB�s professional standard, and a well known resource for consumers who want to confidently select a professional company with which to do business. QPS is proud to display the BBB�s accredited business seal on its website.  
             <a target="_blank" href="http://www.bbb.org/central-western-massachusetts/business-reviews/business-consultants/quality-and-productivity-solutions-in-oxford-ma-205649">Click here to view QPS on the BBB Accredited Business Directory.</a>
            </font>
            </p>
            <div>
            </div>
            <p style="font-size: 125%">
                <b class="colbright">Contact</b><br />
                Jay Patel<br />
                President & CEO<br />
                Toll Free: 1-877-987-3801<br />
                <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a><br />
                <a href="http://www.qpsinc.com">www.qpsinc.com</a><br />
            </p>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

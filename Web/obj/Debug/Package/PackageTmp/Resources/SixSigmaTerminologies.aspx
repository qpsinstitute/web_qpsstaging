<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="SixSigmaTerminologies.aspx.cs" Inherits="QPS.Resources.SixSigmaTerminologies" %>
<%--<%@ Register TagPrefix="QPSSidebar" TagName="Resources" Src="~/Controls/Sidebar_Resources.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>--%>

<asp:Content ID="ContentSixSigmaTerminologies" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("Resources");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
        <div class="span-3 blackbk last"><a href="references.aspx" title="References">References</a></div>
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">References</h3>

		<h4 class="colblu mostwrap bold">Six Sigma Tools &amp; Terminologies</h4>

<hr class="space">

        <p><b><span style="color: navy;">ANOVA</span></b><span style="color: navy;"> -</span> Analysis of Variance  A method for identifying
differences in mean values using the variation of the measurements.</p>

<b><span style="color: navy;">Acceptance Region</span></b> 
The region of values for which the null hypothesis is accepted.</p>

<br><br><b><span style="color: navy;">Acceptance Sampling</span></b> 
refers to a sampling inspection (as opposed to 100% inspection) in which
decisions are made to accept or not to accept a product or service.</p>

<br><br><b><span style="color: navy;">Accuracy</span></b><span style="color: navy;"> </span> is a characteristic of measurement, which
addresses how close an observed value is to the true value. It answers the
question, Is it right?</p>

<br><br><b><span style="color: navy;">Affinity Diagram</span> </b> is
a management and planning tool used to organize ideas into natural groupings in
a way that stimulates new, creative ideas.</p>

<br><br><b><span style="color: navy;">Alpha Risk</span></b>  The
probability of accepting the alternate hypothesis when, in reality, the null
hypothesis is true.</p>

<br><br><b><span style="color: navy;">Alternate Hypothesis</span></b>

- A tentative explanation, which indicates that an event does not follow a
chance distribution; a contrast to null hypothesis.</p>

<br><br><b><span style="color: navy;">Arrow Diagram</span></b>  is a
management and planning tool used to develop the best possible schedule and
appropriate controls to accomplish the schedule; the critical path method and
the program evaluation and review technique.</p>

<br><br><b><span style="color: navy;">Assignable Cause</span></b>  A
source of variation which is non random; a change in the source will produce a
significant change of some magnitude in the (dependent variable) response.</p>

<br><br><b><span style="color: navy;">Attribute Data</span></b>  are
data, which are countable, such as number of rejects or number of errors.</p>

<br><br><b><span style="color: navy;">Attribute Sampling Plan</span></b>
 is a plan that allows users to count the number of confirming or
non-confirming parts and look for defects. The four types of attribute plans
include single, double, multiple, and sequential.</p>

<br><br><b><span style="color: navy;">Background Variables</span> </b>
Variables that are of no experimental interest and are not held constant. Their
effects are often assumed insignificant or negligible, or they are randomized
to ensure that contamination of the primary response does not occur.</p>

<br><br><b><span style="color: navy;">Balance Design</span></b>  A
design with an equal number of experimental units in each treatment
combinations or run</p>

<br><br><b><span style="color: navy;">BB</span></b><span style="color: navy;"> </span> Black Belt  A Six Sigma project leader</p>

<br><br><b><span style="color: navy;">Beta Risk</span></b>  The
probability of accepting the null hypothesis when, in reality, the alternate
hypothesis is true.</p>

<br><br><b><span style="color: navy;">Black Belt Certification</span></b>
 Recognition obtained upon satisfactory completion of 1 major, or 2 small
projects.</p>

<br><br><b><span style="color: navy;">Block</span></b>  Group of
homogeneous experimental units.</p>

<br><br><b><span style="color: navy;">Blocking</span></b>  An
experiment in which the trials &amp;e made in some restricted order or under
restricted conditions.<span style="">&nbsp; </span>The experiment
is designed such that any nuisance factors do not confuse the true effects f
the factors of interest.</p>

<br><br><b><span style="color: navy;">Breakthrough</span></b>  is a
method of solving chronic problems, which results from the effective execution
of a strategy designed to reach the next level of quality. Such change often
requires a paradigm shift within the organization.</p>

<br><br><b><span style="color: navy;">C-Charts</span></b>  Charts,
which display the number of defects per sample.</p>

<br><br><b><span style="color: navy;">Capability</span></b>  The
ability of a process to stay within specifications and on target.</p>

<br><br><b><span style="color: navy;">Capability Ratio (Cp)</span></b>

 is equal to the specification tolerance width divided by the process
capability.</p>

<br><br><b><span style="color: navy;">Cause &amp; Effect Diagrams</span></b>
 A diagram to show the relationship between and effect (outcome) and its
possible causes.<span style="">&nbsp; </span>Often displayed with
five spines to group the potential causes as method, material, people,
environmental and machine.<span style="">&nbsp; </span>Asking five
whys helps to lead toward the root cause.</p>

<br><br><b><span style="color: navy;">Center points</span> </b>
Experimental runs with all factor levels set half way between the low and high
settings.<span style="">&nbsp; </span>Obviously can only be done
with quantitative factors.</p>

<br><br><b><span style="color: navy;">Central Tendency</span></b> 
Numerical average; e.g. mean, median, and mode; center line on a statistical
process control chart.</p>

<br><br><b><span style="color: navy;">Champion</span></b><span style="color: navy;"> </span> is an individual who has an accountability and
responsibility for many processes or who is involved in making strategic level
decisions for the organization. The champion ensures ongoing dedication of
project resources and monitors strategic alignment (also referred to as a
sponsor).</p>

<br><br><b><span style="color: navy;">Completely Randomized Design
(CRD)</span></b>  An experiment in which one factor of interest is
investigated (at multiple levels).<span style="">&nbsp;&nbsp; </span>The
trials are made in completely randomized order to limit the effect of
uncontrollable factors.</p>

<br><br><b><span style="color: navy;">Confidence Interval</span></b> 
the range of response values for which one is some percent confident that the
true average of the response will fall within that interval that percent of the
time.</p>

<br><br><b><span style="color: navy;">Confounding</span></b><span style="color: navy;"> </span> One or more effects that cannot unambiguously be
attributed to a single factor or interaction.</p>

<br><br><b><span style="color: navy;">Confounded Effects</span></b> 
Effects, which cannot be estimated independently of each other.</p>

<br><br><b><span style="color: navy;">Consumers Risk</span></b>  for
a sampling plan refers to the probability of acceptance of a lot, the quality
of which is designated numerical value representing a level that is seldom
desirable. Usually the designated value will be the limiting quality level.</p>

<br><br><b><span style="color: navy;">Continuous Probability
Distribution</span></b>  means that the greatest number of observations fall
in the center with fewer and fewer observations falling on either side of the
average, forming a normal bell-shaped curve.</p>

<br><br><b><span style="color: navy;">Control
Chart</span></b><span style="">  A methodology for
identifying when a process is operating in Control (within known statistical
boundaries).</span></p>

<br><br><b><span style="color: navy;">Correlation</span></b><span style="">  refers to the measure of the relationship
between two sets of numbers or variables.</span></p>

<br><br><b><span style="color: navy;">Correlation
Coefficient</span></b><span style="">  describes the
magnitude and direction of the relationship between two variables.</span></p>

<br><br><b><span style="color: navy;">Cost
Curve</span></b><span style="">  is a model, which
shows how developments in new technology, automation and other areas have
resulted in the ability to achieve perfection at finite costs.</span></p>

<br><br><b><span style="color: navy;">Cp</span></b><b><span style=""> </span></b><span style=""> Potential Capability Index  Cp = Tolerance /6s.</span></p>

<br><br><b><span style="color: navy;">Cpk</span></b><span style="">  Performance Capability Index  Cpk = min of
(USL  mean) or (mean  LSL)/3s.</span></p>

<br><br><b><span style="color: navy;">Cross
Functional Team</span></b><span style="">  is a group
organized by management and drawn from a variety of functional areas whose
responsibility is to identify, analyze and solve chronic problems that are
beyond the scope of a quality circles effort.</span></p>

<br><br><b><span style="color: navy;">CTQ</span></b><b><span style=""> </span></b><span style=""> Critical to Quality <b><u></u></b></span></p>

<br><br><b><span style="color: navy;">CTQ
4-Block</span></b><span style="">  Powerful device
within the Jugular process for analyzing most critical parameters/process steps
vs. current knowledge.</span></p>

<br><br><b><span style="color: navy;">CTQ
Flowdown</span></b><span style="">  A very rigorous
methodology for allocating requirements and assessing capabilities of the most
critical segments of a product prior to M1.</span></p>

<br><br><b><span style="color: navy;">CTQ
Jugular</span></b><span style="">  A process of
structured and rigorous brainstorming and identification of parameters that the
most r\critical together with assessment of current knowledge about the
process, culminating in a monitoring devise for maintained success.</span></p>

<br><br><b><span style="color: navy;">Cycle
Time</span></b><b><span style=""> </span></b><span style=""> refers to the time that it takes to complete
a process from beginning to end and is a critical MBNQA criterion.</span></p>

<br><br><b><span style="color: navy;">Data</span></b><span style="color: navy;"> </span><span style=""> Factual information used as a basis for reasoning, discussion, or calculation;
often refers to quantitative information.</span></p>

<br><br><b><span style="color: navy;">Degrees
of Freedom</span></b><span style="">  Values used in
the analysis of variance.<span style="">&nbsp; </span>The number of
independent pieces of information used to estimate the variability of a factor.</span></p>

<br><br><b><span style="color: navy;">Degrees
of Freedom for Error</span></b><span style="">  Values
used in the analysis of variance to estimate the process noise.<span style="">&nbsp; </span>Without a good estimate of the process
noise, determination of which factors are significant and to what degree may be
fruitless.<span style="">&nbsp; </span>A rule f thumb is 5 degrees
of freedom for error at a minimum.<span style="">&nbsp; </span>This
can equate to at least six replicates.</span></p>

<br><br><b><span style="color: navy;">Defect</span></b><span style="">  Any parameter identified to be evaluated to
a given standard, which fails to meet that standard.</span></p>

<br><br><b><span style="color: navy;">Defective
(part)</span></b><span style=""> A part identified to
be evaluated to a given standard, which fails to meet any portion of that
standard.<span style="">&nbsp; </span>A single defective part could
have multiple defects.</span></p>

<br><br><b><span style="color: navy;">Demographics</span></b><span style="color: navy;"> </span><span style=""> are variables among buyers in the consumer market, which include
geographic location, age, sex, marital status, family size, social class,
education, nationality, occupation and income.</span></p>

<br><br><b><span style="color: navy;">Density
Function</span></b><span style="color: navy;"> </span><span style=""> The function that yields the probability
that a particular random variable takes on any one of its possible values.</span></p>

<br><br><b><span style="color: navy;">DFT</span></b><b><span style=""> </span></b><span style=""> Demand Flow Technology  Materials Management methodology that assures
adequate, but not excessive availability of material at the specific time of
the need  neither early nor late.</span></p>

<br><br><b><span style="color: navy;">Discrete
Probability Distribution</span></b><b><span style=""> </span></b><span style=""> means that the measured process variable
takes on a finite or limited number of values; no other possible values exist.</span></p>

<br><br><b><span style="color: navy;">Discrete
Random Variable</span></b><span style="">  A random
variable that can assume values only from a definite number of discrete values.</span></p>

<br><br><b><span style="color: navy;">Distributions</span></b><span style="">  Tendency of large number of observations to
group themselves around some central value with a certain amount of variation
on either side.</span></p>

<br><br><b><span style="color: navy;">DOA 
Dead on Arrival</span></b><b><span style=""> </span></b><span style="">Product, which will not work upon customers
receipt.<b><span style="">&nbsp; </span></b></span></p>

<br><br><b><span style="color: navy;">DoE</span></b><span style="color: navy;"> </span><span style=""> Design of Experiments Any of a class of matrices (usually orthogonal)
used to understand high-contribution factors. Most often associated with
factorial designs.</span></p>

<br><br><b><span style="color: navy;">DPU
Defects Per Unit</span></b><span style="">  #defects
found / # Total units physically evaluated.</span></p>

<br><br><b><span style="color: navy;">DPPM </span></b><span style=""> Defective Parts Per Million (outside of
specification)  (# defective units /#total units x 1,000,000.</span></p>

<br><br><b><span style="color: navy;">Duncans
Method</span></b><span style="">  a statistical method
used to determine which levels of a factor cause a change in the response.<span style="">&nbsp; </span>Used only after the ANOVA indicates a
difference among all levels.</span></p>

<br><br><b><span style="color: navy;">Effect</span></b><span style="">  the average change in the response when a
factor is changed from a low level to a high level.</span></p>

<br><br><b><span style="color: navy;">Error</span></b><span style="">  the inherent variability in a process.<span style="">&nbsp; </span>Represents the change in a response when no
change in the factor is made.<span style="">&nbsp; </span>See
noise.</span></p>

<br><br><b><span style="color: navy;">Estimate</span></b><span style="">  a prediction of some response based on the
level of impact of all factors in a process.<span style="">&nbsp;
</span>See prediction.</span></p>

<br><br><b><span style="color: navy;">EVOP</span></b><span style="">  Evolutionary Operation  A method of
conducting designed experiments on an ongoing process without interrupting
affecting its efficiency.</span></p>

<br><br><b><span style="color: navy;">EWMA</span></b><b><span style=""> </span></b><span style=""> Exponentially Weighted Moving Average  a control charting methodology
that utilizes historical data at an exponentially diminishing weighted value.</span></p>

<br><br><b><span style="color: navy;">Experimental
Region</span></b><span style="">  All possible
factor-level combinations for which experimentation is possible.<span style="">&nbsp; </span>Also known as Factor Space</span></p>

<br><br><b><span style="color: navy;">Experimental
Unit</span></b><span style="">  The unit that is
observed and measured during the experiment.<span style="">&nbsp;

</span>Also known as unit of analysis.</span></p>

<br><br><b><span style="color: navy;">External
Failure Costs</span></b><span style="">  are costs
associated with defects found after the customer receives the product or
service. </span></p>

<br><br><b><span style="color: navy;">F Test</span></b><span style="">  A statistical test to determine if a
difference exists between two variances.</span></p>

<br><br><b><span style="color: navy;">Factor</span></b><span style="">  an input to a process, which can be changed
during experimentation. <span style="">&nbsp;</span>Can qualitative
(e.g. type of additive) or quantitative (e.g. temperature, pressure)</span></p>

<br><br><b><span style="color: navy;">Factor
Analysis</span></b><span style="">  is a statistical
technique that examines the relationships between a single dependent variable
and multiple independent variations. For example, it is used to determine which
questions on a questionnaire are related to specific question such as, Would
you buy this product again?</span></p>

<br><br><b><span style="color: navy;">Factor,
Fixed</span></b><span style="">  If factor levels are
specifically assigned, the factor is said to be fixed. Inferences generalize to
only those factors.<span style="">&nbsp; </span>Effects are of
interest. </span></p>

<br><br><b><span style="color: navy;">Factor,
Monitored</span></b><span style="">  a factor (usually
uncontrollable and hence cannot be held constant) that is observed throughout
the experiment and can possibly be correlated to part of the unexplained
variation in the process.</span></p>

<br><br><b><span style="color: navy;">Factor,
Nuisance</span></b><span style="">  a factor that is
known to cause variability in the process; it is not desired to investigate the
factor, but rather not to have the factor influence the effect of the factor of
interest.<span style="">&nbsp; </span>See blocking.</span></p>

<br><br><b><span style="color: navy;">Factor
Random</span></b><span style="">  If factor levels are
selected randomly from a population of values, the factor is said to be
Random.<span style="">&nbsp; </span>Variance components are of
interest.</span></p>

<br><br><b><span style="color: navy;">Full
Factorial Experiment</span></b><span style="">  A
class of DoE where 2 levels of several variables are only partially
explored.<span style="">&nbsp; </span>Used to screen out Trivial many
and allow focus on vital few variables controlling the process.</span></p>

<br><br><b><span style="color: navy;">Fixed
Effects Factor</span></b><span style="">  a factor for
which the levels are chosen selectively.<span style="">&nbsp;
</span>For example, the effect of temperature will be investigated at 400, 450
and 500 degrees (Compare with Random effects Factor).</span></p>

<br><br><b><span style="color: navy;">Fluctuations</span></b><span style="">  Variances in data, which are caused by a
large number of minute variation differences.</span></p>

<br><br><b><span style="color: navy;">Fractional
2k Designs</span></b><span style="">  all factors are
run at a low level and high level (see Fractional Factorial Designs).</span></p>

<br><br><b><span style="color: navy;">Fractional
3K Designs</span></b><span style="">  all factors are
run at three levels: a low, medium and high (see Fractional Factorial Designs).</span></p>

<br><br><b><span style="color: navy;">Frequency
Distribution</span></b><span style="">  The pattern or
shape formed by the group of measurements in a distribution.</span></p>

<br><br><b><span style="color: navy;">Gage
R&amp;R</span></b><span style="">  Gage Repeatability
and Reproducibility.<span style="">&nbsp; </span>An analysis of the
percent of total variation of a distribution that can be attributed to
variation in the measurement system. </span></p>

<br><br><b><span style="color: navy;">Gage
Repeatability</span></b><span style="">  Variation in
the measurements obtained when one operator uses the same gage for measuring
the identical characteristics of the same parts.</span></p>

<br><br><b><span style="color: navy;">Gage
Reproducibility</span></b><span style="">  Variation
in the average of measurements made by different operators using the same gage
when measuring identical characteristics of the same parts.</span></p>

<br><br><b><span style="color: navy;">Gantt
Chart</span></b><span style="">  is a project
management technique by which the activities of a project are displayed
graphically and in sequential order and are plotted against time.</span></p>

<br><br><b><span style="color: navy;">Gap
Analysis</span></b><span style="">  is a technique
that compares a companys existing state to its desired state (as expressed by
its long term plans) and determines what needs to be done to remove or minimize
the gap.</span></p>

<br><br><b><span style="color: navy;">GLM</span></b><b><span style=""> </span></b><span style=""> General Liner Model  A form of ANOVA that allows for a small degree
of unbalance in the experimental design.</span></p>

<br><br><b><span style="color: navy;">Goal</span></b><span style="">  is a non-quantitative statement of general
intent, aim, or desire; it is the end point toward which management directs its
efforts and resources.</span></p>

<br><br><b><span style="color: navy;">Graeco</span></b><span style="">  Latin-Square Design  an experimental
design in which one factor of interest is investigated and three nuisance
factors are blocked against.</span></p>

<br><br><b><span style="color: navy;">HALT</span></b><span style="">  Highly Accelerated Life Testing  One of
several methods for achievement of a reliable design.<span style="">&nbsp; </span>Concept is to test a product to extreme (failure) conditions,
find root cause of failure, improve design, and repeat the process.</span></p>

<br><br><b><span style="color: navy;">Hetero-skedasticity
unequal variances</span></b><span style="">  This
condition when applied to factor levels may affect the conclusion from ANOVA.</span></p>

<br><br><b><span style="color: navy;">Histogram</span></b><span style="color: navy;"> </span><span style=""> A bar chart to show the distribution of the collected data.</span></p>

<br><br><b><span style="color: navy;">Hoshin
Planning</span></b><span style="">  is a methodology
for organizing and focusing enterprise efforts on critical issues impacting its
success.</span></p>

<br><br><b><span style="color: navy;">Hyper-Graeco-Latin
Square Design</span></b><span style="">  an
experimental design in which one factor of interest is investigated and four
nuisance factors are blocked against.</span></p>

<br><br><b><span style="color: navy;">Hypothesis</span></b><span style="">  an assertion that is tested using a
statistical technique.<span style="">&nbsp; </span>The hypothesis
will either be rejected or insufficient evidence will be available to reject.</span></p>

<br><br><b><span style="color: navy;">Interrelationship
Diagram</span></b><b><span style=""> </span></b><span style=""> is a management and planning tool that
displays the relationship between factors in a complex situation. It identifies
meaningful categories from a mass of ideas and is useful when relationship is
difficult to determine.</span></p>

<br><br><b><span style="color: navy;">Interaction</span></b><span style="">  a condition in which the effect of the
level of a factor on a response is different for a different levels of a second
factor.<span style="">&nbsp; </span>There are two-way interactions,
three-way interactions, etc.</span></p>

<br><br><b><span style="color: navy;">Instability</span></b><span style="">  Unnaturally large fluctuations in a
pattern.</span></p>

<br><br><b><span style="color: navy;">Interval</span></b><span style="">  Numeric categories with equal unit of
measure by no absolute zero point, i.e. quality scale index.</span></p>

<br><br><b><span style="color: navy;">IX-MR 
Individual X and Moving Range</span></b><span style="">
 a control chart of sequential data points, together with a chart of ranges
between points. </span></p>

<br><br><b><span style="color: navy;">Just-in-time
Training</span></b><span style="">  is training that
is offered to employees, as it is needed, so that employees will be able to use
their new skills immediately after training.</span></p>

<br><br><b><span style="color: navy;">Kano
Model</span></b><span style="">  is a representation
of the three levels of customer satisfaction defined as dissatisfaction,
neutrality, and delight.</span></p>

<br><br><b><span style="color: navy;">Latin
Square Design (LSD)</span></b><span style="">  an
experimental design in which one factor of interest is investigated and two
nuisance factors are blocked against.</span></p>

<br><br><b><span style="color: navy;">Line
Charts</span></b><span style="">  Charts used to track
the performance without relationship to the process capability or control
limits.</span></p>

<br><br><b><span style="color: navy;">Level</span></b><span style="color: navy;"> </span><span style=""> A setting or value of a factor.<span style="">&nbsp;

</span>Can be qualitative (e.g. additive A and additive B) or qualitative (e.g.
1000psi, 2000 psi.)</span></p>

<br><br><b><span style="color: navy;">Main
Effect</span></b><span style="">  The change in
response that occurs when a factor is changed from its low level to its high
level.</span></p>

<br><br><b><span style="color: navy;">Matrix
Chart</span></b><span style="">  is a management and
planning tool that shows the relationship among various groups of data; it
yields information about the relationships and the importance of task/method
elements of the subjects.</span></p>

<br><br><b><span style="color: navy;">MBB</span></b><span style="">  Master Black Belt  A Six Sigma trainer and
project mentor.</span></p>

<br><br><b><span style="color: navy;">MBQNA 
Malcom Baldrige National Quality Award</span></b><span style="color: navy;"> </span><span style=""> is an award
that recognizes American companies for business performance excellence and
quality achievements. The award criterions describe a total quality management
system and include an approach, deployment characteristics, and results that
can be applied to the development of quality system.</span></p>

<br><br><b><span style="color: navy;">Mean</span></b><span style="">  Measure of central tendency of a variable 
the first moment around the origin.</span></p>

<br><br><b><span style="color: navy;">Mean
Square</span></b><span style="">  a column in the
ANOVA table that represents the variance of response due to different sources
of variability.</span></p>

<br><br><b><span style="color: navy;">Mean
Square Error</span></b><span style="">  an entry in
the ANOVA table that represents the variance of a response at a given levels of
all factors.<span style="">&nbsp; </span>An estimate of the
variance of a response due to noise (error).</span></p>

<br><br><b><span style="color: navy;">Median</span></b><span style="color: navy;"> </span><span style=""> is the middle number or the center value of a set of data when all the
data are arranged in an increasing sequence.</span></p>

<br><br><b><span style="color: navy;">Mode</span></b><span style="">  is the score that occurs most frequently in
the data.</span></p>

<br><br><b><span style="color: navy;">Minitab</span></b><b><span style=""> </span></b><span style=""> GEs current statistical analysis software application of choice.</span></p>

<br><br><b><span style="color: navy;">Multiple
Comparison Procedure</span></b><span style="">  a
statistical method used to determine which levels of a factor cause a change in
the response.<span style="">&nbsp; </span>Used only after the ANOVA
Indicates a difference among all levels.<span style="">&nbsp;

</span>Examples are Fishers method, Duncans method, and Scheffes method. </span></p>

<br><br><b><span style="color: navy;">Multi-Vari
Analysis</span></b><span style="">  A graphical method
of decomposing the sources of process variation into their basic components. <span style="">&nbsp;</span>This technique is an early step in removing
some of the trivial many and preparing a sub-set of factors for designed
experimentation.</span></p>

<br><br><b><span style="color: navy;">Multivariate
Statistical Methods</span></b><span style=""> 
Statistical tools for analyzing a set of variables to determine their influence
on several responses.<span style="">&nbsp; </span>Includes a wide
class of statistical tools such as regression, principle components, factor
analysis, clustering, and discriminent analysis.</span></p>

<br><br><b><span style="color: navy;">Nominal
Group Technique</span></b><span style="">  is a
problem solving technique used to generate ideas related to a particular
subject. Team members write down their ideas individually and share them one at
a time. When all ideas are recorded, they are discussed and prioritized by the
group.</span></p>

<br><br><b><span style="color: navy;">Nested
Design</span></b><span style="">  an experimental
design in which a one factor has different level settings depending on the
level of another variable.<span style="">&nbsp; </span>For example,
batches within different suppliers, levels of competing additives, etc.</span></p>

<br><br><b><span style="color: navy;">Noise</span></b><span style="">  the inherent variability in a process.<span style="">&nbsp; </span>Represents the change in a response when no
change in the factor is made. See error.</span></p>

<br><br><b><span style="color: navy;">Nominal</span></b><span style="">  Unordered categories which indicate
membership or non-membership with no implication of quality, i.e. assembly area
number 1, part numbers, etc.</span></p>

<br><br><b><span style="color: navy;">Nonconformity</span></b><span style="">  A condition within a unit, which does not
confirm to some specific specification/standard and or requirement.</span></p>

<br><br><b><span style="color: navy;">Normal
Distribution</span></b><span style="">  a bell-shaped
curve of probabilities that describe many natural processes.<span style="">&nbsp; </span>Can occur also in situations in which
replicates are taken and are averaged </span></p>

<br><br><b><span style="color: navy;">Normal
Probability Plot</span></b><span style="">  a
graphical method for investigating whether a sample might have come from a
population with a normal distribution.<span style="">&nbsp;

</span>Often used to check the validity of using ANOVA.</span></p>

<br><br><b><span style="color: navy;">One-Way
ANOVA</span></b><span style="">  analysis of variance
for investigating a single factor at multiple levels. See ANOVA.</span></p>

<br><br><b><span style="color: navy;">Optimization</span></b><span style="">  Finding the combination of factors and
levels that produces the most desirable output from a process.</span></p>

<br><br><b><span style="color: navy;">Pareto
Chart</span></b><span style="">  A bar chart to
display events with respect to a common metric (#of times, $, time, etc.)</span></p>

<br><br><b><span style="color: navy;">Ordinal</span></b><span style="">  Ordered categories (ranking) with no
information about distance between each category.</span></p>

<br><br><b><span style="color: navy;">P Chart</span></b><span style="">  Charts used to plot percent defectives in a
sample.</span></p>

<br><br><b><span style="color: navy;">Parameter</span></b><span style="color: navy;"> </span><span style=""> A constant defining a particular property of the density function of a
variable.</span></p>

<br><br><b><span style="color: navy;">Pareto
Chart</span></b><span style="">  A bar chart to
display events with respect to a common metric</span></p>

<br><br><b><span style="color: navy;">Plackettt-Burman
Design</span></b><span style="">  a designed
experiment used in screening experimentation in which a minimal number of
trials are need.<span style="">&nbsp; </span>Typically only one
main effects are investigated with no estimate of the interaction effects.</span></p>

<br><br><b><span style="color: navy;">Plan-do-check-act
Cycle</span></b><span style="">  is a continuous
improvement model that teaches that organizations should plan an action, do it,
check to see how it confirms to plan and expectations, and act on what has been
learned.</span></p>

<br><br><b><span style="color: navy;">Point
Estimate</span></b><span style="">  the best single
value estimate of some prediction or mean response  should be used in
conjunction with confidence and/or prediction intervals.</span></p>

<br><br><b><span style="color: navy;">Poka 
Yoke</span></b><b><span style=""> </span></b><span style=""> is a term that means to foolproof the
process by building safeguards into the system that avoid or immediately find
errors.</span></p>

<br><br><b><span style="color: navy;">Population</span></b><span style="">  A group of similar items from which a
sample is drawn. Often referred to as the universe.</span></p>

<br><br><b><span style="color: navy;">Pre-control</span></b><span style="">  A methodology for establishing
statistically sound probabilities of goodness when a process is starting.</span></p>

<br><br><b><span style="color: navy;">PRD 
Phase Review Discipline</span></b><span style="">  A
rigorous methodology for new product introduction, which includes milestones at
critical points.</span></p>

<br><br><b><span style="color: navy;">Precision</span></b><span style="">  is a characteristic of measurement, which
addresses the consistency or repeatability of a measurement system when the
identical item is measured a number of times.</span></p>

<br><br><b><span style="color: navy;">Prediction</span></b><span style="color: navy;"> </span><span style=""> a best estimate of some response for a given set of levels for all
factors.</span></p>

<br><br><b><span style="color: navy;">Prediction
Interval</span></b><span style="">  the range of
values for a response of which one is some percent confident that a future
observation will fall within.<span style="">&nbsp; </span>See
Confidence Interval.</span></p>

<br><br><b><span style="color: navy;">Prevention
Costs</span></b><span style="color: navy;"> </span><span style=""> are costs incurred to keep internal and
external failure costs and appraisal costs to a minimum.</span></p>

<br><br><b><span style="color: navy;">Prioritization
matrix</span></b><span style="color: navy;"> </span><span style=""> is a management and planning tool used to
determine the highest-priority options/alternatives to accomplish an objective.</span></p>

<br><br><b><span style="color: navy;">Process
Capability</span></b><span style="">  refers to the
limits within which a tool or process operates, based upon minimum variability
as governed by the prevailing circumstances.</span></p>

<br><br><b><span style="color: navy;">Process
Decision Program Charts</span></b><span style="color: navy;"> <b>(PDPC)</b></span><span style="">  is a
management and planning tool that identifies all events that can go wrong and
the appropriate counter measures for these events. It graphically represents
all sequence that lead to a desirable effect.</span></p>

<br><br><b><span style="color: navy;">Process
Demographics</span></b><span style="">  The list of
conditions/states of various factors during the time a response is
generated.<span style="">&nbsp; </span>These help us to understand
what area of the process may be driving the problem.</span></p>

<br><br><b><span style="color: navy;">Process
Mapping</span></b><span style="">  is the flowcharting
of a work process.</span></p>

<br><br><b><span style="color: navy;">Producers
Risk</span></b><span style="">  for a sampling plan
refers to the probability of not accepting a lot, the quality of which has a
designated numerical value representing a level that is generally desirable.
Usually the designated value will be the acceptable quality level.</span></p>

<br><br><b><span style="color: navy;">Project
Life Cycle</span></b><span style="">  refers to the
four sequential phases of project management: conceptualization, planning,
implementation, and completion.</span></p>

<br><br><b><span style="color: navy;">Quality
</span></b><span style=""> denotes an excellence in
goods and services, especially to the degree they confirm to requirements and
satisfy customers.</span></p>

<br><br><b><span style="color: navy;">Quality
Control</span></b><span style="">  is the operational
techniques and activities that are used to fulfill requirements for quality and
is aimed at both monitoring a process and eliminating the causes of
unsatisfactory process.</span></p>

<br><br><b><span style="color: navy;">Quality
Function</span></b><b><span style=""> </span></b><span style=""> is the entire collection of activities
through which we achieve fitness-for-use, no matter where these activities are
performed.</span></p>

<br><br><b><span style="color: navy;">Quality
Function Deployment</span></b><span style="">  is a
process used to understand the voice of the customer and to translate customer
expectations into technical design parameters for each stage of the product development
cycle.</span></p>

<br><br><b><span style="color: navy;">Quality
System</span></b><b><span style=""> </span></b><span style=""> is the organizational structure, procedure,
processes, and resources needed to implement quality management.</span></p>

<br><br><b><span style="color: navy;">Random
Sampling</span></b><b><span style="">  </span></b><span style="">is a sampling method in which every element in
the population has an equal chance of being included.</span></p>

<br><br><b><span style="color: navy;">Random
Effects Factor</span></b><span style="">  a factor for
which the levels are chosen at random from a definable population.<span style="">&nbsp; </span>For example, the effect of batches will be
investigated by randomly choosing five batches.<span style="">&nbsp; </span>(Compare with Fixed Effects Factor).</span></p>

<br><br><b><span style="color: navy;">Randomization</span></b><span style="">  Mixing up the order of the runs in an
experiment as completely as is practical.</span></p>

<br><br><b><span style="color: navy;">Randomized
Block Design</span></b><span style="">  an experiment
in which one factor of interest is investigated and one nuisance factor is
blocked against.</span></p>

<br><br><b><span style="color: navy;">Regression
Analysis</span></b><span style="">  is a study used to
understand the relationship between two or more variables. Regression analysis
makes it possible to predict one variable from the knowledge about the other.</span></p>

<br><br><b><span style="color: navy;">Reliability</span></b><span style="color: navy;"> </span><span style=""> refers to the ability of a feedback instrument to produce the same
results over the repeated administration.</span></p>

<br><br><b><span style="color: navy;">Repetition</span></b><span style="">  Running several experimental unties over
one treatment combination. Contrast with Replication.</span></p>

<br><br><b><span style="color: navy;">Replication</span></b><span style="">  repeated runs at the same experimental
conditions; provides and estimate of the noise in the process.</span></p>

<br><br><b><span style="color: navy;">Residuals</span></b><span style="">  the difference between the observed
response and the predicted response for a given set of factor conditions.<span style="">&nbsp; </span>Used in model validation and process
investigation.</span></p>

<br><br><b><span style="color: navy;">Resolution</span></b><span style="">  a description of fractional factorial
designs that gives the degree to which factors will be aliased with other
factors interactions. </span></p>

<br><br><b><span style="color: navy;">Response</span></b><span style="">  an output from the process, which will be
measured during the experiment.</span></p>

<br><br><b><span style="color: navy;">Root
Cause Analysis</span></b><span style="">  is a quality
tool used to distinguish the source of defects or problems. It is a structured
approach that focuses on the decisive or original cause of a problem or
condition.</span></p>

<br><br><b><span style="color: navy;">RSM</span></b><span style="color: navy;"> </span><span style=""> Response Surface Methodology  a class of designed experiments where
curvature of the vital few is examined and understood.<span style="">&nbsp; </span>Subsets include central composite designs
with star or face points.</span></p>

<br><br><b><span style="color: navy;">R-Square</span></b><span style="">  percent of variability in the response
explained by the controlled factors.<span style="">&nbsp; </span></span></p>

<br><br><b><span style="color: navy;">Run</span></b><span style="">  a set of process conditions defined by
specifying levels of all the factors in the experiment. Also knows as a
Treatment Combination.</span></p>

<br><br><b><span style="color: navy;">Run
chart</span></b><span style="">  a sequential time
series plot of data, which provides some statistical analysis capabilities and
probabilities.</span></p>

<br><br><b><span style="color: navy;">Sample</span></b><span style="">  One or more observations drawn from a
larger collection of observations or universe.</span></p>

<br><br><b><span style="color: navy;">Scatter
Plot</span></b><span style="">  a chart (dot plot) to show
the relationship between two variables.</span></p>

<br><br><b><span style="color: navy;">SCBN 
Supplier Change Notice</span></b><span style="color: navy;"> </span><span style=""> The communication
device for requesting a change to a purchased part, initiated either by a
Supplier to GEMS, or by GEMS to a Supplier.</span></p>

<br><br><b><span style="color: navy;">Screening
Experiment</span></b><span style="">  a technique used
to characterize a process (usually assumes liner changes in the response for a
change of factor levels) (Compare with RSM).</span></p>

<br><br><b><span style="color: navy;">Signal
to Noise Ration</span></b><span style="">  a ratio
that depends on the variability in the response due to changing factor levels
relative to the variability when there is no change in the factor level.</span></p>

<br><br><b><span style="color: navy;">(Six)
6-s process</span></b><span style="">  a stable
process operating such that its output has minimum Cpk of 1.5.</span></p>

<br><br><b><span style="color: navy;">Skewness</span></b><span style="color: navy;"> </span><span style=""> a condition in which the normal distribution is shifted to the left or
right, that is, no longer symmetric.<span style="">&nbsp;
</span>Can influence the validity of using ANOVA.</span></p>

<br><br><b><span style="color: navy;">SPC </span></b><span style=""> Statistical Process Control  Used to
monitor process stability preferably after modification to desired state.</span></p>

<br><br><b><span style="color: navy;">Special
Cause of Variation</span></b><span style="">  are the
factors that disrupt the usual flow of work. Processes with special causes are
unstable and unpredictable.</span></p>

<br><br><b><span style="color: navy;">Standard
Deviation</span></b><b><span style=""> </span></b><span style=""> A statistical index of variability, which
describes the spread of the data.</span></p>

<br><br><b><span style="color: navy;">Statistical
Control</span></b><span style="">  A quantitative
condition, which describes a process free of assignable/special causes of
variation.</span></p>

<br><br><b><span style="color: navy;">Statistical
Process Control</span></b><span style="">  The
application of statistical methods and procedures relative to a process and
given set of standards.</span></p>

<br><br><b><span style="color: navy;">SWOT
Analysis</span></b><span style="">  is an assessment
of an organizations key strengths, weaknesses, opportunities, and threats. It
considers factors such as the organizations industry, the competitive
position, functional areas and management.</span></p>

<br><br><b><span style="color: navy;">T-Test</span></b><span style="">  A statistical comparison of man values of a
sample, assuming a normal population.</span></p>

<br><br><b><span style="color: navy;">Test of
Significance</span></b><span style="color: navy;"> </span><span style=""> A procedure to determine whether a quantity
subjected to random variation differs from a postulated value by an amount
greater than that due to random variation alone.</span></p>

<br><br><b><span style="color: navy;">Tree
Diagram</span></b><span style="">  is a management and
planning tool that shows the complete range of subtasks required to achieve an
objective. A problem solving method can be identified from this analysis.</span></p>

<br><br><b><span style="color: navy;">Trivial
Many</span></b><span style="">  The factors that have
long been thought to have some influence on the process but really account for
very little of the variation in performance.</span></p>

<br><br><b><span style="color: navy;">Total
Quality Management</span></b><b><span style=""> </span></b><span style=""> is a strategic, integrated management system
for achieving customer satisfaction, which involves all managers and employees
and uses quantitative methods to continuously improve an organizations process.</span></p>

<br><br><b><span style="color: navy;">Treatment
Combination</span></b><span style="">  a set of process
conditions defined by specifying levels of all the factors in the
experiment.<span style="">&nbsp; </span>Also known as a Run.</span></p>

<br><br><b><span style="color: navy;">Two-Way
ANOVA</span></b><span style="">  analysis of variance
for investigating two factors at multiple levels </span></p>

<br><br><b><span style="color: navy;">Two
way Interaction Plot</span></b><span style="">  a
scatter plot of the average responses (vertical axis) as a factor of one factor
(horizontal axis) and the average response for each level of the second factor
connected by lines.</span></p>

<br><br><b><span style="color: navy;">Type I
Error</span></b><span style="">  the error of assuming
that the hypothesis s false when in fact it is true.<span style="">&nbsp; </span>Associated probability is labeled a.</span></p>

<br><br><b><span style="color: navy;">Type II
Error</span></b><span style="">  the error of assuming
that the hypothesis is true when it is in fact false.<span style="">&nbsp; </span>Associated probability is labeled b.</span></p>

<br><br><b><span style="color: navy;">Unbalanced
Design</span></b><span style="">  A design with an
unequal number of experimental units in each treatment combinations or run.</span></p>

<br><br><b><span style="color: navy;">USL or
LSL</span></b><span style="">  Upper (Lower)
Specification Limit  boundaries of design criteria or statistical boundaries
of a control chart.</span></p>

<br><br><b><span style="color: navy;">UCL or
LCL</span></b><span style=""> - Upper (Lower) Control
Limit  the statistical boundaries of a control chart <b>OR</b> Upper (Lower)
Confidence Limit  (used in T-test, ANOVA, etc.).</span><span style="display: none;"></span></p>


<br><br><b><span style="color: navy;">Variable</span></b><span style="">  A characteristic that may take on different
values.</span></p>


<br><br><b><span style="color: navy;">Variance</span></b><b><span style=""> </span></b><span style=""> Provides a measure of dispersion.<span style="">&nbsp;
</span>The square root of the variance is the standard deviation.<span style="">&nbsp; </span>The 2</span><sup><span style="">nd</span></sup><span style=""> moment around the
mean.</span></p>

<br><br><b><span style="color: navy;">Variation</span></b><b><span style=""> </span></b><span style=""> Any quantifiable difference between individual measurements; such
differences can be classified as being due to common (random) or special
(assignable) causes.</span></p>

<br><br><b><span style="color: navy;">Vital
Few</span></b><span style="">  The factors that are
critical in controlling the process.</span></p>

<br><br><b><span style="color: navy;">Z</span></b><b><sub><span style="color: navy;">B</span></sub></b><b><span style="color: navy;"> Z benchmark, Z</span></b><b><sub><span style="color: navy;">st</span></sub></b><b><span style="color: navy;">, short-term sigma</span></b><span style="color: navy;"> </span><span style=""> Assumes process is centered (on target) with short term variation.</span></p>

<br><br><b><span style="color: navy;">Z value</span></b><span style="">  Calculation of how many sigmas fit between
the process output average and the closest specification limit.</span></p>
</div>

	<div class="span-4">
	    <img src="../images/references-01.gif" class="pull-1" alt="QPS References">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>

</div>

<%--<table>
<tr>
<td valign="top" style="background-color:#d9d9d9;">
    <QPSSidebar:Resources id="Sidebar_Resources" runat="server"/>
    <QPSSidebar:Sidebar id="Sidebar" runat="server"/>
</td>
<td>
<table class="mainText">
    <tr>
        <td>
            <%=SSTerminology%>
        </td>
    </tr>
</table>
</td>
</tr></table>--%>
</asp:Content>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CaseStudy2.aspx.cs" Inherits="QPS.Resources.CaseStudy2" MasterPageFile="~/QPSSiteHome.Master" %>

<asp:Content ID="ContentCaseStudy2" ContentPlaceHolderID="cphContent1" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("CaseStudy2");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	    <div class="span-4 blackbk last"><a href="casestudy.aspx" title="Case Studies">Case Studies</a></div>
        <div class="span-3 blackbl last"><a href="References.aspx" title="References">References</a></div>
		<div class="span-4 blackbl last"><a href="Links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
		<div class="span-4 blackbl last"><a href="Releases.aspx" title="Press Releases">Press Releases</a></div>
		
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training" style="margin-left:300px;">Case Study 2
</h3>

<h3 class="colbright" id="H3_1" style="margin-left:250px;">Insurance Application Process

</h3>
	<p>
		    <span style="font-size:125%">
		        The case of reviewing an application to work for an insurance organization that had to be approved by corporate. The project was to address many customer complaints about the process from the customers or agencies. Our first look at the process showed a number of things; a three month backlog, average turnaround time of 16 days, a 53% error rate and very dissatisfied customers. A value stream map was one of the first things to try and figure out what was happening, we had a lot of anecdotal information and everyone had an opinion of what was happening. The goal here is to convert this into statistical data that we could use to make good decisions.
            <br />            <br />
		    </span>
		    <span style="font-size:125%">
		       Mapping out the process and collecting factual data, helped us determine what needed to be done. First thing or our first Kaizen was to review the application itself, why do we have a 6 page application? We did some benchmarking of other companies and discovered they only used a one page application. Reviewing with legal and others to find out what we really needed, we ended up with one page. Most of the errors came from redundant information on the application and we sent the application back to the agency several times during the review process. We started a screen process to check the entire application to check everything that was needed and eliminated what was not needed and we eliminated the backlog in a couple of days. Then while we waited for the new application to be implemented we continued to screen the old application for missing information and had it completed up front and then put into the process. 
            <br />            <br />
		    </span>
		    <span style="font-size:125%">
		       After the new application was implemented worldwide we are now averaging 1.2 days turnaround time. As a result we reallocated half the staff to other areas that needed recourses.  Results included a new layout of the process, because people were walking 15 miles a month because of centralized printing 150 yards away from the process. They now have their own printer, fax machine and copier, saving thousands of dollars per month in wasted walking time.  Another result was a daily Kaizen to schedule their own work every day and to review problems from the day before, creating an improvement in morale.
            <br />
		    </span>
		</p>
		
	</div>
	<div class="span-4">
	    <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space"><br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
<%--<table>
<tr>
<td valign="top" style="background-color:#d9d9d9;">
    <QPSSidebar:Resources id="Sidebar_Resources" runat="server"/>
    <QPSSidebar:Sidebar id="Sidebar" runat="server"/>
</td>
<td>
<table class="mainText" width='100%' ID="Table1">
    <tr class="Resources" style="HEIGHT: 2px;">
	    <td style="WIDTH: 7px;"><img src='..\Images\Clear.gif' width=7 height=2></td>
	    <td></td>
	    <td></td>
    </tr>
    <tr>
	    <td bgcolor='#1158E7' style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=5></td>
	    <td rowspan="3"><img src="../Images/resources_img.jpg"></td>
	    <td class="Resources" width="100%">Reference Materials</td>
    </tr>
    <tr>
	    <td class="ResourcesSpacer" style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=5></td>
	    <td class="ResourcesSpacer" style="padding-left: 20px; padding-right: 20px;">
	    <img src='..\Images\Clear.gif' width=5 height=65>
	    </td>
    </tr>
    <tr bgcolor="white" style="HEIGHT: 20px;">
	    <td style="WIDTH: 5px;"><img src="..\Images\Clear.gif" width=5 height=10></td>
	    <td></td>
    </tr>
</table>
<table class="mainText">
    <tr>
        <td>
            <%=Resource%>
        </td>
    </tr>
</table>
</td>
</tr></table>--%>
</asp:Content>
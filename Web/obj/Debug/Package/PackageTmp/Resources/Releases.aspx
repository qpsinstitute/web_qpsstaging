<%@ Page Language="C#"  MasterPageFile="~/QPSSite.Master"   AutoEventWireup="true" CodeBehind="Releases.aspx.cs" Inherits="QPS.Resources.Releases" %>


<asp:Content ID="ContentReleases" ContentPlaceHolderID="cphContent" runat="server"  >
<script language ="javascript" type ="text/javascript" >
    setPage("Resources");

</script>

	
	
<div class="span-24 prepend-1 top highcont">
	<div class="span-19 top" style="float:right">
	  
		<div class="span-4 blackbl last"><a  href ="links.aspx" title="Links">Links</a></div>
		<div class="span-4 blackbl last"><a href="NewsLetter.aspx" title="Newsletters">Newsletters</a></div>
	    <div class="span-4 blackbk last"><a title="Press Releases">Press Releases</a></div>
	    <%--<div class="span-4 blackbl last"><a href="../Jobs/JobPosting.aspx" title="Jobs">Jobs</a></div>--%>
        <div class="span-4 blackbl last"><a href="../Placement/Aboutus.aspx" title="Jobs">Staffing & Recruitment</a></div>
	     	 <%--<div class="span-3 blackbl last"><a href="../Events/Default.aspx" title="Events">Events</a></div>--%>
        <div class="span-3 blackbl last"><a href="Articals.aspx" title="Articals">Articals</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">Press Releases</h3>
		<ul id="photos">
        </ul>
		
		<table border="0">
		<tr style="vertical-align: top;">
                <td style="vertical-align: top;width:10%">
                    06/18/2015</td>
                <td valign="top">
                    <a  style="text-decoration: none" href="../Documents/Asq meeting.pdf" target="_blank" title="June Dinner Meeting (ASQ)"> Jay Patel is Speaker at ASQ Worcetser June 18, 2015  dinner meeting on "Risk Based Thinking & Risk Management for ISO 9001:2015</a>
                </td>
            </tr>
        <tr style="vertical-align: top;">
                <td style="vertical-align: top;width:10%">
                    02/25/2015</td>
                <td valign="top">
                    <a href="IsoSeminar.aspx" style="text-decoration: none" target="_blank">QPS Institute - Offering new seminars for ISO 9001:2015</a>
                </td>
            </tr>

       <%--   <tr style="vertical-align: top;">
                <td style="vertical-align: top;">
                    23/05/2013</td>
                <td valign="top">
                    <a href="SixSigmaYellow.aspx" style="text-decoration: none" target="_blank">Congratulations to the New Six Sigma Yellow Belts at Dunkin� Donuts!</a>
                </td>
            </tr>


         <tr style="vertical-align: top;">
                <td style="vertical-align: top;">
                    11/01/2012</td>
                <td valign="top">
                    <a href="QPSNewJersey.aspx" style="text-decoration: none" target="_blank">QPS Institute - also in New Jersey!</a>
                </td>
            </tr>

         <tr style="vertical-align: top;">
                <td style="vertical-align: top;">
                    04/16/2012</td>
                <td valign="top">
                    <a href="QPSStaffing.aspx" style="text-decoration: none" target="_blank">QPS launches new division � Staffing & Recruitment!</a>
                </td>
            </tr>
        <tr style="vertical-align: top;">
                <td style="vertical-align: top;">
                    02/13/2012</td>
                <td valign="top">
                    <a href="../Resources/QpsFdaRelated.aspx" style="text-decoration: none" target="_blank">QPS expanding FDA Related BIOMedical/PharmaTopic Offerings and adding Food Safety!</a>
                </td>
            </tr>
        <tr style="vertical-align: top;">
                <td style="vertical-align: top;">
                    02/01/2012</td>
                <td valign="top">
                    <a href="../Resources/QpsWebinars.aspx" style="text-decoration: none" target="_blank">QPS - Now Offering Webinars!</a>
                </td>
            </tr>

		     <tr style="vertical-align: top;">
                <td style="vertical-align: top;">
                    06/01/2011</td>
                <td valign="top">
                    <a href="../Resources/QpsAgile.aspx" style="text-decoration: none" target="_blank">QPS � Now Offering Agile!</a>
                </td>
            </tr>
		
            <tr style="vertical-align: top;">
                <td style="vertical-align: top;">
                    11/01/2010</td>
                <td valign="top">
                    <a href="../Resources/QPSRIMA.aspx" style="text-decoration: none" target="_blank">QPS � A Member of the Rhode Island Manufacturers Association</a>
                </td>
            </tr>
                <tr style="vertical-align:top;">
		            <td style="vertical-align:top;">08/09/2010</td>
		            <td valign="top">
                        <a href="../Resources/BBJrelease.aspx" target="_blank" style="text-decoration:none">Jay P. Patel, President & CEO, featured in Boston Business Journal�s Lessons Learned on �What would you tell naturally shy people who have difficulty networking?�</a>
                        </td>
		        </tr>
		        
                <tr style="vertical-align: top;">
                    <td style="vertical-align: top;">08/26/2010</td>
                    <td valign="top"><a href="../Resources/QpsNRICC.aspx" style="text-decoration: none" target="_blank">QPS joined the Northern Rhode Island Chamber of Commerce</a>
                    </td>
                </tr>
                <tr style="vertical-align: top;">   
                    <td style="vertical-align: top;">08/22/2010</td>
                    <td valign="top"><a href="../Resources/QpsMCC.aspx" style="text-decoration: none" target="_blank">QPS joined the Marlborough Regional Chamber of Commerce </a></td>
                </tr>
		        <tr style="vertical-align:top;">
		            <td style="vertical-align:top;">08/20/2010</td>
		            <td valign="top"><a href="../Resources/Cambridge.aspx" target="_blank" style="text-decoration:none">Cambridge Who�s Who Recognizes Jay P. Patel, ASQ Fellow, as an Honored Member</a></td>
		        </tr>
		        <tr style="vertical-align:top;">
		            <td style="vertical-align:top;">07/14/2010</td>
		            <td valign="top"><a href="../Resources/QpsMBE.aspx" target="_blank" style="text-decoration:none">QPS Now a Certified MBE!</a></td>
		        </tr>
		        <tr style="vertical-align:top;">
		            <td style="vertical-align:top;">07/02/2010</td>
		            <td valign="top"><a href="../Resources/BusinessLesson.aspx" target="_blank" style="text-decoration:none">Jay P. Patel, President & CEO, featured in Boston Business Journal�s Lessons Learned on <br /> <font style="font-style:italic"> �For Entrepreneurs, Novel Advice Has a Lasting Impact.�</font></a></td>
		        </tr>
		        <tr style="vertical-align:top;">
		            <td style="vertical-align:top;">06/29/2010</td>
		            <td valign="top"><a target="_blank" href="../Resources/QpsRating.aspx" style="text-decoration:none">QPS Accredited with A+ Rating by the Better Business Bureau!</a></td>
		        </tr>
		        <tr style="vertical-align:top;">
		            <td style="vertical-align:top;">06/08/2010</td>
		            <td valign="top"><a href="../Resources/QpsWorldCenter.aspx" target="_blank" style="text-decoration:none">QPS opens World Class Corporate Training Center, <font style="font-style:italic">QPS Institute</font></a></td>
		        </tr>
		        <tr style="vertical-align:top;">
		            <td style="vertical-align:top;">06/04/2010</td>
		            <td valign="top"><a href="../Resources/RiskManagement.aspx" target="_blank" style="text-decoration:none;">Jay P.Patel, President & CEO, featured in Boston Business Journal�s Lessons Learned on <br /> <font style="font-style:italic"> �Risk Management: Setting the Right Growth Gauge is a Challenge for Startups.�</font></a></td>
		        </tr>
		      
		        <tr style="vertical-align:top;">
		            <td style="vertical-align:top;">05/21/2010</td>
		            <td valign="top"><a href="../Resources/UsefulBook.aspx" target="_blank" style="text-decoration:none">Jay P. Patel, President & CEO, featured in Boston Business Journal�s Lessons Learned on <br /> <font style="font-style:italic">�What is the Most Useful Book You Have Ever Read?�</font></a></td>
		        </tr>
		      --%>
		         <tr style="vertical-align:top;">
		            <td style="vertical-align:top;">04/13/2010</td>
		            <td valign="top"><a href="../Resources/QpsCNN.aspx" target="_blank" style="text-decoration:none">QPS Certifications & Student Featured on CNN!</a></td>
		        </tr>
		       
		</table>
	</div>
	<div class="span-4">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <hr class="space">
	    <br class="clear"><br class="clear">
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>

</asp:Content>

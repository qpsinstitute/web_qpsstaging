﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="QPS.Webinars.Default"
    MasterPageFile="~/QPSSite.Master" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="contentwebinars" ContentPlaceHolderID="cphContent" runat="server">

    <style type="text/css">
        #ctl00_cphContent_tbldata td
        {
            padding: 0px;
        }
        .lock
        {
            border-left: 1px solid #D3D4D4;
            border-bottom: 1px solid #D3D4D4;
            border-right: 1px solid #D3D4D4;
            border-top: 1px solid #D3D4D4;
            background-color: #F1F1F1;
        }
        
        div#tipDiv
        {
            background: transparent url(../Images/P_center.gif) repeat-y scroll 0%;
            position: absolute;
            visibility: hidden;
            left: 0;
            top: 0;
            z-index: 10000;
            font-size: 11px;
            line-height: 1.2;
        }
        
        .span-22
        {
            height: 33px !important;
        }
    </style>
     <script language="javascript" type="text/javascript">
         setPage("Webinars");
     </script>

    <br class="clear">
    <div style="padding: 5px">
        <table border="0" cellpadding="0" cellspacing="0" topmargin="0" style="padding-top: 500"
            leftmargin="0" width="100%">
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="tblUpCommingSeminar" style="width:50%;float:left">
                        <tr>
                            <td colspan="2" style="word-spacing: 0px; font: medium arial, helvetica, sans-serif;
                                font-size: 20px; text-transform: none; text-indent: 0px; white-space: normal;
                                letter-spacing: normal; background-color: rgb(255,255,255); text-decoration: none;
                                orphans: 2; widows: 2; webkit-text-size-adjust: auto; webkit-text-stroke-width: 0px;
                                color: #a68d0d">
                                UpComing Webinar
                            </td>
                        </tr>
                        <asp:Repeater ID="UpCommingSeminar" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td colspan="5" style="word-spacing: 0px; font: medium arial, helvetica, sans-serif;
                                        text-transform: none; text-indent: 0px; white-space: normal; letter-spacing: normal;
                                        background-color: rgb(255,255,255); text-decoration: none; orphans: 2; widows: 2;
                                        webkit-text-size-adjust: auto; webkit-text-stroke-width: 0px">
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "WebinarID", "WebinarDetails.aspx?WebinarId={0}") %>'
                                            Text='<%#DataBinder.Eval(Container,"DataItem.Title")%>' class="verdana12black"
                                            Style="text-decoration: none; font-size: 15px; color: rgb(0,96,165); font-family: Verdana, Arial, Helvetica, sans-serif">  </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="60px">
                                        <image src='../images/<%#DataBinder.Eval(Container,"DataItem.InstructorImage")%>'
                                            height="50" width="50"></image>
                                    </td>
                                    <td>
                                        <b>Instructor :</b>
                                        <%#DataBinder.Eval(Container, "DataItem.Instructor")%>
                                        <br />
                                        <b>Webinar Date :</b>
                                        <%#DataBinder.Eval(Container, "DataItem.WebinarOn", "{0:dddd, MMMM d, yyyy HH:mm}")%>
                                         <%#DataBinder.Eval(Container, "DataItem.WebinarTime")%>&nbsp;EST

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="border-bottom-style: dotted; border-bottom-width: thin; border-bottom-color: #000000;
                                        text-align: justify;">
                                        <%#DataBinder.Eval(Container, "DataItem.Description")%>
                                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "WebinarID", "WebinarDetails.aspx?WebinarId={0}") %>'
                                            Text='View Details' class="verdana12black" Style="text-decoration: none; color: rgb(0,102,255);
                                            font-family: Verdana, Arial, Helvetica, sans-serif">  </asp:HyperLink>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <img src="../Images/download.jpg" style="float:left;width:5px;height:100%"  />
                    <table style="width:49%;float:left" cellpadding="0" cellspacing="0">
                        <tr>
                            <td colspan="2" style="word-spacing: 0px; font: medium arial, helvetica, sans-serif;
                                font-size: 20px; text-transform: none; text-indent: 0px; white-space: normal;
                                letter-spacing: normal; background-color: rgb(255,255,255); text-decoration: none;
                                orphans: 2; widows: 2; webkit-text-size-adjust: auto; webkit-text-stroke-width: 0px;
                                color: #a68d0d">
                                Past Webinar
                            </td>
                        </tr>
                        <asp:Repeater ID="PastSeminar" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td colspan="4" style="word-spacing: 0px; font: medium arial, helvetica, sans-serif;
                                        text-transform: none; text-indent: 0px; white-space: normal; letter-spacing: normal;
                                        background-color: rgb(255,255,255); text-decoration: none; orphans: 2; widows: 2;
                                        webkit-text-size-adjust: auto; webkit-text-stroke-width: 0px">
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "WebinarID", "WebinarDetails.aspx?WebinarId={0}&IsPast=true") %>'
                                            Text='<%#DataBinder.Eval(Container,"DataItem.Title")%>' class="verdana12black"
                                            Style="text-decoration: none; font-size: 15px; color: rgb(0,96,165); font-family: Verdana, Arial, Helvetica, sans-serif">  </asp:HyperLink>
                                    </td>
                                    <td style="width: 100px; text-align: right">
                                        <div style="cursor: pointer" onclick='<%# DataBinder.Eval(Container.DataItem, "WebinarID", "ShowHideDetails({0})") %>'>
                                            <a id='<%# DataBinder.Eval(Container.DataItem, "WebinarID", "showhidetext_{0}") %>'>
                                                Show Details</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr >
                                    <td width="60px" style="border-bottom-style: dotted; border-bottom-width: thin; border-bottom-color: #000000;">
                                        <span id=<%# DataBinder.Eval(Container.DataItem, "WebinarID","'span_{0}'") %> style="display: none">
                                            <image src='../images/<%#DataBinder.Eval(Container,"DataItem.InstructorImage")%>'
                                                height="50" width="50"></image>
                                        </span>
                                    </td>
                                    <td colspan="6" style="border-bottom-style: dotted; border-bottom-width: thin; border-bottom-color: #000000;">
                                        <span id=<%# DataBinder.Eval(Container.DataItem, "WebinarID","'span1_{0}'") %> style="display: none">
                                            <b>Instructor :</b>
                                            <%#DataBinder.Eval(Container, "DataItem.Instructor")%>
                                            <br />
                                            <b>Webinar Date :</b>
                                            <%#DataBinder.Eval(Container, "DataItem.WebinarOn", "{0:dddd, MMMM d, yyyy HH:mm}")%>
                                            <br />
                                            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "WebinarID", "WebinarDetails.aspx?WebinarId={0}&IsPast=true") %>'
                                                Text="View Details" class="verdana12black" Style="text-decoration: none; 
                                                color: rgb(0,102,255); font-family: Verdana, Arial, Helvetica, sans-serif">  </asp:HyperLink>
                                        </span>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                         <tr>
                            <td colspan="6" ><font style="background-color: Yellow;"><b>Note: </b>If you want to attend any past webinar,
        please contact us on <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> or Toll
        Free: <span style="color: rgb(0,102,255);">1-877-987-3801. </span>
        
              </font>
                           </td>
        </tr>
                    </table>
                </td>
            </tr>
        </table>
     
    </div>
    <script type="text/javascript">
        function ShowHideDetails(rowNo) {
            //debugger;
            var span = document.getElementById('span_' + rowNo);
            var showhidetext = document.getElementById('showhidetext_' + rowNo);

            if (span) {
                if (span.style.display == "none") {
                    span.style.display = "inline";
                    showhidetext.innerHTML = "Hide Details"
                }
                else {
                    span.style.display = "none";
                    showhidetext.innerHTML = "Show Details"
                }
            }


            span = document.getElementById('span1_' + rowNo);

            if (span) {
                if (span.style.display == "none") {
                    span.style.display = "inline";
                }
                else {
                    span.style.display = "none";
                }
            }

            span = document.getElementById('span2_' + rowNo);
            if (span) {
                if (span.style.display == "none") {
                    span.style.display = "inline";
                }
                else {
                    span.style.display = "none";
                }
            }
        }     
    </script>
</asp:Content>

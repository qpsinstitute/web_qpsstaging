<%@ Page AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="QPS._Default"
    Language="C#" MasterPageFile="~/QPSSiteHome.Master" %>

<asp:Content ID="ContentDefault" runat="server" ContentPlaceHolderID="cphContent1">
    <script language="javascript" type="text/javascript">
        //  setPage("Home");


        function CheckOut(CourseCode, Title, Cost) {

            document.getElementById("hdnCourseCode").value = CourseCode;
            document.getElementById("hdnTitle").value = Title;
            document.getElementById("hdnCost").value = Cost;
            document.FormCheckOut.submit();
        }
    
    </script>
    <style>
        .btnTxtHM {
  float: left;
  width: 131px;
}
    </style>
    <form id="FormCheckOut" action="https://online.qpsinc.com/youraccount/RegistrationSignIn.aspx"
    method="post" name="FormCheckOut">
    <input id="hdnCourseCode" name="courseId" type="hidden" />
    <input id="hdnTitle" name="courseTitle" type="hidden" />
    <input id="hdnCost" name="cost" type="hidden" />
    </form>
    <div class="highcont defaultContentWrp">
     
        <div class="defaultLeftBtnWrpHM fs14">
            <p>
                <strong>Quality & Productivity Solutions, Inc. </strong>is a training and consulting
                firm providing services to improve quality, productivity, speed, cost, and individual
                skills, in addition to streamlining processes. Our goal is to improve processes,
                products and performance � as well as achieving organizational and individual objectives.
            </p>
            <p>
                QPS has the capability, skills, resources and experience to work in all types of
                industries � including Service, Manufacturing, Government and Software. Training
                in over 100 topics ranging from Lean, Six Sigma, PMP, Agile, Supply Chain, and Quality
                Management Systems � QPS offers training locations globally with excellent curriculum
                resulting in <u>guaranteed satisfaction</u>.
            </p>
        </div>

           <div class="defaultfullBtnWrpHM">
            <div title="Training Calendar" class="bigBtn1HM txtAL lh40">
                <div class="btnTxtHM">
                   <a href="../Documents/corporate calender training Jan -June 2016.pdf" title="Training Calendar"
                        target="_blank">Training Calendar</a>
                </div>
               <div class="btnTxtIconWrp">
                   <%-- <a href="../Documents/corporate calender training Jul-Dec 2014.pdf" title="Training Calendar"
                        target="_blank">
                        <img src="Images/pdf.png" title="Download PDF" /></a>--%>
                </div>
            </div>
            <div title="PMP" class="bigBtn1HM txtAL lh25 lh40">
                <div class="btnTxtHM">
                  <%--<a href="Documents/PMP.pdf"
                            target="_blank">PMP</a>--%>
                    <a href="javascript:void(0);">PMP</a>

                </div>
                <div class="btnTxtIconWrp">
                    <a href="#" onclick="CheckOut('CCRTPMP','PMI � Project Management Professional  Certification � (PMP) Preparation','2500')">
                        <img src="Images/cart1.png" title="Add to cart" /></a> <%--<a href="Documents/PMP.pdf"
                            target="_blank">
                            <img src="Images/pdf.png" title="Download PDF" /></a>--%>
                </div>
            </div>
           <div title="Risk Based thinking and Risk Management for ISO 9001:2015" class="bigBtn1HM txtAL lh25 mr0">
                <div class="btnTxtHM">
                    <a href="Documents/101D.pdf"
                            target="_blank">Risk Based thinking
                            <br />& Risk Management..</a>
                            
                            </div>
                <div class="btnTxtIconWrp">
                    <a href="#" onclick="CheckOut('CI01AISOD','Risk Based thinking and Risk Management  for ISO 9001:2015','495')">
                        <img src="Images/cart1.png" title="Add to cart" /></a> <%--<a href="Documents/311 - Scrum Master Certified - SMC.pdf"
                            target="_blank">
                            <img src="Images/pdf.png" title="Download PDF" /></a>--%>
                </div>
            </div>
            <div title="Scrum Master" class="bigBtn1HM txtAL lh40 mr0">
                <div class="btnTxtHM">
                    <%--<a href="Documents/311 - Scrum Master Certified - SMC.pdf"  target="_blank">Scrum Master</a>--%>
                    <a href="javascript:void(0);">Scrum Master</a>
                    
                </div>
                <div class="btnTxtIconWrp">
                    <a href="#" onclick="CheckOut('eSMCTM','Scrum Master Certified-SMC','1500')">
                        <img src="Images/cart1.png" title="Add to cart" /></a> <%--<a href="Documents/311 - Scrum Master Certified - SMC.pdf"
                            target="_blank">
                            <img src="Images/pdf.png" title="Download PDF" /></a>--%>
                </div>
            </div>
            <div title="Onsite Training" class="bigBtn1HM txtAL lh40">
                <div class="btnTxtHM">
                    <a href="../Training/Onsite.aspx" title="Onsite Training" target="_blank">Onsite Training</a>
                </div>
                <div class="btnTxtIconWrp">
                   <%-- <a href="../Training/Onsite.aspx" title="Onsite Training" target="_blank">
                        <img src="Images/pdf.png" title="Download PDF" /></a>--%>
                </div>
            </div>
           <div title="Understanding and Implementing ISO 9001:2015" class="bigBtn1HM txtAL lh25">
                <div class="btnTxtHM">
                   <a href="Documents/101A.pdf"
                            target="_blank">Understanding and<br />
                    Implementing ISO..</a></div>
                <div class="btnTxtIconWrp">
                    <a href="#" onclick="CheckOut('CI01AISOA','Understanding and Implementing ISO 9001:2015','895')">
                        <img src="Images/cart1.png" title="Add to cart" /></a> <%--<a href="Documents/313 - Scrum Product Owner Certified SPOC.pdf"
                            target="_blank">
                            <img src="Images/pdf.png" title="Download PDF" /></a>--%>
                </div>
            </div>

            
             <div title="Auditing to ISO 9001:2015 (System and Process audits)" class="bigBtn1HM txtAL lh25">
                <div class="btnTxtHM">
                   <a href="Documents/101C.pdf"
                            target="_blank">Auditing to<br />
                     ISO 9001:2015</a>
                </div>
                <div class="btnTxtIconWrp">
                    <a href="#" onclick="CheckOut('CI01AISOC','Auditing to ISO 9001:2015 (System and Process audits)','895')">
                        <img src="Images/cart1.png" title="Add to cart" /></a> <%--<a href="Documents/Lean Six Sigma Green Belt Certification.pdf"
                            target="_blank">
                            <img src="Images/pdf.png" title="Download PDF" /></a>--%>
                </div>
            </div>
            <%--  <a href="../Documents/corporate calender training Jul-Dec 2014.pdf" title="Training Calendar" class="bigBtn1HM lh45"
                target="_blank">Training Calendar</a> <a href="../Training/Onsite.aspx" title="Onsite Training"
                    class="bigBtn1HM lh45" target="_blank">Onsite Training</a>--%>
            <div title="Documenting ISO 9001:2015" class="bigBtn1HM txtAL lh25 mr0">
                <div class="btnTxtHM">
                
                  <a href="Documents/101B.pdf"
                            target="_blank">Documenting<br />
                     ISO 9001:2015</a></div>
                <div class="btnTxtIconWrp">
                    <a href="#" onclick="CheckOut('CI01AISOB','Documenting ISO 9001:2015','495')">
                        <img src="Images/cart1.png" title="Add to cart" /></a> <%--<a href="Documents/Six Sigma Green Belt Certification.pdf"
                            target="_blank">
                            <img src="Images/pdf.png" title="Download PDF" /></a>--%>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>

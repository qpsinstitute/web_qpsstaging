<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Clients.aspx.cs" Inherits="QPS.Company.Clients" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Company" Src="~/Controls/Sidebar_Company.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>

<asp:Content ID="ContentClients" ContentPlaceHolderID="cphContent" runat="server">
<script language="javascript" type="text/javascript">
    setPage("Company");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-18 top" style="float:right">
		<div class="span-3 blackbl last" style="width:80px;"><a title="About QPS" href="about.aspx">About QPS</a></div>
		  <div class="span-3 blackbl last" style="width:180px;"><a title="Membership and Partnership" href="Membership.aspx">Membership and Partnership</a></div>

		<div class="span-3 blackbl last" style="width:80px;"><a title="Our Staff" href="Staff.aspx">Our Staff</a></div>
		<%--<div class="span-3 blackbl last" style="width:90px;"><a title="Core Values" href="CoreValue.aspx">Core Values</a></div>--%>
		<div style="width: 90px;"  class="span-3 blackbk last"><a title="Our Clients">Our Clients</a></div>
        <%--<div style="width: 140px;" class="span-3 blackbl last"><a title="Student Testimonials" href="StudentTestimonials.aspx">Student Testimonials</a></div>--%>
        <div class="span-3 blackbl last" style="width: 100px;">
            <a title="Testimonials" href="Testimonials.aspx">Testimonials</a></div>
		<%--<div class="span-3 blackbl last" style="width:70px;"><a title="Contact" href="Contact.aspx">Contact</a></div>--%>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="clients" title="Our Clients">Our Clients</h3>
		<hr/>
		<p style="font-size:125%">
		    A Partial Listing Of QPS Clients And Organizations Trained By Our Staff:
		</p>
		<div class="span-18 last">
			<div class="span-6">
			    <ul>
				    <li>Allegro MicroSystems, Inc.</li>
                    <li>American Superconductor</li>
                    <li>American Society for Quality</li>
                    <li>Anheuser Busch, Inc.</li>
                    <li>ARK-LES Corporation</li>
                    <li>AstraZeneca</li>
                    <li>AT&amp;T Broadband</li>
                    <li>Atlantic Mutual</li>
                    <li>Attrium Medical</li>
                    <li>Bayer Corporation</li>
                    <li>Becton Dickinson</li>
                    <li>BIC Corporation</li>
                    <li>Blue Cross Blue Shield</li>
                    <li>Boston Scientific</li>
                    <li>Brainan Industries</li>
                    <li>C. R. Bard, Inc.</li>
                    <li>Clark Cutler McDermott</li>
                    <li>Coghlin Electric</li>
                    <li>Cri-tech Inc.</li>
                    <li>Dow Jones</li>
                    <li>Draeger Medical</li>
                    <li>Dunkin Donuts</li>
                    <li>Dynabil Industries</li>
                    <li>E. A. Patten Company</li>
                    <li>EAD Motors</li>
                    <li>EMC Corporation</li>
                    <li>Evans Plating Corporation</li>
                    <li>Extrusion Technology, Inc.</li>                                      
			    </ul>
			</div>
			<div class="span-6">
			    <ul>       
			        <li>Flexcon Company</li>  
			        <li>General Electric</li>  
                    <li>Genzyme Corporation</li>
                    <li>Hanover Insurance</li>
                    <li>Hewlett Packard</li>
                    <li>Hi-Tech Gold Plating</li>
                    <li>Hollingsworth Vose Companies</li>
                    <li>Honeywell, D. I.</li>
                    <li>Husky Injection Molding</li>
                    <li>In-Cal</li>
                    <li>Intermagnetics</li>
                    <li>Ionics</li>
                    <li>Johnson &amp; Johnson</li>
                    <li>Kendall Company</li>
                    <li>Laerdal Medical Corporation</li>
                    <li>Lego Systems, Inc.</li>
                    <li>Luster-On</li>
                    <li>Martin Point Healthcare</li>
                    <li>Maryland Mold, Inc.</li>
                    <li>Mass Mutual Life Ins. Co.</li>
                    <li>Merrill Lynch</li>
                    <li>Micro E. Systems</li>
                    <li>Middleton Aerospace Corp.</li>
                    <li>North East Quality Council</li>
                    <li>Northrop Grumman</li>
                    <li>OEM Controls</li>      
                    <li>Pfizer</li>  
			        <li>Philips Corporation</li>              
			    </ul>
			</div>
			<div class="span-5 last">
			    <ul>      
			        
			        <li>Picis Inc.</li>
			        <li>Pitney Bowes</li>
			      	<li>PRI Automation </li>
                    <li>Remington Products Co.</li>
                    <li>Rubb Building Systems</li>
                    <li>Saint Gobain/Norton Co.</li>
                    <li>Sensitech, Inc.</li>
                    <li>Shire Pharmeceuticals</li>
                    <li>Simplex Time Recorder</li>
                    <li>Sippican</li>
                    <li>Spirol International</li>
                    <li>Staples, Inc.</li>
                    <li>Starent Networks/Cisco</li>
                    <li>Strahman Valves</li>
                    <li>Tech-Etch, Inc.</li>
                    <li>Textron, Inc.</li>
                    <li>The First Years, Inc.</li>
                    <li>TJX companies</li>
                    <li>Toray Plastics</li>
                    <li>Traverse USA</li>
                    <li>Tuscarora, Inc.</li>
                    <li>US Air Force</li>
                    <li>US Army</li>
                    <li>US Navy</li>
                    <li>Vectron International</li>
                    <li>Vista Print</li>
                    <li>Zoll Medical</li>                                        
			    </ul>
			</div>
			<br class="clear">
			<p style="float:right;text-align:right;"><a class="nounder" href="#top" title="Top"><img src="../images/top.gif" title="Top"></a></p>
	    </div>
	</div>
	<div class="span-4 top">
	    <img src="../images/about_us.gif" class="pull-1 top"><hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
<%--<table>
<tr>
<td valign="top" style="background-color:#d9d9d9;">
    <QPSSidebar:Company id="Sidebar_Company" runat ="server"/>
    <QPSSidebar:Sidebar id="Sidebar" runat ="server"/>
</td>
<td>    
    <table class="mainText" width='100%' id="Table1">
	    <tr class="Company" style="HEIGHT: 2px;">
		    <td style="WIDTH: 7px;"><img src='..\Images\Clear.gif' width=7 height=2></td>
		    <td></td>
		    <td></td>
	    </tr>
	    <tr>
		    <td bgcolor='#339933' style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=5></td>
		    <td rowspan="3"><img src='../Images/aboutUs_img.jpg'></td>
		    <td class="Company" width="100%">Clients</td>
	    </tr>
	    <tr>
		    <td class="CompanySpacer" style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=5></td>
		    <td class="CompanySpacer" style="padding-left: 20px; padding-right: 20px; HEIGHT:65px">			
			    A Partial Listing Of QPS Clients And Organizations Trained By Our Staff:		
		    <td>
	    </tr>
	    <tr bgcolor="white" style="HEIGHT: 20px;">
		    <td style="WIDTH: 5px;"><img src='..\Images\Clear.gif' width=5 height=10></td>
		    <td></td>
	    </tr>
    </table>
    <table class="mainText"> 
    <tr>      
        <td><%=Client%></td>
    </tr>
    </table>    
</td>
</tr></table>--%>
</asp:Content>

<%@ Page AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="QPS.Company.Contact"
    Language="C#" MasterPageFile="~/QPSSite.Master" %>

<%@ Register Src="~/Controls/Sidebar_Company.ascx" TagName="Company" TagPrefix="QPSSidebar" %>
<%@ Register Src="~/Controls/Sidebar.ascx" TagName="Sidebar" TagPrefix="QPSSidebar" %>
<asp:Content ID="ContentContact" runat="server" ContentPlaceHolderID="cphContent">
  
       <script language="javascript" type="text/javascript">
           setPage("Contact us");

           function loadMap(Lat, Lng, i) {
               if (GBrowserIsCompatible()) {
                   var infotext = "";
                   if (i == 1) infotext = "<b><a target='_new' href='http://maps.google.com/maps/place?cid=5644619032564209082&q=One+Sunny+Hill+Drive+Oxford,+MA+01540&hl=en&cd=1&ei=F3bES5qcIaT4ugPorvS9DQ&sll=42.110003,-71.896505&sspn=0.009487,0.022724&ie=UTF8&ll=42.11489,-71.907878&spn=0,0&z=16&iwloc=r0'>Quality and Productivity Solutions, Inc.</a></b><br><br>Mailing address:<br>One Sunny Hill Drive<br>Oxford, MA 01540";
                   else if (i == 2) infotext = "<b><a target='_new' href='http://maps.google.com/maps/place?cid=6268279754568657159&q=perpetuating+software&hl=en&cd=1&ei=6HbES-bnCqP4ugPH6IUD&sll=23.011008,72.507083&sspn=0.071946,0.071946&ie=UTF8&ll=23.056989,72.416039&spn=0,0&z=13&iwloc=A'>Perpetuating Software Technologies Pvt. Ltd.</a></b><br><br>Asia Pacific:<br>404, Ashirvad Paras<br>Corporate Road<br>Nr. Auda Garden, Prahlad Nagar<br>Ahmedabad � 380015<br>Gujarat, India";
                   else if (i == 3) infotext = "<b><a target='_new' href='https://www.google.com/maps/place/28+Lord+Rd+%23205,+Marlborough,+MA+01752/@42.3464018,-71.5714875,17z/data=!3m1!4b1!4m2!3m1!1s0x89e38b4b25a18d51:0xc680feb2756c7d0c'>Quality and Productivity Solutions, Inc.</a></b> <br><br>Corporate:<br>28 Lord Road,<br>Suite 205,<br>Marlborough, MA 01752<br>Toll free: 1 (877) 987-3801<br>Phone no.: (508) 786-0777, 0775<br>Fax: (508) 786-0778<br>Email: <a href='mailto:info@qpsinc.com'>info@qpsinc.com</a>";
                   else if (i == 4) infotext = "<b><a target='_new' href='http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=15+New+England+Executive+Park+1st+Floor+Burlington,+MA+01803&ie=UTF8&hq=&hnear=15+New+England+Executive+Park,+Burlington,+Middlesex,+Massachusetts+01803&ll=42.485944,-71.208937&spn=0.009431,0.022724&z=16&iwloc=A'>Boston, MA Training Facility</a></b><br>15 New England Executive Park<br>1st Floor<br>Burlington, MA 01803";
                   else if (i == 5) infotext = "<b><a target='_new' href='https://www.google.co.in/maps/place/175+Capital+Blvd+%23402,+Rocky+Hill,+CT+06067,+USA/@41.6490528,-72.6680367,17z/data=!3m1!4b1!4m2!3m1!1s0x89e64cf922414bad:0x6d91376e94c72da2'>Connecticut Training Facility</a></b><br>175 Capital Blvd,<br>Suite 402,<br>Rocky Hill, CT 06067";
                   else if (i == 6) infotext = "<b><a target='_new' href='https://maps.google.co.in/maps?f=d&source=s_d&saddr=249+Roosevelt+Avenue,+Suite+203+Pawtucket,+RI+02860&daddr=&hl=en&geocode=&aq=&sll=41.714324,-71.463808&sspn=0.011196,0.022724&vpsrc=0&mra=ls&ie=UTF8&t=m&z=16&iwloc=ddw0'>Rhode Island Training Facility</a></b><br>QPS Training<br>249 Roosevelt Avenue, Suite 203<br>Pawtucket, RI 02860";
                   else if (i == 7) infotext = "<b><a target='_new' href='http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=230+Park+Avenue+10th+Floor,+Suite+1000+New+York,+NY&sll=40.754361,-73.975775&sspn=0.009688,0.022724&ie=UTF8&hq=&hnear=230+Park+Ave+%231000,+New+York,+10017&ll=40.754832,-73.975625&spn=0.009688,0.022724&z=16&iwloc=A'>New York Training Facility</a></b><br>230 Park Avenue<br>10th Floor, Suite 1000<br>New York, NY 10169";
                   else if (i == 8) infotext = "<b><a target='_new' href='https://maps.google.co.in/maps?q=581+Main+Street+Suite+640++Woodbridge+NJ,+07095&hl=en&sll=40.544913,-74.301076&sspn=0.006612,0.009645&hnear=581+Main+St+%23640,+Fords,+New+Jersey+08863,+United+States&t=m&z=17'>New Jersey Training Facility</a></b><br>581 Main Street Suite 640,<br>Turnpike Building,<br>Woodbridge, NJ 07095";
                   //else if (i == 9) infotext = "<b><a target='_new' href='http://maps.google.co.in/maps?f=q&source=s_q&hl=en&q=10225+Feldfarm+Ln,+Charlotte,+Mecklenburg,+North+Carolina+28210,+United+States&sll=40.567041,-74.330792&sspn=0.007139,0.01929&ie=UTF8&cd=1&geocode=FR-CFwId6x4u-w&split=0&hq=&hnear=10225+Feldfarm+Ln,+Charlotte,+Mecklenburg,+North+Carolina+28210,+United+States&ll=35.094859,-80.863516&spn=0.00769,0.01929&z=16'>North Carolina Training Facility</a></b><br>10225 Feld Farm Lane<br>Building B<br> Charlotte, NC 28210";
                   else if (i == 10) infotext = "<b><a target='_new' href='http://maps.google.co.in/maps?f=q&source=s_q&hl=en&geocode=&q=AAA+Tower+Inn+%26+Suites++303+W+Algonquin+Road++Mount+Prospect,+IL+60056&sll=42.006229,-87.870229&sspn=0.891858,2.469177&ie=UTF8&hq=AAA+Tower+Inn+%26+Suites&hnear=303+W+Algonquin+Rd,+Mt+Prospect,+Cook,+Illinois+60056,+United+States&ll=42.031381,-87.941115&spn=0.006567,0.01929&z=16&iwloc=A&cid=3216466983382195253'>Illinois Training Facility </a></b><br>CHICAGO<br>15 South Racine Ave,Suite 2S,<br>Chicago, IL 60607<br>";

                   var point;
                   var map = new GMap2(document.getElementById("map"));
                   map.enableDoubleClickZoom();
                   map.enableScrollWheelZoom();
                   map.addControl(new GMapTypeControl());
                   map.addControl(new GSmallMapControl());
                   var marker = new GMarker(new GLatLng(Lat, Lng));
                   map.setCenter(new GLatLng(Lat, Lng), 17);
                   GEvent.addListener(marker, "click", function () {
                       marker.openInfoWindowHtml(infotext);
                   });
                   map.addOverlay(marker);
                   if (i == 3) { marker.openInfoWindowHtml(infotext); }
                   map.setCenter(new GLatLng(Lat, Lng), 16);
                   var polyline = new GPolyline([new GLatLng(Lat, Lng),
        new GLatLng(Lat, Lng),
        new GLatLng(Lat, Lng)], "#3333cc", 10);
               }
               map.addOverlay(polyline);
           }

           function openPopup(txtURL) {
               window.open(txtURL, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width=600,height=500");
           }
    </script>
    <!--http://localhost/ (Used for Local) -->
    <%--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAA-q_sUFm-d7vB8TOwq4T5oxT2yXp_ZAY8_ufC3CFXhHIE1NvwkxSQ04rg94ywX08wmmeqTPpkNl16xA" type="text/javascript"></script>--%>
    <!--http://qps.staging.perpetuating.com/ (Used for Staging) -->
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAA-q_sUFm-d7vB8TOwq4T5oxR4_7oGciRO3Q8aLo_ZdK78Uv3TtBR3ypRIVWDWBQWIPbpw-nHY88LesQ"
        type="text/javascript"></script>
    <!--http://www.qpsinc.com/ (Used for Production) -->
    <%--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAA-q_sUFm-d7vB8TOwq4T5oxSG0AVmqPV_oSsMUsIP2V-uCt8PCBRbDPHEGSYLueDWk2Q9mjfIahE7KQ" type="text/javascript"></script>--%>
    <!--http://qpsinc.com/ -->
    <%--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAA-q_sUFm-d7vB8TOwq4T5oxR4_7oGciRO3Q8aLo_ZdK78Uv3TtBR3ypRIVWDWBQWIPbpw-nHY88LesQ" type="text/javascript"></script>--%>
    <style type="text/css">
        :-moz-any-link:focus
        {
            outline: none;
        }
        a
        {
            outline: none;
        }
        a:active
        {
            outline: none;
            -moz-outline-style: none;
        }
        a:focus
        {
            outline: none;
            -moz-outline-style: none;
        }
        .colblk
        {
            text-decoration: underline;
        }
    </style>
    <body onload="javascript:loadMap(42.346402,-71.571487,3)">
        <div class="span-24 prepend-1 top highcont">
            <div class="span-18 top" style="float: right">
                <div class="span-3 blackbl last" style="width: 80px;">
                    <a href="about.aspx" title="About QPS">About QPS</a></div>
                <div class="span-3 blackbl last" style="width: 80px;">
                    <a href="Staff.aspx" title="Our Staff">Our Staff</a></div>
                <div class="span-3 blackbl last" style="width: 90px;">
                    <a href="Clients.aspx" title="Our Clients">Our Clients</a></div>
                <div class="span-3 blackbl last" style="width: 100px;">
                    <a href="Testimonials.aspx" title="Client Testimonials">Testimonials</a></div>
                <div class="span-3 blackbk last" style="width: 70px;">
                    <a title="Contact">Contact</a></div>
                <br class="clear">
                <br class="clear">
                <h3 id="testimonials" class="colbright" title="Contact Us">
                    Contact Us</h3>
                <hr />
                <div class="span-18 last">
                    <div class="span-13">
                        <div class="quotediv_New">
                            <table>
                                <tr>
                                    <td valign="top">
                                        <div id="divMap" style="width: 344px; vertical-align: top;">
                                            <h2 style="color: #366d9a; text-transform: uppercase; font-size: 14px; font-weight: bold;">
                                                Map & Directions</h2>
                                            <div id="map" style="width: 500px; height: 450px">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <form id="frmContact" runat="server" style="vertical-align: top;">
                            <table width="100%">
                                <tr>
                                    <td class="first" colspan="2">
                                        <h4>
                                            <b>Contact Form</b>
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Please feel free to ask any questions on our services and any inquiries. Appropriate
                                        experts will answer your questions with no obligation to you!<br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br>
                                        <b>Note: Required fields marked with </b>
                                        <img src="../images/req.gif"><br>
                                        <span id="ErrorMsg" runat="server" style="color: #C00;"></span>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="third">
                                        Name
                                        <img src="../images/req.gif"><br>
                                        <input id="name" runat="server" name="ctl00$cphContent$name" size="34" title="Name"
                                            type="text" />
                                        <asp:RequiredFieldValidator ID="Rqname" runat="server" ControlToValidate="name" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revName" runat="server" ControlToValidate="name"
                                            ErrorMessage="Only alphanumeric characters are allowed" ValidationExpression="[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="third">
                                        Email Address
                                        <img src="../images/req.gif"><br>
                                        <input id="emailaddress" runat="server" name="ctl00$cphContent$emailaddress" size="34"
                                            title="Email Address" type="text" />
                                        <asp:RequiredFieldValidator ID="Rqemailaddress" runat="server" ControlToValidate="emailaddress"
                                            ErrorMessage="*"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="Rgemailaddress" runat="server" ControlToValidate="emailaddress"
                                            ErrorMessage="Please enter a valid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="third">
                                        Company
                                        <br>
                                        <input id="company" runat="server" name="ctl00$cphContent$company" size="34" title="Company"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="third">
                                        Position<br>
                                        <input id="position" runat="server" name="ctl00$cphContent$position" size="34" title="Position"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="third">
                                        Telephone #
                                        <img src="../images/req.gif">
                                        <br>
                                        <input id="telephone" runat="server" maxlength="50" name="ctl00$cphContent$telephone"
                                            size="34" title="Telephone" type="text" />
                                        <asp:RequiredFieldValidator ID="Rqtelephone" runat="server" ControlToValidate="telephone"
                                            ErrorMessage="*"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revTelephone" runat="server" ControlToValidate="telephone"
                                            ErrorMessage="Only numeric characters are allowed" ValidationExpression="[^a-zA-Z ]*"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="third">
                                        Questions/Comments
                                        <img src="../images/req.gif">
                                        <asp:RequiredFieldValidator ID="Rqcomments" runat="server" ControlToValidate="comments"
                                            ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <textarea id="comments" runat="server" cols="26" name="ctl00$cphContent$comments"
                                            rows="4" title="Questions/Comments"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <tr>
                                        <td class="third">
                                            Verification Code
                                            <img src="../images/req.gif">
                                        </td>
                                    </tr>
                                    <td>
                                        <asp:TextBox ID="txtVerify" Style="vertical-align: top" Width="70px" Height="24px"
                                            runat="server"></asp:TextBox>
                                        <asp:Image ID="imCaptcha" ImageUrl="~/Controls/Captcha.ashx" ImageAlign="AbsMiddle"
                                            runat="server" />
                                        <asp:RequiredFieldValidator ID="reqcaptcha" runat="server" ControlToValidate="txtVerify"
                                            Style="vertical-align: super;" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtVerify"
                                            Display="Dynamic" ErrorMessage="You have Entered a Wrong Verification Code!Please Re-enter!!!"
                                            OnServerValidate="CAPTCHAValidate"></asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="Submit" runat="server" class="third" name="ctl00$cphContent$Submit" onserverclick="btnSubmit_Click"
                                            title="Submit" type="submit" value="Submit" /><br>
                                    </td>
                                </tr>
                            </table>
                            </form>
                        </div>
                    </div>
                    <div class="span-47 last" style="font-size: 10px; font-family: Verdana">
                        <div class="quotediv_New">
                            <!-- Contact Box Start Here -->
                            <div class="contBoxWrp">
                                <div class="contBoxInsTitle">
                                    <h3>
                                        Corporate</h3>
                                </div>
                                <p>
                                    28 Lord Road,<br>
                                     Suite 205,<br>
                                    Marlborough, MA 01752<br>
                                   
                                     Toll free: 1 (877) 987-3801<br />
                                     Phone no.: (508) 786-0777, 0775<br />
                                    Fax: (508) 786-0778<br />
                                    Email: <a class="nounder" href="mailto:info@qpsinc.com">info@qpsinc.com</a><br>

                                     <a href="#" onclick="javascript:loadMap(42.346402,-71.571487,3)" style="color: #FFFFFF;">
                                        Map & Directions</a></p>
                            </div>
                            <!-- Contact Box End Here -->
                               <!-- Contact Box Start Here -->
                            <div class="contBoxWrp">
                                <div class="contBoxInsTitle">
                                    <h3>
                                        New Jersey Training Facility</h3>
                                </div>
                                <p>
                                    581 Main Street Suite 640,<br>
                                    Turnpike Building,<br>
                                    Woodbridge, NJ 07095<br>
                                    <a href="#" onclick="javascript:loadMap(40.54332,-74.301516,8)" style="color: #FFFFFF;">
                                        Map & Directions</a></p>
                            </div>
                            <!-- Contact Box End Here -->
                              <!-- Contact Box Start Here -->
                            <div class="contBoxWrp">
                                <div class="contBoxInsTitle">
                                    <h3>
                                        Connecticut Training Facility</h3>
                                </div>
                                <p>
                                    175 Capital Blvd,<br>
                                    Suite 402,<br>
                                    Rocky Hill, CT 06067<br>
                                    <a href="#" onclick="javascript:loadMap(41.6493724,-72.6651971,5)" style="color: #FFFFFF;">
                                        Map & Directions</a></p>
                            </div>
                            <!-- Contact Box End Here -->.
                               <!-- Contact Box Start Here -->
                            <div class="contBoxWrp">
                                <div class="contBoxInsTitle">
                                    <h3>
                                        Rhode Island Training Facility</h3>
                                </div>
                                <p>
                                    <%--QPS Training<br>--%>
                                  249 Roosevelt Avenue,<br />
                                   Suite 203<br />
                                    Pawtucket, RI 02860<br />
                                    <a href="#" onclick="javascript:loadMap(41.881168,-71.382291,6)" style="color: #FFFFFF;">
                                        Map & Directions</a></p>
                            </div>
                            <!-- Contact Box End Here -->
                            <!-- Contact Box Start Here -->
                            <%--<div class="contBoxWrp1">
                                <div class="contBoxInsTitle">
                                    <h3>
                                        Mailing address</h3>
                                </div>
                                <p>
                                   One Sunny Hill Drive<br />
                                   Oxford, MA 01540<br />
                                  
                                    <a href="#" onclick="javascript:loadMap(42.110374,-71.896445,1)" style="color: #FFFFFF;">
                                        Map & Directions</a></p>
                            </div>--%>
                            <!-- Contact Box End Here -->
                          
                         
                            <!-- Contact Box Start Here -->
                            <div class="contBoxWrp">
                                <div class="contBoxInsTitle">
                                    <h3>
                                        New York Training Facility</h3>
                                </div>
                                <p>
                                    230 Park Avenue<br>
                                    10th Floor, Suite 1000<br>
                                    New York, NY 10169<br>
                                    <a href="#" onclick="javascript:loadMap(40.754999,-73.975672,7)" style="color: #FFFFFF;">
                                        Map & Directions</a></p>
                            </div>
                            <!-- Contact Box End Here -->
                            <!-- Contact Box Start Here -->
                            <%--<div class="contBoxWrp">
                                <div class="contBoxInsTitle">
                                    <h3 class="tpd7">
                                        North Carolina Training Facility</h3>
                                </div>
                                <p>
                                    10225 Feld Farm Lane<br>
                                    Building B<br>
                                    Charlotte, NC 28210<br>
                                    <a href="#" onclick="javascript:loadMap(35.095391,-80.863498,9)" style="color: #FFFFFF;">
                                        Map & Directions</a></p>
                            </div>--%>
                            <!-- Contact Box End Here -->
                            <!-- Contact Box Start Here -->
                            <div class="contBoxWrp">
                                <div class="contBoxInsTitle">
                                    <h3>
                                        Illinois Training Facility</h3>
                                </div>
                                <p>
                                    CHICAGO<br />
                                    15 South Racine Ave,Suite 2S,<br />
                                    Chicago, IL 60607<br />
                                    <a href="#" onclick="javascript:loadMap(41.8809657,-87.6568306,10)" style="color: #FFFFFF;">
                                        Map & Directions</a></p>
                            </div>
                            <!-- Contact Box End Here -->
                         
                            <!-- Contact Box Start Here -->
                            <div class="contBoxWrp2">
                                <div class="contBoxInsTitle">
                                    <h3>
                                        Asia Pacific Office</h3>
                                </div>
                                <p>
                                    404, Ashirvad Paras<br>
                                    Corporate Road<br>
                                    Nr. Auda Garden, Prahlad Nagar<br>
                                    Ahmedabad � 380015<br>
                                    Gujarat, India<br>
                                    <a href="#" onclick="javascript:loadMap(23.011498,72.507123,2)" style="color: #FFFFFF;">
                                        Map & Directions</a></p>
                            </div>
                            <!-- Contact Box End Here -->
                        </div>
                    </div>
                    <br class="clear">
                    <br class="clear">
                    <br class="clear">
                    <p style="float: right; text-align: right;">
                        <a class="nounder" href="#top" title="Top">
                            <img src="../images/top.gif" title="Top"></a>
                    </p>
                </div>
            </div>
            <div class="span-4 top">
                <img class="pull-1 top" src="../images/about_us.gif">
                <hr class="space">
                <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                    <img src="../images/check.gif" title="Click here">
                    <h3 class="mostwrap">
                        <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                    for our Upcoming Corporate Training Programs
                </div>
            </div>
            <div class="span-16">
                &nbsp;
            </div>
        </div>
    </body>




</asp:Content>

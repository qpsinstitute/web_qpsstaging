<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="StudentTestimonials.aspx.cs" Inherits="QPS.Company.StudentTestimonials" Title="Student Testimonials" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Company" Src="~/Controls/Sidebar_Company.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>

<asp:Content ID="ContentClients" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("Company");       
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-18 top" style="float:right">
		<div class="span-3 blackbl last" style="width:80px;"><a title="About QPS" href="about.aspx">About QPS</a></div>
		<div class="span-3 blackbl last" style="width:80px;"><a title="Our Staff" href="Staff.aspx">Our Staff</a></div>
		<%--<div class="span-3 blackbl last" style="width:90px;"><a title="Core Values" href="CoreValue.aspx">Core Values</a></div>--%>
		<div style="width: 90px;"  class="span-3 blackbl last"><a title="Our Clients" href="Clients.aspx">Our Clients</a></div>
		<div style="width: 140px;" class="span-3 blackbk last"><a title="Student Testimonials">Student Testimonials</a></div>
		<div class="span-3 blackbl last" style="width:130px;"><a title="Client Testimonials" href="Testimonials.aspx">Client Testimonials</a></div>		
		<%--<div class="span-3 blackbl last" style="width:70px;"><a title="Contact" href="Contact.aspx">Contact</a></div>--%>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="testimonials" title="Testimonials">Student Testimonials</h3>
		<hr/>		
		<div class="span-18 last">
            <div class="span-9 colborder">
                <div class="quotediv">
                    Coming Soon
                </div>
            </div>            
			<br class="clear">
			
		</div>
	</div>
	<div class="span-4 top">
	    <img src="../images/about_us.gif" class="pull-1 top">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif"  title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx"  title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
</asp:Content>
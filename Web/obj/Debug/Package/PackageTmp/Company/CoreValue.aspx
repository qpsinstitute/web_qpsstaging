<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="CoreValue.aspx.cs" Inherits="QPS.Company.CoreValue" Title="Core Values" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Company" Src="~/Controls/Sidebar_Company.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
<script language="javascript" type="text/javascript">
    setPage("Company");
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-18 top" style="float:right">
		<div class="span-3 blackbl last" style="width:80px;"><a title="About QPS" href="about.aspx">About QPS</a></div>
		<div class="span-3 blackbl last" style="width:80px;"><a title="Our Staff" href="Staff.aspx">Our Staff</a></div>
		<div class="span-3 blackbk last" style="width:90px;"><a title="Core Values">Core Values</a></div>
		<div style="width: 90px;"  class="span-3 blackbl last"><a title="Our Clients" href="Clients.aspx">Our Clients</a></div>
        <%--<div style="width: 140px;" class="span-3 blackbl last"><a title="Student Testimonials" href="StudentTestimonials.aspx">Student Testimonials</a></div>--%>
        <div class="span-3 blackbl last" style="width: 100px;">
            <a href="Testimonials.aspx" title="Client Testimonials">Testimonials</a></div>
		<div class="span-3 blackbl last" style="width:70px;"><a title="Contact" href="Contact.aspx">Contact</a></div>
		<br class="clear"><br class="clear">
		
       <h3 class="colbright" id="values">Our Core Values</h3>
		<div class="span-18 last">
		    <ul class="tunedul">
		        <li><h4 class="colblu mostwrap bold">Partner with Clients</h4>
		        <p>We believe that our first responsibility to the client is to act as a partner in their business, not just a contractor.
		        Our team is in the business to use our knowledge and skills to the benefit of the client and for the achievement of the client�s objectives.</p>
		        <li><h4 class="colblu mostwrap bold">Build Client's Capabilities</h4>
		        <p>It is our responsibility to transfer all the knowledge possible in order that our client become a self-sustaining,
		        able to continue to maintain, sustain, and improve.</p>
		        <li><h4 class="colblu mostwrap bold">Reflect Client's Values</h4>
		        <p>Because certain values are fundamental to each client, we incorporate and embody those values in the change management process.
		        We explicitly involve everyone in acting out these values during the project in order to accelerate cultural change.</p>
		        <li><h4 class="colblu mostwrap bold">Gap Assessment</h4>
		        <p>To provide a detailed, business-specific road map for continuous improvement, we believe in a thorough assessment of an organization,
		        its processes, and methods. It is with this, we can create the appropriate structure for effective, efficient operations.</p>
		        <li><h4 class="colblu mostwrap bold">Provide Architecture, Not Decisions</h4>
		        <p>We believe that our business is to provide our clients with the business architecture they will need for growth and
		        continued success in order that the client may make the best possible decisions for themselves.
		        That said, QPS implements and supports only those programs requested, based on our client�s decisions.
		        We take great pride in our successes; those based on true client needs and desires,
		        helping businesses realize true culture change, team effort, excitement for results and enjoyment of work.
		        This the reward of meaningful effort toward business excellence for which we strive.</p>
		    </ul>
		</div>
		</div>
		<div class="span-4 top">
	    <img src="../images/about_us.gif" class="pull-1 top">
	    <hr class="space">
	    <hr/>
	    <h4>Our Core Values</h4>
	    <hr/>
	    
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
	</div>
</asp:Content>
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/QPSSite.Master" CodeBehind="ViewRegistration.aspx.cs" Inherits="QPS.Registration.ViewRegistration" %>

<asp:Content ID="ContentRegister" ContentPlaceHolderID="cphContent" runat="server">

    <script language="javascript" type="text/javascript">
        setPage("Training");
      function setFocus()
      {
      document.getElementById("txtFname").focus();
      }    
    </script>
<%--<%=ConfigurationManager.AppSettings["ResumePath"] %><%# DataBinder.Eval(Container.DataItem,"ResumeGUID")%>--%>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top">
            <br class="clear"/>
            <h3 class="colbright" id="H3_1">
              View Registrations</h3>
            <form id="fRegister" runat="server">
                <div id="dreg" runat="server">
                    <div id="logout" style="position:relative; left:115%; top:-35px; width:0px;">
                        <asp:LinkButton ID="lnkLogout" runat="server"  OnClick="lnkLogout_Click">Logout</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkExport" runat="server"  OnClick="btnExport_click">Export</asp:LinkButton>
                     </div>
                    <table width="100%">
                        <tr>
                            <td>
                            <asp:Repeater ID="rptRegistration" runat="server" OnItemDataBound="R1_ItemDataBound" >
                                    <HeaderTemplate>
                                        <div style="height: 400px;">
                                            <table style="width:900px;" rules="none" align="left" cellpadding="5" cellspacing="0"
                                                border="2px">
                                                <tr style="background-color: Gray; color: White;">
                                                <td width="100px;" style="font-weight: bolder;"><asp:LinkButton ID="lnkdate" Runat="server" style="white-space:nowrap;color:White;" CommandArgument="RegistrationDate"  OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click"  >Date</asp:LinkButton>
                                                      </td>
                                                    <td width="100px;" style="font-weight: bolder;"><asp:LinkButton ID="lnkfname" Runat="server" style="white-space:nowrap;color:White;"  CommandArgument="FirstName"  OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click" >First Name</asp:LinkButton>
                                                        </td>
                                                    <td width="100px;" style="font-weight: bolder;white-space:nowrap;"><asp:LinkButton ID="lnklname" Runat="server" style="white-space:nowrap;color:White;" CommandArgument="LastName"  OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click"  >Last Name</asp:LinkButton>
                                                        </td>
                                                    <td style="font-weight: bolder;"><asp:LinkButton ID="lnkemail" Runat="server" style="white-space:nowrap;color:White;" CommandArgument="Email"  OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click"  >Email Address</asp:LinkButton>
                                                      </td>
                                                    <td style="font-weight: bolder; width:200px;"><asp:LinkButton ID="lnkcell" Runat="server" style="white-space:nowrap;color:White;" CommandArgument="CellPhone"  OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click"  >Cell Phone</asp:LinkButton>
                                                        </td>
                                                    <td width="200px;" style="font-weight: bolder;">
                                                        Attachments</td>
                                                    <td width="150px;" valign="middle" style="font-weight: bolder;"><asp:LinkButton ID="lnkstate" Runat="server" style="white-space:nowrap;color:White;"  CommandArgument="Careerstate" OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click" >Center State</asp:LinkButton>
                                                        </td>
                                                      <td width="320px;" valign="middle" style="font-weight: bolder;"><asp:LinkButton ID="lnkcourse" Runat="server" style="white-space:nowrap;color:White;"  CommandArgument="CourseNo" OnCommand="CommandBtn_Click"  CommandName="CommandBtn_Click" >Course Name</asp:LinkButton>
                                                        </td>
                                                </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                        <td   width="100px;" style="vertical-align:top;">
                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"RegistrationDate") %>'> </asp:Label>
                                                
                                            </td>
                                            <td width="100px;" style="vertical-align:top;">
                                                <asp:Label ID="lblfrstname" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FirstName")%>'> </asp:Label>
                                                
                                            </td>
                                            <td  width="100px;" style="vertical-align:top;">
                                                <asp:Label ID="lbllstname" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"LastName")%>'></asp:Label>
                                            </td>
                                            <td style="vertical-align:top;">
                                                <a href="Registration.aspx?RegID=<%#Eval("RegID")%>"><asp:Label ID="lblemail" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Email")%>'></asp:Label></a> 
                                            </td>
                                            <td style="vertical-align:top;">
                                                <asp:Label ID="lblPhone" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CellPhone")%>'></asp:Label>
                                            </td>
                                            <td  width="200px;" style="vertical-align:top;">
                                                <a href= "<%=ConfigurationManager.AppSettings["ResumeVPath"] %><%#DataBinder.Eval(Container.DataItem,"ResumeGUID") %>" title="<%# DataBinder.Eval(Container.DataItem,"Resume")%><%#DataBinder.Eval(Container.DataItem,"Extension") %>" ><asp:Label ID="lblResume" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ResumeUpdated")%>'></asp:Label></a> 
                                                <asp:Literal ID="data" runat="server"></asp:Literal>
                                                <asp:Label ID="lblRegid" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem,"RegID")%>'></asp:Label>
                                            </td>
                                            <td width="150px;"  style="vertical-align:top;">
                                                <asp:Label ID="lblCener" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Careerstate")%>'></asp:Label>
                                            </td>
                                            
                                            <td  width="320px;" style="vertical-align:top;">
                                                <asp:Label ID="lblCourse" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CourseNo")%>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8">
                                                <div style="background-color: Gray; height: 1px;">
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                    
                                     </table> </div>
                                   </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </div>
                <div align="center" style="padding-left:50%;">
                    <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnPrev" runat="server" Text=" << Prev " OnClick="btnPrev_Click"/> &nbsp; <asp:Button ID="btnNext" runat="server" Text=" Next >> " OnClick="btnNext_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:40px;">
                            <asp:label id="lblCurrentPage" Font-Bold="true" runat="server"></asp:label>
                        </td>
                    </tr>
                </table>
                </div>
            </form>
        </div>
        <%--<div class="span-4">
            <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>--%>
    </div>
</asp:Content>

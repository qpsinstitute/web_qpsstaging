﻿using System;
using System.Data;
using System.Data.Sql;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using Lib.Classes;
using log4net;
using log4net.Config;
using Lib.Businesslogic;
using System.Text.RegularExpressions;

namespace QPS.Registration
{
    public partial class FileData : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        DataTable dtSession;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Session_Table"] == null)
                {
                    dtSession = new DataTable("Session");
                    dtSession.Columns.Add("ID", typeof(int));
                    dtSession.Columns.Add("FileName", typeof(string));
                    dtSession.Columns.Add("FileExtension", typeof(string));
                    dtSession.Columns.Add("GuidName", typeof(string));
                    dtSession.Columns.Add("FileSize", typeof(int));
                    dtSession.Columns.Add("IsAsset", typeof(int));
                    Session["Session_Table"] = dtSession;
                    Session["Count"] = 0;
                }
                else
                    BindRepeater();
            }
            if (IsPostBack)
            {
                lblmsg.Text = "";
                if (Upload.Value != string.Empty)
                {
                    string strFileName;
                    string strFileExtension;
                    int intLastIndex;
                    string guidname;
                    int size;
                    try
                    {
                        if (Upload.Value != string.Empty)
                        {
                            dtSession = (DataTable)Session["Session_Table"];
                            int j = (int)Session["Count"];
                            if (j == 5)
                            {
                                lblmsg.Text = "Can not attach more than 5 files.";
                                goto a;
                            }
                            strFileName = Upload.PostedFile.FileName;
                            intLastIndex = strFileName.LastIndexOf("\\");
                            intLastIndex += 1;
                            strFileName = strFileName.Substring(intLastIndex, (strFileName.Length - intLastIndex));
                            strFileExtension = strFileName.Substring(strFileName.Length - 4, 4);

                            string filename = "";
                            string guidFilename = System.Guid.NewGuid().ToString();
                            filename = ConfigurationManager.AppSettings["Attachment_VirtualPath"].ToString() + guidFilename + Upload.Value.Substring(Upload.Value.LastIndexOf("."));
                            Upload.PostedFile.SaveAs(filename);
                            size = Upload.PostedFile.ContentLength;
                            guidname = guidFilename + Upload.Value.Substring(Upload.Value.LastIndexOf("."));

                            DataRow drFile = dtSession.NewRow();
                            drFile["ID"] = dtSession.Rows.Count + 1;
                            drFile["FileName"] = strFileName.ToString();
                            drFile["FileExtension"] = strFileExtension.ToString();
                            drFile["GuidName"] = guidname.ToString();
                            drFile["FileSize"] = size.ToString();
                            drFile["IsAsset"] = 0;
                            dtSession.Rows.Add(drFile);

                            if (dtSession.Rows.Count > 0)
                            {
                                Session["Session_Table"] = dtSession;
                                Session["Count"] = dtSession.Rows.Count;
                            }
                        }
                        Page.RegisterStartupScript("show", "<script>javascript:hidewait();</script>");
                    }
                    catch (Exception ex)
                    {
                        if (log.IsErrorEnabled)
                            log.Error("[" + System.DateTime.Now.ToString("G") + "] FileUpload::Page_Load - ", ex);
                        Response.Redirect("/errorpages/SiteError.aspx", false);
                    }
                    BindRepeater();
                a: { }

                }
            }
        }
        protected void BindRepeater()
        {
            dtSession = (DataTable)Session["Session_Table"];
            rptFileUpload.DataSource = dtSession;
            rptFileUpload.DataBind();
        }
        protected void rptFileUpload_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                dtSession = (DataTable)Session["Session_Table"];
                DataRow[] dr = dtSession.Select("ID=" + id);
                dtSession.Rows.Remove(dr[0]);
                Session["Session_Table"] = dtSession;
                Session["Count"] = dtSession.Rows.Count;
                BindRepeater();
            }
        }
        protected void CheckChanged(object s, EventArgs e)
        {
            CheckBox chk = (CheckBox)s;
            int id = Convert.ToInt32(hdnID.Value);
            dtSession = (DataTable)Session["Session_Table"];
            dtSession.DefaultView.RowFilter = "ID=" + id.ToString();
            if (dtSession.DefaultView.Count > 0)
            {
                dtSession.DefaultView[0].BeginEdit();
                if (chk.Checked == true)
                    dtSession.DefaultView[0].Row["IsAsset"] = 1;
                else
                    dtSession.DefaultView[0].Row["IsAsset"] = 0;

                dtSession.DefaultView[0].EndEdit();
                dtSession.DefaultView.RowFilter = "";
                Session["Session_Table"] = dtSession;
                Session["Count"] = dtSession.Rows.Count;
            }
        }
    }
}
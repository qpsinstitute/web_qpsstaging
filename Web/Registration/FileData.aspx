﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileData.aspx.cs" Inherits="QPS.Registration.FileData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function GetID(ID) {
            document.getElementById("hdnID").value = ID;
        }
        function showwait() {
          
            var filename;
            var temp, index;
            filename = document.frmupload.Upload.value;
            index = filename.lastIndexOf("\\");
            if (TestFileType(filename, [".doc", ".docx", ".pdf", ".jpeg", ".jpg", ".png", ".tiff", ".tif", ".bmp", ".gif"])) {
                index = index + 1;
                filename = filename.substring(index);
                // document.getElementById('waitfile').alt = "Attaching File.." + filename;
                document.getElementById('dvupload').style.display = 'none';
                document.getElementById('process').style.display = 'block';
                document.frmupload.submit();
            }
            else {
                document.getElementById("Upload").value = "";
            }
        }
        function hidewait() {
            document.getElementById('process').style.display = 'none';
            document.getElementById('dvupload').style.display = 'block';
        }

        function TestFileType(fileName, fileTypes) {
            if (!fileName) return;

            dots = fileName.split(".")
            //get the part AFTER the LAST period.
            fileType = "." + dots[dots.length - 1];

            return (fileTypes.join(".").indexOf(fileType) != -1) ?
            true:
            alert("Please upload following type of files only: \n\n .doc, .docx, .pdf, .jpeg, .jpg, .png, .tiff, .tif, .bmp, .gif\n\nPlease select suggested type of file and try again.");
        }
    </script>
</head>
<body style="margin-left: -3px;">
    <form id="frmupload" runat="server">
    <div id="dvupload">
        <table style="font-family: Verdana; font-size: 13px;">
            <tr>
                <td>
                    <asp:Label ID="lblmsg" runat="Server" ForeColor="red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="Upload" runat="server" type="file" onchange="javascript:showwait();" />
                    
                </td>
            </tr>
            <tr>
                <td>
                <div id="repdiv">
                    <asp:Repeater runat="server" ID="rptFileUpload" OnItemCommand="rptFileUpload_ItemCommand">
                        <HeaderTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" class="adtbl">
                                <br />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "FileName").ToString().Length <= 21 ? DataBinder.Eval(Container.DataItem, "FileName").ToString() : DataBinder.Eval(Container.DataItem, "FileName").ToString().Substring(0, 10) + "..." + DataBinder.Eval(Container.DataItem, "FileName").ToString().Substring(DataBinder.Eval(Container.DataItem, "FileName").ToString().Length - 10).ToString()%>
                                    &nbsp;<asp:ImageButton ID="Delete" runat="server" ImageUrl="~/images/trash.gif" CommandName="Delete"
                                        CommandArgument='<%#DataBinder.Eval(Container.DataItem, "ID").ToString() %>' />
                                </td>
                                <td style="padding-left: 20px;">
                                    <span onclick="GetID('<%#DataBinder.Eval(Container.DataItem, "ID").ToString() %>')">
                                        <asp:CheckBox ID="chk" runat="server" Visible="false" Text="Retain as project asset"
                                            Checked='<%#DataBinder.Eval(Container.DataItem, "IsAsset").ToString()== "0" ? false : true %>'
                                            OnCheckedChanged="CheckChanged" AutoPostBack="true" /></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table></FooterTemplate>
                    </asp:Repeater>
                </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="process" style="display: none;">
        <table>
            <tr>
                <td>
                    <img src="../images/loadattachment.gif" alt="...." />
                   <%-- <img id="waitfile" src="" alt="Please Wait" style="font-family: Verdana; font-size: 13px;" />--%>
                   
                </td>
            </tr>
        </table>
    </div>
    <input id="hdnID" runat="server" type="hidden" />
    </form>
</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Lib.BusinessLogic;
using Lib.Classes;
using log4net;
using System.IO;
using log4net.Config;
using Lib.Businesslogic;

namespace QPS.Registration
{
    public partial class ViewRegistration : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static DataTable dtRegUsers = null;
        public static DataTable dtRegUsersfile = null;
        protected static string SortDirections = "", SortExpression = "";
        string wClause = "";
        public int RowCount
        {
            get
            {
                if (ViewState["RowCount"] != null)
                    return (int)ViewState["RowCount"];
                else
                    return 0;
            }
            set
            {
                ViewState["RowCount"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           // SortDirections = "DESC";
         
            if (!IsPostBack)
            {
                SortExpression = "LastName";
                ViewState["SortOrder"] = "ASC";
                try
                {
                    if (Session["AdminID"] != null && Session["Email"] != null)
                        BindRepeater();
                    else
                        FormsAuthentication.RedirectToLoginPage();
                }
                catch (Exception ex)
                {
                    if (log.IsErrorEnabled)
                        log.Error("[" + System.DateTime.Now.ToString("G") + "] SignIn::lnkLogin_Click - ", ex);
                    Response.Redirect("/errorpages/SiteError.aspx", false);
                }
            }
        }
       
        protected void Sortfname(object sender, EventArgs e)
        {
            SortExpression = "FirstName";
            if (ViewState["SortOrder"] == null)
            {
                ViewState["SortOrder"] = "ASC";
            }
            else
            {
                if (ViewState["SortOrder"].ToString() == "ASC")
                {
                    ViewState["SortOrder"] = "DESC";
                }
                else
                {
                    ViewState["SortOrder"] = "ASC";
                }
            }

            BindRepeater();
            
        }
        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            SortExpression = e.CommandArgument.ToString();
            if (ViewState["SortOrder"] == null)
            {
                ViewState["SortOrder"] = "ASC";
            }
            else
            {
                if (ViewState["SortOrder"].ToString() == "ASC")
                {
                    ViewState["SortOrder"] = "DESC";
                }
                else
                {
                    ViewState["SortOrder"] = "ASC";
                }
            }
            BindRepeater();
        }
        protected void BindRepeater()
        {
                dtRegUsers = RegisterController.GetRegisterUsers(wClause);
                if (dtRegUsers.Rows.Count > 0)
                {
                    PagedDataSource objPDS = new PagedDataSource();
                    objPDS.AllowPaging = true;
                    objPDS.PageSize = 5;
                    objPDS.DataSource = dtRegUsers.DefaultView;
                    DataView dv = dtRegUsers.DefaultView;
                    //apply the sort on CustomerSurname column  
                    dv.Sort = SortExpression.ToString() + "  " + ViewState["SortOrder"];
                    
                    //save our newly ordered results back into our datatable  
                    dtRegUsers = dv.ToTable();
                      if (objPDS.Count > 0)
                        {
                            objPDS.CurrentPageIndex = RowCount; //- 1;
                            lblCurrentPage.Text = "Page: " + (RowCount+ 1).ToString() + " of " + objPDS.PageCount.ToString();
                            btnPrev.Enabled = !objPDS.IsFirstPage;
                            btnNext.Enabled = !objPDS.IsLastPage;
                            rptRegistration.DataSource = objPDS;
                            rptRegistration.DataBind();
                        }
                }
        }
        protected void R1_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {

            // This event is raised for the header, the footer, separators, and items.

            // Execute the following logic for Items and Alternating Items.
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Literal info = ((Literal)e.Item.FindControl("data"));
                dtRegUsersfile = RegisterController.GetRegisterFileByWClause("And RegID=" +int.Parse(((Label)e.Item.FindControl("lblRegid")).Text.ToString()));
                info.Text = string.Empty;
                if (dtRegUsersfile.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRegUsersfile.Rows.Count; i++)
                    {
                        info.Text += "<a href='" + ConfigurationManager.AppSettings["AttachmentVPath"] + dtRegUsersfile.Rows[i]["regfileGuid"].ToString() + "' Title='" + dtRegUsersfile.Rows[i]["regfilename"].ToString() + dtRegUsersfile.Rows[i]["Extension"].ToString() + "' >" + (dtRegUsersfile.Rows[i]["regfilename"].ToString().Length > 8 ? dtRegUsersfile.Rows[i]["regfilename"].ToString().Substring(0, 8) + "...." : dtRegUsersfile.Rows[i]["regfilename"].ToString()) + dtRegUsersfile.Rows[i]["Extension"].ToString() + "<br/>";
                    }
                }
            }
        }
        protected void btnPrev_Click(object sender, EventArgs e)
        {
            RowCount -= 1;
            BindRepeater();
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            RowCount += 1;
            BindRepeater();
        }
       
        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["AdminID"] != null && Session["Email"] != null)
                {
                    Session.Clear();
                    Session.Remove("AdminID");
                    Session.Remove("Email");
                }
                Response.Redirect("SignIn.aspx?rmvCookie=yes", false);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] QPS::lnkLogout_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void btnExport_click(object sender, EventArgs e)
        {
            DataTable dtExport = new DataTable();
            dtExport = RegisterController.GetToExport(wClause);
            ExportToSpreadsheet(dtExport);
        }
        public void ExportToSpreadsheet(DataTable table)
        {
            GridView gvUsers = new GridView();
            string attachment = "attachment; filename=Registered_Users.xls";
            Response.ClearContent();
            Response.ContentEncoding = Encoding.Unicode;
            Response.BinaryWrite(Encoding.Unicode.GetPreamble());
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gvUsers.DataSource = table;
            gvUsers.DataBind();
            gvUsers.RenderControl(htextw);
            Response.Write(stw.ToString());
            Response.End();
        }
        
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lib.BusinessLogic;
//using log4net;
using Lib.Businesslogic;
using System.Collections;

namespace QPS.Registration
{
    public partial class Registration : System.Web.UI.Page
    {
      //  protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static DataTable dtcity = null;
        public static DataTable dtstate = null;
        public static DataTable dtinfo = null;
        DataRow drinfo = null;
        public string WClause = "";
        protected string ServerQualifiedPath = "";
        int year, month;
        protected void Page_Load(object sender, EventArgs e)
        {

            // txtsession.Attributes.Add("readonly", "readonly");
            if (!IsPostBack)
            {
                BindLists();
                txtFname.Focus();
                getCourses();

                BindsessionLists();
                SetVerificationText();
                Session["Session_Table"] = null;

                if (Request.QueryString["RegID"] != "" && Request.QueryString["RegID"] != null)
                {
                    DateTime d = new DateTime();
                    BindviewsessionLists();
                    int RegID = Int32.Parse(Request.QueryString["RegID"]);
                    WClause = "AND RegID =" + RegID;
                    dtinfo = RegisterController.GetRegisterInfoByWClause(WClause);
                    DataRow drinfo = dtinfo.Rows[0];
                    Fname.Text = drinfo["FirstName"].ToString();
                    Lname.Text = drinfo["LastName"].ToString();
                    Email.Text = drinfo["Email"].ToString();
                    Phone.Text = drinfo["CellPhone"].ToString();
                    ddlviewCCenterState.SelectedValue = drinfo["Careerstate"].ToString();
                    Center.Text = drinfo["CareerCenter"].ToString();
                    txtviewccemail.Text = drinfo["CCounselorEmail"].ToString();
                    Counselor.Text = drinfo["CareerCounselor"].ToString();
                    if (Convert.ToString(drinfo["funding"]) != "")
                        chkfund.Checked = Convert.ToBoolean(drinfo["funding"]);
                    else
                    {
                        chkfund.Checked = false;
                      // ddleditcourse.SelectedItem.Text = drinfo["CourseNo"].ToString();
                        if (ddleditcourse.Items.FindByText("drinfo") != null)
                        {
                            ddleditcourse.Items.FindByText("drinfo").Selected = true;
                        }

                    }
                    if (drinfo["Session"].ToString() != "")
                    { 
                        DateTime dt = DateTime.Parse(drinfo["Session"].ToString());
                       // Sessiontxt.Text = dt.ToString("MM/dd/yyyy");
                        //ddlviewhour.SelectedItem.Text = dt.ToString("HH");
                        ddlYear.SelectedValue = dt.Year.ToString();
                        ddlMonth.SelectedValue = dt.Month.ToString();
                        ddlDay.SelectedValue = dt.Day.ToString();
                        ddlviewhour.SelectedValue = dt.ToString("HH");
                        ddlviewminute.SelectedValue = dt.ToString("mm");
                    }
                    //else
                    //   // Sessiontxt.Text = drinfo["Session"].ToString();
                    txtviewStaffing.Text = drinfo["Qpsstaffandreq"].ToString();
                    WorkExp.Text = drinfo["WorkExp"].ToString();
                    AcedemicBG.Text = drinfo["AcedemicBack"].ToString();
                    Certification.Text = drinfo["CurrentCerti"].ToString();
                    txtSentInfo.Text = drinfo["Information"].ToString();
                    txtMartix.Text = drinfo["CourseMetrix"].ToString();
                    txtBrochure.Text = drinfo["CourseBrochure"].ToString();

                    if (drinfo["AppointmentDatetime"] != null && drinfo["AppointmentDatetime"] != System.DBNull.Value)
                    {
                        //appointment.Style.Add("display", "none");
                        //time.Style.Add("display", "none");
                        d = (DateTime)drinfo["AppointmentDatetime"];
                        ddlappyear.SelectedValue = d.Year.ToString();
                        ddlappmonth.SelectedValue = d.Month.ToString();
                        ddlappday.SelectedValue = d.Day.ToString();
                        ddlHour.SelectedValue = d.Hour.ToString();
                        ddlMinute.SelectedValue = d.Minute.ToString();
                    }
                    if (drinfo["City"] != null && drinfo["City"] != System.DBNull.Value)
                    {
                        city.Style.Add("display", "none");
                        txtCity.Text = drinfo["City"].ToString();
                    }
                    if (drinfo["City"] != null && drinfo["City"] != System.DBNull.Value)
                    {
                        state.Style.Add("display", "none");
                        ddlState.SelectedValue = drinfo["State"].ToString();
                    }

                    tbledit.Style.Add("display", "block");
                }
                else
                    tblAdd.Style.Add("display", "block");
            }
        }

        public void SetVerificationText()
        {
            Random ran = new Random();
            int no = ran.Next();
            Session["Captcha"] = no.ToString().Substring(0, 4);
        }
        protected void CAPTCHAValidate(object source, ServerValidateEventArgs args)
        {
            if (Session["Captcha"] != null)
            {
                if (txtVerify.Text != Session["Captcha"].ToString())
                {
                    SetVerificationText();
                    args.IsValid = false;
                    return;
                }
            }
            else
            {
                SetVerificationText();
                args.IsValid = false;
                return;
            }

        }
        private void getCourses()
        {
            ddlcoursename.DataSource = CourseController.GetAllCourse_elearn();
            ddlcoursename.DataTextField = "CourseTitle";
            ddlcoursename.DataValueField = "CourseID";
            ddlcoursename.DataBind();
            ddlcoursename.Items.Add(new ListItem("- Select Course -", "0"));
            ddlcoursename.SelectedValue = "0";

        }
        private void BindCourses()
        {
            ddleditcourse.DataSource = CourseController.GetAllCourse_elearn();
            ddleditcourse.DataTextField = "CourseTitle";
            ddleditcourse.DataValueField = "CourseID";
            ddleditcourse.DataBind();
            ddleditcourse.Items.Add(new ListItem("- Select Course -", "0"));


        }



        protected bool checkUrl(String url)
        {

            Uri uri = null;
            if (!Uri.TryCreate(url, UriKind.Absolute, out uri) || null == uri)
            {
                //Invalid URL
                return false;
            }
            else
                return true;
        }


        public bool IsValidEmail(string Email)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            System.Text.RegularExpressions.Regex _Regex = new System.Text.RegularExpressions.Regex(strRegex);
            if (_Regex.IsMatch(Email))
                return (true);
            else
                return (false);
        }





        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //if (chkadddate())
                //{



                
                if (!Page.IsValid)
                {
                    return;
                }
            
                else{





                    WClause = "AND RegID =" + 0;
                    dtinfo = RegisterController.GetRegisterInfoByWClause(WClause);

                    drinfo = dtinfo.NewRow();
                    drinfo["FirstName"] = txtFname.Text.ToString().Trim();
                    drinfo["LastName"] = txtLname.Text.ToString().Trim();
                    drinfo["Email"] = txtemail.Text.ToString().Trim();
                    drinfo["CellPhone"] = txtmob.Text.ToString().Trim();
                    if (txtccenter.Text == "")
                        drinfo["CareerCenter"] = DBNull.Value;
                    else
                        drinfo["CareerCenter"] = txtccenter.Text.ToString();

                    if (ddlccstate.SelectedItem.Text.ToString() == "Select")
                        drinfo["Careerstate"] = DBNull.Value;
                    else
                        drinfo["Careerstate"] = ddlccstate.SelectedItem.Text;

                    if (txtstaff.Text == "")
                        drinfo["Qpsstaffandreq"] = DBNull.Value;
                    else
                        drinfo["Qpsstaffandreq"] = txtstaff.Text.ToString();

                    if (txtccemail.Text == "")
                        drinfo["CCounselorEmail"] = DBNull.Value;
                    else
                        drinfo["CCounselorEmail"] = txtccemail.Text.ToString();

                    if (txtccounselor.Text == "")
                        drinfo["CareerCounselor"] = DBNull.Value;
                    else
                        drinfo["CareerCounselor"] = txtccounselor.Text.ToString();

                    //if (txtcourse.Text== "")
                    //    drinfo["CourseNo"] = DBNull.Value;
                    //else
                    //    drinfo["CourseNo"] = txtcourse.Text.ToString();
                    drinfo["CourseNo"] = ddlcoursename.SelectedItem.Text;

                    if (ddlsessionyear.SelectedIndex > 0 && ddlsessionmonth.SelectedIndex > 0 && ddlsessionday.SelectedIndex > 0)
                    {
                        DateTime dateValue;
                        if ((ddlsessionhour.SelectedIndex > 0 || ddlsessionminute.SelectedIndex > 0))
                        {
                            dateValue = new DateTime(Int32.Parse(ddlsessionyear.SelectedValue.ToString()), Int32.Parse(ddlsessionmonth.SelectedValue.ToString()), Int32.Parse(ddlsessionday.SelectedValue.ToString()), Int32.Parse(ddlsessionhour.SelectedValue.ToString()), Int32.Parse(ddlsessionminute.SelectedValue.ToString()), 0);
                        }
                        else
                        {
                            dateValue = new DateTime(Int32.Parse(ddlsessionyear.SelectedValue.ToString()), Int32.Parse(ddlsessionmonth.SelectedValue.ToString()), Int32.Parse(ddlsessionday.SelectedValue.ToString()), 0, 0, 0);

                        }
                        drinfo["Session"] = dateValue;
                    }
                    else
                    {
                        drinfo["Session"] = DBNull.Value;
                    }
                    //if (Request[txtsession.UniqueID] != null)
                    //{
                    //    if (Request[txtsession.UniqueID].Length > 0)
                    //    {
                    //txtsession.Text = Request[txtsession.UniqueID];
                    //if (txtsession.Text != "")
                    //{
                    //    if ((ddlsessionhour.SelectedIndex > 0 || ddlsessionminute.SelectedIndex > 0))
                    //    {
                    //        DateTime dateValue = DateTime.Parse(txtsession.Text.ToString() + " " + ddlsessionhour.SelectedItem.Value + ":" + ddlsessionminute.SelectedItem.Value + ":" + "00");
                    //        drinfo["Session"] = dateValue;
                    //    }
                    //    else
                    //        drinfo["Session"] = txtsession.Text.ToString();
                    //}
                    //else
                    //    drinfo["Session"] = DBNull.Value;
                    //calex.SelectedDate = DateTime.Parse(Request[txtsession.UniqueID]);
                    //    }
                    //}
                    //else
                    //    drinfo["Session"] = DBNull.Value;


                    if (txtwexp.Text == "")
                        drinfo["WorkExp"] = DBNull.Value;
                    else
                        drinfo["WorkExp"] = txtwexp.Text.ToString();

                    if (txtaback.Text == "")
                        drinfo["AcedemicBack"] = DBNull.Value;
                    else
                        drinfo["AcedemicBack"] = txtaback.Text.ToString();

                    if (txtccerti.Text == "")
                        drinfo["CurrentCerti"] = DBNull.Value;
                    else
                        drinfo["CurrentCerti"] = txtccerti.Text.ToString();














                    if (ResumeUpload.FileName == "")
                    {
                        drinfo["Resume"] = DBNull.Value;
                        drinfo["ResumeGuId"] = DBNull.Value;
                        dtinfo.Rows.Add(drinfo);
                        RegisterController.UpdateRegisterUser(dtinfo);
                        Multipleattachment();
                        btnReset_Click(sender, e);
                        lblmsg.Text = "You are registered successfully.";
                    }

                    else
                    {
                        String Filename = ResumeUpload.FileName.ToString();
                        string Extention = System.IO.Path.GetExtension(ResumeUpload.PostedFile.FileName);
                        try
                        {
                            if (Extention == ".doc" || Extention == ".docx" || Extention == ".pdf")
                            {
                                if (Filename.Replace(Extention, "").Length >= 8)
                                    drinfo["Resume"] = ResumeUpload.FileName.Replace(Extention, "").ToString();
                                else
                                    drinfo["Resume"] = ResumeUpload.FileName.Replace(Extention, "").ToString();
                                drinfo["Extension"] = Extention;
                                drinfo["ResumeGuId"] = UploadResume(ResumeUpload);
                                lblresume.Text = "";
                                dtinfo.Rows.Add(drinfo);
                                RegisterController.UpdateRegisterUser(dtinfo);
                                Multipleattachment();
                                btnReset_Click(sender, e);
                                lblmsg.Text = "You are registered successfully.";
                            }
                            else
                                lblresume.Text = "please upload only doc,docx or pdf";
                        }
                        catch (Exception ex)
                        {
                            //if (log.IsDebugEnabled)
                            //    log.Error("[" + System.DateTime.Now.ToString("G") + "] Register::btnsubmit_Click() - ", ex);
                            Lib.BusinessLogic.LogManagerQps.Instance.WriteError("[" + System.DateTime.Now.ToString("G") + "] Register::btnsubmit_Click() - ", ex);
                            Response.Redirect("/errorpages/SiteError.aspx", false);
                        }
                        //}
                    }
                    SetVerificationText();
                    txtVerify.Text = "";
                }
            }
            catch (Exception ex)
            {
                //if (log.IsDebugEnabled)
                //    log.Error("[" + System.DateTime.Now.ToString("G") + "] Register::btnsubmit_Click() - ", ex);
                Lib.BusinessLogic.LogManagerQps.Instance.WriteError("[" + System.DateTime.Now.ToString("G") + "] Register::btnsubmit_Click() - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);

            }

        }
        protected Boolean chkadddate()
        {
            int res = 0;
            if (ddlsessionyear.SelectedIndex > 0 && ddlsessionmonth.SelectedIndex > 0 && ddlsessionday.SelectedIndex > 0)
            {
                DateTime dateValue;
                if ((ddlsessionhour.SelectedIndex > 0 || ddlsessionminute.SelectedIndex > 0))
                {
                    dateValue = new DateTime(Int32.Parse(ddlsessionyear.SelectedValue.ToString()), Int32.Parse(ddlsessionmonth.SelectedValue.ToString()), Int32.Parse(ddlsessionday.SelectedValue.ToString()), Int32.Parse(ddlsessionhour.SelectedValue.ToString()), Int32.Parse(ddlsessionminute.SelectedValue.ToString()), 0);
                    res = DateTime.Compare(dateValue, System.DateTime.Now);
                }
                else
                {
                    dateValue = new DateTime(Int32.Parse(ddlsessionyear.SelectedValue.ToString()), Int32.Parse(ddlsessionmonth.SelectedValue.ToString()), Int32.Parse(ddlsessionday.SelectedValue.ToString()), 0, 0, 0);
                    res = DateTime.Compare(dateValue, System.DateTime.Now.Date);
                }
            }
            if (res < 0)
            {
                lblerror1.Text = "Date/time should not be less than the present date/time.";
                return false;
            }
            else
            {
                lblerror1.Text = "";
                return true;
            }
        }
        public void Multipleattachment()
        {
            /* ADD MULTIPLE ATTACHMENT IN TO DATABASE BY DEEPAK PATEL */
            //FILE UPLOAD
            try
            {

                WClause = "AND registrationfileid =" + 0;
                DataTable dtSession;
                if (Session["Session_Table"] != null)
                {
                    dtSession = (DataTable)Session["Session_Table"];
                    if (dtSession.Rows.Count > 0)
                    {

                        dtinfo = RegisterController.GetRegisterInfoByWClause("AND Regid=(select Max(Regid) from registration)");
                        int regid = int.Parse(dtinfo.Rows[0][0].ToString());
                        string fileUpload;
                        for (int i = 0; i < dtSession.Rows.Count; i++)
                        {
                            fileUpload = dtSession.Rows[i]["FileName"].ToString();

                            String Extention = string.Empty;
                            Extention = System.IO.Path.GetExtension(fileUpload);
                            if (Extention != "")
                            {
                                DataTable dtRequest1 = RegisterController.GetRegisterFileByWClause(WClause);
                                DataRow drRequest1 = dtRequest1.NewRow();
                                drRequest1["Regid"] = regid;
                                drRequest1["regfilename"] = fileUpload.Replace(Extention, "");
                                drRequest1["Extension"] = Extention;
                                drRequest1["regfileGuid"] = dtSession.Rows[i]["GuidName"].ToString();
                                drRequest1["createdate"] = DateTime.Now;
                                dtRequest1.Rows.Add(drRequest1);
                                RegisterController.Updateregusterfile(dtRequest1);
                            }

                        }
                    }

                }
                //iframeupload.InnerHtml = "";
               // Page.RegisterStartupScript("show", "<script>javascript:hidewait();</script>");
            }
            catch (Exception ex)
            {
                //if (log.IsErrorEnabled)
                //    log.Error("[" + System.DateTime.Now.ToString("G") + "] EDIT PSR::upload file - ", ex);
                Lib.BusinessLogic.LogManagerQps.Instance.WriteError("[" + System.DateTime.Now.ToString("G") + "] EDIT PSR::upload file - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }

        }

        protected void BindLists()
        {
            ListItem l;
            for (int year = DateTime.Now.Year; year < DateTime.Now.Year + 15; year++)
            {
                l = new ListItem(year.ToString(), year.ToString());
                ddlsessionyear.Items.Add(l);
            }

            for (int month = 1; month <= 12; month++)
            {
                string month_name = "";



                if (month <= 9)
                {
                    l = new ListItem(month_name + " 0" + month.ToString(), month.ToString());
                    ddlsessionmonth.Items.Add(l);
                }
                else
                {
                    l = new ListItem(month_name + " " + month.ToString(), month.ToString());
                    ddlsessionmonth.Items.Add(l);

                }
            }
            for (int day = 1; day < 32; day++)
            {
                l = new ListItem(day.ToString(), day.ToString());
                ddlsessionday.Items.Add(l);

            }
            for (int hrs = 0; hrs <= 23; hrs++)
            {
                if (hrs <= 9)
                {
                    l = new ListItem("0" + hrs.ToString(), hrs.ToString());
                    ddlHour.Items.Add(l);
                }
                else
                {
                    l = new ListItem(hrs.ToString(), hrs.ToString());
                    ddlHour.Items.Add(l);
                }
            }
            for (int min = 0; min < 60; min += 5)
            {
                if (min <= 5)
                {
                    l = new ListItem("0" + min.ToString(), min.ToString());
                    ddlMinute.Items.Add(l);
                }
                else
                {
                    l = new ListItem(min.ToString(), min.ToString());
                    ddlMinute.Items.Add(l);
                }
            }

        }
        protected void BindsessionLists()
        {
            ListItem l;


            for (int year = DateTime.Now.Year; year < DateTime.Now.Year + 15; year++)
            {
                l = new ListItem(year.ToString(), year.ToString());
                ddlappyear.Items.Add(l);
            }

            for (int month = 1; month <= 12; month++)
            {
                string month_name = "";

            

                if (month <= 9)
                {
                    l = new ListItem(month_name + " 0" + month.ToString(), month.ToString());
                    ddlappmonth.Items.Add(l);
                }
                else
                {
                    l = new ListItem(month_name + " " + month.ToString(), month.ToString());
                    ddlappmonth.Items.Add(l);

                }
            }
            for (int day = 1; day < 32; day++)
            {
                l = new ListItem(day.ToString(), day.ToString());
                ddlappday.Items.Add(l);

            }
            for (int hrs = 0; hrs <= 23; hrs++)
            {
                if (hrs <= 9)
                {
                    l = new ListItem("0" + hrs.ToString(), "0" + hrs.ToString());
                    ddlsessionhour.Items.Add(l);
                }
                else
                {
                    l = new ListItem(hrs.ToString(), hrs.ToString());
                    ddlsessionhour.Items.Add(l);
                }
            }
            for (int min = 0; min < 60; min += 5)
            {
                if (min <= 5)
                {
                    l = new ListItem("0" + min.ToString(), "0" + min.ToString());
                    ddlsessionminute.Items.Add(l);
                }
                else
                {
                    l = new ListItem(min.ToString(), min.ToString());
                    ddlsessionminute.Items.Add(l);
                }
            }

        }
        protected void BindviewsessionLists()
        {
            ListItem l;
            for (int year = DateTime.Now.Year; year < DateTime.Now.Year + 15; year++)
            {
                l = new ListItem(year.ToString(), year.ToString());
                ddlYear.Items.Add(l);
            }

            for (int month = 1; month <= 12; month++)
            {
                string month_name = "";



                if (month <= 9)
                {
                    l = new ListItem(month_name + " 0" + month.ToString(), month.ToString());
                    ddlMonth.Items.Add(l);
                }
                else
                {
                    l = new ListItem(month_name + " " + month.ToString(), month.ToString());
                    ddlMonth.Items.Add(l);

                }
            }
            for (int day = 1; day < 32; day++)
            {
                l = new ListItem(day.ToString(), day.ToString());
                ddlDay.Items.Add(l);

            }
           
            for (int hrs = 0; hrs <= 23; hrs++)
            {
                if (hrs <= 9)
                {
                    l = new ListItem("0" + hrs.ToString(), "0" + hrs.ToString());
                    ddlviewhour.Items.Add(l);
                }
                else
                {
                    l = new ListItem(hrs.ToString(),  hrs.ToString());
                    ddlviewhour.Items.Add(l);
                }
            }
            for (int min = 0; min < 60; min += 5)
            {
                if (min <= 5)
                {
                    l = new ListItem("0" + min.ToString(), "0" + min.ToString());
                    ddlviewminute.Items.Add(l);
                }
                else
                {
                    l = new ListItem(min.ToString(), min.ToString());
                    ddlviewminute.Items.Add(l);
                }
            }

        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Session["Session_Table"] = null;
            lblmsg.Text = "";
            txtFname.Text = "";
            txtLname.Text = "";
            txtmob.Text = "";
            txtemail.Text = "";
          //  txtsession.Text = "";
            //txtcourse.Text = "";
            txtccounselor.Text = "";
            txtccerti.Text = "";
            txtccenter.Text = "";
            txtaback.Text = "";
            txtwexp.Text = "";
            lblresume.Text = "";
            txtccemail.Text = "";
            txtstaff.Text = "";
            lblerror1.Text = "";
            ddlccstate.SelectedIndex = 0;
            ddlsessionday.SelectedIndex = 0;
            ddlsessionmonth.SelectedIndex = 0;
            ddlsessionyear.SelectedIndex = 0;
            ddlsessionhour.SelectedIndex = 0;
            ddlsessionminute.SelectedIndex = 0;
            ddlcoursename.SelectedValue =  "0";
            chkfund.Checked = false;
            txtVerify.Text = "";
        }

        
        private string UploadResume(FileUpload avatar_file)
        {
            string guidFilename = System.Guid.NewGuid().ToString();
            guidFilename += avatar_file.FileName.Substring(avatar_file.FileName.LastIndexOf(@"."));
            ServerQualifiedPath = ConfigurationManager.AppSettings["ResumePath"] + guidFilename;
            avatar_file.PostedFile.SaveAs(ServerQualifiedPath);
            return guidFilename;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["RegID"] != null && Request.QueryString["RegID"] != "")
            {
                //if (chkdate())
                //{
                    int RegID = Int32.Parse(Request.QueryString["RegID"]);
                    WClause = "AND RegID =" + RegID;
                    dtinfo = RegisterController.GetRegisterInfoByWClause(WClause);
                    drinfo = dtinfo.Rows[0];
                    drinfo.BeginEdit();
                    drinfo["FirstName"] = Fname.Text.ToString().Trim();
                    drinfo["LastName"] = Lname.Text.ToString().Trim();
                    drinfo["Email"] = Email.Text.ToString().Trim();
                    drinfo["CellPhone"] = Phone.Text.ToString().Trim();

                    if (ddlviewCCenterState.SelectedItem.Value.ToString() == "Select")
                        drinfo["Careerstate"] = DBNull.Value;
                    else
                        drinfo["Careerstate"] = ddlviewCCenterState.SelectedItem.Value.ToString();

                    if (Center.Text == "")
                        drinfo["CareerCenter"] = DBNull.Value;
                    else
                        drinfo["CareerCenter"] = Center.Text.ToString();



                    if (Counselor.Text == "")
                        drinfo["CareerCounselor"] = DBNull.Value;
                    else
                        drinfo["CareerCounselor"] = Counselor.Text.ToString();

                    if (txtviewccemail.Text == "")
                        drinfo["CCounselorEmail"] = DBNull.Value;
                    else
                        drinfo["CCounselorEmail"] = txtviewccemail.Text.ToString();



                    if (txtviewStaffing.Text == "")
                        drinfo["Qpsstaffandreq"] = DBNull.Value;
                    else
                        drinfo["Qpsstaffandreq"] = txtviewStaffing.Text.ToString();




                    //if (txtcourse.Text== "")
                    //    drinfo["CourseNo"] = DBNull.Value;
                    //else
                    //    drinfo["CourseNo"] = txtcourse.Text.ToString();

                    drinfo["CourseNo"] = ddleditcourse.Items.FindByText("drinfo");
                   // drinfo["CourseNo"] = ddleditcourse.SelectedItem.Text;


                    //if (Request[Sessiontxt.UniqueID] != null)
                    //{
                    //    if (Request[Sessiontxt.UniqueID].Length > 0)
                    //    {
                    //        Sessiontxt.Text = Request[Sessiontxt.UniqueID];
                    //if (Sessiontxt.Text != "")
                    //{

                    //    if ((ddlviewhour.SelectedIndex > 0 || ddlviewminute.SelectedIndex > 0))
                    //    {
                    //        DateTime dateValue = DateTime.Parse(Sessiontxt.Text.ToString() + " " + ddlviewhour.SelectedItem.Value + ":" + ddlviewminute.SelectedItem.Value + ":" + "00");
                    //        drinfo["Session"] = dateValue;
                    //    }
                    //    else
                    //        drinfo["Session"] = txtsession.Text.ToString();
                    //}
                    //else
                    //{
                    //    drinfo["Session"] = DBNull.Value;
                    //}
                    //calex.SelectedDate = DateTime.Parse(Request[txtsession.UniqueID]);
                    //    }
                    //}


                    if (WorkExp.Text == "")
                        drinfo["WorkExp"] = DBNull.Value;
                    else
                        drinfo["WorkExp"] = WorkExp.Text.ToString();

                    if (AcedemicBG.Text == "")
                        drinfo["AcedemicBack"] = DBNull.Value;
                    else
                        drinfo["AcedemicBack"] = AcedemicBG.Text.ToString();

                    if (Certification.Text == "")
                        drinfo["CurrentCerti"] = DBNull.Value;
                    else
                        drinfo["CurrentCerti"] = Certification.Text.ToString();

                    drinfo["Information"] = txtSentInfo.Text;
                    drinfo["CourseMetrix"] = txtMartix.Text;
                    drinfo["CourseBrochure"] = txtBrochure.Text;
                    drinfo["CourseNo"] = ddleditcourse.Items.FindByText("drinfo");
                   // drinfo["CourseNo"] = ddleditcourse.SelectedItem.ToString();


                    //if (Request[txtappdate.UniqueID] != null)
                    //{
                    //    if (Request[txtappdate.UniqueID].Length > 0)
                    //    {
                    //        txtappdate.Text = Request[txtappdate.UniqueID];
                    //    }
                    //}

                    if (ddlYear.SelectedIndex > 0 && ddlMonth.SelectedIndex > 0 && ddlDay.SelectedIndex > 0)
                    {
                        DateTime dateValue;
                        if ((ddlviewhour.SelectedIndex > 0 || ddlviewminute.SelectedIndex > 0))
                        {
                            dateValue = new DateTime(Int32.Parse(ddlYear.SelectedValue.ToString()), Int32.Parse(ddlMonth.SelectedValue.ToString()), Int32.Parse(ddlDay.SelectedValue.ToString()), Int32.Parse(ddlviewhour.SelectedValue.ToString()), Int32.Parse(ddlviewminute.SelectedValue.ToString()), 0);
                        }
                        else
                        {
                            dateValue = new DateTime(Int32.Parse(ddlYear.SelectedValue.ToString()), Int32.Parse(ddlMonth.SelectedValue.ToString()), Int32.Parse(ddlDay.SelectedValue.ToString()), 0, 0, 0);
                        }
                        drinfo["Session"] = dateValue;
                    }
                    else
                    {
                        drinfo["Session"] = DBNull.Value;
                    }
                    DateTime date;
                     if (ddlappyear.SelectedIndex > 0 && ddlappmonth.SelectedIndex > 0 && ddlappday.SelectedIndex > 0)
                     {
                            if (ddlappyear.SelectedIndex > 0 && ddlappmonth.SelectedIndex > 0 && ddlappday.SelectedIndex > 0 && ddlHour.SelectedIndex > 0 && ddlMinute.SelectedIndex > 0)
                                date = new DateTime(Int32.Parse(ddlappyear.SelectedValue.ToString()), Int32.Parse(ddlappmonth.SelectedValue.ToString()), Int32.Parse(ddlappday.SelectedValue.ToString()), Int32.Parse(ddlHour.SelectedValue.ToString()), Int32.Parse(ddlMinute.SelectedValue.ToString()), 0);
                            else 
                                date = new DateTime(Int32.Parse(ddlappyear.SelectedValue.ToString()), Int32.Parse(ddlappmonth.SelectedValue.ToString()), Int32.Parse(ddlappday.SelectedValue.ToString()), 0, 0, 0);
                            drinfo["AppointmentDateTime"] = date;
                    }
                    else
                        drinfo["AppointmentDateTime"] = DBNull.Value;



                    drinfo["City"] = txtCity.Text;
                    drinfo["State"] = ddlState.SelectedValue;
                    drinfo["funding"] = chkfund.Checked;
                    drinfo.EndEdit();
                    lblError.Text = "";
                    lblsessionval.Text = "";
                    RegisterController.UpdateRegisterUser(dtinfo);
                    Multittachment(int.Parse(Request.QueryString["RegID"].ToString()));

                    Response.Redirect("ViewRegistration.aspx", false);
               // } 
            }
        }
        public Boolean chkdate()
        {

            //DateTime date;
            //int res = 0;
            //if (ddlappyear.SelectedIndex > 0 && ddlappmonth.SelectedIndex > 0 && ddlappday.SelectedIndex > 0 && ddlHour.SelectedIndex > 0 && ddlMinute.SelectedIndex > 0)
            //{
            //    date = new DateTime(Int32.Parse(ddlappyear.SelectedValue.ToString()), Int32.Parse(ddlappmonth.SelectedValue.ToString()), Int32.Parse(ddlappday.SelectedValue.ToString()), Int32.Parse(ddlHour.SelectedValue.ToString()), Int32.Parse(ddlMinute.SelectedValue.ToString()), 0);
            //    res = DateTime.Compare(date, System.DateTime.Now);
            //}
            //else if (ddlappyear.SelectedIndex > 0 && ddlappmonth.SelectedIndex > 0 && ddlappday.SelectedIndex > 0)
            //{
            //    date = new DateTime(Int32.Parse(ddlappyear.SelectedValue.ToString()), Int32.Parse(ddlappmonth.SelectedValue.ToString()), Int32.Parse(ddlappday.SelectedValue.ToString()), 0, 0, 0);
            //    res = DateTime.Compare(date, System.DateTime.Now);
            //}
            int res1 = 0;
            if (ddlYear.SelectedIndex > 0 && ddlMonth.SelectedIndex > 0 && ddlDay.SelectedIndex > 0)
            {
                DateTime dateValue;
                if ((ddlviewhour.SelectedIndex > 0 || ddlviewminute.SelectedIndex > 0))
                {
                    dateValue = new DateTime(Int32.Parse(ddlYear.SelectedValue.ToString()), Int32.Parse(ddlMonth.SelectedValue.ToString()), Int32.Parse(ddlDay.SelectedValue.ToString()), Int32.Parse(ddlviewhour.SelectedValue.ToString()), Int32.Parse(ddlviewminute.SelectedValue.ToString()), 0);
                    res1 = DateTime.Compare(dateValue, System.DateTime.Now);
                }
                else
                {
                    dateValue = new DateTime(Int32.Parse(ddlYear.SelectedValue.ToString()), Int32.Parse(ddlMonth.SelectedValue.ToString()), Int32.Parse(ddlDay.SelectedValue.ToString()), 0, 0, 0);
                    res1 = DateTime.Compare(dateValue, System.DateTime.Now.Date);    
                }
                
            }
            //if (res < 0 && res1 < 0)
            //{
            //    lblsessionval.Text = "Date/time should not be less than the present date/time.";
            //    lblError.Text = "Date/time should not be less than the present date/time.";
            //    return false;
            //}
            if (res1 < 0)
            {
                lblsessionval.Text = "Date/time should not be less than the present date/time.";
                lblError.Text = "";
                return false;
            }
            //else if (res < 0)
            //{
            //    lblError.Text = "Date/time should not be less than the present date/time.";
            //    lblsessionval.Text = "";
            //    return false;
            //}
            else
            {
                return true;
            }

        }

        public void Multittachment(int RegID)
        {
            /* ADD MULTIPLE ATTACHMENT IN TO DATABASE BY DEEPAK PATEL */
            //FILE UPLOAD
            try
            {

                WClause = "AND registrationfileid =" + 0;
                DataTable dtSession;
                if (Session["Session_Table"] != null)
                {
                    dtSession = (DataTable)Session["Session_Table"];
                    if (dtSession.Rows.Count > 0)
                    {

                        dtinfo = RegisterController.GetRegisterInfoByWClause("AND Regid=" + RegID);
                        int regid = int.Parse(dtinfo.Rows[0][0].ToString());
                        string fileUpload;
                        for (int i = 0; i < dtSession.Rows.Count; i++)
                        {
                            fileUpload = dtSession.Rows[i]["FileName"].ToString();

                            String Extention = string.Empty;
                            Extention = System.IO.Path.GetExtension(fileUpload);
                            if (Extention != "")
                            {
                                DataTable dtRequest1 = RegisterController.GetRegisterFileByWClause(WClause);
                                DataRow drRequest1 = dtRequest1.NewRow();
                                drRequest1["Regid"] = regid;
                                drRequest1["regfilename"] = fileUpload.Replace(Extention, "");
                                drRequest1["Extension"] = Extention;
                                drRequest1["regfileGuid"] = dtSession.Rows[i]["GuidName"].ToString();
                                drRequest1["createdate"] = DateTime.Now;
                                dtRequest1.Rows.Add(drRequest1);
                                RegisterController.Updateregusterfile(dtRequest1);
                            }

                        }
                    }

                }
                //iframeupload.InnerHtml = "";

            }
            catch (Exception ex)
            {
                //if (log.IsErrorEnabled)
                //    log.Error("[" + System.DateTime.Now.ToString("G") + "] EDIT PSR::upload file - ", ex);
                Lib.BusinessLogic.LogManagerQps.Instance.WriteError("[" + System.DateTime.Now.ToString("G") + "] EDIT PSR::upload file - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewRegistration.aspx", false);
        }
    }
}

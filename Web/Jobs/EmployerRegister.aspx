<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployerRegister.aspx.cs" Inherits="QPS.Jobs.EmployeeRegister" MasterPageFile="~/QPSSite.Master" %>
 
<asp:Content ID="ContentEmployerRegister" ContentPlaceHolderID="cphContent" runat="server">
<script language="javascript">
    setPage("Resources");
    </script>
    <script language="javascript" type="text/javascript">
      function setFocus()
      {
        document.getElementById("txtFname").focus();
      }    
    </script>
    
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <div class="span-3 blackbl last">
                <a href="JobPosting.aspx" title="Jobs">Jobs</a></div>
            <div class="span-4 blackbl last">
                <a title="Register" href="Register.aspx">Candidate Registration</a></div>
            <div class="span-4 blackbk last">
                <a title="Register">Employer Registration</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="H3_1">
                Employer Registration Form</h3>
            <form id="fRegister" runat="server">
                <div id="dreg" runat="server">
                    <table width="100%">
                       <tr>
                           <td colspan="2">
                                <a href ='<%= ConfigurationSettings.AppSettings["QPSAdminPath"].ToString() %>' target="_blank"> Employer Login </a>
                                <br /><br />
                           </td> 
                       </tr>
                       <tr>
                           <td colspan="2">
                                Fields marked with * are mandatory
                           </td>
                       </tr>
                       <tr>
                            <td></td>
                            <td>
                                <br />
                                <asp:Label ID="lblmsg" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                            </td>
                       </tr>
                       <tr>
                            <td align="right" width="200px">
                                First Name*
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtFname" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvFirstName" runat="server" ControlToValidate="txtFname" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Enter Text only" ValidationExpression="^([a-zA-Z ]*)$" ValidationGroup="register" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtFname"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Last Name*
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtLname" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvLastName" runat="server" ControlToValidate="txtLname" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Enter Text only" ValidationExpression="^([a-zA-Z ]*)$" ValidationGroup="register" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtLname"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email Address*
                            </td>
                            <td>
                                <asp:TextBox ID="txtemail" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvemail" runat="server" ControlToValidate="txtemail" ErrorMessage="*" Display="Dynamic" ValidationGroup="register" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionEmail" runat="server" ErrorMessage="Invalid Email Address" ControlToValidate="txtemail" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="register"></asp:RegularExpressionValidator>&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                               Password*
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtpassword" runat="server" Width="200px" MaxLength="50" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtpassword" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="right">
                              Confirm Password*
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtcpassword" runat="server" Width="200px" MaxLength="50" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtcpassword" Display="Dynamic" ErrorMessage="*"  SetFocusOnError="True" ValidationGroup="register"></asp:RequiredFieldValidator>&nbsp;
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtpassword" ControlToValidate="txtcpassword" ErrorMessage="Confirm passowrd must be same as password" SetFocusOnError="True" ValidationGroup="register" Display="Dynamic">Confirm passowrd must be same as password</asp:CompareValidator>
                            </td>
                        </tr>
                        
                        <tr>
                           <td valign="top">
                              Phone*</td>
                           <td align="left" nowrap="noWrap">
                                <asp:TextBox ID="txtPhone" runat="server" Width="200px" MaxLength="50" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="txtPhone" SetFocusOnError="true" ValidationGroup="register" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPhone" ErrorMessage="Enter Valid Phone Number" ValidationExpression="[^a-zA-Z ]*" Display="Dynamic" ValidationGroup="register"></asp:RegularExpressionValidator>
                           </td>
                        </tr>
                            
                        <tr>
                           <td valign="top">
                               Fax*</td>
                           <td align="left" nowrap="noWrap">
                               <asp:TextBox ID="txtfax" runat="server" Width="200px" MaxLength="50" ></asp:TextBox>
                               <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtfax"  Display="Dynamic" SetFocusOnError="true" ValidationGroup="register" ErrorMessage="*"></asp:RequiredFieldValidator>
                               <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtfax" ErrorMessage="Enter Valid Fax Numbar" ValidationExpression="[^a-zA-Z ]*" Display="Dynamic" ValidationGroup="register" ></asp:RegularExpressionValidator>
                           </td>
                        </tr>
                            
                        <tr>
                          <td valign="top">
                              Position*</td>
                          <td align="left" nowrap="noWrap">
                              <asp:TextBox ID="txtposition" runat="server" Width="200px" MaxLength="50" ></asp:TextBox>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtposition" SetFocusOnError="true"  ValidationGroup="register" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                              <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Enter Text only" ValidationExpression="^([a-zA-Z ]*)$" ValidationGroup="register" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtposition"></asp:RegularExpressionValidator>
                          </td>
                        </tr>
                            
                        <tr>
                            <td align="right">
                             Company*
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtcompany" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvcompany" runat="server" ControlToValidate="txtcompany" SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Enter only alphanumeric value" ValidationExpression="^[a-zA-Z0-9 ]+$" ValidationGroup="register" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtcompany"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <br />
                                <asp:Button ID="btnsubmit" runat="server" Text="Submit" ValidationGroup="register" OnClick="btnsubmit_Click" />&nbsp;
                                <asp:Button ID="btnReset" runat="server" Text="Reset"  OnClick="btnReset_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False" ValidationGroup="register" />
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
        <div class="span-4">
            <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
    </div>
</asp:Content>
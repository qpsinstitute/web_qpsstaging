using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using Lib.Classes;
using log4net;
using log4net.Config;

namespace QPS.Jobs
{
    public partial class JobDescription : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected static int Jobid;
        DataSet dsJob = new DataSet();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Jobid = Convert.ToInt32(Request["JobID"]);
            if (!IsPostBack)
            {
                try
                {
                    string WClause = "AND JobID =" + Jobid; 
                        rptdescript.DataSource =JobController.SelectJobPostingByWClause(WClause);
                        rptdescript.DataBind();
                }
                catch (Exception ex)
                {
                      if (log.IsDebugEnabled)
                          log.Error("[" + System.DateTime.Now.ToString("G") + "] JobDescription::Page_Load() - ", ex);
                      Response.Redirect("/errorpages/SiteError.aspx",false);
                }
            }

        }
    }
}
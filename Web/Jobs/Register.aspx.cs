using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using Lib.Classes;
using log4net;
using log4net.Config;

namespace QPS.Jobs
{
    public partial class Register : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static DataTable dtcity = null;
        public static DataTable dtstate = null;
        public static DataTable dtinfo = null;
        protected string ServerQualifiedPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtFname.Focus();
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            try
            {                
                string WClause = "AND CandidateID =" + 0;
                dtinfo = CandidateCotroller.GetCandidateByWClause(WClause);

                DataRow drinfo = dtinfo.NewRow();
                drinfo["FirstName"] = txtFname.Text.ToString().Trim();
                drinfo["LastName"] = txtLname.Text.ToString().Trim();
                drinfo["Street"] = txtAdd.Text.ToString().Trim();
                drinfo["City"] = txtcity.Text.ToString().Trim();
                drinfo["State"] = dpstate.SelectedItem.Text.ToString();
                if (txtzip.Text.ToString().Trim() == "")
                    drinfo["Zipcode"] = DBNull.Value;
                else drinfo["Zipcode"] = txtzip.Text.ToString().Trim();

                if (txtphone.Text.ToString().Trim() == "")
                    drinfo["Phone"] = DBNull.Value;
                else drinfo["Phone"] = txtphone.Text.ToString().Trim();

                drinfo["Mobile"] = txtmob.Text.ToString().Trim();
                drinfo["JobSkill"] = txtjob.Text.ToString().Trim();
                drinfo["Email"] = txtemail.Text.ToString().Trim();
                drinfo["CreatedDate"] = Convert.ToDateTime(System.DateTime.Now.ToString());
                drinfo["ModifiedDate"] = Convert.ToDateTime(System.DateTime.Now.ToString());
                drinfo["Status"] = true;
                if (ResumeUpload.FileName == "" && CletterUpload1.FileName == "")
                {
                    drinfo["Resume"] = DBNull.Value;
                    drinfo["ResumeGuId"] = DBNull.Value;
                    drinfo["CoverLetter"] = DBNull.Value; ;
                    drinfo["CoverLetterGUID"] = DBNull.Value;
                    dtinfo.Rows.Add(drinfo);
                    CandidateCotroller.UpdateCandidate(dtinfo);
                    btnReset_Click(sender, e);
                    lblmsg.Text = "You are Registered Successfully";
                }

                else
                {
                    String Filename = ResumeUpload.FileName.ToString();
                    String clettername = CletterUpload1.FileName.ToString();
                    string Extention = System.IO.Path.GetExtension(ResumeUpload.PostedFile.FileName);
                    string clettertExtention = System.IO.Path.GetExtension(CletterUpload1.PostedFile.FileName);
                    try
                    {                        
                            drinfo["CoverLetter"] = CletterUpload1.FileName;
                            drinfo["CoverLetterGUID"] = UploadCoverLetter(CletterUpload1);
                            lblCoverLetter.Text = "*";
                       
                            drinfo["Resume"] = ResumeUpload.FileName;
                            drinfo["ResumeGuId"] = UploadResume(ResumeUpload);
                            lblresume.Text = "";
                       
                            dtinfo.Rows.Add(drinfo);
                            CandidateCotroller.UpdateCandidate(dtinfo);
                            btnReset_Click(sender, e);
                            lblmsg.Text = "You are Registered Successfully";
                        
                    }
                    catch (Exception ex)
                    {
                        if (log.IsDebugEnabled)
                            log.Error("[" + System.DateTime.Now.ToString("G") + "] Register::btnsubmit_Click() - ", ex);
                        Response.Redirect("/errorpages/SiteError.aspx", false);

                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Register::btnsubmit_Click() - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);

            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            lblresume.Text = "";
            lblCoverLetter.Text = "";
            dpstate.SelectedIndex = 0;
            txtzip.Text = "";
            txtphone.Text = "";
            txtmob.Text = "";
            txtLname.Text = "";
            txtcity.Text = "";
            txtAdd.Text = "";
            txtemail.Text = "";
            txtFname.Text = "";
            txtjob.Text = "";
            lblmsg.Text = "";
            txtFname.Focus();
        }

        private string UploadResume(FileUpload avatar_file)
        {
            string guidFilename = System.Guid.NewGuid().ToString();
            guidFilename += avatar_file.FileName.Substring(avatar_file.FileName.LastIndexOf(@"."));
            ServerQualifiedPath = ConfigurationManager.AppSettings["ResumePath"] + guidFilename;
            avatar_file.PostedFile.SaveAs(ServerQualifiedPath);
            return guidFilename;
        }

        private string UploadCoverLetter(FileUpload cletter_file)
        {
            string guidFilename = System.Guid.NewGuid().ToString();
            guidFilename += cletter_file.FileName.Substring(cletter_file.FileName.LastIndexOf(@"."));
            ServerQualifiedPath = ConfigurationManager.AppSettings["CoverLetterPath"] + guidFilename;
            cletter_file.PostedFile.SaveAs(ServerQualifiedPath);
            return guidFilename;
        }

        private bool IsNumeric(string strText)
        {
            if (strText == "")
                return false;
            CharEnumerator ce = strText.GetEnumerator();
            bool ReturnValue = true;
            while (ce.MoveNext())
            {
                char SingleCharacter = ce.Current;
                if (Char.IsNumber(SingleCharacter) == false)
                {
                    if (SingleCharacter != Convert.ToChar("."))
                    {
                        ReturnValue = false;
                        break;
                    }
                }
            }
            return ReturnValue;
        }

    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using Lib.Classes;
using log4net;
using log4net.Config;

namespace QPS.Jobs
{
    public partial class JobPosting : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        DataSet dsJob = new DataSet();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               try
                {
                    DataSet dsJob = JobController.SelectAllJobPosting();
                    if (dsJob.Tables[0].Rows.Count > 0)
                    {
                        rptjobs.DataSource = dsJob;
                        rptjobs.DataBind();
                    }
                    else
                    {
                        lbldata.Text = " No Data found";
                    }
                }
                catch (Exception ex)
                {
                    if (log.IsDebugEnabled)
                        log.Error("[" + System.DateTime.Now.ToString("G") + "] JobPosting::Page_Load() - ", ex);
                    Response.Redirect("/errorpages/SiteError.aspx", false);
                }
            }

        }
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
    }
}

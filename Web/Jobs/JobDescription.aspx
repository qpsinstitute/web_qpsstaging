<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" Codebehind="JobDescription.aspx.cs"
    Inherits="QPS.Jobs.JobDescription" %>

<asp:Content ID="Contentdescription" ContentPlaceHolderID="cphContent" runat="server">

    <script language="javascript">
        setPage("Resources");

    </script>

    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <div class="span-3 blackbk last">
                <a href="JobPosting.aspx" title="Jobs">Jobs</a></div>
            <div class="span-4 blackbl last">
                <a href="Register.aspx" title="Candidate Register">Candidate Registration</a></div>
            <div class="span-4 blackbl last">
                <a href="EmployerRegister.aspx" title="Employer Register">Employer Registration</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="H3_1">
                Job Description</h3>
            <form id="fLogin" runat="server">
                <div id="dlogin" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Repeater ID="rptdescript" runat="server">
                                    <HeaderTemplate>
                                        <div style="height: 400px; overflow-y: auto;">
                                            <table style="width: 580px;" rules="none" align="left" cellpadding="5" cellspacing="0">
                                                
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                   <tr style="background-color: Gray; color: White; font-weight: bold;">
                                                    <td width="180px;" style="font-weight: bolder;">
                                                        <%# Eval("JobTitle") %></td>
                                                </tr>
                                        <tr>
                                            <td>
                                                <br />
                                                 <%# Eval("Description") %>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table> </div>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
        <div class="span-4">
            <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
       
    </div>
</asp:Content>

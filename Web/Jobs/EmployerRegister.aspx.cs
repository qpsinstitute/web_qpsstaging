using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using Lib.Classes;
using log4net;
using log4net.Config;

namespace QPS.Jobs
{
    public partial class EmployeeRegister : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static DataTable dtResult = null, dtExist = null;
        public string strExist = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtFname.Focus();
            }
        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                try
                {
                    string wclause = "And UserID =" + 0;
                    dtResult = UserController.GetEmployerByWhereClause(wclause);
                    DataRow drinfo = dtResult.NewRow();
                    drinfo["FirstName"] = txtFname.Text.ToString();
                    drinfo["LastName"] = txtLname.Text.ToString();
                    drinfo["Email"] = txtemail.Text.ToString();
                    drinfo["Password"] = txtpassword.Text.ToString();
                    drinfo["Status"] = true;
                    drinfo["RoleID"] = 2;
                    drinfo["Company"] = txtcompany.Text.ToString();
                    drinfo["Phone"] = txtPhone.Text.ToString();
                    drinfo["Fax"] = txtfax.Text.ToString().Trim();
                    drinfo["Position"] = txtposition.Text.ToString().Trim();
                    drinfo["CreatedDate"] = Convert.ToDateTime(System.DateTime.Now.ToString());
                    drinfo["CreatedBy"] = 1;
                    dtResult.Rows.Add(drinfo);
                    UserController.UpdateUsers(dtResult);
                    btnReset_Click(sender, e);
                    lblmsg.Text = "You are registered successfully";
                }
                catch (Exception ex)
                {
                    if (log.IsDebugEnabled)
                        log.Error("[" + System.DateTime.Now.ToString("G") + "] Register::btnsubmit_Click() - ", ex);
                    Response.Redirect("/errorpages/SiteError.aspx", false);
                }
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtFname.Text = "";
            txtLname.Text = "";
            txtemail.Text = "";
            txtcpassword.Text = "";
            txtpassword.Text = "";
            txtcompany.Text = "";
            txtfax.Text = "";
            txtposition.Text = "";
            txtPhone.Text = "";
            lblmsg.Text = "";
            txtFname.Focus();
        }

        public bool ValidateControls()
        {
            bool flag = true;
            if (Request.QueryString["UserID"] != null)
            {
                strExist = "And Email = '" + txtemail.Text + "' and UserID<>" + Request.QueryString["UserID"].ToString();
                dtExist = UserController.GetAllUsersByWhereClause(strExist);
                if (dtExist.Rows.Count > 0)
                {
                    lblmsg.Text = "This username is already registered, enter another username<br/>";
                    flag = false;
                }
                if (txtemail.Text.Trim().ToString() != string.Empty)
                {
                    strExist = "And Email = '" + txtemail.Text + "' and UserID<>" + Request.QueryString["UserID"].ToString();
                    dtExist = UserController.GetAllUsersByWhereClause(strExist);
                    if (dtExist.Rows.Count > 0)
                    {
                        lblmsg.Text = "This email address is already registered.<br/>";
                        flag = false;
                    }
                }
            }
            else
            {
                strExist = "And Email = '" + txtemail.Text + "'";
                dtExist = UserController.GetEmployerByWhereClause(strExist);
                if (dtExist.Rows.Count > 0)
                {
                    lblmsg.Text = "This username has already been taken, choose another username<br/>";
                    flag = false;
                }
                if (txtemail.Text.Trim().ToString() != string.Empty)
                {
                    strExist = "And Email = '" + txtemail.Text + "'";
                    dtExist = UserController.GetEmployerByWhereClause(strExist);
                    if (dtExist.Rows.Count > 0)
                    {
                        lblmsg.Text = "This email address is already registered.<br/>";
                        flag = false;
                    }
                }
            }
            if (flag == true)
                return true;
            else return false;
        }
    }
}

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Testimonials.aspx.cs"
    Inherits="QPS.Company.Testimonials" %>

<%@ Register TagPrefix="QPSSidebar" TagName="Company" Src="~/Controls/Sidebar_Company.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>
<asp:Content ID="ContentClients" ContentPlaceHolderID="cphContent" runat="server">
  
    <script language="javascript" type="text/javascript">
        setPage("Company");       
    </script>
    <style type="text/css">
        .quotediv div p span
        {
            display: inline;
            padding-left: 5px;
        }
        .quotediv div h6
        {
            font-size: 1.2em;
        }
    </style>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right; width: 650px;">
            <div class="span-3 blackbl last" style="width: 80px;">
                <a title="About QPS" href="about.aspx">About QPS</a></div>
            <div class="span-3 blackbl last" style="width: 180px;">
                <a title="Membership and Partnership" href="Membership.aspx">Membership and Partnership</a></div>
            <div class="span-3 blackbl last" style="width: 80px;">
                <a title="Our Staff" href="Staff.aspx">Our Staff</a></div>
            <div style="width: 90px;" class="span-3 blackbl last">
                <a title="Our Clients" href="Clients.aspx">Our Clients</a></div>
            <div class="span-3 blackbk last" style="width: 100px;">
                <a href="Testimonials.aspx" title="Testimonials">Testimonials</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="testimonials" title="Testimonials">
                Client Testimonials</h3>
            <hr />
            <p style="font-size: 125%">
                What Clients Are Saying About QPS:
            </p>
            <div class="span-18 last" style="width: 650px;">
                <div class="span-9 colborder" style="width: 85%">
                    <div class="quotediv">
                       
                        <div>
                            
                            <h6>
                                The QPS Course provided clear & practical steps on how Lean can be implemented to
                                improve Quality, reduce cost, and satisfy today�s demanding customers.</h6>
                            <p>
                                Ed Dolan, President<span>Dolan Industries</span></p>
                        </div>
                        <div>
                            <h6>
                                The most comprehensive, detailed information on Lean Deployment � truly remarkable
                                course on Lean.</h6>
                            <p>
                                Joan Forrer, Program Manager<span>Horizon Blue cross Blue shield</span></p>
                        </div>
                        <div>
                            <h6>
                                Lean Manufacturing course offered sound solutions to our problems. It provided new
                                insight how non-value added tasks can be reduced without much cost</h6>
                            <p>
                                Al Pizzo,Quality Manager<span>Kreisler corporation</span></p>
                        </div>
                        <div>
                            <h6>
                                QPS Six Sigma course taught us a wide variety of tools to improve and control processes.
                                It provided proven processes for improving the process. The course demonstrated
                                how Six Sigma can be deployed in the organization for breakthrough improvements
                                using tools, processes and people.</h6>
                            <p>
                                Todd Palmer, Manufacturing Manager<span>Master black Belt ; Vectron Corporation, Hudson,
                                    MA</span></p>
                        </div>
                        <div>
                            <h6>
                                Team Management Module of the �Lean Program� focused on how a team-based organization
                                can change the bottom line. I am impressed.</h6>
                            <p>
                                Linda Miller, Controller<span>Dolan Industries</span></p>
                        </div>
                        <div>
                            <h6>
                                QPS provided a logical approach for solving business issues by providing excellent
                                instruction & detailed class material on Process Mapping</h6>
                            <p>
                                Dave Mahr, Quality Manager<span>Sippican Inc.</span></p>
                        </div>
                        <div>
                            <h6>
                                ISO 9001:2000 and ISO 13485:2003 Internal Auditing course really improved my understanding
                                of Process Auditing. I believe I truly have an understanding now.</h6>
                            <p>
                                Karen Iorio, Dorector of Quality<span>Draeger</span></p>
                        </div>
                        <div>
                            <h6>
                                Class material is clear and easy to follow.</h6>
                            <p>
                                Peter Caputo<span>Boston Scientific</span></p>
                        </div>
                        <div>
                            <h6>
                                Consultant and Instructor, Jay Patel gave real-life experiences and examples that
                                were very useful, not just hypothetical situations ; He motivated and involved all
                                our employees to get ISO 9001:2000</h6>
                            <p>
                                Paul Lane, President<span>Luster-ON</span></p>
                        </div>
                        <div>
                            <h6>
                                Lean Expert training sessions were very thorough with a lot of good interaction.
                                Shop floor application exercises were also beneficial.</h6>
                            <p>
                                Matt Beemiller, Process Engineer<span>GST Autoleather</span></p>
                        </div>
                        <div>
                            <h6>
                                The fundamentals taught in the QPS Lean Expert program can be applied right away
                                to show improvements immediately.</h6>
                            <p>
                                Daniel Laforest, Manufacturing Engineer<span>A.J. Knott Manufacturing Co.</span></p>
                        </div>
                        <div>
                            <h6>
                                I really liked the exercises & explanation of the difference between "flow" and
                                "process map". This is a new concept for me to utilize at my company.</h6>
                            <p>
                                Jeanne Maciel, Project Specialist<span>Biogen Idec</span></p>
                        </div>
                        <div>
                            <h6>
                                The Internal Auditing course was very informative, entertaining and up-beat. We
                                will be able to utilize all the information to audit our process.</h6>
                            <p>
                                Martha Orants, Q. A. Supervisor<span>ESI</span></p>
                        </div>
                        <div>
                            <h6>
                                I liked the way the instructor made analogies between ISO auditing and the real
                                world. His sense of humor kept me engaged to what we were learning.</h6>
                            <p>
                                Ryan O�Connell, Technician<span>ESI</span></p>
                        </div>
                        <div>
                            <h6>
                                The cross-functional auditing will enhance my company�s ability to perform and review
                                audits from many perspectives.</h6>
                            <p>
                                Karl Krainski, Quality Engineer<span>Polaris</span></p>
                        </div>
                        <div>
                            <h6>
                                The Internal Auditing course was interesting, provided good detail, and the instructor
                                kept the pace moving along at a good rate.</h6>
                            <p>
                                Norm Forand, Quality Assurance Manager<span>Cametoid Technologies</span></p>
                        </div>
                        <div>
                            <h6>
                                I worked with Jay to help deploy Lean Six Sigma (LSS) at MassMutual. Jay provided
                                training, project leadership and coaching to our staff. His enthusiasm and expertise
                                helped us get immediate results. Jay is a high-energy individual who generated excitement
                                for contiuous improvement throughout our organization. He helped us deliver a balanced
                                portfolio of Lean and DMAIC initiatives for maximum impact.</h6>
                            <p>
                                Sue Drummey</p>
                        </div>
                        <div>
                            <h6>
                                I�ve worked with Jay Patel for many years. During this time I�ve learned of, and
                                seen first hand, his great respect for both clients and students and his desire
                                to give them great value. His new training location in Marlboro, MA is an example
                                of his success built on customer satisfaction. He has been able to move his training
                                programs into a superior facility creating a venue that contributes to and enhances
                                the learning environment. Jay also gives back to the profession. His role in sustaining
                                NEQC and managing the bi-annual conference demonstrates his commitment to the strength
                                of the quality profession in the Northeast.</h6>
                            <p>
                                Dan O'Leary, President<span>Ombu Enterprises, LLC.</span></p>
                        </div>
                        <div>
                            <h6>
                                Jay was my instructor for the Green Belt Certification class conducted at Horizon
                                Blue Cross Blue Shield of NJ. The classes were in intense but the learning was easily
                                understood and retained. Jay also made the classes exciting and was extremely cooperative,
                                attentive to all the students.</h6>
                            <p>
                                Dawn Bradley, Business Analyst<span>Horizon Blue Cross Blue Shield</span></p>
                        </div>
                        <div>
                            <h6>
                                I have known Jay from a Process Mapping class in Millipore. I was fortunate enough
                                to have the honor to attend his QPS classes for the Master Expert package of the
                                Six Sigma Methodology. This training acknowledgment makes people a lot more marketable
                                in times, when there is a big demand for Six Sigma Black Belt experts, to improve
                                and endure the businesses. This is the best opportunity to show gratitude to him
                                and his talented staff.</h6>
                            <p>
                                Sarkis Karakozian</p>
                        </div>
                        <div>
                            <h6>
                                Jay and his team definitely get the job done. Our company received our ISO9001 registration
                                on the first try with zero non-conformance items. His team also got us though ISO14000
                                registration. We are also in the middle of the TL9000 and ISO18000 registration
                                process.</h6>
                            <p>
                                Bob Kelly</p>
                        </div>
                        <div>
                            <h6>
                                After losing my job in May 2009 to a plant closure, I was referred to Jay's company,
                                QPS by Massachusetts DET. QPS offered certifications in the areas that I felt would
                                make me more marketable to employers. I have been in the Master Expert program since
                                September and have found Jay and his other instructors to be excellent. They have
                                great knowledge of the subject matter, are always available for extra help, and
                                teach the class at a level that everyone can understand. Jay has also provided valuable
                                help with resume writing to help insure sucess when I complete the program.</h6>
                            <p>
                                Erik Aho</p>
                        </div>
                        <div>
                            <h6>
                                As a new Black Belt (circa. 2002), Mr. Jay Patel gave me my first break at a training
                                contract with Quality & Productivity Solutions (QPS). He entrusted in me the confidence,
                                which has led to my success today. It was also through Jay and his generosity that
                                I was able complete my training as a Master Black Belt, again through QPS, and in
                                return I have been able to provide him with training support for his highly successful
                                organization. He is an excellent colleague and a friend, and it is my pleasure to
                                provide him with the recommendation.</h6>
                            <p>
                                E. George Woodley, Lean Six Sigma Master Black Belt, American Society for Quality</p>
                        </div>
                        <div>
                            <h6>
                                Jay and his associates at QPS, Inc. were integral to the development and implementation
                                of Lean / Six Sigma methodology in my business operation. He provided an overall
                                strategy, green belt and black belt training, and project oversight that resulted
                                in breakthrough business process improvements highly recognized by our customers.
                                Jay has both academic and practical experience in these process improvement methodologies
                                that he uses to effectively train while positioning organizations to become independent,
                                ongoing practitioners of LEAN and Six Sigma. I highly recommend Jay and his company
                                for these services.</h6>
                            <p>
                                Alan Taylor</p>
                        </div>
                        <div>
                            <h6>
                                I�ve worked with Jay for many years. In QPS, he has been an example for a training
                                organization. He has held the best interests of the students in mind as he developed
                                course work and expanded his offerings. More than once, students have commented
                                that when they have problems, conflicts, or issues, they can take them to Jay and
                                he will help. His flexibility and concern make him a great person to work with.
                                I enjoy him as both a colleague and a collaborator.</h6>
                            <p>
                                Dan O'Leary, President,<span>Ombu Enterprises, LLC.</span></p>
                        </div>
                        <div>
                            <h6>
                                QPS is an outstanding student-focused quality training and consulting company. I
                                hired one of his six-sigma black belt students into the company I work for, Philips
                                Healthcare. In addition, Jay devotes much of his time to mentoring people and advancing
                                quality improvement.</h6>
                            <p>
                                Rob Michaels, Six Sigma Black Belt Trainer,<span>Quality & Productivity Solutions Inc.</span></p>
                        </div>
                        <div>
                            <h6>
                                The new system that we released for Dispensing is going very well and is driving
                                more ownership of the process and outputs. I appreciate the input for Dan and you
                                that helped us to get where we are.</h6>
                            <p>
                                Mike,<span>CareFusion</span></p>
                        </div>
                    </div>
                </div>
                <br />
                <h3 id="H3_1" class="colbright" title="Testimonials">
                    Student Testimonials</h3>
                <hr />
                <p style="font-size: 125%">
                    What our Students say about:
                </p>
                <p>
                </p>
                <br />
                <div class="span-9 colborder" style="width: 85%">
                    <div class="quotediv">

                    <div style="font-style: italic">
                            <h6>
                              QPS Staff are friendly, professional, and business-experienced.  They are committed to the success of their students.  QPS Staff are available after hours and helped with tours, additional text recommendation and professional advice.  <br /> <br />
The QPS environment is professional, spacious, well-lit and professionally maintained.  Technology is current.  Class materials are concise, comprehensive and complete, easy to follow and understand.  The exam prep, exams and post exam follow-up are very beneficial towards retaining knowledge. <br /><br />
I relied on the QPS approach 100% as they know how to prepare students for testing.  To pass the exams, it�s important to follow the guidance of the instructors.  The QPS training style is logical, well thought out and beneficial.  QPS overall guidance is very beneficial.  Instructors are available as long and as much as needed.   <br /><br />
QPS prepared for exam day fully and completely.  I was confidently prepared for each test.  The attributes of QPS are dedication and success of the student (is) evident with each instructor and employee.  The entire QPS environment is one of education, success and fun.  The staff, training, curriculum and resources are the precise solution for knowledge and test prep.<br /><br />
My experience with QPS is one of complete satisfaction.</h6>
                            <p>
                                Rick Burnett, <span>Master Expert Program Participant, Spring 2011
                                <br />on programs:
                                    <br />
                                    Agile Certified Practitioner Prep (PMI - ACP)<sup>SM</sup><br />
                                    Project Management Professional (PMI-PMP�)<br />
                                    Six Sigma Black Belt & Green Belt (ASQ-SSBB/GB)<br />
                                    Supply Chain Professional (APICS-CSCP)<br />
                                    Lean Expert (SME-LEAN)<br />
                                    ISO 9001:2008<br />
                                    </span></p>
                        </div>
                      </div>
                </div>
                 <div class="span-9 colborder" style="width: 85%">
                    <div class="quotediv">
   
                     <div>
                            <h6>
                                Happy seventeenth. May you have many more. I wish you only the best. You have helped
                                so many people in their careers including me for one. You are a gifted man. I truly
                                enjoy the friendship we have. <br />Take Care.</h6>
                            <p>
                                Joseph Sbrogna, QC Manager & Supplier Quality Engineer</p>
                        </div>
</div>
</div>
            <div class="span-9 colborder" style="width: 85%">
                    <div class="quotediv">



                        <div style="font-style: italic">
                            <h6>
                                When I was seeking employment in 2010, I was very frustrated in the requirements
                                that employers were seeking from prospective candidates. They were looking for certified
                                green belts, black belts, CQA and CQEs. I did not have any of those certifications.
                                So I decided to obtain all these certifications. I became a LSSGB, LSSE, LSSBB,
                                CQA and trained for the CQE. These certifications opened a world of employment for
                                me. I chose to train at QPS for three reasons: One was a very personal reason. When
                                I spoke with Jay, I was unemployed. For Jay it was not about the money for the courses.
                                He helped me get through the finances. He truly cares about people. I always said
                                that knowledge is power. The second reason was the instructors. They all come from
                                some branch of industry. Lastly, it was the proximity from my home.
                                <br />
                                <br />
                                Jay and Jack are a credit to their field. I gained much insight from their experiences.
                                Both were very patient in their training styles. They were able to answer any question
                                no matter how unimportant you thought it was. Their patience allowed me to better
                                understand the materials. For me it was not the Primer or class room materials.
                                It was the tutoring and mentoring from Jay and Jack that helped me pass this exam.
                                Jay showed me how to prepare for this exam and Jack taught me the higher level mathematics
                                and or statistics. Jay�s shortcuts to learning, study tips, and what to really focus
                                on was a great help to me. Not having to memorize the Primer book but to know how
                                to locate the information was most important to me. I did everything they told me
                                to do.
                                <br />
                                <br />
                                I found the QPS training fun and resourceful. Each and every day was exciting. Something
                                different was taught every day. Their hands on experience, made you think as to
                                solve real life, industry problems. I looked forward to doing the homework assignments.
                                QPS�s time management, course flexibility, consistency, and their standardized approach
                                to training aided me directly in comprehending the amount of material needed to
                                be learned for all the certifications. QPS had the confidence in me that I would
                                pass any and all tests. They made a believer out of me and made me believe in myself�with
                                all my studying, Jay�s and Jack�s words from their mouths, with confidence, (where)
                                �you will pass this exam�, and I did.
                                <br />
                                <br />
                                Obtaining all my certifications, I now am a QC Manager for a medical device company
                                with three direct reports. I am also the company�s only supplier quality engineer.
                                I am very lucky. Not only do I get to work with suppliers, but I get to see their
                                product. In today�s tough, job market, you need to set yourself apart from all the
                                other job seeking candidates. <b>Take a chance and obtain as many certifications as
                                    possible.</b> Do not think you got what it takes, because you don�t. By becoming
                                certified, you make yourself more marketable. I would recommend QPS to anyone. Remember
                                knowledge is power and that is something no one can take away from you.
                            </h6>
                            <p>
                                Joseph Sbrogna,<span>Master Expert Program Participant, Winter 2011<br />
                                    on programs:
                                    <br />
                                    Six Sigma Black Belt & Green Belt (ASQ-SSBB/GB)
                                    <br />
                                    Supply Chain Professional (APICS-CSCP)<br />
                                    Lean Expert (SME-LEAN)
                                    <br />
                                    Certified Quality Auditor (ASQ-CQA)</span></p>
                        </div>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <div class="quotediv">
                        <div style="font-style: italic">
                            <h6>
                                Passing exams at QPS was a great feeling. However, the best was to pass the external
                                exams (PMI, ASQ). Each and every QPS member contributed to the success: they have
                                different styles and background, but there was one single goal: to help the student
                                (answering questions on time, offering help to select best courses for the individual,
                                etc.) The friendly, respectful environment strengthened self confidence, and motivates
                                students. Jay put me to a friendly challenge to pass PMI and ASQ exams in a short
                                window. That was my key and best motivation. Thank you all for providing (a) very
                                valuable education. I made a good choice when I signed up for the training.
                            </h6>
                            <p>
                                Kornel Mezo,<span>Master Expert Program Participant, Summer 2011<br />
                                    on programs:
                                    <br />
                                    Agile Certified Practitioner Prep (PMI - ACP)<sup>SM</sup>
                                    <br />
                                    Project Management Professional (PMI-PMP�)
                                    <br />
                                    Risk Management Professional (PMI-RMP�)
                                    <br />
                                    Six Sigma Black Belt & Green Belt (ASQ-SSBB/GB)
                                    <br />
                                    Lean Expert (SME-LEAN)
                                    <br />
                                    ISO 9001:2008</span></p>
                        </div>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <div class="quotediv">
                        <div style="font-style: italic">
                            <h6>
                                My experiences at your training facility were excellent. The instructors and fellow
                                students made learning enjoyable. The guidance, support, and encouragement were
                                great. I think the opportunity to go back to a class or the invitation to ask for
                                help in a lean project in the future is an asset to your organization. The one-on-one
                                mentoring and coaching is also a great feature. Once I started taking classes I
                                found the staff great and that they were providing me with the tools I needed to
                                obtain a job. The instructors were well versed in the subject matter. I found the
                                instructors very approachable and friendly and they took each students question
                                seriously. The instructors welcomed participation from the students and took the
                                time to answer questions until the concept was understood. The instructors also
                                provided real life experiences and situations which helped the student learn the
                                materials. The material covered was just what I needed from a skills perspective.
                                The setting for learning was top notch and I felt very comfortable with the facility.
                                The visual materials helped reinforce learning the concepts. The access to a computer
                                and being able to take it home to study mini-tab during my class was great. A major
                                aspect of your training was that there was no �out of pocket costs� to the student.
                                As I completed courses and updated my resume the phone rang more and I was getting
                                more interviews. This program helped me get the interview and ultimately a job.
                                Shortly after I finished the program I was offered a job. I would definitely recommend
                                your program to prospective students. The instructors engage the students and the
                                program curriculum provided me with the skills I needed to obtain a job in this
                                tough job market.
                            </h6>
                            <p>
                                Michael Anastas, <span>Master Expert Program Participant, Summer 2011<br />
                                    on programs:
                                    <br />
                                    Project Management Professional (PMI-PMP�)
                                    <br />
                                    Six Sigma Black Belt & Green Belt (ASQ-SSBB/GB)<br />
                                    Supply Chain Professional (APICS-CSCP)<br />
                                    Lean Expert (SME-LEAN)<br />
                                    ISO 9001:2008</span></p>
                        </div>
                    </div>
                </div>
                
                <div class="span-9 colborder" style="width: 85%">
                    <div class="quotediv">
                        <div style="font-style: italic">
                            <h6>
                                There is so much to learn through the teachings of the instructors and stories of
                                the other students. QPS teaches about building lasting relationships, getting involved,
                                giving back, and to look for opportunities where one might never look. Simply being
                                involved with QPS has given me a new outlook on what education and life is all about!
                                Thank you!
                            </h6>
                            <p>
                                Karen Szczur, <span>Management Certification for Manufacturing Professionals Program
                                    Participant, Winter 2011<br />
                                    on programs:
                                    <br />
                                    Six Sigma Black Belt & Green Belt (ASQ-SSBB/GB)<br />
                                    Supply Chain Professional (APICS-CSCP)
                                    <br />
                                    Lean Expert (SME-LEAN)<br />
                                    ISO 9001:2008</span></p>
                        </div>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <div class="quotediv">
                        <div style="font-style: italic">
                            <h6>
                                QPS training and knowledge made a positive impact on my life. I am not only working
                                with SharePoint, but I am teaching courses because of the experience I gained from
                                QPS. I am developing a PMO because of the PMP credentials and confidence from the
                                exposure to the materials, and I am applying Lean, with SharePoint, as the prime
                                automation tool and plan to use some data analysis I learned in QPS six sigma class.</h6>
                            <p>
                                David McCordick, <span>CISSP, PMP | SharePoint Business Analyst
                                    <br />
                                    GraceHunt | A Microsoft SharePoint and Dynamics Gold Partner</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <div class="quotediv">
                        <div style="font-style: italic">
                            <h6>
                                I just wanted to take a moment to thank all of you at QPS. Jay and Jack are tremendous.
                                I am currently working at a small CM in Northborough named O.R.M. as the Production
                                Manager. I know that the biggest reason I was offered this opportunity was because
                                of the education I received from your team.
                            </h6>
                            <p>
                                Paul Oliveira
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <font size="2px" style="font-style: italic"><b>Lean Six Sigma</b></font>
                    <div class="quotediv">
                        <font style="font-style: italic">
                            <div>
                                <h6>
                                    The course moves rapid and keeps my interest. The use of the tools and instructors�
                                    leadership was very helpful in understanding material.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    This is a great fast-paced course. The instructors provide great real-world knowledge
                                    and examples.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    I really enjoyed the DOT Enterprises Lab. The facilitator role gave me an extra
                                    opportunity to get hands on and learn.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    The training is for knowledge and skill that are currently in demand.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Extremely helpful and detailed course. Very knowledgeable instructors. I enjoyed
                                    it.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    The course is helpful and provides useful information with direct impact on the
                                    student�s ability to pass the tests.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Excellent teacher--calm, patient demeanor and encourages questions and participation.
                                    Very well paced and thorough.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    The instructor and training material for the course are excellent. The hands on
                                    group activities were particularly helpful to master many of the concepts.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    The instructor consistently engage the class and demonstrated key concepts using
                                    real world experience. I would highly recommend this course and instructor. The
                                    examples and experience were great.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    It is a good program for those who want to get advanced certificates in quality.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Attained Lean Six Sigma Black Belt training from QPS Inc. The training program is
                                    dynamic and well paced. The training extended beyond the stated curricula to cover
                                    resume writing, job search and even interviewing skills. Jay realized that getting
                                    a certificate in LSSBB was only the first step in acquiring jobs and ensured the
                                    next steps were well covered. Jay's knowledge of Lean Six Sigma concepts is phenominal
                                    and his willingness and ability to transfer this knowledge is a credit to him and
                                    his company. Jay is a great leader and an inspiration to me
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    I have known Jay for over 15 years. Jay Patel organized a first-class ISO 9001:2000
                                    training session with RAB Registrar TUV in 1999. This meeting was my first exposure
                                    to Six-Sigma and Lean principles. Needing official certification and training in
                                    Six-Sigma around 2005, I turned to Jay Patel and his fine organization- Quality
                                    and Productivity Solutions. Jay brought state of the art examples and knowledge
                                    to these informative training sessions. With Jay's contacts in the Quality industry,
                                    he was able to bring in very knowledgeable Guest lecturers. Jay and his company
                                    are highly recommended!
                                </h6>
                                <p>
                                </p>
                            </div>

                              <div>
                                <h6>
                                   I would like to express my sincere gratitude for all the efforts and guidance you have taken during training period.
                                   I really appreciated the way in which you helped us during Lean Six Sigma classes with your extended knowledge and professional experience,
                                   with your sincere and thoughtful insight which will be great inspiration to grow forward. 
                                </h6>
                                <p>Adrian Gugushi
                                </p>

                               




                            </div>














                        </font>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <font size="2px" style="font-style: italic"><b>Management Systems (ISO & Related)</b></font>
                    <div class="quotediv">
                        <font style="font-style: italic">
                            <div>
                                <h6>
                                    Very helpful and the education will be useful.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Very good auditor course with good examples and forms for everyday use.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Excellent Coverage of the ISO 13485 Standard. I enjoyed learning about the differences
                                    between ISO 9001:2008 and ISO 13485. Instructors are very knowledgeable in not only
                                    these standards, but in other standards.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Excellent breakdown of how ISO 9001 & 13485 are similar and how they differ.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Excellent interactive discussions with Jay! Supplemental course documentation was
                                    very helpful, particularly the side-by-side comparisons of ISO 9001 and ISO 13485:
                                    very concise and accurate.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Excellent discussion of ISO 13485 Requirements. Very clear explanation of differences
                                    between ISO 9001 and ISO 13485.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Excellent training approach.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    This course agenda and material are on target to the course objective.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    It�s a great skill to have.</h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Flowed in a very methodical manner---built up, very logical. Very easy to follow
                                    and understand. Notebook will be useful and CD, forms, templates ---GREAT!</h6>
                                <p>
                                </p>
                            </div>
                        </font>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <font size="2px" style="font-style: italic"><b>Lean </b></font>
                    <div class="quotediv">
                        <font style="font-style: italic">
                            <div>
                                <h6>
                                    Excellent course---provides the tools and techniques required by all employers from
                                    both the service and the manufacturing sectors.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    The instructor provided great samples of Value Stream Maps & 5S Projects.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Necessary training in today�s market.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Excellent instructor who responds to the questions with interesting, thoughtful
                                    answers.
                                </h6>
                                <p>
                                </p>
                            </div>
                        </font>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <font size="2px" style="font-style: italic"><b>Medical Device Regulations</b></font>
                    <div class="quotediv">
                        <font style="font-style: italic">
                            <div>
                                <h6>
                                    The material was formulated in an interesting way.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    This was a very informative course to me.
                                </h6>
                                <p>
                                </p>
                            </div>
                        </font>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <font size="2px" style="font-style: italic"><b>Supply Chain</b></font>
                    <div class="quotediv">
                        <font style="font-style: italic">
                            <div>
                                <h6>
                                    You MUST take this course training if you wish to become a cutting-edge buyer!
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Being involved with QPS is a wonderful adventure! There is so much to learn through
                                    the teachings of the instructors and stories of the other students. The atmosphere
                                    is greater than quality, statistics, project management, and Supply Chain. QPS teaches
                                    about building lasting relationships, getting involved, giving back, and to look
                                    for opportunities where one might never look. Simply being involved with QPS has
                                    given me a new outlook on what education and life is all about!
                                </h6>
                                <p>
                                </p>
                            </div>
                        </font>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <font size="2px" style="font-style: italic"><b>Project Management</b></font>
                    <div class="quotediv">
                        <font style="font-style: italic">
                            <div>
                                <h6>
                                    Excellent preparation for PMP� Test.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Excellent Instructors who work with and support students.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Excellent training to prepare for obtaining PMI PMP� certification.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    This course has added value to my work experience and will help in the job market.
                                    I am very happy I was able to take the class.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    I found the books provided very helpful. The test review classes were also of great
                                    benefit.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    I was a student at QPSI and participated in the Blackbelt and Project Manager programs.
                                    The superior level of content and commitment to each student was overwhelming. I
                                    have grown both professionally and personally after my experience with Jay and all
                                    the staff at QPSI.
                                </h6>
                                <p>
                                </p>
                            </div>

                            <div>
                            <h6>I want to contact the group or department that governs the training courses approved by the state of RI. I wish to explain the advantages of taking the PMP classes with Quality and Productivity Solutions, rather than at Bryant University. The Bryant course that Fidelity had arranged at Fidelity, which was taught by the Bryant instructor/professor, had a different approach, rather than focusing on what to learn and how to remember it, in preparation for the PMP exam. Quality and Productivity Solutions also offers more individual training, one on one. QPS will schedule the courses more frequently, accommodating the students� schedules, rather than waiting for Bryant�s schedule, which is October, March, May and July, 2012 (once per season according to their website). In the event that the student doesn�t pass the exam, she/he is allowed to attend the PMP course again at QPS, as a refresher. Additionally, QPS will reimburse the student for the exam fee once the student has passed it. In my case, because I had a scholarship for the additional courses, the agreement was that I would not be reimbursed. (the scholarship greatly exceeded the exam fee). QPS also had an online tool (that they signed the student up for, a 3 month duration) for practice exams that included over 1100 questions, that could repeatedly be taken over and over. The online practice exams simulated the actual proctored PMP exam, which is timed; a clock is displayed on the screen counting down the remaining time.</h6>
                            
                             <p>
                                Donna A. Cardi, <span>PMP</span>
                            
                           
                            </div>


                             <div>




                             <h6>
                                Please accept my deepest appreciation for your dedication and kindness during the training period. Personally, I would like to thank you for your additional assistance and time to help us to become more knowledgeable and professional.
                                I really enjoyed your PMP classes, your love of teaching, your sincerity and focus, your passion and enthusiasm.
                                
                                
                                </h6>

                                <p>Adrian Gugushi</p>
                                </div>


                        </font>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <font size="2px" style="font-style: italic"><b>Bio Medical Certification Program</b></font>
                    <div class="quotediv">
                        <font style="font-style: italic">
                            <div>
                                <h6>
                                    �Training was good and challenging. Lots of detailed information, real-life information.�
                                </h6>
                                <p>
                                </p>
                            </div>
                        </font>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <font size="2px" style="font-style: italic"><b>Management Certification for Manufacturing
                        Professionals Program</b></font>
                    <div class="quotediv">
                        <font style="font-style: italic">
                            <div>
                                <h6>
                                    Exercises were very informative, demonstrable and fun. The instructor is a wonderful
                                    plus to the course, and books are very well written and interesting.
                                </h6>
                                <p>
                                </p>
                            </div>
                        </font>
                    </div>
                </div>
                <div class="span-9 colborder" style="width: 85%">
                    <font size="2px" style="font-style: italic"><b>Management Certification Program</b></font>
                    <div class="quotediv">
                        <font style="font-style: italic">
                            <div>
                                <h6>
                                    The course is well structured, materials are informative and relative to skills
                                    required to analyze and improve processes. Not too high level which means you come
                                    away with knowledge, skills and tools to make you confident you will succeed.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    Instructor was very knowledgeable with work and life experience in Lean Six Sigma.
                                    His exercises are well designed and instructional. Lectures involve group interaction.
                                    I would recommend it.
                                </h6>
                                <p>
                                </p>
                            </div>
                            <div>
                                <h6>
                                    The Management certification covers both --Six Sigma and PMP�. These skills are
                                    commonly requested by many companies.
                                </h6>
                                <p>
                                </p>
                            </div>
                        </font>
                    </div>
                </div>
                <br class="clear">
                <p style="float: right; text-align: right;">
                    <a class="nounder" href="#top" title="Top">
                        <img src="../images/top.gif" title="Top"></a></p>
            </div>
        </div>
        <div class="span-4 top" style="width: 280px;">
            <img src="../images/about_us.gif" class="pull-1 top">
            <hr class="space">
            
            <script src="../js/flowplayer/swfobject.js" type="text/javascript"></script>
            <div class="pull-1">
            <b>Students Endorse QPS Training</b>
            <img src="../Images/handshake.jpg"  class="" height=200px width=310px />
            <br />
              <div class="quotediv" style="margin-top:10px;">
                       
                        <div>
                        <h6>
                        
                        John Agyeman (left), Project Management Modern student, receives Risk Management Professional Preparation Course Certificate from QPS Instructor Larry Clockedile (right)
                        </h6>
                        <p></p>
                        </div>
                 </div>       </div>
                        <div>
            <div id="divFLV2" runat="server" style="margin-top: 10px;" class="pull-1 top">
                <div id="flowplayerholder2" style="padding-right: 5px;">
                    This will be replaced by the player.
                </div>
                <div>
                    <script type="text/javascript">
                    // <![CDATA[
                        var fo = new SWFObject("../videos/FlowPlayerDark.swf", "FlowPlayer", "320", "240", "7", "#ffffff", true);
                        // need this next line for local testing, it's optional if your swf is on the same domain as your html page
                        fo.addParam("allowScriptAccess", "always");
                        fo.addParam("allowFullScreen", "true");
                        fo.addParam("wmode", "transparent");
                        fo.addParam("quality", "high");
                        fo.addVariable("config", "{ videoFile: '../videos/The Classes 1-25-10.flv', autoPlay: false, initialScale: 'fit', showOnLoadBegin:true, showMenu:false, usePlayOverlay:false,loop:false,autoRewind:true }");
                        fo.write("flowplayerholder2");
                    // ]]>
                    </script>
                </div>
            </div>
            <div id="divFLV" runat="server" style="margin-top: 10px;" class="pull-1 top">
                <div id="flowplayerholder" style="padding-right: 5px;">
                    This will be replaced by the player.
                </div>
                <div>
                    <script type="text/javascript">
                    // <![CDATA[
                        var fo = new SWFObject("../videos/FlowPlayerDark.swf", "FlowPlayer", "320", "240", "7", "#ffffff", true);
                        // need this next line for local testing, it's optional if your swf is on the same domain as your html page
                        fo.addParam("allowScriptAccess", "always");
                        fo.addParam("allowFullScreen", "true");
                        fo.addParam("wmode", "transparent");
                        fo.addParam("quality", "high");
                        fo.addVariable("config", "{ videoFile: '../videos/Student endorsements It helped.flv', autoPlay: false, initialScale: 'fit', showOnLoadBegin:true, showMenu:false, usePlayOverlay:false,loop:false,autoRewind:true }");
                        fo.write("flowplayerholder");
                    // ]]>
                    </script>
                </div>
            </div>
            <div id="divFLV3" runat="server" style="margin-top: 10px;" class="pull-1 top">
                <div id="flowplayerholder3" style="padding-right: 5px;">
                    This will be replaced by the player.
                </div>
                <div>
                    <script type="text/javascript">
                    // <![CDATA[
                        var fo = new SWFObject("../videos/FlowPlayerDark.swf", "FlowPlayer", "320", "240", "7", "#ffffff", true);
                        // need this next line for local testing, it's optional if your swf is on the same domain as your html page
                        fo.addParam("allowScriptAccess", "always");
                        fo.addParam("allowFullScreen", "true");
                        fo.addParam("wmode", "transparent");
                        fo.addParam("quality", "high");
                        fo.addVariable("config", "{ videoFile: '../videos/Student endorsements QPS classes.flv', autoPlay: false, initialScale: 'fit', showOnLoadBegin:true, showMenu:false, usePlayOverlay:false,loop:false,autoRewind:true }");
                        fo.write("flowplayerholder3");
                    // ]]>
                    </script>
                </div>
            </div>
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
    </div>
</asp:Content>

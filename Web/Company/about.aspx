<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="about.aspx.cs"
    Inherits="QPS.Company.about" %>

<%@ Register TagPrefix="QPSSidebar" TagName="Company" Src="~/Controls/Sidebar_Company.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>
<asp:Content ID="ContentAbout" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">

        setPage("Company");
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <div class="span-3 blackbk last" style="width: 80px;">
                <a title="About QPS">About QPS</a></div>
            <div class="span-3 blackbl last" style="width: 180px;">
                <a title="Membership and Partnership" href="Membership.aspx">Membership and Partnership</a></div>
            <div class="span-3 blackbl last" style="width: 80px;">
                <a title="Our Staff" href="Staff.aspx">Our Staff</a></div>
            <div style="width: 90px;" class="span-3 blackbl last">
                <a title="Our Clients" href="Clients.aspx">Our Clients</a></div>
            <div class="span-3 blackbl last" style="width: 100px;">
                <a title="Testimonials" href="Testimonials.aspx">Testimonials</a></div>
            <br class="clear">
            <br class="clear">
            <table>
                <tr>
                    <td style="vertical-align: bottom;">
                        <h3 class="colbright" id="training">
                            The QPS Institute 
                        </h3>
                        <div style="padding-left: 1 0px">
                            <h3 class="colbright">
                                About QPS</h3>
                            <p style="font-size: 125%">
                                Quality & Productivity Solutions, Inc. is an international firm dedicated to providing practical and effective consulting and training to achieve client objectives. Whether it's improvement in process, product, service, or overall performance, exceptional results are our business. We partner with our customers to understand and assess their needs, developing effective strategies and solutions that enhance the total organization.
                            </p>
                            </div>
                    </td>
                </tr>
            </table>
            <hr />
            <div class="span-19 last">
                <h3 class="colbright" id="contrn">
                    Consulting & Training</h3>
                <div class="span-6">
                    <h4 class="splfn marginborder">
                        Six Sigma</h4>
                    <ul>
                        <li>
                        Consulting, Coaching and Training
                        <li>
                        Contracting Black Belts for Client Projects
                        <li>
                        Training all levels- Onsite and Public: Yellow Belt, Green Belt, Black Belt, Champions,
                        DFSS, Master Black Belts
                    </ul>
                    <h4 class="splfn marginborder">
                        Lean Enterprise</h4>
                    <ul>
                        <li>
                        Program Implementation
                        <li>
                        Contracting Lean Experts for Client Projects
                        <li>
                        Training all levels- Onsite and Public: Lean Experts, Lean Implementation
                    </ul>
                    <h4 class="splfn marginborder">
                        Supply Chain</h4>
                    <ul>
                        <li>
                        APICS and SMP certifications
                        <li>
                        Supply Chain Excellence
                        <li>
                        Outsourcing
                    </ul>
                </div>
                <div class="span-6">
                    <h4 class="splfn marginborder">
                        Business Improvement</h4>
                    <ul>
                        <li>
                        Performance Improvement
                        <li>
                        Strategic Planning
                        <li>
                        Change Management
                        <li>
                        Management Development
                        <li>
                        Human Resources
                    </ul>
                    <h4 class="splfn marginborder">
                        Project Management</h4>
                    <ul>
                        <li>
                        Project Management for Executives
                        <li>
                        CAPM/PMP/RMP/SP/ PgMP /Agile Certification Preparation Training
                        <li>
                        PMO
                        <li>
                        Change Management
                        <li>
                        Contracting Project Managers
                    </ul>
                    <h4 class="splfn marginborder">
                        Certifications</h4>
                    <ul>
                        <li>ASQ, PMI, APICS, SME, ISO, Software related and others
                            <br />
                            - Certifications training
                        <li>
                        Coaching and Mentoring
                    </ul>
                </div>
                <div class="span-6 last">
                    <h4 class="splfn marginborder">
                        ISO & Related</h4>
                    <ul>
                        Documentation, Implementation, training and auditing for
                        <li>
                        AS 9100C and 9110
                        <li>
                        Baldrige Based improvement
                        <li>
                        FDA Related QSR, CGMP, GLP
                        <li>
                        ISO 9001
                        <li>
                        ISO 13485
                        <li>
                        ISO 14001/OHSAS 18001
                        <li>
                        ISO/TS 16949
                        <li>
                        ISO 20001
                        <li>
                        ISO 27001
                        <li>
                        Nuclear Quality Assurance
                        <li>
                        TL 9000
                    </ul>
                </div>
                <br class="clear">
                <div class="span-18">
                    <hr />
                </div>
                <br />
                <br class="clear">
                <h3 class="colbright" id="values">
                    Our Core Values</h3>
                <div class="span-18 last">
                    <ul class="tunedul">
                        <li style="list-style-type:disc">
                            <h4 class="colblu mostwrap bold">
                                Partner with Clients</h4>
                            <p>
                               We believe that the foremost commitment to our client is to act as a business partner, not just a contractor. QPS is in the business of using its knowledge and skills to benefit clients and achieve their objectives.</p>
                            <li style="list-style-type:disc">
                                <h4 class="colblu mostwrap bold">
                                    Build Client's Capabilities</h4>
                                <p>
                                It is our responsibility to provide all the knowledge possible to our clients so that they become self-sustained and are able to improve.</p>
                                     <li style="list-style-type:disc">
                                    <h4 class="colblu mostwrap bold">
                                        Reflect Client's Values</h4>
                                    <p>
                                        Because certain values are fundamental to each client, we incorporate and embody those values in the change management process. We explicitly involve everyone in acting out these values during the project in order to accelerate cultural change.</p>
                                    <li style="list-style-type:disc">
                                        <h4 class="colblu mostwrap bold">
                                            Gap Assessment</h4>
                                        <p>
                                            We believe in a thorough assessment of the organization, its processes, and methods. As a result, we can create the appropriate structure for effective and efficient operations.</p>
                                    
                    </ul>
                    <br class="clear">
                    <strong>For more details, please download the <a target="_blank" href="../documents/QPSBrochure.pdf"
                        class="nounder bold" title="QPS Brochure">QPS Brochure</a>.</strong>
                    <div style="float: right; text-align: right;">
                        <a class="nounder" href="#top" title="Top">
                            <img src="../images/top.gif" title="Top"></a></div>
                </div>
            </div>
        </div>
        <div class="span-4 top">
            <img src="../images/about_us.gif" class="pull-1 top">
            <hr class="space">
            <hr />
            <h4>
                About QPS</h4>
            <hr />
            <ul class="tunedul mostwrap">
                <li>
                    <h4 class="colblu mostwrap">
                        <a href="#top" class="nounder" title="Consulting & Training">Consulting & Training</a></h4>
            </ul>
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

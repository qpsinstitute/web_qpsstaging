using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using log4net.Config;
using System.Text.RegularExpressions;
using Joel.Net;

namespace QPS.Company
{
    public partial class Contact : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected static string ContactUs = "";
        private const string key = "c8128a057f1b";
        private const string blog = "http://qpsinc.com";


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HtmlHead f = (HtmlHead)Master.FindControl("masterHead");
                MetaTagController.addMetatagPage(Request.Url.AbsolutePath, f);
                if (!IsPostBack)
                {
                    //DataTable dtContactUs = UserController.GetContactUs();
                    //dtContactUs.DefaultView.RowFilter = "Type ='ContactUs'";
                    //if (dtContactUs.DefaultView.Count > 0)
                    //{
                    //    ContactUs = dtContactUs.DefaultView[0]["Description"].ToString();
                    //}
                    SetVerificationText();

                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Clients::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }

        public void SetVerificationText()
        {
            Random ran = new Random();
            int no = ran.Next();
            Session["Captcha"] = no.ToString().Substring(0, 4);
        }
        protected void CAPTCHAValidate(object source, ServerValidateEventArgs args)
        {
            if (Session["Captcha"] != null)
            {
                if (txtVerify.Text != Session["Captcha"].ToString())
                {
                    SetVerificationText();
                    args.IsValid = false;

                    return;
                }
            }
            else
            {
                SetVerificationText();
                args.IsValid = false;
                return;
            }

        }
        protected bool checkUrl(String url)
        {

            Uri uri = null;
            if (!Uri.TryCreate(url, UriKind.Absolute, out uri) || null == uri)
            {
                //Invalid URL
                return false;
            }
            else
                return true;
        }

        public bool IsValidEmail(string Email)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            System.Text.RegularExpressions.Regex _Regex = new System.Text.RegularExpressions.Regex(strRegex);
            if (_Regex.IsMatch(Email))
                return (true);
            else
                return (false);
        }

        public bool IsSpamComment()
        {
            Akismet api = new Akismet(key, blog, null);

            AkismetComment comment = new AkismetComment();
            comment.Blog = blog;
            comment.UserIp = "147.202.45.202";
            comment.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)";
            comment.CommentContent = comments.Value.ToString();
            comment.CommentType = "blog";
            comment.CommentAuthor = name.Value.ToString();
            comment.CommentAuthorEmail = emailaddress.Value.ToString();
            comment.CommentAuthorUrl = "http://qpsinc.com";

            bool result = api.CommentCheck(comment);
            return result;

        }



        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (comments.Value.Trim().Length > 5000)
                {
                    ErrorMsg.InnerHtml = "<font class='second'>Questions/Comments can't exceed 5000 characters.</font>";
                }
                else if (!Page.IsValid)
                {
                    return;
                }
                else
                {
                    if (IsSpamComment())
                    {
                        ErrorMsg.InnerHtml = "<font class='second'>Spam Comments.</font>";

                    }
                    else
                    {

                        string txtBody, Name, Email, Company, Position, Telephone, Comments = "";
                        Name = name.Value.Trim();
                        Email = emailaddress.Value.Trim();
                        Company = company.Value.Trim();
                        if (position.Value.Trim() != "")
                            Position = "as a " + position.Value.Trim();
                        else
                            Position = "";
                        Telephone = telephone.Value.Trim();
                        Comments = comments.Value.Trim();
                        //string data;
                        //data = comments.Value.Replace("\r", " ");
                        //data = data.Replace("\n", " ");
                        //string[] words = data.Split(' ');

                        //foreach (string word in words)
                        //{
                        //    if (!IsValidEmail(word))
                        //    {
                        //        if (!checkUrl(word))
                        //            Comments += ' '+ word;

                        //    }

                        //}

                        Hashtable hsDocument = new Hashtable();
                        hsDocument.Add("$$NAME$$", Name);
                        hsDocument.Add("$$EMAIL$$", Email);
                        hsDocument.Add("$$COMPANY$$", Company);
                        hsDocument.Add("$$POSITION$$", Position);
                        hsDocument.Add("$$TELEPHONE$$", Telephone);
                        hsDocument.Add("$$COMMENTS$$", Comments.Trim());
                        txtBody = EmailController.GetBodyFromTemplate("/EmailTemplate/Contact.htm", hsDocument);
                        EmailController.SendMail(ConfigurationSettings.AppSettings["ContactFromMail"], ConfigurationSettings.AppSettings["FeedbackToMail"], Email, "Website Feedback", txtBody, true);
                        name.Value = "";
                        company.Value = "";
                        emailaddress.Value = "";
                        telephone.Value = "";
                        position.Value = "";
                        comments.Value = "";
                        txtVerify.Text = "";
                        ErrorMsg.InnerHtml = "<font class='second'>Your feedback has been sent successfully!</font>";
                        SetVerificationText();
                    }
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Default::btnSubmit_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }        
    }
}

<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="PrivacyPolicy.aspx.cs" Inherits="QPS.Company.PrivacyPolicy" %>

<asp:Content ID="ContentContact" ContentPlaceHolderID="cphContent" runat="server"  >

<div class="span-24 prepend-1 top highcont">
	    <div class="span-24 top" style="float:right">
	    <br class="clear"><br class="clear">
		    <h3 class="colbright" id="testimonials">Quality & Productivity Solutions Privacy Commitment</h3>
		    <hr/>
		    	 
<div style="text-align:left;padding-right:5px;">
			
		<p>
		QPS has redesigned its global web site, including the Privacy Statement. The new Privacy Statement provides:
        <br>
        <br>
        
          &nbsp;&nbsp;&nbsp;&nbsp;  * Greater ease of use;<br>
                 &nbsp;&nbsp;&nbsp;&nbsp; * Updated content areas; and<br>

                 &nbsp;&nbsp;&nbsp;&nbsp; * Continued emphasis on our privacy mission.<br><br>
                 This site is owned and operated by QPS, Inc. Your privacy on the Internet is of the utmost importance to us. At QPS, Inc we want to make your experience online satisfying and safe. Because we gather certain types of information about our users, we feel you should fully understand the terms and conditions surrounding the capture and use of that information. QPS is a privacy conscious global organization and is strongly committed to your right to privacy. This privacy statement discloses what information we gather and how we use it. Should you have questions however, feel free to mail to 
                 <a href="mailto:info@qpsinc.com">info@qpsinc.com</a><br>
                 <br>
                 The QPS Privacy Mission<br><br>

QPS, Inc gathers two types of information about users: <br><br>
</p><ul>

    <li>Information that users provide through optional, voluntary submissions. These are voluntary submissions to receive our electronic newsletters, to participate in our message boards or forums, apply for a job, or order a publication.
    </li>
    <li>Information QPS, Inc gathers through aggregated tracking information derived mainly by tallying page views throughout our sites. This information allows us to better tailor our content to readers' needs and to help our advertisers and sponsors better understand the demographics of our audience.
    </li>
</ul>


		
		<p>
		Our policy is not to share personally identifiable information with third parties, unless required by law, or unless explicitly requested by a visitor. We recognize that your information is valuable and we take all reasonable measures to protect your information while it is in our care. We use small text files called cookies to improve the overall site experience; however, these are not used to track individual visitors to our site. The use of cookies is now standard operating procedure for most web sites, however if you are uncomfortable with the use of cookies, most browsers now permit users to opt-out of receiving them. 
		
		</p>
		
		    <h4 class="colblu">Internet Privacy Policy</h4>

		   <p> This site is owned and operated by QPS, Inc. Your privacy on the Internet is of the utmost importance to us. At QPS, Inc we want to make your experience online satisfying and safe. Because we gather certain types of information about our users, we feel you should fully understand the terms and conditions surrounding the capture and use of that information. This privacy statement discloses what information we gather and how we use it.</p>
		   <h4 class="colblu">Information gathering and tracking</h4>
		   <p>QPS, Inc gathers two types of information about users:<br><br>

     &nbsp;&nbsp;&nbsp;&nbsp;* Information that users provide through optional, voluntary submissions. These are voluntary submissions to receive our electronic newsletters, to participate in our message boards or forums, register for certain areas of the site, inquire for further information, distribute requested reference materials, and submit resumes.<br>
    &nbsp;&nbsp;&nbsp;&nbsp; * Information QPS, Inc gathers through aggregated tracking information derived mainly by tallying page views throughout our sites. This information allows us to better tailor our content to readers' needs and to help our advertisers and sponsors better understand the demographics of our audience.


</p>
		    <%--<h2>Cookies</h2>
		   <p>We may place a text file called a "cookie" in the browser files of your computer. The cookie itself does not contain personal information although it will enable us to relate your use of this site to information that you have specifically and knowingly provided. But the only personal information a cookie can contain is information you supply yourself. A cookie can't read data off your hard disk or read cookie files created by other sites. QPS, Inc uses cookies to track user traffic patterns (as described above).

</p>--%>
		    <h4 class="colblu">Use of information</h4>
		   <p>QPS, Inc uses any information voluntarily given by our users to enhance their experience in our network of sites, whether to provide interactive or personalized elements on the sites or to better prepare future content based on the interests of our users.
<br><br>
When we use tracking information to determine which areas of our sites users like and don't like based on traffic to those areas. We do not track what individual users read, but rather how well each page performs overall. This helps us continue to build a better service for you. We track search terms entered in Search function as one of many measures of what interests our users. But we don't track which terms a particular user enters.
</p>
		    <h4 class="colblu">Sharing of the information</h4>

		   <p>QPS, Inc uses the above-described information to tailor our content to suit your needs and help our advertisers better understand our audience's demographics. We will not share information about individual users with any third party, except to comply with applicable law or valid legal process or to protect the personal safety of our users or the public.

</p>
		 <h4 class="colblu">Your consent</h4>
		   <p>By using this site, you consent to the collection and use of this information by QPS, Inc. If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.</p>
		    <h4 class="colblu">Third party links</h4>
		   <p>There are several places throughout QPSINC.com that may link to other web sites that do not operate under QPSINC.com's privacy practices. When you link to other web sites, QPS�s privacy practices no longer apply. We encourage visitors to review each site's privacy policy before disclosing any personally identifiable information.</p> 
		   <h4 class="colblu">Sites covered</h4>

		   <p>
		    This privacy statement applies to QPS web sites located within the QPSINC.com domain:<a href="http://www.QPSINC.com">http://www.QPSINC.com</a>. Currently, other QPS web sites are linked to QPSINC.com which are not covered under this policy. These sites either have their own tailored privacy statement, or will be covered under the global privacy statement at a later date.

            Although our privacy statement is limited to the sites listed above, we welcome your inquiries or comments about our privacy statement and any queries or concerns that you may have about any QPS Web site. You may direct these to <a href="mailto:info@qpsinc.com">info@qpsinc.com</a>.</p>
    </div>
    </div>
    <div class="span-2 top" style="float:right">
	    &nbsp;
	</div>
    </div>
</asp:Content>

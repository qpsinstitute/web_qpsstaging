<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Staff.aspx.cs"
    Inherits="QPS.Company.Staff" %>

<%@ Register TagPrefix="QPSSidebar" TagName="Company" Src="~/Controls/Sidebar_Company.ascx" %>
<%@ Register TagPrefix="QPSSidebar" TagName="Sidebar" Src="~/Controls/Sidebar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Company");
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <div class="span-3 blackbl last" style="width: 80px;">
                <a title="About QPS" href="about.aspx">About QPS</a></div>
            <div class="span-3 blackbl last" style="width: 180px;">
                <a title="Membership and Partnership" href="Membership.aspx">Membership and Partnership</a></div>
            <div class="span-3 blackbk last" style="width: 80px;">
                <a title="Our Staff">Our Staff</a></div>
            <div style="width: 90px;" class="span-3 blackbl last">
                <a title="Our Clients" href="Clients.aspx">Our Clients</a></div>
            <div class="span-3 blackbl last" style="width: 100px;">
                 <a title="Testimonials" href="Testimonials.aspx">Testimonials</a></div>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="staff">
                Our Staff</h3>
            <p class="splfn">
                Our business consultants and trainers are highly qualified in many areas, ranging
                from strategic planning to breakthrough improvements. Most of our staff have qualifications
                and certifications with over 25 years of work experience:
            </p>
            <div class="span-8">
                <ul>
                    <li>
                    Agile Certified Professionals
                    <li>
                    Scrum Master Certified Professionals
                    <li>
                    Scrum Product Owner Certified
                    <li>
                    Scrum Developer Certified
                    <li>
                    Agile Expert Professionals
                    <li>
                    ASQ Certified Quality Auditor
                    <li>
                    ASQ Certified Quality Manager
                    <li>
                    ASQ Certified Quality Engineer
                    <li>
                    ASQ Certified Software Quality Engineer
                    <li>
                    APICS Certified Professionals
                </ul>
            </div>
            <div class="span-8 last">
                <ul>
                    
                    <li>
                    PMI Certified PMP, RMP, ACP, SP Professionals
                    <li>
                    ASQ Certified Six Sigma Black Belts
                    <li>
                    ASQ Certified Calibration Technicians
                    <li>
                    Certified Six Sigma Master Black Belts
                    <li>
                    Certified Lean Experts
                    <li>
                    RAB Quality System Lead Assessors
                    <li>
                    Quality Award Examiners
                    <li>
                    ASQ Certified Biomedical Auditors
                    <li>
                    ASQ HACCP Auditors
                    <li>
                    ITIL, CISSP and other IT/Software Certifications
                </ul>
            </div>
        </div>
        <div class="span-4 top">
            <img src="../images/about_us.gif" class="pull-1 top">
            <hr class="space">
            <hr />
            <h4>
                Our Staff</h4>
            <hr />
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <br />
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

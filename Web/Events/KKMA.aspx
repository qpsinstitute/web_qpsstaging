﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true"
    CodeBehind="KKMA.aspx.cs" Inherits="QPS.Events.KKMA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Events");
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <br class="clear" />
            <br class="clear" />
            <div>
                <h1 class="arial13black eititle">
                    <span>How to Conduct Kaizen and Kaizen Concepts - Marlborough </span>
                </h1>
                <br />
            </div>
            <div class="eidtitle">
                <% if (Request.QueryString["Date"] == "true")
                   {%>
                <strong>Date :</strong> Wednesday, March 27, 2013<br />
                <% }%>
                <%else
                   { %>
                <strong>Date :</strong> Wednesday, March 27, 2013<br />
                <%} %>
                <strong>Time :</strong> 9:00 AM – 12:00 PM<br />
                <br />
                <strong>Location : </strong>The QPS Institute, 225 Cedar Hill Street, Marlborough,
                MA 01752<br />
                <br />
                <a href="Register.aspx?sub=How to Conduct Kaizen and Kaizen Concepts - Marlborough"
                    target="_blank">REGISTRATION IS REQUIRED!</a><br />
                <br />
               Contact Customer Service 1-877-987-3801, Email <a href="mailto:info@qpsinc.com" style="color: #5E5E5E">
                    info@qpsinc.com</a> with your name, phone and email address so I can respond with your registration
                confirmation!
            </div>
            <br />
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

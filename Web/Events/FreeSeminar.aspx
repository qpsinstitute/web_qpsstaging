<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="FreeSeminar.aspx.cs"
    Inherits="QPS.Events.FreeSeminar" %>

<asp:Content ID="ContentSemianr" ContentPlaceHolderID="cphContent" runat="server">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <br class="clear" />
            <br class="clear" />
            <h3 class="colbright" id="events">
                Free Seminar: Agile & Project Management Overview!</h3>
            <table border="0">
                <tr style="vertical-align: top;">
                    <td style="vertical-align: top;">
                        <font style="font-size: 12px">Learn about Agile and Project Management.  Understand its features in planning, executing, monitoring, controlling, and more!   Learn about the PMI Body of Knowledge and their certifications (AgileSM & PMP�). 
               </font>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-size: 12px">
                Some benefits of Agile:</font>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul>
                            <ol>
                                <font style="font-size: 12px">
                <li>More flexibility!  The agile approach allows the ability to correct mistakes.  It keep mistakes low, or "refine your solution" through its adaptive team that responds to changing requirements.</li><li>Reduce project risks!  The agile way lets you know very quickly if your project is off schedule.  It can provide control over what to do to get back on schedule, making forecasting/predicting much better and preventing overruns of costs / explanations for long delays. </li>
                <li>Visibility!  Agile eliminates guesswork because it paves the way for a transparent management style, allowing you to see features being worked on. </li>
                <li>Maximize Value!  Agile results in early delivery of value because it provides continuous planning and feedback throughout development! </li>
                 </font>
                            </ol>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-size: 12px">
                Date:</font>
                        <br />
                        <font style="font-size: 12px">Friday, October 14, 2011
                    </font>
                        <br />
                    </td>
                </tr>
                <br />
                <tr>
                    <td>
                        <font style="font-size: 12px">Time</font>
                        <br />
                        <font style="font-size: 12px">9 am � 12 noon</font>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-size: 12px">
                Location:
                    <br />
                The QPS Institute<br />
                225 Cedar Hill Street - Floor 3<br />
                Marlborough, MA 01752 USA
                </font>
                    </td>
                </tr>
            </table>
            <span style="color: red;">*</span> <font style="font-weight: bold; background-color: Yellow;
                font-size: 125%">Registration is required.<br />
                <br />
                
            </font><a href="../Events/Register.aspx?sub=Free Seminar : Agile +%26+ Project Management Overview!"
                style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

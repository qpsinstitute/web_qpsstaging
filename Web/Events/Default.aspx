<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="QPS.Events.Default"
    MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentReg" runat="server" ContentPlaceHolderID="cphContent">
  
      <script language="javascript" type="text/javascript">


        setPage("Events");
        function SelectDiv(no) {
            if (no == 1) {
                document.getElementById("Massachusetts").style.visibility = 'visible';

                document.getElementById("Connecticut").style.visibility = 'hidden';
                document.getElementById("New Jersey").style.visibility = 'hidden';
                document.getElementById("North Carolina").style.visibility = 'hidden';
                document.getElementById("Illinois").style.visibility = 'hidden';

                document.getElementById("Massachusetts").style.height = 'auto';

                document.getElementById("Connecticut").style.height = '0px';
                document.getElementById("New Jersey").style.height = '0px';
                document.getElementById("North Carolina").style.height = '0px';
             //   document.getElementById("Illinois").style.height = '0px';


                document.getElementById("tab1").className = 'span-3 blackbk last';
                document.getElementById("tab6").className = 'span-3 blackbl last';
                document.getElementById("tab3").className = 'span-3 blackbl last';
               // document.getElementById("tab4").className = 'span-3 blackbl last';
               // document.getElementById("tab5").className = 'span-3 blackbl last';
               

            }

            else if (no == 3) {

                document.getElementById("Massachusetts").style.visibility = 'hidden';

                document.getElementById("Connecticut").style.visibility = 'visible';
                document.getElementById("New Jersey").style.visibility = 'hidden';
                document.getElementById("North Carolina").style.visibility = 'hidden';
                document.getElementById("Illinois").style.visibility = 'hidden';

                document.getElementById("Massachusetts").style.height = '0px';

                document.getElementById("Connecticut").style.height = 'auto';
                document.getElementById("New Jersey").style.height = '0px';
                //document.getElementById("North Carolina").style.height = '0px';
                //document.getElementById("Illinois").style.height = '0px';


                document.getElementById("tab1").className = 'span-3 blackbl last';

                document.getElementById("tab3").className = 'span-3 blackbk last';
              //  document.getElementById("tab4").className = 'span-3 blackbl last';
                //document.getElementById("tab5").className = 'span-3 blackbl last';
                document.getElementById("tab6").className = 'span-3 blackbl last';

            }
            else if (no == 4) {
                document.getElementById("Massachusetts").style.visibility = 'hidden';

                document.getElementById("Connecticut").style.visibility = 'hidden';
                document.getElementById("New Jersey").style.visibility = 'hidden';
                //document.getElementById("North Carolina").style.visibility = 'visible';
                //document.getElementById("Illinois").style.visibility = 'hidden';

                document.getElementById("Massachusetts").style.height = '0px';

                document.getElementById("Connecticut").style.height = '0px';
                document.getElementById("New Jersey").style.height = '0px';
                //document.getElementById("North Carolina").style.height = 'auto';
                //document.getElementById("Illinois").style.height = '0px';


                document.getElementById("tab1").className = 'span-3 blackbl last';

                document.getElementById("tab3").className = 'span-3 blackbl last';
               // document.getElementById("tab4").className = 'span-3 blackbk last';
                //document.getElementById("tab5").className = 'span-3 blackbl last';
                document.getElementById("tab6").className = 'span-3 blackbl last';

            }
            else if (no == 5) {
                document.getElementById("Massachusetts").style.visibility = 'hidden';

                document.getElementById("Connecticut").style.visibility = 'hidden';
                document.getElementById("New Jersey").style.visibility = 'hidden';
                //document.getElementById("North Carolina").style.visibility = 'hidden';
                //document.getElementById("Illinois").style.visibility = 'visible';

                document.getElementById("Massachusetts").style.height = '0px';

                document.getElementById("Connecticut").style.height = '0px';
                document.getElementById("New Jersey").style.height = '0px';
               // document.getElementById("North Carolina").style.height = '0px';
                //document.getElementById("Illinois").style.height = 'auto';


                document.getElementById("tab1").className = 'span-3 blackbl last';

                document.getElementById("tab3").className = 'span-3 blackbl last';
               // document.getElementById("tab4").className = 'span-3 blackbl last';
//              document.getElementById("tab5").className = 'span-3 blackbk last';
                document.getElementById("tab6").className = 'span-3 blackbl last';

            }

            //Added New Jersey
            else if (no == 7) {

      
                document.getElementById("Massachusetts").style.visibility = 'hidden';

                document.getElementById("Connecticut").style.visibility = 'hidden';
                document.getElementById("New Jersey").style.visibility = 'visible';
             //   document.getElementById("North Carolina").style.visibility = 'hidden';
              //  document.getElementById("Illinois").style.visibility = 'hidden';

                document.getElementById("Massachusetts").style.height = '0px';

                document.getElementById("Connecticut").style.height = '0px';
                document.getElementById("New Jersey").style.height = 'auto';
          //      document.getElementById("North Carolina").style.height = '0px';
             //   document.getElementById("Illinois").style.height = '0px';


                document.getElementById("tab1").className = 'span-3 blackbl last';

                document.getElementById("tab3").className = 'span-3 blackbl last';
              //  document.getElementById("tab4").className = 'span-3 blackbl last';
             //   document.getElementById("tab5").className = 'span-3 blackbl last';
                document.getElementById("tab6").className = 'span-3 blackbk last';

            }
            //// over ///
            else {
                document.getElementById("Massachusetts").style.visibility = 'hidden';

                document.getElementById("Connecticut").style.visibility = 'hidden';
                document.getElementById("New Jersey").style.visibility = 'hidden';
            //    document.getElementById("North Carolina").style.visibility = 'hidden';
            //    document.getElementById("Illinois").style.visibility = 'hidden';

                document.getElementById("Massachusetts").style.height = '0px';

                document.getElementById("Connecticut").style.height = '0px';
                document.getElementById("New Jersey").style.height = '0px';
            //    document.getElementById("North Carolina").style.height = '0px';
           //     document.getElementById("Illinois").style.height = '0px';


                document.getElementById("tab1").className = 'span-3 blackbl last';

                document.getElementById("tab3").className = 'span-3 blackbl last';
              //  document.getElementById("tab4").className = 'span-3 blackbl last';
            //    document.getElementById("tab5").className = 'span-3 blackbl last';
                document.getElementById("tab6").className = 'span-3 blackbk last';


            }
        }
        function DIV1_onclick() { }
      
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <br class="clear">
            <br class="clear">
            <h2 id="registration" class="colbright">
                Free Events
            </h2>
            <div id="tab1" class="span-3 blackbk last" style="width: 105px;">
                <a href="#" title="Massachusetts" style="color: Silver;" onmouseover="this.style.color = 'White'"
                    onmouseout="this.style.color = 'Silver'" onclick="SelectDiv(1)">Massachusetts</a></div>
            <div id="tab3" class="span-3 blackbl last" style="width: 90px;">
                <a href="#" title="Connecticut" style="color: Silver" onmouseover="this.style.color = 'White'"
                    onmouseout="this.style.color = 'Silver'" onclick="SelectDiv(3)">Connecticut</a>
            </div>
            <div id="tab6" class="span-3 blackbl last" style="width: 95px;">
                <a href="#" title="New Jersey" style="color: Silver" onmouseover="this.style.color = 'White'"
                    onmouseout="this.style.color = 'Silver'" onclick="SelectDiv(7)">New Jersey</a>
            </div>
           <%-- <div id="tab4" class="span-3 blackbl last" style="width: 105px;">
                <a href="#" title="North Carolina" style="color: Silver" onmouseover="this.style.color = 'White'"
                    onmouseout="this.style.color = 'Silver'" onclick="SelectDiv(4)">North Carolina</a>
            </div>--%>
            <%--<div id="tab5" class="span-3 blackbl last" style="width: 70px;">
                <a href="#" title="Illinois" style="color: Silver" onmouseover="this.style.color = 'White'"
                    onmouseout="this.style.color = 'Silver'" onclick="SelectDiv(5)">Illinois</a>
            </div>--%>
            <br class="clear">
            <br class="clear">
            <div id="Massachusetts" style="visibility: visible">
                <h3 id="H3_3" class="colbright">
                    <b>Massachusetts</b></h3>
                <table>
                  
                   
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="ISO20092015.aspx?State=MA" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Risk Based Thinking in ISO 9001:2015
                                - Marlborough</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                 <b>Event Date : </b>Monday, January 04, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location :</b> The QPS Institute, 28 Lord Road, Suite 205, Marlborough, MA 01752<br />
                                
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="ISO20092015.aspx?State=MA" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>
                   
                    <td>
                        <hr style="" />
                    </td>
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="RiskBasedThinking.aspx?State=MA" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Process Approach in ISO 9001:2015
                                - Marlborough</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                               <b>Event Date : </b>Monday, January 11, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location :</b> The QPS Institute, 28 Lord Road, Suite 205, Marlborough, MA 01752<br />
                               
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="RiskBasedThinking.aspx?State=MA" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>
                    <td>
                        <hr style="" />
                    </td>
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="Processapprochiso.aspx?State=MA" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif"> Lean six sigma deployment  in Manufacturing 
                                - Marlborough</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                 <b>Event Date : </b>Monday, January 18, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location :</b> The QPS Institute, 28 Lord Road, Suite 205, Marlborough, MA 01752<br />
                               
                            </p>
                        </td>
                    </tr>




        

                    <tr>
                        <td>
                            <a href="Processapprochiso.aspx?State=MA" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                    
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="Leandep.aspx?State=MA" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Lean deployment in service 
                                - Marlborough</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                               <b>Event Date : </b>Monday, January 25, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location :</b> The QPS Institute, 28 Lord Road, Suite 205, Marlborough, MA 01752<br />
                               
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="Leandep.aspx?State=MA" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>
                    <td>
                        <hr style="" />
                    </td>


                    
                       <tr style="vertical-align: top;">
                        <td>
                            <a href="Leansupply.aspx?State=MA" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Lean Supply Chain 
                                - Marlborough</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                               <b>Event Date : </b>Monday, February 01, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location :</b> The QPS Institute, 28 Lord Road, Suite 205, Marlborough, MA 01752<br />
                               
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="Leansupply.aspx?State=MA" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>
                    <td>
                        <hr style="" />
                    </td>


                           <tr style="vertical-align: top;">
                        <td>
                            <a href="ISO9000.aspx?State=MA" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">ISO 9001:2008 transition to ISO 
                                - Marlborough</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                               <b>Event Date : </b>Monday, February 08, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location :</b> The QPS Institute, 28 Lord Road, Suite 205, Marlborough, MA 01752<br />
                               
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="ISO9000.aspx?State=MA" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>
                    <td>
                        <hr style="" />
                    </td>


                   
              

                </table>


            </div>
          
            <div id="Connecticut" style="visibility: hidden; height: 0px">
                <h3 id="H3" class="colbright">
                    <b>Connecticut</b></h3>
                <table>
                   
            
                    
                  
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="ISO20092015.aspx?State=CT" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Risk Based Thinking in ISO 9001:2015
                                - East Berlin</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jack Reardon<br />
                                <b>Event Date : </b>Tuesday, January 05, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 1224 Mill Street, Building B, East Berlin, CT 06023<br />
                                
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="ISO20092015.aspx?State=CT" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="RiskBasedThinking.aspx?State=CT" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Process Approach in ISO 9001:2015
                                - East Berlin</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jack Reardon<br />
                                 <b>Event Date : </b>Tuesday, January 12, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 1224 Mill Street, Building B, East Berlin, CT 06023<br />
                               
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="RiskBasedThinking.aspx?State=CT" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="Processapprochiso.aspx?State=CT" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Lean six sigma deployment  in Manufacturing 
                                - East Berlin</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jack Reardon<br />
                                 <b>Event Date : </b>Tuesday, January 19, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 1224 Mill Street, Building B, East Berlin, CT 06023<br />
                                
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="Processapprochiso.aspx?State=CT" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>


                    <td>
                        <hr style="" />
                    </td>
                  

                   <tr style="vertical-align: top;">
                        <td>
                            <a href="Leandep.aspx?State=CT" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Lean deployment in service 
                                - East Berlin</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jack Reardon<br />
                                 <b>Event Date : </b>Tuesday, January 26, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 1224 Mill Street, Building B, East Berlin, CT 06023<br />
                                
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="Leandep.aspx?State=CT" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>


                    <td>
                        <hr style="" />
                    </td>

                    <tr style="vertical-align: top;">
                        <td>
                            <a href="Leansupply.aspx?State=CT" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Lean Supply Chain 
                                - East Berlin</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jack Reardon<br />
                                 <b>Event Date : </b>Tuesday, February 02, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 1224 Mill Street, Building B, East Berlin, CT 06023<br />
                                
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="Leansupply.aspx?State=CT" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>


                    <td>
                        <hr style="" />
                    </td>

                     <tr style="vertical-align: top;">
                        <td>
                            <a href="ISO9000.aspx?State=CT" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">ISO 9001:2008 transition to ISO 9001:2015
                                - East Berlin</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jack Reardon<br />
                                 <b>Event Date : </b>Tuesday, February 09, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 1224 Mill Street, Building B, East Berlin, CT 06023<br />
                                
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="ISO9000.aspx?State=CT" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>


                    <td>
                        <hr style="" />
                    </td>

                    
                  

                                        
                </table>
            </div>
            <div id="New Jersey" style="visibility: hidden; height: 0px">
                <h3 id="H4" class="colbright">
                    <b>New Jersey</b></h3>
                  <table>
                  
                 
                     <tr style="vertical-align: top;">
                        <td>
                            <a href="ISO20092015.aspx?State=NJ" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Risk Based Thinking in ISO 9001:2015
                                - Woodbridge</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Edwin May<br />
                                <b>Event Date : </b>Wednesday, January 06, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 581 Main Street Suite 640 Woodbridge, NJ 07095<br />
                                
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="ISO20092015.aspx?State=NJ" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="RiskBasedThinking.aspx?State=NJ" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Process Approach in ISO 9001:2015
                                - Woodbridge</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Edwin May<br />
                                 <b>Event Date : </b>Wednesday, January 13, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 581 Main Street Suite 640 Woodbridge, NJ 07095<br />
                               
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="RiskBasedThinking.aspx?State=NJ" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="Processapprochiso.aspx?State=NJ" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Lean six sigma deployment  in Manufacturing 
                                - Woodbridge</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Edwin May<br />
                                 <b>Event Date : </b>Wednesday, January 20, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 581 Main Street Suite 640 Woodbridge, NJ 07095<br />
                                
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="Processapprochiso.aspx?State=NJ" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>


                    <td>
                        <hr style="" />
                    </td>
                  

                   <tr style="vertical-align: top;">
                        <td>
                            <a href="Leandep.aspx?State=NJ" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Lean deployment in service 
                                - Woodbridge</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Edwin May<br />
                                 <b>Event Date : </b>Wednesday, January 27, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 581 Main Street Suite 640 Woodbridge, NJ 07095<br />
                                
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="Leandep.aspx?State=NJ" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>


                    <td>
                        <hr style="" />
                    </td>

                    <tr style="vertical-align: top;">
                        <td>
                            <a href="Leansupply.aspx?State=NJ" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Lean Supply Chain 
                                - Woodbridge</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Edwin May<br />
                                 <b>Event Date : </b>Wednesday, February 03, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 581 Main Street Suite 640 Woodbridge, NJ 07095<br />
                                
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="Leansupply.aspx?State=NJ" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>


                    <td>
                        <hr style="" />
                    </td>

                     <tr style="vertical-align: top;">
                        <td>
                            <a href="ISO9000.aspx?State=NJ" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">ISO 9001:2008 transition to ISO 9001:2015
                                - Woodbridge</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Edwin May<br />
                                 <b>Event Date : </b>Wednesday, February 10, 2016
                                <br />
                                <b>Time :</b> 5:00 PM � 6:00 PM<br />
                                <b>Location : </b>The QPS Institute, 581 Main Street Suite 640 Woodbridge, NJ 07095<br />
                                
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="ISO9000.aspx?State=NJ" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>


                    <td>
                        <hr style="" />
                    </td>
                   
<%--
                    <td>
                        <hr style="" />
                    </td>
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeWO9.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">ISO 9001:2015 changes
                                - Woodbridge</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Edwin May/Patricia Smith<br />
                                  <b>Event Date : </b>Wednesday, May 13, 2015
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location :</b> The QPS Institute, 581 Main Street Suite 640 Woodbridge, NJ 07095<br />
                               
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeWO9.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeWO10.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">How to achieve Black Belt Certification
                                - Woodbridge</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Edwin May/Patricia Smith<br />
                                <b>Event Date : </b>Wednesday, May 20, 2015
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location :</b> The QPS Institute, 581 Main Street Suite 640 Woodbridge, NJ 07095<br />
                               
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeWO10.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>
                    <td>
                        <hr style="" />
                    </td>
                     <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeWO11.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">PMI PMP certification-Introduction
                                - Woodbridge</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Edwin May/Patricia Smith<br />
                                <b>Event Date : </b>Wednesday, May 27, 2015
                                <br />
                                <b>Time :</b> 4:30 PM<br />
                                <b>Location :</b> The QPS Institute, 581 Main Street Suite 640 Woodbridge, NJ 07095<br />
                               
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeWO11.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeWO3.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">How to Achieve Scrum Certifications
                                - Woodbridge</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Edwin May/Patricia Smith<br />
                                <b>Event Date : </b>Wednesday, June 04, 2015
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location :</b> The QPS Institute, 581 Main Street Suite 640 Woodbridge, NJ 07095<br />
                               
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeWO3.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeIS2.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Woodbridge</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Edwin May/Patricia Smith<br />
                                  <b>Event Date : </b>Wednesday, June 10, 2015
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location :</b> The QPS Institute, 581 Main Street Suite 640 Woodbridge, NJ 07095<br />
                               
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeIS2.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>
--%>
                    
                </table>
            </div>
         
            <div id="Illinois" style="visibility: hidden; height: 0px; padding-right: 40px">
                <h3 id="H6" class="colbright">
                    <b>Illinois</b></h3>
                 <table>
				 

               
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeCH15.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Chicago</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Rupal Patel/Jay Patel<br />
                                <b>Event Date : </b>Thursday, April 16, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location : </b> The QPS Institute, 303 W Algonquin Road, Mount Prospect, IL 60056<br />
                                
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeCH15.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>

                    <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeCH17.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free-Enhance Interviewing skills
                                - Chicago</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Rupal Patel/Jay Patel<br />
                                <b>Event Date : </b>Thursday, April 23, 2016 
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location : </b> The QPS Institute, 303 W Algonquin Road, Mount Prospect, IL 60056<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeCH17.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                     <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeCH18.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free- Lean six Sigma overview
                                - Chicago</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Rupal Patel/Jay Patel<br />
                                 <b>Event Date : </b>Thursday, April 30, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location : </b> The QPS Institute, 303 W Algonquin Road, Mount Prospect, IL 60056<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeCH18.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                     <td>
                        <hr style="" />
                    </td>

                   <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeCH.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free- Medical Device Regulations
                                - Chicago</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Rupal Patel/Jay Patel<br />
                                <b>Event Date : </b>Thursday, May 07, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location : </b> The QPS Institute, 303 W Algonquin Road, Mount Prospect, IL 60056<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeCH.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>
                    <td>
                        <hr style="" />
                    </td>
                     <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeCH9.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Chicago</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Rupal Patel/Jay Patel<br />
                                 <b>Event Date : </b>Thursday, May 14, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location : </b> The QPS Institute, 303 W Algonquin Road, Mount Prospect, IL 60056<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeCH9.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                <%--     <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeCH10.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">ISO 9001:2015 changes
                                - Chicago</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Rupal Patel/Jay Patel<br />
                                <b>Event Date : </b>Thursday, May 21, 2015
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location : </b> The QPS Institute, 303 W Algonquin Road, Mount Prospect, IL 60056<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeCH10.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                     <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeCH11.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">How to achieve Black Belt Certification
                                - Chicago</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Rupal Patel/Jay Patel<br />
                                  <b>Event Date : </b>Thursday, May 28, 2015
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location : </b> The QPS Institute, 303 W Algonquin Road, Mount Prospect, IL 60056<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeCH11.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>
                    <td>
                        <hr style="" />
                    </td>
                     <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeCH12.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">PMI PMP certification-Introduction
                                - Chicago</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Rupal Patel/Jay Patel<br />
                                 <b>Event Date : </b>Thursday, June 04, 2015
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location : </b> The QPS Institute, 303 W Algonquin Road, Mount Prospect, IL 60056<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeCH12.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                     <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeCH13.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">How to Achieve Scrum Certifications
                                - Chicago</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Rupal Patel/Jay Patel<br />
                                  <b>Event Date : </b>Thursday, June 11, 2015
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location : </b> The QPS Institute, 303 W Algonquin Road, Mount Prospect, IL 60056<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeCH13.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                     <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeCH14.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Chicago</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Rupal Patel/Jay Patel<br />
                              <b>Event Date : </b>Thursday, June 18, 2015
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>Location : </b> The QPS Institute, 303 W Algonquin Road, Mount Prospect, IL 60056<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeCH14.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>--%>

                </table>
            </div>
               <div id="North Carolina" style="visibility: hidden; height: 0px;">
                <h3 id="H5" class="colbright">
                    <b>North Carolina</b></h3>
               <table>
                   
                    <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeChar1.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Charlotte</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                <b>Event Date : </b>Thursday, June 26, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>State :</b> NC<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeChar1.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                     <td>
                        <hr style="" />
                    </td>
                      <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeChar2.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Charlotte</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                 <b>Event Date : </b>Thursday, May 22, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>State :</b> NC<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeChar2.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                      <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeChar3.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Charlotte</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                 <b>Event Date : </b>Thursday, May 29, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>State :</b> NC<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeChar3.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                      <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeChar4.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Charlotte</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                 <b>Event Date : </b>Thursday, June 05, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>State :</b> NC<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeChar4.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                      <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeChar5.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Charlotte</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                <b>Event Date : </b>Thursday, June 12, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>State :</b> NC<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeChar5.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                      <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeChar6.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Charlotte</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                <b>Event Date : </b>Thursday, June 19, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>State :</b> NC<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeChar6.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                      <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeChar7.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Charlotte</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                <b>Event Date : </b>Thursday, June 26, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>State :</b> NC<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeChar7.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>


                    <td>
                        <hr style="" />
                    </td>
                      <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeChar8.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Charlotte</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                 <b>Event Date : </b>Thursday, July 03, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>State :</b> NC<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeChar8.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                      <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeChar9.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Charlotte</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                  <b>Event Date : </b>Thursday, July 10, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>State :</b> NC<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeChar9.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>

                    <td>
                        <hr style="" />
                    </td>
                      <tr style="vertical-align: top;">
                        <td>
                            <a href="FreeResumeChar10.aspx" style="text-decoration: none; font-size: 15px; color: rgb(0,96,165);
                                font-family: Verdana, Arial, Helvetica, sans-serif">Free Resume and Networking Session
                                - Charlotte</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <b>Instructor :</b> Jay Patel<br />
                                <b>Event Date : </b>Thursday, July 17, 2016
                                <br />
                                <b>Time :</b> 4:00 PM � 6:00 PM<br />
                                <b>State :</b> NC<br />
                              
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="FreeResumeChar10.aspx" style="text-decoration: none; color: rgb(0,102,255);
                                font-family: Verdana, Arial, Helvetica, sans-serif">View Details </a>
                        </td>
                    </tr>



                </table>
            </div>
          
        </div>
        <div class="span-4">
            <hr class="space">
            <div class="span-16" id="DIV8" language="javascript" onclick="return DIV1_onclick()">
                &nbsp;
            </div>
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>

                    
                  
   
</asp:Content>

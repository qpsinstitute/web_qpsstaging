<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FreeInfoSession.aspx.cs"
    Inherits="QPS.Events.FreeInfoSession" MasterPageFile="~/QPSSiteHome.Master" %>

<asp:Content ID="ContentFreeInfoSession" runat="server" ContentPlaceHolderID="cphContent1">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <br class="clear">
            <br class="clear">
            <h3 id="training" class="colbright">
                Free Informational Session
            </h3>
            <br />
            <p style="font-size: 12px" align="center">
                <font style="font-weight: bold; background-color: Yellow;">Friday, November 12, 2010 at 11 AM 
                <br />The QPS Institute 225 Cedar Hill Street Marlborough, MA 01752</font>
                <br />
                <br />
                <b style="font-size: 12px;">SEATING IS LIMITED<br />
                    TO REGISTER: CALL TOLL-FREE 1-877-987-3801 1-877-987-3801 AND SPEAK TO JAY
                    <br />
                    (EMAIL <a href="mailto:INFO@QPSINC.COM">INFO@QPSINC.COM</a> AND <a href="mailto:JAYP@QPSINC.COM">
                        JAYP@QPSINC.COM</a>)
                    <br />
                    OR FILL OUT THE FORM BELOW:</b>
            </p>
            <br />
            <br />
            <span style="color: red;">*</span> <font style="font-weight: bold; background-color: Yellow;
                font-size: 12px">Registration is required.<br />
                <br />
            </font><a href="../Events/Register.aspx?sub=Free Informational Session" style="text-decoration: none;
                color: #0060A5;" target="_blank">Click Here to Register</a>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

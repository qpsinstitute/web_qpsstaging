<%@ Page Language="C#" AutoEventWireup="true" Codebehind="TrainingEvent.aspx.cs"
    Inherits="QPS.Events.TrainingEvent" MasterPageFile="~/QPSSite.Master" %>

<asp:Content ID="ContentTrainingEvent" runat="server" ContentPlaceHolderID="cphContent">
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
     
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                <asp:Label ID="lbl1" runat="server" Style="font-weight: bold; font-size: 25px; margin-left: 120px;
                    color: #5F04B4;">Training Opportunities Available!
                </asp:Label>
            </h3>
            <br />
            <p style="font-size: 12px">
                <font style="font-size: 12px">If you are unemployed and interested in exploring certification
                    options in Lean Six Sigma or Project Management, now is the time to act! Come to
                    the next free informational session at the QPS Institute where we can assist you
                    with:
                    <ul style="font-size: 15px; font-style: italic; margin-left: 150px;">
                        <li>Jobs & Career Goals</li><li>Training Opportunities </li>
                        <li>Review of QPS Institute�s Key Courses & Benefits to You</li><li>Specific Candidate
                            Consideration</li><li>Why Lean Six Sigma & PMP�</li><li>Instructors � Qualifications</li><li>
                                Scholarships & Application</li><li>Refreshments Provided</li></ul>
                </font>
            </p>
            <br />
         
            <font style="font-size: 12px">11 AM - 1 PM: Tuesday, April 3, 2012 - <a href="../Events/Register.aspx?sub=Training Opportunities available on Tuesday, April 3, 2012"
                style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
            </font>
            <br />
            <font style="font-size: 12px">11 AM - 1 PM: Tuesday, April 10, 2012 - <a href="../Events/Register.aspx?sub=Training Opportunities available on Tuesday,  April 10, 2012"
                style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
            </font>
            <br />
            <font style="font-size: 12px">11 AM - 1 PM: Tuesday, April 17, 2012 - <a href="../Events/Register.aspx?sub=Training Opportunities available on Tuesday,  April 17, 2012"
                style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
            </font>
            <br />
            <font style="font-size: 12px">11 AM - 1 PM: Tuesday, April 24, 2012 - <a href="../Events/Register.aspx?sub=Training Opportunities available on Tuesday,  April 24, 2012"
                style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
            </font>
            <br />
            <font style="font-size: 12px">11 AM - 1 PM: Tuesday, May 1, 2012 - <a href="../Events/Register.aspx?sub=Training Opportunities available on Tuesday, May 1, 2012"
                style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
            </font>
            <br />
            <font style="font-size: 12px">11 AM - 1 PM: Tuesday, May 8, 2012 - <a href="../Events/Register.aspx?sub=Training Opportunities available on Tuesday, May 8, 2012"
                style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
            </font>
            <br />
            <br />
            <span style="color: red;">*</span> <font style="font-weight: bold; background-color: Yellow;
                font-size: 125%">Registration is required.<br />
                <br />
            </font>
            <p style="font-size: 12px" align="center">
                <font style="font-weight: bold; background-color: Yellow;">The QPS Institute 225 Cedar
                    Hill Street Marlborough, MA 01752<br />
                    SEATING IS LIMITED! </font>
            </p>
            <br />
            <p style="font-size: 12px" align="center">
                <font style="font-weight: bold;">TO REGISTER:<br />
                    Send an email / rsvp to <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a> with
                    contact information or<br />
                    visit the QPS website <a href="http://www.qpsinc.com/Events/Default.aspx" target="_blank"
                        style="color: #045FB4">http://www.qpsinc.com/Events/Default.aspx</a><br />
                    CALL TOLL-FREE 1-877-987-3801 TODAY!</font></p>
            <br />
            <b style="font-size: 12px; font-weight: bold;">Visit us daily! Directions are easy as
                1-2-3!</b><br />
            <font style="font-size: 12px">1. Take 495,<br />
                2. Take Exit 23 C (Simarano Drive Exit toward Marlborough / Southboro & keep left
                at fork in the ramp),<br />
                3. Take left onto Simarano Drive then turn right onto Cedar Hill.
                <br />
            </font>
            <br />
            <p align="center">
                <font style="font-weight: bold; font-size: 18pt; color: #1B2A0A;">Quality & Productivity
                    Solutions, Inc.</font><br />
                <font style="font-weight: bold; font-style: italic; font-size: 10pt; color: #1B2A0A;">
                    Features</font>
                <br />
                <font style="font-weight: bold; font-size: 30pt; color: #1B2A0A;">THE QPS INSTITUTE</font><br />
                <font style="font-weight: bold; font-size: 10pt; color: #1B2A0A;">*BOSTON* *PROVIDANCE*
                    *HARTFORD* *CHARLOTTE* *CHICAGO*</font><br />
                <font style="font-weight: bold; font-size: 9pt; font-style: italic; color: #1B2A0A;">
                    Experts in Lean Six Sigma, Management Systems, Supply Chain, Project Management
                    & Professional Development</font><br />
            </p>
            <br />
            <br />
            <br />
            <div>
            </div>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

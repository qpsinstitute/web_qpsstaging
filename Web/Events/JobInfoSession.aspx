<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/QPSSite.Master" CodeBehind="JobInfoSession.aspx.cs"
    Inherits="QPS.Events.JobInfoSession" %>

<asp:Content ID="infosession" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Events");
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <table border="0">
                <br class="clear" />
                <br class="clear" />
                <h3 class="colbright" id="events" style="leftpadding: 1px">
                    &nbsp;Jobs Information Session</h3>
                <tr style="vertical-align: top;">
                    <td style="vertical-align: top;">
                        <font style="font-size: 12px">Wednesday, April 4, 2012 -   <a href="../Events/Register.aspx?sub=Job Information Session On Wednesday, April 4, 2012" style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
                  </font>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="vertical-align: top;">
                        <font style="font-size: 12px">Thursday, April 5, 2012 -   <a href="../Events/Register.aspx?sub=Job Information Session On Thursday, April 5, 2012" style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
                  </font>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="vertical-align: top;">
                        <font style="font-size: 12px">Wednesday, April 11, 2012 -   <a href="../Events/Register.aspx?sub=Job Information Session On Wednesday,  April 11, 2012" style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
                  </font>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="vertical-align: top;">
                        <font style="font-size: 12px">Wednesday, April 18, 2012 -   <a href="../Events/Register.aspx?sub=Job Information Session On Wednesday, April 18, 2012" style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
                  </font>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="vertical-align: top;">
                        <font style="font-size: 12px">Wednesday, April 25, 2012 -   <a href="../Events/Register.aspx?sub=Job Information Session On Wedneday, April 25, 2012, 2012" style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
                  </font>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="vertical-align: top;">
                        <font style="font-size: 12px">Wednesday, May 2, 2012 -   <a href="../Events/Register.aspx?sub=Job Information Session On Wedneday, May 2, 2012" style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
                  </font>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="vertical-align: top;">
                        <font style="font-size: 12px">
            Wednesday, May 9, 2012 -   <a href="../Events/Register.aspx?sub=Job Information Session On Wednesday, May 9, 2012" style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
                  </font>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="vertical-align: top;">
                        <font style="font-size: 12px">
            Wednesday, May 16, 2012 -   <a href="../Events/Register.aspx?sub=Job Information Session On Wednesday, May 16, 2012" style="text-decoration: none; color: #0060A5;" target="_blank">Click Here to Register</a>
                  </font>
                        <br />
                        <br />
                        <font style="font-weight: bold; background-color: Yellow; font-size: 12px">Registration Required.<br />
                     To register,<br />send an email with contact information to:<br />Jay Patel, President & CEO<br />
                     <a href="mailto:info@qpsinc.com">info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com">jayp@qpsinc.com</a>
                     </font>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="center">
                            <map name="logo" id="logo">
                                <area shape="rect" coords="109, 90, 240, 100" href="mailto:info@qpsinc.com" alt="info@qpsinc.com"
                                    title="info@qpsinc.com" />
                                <area shape="rect" coords="252, 89, 382, 99" href="http://www.qpsinc.com" alt="www.qpsinc.com"
                                    title="www.qpsinc.com" target="_blank" />
                            </map>
                            <img src="../Images/qpsadd.jpg" usemap="#logo" alt="image" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="center">
                            <strong><font style="font-size: 12px">
                 JOBS<br />in Management, Manufacturing,Customer Service, Logistics, Quality, Supply Chain, IT, Software, Biomedical, Insurance, Project Management, Software and more!<br />
                     <br />Information Session<br /><br /></font><font style="font-weight: bold; background-color: Yellow;
                         font-size: 12px"> Thursday, April 5, 2012</font>
                                <br />
                                <br />
                                <font style="font-size: 12px"> 11 AM&nbsp; �&nbsp; 1 PM<br /><br />
                     Bring your resume!<br />
                     -<br /></font></strong>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="left">
                            <font style="font-size: 12px"><strong>
                        Positions: </strong>
                     <ul>
                     <li type="bullet">Managers</li><li type="bullet">Supervisors</li><li type="bullet">Inspectors & Technicians</li><li type="bullet">Logistics Director</li><li type="bullet">Customer Service Analysts</li><li type="bullet"> Business Analysts</li><li type="bullet"> IT Professionals</li><li type="bullet"> Software Professionals</li><li type="bullet">  Director of Procurement</li><li type="bullet">  Senior Quality Engineer</li><li type="bullet">Industrial Engineer</li><li type="bullet"> Operations Engineer</li><li type="bullet"> Lean Project Manager</li><li type="bullet">Quality Engineer </li><li type="bullet">Quality Assurance Manager</li><li type="bullet"> Quality Systems Manager</li><li type="bullet">Packaging Quality Engineer</li><li type="bullet">Healthcare Quality Engineer</li><li type="bullet"> Metrologist / Calibration Lab Supervisor</li><li type="bullet"> Director of Manufacturing</li><li type="bullet"> Design Assurance Engineer</li><li type="bullet"> Sourcing Specialist � Research Outsourcing Procurement</li><li type="bullet">Director, Strategic Sourcing</li><li type="bullet"> Director, Supply Chain</li><li type="bullet"> Buyer / Planner</li><li type="bullet"> Sr Quality Systems Engineer</li><li type="bullet"> Contract Manufacturing Commodity Manager</li><li type="bullet"> Product Engineer</li><li type="bullet"> Principal Project Manager</li><li type="bullet">Project Manager Senior</li><li type="bullet">  Lean / Six Sigma Engineer</li><li type="bullet">Medical Device related + more!
</li>
                     </ul></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="left">
                            <font style="font-size: 12px"><strong>
                       Categories / Industries Served: </strong>
                     <ul>
                     <li type="bullet"> Automotive & Parts Manufacturing</li><li type="bullet"> Biotechnology & Pharmaceutical</li><li type="bullet">  Consumer Packaged Goods Manufacturing</li><li type="bullet">Electronics, Components & Semiconductor Manufacturing</li><li type="bullet">Energy & Utilities</li><li type="bullet">  Industrial & Manufacturing Engineering</li><li type="bullet">Insurance</li><li type="bullet"> IT & Software Development</li><li type="bullet">    Logistics & Transportation</li><li type="bullet"> Medical Devices & Supplies</li><li type="bullet">Product Development Telecommunications</li></ul></font>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

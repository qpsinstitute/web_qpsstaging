﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true"
    CodeBehind="FreeResumeMA1.aspx.cs" Inherits="QPS.Events.FreeResumeMA1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Events");
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <br class="clear" />
            <br class="clear" />
            <div>
                <h1 class="arial13black eititle">
                    <span>How to Implement Lean - Removing Non-Value Added Activities in the Workplace -
                        Marlborough</span>
                </h1>
                <br />
                <p>
                    A great time to network with others and meet Jay Patel, President of QPS who will
                    be offering advice on not just resume refinement and interviewing techniques, but
                    how to get the job! Please bring a hard copy of your resume and attire is business
                    casual.
                </p>
            </div>
            <div class="eidtitle">
                <% if (Request.QueryString["Date"] == "true")
                   {%>
                <strong>Date :</strong> Monday, April 01, 2013<br />
                <% }%>
                <%else
                   { %>
                <strong>Date :</strong> Monday, April 01, 2013<br />
                <%} %>
                <strong>Time :</strong> 5:00 PM – 7:00 PM<br />
                <br />
                <strong>Location : </strong>The QPS Institute, 225 Cedar Hill Street, Marlborough,
                MA 01752<br />
                <br />
                Refreshments provided.<br />
                <br />
                <a href="Register.aspx?sub=How to Implement Lean - Removing Non-Value Added Activities in the Workplace - Marlborough"
                    target="_blank">REGISTRATION IS REQUIRED!</a><br />
                <br />
                Contact Customer Service 1-877-987-3801, Email <a href="mailto:info@qpsinc.com" style="color: #5E5E5E">
                    info@qpsinc.com</a> and <a href="mailto:jayp@qpsinc.com" style="color: #5E5E5E">jayp@qpsinc.com</a>
                with your name, phone and email address so I can respond with your registration
                confirmation!
            </div>
            <br />
            <div class="eidtitle">
                <strong>Instructor Profile:</strong><br />
                <span>&nbsp;</span><p>
                    About Jay P. Patel<br />
                    President & CEO of Quality & Productivity Solutions, Inc.<br />
                    Founder, The QPS Institute
                </p>
                <p>
                    Jay P. Patel, President & CEO of Quality & Productivity Solutions, Inc. and Founder,
                    the QPS Institute, possesses over 25 years of professional work experience in management
                    and quality assignments, including holding past positions as Project Engineer, Program
                    Manager. He has provided consulting to many Fortune 500 clients and trained thousands
                    of people. He holds 10 ASQ Certifications including Six Sigma Certified Black Belt
                    and Certified Quality Auditor in addition to CQE, CQIA, CBA, CCT, CHA, CSQE, CQT,
                    CMQ/OE. He has a Bachelor and Master Degrees in Engineering and MBA. He is a RAB-Quality
                    System Lead Auditor and Environmental Auditor. Jay holds PMI certifications. He
                    is a certified Project Management Professional (PMP®), a certified Risk Management
                    Professional (RMP®) and a certified Scheduling Professional (PMI-SP®). He has been
                    a frequent speaker on Leadership, Lean Six Sigma and Quality/Productivity related
                    topics. He has taught over 100+ courses ranging from Strategic Planning to Lean
                    Six Sigma Master Black Belt.
                </p>
            </div>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

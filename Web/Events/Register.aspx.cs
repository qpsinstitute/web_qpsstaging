using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using log4net.Config;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Imaging;

namespace QPS.InformationSession
{
    public partial class Register : System.Web.UI.Page
    {
        public string sub1; 
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {

            sub1 = Request.QueryString["sub"];
            try
            {
                lblhead.Text = sub1;
                if (!IsPostBack)
                {
                    SetVerificationText(); 
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Clients::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        public void SetVerificationText()
        {
            Random ran = new Random();
            int no = ran.Next();
            Session["Captcha"] = no.ToString().Substring(0,4);
        }
        protected void CAPTCHAValidate(object source, ServerValidateEventArgs args)
        {
            if (Session["Captcha"] != null)
            {
                if (txtVerify.Text != Session["Captcha"].ToString())
                {
                    SetVerificationText();
                    args.IsValid = false;
                    return;
                }
            }
            else
            {
                SetVerificationText();
                args.IsValid = false;
                return;
            }

        }
        protected bool checkUrl(String url)
        {

            Uri uri = null;
            if (!Uri.TryCreate(url, UriKind.Absolute, out uri) || null == uri)
            {
                //Invalid URL
                return false;
            }
            else
                return true;
        }


        public bool IsValidEmail(string Email)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            System.Text.RegularExpressions.Regex _Regex = new System.Text.RegularExpressions.Regex(strRegex);
            if (_Regex.IsMatch(Email))
                return (true);
            else
                return (false);
        }




        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (comments.Value.Trim().Length > 5000 )
                {
                    ErrorMsg.InnerHtml = "<font class='second'>Questions/Comments can't exceed 5000 characters.</font>";
                }
                else if(!Page.IsValid)
                {
                    return;
                }
                else
                {
                    string txtBody, Name, Email, Company, Position, Telephone, Subject1,Subject, Comments = "";
                    Name = name.Value.Trim();
                    Email = emailaddress.Value.Trim();
                    Company = company.Value.Trim();
                    Subject1 = sub1.ToString();
                    Subject = sub1.ToString(); 
                    if (position.Value.Trim() != "")
                        Position = "as a " + position.Value.Trim();
                    else
                        Position = "";
                    Telephone = telephone.Value.Trim();
                    Comments = comments.Value.Trim();

                    //string data;
                    //data = comments.Value.Replace("\r", " ");
                    //data = data.Replace("\n", " ");
                    //string[] words = data.Split(' ');

                    //foreach (string word in words)
                    //{
                    //    if (!IsValidEmail(word))
                    //    {
                    //        if (!checkUrl(word))
                    //            Comments += word+ " ";

                    //    }

                    //}
                    
                    Hashtable hsDocument = new Hashtable();
                    hsDocument.Add("$$NAME$$", Name);
                    hsDocument.Add("$$EMAIL$$", Email);
                    hsDocument.Add("$$COMPANY$$", Company);
                    hsDocument.Add("$$SUBJECT$$", Subject); 
                    hsDocument.Add("$$POSITION$$", Position); 
                    hsDocument.Add("$$TELEPHONE$$", Telephone);
                    hsDocument.Add("$$COMMENTS$$", Comments.Trim());
                    txtBody = EmailController.GetBodyFromTemplate("/EmailTemplate/Contact.htm", hsDocument);
                    EmailController.SendMail(ConfigurationSettings.AppSettings["InformationalFromMail"],Email, Subject1, txtBody, true);
                    name.Value = "";
                    company.Value = "";
                    emailaddress.Value = "";
                    telephone.Value = "";
                    position.Value = "";
                    comments.Value = "";
                    txtVerify.Text = "";
                    ErrorMsg.InnerHtml = "<font class='second'>Your Registration Information has been sent successfully!</font>";
                    SetVerificationText();
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Register::btnSubmit_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }        
    }
}

using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using log4net.Config;
using System.Collections.Generic;
using System.Text;

namespace QPS.Training
{
    public partial class Calendar : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            //rptDetails.ItemDataBound += new RepeaterItemEventHandler(rptDetails_ItemDataBound);
        }

        public void lnkCity_Click(object sender, EventArgs e)
        {
            LinkButton lnk = sender as LinkButton;
            ViewState["CityId"] = lnk.CommandArgument;
            if (ViewState["CategoryId"] == null)
                ViewState["CategoryId"] = "1";
            BindCourseDetails(ViewState["CategoryId"].ToString());
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, Page.GetType(), "ExecuteIt", "<script>SetCurrentTab('" + ViewState["CategoryId"].ToString() + "');</script>", false);
        }

        //protected void rptDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        DataRowView dr = e.Item.DataItem as DataRowView;
        //        if (dr != null)
        //        {
        //            string courseId = dr["CourseID"].ToString();
        //            string loc = "";
        //            var CourseLocTbl = eLearnCategoryController.GetLocationByCourseID(int.Parse(courseId));
        //            if (CourseLocTbl != null && CourseLocTbl.Tables != null && CourseLocTbl.Tables.Count > 0 && CourseLocTbl.Tables[0].Rows != null && CourseLocTbl.Tables[0].Rows.Count > 0)
        //            {
        //                foreach (DataRow drLoc in CourseLocTbl.Tables[0].Rows)
        //                {
        //                    loc += drLoc["CityName"].ToString() + ", " + drLoc["StateName"].ToString() + "<br/>";
        //                }
        //            }
        //            loc = loc.Length > 0 ? loc.Substring(0, loc.Length - 5) : "";
        //            if (loc.Length > 0)
        //            {
        //                Literal lblLoc = e.Item.FindControl("lblLoc") as Literal;
        //                lblLoc.Text = loc;
        //            }
        //        }
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    //BindCity();
                    rptCategory.DataSource = eLearnCategoryController.SelectAllCategoryByWClause(" WHERE CourseCategory.Status=1  and (  Course.Locations <> '' or  Course.Locations <> null ) and   Course.status=1");
                    rptCategory.DataBind();

                    int PageID = 0;
                    PageID = Convert.ToInt32(Request.QueryString["id"]);

                    if (PageID != 0)
                    {
                        BindCourseDetails(PageID.ToString());
                        PageID = PageID - 1;
                    }
                    else
                        BindCourseDetails("1");
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Calendar::Page_Load - ", ex);
                //Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void lnkCategory_Click(object sender, EventArgs e)
        {
            LinkButton _sender = (LinkButton)sender;
            string CategoryID = _sender.CommandArgument;
            ViewState["CategoryId"] = CategoryID;
            ViewState["CityId"] = null;

            BindCourseDetails(CategoryID);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, Page.GetType(), "ExecuteIt", "<script>SetCurrentTab('" + CategoryID + "');</script>", false);
        }

        void BindCourseDetails(string CategoryID)
        {
            string cityid = "";
            if (ViewState["CityId"] != null)
            {
                cityid = ViewState["CityId"].ToString();
            }
            DataTable dtCourses = eLearnCategoryController.SelectAllCoursesByCategoryID("WHERE CategoryID=" + CategoryID + "  and ( Locations <> '' or Locations <> null ) and status=1");
            //DataTable dtCourses = null;            
            //if (string.IsNullOrEmpty(cityid))
            //{
            //    dtCourses = eLearnCategoryController.SelectAllCoursesByCategoryID("WHERE Course.CategoryID=" + CategoryID + "  and Course.status=1");
            //}
            //else
            //{
            //    dtCourses = eLearnCategoryController.SelectAllCoursesByCategoryID(", Course_Location WHERE Course.Courseid=Course_Location.Courseid and  Course.CategoryID=" + CategoryID + " and Course_Location.Cityid=" + cityid + " and Course.status=1");
            //}
            rptDetails.DataSource = dtCourses;
            rptDetails.DataBind();
        }

        //public void BindCity()
        //{
        //    DataSet ds = CourseController.GetAllLocation();
        //    if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
        //    {
        //        rptCity.DataSource = ds.Tables[0];
        //        rptCity.DataBind();
        //    }
        //}
    }
}
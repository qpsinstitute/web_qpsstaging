<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="OnSite.aspx.cs"
    Inherits="QPS.Training.OnSite" %>

<asp:Content ID="ContentOnSite" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Training");
        function openPopup(txtURL) {
            window.open(txtURL, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width=600,height=500");
        }    
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="../Js/qfunctions.js"></script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <div class="span-3 blackbl last" style="width: 130px">
                <a href="Registration.aspx" title="Corporate Register">Corporate Register</a></div>
            <div class="span-3 blackbl last">
                <a href="Training.aspx" title="Training Grants">Training Grants</a></div>
            <div class="span-3 blackbl last" style="width: 87px">
                <a href="/ELearning/EcourseList.aspx" title="eLearning">eLearning</a></div>
            <div class="span-3 blackbk last" style="width: 100px">
                <a href="OnSite.aspx" title="On-Site Training">On-Site Training</a></div>
            <div class="span-3 blackbl last" style="width: 120px">
                <a href="UnEmployed.aspx" title="Public Training">Public Training</a></div>
            <div class="span-3 blackbl last" style="width: 123px">
                <a href="Calendar.aspx" title="Corporate Calendar">Corporate Calendar</a></div>
            <%-- <div class="span-3 blackbl last" style="width: 110px">
                <a href="../Instructor/Signin.aspx" title="Instructor Login">Instructor Login</a></div>--%>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                On-Site Training</h3>
            <p style="font-size: 125%">
                The following list shows some of the training topics which can be customized to
                meet your needs. Please contact us at <a href="mailto:info@qpsinc.com" class="nounder">
                    <u>info@qpsinc.com</u></a>&nbsp;for details. Public training topics with course
                brochures are listed in the <a href="calendar.aspx" class="nounder bold" title="Calendar">
                    Calendar.</a>
            </p>
            <div class="span-18 last showgrid" id="fullonsite" style="border: 1px solid silver">
                <div class="span-6">
                    <ul class="coleablock">
                        <li id="on1" class="clicked"><a href="javascript:onsite('on1');" title="Lean Six Sigma">
                            Lean Six Sigma</a>
                            <li id="on2"><a href="javascript:onsite('on2');" title="Lean">Lean</a>
                                <li id="on3"><a href="javascript:onsite('on3');" title="Process Management">Process
                                    Management</a>
                                    <li id="on4"><a href="javascript:onsite('on4');" title="Strategic Planning">Strategic
                                        Planning</a>
                                        <li id="on5"><a href="javascript:onsite('on5');" title="Statistics">Statistics</a>
                                            <li id="on6"><a href="javascript:onsite('on6');" title="Supply Chain">Supply Chain</a>
                                                <li id="on23"><a href="javascript:onsite('on23');" title="APICS Certifications">APICS
                                                    Certifications</a>
                                                    <li id="on22"><a href="javascript:onsite('on22');" title="SME/Shingo/AME Certifications">
                                                        SME/Shingo/AME Certifications</a>
                                                        <li id="on7"><a href="javascript:onsite('on7');" title="ISO 9001:2015">ISO 9001:2015</a>
                                                            <li id="on8"><a href="javascript:onsite('on8');" title="Automotive/TS 16949">Automotive/TS
                                                                16949</a>
                                                                <li id="on9"><a href="javascript:onsite('on9');" title="FDA Regulated Industries">FDA
                                                                    Regulated Industries</a>
                                                                    <li id="on10"><a href="javascript:onsite('on10');" title="Aerospace Industries">Aerospace
                                                                        Industries</a>
                                                                        <li id="on11"><a href="javascript:onsite('on11');" title="Foods">Foods</a>
                                                                            <li id="on12"><a href="javascript:onsite('on12');" title="Healthcare">Healthcare</a>
                                                                                <li id="on13"><a href="javascript:onsite('on13');" title="Service">Service</a>
                                                                                    <li id="on14"><a href="javascript:onsite('on14');" title="Manufacturing">Manufacturing
                                                                                    </a>
                                                                                        <li id="on15"><a href="javascript:onsite('on15');" title="Software Organizations">Software
                                                                                            Organizations</a>
                                                                                            <li id="on16"><a href="javascript:onsite('on16');" title="Nuclear Industries">Nuclear
                                                                                                Industries</a>
                                                                                                <li id="on17"><a href="javascript:onsite('on17');" title="ISO 14001/EMS">ISO 14001/EMS</a>
                                                                                                    <li id="on18"><a href="javascript:onsite('on18');" title="Safety & Environmental">Safety
                                                                                                        & Environmental</a>
                                                                                                        <li id="on19"><a href="javascript:onsite('on19');" title="Leadership/Teams">Leadership/Teams</a>
                                                                                                            <li id="on20"><a href="javascript:onsite('on20');" title="ASQ Certifications">ASQ Certifications</a>
                                                                                                                <li id="on21"><a href="javascript:onsite('on21');" title="Project Management">Project
                                                                                                                    Management</a>
                    </ul>
                </div>
                <div class="span-11 spaceheadline" id="onsite1">
                    <h3>
                        Lean Six Sigma</h3>
                    Six Sigam Yellow Belt Certification-ASQ BOK
                    <br />
                    Six Sigma Green Belt Certification-ASQ BOK
                    <br />
                    Six Sigma Black Belt Certification -ASQ BOK<br />
                    Lean Six Sigma for Healthcare, Services and Financial services (customized)<br />
                    Lean Six Sigma Black Belt Certification<br />
                    Master Black Belt Certifications<br />
                    Lean Six Sigma Yellow Belt Certification<br />
                    Champion Training<br />
                    Process Mapping<br />
                    Design for Six Sigma -Overview<br />
                    Design for Six Sigma Certification �For Products<br />
                    Design for Six Sigma Certification �For Services<br />
                    TRIZ
                    <br />
                    Quality Function Deployment<br />
                    Measurement Systems Analysis<br />
                    Lean Six Sigma for Executives<br />
                    Lean Six Sigma Project Management<br />
                </div>
                <div class="span-11 spaceheadline" id="onsite2" style="display: none">
                    <h3>
                        Lean</h3>
                    Lean Expert Certification
                    <br />
                    How to deploy lean in Mfg.
                    <br />
                    How to deploy lean in Service
                    <br />
                    Value Stream Mapping
                    <br />
                    Lean Project Management
                    <br />
                    How to deploy Kaizen
                    <br />
                    Standardization<br />
                    5 S/Visual Controls<br />
                    Set up reduction/JIT<br />
                    Lean Supply Chain<br />
                    Standards of Work<br />
                    Managing Team based culture<br />
                    Cellular Manufacturing
                    <br />
                    TPM
                    <br />
                    Mistake Proofing
                    <br />
                    Lean Bronz Certification<br />
                    Lean Silver Certification
                    <br />
                    Lean Gold Certification
                    <br />
                    Training for Shingo Prize
                </div>
                <div class="span-11 spaceheadline" id="onsite3" style="display: none">
                    <h3>
                        Process Management</h3>
                    Process Mapping
                    <br />
                    Process focused Documentation<br />
                    Team Based Problem Solving
                    <br />
                    Malcolm Baldrige Based Business Improvements
                    <br />
                    Seven Basic Tools
                </div>
                <div class="span-11 spaceheadline" id="onsite4" style="display: none">
                    <h3>
                        Strategic Planning</h3>
                    Strategic Planning<br />
                    Hoshin Kanri (Policy Deployment)<br />
                    Coaching & Mentoring<br />
                    Management Reviews<br />
                </div>
                <div class="span-11 spaceheadline" id="onsite5" style="display: none">
                    <h3>
                        Statistics</h3>
                    Basic Statistics<br />
                    Intermediate Statistics<br />
                    Design of Experiments
                </div>
                <div class="span-11 spaceheadline" id="onsite6" style="display: none">
                    <h3>
                        Supply Chain</h3>
                    Lean Supply Chain
                    <br />
                    Supplier Certifications
                    <br />
                    Developing suppliers/Partnership
                    <br />
                    Outsourcing (Manufacturing)
                    <br />
                    Outsourcing to India for Software Development
                    <br />
                    Import/Export<br />
                    Supplier auditor certification<br />
                    APICS CSCP Certification Prep- Certified Supply Chain Professional
                </div>
                <div class="span-11 spaceheadline" id="onsite7" style="display: none">
                    <h3>
                        ISO 9001:2015</h3>
                    Transitioning to ISO 9001:2015<br />
                    Internal Auditing (Includes Process auditing) ISO 9001:2015 Certification<br />
                    Lead Auditor � ISO 9001:2015<br />
                    Process approach and Documenting information to ISO 9001:2015<br />
                    Risk based thinking and Risk Management for ISO 9001:2015<br />
                    Implementation and Auditing to ISO 9001:2015<br />
                    ISO 31000 & ISO 31010- Risk Management and Risk Analysis-Understanding and Implementing<br />
                    Managing Global Management System using ISO 9001:2015
                </div>
                <div class="span-11 spaceheadline" id="onsite8" style="display: none">
                    <h3>
                        Automotive /TS 16949</h3>
                    Understanding and Implementing ISO/TS 16949<br />
                    Internal auditing ( includes process and product audits)<br />
                    Lead Auditor Training
                    <br />
                    Certification- Core Tools<br />
                    APQP/PPAP<br />
                    FMEA and Risk Analysis
                    <br />
                    Applied MSA
                </div>
                <div class="span-11 spaceheadline" id="onsite9" style="display: none">
                    <h3>
                        FDA Regulated Industries</h3>
                    Understanding and Implementing ISO 9001 and ISO 13485<br />
                    Quality System Regulations? Medical Device Regulations
                    <br />
                    Auditing to ISO 9001 and ISO 13485<br />
                    Design Controls
                    <br />
                    CAPA<br />
                    CE Marking for the EU medical device Directives<br />
                    FDA Inspection
                    <br />
                    Risk Management
                    <br />
                    Management Certifications for Medical Device Professionals
                    <br />
                    Process Validations
                    <br />
                    510(K): Medical Device Process
                    <br />
                    CAPA for Medical Devices & Drugs
                    <br />
                    European Union (EU) Directives, European Conformity (CE) Marking & Guidance Documentation
                    <br />
                    GMP / ISO System Documentation
                    <br />
                    QSIT Quality System Inspection Technique
                    <br />
                    Medical Device Sterilization Planning
                    <br />
                </div>
                <div class="span-11 spaceheadline" id="onsite10" style="display: none">
                    <h3>
                        Aerospace Industries</h3>
                    Internal auditing to AS9100 and AS9110<br />
                    Failure Modes and Effects Analysis<br />
                    Corrective and Preventive actions<br />
                    Risk Management
                    <br />
                    Process Improvement<br />
                    Modern Applied Project Management<br />
                </div>
                <div class="span-11 spaceheadline" id="onsite11" style="display: none">
                    <h3>
                        Foods</h3>
                    Food Safety<br />
                    Auditing
                    <br />
                    ISO 22000 Food Safety Management System
                    <br />
                    Food Safety Management System Requirements & ISO 22000 Overview
                    <br />
                    HACCP (Hazard Analysis Critical Control Point) Overview
                    <br />
                    Hazard Analysis Critical Control Point (HACCP)
                    <br />
                    Internal Auditor - ISO 22000
                    <br />
                    Lead Internal Auditor - ISO 22000
                </div>
                <div class="span-11 spaceheadline" id="onsite12" style="display: none">
                    <h3>
                        Healthcare</h3>
                    Customer Service and Satisfaction<br />
                    Implementing Team based Improvements
                    <br />
                    Information Security ISO 27001<br />
                    QMS- ISO 9001, ISO 13485, FDA QSR<br />
                    Lean Deployment for Healthcare
                    <br />
                    Project and Program Management (PMP, PgMP, Scrum/agile)
                    <br />
                    Quality Management System
                    <br />
                    Six Sigma<br />
                    Solution Management- Planning, Development and Deployment
                    <br />
                    Software Design and Development
                </div>
                <div class="span-11 spaceheadline" id="onsite13" style="display: none">
                    <h3>
                        Service</h3>
                    Team Based Problem Solving
                    <br />
                    Lean six sigma for service
                    <br />
                    Customer service
                    <br />
                    Measuring Customer satisfaction
                    <br />
                    Implementing ISO 9001 for service organizations
                    <br />
                    Management Certifications for Service Professionals
                </div>
                <div class="span-11 spaceheadline" id="onsite14" style="display: none">
                    <h3>
                        Manufacturing</h3>
                    Management Certification for Mfg Professionals
                    <br />
                    Inspector and Technician Certification
                    <br />
                    Supervisory and Managerial skills certification
                </div>
                <div class="span-11 spaceheadline" id="onsite15" style="display: none">
                    <h3>
                        Software Organizations</h3>
                    Information Security Management System-ISO 27001<br />
                    Management System-ISO 9001, ISO 14001, OHSAS 18001<br />
                    Software Verification and Validation<br />
                    Understanding and Implementing TL 9000<br />
                    Lean six sigma deployment
                    <br />
                    Management development
                    <br />
                    Soft skills- Facilitation, Team Building, Communication
                    <br />
                    Risk Management
                    <br />
                    Project and Program Management (PMP, PgMP, Scrum/agile)
                </div>
                <div class="span-11 spaceheadline" id="onsite16" style="display: none">
                    <h3>
                        Nuclear Industries</h3>
                    Nuclear Quality Assurance � Documentation
                    <br />
                    NQA Implementation and Auditing<br />
                    NQA -Lead Auditor<br />
                    NQA- Internal Auditor
                </div>
                <div class="span-11 spaceheadline" id="onsite17" style="display: none">
                    <h3>
                        ISO 14001/EMS/OHSAS</h3>
                    Understanding and Implementing ISO 14001:2015
                    <br />
                    Documenting ISO 14001:2015<br />
                    Documenting OHSAS<br />
                    Auditing to QMS, EMS and OHSAS<br />
                    Integrating QMS, EMS and OHSAS<br />
                    Understanding and Implementing OHSAS
                </div>
                <div class="span-11 spaceheadline" id="onsite18" style="display: none">
                    <h3>
                        Safety & Environmental</h3>
                    Safety Program documentation
                    <br />
                    Implementing Safety Program
                    <br />
                    Office Safety
                    <br />
                    Chemical Safety/Hazard Communication
                    <br />
                    Spill prevention and Control
                    <br />
                    Lock out/Tag out
                    <br />
                    Electrical Safety
                    <br />
                    OSHA Inspections
                    <br />
                    Personal Protective Equipment
                    <br />
                    Bloodborne Pathogens
                    <br />
                    Contractor Safety
                    <br />
                    Lead Safety
                    <br />
                    Machine Guarding
                    <br />
                    EPA Risk Management
                    <br />
                    ISO 14001 Overview
                    <br />
                    Documenting EMS
                    <br />
                    Implementing and Auditing ISO 14001
                    <br />
                    ISO 14001 awareness
                </div>
                <div class="span-11 spaceheadline" id="onsite19" style="display: none">
                    <h3>
                        Leadership/Teams</h3>
                    Developing Leaders for transformation
                    <br />
                    Managing and Leading Change
                    <br />
                    Team Building
                    <br />
                    Facilitation
                    <br />
                    Deploying Kaizen
                    <br />
                    Managing Team based Organization
                </div>
                <div class="span-11 spaceheadline" id="onsite20" style="display: none">
                    <h3>
                        ASQ Certifications</h3>
                    Certified Quality Auditor<br />
                    Certified Quality Engineer<br />
                    Certified Quality Improvement Associate
                    <br />
                    Certified Manager of Quality/Org Excellence
                    <br />
                    Certified Six Sigma Black Belt
                    <br />
                    Certified Software Quality Engineer<br />
                    Certified Bio-Medical Auditor
                    <br />
                    Certified Quality Technician
                    <br />
                    Certified Calibration Technician
                    <br />
                    Certified HACCP auditor
                    <br />
                    Certified Pharmaceutical GMP professional _CPGP
                </div>
                <div class="span-11 spaceheadline" id="onsite21" style="display: none">
                    <h3>
                        Project Management</h3>
                    Project Management Professional (PMP�) Certification Preparation<br />
                    Certified Associate in Project Management (CAPM)<br />
                    PMI scheduling Professional
                    <br />
                    PMI Risk Management Professional
                    <br />
                    Program Management Professional (PgMP�)<br />
                    MS project Training
                    <br />
                    Fundamentals of Project Management
                    <br />
                    Project Management for IT Professionals
                </div>
                <div class="span-11 spaceheadline" id="onsite22" style="display: none">
                    <h3>
                        SME/Shingo/AME Certifications</h3>
                    Lean Bronz Certification<br />
                    Lean Silver Certification
                    <br />
                    Lean Gold Certification
                    <br />
                    Training for Shingo Prize
                </div>
                <div class="span-11 spaceheadline" id="onsite23" style="display: none">
                    <h3>
                        APICS Certifications</h3>
                    APICS CSCP Certification Prep- Certified Supply Chain Professional<br />
                    APICS CPIM Certification Prep- Certified Production and Inventory control Management
                </div>
            </div>
            <div class="span-18 last" id="Div1" style="background: none;">
                <table style="width: 100%; font-size: 125%">
                    <tr>
                        <td>
                            <br />
                            <h4>
                                QPS On-Site Training Advantages:</h4>
                            <ul>
                                <li>Customized training focused on YOUR industry and YOUR business</li>
                                <li>Overall cost savings, enabling more students to attend courses</li>
                                <li>Increased customer satisfaction both internally and externally due to increased
                                    confidence and qualifications obtained</li>
                                <li>Instructor's ability to deliver courses in different methods to meet varying levels
                                    of student's ability and knowledge</li>
                                <li>Experiences and exercises on site-specific cases for better comprehension</li>
                                <li>Eliminate employee travel costs and lost time as our trainers come to you</li>
                                <li>Raise overall level of teamwork and communication across the board</li>
                                <li>We arrange courses to meet your schedules</li>
                                <li>Our trainers are qualified auditors who provide broad, detailed understanding of
                                    your management system and its information</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            "Every course at QPS is detailed, relevant, comprehensive, and effective in nature.
                            They contain clear and easy-to-follow class material. Our training sessions provide
                            easy-to-understand instructions, clear and practical steps. Our courses and training
                            offer sound solutions to problems."
                        </td>
                    </tr>
                </table>
            </div>
            <p style="float: right; text-align: right;">
                <a class="nounder" href="#top" title="Top">
                    <img src="../images/top.gif" title="Top"></a></p>
        </div>
        <div class="span-4" style="">
            <img src="../images/training.gif" class="pull-1" alt="QPS Onsite Training">
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

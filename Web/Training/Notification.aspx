﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true"
    CodeBehind="Notification.aspx.cs" Inherits="QPS.Training.Notification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Training");
        function onselected() {
            var lst = document.getElementById('<%= ddlCourses.ClientID %>');
            var lstdata = 0;
            var count = 0
            for (count = 0; lst.options.length; count++) {
                if (lst.options[count].selected) {

                    if (lst.options[count].value == '0') {

                        lst.options[count].selected = false;
                    }
                    else
                    { }

                }
            }
        }
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-19 top" style="float: right">
            <br class="clear">
            <br class="clear">
            <h3 id="training" class="colbright">
                <asp:Label ID="lblhead" runat="server" Text=""></asp:Label>Corporate Registration form to
                get notification for the particular course
            </h3>

      
           <br />

            <br />

            <form id="frmContact" runat="server" style="vertical-align: top;">
            <table width="100%">
                <tr>
                    <td>
                        <br>
                        <b>Note: Required fields marked with </b>
                        <img src="../images/req.gif"><br>
                        <span id="ErrorMsg" runat="server" style="color: #C00;"></span>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td class="third">
                        First Name
                        <img src="../images/req.gif"><br>
                        <input id="Firstname" runat="server" name="ctl00$cphContent$name" size="34" title="First Name"
                            type="text" />
                        <asp:RequiredFieldValidator ID="Rqfname" runat="server" ControlToValidate="Firstname"
                            ErrorMessage="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revfName" runat="server" ControlToValidate="Firstname"
                            ErrorMessage="Only alphanumeric characters are allowed" ValidationExpression="[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="third">
                        Last Name
                        <img src="../images/req.gif"><br>
                        <input id="Lastname" runat="server" name="ctl00$cphContent$lastname" size="34" title="Last Name"
                            type="text" />
                        <asp:RequiredFieldValidator ID="Rqlname" runat="server" ControlToValidate="Lastname"
                            ErrorMessage="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revlname" runat="server" ControlToValidate="Lastname"
                            ErrorMessage="Only alphanumeric characters are allowed" ValidationExpression="[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="third">
                        Company
                        <img src="../images/req.gif">
                        <br>
                        <input id="company" runat="server" name="ctl00$cphContent$company" size="34" title="Company"
                            type="text" />
                        <asp:RequiredFieldValidator ID="Rqcompany" runat="server" ControlToValidate="company"
                            ErrorMessage="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revCompany" runat="server" ControlToValidate="company"
                            ErrorMessage="Only alphanumeric characters allowed" ValidationExpression="[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="third">
                        Location
                        <br>
                        <input id="txtlocation" runat="server" name="ctl00$cphContent$txtlocation" size="34"
                            title="Location" type="text" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtlocation"
                            ErrorMessage="Only alphanumeric characters allowed" ValidationExpression="[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="third">
                        State
                        <img src="../images/req.gif">
                        <br>
                        <input id="state" runat="server" name="ctl00$cphContent$state" size="34" title="State"
                            type="text" />
                        <asp:RequiredFieldValidator ID="Rqstate" runat="server" ControlToValidate="state"
                            ErrorMessage="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="state"
                            ErrorMessage="Only alphanumeric characters allowed" ValidationExpression="[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="third">
                        Email Address
                        <img src="../images/req.gif"><br>
                        <input id="Emailaddress" runat="server" name="ctl00$cphContent$emailaddress" size="34"
                            title="Email Address" type="text" />
                        <asp:RequiredFieldValidator ID="Rqemailaddress" runat="server" ControlToValidate="Emailaddress"
                            ErrorMessage="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="Rgemailaddress" runat="server" ControlToValidate="Emailaddress"
                            ErrorMessage="Please enter a valid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="third">
                        Phone
                        <img src="../images/req.gif">
                        <br>
                        <input id="phone" runat="server" maxlength="50" name="ctl00$cphContent$telephone"
                            size="34" title="Phone" type="text" />
                        <asp:RequiredFieldValidator ID="Rqphone" runat="server" ControlToValidate="phone"
                            ErrorMessage="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revphone" runat="server" ControlToValidate="phone"
                            ErrorMessage="Only numeric characters are allowed" ValidationExpression="[^a-zA-Z ]*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="third">
                        Course Name
                        <img src="../images/req.gif">
                        <br />
                        <asp:ListBox runat="server" ID="ddlCourses" onclick="onselected();" Width="600" Height="225px"
                            SelectionMode="Multiple"></asp:ListBox>
                        <asp:RequiredFieldValidator ID="Reqcourses" runat="server" ErrorMessage="*" ControlToValidate="ddlCourses"
                            InitialValue="0"></asp:RequiredFieldValidator>
                        <br />
                        <span style="color: #8D8D8D; font-size: normal; margin: 0px  0px  0px 0px; vertical-align: top;">
                            Press Ctrl key to choose multiple courses.</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="Submit" runat="server" onserverclick="btnSubmit_Click" class="third" name="ctl00$cphContent$Submit"
                            title="Submit" type="submit" value="Submit" /><br>
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <div class="span-4">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using log4net.Config;

namespace QPS.Training
{
    public partial class Notification : System.Web.UI.Page
    {
        public string sub1;
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
       
        protected void Page_Load(object sender, EventArgs e)
        {
            sub1 = Request.QueryString["sub"];
            try
            {
                lblhead.Text = sub1;
                if (!IsPostBack)
                    getCourses();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Clients::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        private void getCourses()
        {
            ddlCourses.DataSource = CourseController.GetAllCourse_elearn();
            ddlCourses.DataTextField = "CourseTitle";
            ddlCourses.DataValueField = "CourseID";
            ddlCourses.DataBind();

            ddlCourses.Items.Add(new ListItem("- Select Course -", "0"));
            ddlCourses.SelectedValue = "0";
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string txtBody, FirstName, LastName, Email, Company, State, Phone, Coursename, Cost;
                for (int i = 0; i < ddlCourses.Items.Count; i++)
                {
                    if (ddlCourses.Items[i].Selected)
                    {
                     
                          FirstName = Firstname.Value.Trim();
                          LastName = Lastname.Value.Trim();
                          Company = company.Value.Trim();
                          State =   state.Value.Trim();
                          Email = Emailaddress.Value.Trim();
                          Phone = phone.Value.Trim();
                          Coursename =  ddlCourses.Items[i].Text;
                          DataTable ds = new DataTable();
                          ds = CourseController.SelectAlleCourseDetailsByCourseID(Convert.ToInt16( ddlCourses.Items[i].Value));
                            Hashtable hsDocument = new Hashtable();
                        hsDocument.Add("$$FIRSTNAME$$", FirstName);
                        hsDocument.Add("$$LASTNAME$$", LastName);
                        hsDocument.Add("$$COMPANY$$", Company);
                        hsDocument.Add("$$Location$$",txtlocation.Value.Trim());
                        hsDocument.Add("$$STATE$$", State);
                        hsDocument.Add("$$EMAIL$$", Email);
                        hsDocument.Add("$$PHONE$$", Phone);
                        hsDocument.Add("$$CourseName$$", Coursename);
                        hsDocument.Add("$$CourseDesc$$", ds.Rows[0]["CourseDesc"].ToString());
                        hsDocument.Add("$$CourseNo$$", ds.Rows[0]["CourseNo"].ToString());
                        hsDocument.Add("$$Duration$$", ds.Rows[0]["Duration"].ToString());
                        hsDocument.Add("$$Cost$$",'$'  +" "+  ds.Rows[0]["Cost"].ToString());
                        hsDocument.Add("$$TopicsCovered$$", ds.Rows[0]["TopicsCovered"].ToString().Trim());
                        hsDocument.Add("$$MoreInfo$$", ds.Rows[0]["MoreInfo"].ToString());
                        

                        txtBody = EmailController.GetBodyFromTemplate("/EmailTemplate/Notification.htm", hsDocument);
                        EmailController.SendMail(ConfigurationSettings.AppSettings["InformationalFromMail"], Emailaddress.Value.ToString(), "Course information for your notified course  with us", txtBody, true);
                    }

                }
                    Firstname.Value = "";
                    Lastname.Value = "";
                    company.Value = "";
                    state.Value = "";
                    Emailaddress.Value = "";
                    phone.Value = "";
                    txtlocation.Value = "";
                    ddlCourses.SelectedIndex = 0;
                    ErrorMsg.InnerHtml = "<font class='second'>Your notified course information has been sent successfully!</font>";

          }

            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Register::btnSubmit_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }        
    }
}
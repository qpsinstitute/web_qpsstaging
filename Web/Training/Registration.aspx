
<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="QPS.Training.Registration" %>

<asp:Content ID="ContentReg" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Training");       
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
               <div class="span-3 blackbk last" style="width: 130px">
                <a  title="Corporate Register">Corporate Register</a></div>
         <div class="span-3 blackbl last">
                <a href="Training.aspx" title="Training Grants">Training Grants</a></div>
                <div class="span-3 blackbl last" style="width: 87px">
                <a href="/ELearning/EcourseList.aspx" title="eLearning">eLearning</a></div>
                <div class="span-3 blackbl last" style="width: 100px">
                <a href="OnSite.aspx" title="On-Site Training">On-Site Training</a></div>
                <div class="span-3 blackbl last" style="width: 120px">
                <a href="UnEmployed.aspx" title="Public Training">Public Training</a></div>
            <div class="span-3 blackbl last" style="width: 123px">
                <a  href="Calendar.aspx" title="Corporate Calendar">Corporate Calendar</a></div>
           <%--  <div class="span-3 blackbl last" style="width: 110px">
                <a href="../Instructor/Signin.aspx" title="Instructor Login">Instructor Login</a></div>--%>

            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="registration">
                Corporate Registration</h3>
            <p style="font-size: 125%">
                Register <a href="../Registration/Registration.aspx" class="nounder bold" title="Online">
                    Online</a> or via <a target="" href="../documents/QPS Registration form.doc" class="nounder bold"
                        title="Fax">Fax</a>.
            </p>
        </div>
        <div class="span-4" style="">
            <img src="../images/training.gif" class="pull-1" alt="QPS Onsite Training">
            <hr class="space">
        </div>
        <div class="span-16">
            &nbsp;
        </div>
    </div>

</asp:Content>

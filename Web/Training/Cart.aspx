<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Cart.aspx.cs" Inherits="QPS.Training.Cart" %>

<asp:Content ID="ContentCart" ContentPlaceHolderID="cphContent" runat="server"  >
<script language="javascript" type="text/javascript">
    setPage("Training");
</script>
<div class="span-24 prepend-1 top highcont">
	<div class="span-18 top" style="float:right">
	    <div class="span-3 blackbl last"><a href="Calendar.aspx" title="Calendar">Calendar</a></div>		
		<div class="span-3 blackbl last"><a href="OnSite.aspx" title="Onsite Training">Onsite Training</a></div>
		<div class="span-3 blackbl last"><a href="Training.aspx" title="Training">Training Grants</a></div>
		<div class="span-3 blackbl last"><a href="Registration.aspx" title="Register">Register</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">Your Cart</h3>
		<br class="clear">
		<h4>You added:</h4>
		<p>Please review the contents of your cart. You may continue adding courses or proceed to checkout.</p>
		<div class="span-12">		
		<iframe width="400" height="400" scrolling="no" frameborder="0" src="<%=CartURL%>"></iframe>
		</div>
		<br class="clear">

	</div>
	<div class="span-4">
	    <img src="../images/addtocart-big.gif" border="0" class="pull-1 top">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif">
	        <h3 class="mostwrap"><a href="/Training/Calendar.aspx" class="nounder">Click here</a></h3> for our Upcoming Corporate Training Programs
	    </div>
	</div>
	<div class="span-16">
	    &nbsp;
	</div>
</div>
</asp:Content>

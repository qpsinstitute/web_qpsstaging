<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Institute.aspx.cs" Inherits="QPS.Training.Institute" MasterPageFile="~/QPSSite.Master"%>

<asp:Content ID="ContentInstitute" ContentPlaceHolderID="cphContent" runat="server">
<script language="javascript" type="text/javascript">
    setPage("Institute");       
</script>
<script language="javascript" type="text/javascript">
	function openPopup(txtURL)
	{
        window.open(txtURL,"","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width=600,height=500");
	}
</script>
<style type="text/css">
body#libra {
  margin: 0 5px 0 3px;
  padding:0px;
  font:80.5% Arial,Helvetica,sans-serif; 
  color:#000;
  background-color:#fff;
  min-width:902px;
  }
  html body#libra {
    font:80% Arial,Helvetica,sans-serif; 
    width: expression((documentElement.clientWidth < 910) ? "902px" : "auto" ); 
    }
pre { font-size:124.5%; } 
  html pre { font-size:100%; }


/***
-----------------------------------
Web Hierarchical Navigation - hinav
-----------------------------------
. defines classes: parent, peer, selected, & child.
. legend:
     li    : bottom
     li li : all others
***/

.hinav {
  position:relative;
  padding:0px 0px 2px 0;
  }
html .hinav { width:182px; }
.hinav h3 { display:none; }
.hinav ul {
  margin:0;
  padding:0;
  }
.hinav li {
  display:inline;
  margin:0;
  padding:0;
  list-style:none;
  }
.hinav li a, .hinav li a:visited, .hinav span {
  position:relative;
  display:block;
  margin-bottom:-1px;
  color:#2f6681;
  background-color:transparent;
  font-size:86%;
  text-decoration:none;
  border-bottom:1px solid #acc2cd;
  }

/* Bring selected to front */
.hinav li a.selected, .hinav li a.selected:visited, .hinav span.selected {
  z-index:10;
  background-color:#dde6eb;
  border-top:1px solid #c8cacc;
  border-bottom:1px solid #acc2cd;
  }

/* Cancel topmost border */
.hinav li li a, .hinav li li a:visited,  .hinav li li span {
  border-top:1px solid #d5e0e6;
  }
.hinav a, .hinav a:visited, .hinav span {
  padding:3px 6px 3px 7px;
  width:169px; /* IE redraw */
  }
.hinav a.peer, .hinav a.peer:visited,
.hinav a.selected, .hinav a.selected:visited,
.hinav span.peer, .hinav span.selected {
  padding-left:0px;
  width:151px; /* IE redraw */
  font-weight:bold;
  }
.hinav a.child, .hinav a.child:visited, .hinav span.child {
  padding-left:5px;
  width:147px; /* IE redraw */
  }

/* hinav special case for peers of parent */
.hinav.show_parent_peers a.parent_peer {
   padding-left: 0px !important;
   width: 162px !important;
   }
.hinav.show_parent_peers li a.selected {
  padding-left: 0px !important;
  width: 120px !important;
  }
</style>

<div class="span-24 prepend-1 top highcont">

<div class="span-18 top" style="float:right">
	    <div class="span-3 blackbk last" style="width:80px"><a href="Institute.aspx" title="QPS Institute">QPS Institute</a></div>
	    <div class="span-3 blackbl last" style="width:80px"><a href="Calendar.aspx" title="Calendar">Calendar</a></div>
        <div class="span-3 blackbl last" style="width:160px"><a href="UnEmployed.aspx" title="Training for Unemployed">Training for Unemployed</a></div>
		<div class="span-3 blackbl last" style="width:100px"><a href="OnSite.aspx" title="Onsite Training">Onsite Training</a></div>
		<div class="span-3 blackbl last"><a title="Training">Training Grants</a></div>		
		<div class="span-3 blackbl last" style="width:100px"><a href="Registration.aspx" title="Register">Register</a></div>
		<br class="clear"><br class="clear">
		<h3 class="colbright" id="training">QPS Institute</h3>
		<p>
         <table>
		    <tr>
		        <td>
		           <div style="padding-left: 20px">
		            <b class="colbright" style="font-style:italic; font-size:15px;">About our Founder</b> <br /> 
                         <p style="font-size: 120%;">
                           <p style="font-size: 120%;">Jay P. Patel, President & CEO of Quality & Productivity Solutions, Inc. possesses
                             over 25 years of professional work experience in management and quality assignments,
                             including holding past positions as Project Engineer, Program Manager, Plant Manager,
                             and Director of Corporate Quality. His professional work experience includes working
                             at General Electric, Allied Signal-Bendix, United Technologies-Carrier, and Cabot
                             Safety Corporation. He has provided consulting to many Fortune 500 clients and trained
                             thousands of people.<br /></p></p>
                           <p style="font-size: 120%">
                             Jay has taught on site courses to several public and private companies. He has presented
                             speeches and seminars to various organizations such as <a href="http://www.pmi.org/" target="_blank">Project Management Institute,</a>
                             <a href="http://www.apics.org/default.htm" target="_blank">American Production and Inventory Control Society,</a> 
                             <a href="http://www.iienet2.org/" target="_blank">Institute of Industrial Engineers,</a>
                             <a href="http://www.asq.org/?WT.srch=1&_vsrefdom=ppcyahoo&WT.mc_id=MAXPPC2&utm_source=yahoo&utm_medium=cpc&utm_campaign=Branded" target="_blank"> American Society for Quality,</a> and 
                             <a href="http://www.sme.org/cgi-bin/getsmepg.pl?/new-sme.html&&&SME&" target="_blank"> Society of Manufacturing Engineers.</a>
                             He holds 10 ASQ Certifications including Six Sigma Certified Black Belt and Certified Quality
                             Auditor. He has a Bachelor�s Degree in Engineering and two Master Degrees (one in
                             Engineering from Fairleigh Dickinson University and one in Business Administration
                             from the University of Bridgeport). He is a RAB-Quality System Lead Auditor and
                             has taught an RAB-approved course for ISO 9001: 2000 Transition and Auditing. Jay
                             has been the National Malcolm Baldrige Quality Award Examiner.<br /></p>
                           <p style="font-size: 120%">
                             Jay holds PMI certifications. He is a certified Project Management Professional
                             (PMP�), a certified Risk Management Professional (RMP�) and a certified Scheduling
                             Professional (PMI-SP�). Jay has served as Chapter President of the Project Management
                             Institute and served as Chapter President of Institute of Industrial Engineers.
                             He has held a number of positions at Section and Region levels within ASQ, including
                             Worcester Section Chairman, Education and Certification Chair. Currently, Jay is
                             the Chairman of <a href="http://www.neqc.org/" target="_blank">ASQ NEQC (North East Quality Council).</a> 
                             Jay has co-authored the <a href="http://www.qualitycouncil.com/" target="_blank"> Quality Council of Indiana�s Six Sigma Black Belt & Green Belt Primers,</a>  
                             which are used in QPS programs at the QPS Institute.<br />
                            <%--QPS provides consulting, training and auditing services for ISO, Six Sigma, Lean, Quality Management System, Project Management, Supply Chain and other related topics.  Jay has been the National Malcolm Baldrige Quality Award Examiner.  He has also taught on site courses to several public and private companies.  Jay has presented speeches and seminars to various organizations such as <a href="http://www.pmi.org/" target="_blank">Project Management Institute,</a> <a href="http://www.apics.org/default.htm" target="_blank">American Production and Inventory Control Society,</a> <a href="http://www.iienet2.org/" target="_blank">Institute of Industrial Engineers,</a> <a href="http://www.asq.org/?WT.srch=1&_vsrefdom=ppcyahoo&WT.mc_id=MAXPPC2&utm_source=yahoo&utm_medium=cpc&utm_campaign=Branded" target="_blank">American Society for Quality,</a> and <a href="http://www.sme.org/cgi-bin/getsmepg.pl?/new-sme.html&&&SME&" target="_blank">Society of Manufacturing Engineers.</a>  <br />
                            In addition, Jay has 10 ASQ Certifications including Six Sigma Certified Black Belt and Certified Quality Auditor.  Jay�s educational achievements include a Bachelor in Engineering and two Master Degrees (one in Engineering from Fairleigh Dickinson University and one in Business Administration from the University of Bridgeport).  He is a RAB-Quality System Lead Auditor and has taught an RAB-approved course for ISO 9001: 2000 Transition and Auditing.  He has served as Chapter President of the Project Management Institute and is also a certified Project Management Professional.  He has also served as Chapter President of Institute of Industrial Engineers.<br />
                            Jay has held a number of positions at Section and Region levels within ASQ, including Worcester Section Chairman, Education and Certification Chair.  Currently, Jay is the Chairman of <a href="http://www.neqc.org/" target="_blank">ASQ North East Quality Council.</a> Jay has co-authored <a href="http://www.qualitycouncil.com/" target="_blank"> Quality Council of Indiana�s Six Sigma Black Belt & Green Belt Primers,</a> which are used in QPS programs at the QPS Institute.<br />--%>
                         </p>
		            
                    <b class="colbright" style="font-style:italic; font-size:15px;">Our Commitment to Education and Training</b>
                         <p style="font-size: 120%">
                            The QPS Institute is committed to providing the highest quality of education and training for our clients.  Our commitment to the goal of providing quality-driven training and education to our clients is strongly anchored in our history of client success and fully focused in our future mission to continue client success initiatives.  
                            <br class="clear">
                            As global providers of business solutions, quality, productivity and performance excellence, our expertise in the areas of Lean, Six Sigma, Management System, Supply Chain, Project Management and Professional Development demands adherence to the highest of standards which we continuously administer.  Our aim to provide world-class training is accomplished daily in our newly opened corporate training center, through using state of the art technology, and with expert leaders mentoring every step of the way.
                          </p>
                      
                     <b class="colbright" style="font-style:italic; font-size:15px;">Project Management</b>
                         <p style="font-size: 120%">
                            The QPS Institute provides well-rounded training in all areas of Project Management and for PMI certifications, using the Project Management Body of Knowledge (PMBoK) as its foundation for learning.  All of our instructors are PMI PMP� Certified and some of them have multiple certifications such as PMI Risk Management Professionals, ASQ Certified Six Sigma Black Belts and many others.  Over 90% of our clients pass PMI PMP� exam at first attempt due to our unique and personalized approach to Project Management Learning. 
                            <br class="clear">
                            The QPS Institute will help you to achieve PM knowledge and skills, whether your goal is to become a certified Project Management Professional (PMP�), a Risk Management Professional (PMI-RMP�), Program Management Professional (PgMP�) or a Scheduling Professional (PMI-SP�).   All of our Instructors have 15+ years of project experience, cover all practice questions at the end of each module, administer two sample exams and administer a QPS final exam.  Having the ability to practice 1500+ questions, using a project example as an application exercise in class to build skills and access to instructors that are experts because they are industry practitioners contribute to all clients� successful learning, as well as a satisfaction score of 4.9 out of 5.0.
                          </p>
                      <b class="colbright" style="font-style:italic; font-size:15px;">Lean Six Sigma</b>
                         <p style="font-size: 120%">
                            QPS has become a recognized performer in the fields of consulting and training.  Our success is based on the commitment and promise to help our clients succeed as well as the enthusiasm and results we generate to manage and sustain that success.  QPS brings bottom line results by evaluating a client�s needs and developing breakthrough strategies while enhancing processes and people.  We consistently work to understand our clients and turn that knowledge into meaningful results.  Jay P. Patel, QPS founder, has co-authored Six Sigma Black Belt & Green Belt Primers which follow the ASQ Body of Knowledge and are used at the QPS Institute.
                            <br />
                         </p>
                      <b class="colbright" style="font-style:italic; font-size:15px;">Trainers</b> <br /> 
                          <font style="font-size: 120%">QPS has trained over thousands of students in the quality and business improvement area, with instructors that possess a significant amount of credentials.  Some QPS staff are members of:
                                <ul style="padding-left:80px;">
                                    <li> <a target="_blank" href="http://www.asq.org/">American Society for Quality</a></li>
                                    <li> <a target="_blank" href="http://www.pmi.org/Pages/default.aspx">Project Management Institute</a></li>
                                    <li> <a target="_blank" href="http://www.sme.org/cgi-bin/getsmepg.pl?/html/about.htm&&&SME&">Society of Manufacturing Engineers</a></li>
                                    <li> <a target="_blank" href="http://www.apics.org/default.htm">American Production & Inventory Control</a></li>
                                    <li> <a target="_blank" href="http://www.nawdp.org//AM/Template.cfm?Section=HomePage">National Association of Workforce Development Professionals</a></li></ul>
                            The certification of trainers includes:
                              <ul style="padding-left:80px;">
                                    <li> ASQ certified Six Sigma Black Belt </li>
                                    <li> ASQ Certified Quality Engineer</li>
                                    <li> ASQ Certified Quality Manager</li>
                                    <li> ASQ Certified Quality Improvement Associate</li>
                                    <li> ASQ Certified Quality Auditors</li>
                                    <li>SME Lean</li>
                                    <li>Senior Professional in Supply Chain Management (SPSM�)</li> 
                                    <li>PMI Certified Project Management Professional</li> 
                                    <li>PMI Risk Management Professional</li>   
                                    <li>RAB Certified Environmental Auditors</li> 
                                    <li>RAB Certified Lead Quality Management System Auditors</li> 
                                    <li>QPS Certified Master Black Belts</li> 
                                    <li>QPS Certified Lean Experts</li> 
                                </ul>
                         The trainers designated have taught:
                             <ul style="padding-left:80px;">
                                    <li>  Black Belt Certification courses  </li>
                                    <li> Green Belt Certification courses </li>
                                    <li> Champion Certification courses </li>
                                    <li> Master Black Belt Certification courses</li>
                                    <li> Lean Certification courses </li>
                                    <li>Train Trainer courses </li>
                                    <li>Supply Chain Excellence courses</li> 
                                    <li>ISO 9001 courses and other ISO related courses</li> 
                                    <li>ASQ/APICS/SME certification courses (Supply Chain, Lean, Green Belt, Black Belt, etc.)</li>   
                                </ul>
                          </font>
                          <br class ="clear" />
                    </div>
		        </td>
		    </tr>
		</table>
		
		<p style="float:right;text-align:right;"><a class="nounder" href="#top" title="Top"><img src="../images/top.gif" title="Top"></a></p>
	</div>
	<div class="span-4">
	    <img src="../images/training.gif" border=0 class="pull-1 top">
	    <hr class="space">
	    <div style="margin-top:40px;padding:7px;border:1px dotted silver;background:#eaeaea;">
	        <img src="../images/check.gif" title="Click here">
            <h3 class="mostwrap">
                <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
            for our Upcoming Corporate Training Programs
	    </div>
	    <br />
	</div>
</div>
</asp:Content>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
<%@ Page Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Training.aspx.cs"
    Inherits="QPS.Training.Training" %>
<asp:Content ID="ContentTraining" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Training");       
    </script>
    <script language="javascript" type="text/javascript">
        function openPopup(txtURL) {
            window.open(txtURL, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width=600,height=500");
        }
    </script>
    <style type="text/css">
        body#libra
        {
            margin: 0 5px 0 3px;
            padding: 0px;
            font: 80.5% Arial,Helvetica,sans-serif;
            color: #000;
            background-color: #fff;
            min-width: 902px;
        }
        html body#libra
        {
            font: 80% Arial,Helvetica,sans-serif;
            width: expression((documentElement.clientWidth < 910) ? "902px" : "auto" );
        }
        pre
        {
            font-size: 124.5%;
        }
        html pre
        {
            font-size: 100%;
        }
        
        
        .hinav
        {
            position: relative;
            padding: 0px 0px 2px 0;
        }
        html .hinav
        {
            width: 182px;
        }
        .hinav h3
        {
            display: none;
        }
        .hinav ul
        {
            margin: 0;
            padding: 0;
        }
        .hinav li
        {
            display: inline;
            margin: 0;
            padding: 0;
            list-style: none;
        }
        .hinav li a, .hinav li a:visited, .hinav span
        {
            position: relative;
            display: block;
            margin-bottom: -1px;
            color: #2f6681;
            background-color: transparent;
            font-size: 86%;
            text-decoration: none;
            border-bottom: 1px solid #acc2cd;
        }
        
        /* Bring selected to front */
        .hinav li a.selected, .hinav li a.selected:visited, .hinav span.selected
        {
            z-index: 10;
            background-color: #dde6eb;
            border-top: 1px solid #c8cacc;
            border-bottom: 1px solid #acc2cd;
        }
        
        /* Cancel topmost border */
        .hinav li li a, .hinav li li a:visited, .hinav li li span
        {
            border-top: 1px solid #d5e0e6;
        }
        .hinav a, .hinav a:visited, .hinav span
        {
            padding: 3px 6px 3px 7px;
            width: 169px; /* IE redraw */
        }
        .hinav a.peer, .hinav a.peer:visited, .hinav a.selected, .hinav a.selected:visited, .hinav span.peer, .hinav span.selected
        {
            padding-left: 0px;
            width: 151px; /* IE redraw */
            font-weight: bold;
        }
        .hinav a.child, .hinav a.child:visited, .hinav span.child
        {
            padding-left: 5px;
            width: 147px; /* IE redraw */
        }
        
        /* hinav special case for peers of parent */
        .hinav.show_parent_peers a.parent_peer
        {
            padding-left: 0px !important;
            width: 162px !important;
        }
        .hinav.show_parent_peers li a.selected
        {
            padding-left: 0px !important;
            width: 120px !important;
        }
    </style>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <%--<div class="span-3 blackbl last" style="width:80px"><a href="Institute.aspx" title="QPS Institute">QPS Institute</a></div>--%>
             <div class="span-3 blackbl last" style="width: 130px">
                <a href="Registration.aspx" title="Corporate Register">Corporate Register</a></div>
         <div class="span-3 blackbk last">
                <a  title="Training Grants">Training Grants</a></div>
                <div class="span-3 blackbl last" style="width: 87px">
                <a href="/ELearning/EcourseList.aspx" title="eLearning">eLearning</a></div>
                <div class="span-3 blackbl last" style="width: 100px">
                <a href="OnSite.aspx" title="On-Site Training">On-Site Training</a></div>
                <div class="span-3 blackbl last" style="width: 120px">
                <a href="UnEmployed.aspx" title="Public Training">Public Training</a></div>
            <div class="span-3 blackbl last" style="width: 123px">
                <a href="Calendar.aspx" title="Corporate Calendar">Corporate Calendar</a></div>

           <%-- <div class="span-3 blackbl last" style="width: 110px">
                <a href="../Instructor/Signin.aspx" title="Instructor Login">Instructor Login</a></div>--%>
            <br class="clear">
            <br class="clear">
            <h3 class="colbright" id="training">
                Training Grants</h3>
            <p>
                <table style="font-size: 125%">
                    <tr>
                        <td>
                            <div style="border: solid 1px black; background-color: #cfcffc;">
                                "QPS offers some painstakingly-designed courses in a plethora of topics which are
                                prepared according to business, industry and professional needs. The <a class="nounder bold"
                                    href="../Training/Calendar.aspx" title="Course Calendar">Calendar</a> explains
                                and presents major vital aspects of Professional Development, Quality, Standards,
                                Productivity, Business Improvement, Effective Strategies and almost every vital
                                aspect of an organization."
                            </div>
                            <div class="hrefnounderclass">
                                &nbsp; Click here for information on the <a style="background: none; padding: 0px;"
                                    href="javascript:openPopup('../documents/State of Mass Workforce Training Grants.htm')"
                                    class="nounder bold" title="State of Mass Workforce Training Grants">State of Massachusetts
                                    Workforce Training Grants</a>.</div>
                        </td>
                    </tr>
                </table>
            </p>
        </div>
        <div class="span-4">
            <img src="../images/training.gif" border="0" class="pull-1 top">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <br />
        </div>
        <div class="span-4" style="margin-top: 5px; padding: 0px; border: 0px dotted silver;
            display: none">
            <table style="margin-left: 0px; margin-top: 0px; marker-offset: auto; z-index: 10;"
                cellspacing="0" cellpadding="0" border="0" summary="">
                <tr>
                    <td id="framework-column-left" valign="top">
                        <!-- CDC-DM: Hinav Start -->
                        <div class="hinav">
                            <ul class="outer">
                                <li>
                                    <ul>
                                        <li>
                                            <ul>
                                                <li>
                                                    <ul>
                                                        <li><span class="selected">Select Topic To See Our Training Programs</span> </li>
                                                        <ul>
                                                            <li><a href="../Training/Calendar.aspx?id=2" title="AS 9100" class="child">AS 9100</a></li>
                                                            <li><a href="../ELearning/EcourseList.aspx" title="E Learning" class="child">E Learning</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=4" title="FDA Related" class="child">FDA Related</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=6" title="Human Resources" class="child">Human
                                                                Resources</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=2" title="ISO 9001:2008" class="child">ISO
                                                                9001:2008</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=3" title="ISO 14001" class="child">ISO 14001</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=4" title="ISO 13485" class="child">ISO 13485</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=2" title="ISO/TS 16949" class="child">ISO/TS
                                                                16949</a></li>
                                                            <li><a href="../Training/Calendar.aspx" title="Lean Six Sigma" class="child">Lean Six
                                                                Sigma</a></li>
                                                            <li><a href="../Training/Onsite.aspx" title="Onsite Training" class="child">Onsite Training</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=9" title="Project Mgmt. Certification"
                                                                class="child">Project Mgmt. Certification</a></li>
                                                            <li><a href="../Training/Training.aspx" title="Public Training" class="child">Public
                                                                Training</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=6" title="Service" class="child">Service</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=8" title="Software" class="child">Software</a></li>
                                                            <li><a href="../Training/Calendar.aspx?id=5" title="Statistics" class="child">Statistics</a></li>
                                                        </ul>
                                                </li>
                                                <li><strong><span class="selected">Quick Connect >></span></strong>
                                                    <ul>
                                                        <li><a href="../Company/about.aspx" title="About Us" class="child">About Us</a></li>
                                                        <li><a href="../Consulting/QualitySystem.aspx" title="AS 9100" class="child">AS 9100</a></li>
                                                        <li><a href="../Training/Calendar.aspx" title="Calendar" class="child">Calendar</a></li>
                                                        <li><a href="../Company/Clients.aspx" title="Clients" class="child">Clients</a></li>
                                                        <li><a href="../Consulting/Consulting.aspx" title="Consulting" class="child">Consulting</a></li>
                                                        <li><a href="../Company/Contact.aspx" title="Contact Us" class="child">Contact Us</a></li>
                                                        <li><a href="../Consulting/QualitySystem.aspx" title="FDA" class="child">FDA</a></li>
                                                        <li><a href="../Consulting/QualitySystem.aspx" title="ISO 14000" class="child">ISO 14000</a></li>
                                                        <li><a href="../Consulting/QualitySystem.aspx" title="ISO 9001:2008" class="child">ISO
                                                            9001:2008</a></li>
                                                        <li><a href="../Consulting/QualitySystem.aspx" title="ISO 13485" class="child">ISO 13485</a></li>
                                                        <li><a href="../Training/Calendar.aspx?id=2" title="ISO/TS 16949" class="child">ISO/TS
                                                            16949</a></li>
                                                        <li><a href="../Consulting/QualitySystem.aspx" title="ISO 27001" class="child">ISO 27001</a></li>
                                                        <li><a href="../Consulting/BusinessImprovement.aspx" title="Leadership" class="child">
                                                            Leadership</a></li>
                                                        <li><a href="../Resources/Links.aspx" title="Links" class="child">Links</a></li>
                                                        <li><a href="../Training/Calendar.aspx" title="Map & Directions" class="child">Map &
                                                            Directions</a></li>
                                                        <li><a href="../Consulting/QualitySystem.aspx" title="NQA" class="child">NQA</a></li>
                                                        <li><a href="../Consulting/BusinessImprovement.aspx" title="Outsourcing" class="child">
                                                            Outsourcing</a></li>
                                                        <li><a href="../Consulting/BusinessImprovement.aspx" title="Project Management" class="child">
                                                            Project Management</a></li>
                                                        <li><a href="../Consulting/QualitySystem.aspx" title="Quality System" class="child">
                                                            Quality System</a></li>
                                                        <li><a href="../Training/Registration.aspx" title="Register" class="child">Register</a></li>
                                                        <li><a href="../Consulting/BusinessImprovement.aspx" title="Strategic Planning" class="child">
                                                            Strategic Planning</a></li>
                                                        <li><a href="../Consulting/BusinessImprovement.aspx" title="Supply Chain" class="child">
                                                            Supply Chain</a></li>
                                                        <li><a href="../Consulting/BusinessImprovement.aspx" title="Teams" class="child">Teams</a></li>
                                                        <li><a href="../Company/Testimonials.aspx" title="Testimonials" class="child">Testimonials</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!-- CDC-DM: Hinav End -->
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
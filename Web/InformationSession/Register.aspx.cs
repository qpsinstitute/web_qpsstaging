using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using log4net.Config;

namespace QPS.InformationSession
{
    public partial class Register : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {}
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Clients::Page_Load - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (comments.Value.Trim().Length > 5000)
                {
                    ErrorMsg.InnerHtml = "<font class='second'>Questions/Comments can't exceed 5000 characters.</font>";
                }
                else
                {
                    string txtBody,Name,Email,Company,Position,Telephone,Comments = "";
                    Name = name.Value.Trim();
                    Email = emailaddress.Value.Trim();
                    Company = company.Value.Trim();
                    if (position.Value.Trim() != "")
                        Position = "as a " + position.Value.Trim();
                    else
                        Position = "";
                    Telephone = telephone.Value.Trim();
                    Comments = comments.Value.Trim();
                    Hashtable hsDocument = new Hashtable();
                    hsDocument.Add("$$NAME$$", Name);
                    hsDocument.Add("$$EMAIL$$", Email);
                    hsDocument.Add("$$COMPANY$$", Company);
                    hsDocument.Add("$$POSITION$$", Position); 
                    hsDocument.Add("$$TELEPHONE$$", Telephone);
                    hsDocument.Add("$$COMMENTS$$", Comments);
                    txtBody = EmailController.GetBodyFromTemplate("/EmailTemplate/Contact.htm", hsDocument);
                    EmailController.SendMail(ConfigurationSettings.AppSettings["InformationalFromMail"], ConfigurationSettings.AppSettings["InformationalToMail"], "Free Informational Session - Registration", txtBody, true);
                    name.Value = "";
                    company.Value = "";
                    emailaddress.Value = "";
                    telephone.Value = "";
                    position.Value = "";
                    comments.Value = "";
                    ErrorMsg.InnerHtml = "<font class='second'>Your Registration Information has been sent successfully!</font>";
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Register::btnSubmit_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }        
    }
}

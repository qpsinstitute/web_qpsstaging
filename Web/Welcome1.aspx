<%@ Page AutoEventWireup="true" Codebehind="Welcome1.aspx.cs" Inherits="QPS.Welcome"
    Language="C#" MasterPageFile="~/QPSSite.Master" Title="Welcome" %>
<asp:Content ID="Contentwelcome" runat="server" ContentPlaceHolderID="cphContent">

    <script language="javascript" type="text/javascript">
    function changepage(pagenm)
    {
        
        if(pagenm == "welcome")
        {
            document.getElementById('tblwelcome').style.display='block';
            document.getElementById('tblabout').style.display='none';
            document.getElementById('tblcommit').style.display='none';
            document.getElementById('abtimg').setAttribute("src", "../images/about_us.gif");
            document.getElementById('commit').className= "span-3 blackbl last";
            document.getElementById('about').className = "span-3 blackbl last";
            document.getElementById('welcome').className = "span-3 blackbk last";
            document.getElementById('textlbl').innerHTML="Welcome QPS";
        }
    
       if(pagenm =="About")
        {
            document.getElementById('tblwelcome').style.display='none';
            document.getElementById('tblabout').style.display='block';
            document.getElementById('tblcommit').style.display='none';
            document.getElementById('abtimg').setAttribute("src", "../images/Photo of Our Founder.jpg");
            document.getElementById('about').className = "span-3 blackbk last";
            document.getElementById('welcome').className = "span-3 blackbl last";
            document.getElementById('commit').className= "span-3 blackbl last";
            document.getElementById('abtimg').style.height  = '200px'
            document.getElementById('abtimg').style.width  = '193px'
            document.getElementById('textlbl').innerHTML="Founder, Jay Patel";
        }
           if(pagenm =="Commitment")
        {
            document.getElementById('tblwelcome').style.display='none';
            document.getElementById('tblabout').style.display='none';
            document.getElementById('tblcommit').style.display='block';
            document.getElementById('abtimg').setAttribute("src", "../images/about_us.gif");
            document.getElementById('commit').className= "span-3 blackbk last";
            document.getElementById('about').className = "span-3 blackbl last";
            document.getElementById('welcome').className = "span-3 blackbl last";
            document.getElementById('textlbl').innerHTML="Commitment";
            
        }
        
    }
    </script>
    <div class="span-24 prepend-1 top highcont">
    
    <div class="span-18 top" style="float: right">
    
        <div id="welcome" class="span-3 blackbk last" style="width: 80px;">
            <a onclick="javascriprt:changepage('welcome')" title="Welcome">Welcome</a></div>
        <div id="about" class="span-3 blackbl last" style="width: 120px;">
            <a onclick="javascriprt:changepage('About')" title="About our Founder">About our Founder</a></div>
        <div id="commit" class="span-3 blackbl last" style="width: 250px;">
            <a onclick="javascriprt:changepage('Commitment')" title="Our Commitment to Education & Training">Our Commitment to Education & Training</a></div>
        <br class="clear">
        <br class="clear">
        <table id="tblwelcome" cellpadding="0" cellspacing="0" border="0" >
    <tr>
        <td colspan="2" style="font-family:Times New Roman;font-size:xx-large;color:#1c3751;" >
            Director�s Welcome
        </td>
    </tr>
    <tr>
        <td colspan="2" style="font-family:Lucida Sans Unicode;font-size:large;font-weight :bold;">
        By Jay Patel
        </td>
    </tr>
    <tr>
        <td style="font-family: Times New Roman; font-size: medium;text-align: justify">
      <p>Greetings,</p>
            <p>On behalf of QPS, a most sincere thanks to all of you for allowing us
            to work with you. QPS was formed in 1996 by President & CEO Jay P. Patel, to provide
            consulting and training services in the areas of quality and productivity in addition
            to performance excellence business solutions. Our goal is to continuously strive
            to deliver quality consulting services as well as educational opportunities. QPS,
            who has been expanding its training and holds state licensure to operate as a Private</p>
            <p>
             Occupational School though the Commonwealth of Massachusetts Department of Education,
            opened the QPS Institute. Located in the vicinity of several major metropolitan
            areas � Providence, Hartford and Boston - the QPS Institute provides an opportunity
            to experience world-class training within driving distance. With new programs being
            created, new out of state training locations added and eLearning available, the
            QPS Institute continues to grow. Whether your interest is in Lean Six Sigma, Project
            Management, ISO or Supply Chain---we trust you will find your experience at the
            QPS Institute to be successful, beneficial and memorable!</p> <p> QPS is also and approved
            Training Provider for several states, providing public training for those who are
            unemployed, and works with the One Stop Career Center Systems under the Workforce
            Investment Office�s Department of Labor & Training. By being an approved Training
            Provider, QPS can give those that are unemployed an opportunity to obtain certifications
            for Lean, Six Sigma, Project Management, and others through funding from state and
            federal entities. Those interested can easily call the local career center and make
            an appointment with their career counselor to start the application process for
            funding, and career centers are scattered generously throughout each state. QPS
            is also the lead training provider for many companies seeking training for their
            employees. Our extensive client list can be found on our website. QPS also provides
            grant writing services and training needs assessments for those companies seeking
            funding through the State of Massachusetts Workforce Training Grant.</p><p> For this upcoming
            2012 year, expect new programs and new events. Our strong interest in creating new,
            and slightly different, training programs that provide industry-recognized certifications,
            will be adding more variety, and more training selection options for our future
            clients. Our passion for educational opportunities and customer satisfaction foster
            strong ties that we believe are essential to not just our success, but to yours
            as well!</p>
        </td>
    </tr>
</table>
        <table id="tblabout"  style="display:none;" border="0" cellpadding="0" cellspacing="0">
            
            <tr>
                <td style="font-family: Times New Roman; font-size:medium; text-align: justify">
                    <p style="font-weight: bold; font-style: italic; font-family: Times New Roman; font-size: large;">
                        About our Founder</p>
                    <p>
                       Jay P. Patel, President & CEO of Quality & Productivity Solutions, Inc. possesses
                        over 25 years of professional work experience in management and quality assignments,
                        including holding past positions as Project Engineer, Program Manager, Plant Manager,
                        and Director of Corporate Quality. His professional work experience includes working
                        at General Electric, Allied Signal-Bendix, United Technologies-Carrier, and Cabot
                        Safety Corporation. He has provided consulting to many Fortune 500 clients and trained
                        thousands of people.
                    </p>
                    <p>
                        Jay has taught on site to several public and private companies. He has instructed
                        all ASQ Certification, PMI Certification and APICS Certification Courses. He has
                        presented speeches and seminars to various organizations such as the Project Management
                        Institute, American Production and Inventory Control Society, Institute of Industrial
                        Engineers, American Society for Quality, and Society of Manufacturing Engineers.
                        He holds 10 ASQ Certifications including Six Sigma Certified Black Belt and Certified
                        Quality Auditor in addition to CQE, CQIA, CBA, CCT, CHA, CSQE, CQT, CMQ/OE. He has
                        a Bachelor�s Degree in Engineering and two Master Degrees (one in Engineering from
                        Fairleigh Dickinson University and one in Business Administration from the University
                        of Bridgeport). He is a RAB-Quality System Lead Auditor and has taught an RAB-approved
                        course for ISO 9001: 2000 Transition and Auditing.
                    </p>
                    <p>
                       Jay holds PMI certifications. He is a certified Project Management Professional
                        (PMP�), a certified Risk Management Professional (RMP�) and a certified Scheduling
                        Professional (PMI-SP�). Jay has served as Chapter President of the Project Management
                        Institute and served as Chapter President of Institute of Industrial Engineers.
                        He has held a number of positions at Section and Region levels within ASQ, including
                        Worcester Section Chairman, Education and Certification Chair. Currently, Jay is
                        the Chairman of ASQ NEQC (North East Quality Council). Jay has been the National
                        Malcolm Baldrige Quality Award Examiner and is an ASQ Fellow. He has co-authored
                        the Quality Council of Indiana�s Six Sigma Black Belt & Green Belt Primers, which
                        are used in QPS programs at the QPS Institute.</p>
                        <img width="710px;" src="Images/Dr. Juran.JPG" />
                    
                </td>
            </tr>
        </table>
        <table id="tblcommit" border="0" cellpadding="0" cellspacing="0" style="display: none;">
            <tr>
                <td style="font-family: Times New Roman; font-size: medium; text-align: justify">
                    <p style="font-weight: bold; font-size: large; font-style: italic">
                        Our Commitment to Education and Training
                    </p>
                    <p>
                        The QPS Institute is committed to providing the highest quality of education and
                        training for our clients. Our commitment to the goal of providing quality-driven
                        training and education to our clients is strongly anchored in our history of client
                        success and fully focused in our future mission to continue client success initiatives.
                    </p>
                    <p>
                        As global providers of business solutions, quality, productivity and performance
                        excellence, our expertise in the areas of Lean, Six Sigma, Management System, Supply
                        Chain, Project Management and Professional Development demands adherence to the
                        highest of standards which we continuously administer. Our aim to provide world-class
                        training is accomplished daily in our newly opened corporate training center, through
                        using state of the art technology, and with expert leaders mentoring every step
                        of the way.
                    </p>
                </td>
            </tr>
        </table>

    </div>
    <div class="span-4 top">
        <img id="abtimg" class="pull-1 top" src="../images/about_us.gif">
        <hr class="space">
        <hr />
        <h4>
            <span id="textlbl"   >
            
                Welocome QPS</span>
            </h4>
        <hr />
        <ul class="tunedul mostwrap">
            <li></li>
          
        </ul>
        
        
    </div>
    </div>
</asp:Content>

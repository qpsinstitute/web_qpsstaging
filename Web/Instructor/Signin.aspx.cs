﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using Lib.Classes;
using log4net;
using log4net.Config;
using Lib.Businesslogic;
using Lib.businesslogic;


namespace QPS.Instructor
{
    public partial class Signin : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string UserName = "";
        public string PassWord = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            lblmsg.Text = "";
            txtPassword.Attributes.Add("OnKeyDown", "javascript:if (event.keyCode == 13) __doPostBack('" + "','')");
            if (txtemail.Text.Trim() != "" && txtPassword.Text.Trim() != "")
                btnLogin_Click(sender, e);
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (ValidateUserControls())
            {
                UserName = txtemail.Text.Trim();
                PassWord = txtPassword.Text.Trim();

                DataTable dtUser = InsController.CheckAdminUserAuthentication(txtemail.Text.Trim());
                if (dtUser.Rows.Count > 0)
                {
                    Session.Add("INSAdminID", dtUser.Rows[0]["InsadminId"]);
                    Session.Add("INSEmail", dtUser.Rows[0]["Email"]);
                    if (Request.QueryString["ReturnUrl"] != null)
                    {
                        if (chkVal.Value == "true")
                            Page.RegisterStartupScript("doLogin", "<script>doLoginCookie1('" + Request.QueryString["ReturnUrl"].ToString() + "');</script>");
                        else
                            Response.Redirect(Request.QueryString["ReturnUrl"], false);
                    }
                    else
                    {
                        if (chkVal.Value == "true")
                            Page.RegisterStartupScript("doLogin", "<script>doLoginCookie();</script>");
                        else
                            Response.Redirect("Instructor_Registration.aspx", false);
                    }
                }
            }
        }
        private bool ValidateUserControls()
        {
            bool isValidControls = true;
            if (txtemail.Text.Trim() != null && txtPassword.Text.Trim() != null)
            {
                DataTable userDT = InsController.CheckAdminUserAuthentication(txtemail.Text.Trim());
                if (userDT.Rows.Count > 0)
                {
                    if (txtemail.Text.ToString().Trim() != userDT.Rows[0]["Email"].ToString().Trim())
                    {
                        lblmsg.Text = "Invalid UserName Try Again !!!";
                        txtemail.Attributes.Add("Class", "Warning");
                        isValidControls = false;
                    }
                    else if (txtPassword.Text.ToString().Trim() != userDT.Rows[0]["PassWord"].ToString().Trim())
                    {
                        lblmsg.Text = "Invalid Password Try Again !!!";
                        txtPassword.Attributes.Add("Class", "Warning");
                        isValidControls = false;
                    }
                }
                else
                {
                    lblmsg.Text = "Invalid UserName/Password Try Again !!!";
                    txtemail.Attributes.Add("Class", "Warning");
                    txtPassword.Attributes.Add("Class", "Warning");
                    isValidControls = false;
                }
            }
            return isValidControls;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Data;
using System.Web.Security;
using Lib.Businesslogic;
using System.Text;
using System.IO;
using System.Configuration;
using Lib.businesslogic;

namespace QPS.Instructor
{
    public partial class ViewInstructor : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static DataTable dtRegUsers = null;
        public static DataTable dtRegUsersfile = null;
        protected static string SortDirections = "", SortExpression = "";
        string wClause = "";
        public int RowCount
        {
            get
            {
                if (ViewState["RowCount"] != null)
                    return (int)ViewState["RowCount"];
                else
                    return 0;
            }
            set
            {
                ViewState["RowCount"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                SortExpression = "Insid";
                ViewState["SortOrder"] = "DESC";
                try
                {
                    if (Session["AdminID"] != null && Session["Email"] != null)
                        BindRepeater();
                    else
                        FormsAuthentication.RedirectToLoginPage();
                }
                catch (Exception ex)
                {
                    if (log.IsErrorEnabled)
                        log.Error("[" + System.DateTime.Now.ToString("G") + "] SignIn::lnkLogin_Click - ", ex);
                    Response.Redirect("/errorpages/SiteError.aspx", false);
                }
            }
        }

        protected void Sortfname(object sender, EventArgs e)
        {
            SortExpression = "FirstName";
            if (ViewState["SortOrder"] == null)
            {
                ViewState["SortOrder"] = "ASC";
            }
            else
            {
                if (ViewState["SortOrder"].ToString() == "ASC")
                {
                    ViewState["SortOrder"] = "DESC";
                }
                else
                {
                    ViewState["SortOrder"] = "ASC";
                }
            }

            BindRepeater();

        }
        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            SortExpression = e.CommandArgument.ToString();
            if (ViewState["SortOrder"] == null)
            {
                ViewState["SortOrder"] = "ASC";
            }
            else
            {
                if (ViewState["SortOrder"].ToString() == "ASC")
                {
                    ViewState["SortOrder"] = "DESC";
                }
                else
                {
                    ViewState["SortOrder"] = "ASC";
                }
            }
            BindRepeater();
        }
        protected void BindRepeater()
        {
            dtRegUsers = InsController.GetInsdata(wClause);
            if (dtRegUsers.Rows.Count > 0)
            {
                PagedDataSource objPDS = new PagedDataSource();
                objPDS.AllowPaging = true;
                objPDS.PageSize = 10;
                objPDS.DataSource = dtRegUsers.DefaultView;
                DataView dv = dtRegUsers.DefaultView;
                //apply the sort on CustomerSurname column  
                dv.Sort = SortExpression.ToString() + "  " + ViewState["SortOrder"];

                //save our newly ordered results back into our datatable  
                dtRegUsers = dv.ToTable();
                if (objPDS.Count > 0)
                {
                    objPDS.CurrentPageIndex = RowCount; //- 1;
                    lblCurrentPage.Text = "Page: " + (RowCount + 1).ToString() + " of " + objPDS.PageCount.ToString();
                    btnPrev.Enabled = !objPDS.IsFirstPage;
                    btnNext.Enabled = !objPDS.IsLastPage;
                    rptRegistration.DataSource = objPDS;
                    rptRegistration.DataBind();
                }
            }
        }

      


        protected void btnPrev_Click(object sender, EventArgs e)
        {
            RowCount -= 1;
            BindRepeater();
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            RowCount += 1;
            BindRepeater();
        }

        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["AdminID"] != null && Session["Email"] != null)
                {
                    Session.Clear();
                    Session.Remove("AdminID");
                    Session.Remove("Email");
                }
                Response.Redirect("../Registration/SignIn.aspx?rmvCookie=yes", false);
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] QPS::lnkLogout_Click - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);
            }
        }
        protected void btnExport_click(object sender, EventArgs e)
        {
            DataTable dtExport = new DataTable();
            dtExport = InsController.GetToExport(wClause);
            ExportToSpreadsheet(dtExport);
        }
        public void ExportToSpreadsheet(DataTable table)
        {
            GridView gvUsers = new GridView();
            string attachment = "attachment; filename=InstructorReport.xls";
            Response.ClearContent();
            Response.ContentEncoding = Encoding.Unicode;
            Response.BinaryWrite(Encoding.Unicode.GetPreamble());
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gvUsers.DataSource = table;
            gvUsers.DataBind();
            gvUsers.RenderControl(htextw);
            Response.Write(stw.ToString());
            Response.End();
        }
    }
}
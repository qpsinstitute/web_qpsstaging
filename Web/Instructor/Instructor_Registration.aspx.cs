﻿using System;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lib.BusinessLogic;
using log4net;
using Lib.Businesslogic;
using System.Collections;
using Lib.businesslogic;
using System.Web.Security;

namespace QPS.Instructors
{
    public partial class Instructor_Registration : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static DataTable dtcity = null;
        public static DataTable dtstate = null;
        public static DataTable dtinfo = null;
        DataRow drinfo = null;
        public string WClause = "";
        protected string ServerQualifiedPath = "";
        int year, month;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["INSAdminID"] != null && Session["INSEmail"] != null)
                {

                    
                    txtemail.Text = Session["INSEmail"].ToString();
                    txtemail.Attributes.Add("readonly", "readonly");
                    BindLists();
                    txtFname.Focus();
                    BindEventstart();
                    BindEventend();
                    Session["Session_Table"] = null;
                    if (Request.QueryString["InsID"] != "" && Request.QueryString["InsID"] != null)
                    {
                        DateTime d = new DateTime();
                        btnsubmit.Text = "Update";
                        btnReset.Text = "Cancel";
                        int InsID = Int32.Parse(Request.QueryString["InsID"]);
                        WClause = "AND Insid =" + InsID;
                        dtinfo = InsController.GetINSInfoByWClause(WClause);
                        DataRow drinfo = dtinfo.Rows[0];
                        txtFname.Text = drinfo["FirstName"].ToString();
                        txtLname.Text = drinfo["LastName"].ToString();
                        txtemail.Text = drinfo["Email"].ToString();
                        txtmob.Text = Convert.ToString(drinfo["CellPhone"]);
                        txtclientname.Text = Convert.ToString(drinfo["Clientname"]);
                        ddlstate.SelectedValue = Convert.ToString(drinfo["Clientstate"]);
                        txtcity.Text = Convert.ToString(drinfo["ClientCity"]);
                        txttopic.Text = Convert.ToString(drinfo["Topic"]);
                        txtaccomplishments.Text = Convert.ToString(drinfo["Accomplishments"]);

                        if (drinfo["Eventdate"].ToString() != "")
                        {
                            DateTime dt = DateTime.Parse(drinfo["Eventdate"].ToString());
                            // Sessiontxt.Text = dt.ToString("MM/dd/yyyy");
                            //ddlviewhour.SelectedItem.Text = dt.ToString("HH");
                            ddlsessionyear.SelectedValue = dt.Year.ToString();
                            ddlsessionmonth.SelectedValue = dt.Month.ToString();
                            ddlsessionday.SelectedValue = dt.Day.ToString();

                        }
                        //else
                        //   // Sessiontxt.Text = drinfo["Session"].ToString();
                        txtissues.Text = drinfo["Issues"].ToString();
                        txtimprovement.Text = drinfo["improvement"].ToString();
                        txttestimonials.Text = drinfo["Testimonials"].ToString();
                        txtcomments.Text = drinfo["comments"].ToString();

                        if (drinfo["startsession"] != null && drinfo["startsession"] != System.DBNull.Value)
                        {
                            //appointment.Style.Add("display", "none");
                            //time.Style.Add("display", "none");
                            d = (DateTime)drinfo["startsession"];
                            ddlstartyear.SelectedValue = d.Year.ToString();
                            ddlstartmonth.SelectedValue = d.Month.ToString();
                            ddlstartday.SelectedValue = d.Day.ToString();
                            ddlstarthour.SelectedValue = d.Hour.ToString().Length > 1 ? d.Hour.ToString() : "0" + d.Hour.ToString();
                            ddlstartminute.SelectedValue = d.Minute.ToString().Length > 1 ? d.Minute.ToString() : "0" + d.Minute.ToString();
                        }
                        if (drinfo["endsession"] != null && drinfo["endsession"] != System.DBNull.Value)
                        {

                            d = (DateTime)drinfo["endsession"];
                            ddlendyear.SelectedValue = d.Year.ToString();
                            ddlendmonth.SelectedValue = d.Month.ToString();
                            ddlendday.SelectedValue = d.Day.ToString();
                            ddlendhour.SelectedValue = d.Hour.ToString().Length > 1 ? d.Hour.ToString() : "0" + d.Hour.ToString();
                            ddlendminute.SelectedValue = d.Minute.ToString().Length > 1 ? d.Minute.ToString() : "0" + d.Minute.ToString();
                        }

                    }

                }
                else
                    Response.Redirect("Signin.aspx");
            }
           
        }
        protected void BindLists()
        {
            ListItem l;
            for (int year = DateTime.Now.Year; year < DateTime.Now.Year + 15; year++)
            {
                l = new ListItem(year.ToString(), year.ToString());
                ddlsessionyear.Items.Add(l);
            }

            for (int month = 1; month <= 12; month++)
            {
                string month_name = "";



                if (month <= 9)
                {
                    l = new ListItem(month_name + " 0" + month.ToString(), month.ToString());
                    ddlsessionmonth.Items.Add(l);
                }
                else
                {
                    l = new ListItem(month_name + " " + month.ToString(), month.ToString());
                    ddlsessionmonth.Items.Add(l);

                }
            }
            for (int day = 1; day < 32; day++)
            {
                l = new ListItem(day.ToString(), day.ToString());
                ddlsessionday.Items.Add(l);

            }

        }

        protected void BindEventstart()
        {
            ListItem l;


            for (int year = DateTime.Now.Year; year < DateTime.Now.Year + 15; year++)
            {
                l = new ListItem(year.ToString(), year.ToString());
                ddlstartyear.Items.Add(l);
            }

            for (int month = 1; month <= 12; month++)
            {
                string month_name = "";



                if (month <= 9)
                {
                    l = new ListItem(month_name + " 0" + month.ToString(), month.ToString());
                    ddlstartmonth.Items.Add(l);
                }
                else
                {
                    l = new ListItem(month_name + " " + month.ToString(), month.ToString());
                    ddlstartmonth.Items.Add(l);

                }
            }
            for (int day = 1; day < 32; day++)
            {
                l = new ListItem(day.ToString(), day.ToString());
                ddlstartday.Items.Add(l);

            }
            for (int hrs = 0; hrs <= 23; hrs++)
            {
                if (hrs <= 9)
                {
                    l = new ListItem("0" + hrs.ToString(), "0" + hrs.ToString());
                    ddlstarthour.Items.Add(l);
                }
                else
                {
                    l = new ListItem(hrs.ToString(), hrs.ToString());
                    ddlstarthour.Items.Add(l);
                }
            }
            for (int min = 0; min < 60; min += 5)
            {
                if (min <= 5)
                {
                    l = new ListItem("0" + min.ToString(), "0" + min.ToString());
                    ddlstartminute.Items.Add(l);
                }
                else
                {
                    l = new ListItem(min.ToString(), min.ToString());
                    ddlstartminute.Items.Add(l);
                }
            }

        }
        protected void BindEventend()
        {
            ListItem l;


            for (int year = DateTime.Now.Year; year < DateTime.Now.Year + 15; year++)
            {
                l = new ListItem(year.ToString(), year.ToString());
                ddlendyear.Items.Add(l);
            }

            for (int month = 1; month <= 12; month++)
            {
                string month_name = "";



                if (month <= 9)
                {
                    l = new ListItem(month_name + " 0" + month.ToString(), month.ToString());
                    ddlendmonth.Items.Add(l);
                }
                else
                {
                    l = new ListItem(month_name + " " + month.ToString(), month.ToString());
                    ddlendmonth.Items.Add(l);

                }
            }
            for (int day = 1; day < 32; day++)
            {
                l = new ListItem(day.ToString(), day.ToString());
                ddlendday.Items.Add(l);

            }
            for (int hrs = 0; hrs <= 23; hrs++)
            {
                if (hrs <= 9)
                {
                    l = new ListItem("0" + hrs.ToString(), "0" + hrs.ToString());
                    ddlendhour.Items.Add(l);
                }
                else
                {
                    l = new ListItem(hrs.ToString(), hrs.ToString());
                    ddlendhour.Items.Add(l);
                }
            }
            for (int min = 0; min < 60; min += 5)
            {
                if (min <= 5)
                {
                    l = new ListItem("0" + min.ToString(), "0" + min.ToString());
                    ddlendminute.Items.Add(l);
                }
                else
                {
                    l = new ListItem(min.ToString(), min.ToString());
                    ddlendminute.Items.Add(l);
                }
            }

        }

        protected bool checkUrl(String url)
        {

            Uri uri = null;
            if (!Uri.TryCreate(url, UriKind.Absolute, out uri) || null == uri)
            {
                //Invalid URL
                return false;
            }
            else
                return true;
        }
        public bool IsValidEmail(string Email)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            System.Text.RegularExpressions.Regex _Regex = new System.Text.RegularExpressions.Regex(strRegex);
            if (_Regex.IsMatch(Email))
                return (true);
            else
                return (false);
        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //if (chkadddate())
                //{


                if (!Page.IsValid)
                {
                    return;
                }

                else
                {
                    if (Request.QueryString["InsID"] != "" && Request.QueryString["InsID"] != null)
                    {
                        WClause = "AND Insid =" + int.Parse(Request.QueryString["InsID"]);
                        dtinfo = InsController.GetINSInfoByWClause(WClause);
                        drinfo = dtinfo.Rows[0];
                        drinfo.BeginEdit();
                        drinfo["FirstName"] = txtFname.Text.ToString().Trim();
                        drinfo["LastName"] = txtLname.Text.ToString().Trim();
                        drinfo["Email"] = txtemail.Text.ToString().Trim();
                        drinfo["CellPhone"] = txtmob.Text.ToString().Trim();

                        if (txtclientname.Text == "")
                            drinfo["Clientname"] = DBNull.Value;
                        else
                            drinfo["Clientname"] = txtclientname.Text.ToString();

                        if (ddlstate.SelectedItem.Text.ToString() == "Select")
                            drinfo["Clientstate"] = DBNull.Value;
                        else
                            drinfo["Clientstate"] = ddlstate.SelectedItem.Text;



                        if (txtcity.Text == "")
                            drinfo["ClientCity"] = DBNull.Value;
                        else
                            drinfo["ClientCity"] = txtcity.Text.ToString();

                        if (txttopic.Text == "")
                            drinfo["Topic"] = DBNull.Value;
                        else
                            drinfo["Topic"] = txttopic.Text.ToString();

                        if (txtaccomplishments.Text == "")
                            drinfo["Accomplishments"] = DBNull.Value;
                        else
                            drinfo["Accomplishments"] = txtaccomplishments.Text.ToString();

                        if (txtissues.Text == "")
                            drinfo["Issues"] = DBNull.Value;
                        else
                            drinfo["Issues"] = txtissues.Text.ToString();

                        if (txtimprovement.Text == "")
                            drinfo["improvement"] = DBNull.Value;
                        else
                            drinfo["improvement"] = txtimprovement.Text.ToString();

                        if (txttestimonials.Text == "")
                            drinfo["Testimonials"] = DBNull.Value;
                        else
                            drinfo["Testimonials"] = txttestimonials.Text.ToString();

                        if (txtcomments.Text == "")
                            drinfo["comments"] = DBNull.Value;
                        else
                            drinfo["comments"] = txtcomments.Text.ToString();

                        if (ddlsessionyear.SelectedIndex > 0 && ddlsessionmonth.SelectedIndex > 0 && ddlsessionday.SelectedIndex > 0)
                        {

                            DateTime dateValue;
                            dateValue = new DateTime(Int32.Parse(ddlsessionyear.SelectedValue.ToString()), Int32.Parse(ddlsessionmonth.SelectedValue.ToString()), Int32.Parse(ddlsessionday.SelectedValue.ToString()), 0, 0, 0);
                            drinfo["Eventdate"] = dateValue;
                        }
                        else
                        {

                            drinfo["Eventdate"] = DBNull.Value;
                        }
                        if (ddlstartyear.SelectedIndex > 0 && ddlstartmonth.SelectedIndex > 0 && ddlstartday.SelectedIndex > 0)
                        {
                            DateTime dateValue;
                            if ((ddlstarthour.SelectedIndex > 0 || ddlstartminute.SelectedIndex > 0))
                            {
                                dateValue = new DateTime(Int32.Parse(ddlstartyear.SelectedValue.ToString()), Int32.Parse(ddlstartmonth.SelectedValue.ToString()), Int32.Parse(ddlstartday.SelectedValue.ToString()), Int32.Parse(ddlstarthour.SelectedValue.ToString()), Int32.Parse(ddlstartminute.SelectedValue.ToString()), 0);
                            }
                            else
                            {
                                dateValue = new DateTime(Int32.Parse(ddlstartyear.SelectedValue.ToString()), Int32.Parse(ddlstartmonth.SelectedValue.ToString()), Int32.Parse(ddlstartday.SelectedValue.ToString()), 0, 0, 0);

                            }
                            drinfo["startsession"] = dateValue;
                        }
                        else
                        {
                            drinfo["startsession"] = DBNull.Value;
                        }
                        if (ddlendyear.SelectedIndex > 0 && ddlendmonth.SelectedIndex > 0 && ddlendday.SelectedIndex > 0)
                        {
                            DateTime dateValue;
                            if ((ddlendhour.SelectedIndex > 0 || ddlendminute.SelectedIndex > 0))
                            {
                                dateValue = new DateTime(Int32.Parse(ddlendyear.SelectedValue.ToString()), Int32.Parse(ddlendmonth.SelectedValue.ToString()), Int32.Parse(ddlendday.SelectedValue.ToString()), Int32.Parse(ddlendhour.SelectedValue.ToString()), Int32.Parse(ddlendminute.SelectedValue.ToString()), 0);
                            }
                            else
                            {
                                dateValue = new DateTime(Int32.Parse(ddlendyear.SelectedValue.ToString()), Int32.Parse(ddlendmonth.SelectedValue.ToString()), Int32.Parse(ddlendday.SelectedValue.ToString()), 0, 0, 0);

                            }
                            drinfo["endsession"] = dateValue;
                        }
                        else
                        {
                            drinfo["endsession"] = DBNull.Value;
                        }

                        drinfo.EndEdit();
                        InsController.UpdateINSUser(dtinfo);
                        btnReset_Click(sender, e);
                        lblmsg.Text = "Update successfully.";
                        Response.Redirect("ViewInstructor.aspx", false);
                    }
                    else
                    {

                        WClause = "AND Insid =" + 0;
                        dtinfo = InsController.GetINSInfoByWClause(WClause);

                        drinfo = dtinfo.NewRow();
                        drinfo["FirstName"] = txtFname.Text.ToString().Trim();
                        drinfo["LastName"] = txtLname.Text.ToString().Trim();
                        drinfo["Email"] = txtemail.Text.ToString().Trim();
                        drinfo["CellPhone"] = txtmob.Text.ToString().Trim();

                        if (txtclientname.Text == "")
                            drinfo["Clientname"] = DBNull.Value;
                        else
                            drinfo["Clientname"] = txtclientname.Text.ToString();

                        if (ddlstate.SelectedItem.Text.ToString() == "Select")
                            drinfo["Clientstate"] = DBNull.Value;
                        else
                            drinfo["Clientstate"] = ddlstate.SelectedItem.Text;



                        if (txtcity.Text == "")
                            drinfo["ClientCity"] = DBNull.Value;
                        else
                            drinfo["ClientCity"] = txtcity.Text.ToString();

                        if (txttopic.Text == "")
                            drinfo["Topic"] = DBNull.Value;
                        else
                            drinfo["Topic"] = txttopic.Text.ToString();

                        if (txtaccomplishments.Text == "")
                            drinfo["Accomplishments"] = DBNull.Value;
                        else
                            drinfo["Accomplishments"] = txtaccomplishments.Text.ToString();

                        if (txtissues.Text == "")
                            drinfo["Issues"] = DBNull.Value;
                        else
                            drinfo["Issues"] = txtissues.Text.ToString();

                        if (txtimprovement.Text == "")
                            drinfo["improvement"] = DBNull.Value;
                        else
                            drinfo["improvement"] = txtimprovement.Text.ToString();

                        if (txttestimonials.Text == "")
                            drinfo["Testimonials"] = DBNull.Value;
                        else
                            drinfo["Testimonials"] = txttestimonials.Text.ToString();

                        if (txtcomments.Text == "")
                            drinfo["comments"] = DBNull.Value;
                        else
                            drinfo["comments"] = txtcomments.Text.ToString();

                        if (ddlsessionyear.SelectedIndex > 0 && ddlsessionmonth.SelectedIndex > 0 && ddlsessionday.SelectedIndex > 0)
                        {

                            DateTime dateValue;
                            dateValue = new DateTime(Int32.Parse(ddlsessionyear.SelectedValue.ToString()), Int32.Parse(ddlsessionmonth.SelectedValue.ToString()), Int32.Parse(ddlsessionday.SelectedValue.ToString()), 0, 0, 0);
                            drinfo["Eventdate"] = dateValue;
                        }
                        else
                        {

                            drinfo["Eventdate"] = DBNull.Value;
                        }
                        if (ddlstartyear.SelectedIndex > 0 && ddlstartmonth.SelectedIndex > 0 && ddlstartday.SelectedIndex > 0)
                        {
                            DateTime dateValue;
                            if ((ddlstarthour.SelectedIndex > 0 || ddlstartminute.SelectedIndex > 0))
                            {
                                dateValue = new DateTime(Int32.Parse(ddlstartyear.SelectedValue.ToString()), Int32.Parse(ddlstartmonth.SelectedValue.ToString()), Int32.Parse(ddlstartday.SelectedValue.ToString()), Int32.Parse(ddlstarthour.SelectedValue.ToString()), Int32.Parse(ddlstartminute.SelectedValue.ToString()), 0);
                            }
                            else
                            {
                                dateValue = new DateTime(Int32.Parse(ddlstartyear.SelectedValue.ToString()), Int32.Parse(ddlstartmonth.SelectedValue.ToString()), Int32.Parse(ddlstartday.SelectedValue.ToString()), 0, 0, 0);

                            }
                            drinfo["startsession"] = dateValue;
                        }
                        else
                        {
                            drinfo["startsession"] = DBNull.Value;
                        }
                        if (ddlendyear.SelectedIndex > 0 && ddlendmonth.SelectedIndex > 0 && ddlendday.SelectedIndex > 0)
                        {
                            DateTime dateValue;
                            if ((ddlendhour.SelectedIndex > 0 || ddlendminute.SelectedIndex > 0))
                            {
                                dateValue = new DateTime(Int32.Parse(ddlendyear.SelectedValue.ToString()), Int32.Parse(ddlendmonth.SelectedValue.ToString()), Int32.Parse(ddlendday.SelectedValue.ToString()), Int32.Parse(ddlendhour.SelectedValue.ToString()), Int32.Parse(ddlendminute.SelectedValue.ToString()), 0);
                            }
                            else
                            {
                                dateValue = new DateTime(Int32.Parse(ddlendyear.SelectedValue.ToString()), Int32.Parse(ddlendmonth.SelectedValue.ToString()), Int32.Parse(ddlendday.SelectedValue.ToString()), 0, 0, 0);

                            }
                            drinfo["endsession"] = dateValue;
                        }
                        else
                        {
                            drinfo["endsession"] = DBNull.Value;
                        }

                        dtinfo.Rows.Add(drinfo);
                        InsController.UpdateINSUser(dtinfo);
                        btnReset_Click(sender, e);
                        lblmsg.Text = "Information has been submitted successfully.";
                        
                    }
                }
            }

            catch (Exception ex)
            {
                if (log.IsDebugEnabled)
                    log.Error("[" + System.DateTime.Now.ToString("G") + "] Register::btnsubmit_Click() - ", ex);
                Response.Redirect("/errorpages/SiteError.aspx", false);

            }

        }
        protected void btnResetClick(object sender, EventArgs e)
        {
            if (Request.QueryString["InsID"] != "" && Request.QueryString["InsID"] != null)
            {
                Response.Redirect("ViewInstructor.aspx");
            }
            else
            {
                btnReset_Click(sender, e);
            }


        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            Session["Session_Table"] = null;
            lblmsg.Text = "";
            txtFname.Text = "";
            txtLname.Text = "";
            txtmob.Text = "";
            txtemail.Text = "";
            //  txtsession.Text = "";
            //txtcourse.Text = "";
            txttestimonials.Text = "";
            txtcity.Text = "";
            txtclientname.Text = "";
            txttopic.Text = "";
            txtaccomplishments.Text = "";
            txtissues.Text = "";
            txtimprovement.Text = "";
            txttestimonials.Text = "";
            txtcomments.Text = "";
            ddlstate.SelectedIndex = 0;

            ddlsessionday.SelectedIndex = 0;
            ddlsessionmonth.SelectedIndex = 0;
            ddlsessionyear.SelectedIndex = 0;

            ddlstartday.SelectedIndex = 0;
            ddlstartyear.SelectedIndex = 0;
            ddlstartmonth.SelectedIndex = 0;
            ddlstarthour.SelectedIndex = 0;
            ddlstartminute.SelectedIndex = 0;

            ddlendday.SelectedIndex = 0;
            ddlendyear.SelectedIndex = 0;
            ddlendmonth.SelectedIndex = 0;
            ddlendhour.SelectedIndex = 0;
            ddlendminute.SelectedIndex = 0;


        }

    }
}


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/QPSSite.Master" AutoEventWireup="true" CodeBehind="Instructor_Registration.aspx.cs" Inherits="QPS.Instructors.Instructor_Registration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>
<asp:Content ID="ContentRegister" ContentPlaceHolderID="cphContent" runat="server">
    <script language="javascript" type="text/javascript">
        setPage("Training");
        function setFocus() {
            document.getElementById("txtFname").focus();
        }
        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode >= 48 && charCode <= 57) return false;
            return true;
            //            if (charCode > 31 && (charCode < 48 || charCode > 57 || charCode == 08))
            //                return true;
            //            return false;
        }
        function autoIframe(frameId) {
            frame = document.getElementById(frameId);
            innerDoc = (frame.contentDocument) ? frame.contentDocument : frame.contentWindow.document;
            objToResize = (frame.style) ? frame.style : frame;
            objToResize.height = (innerDoc.body.scrollHeight) + 'px';
            objToResize.width = (innerDoc.body.scrollWidth + 20) + 'px';
        }

        function istxt(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode

            if (charCode > 31 && (charCode < 48 || charCode > 57 || charCode == 08) && charCode != 45)
                return false;
            else
                return true;
        }



        function hidewait() {
            //            debugger;
            //            frameid = document.getElementById('iframe1');
            //            var d = document.getElementById('repdiv');

            //            d.innerHTML = "";
            //  frameid.src = "";

            var iframe = document.getElementById('iframe1');
            var innerDoc = iframe.contentDocument || iframe.contentWindow.document;

            var d = innerDoc.getElementById('repdiv');
            d.innerHTML = '';
        }


        function checkData(year, ddlDay, MonthValue) {
            try {
                if (ddlDay.options.length == 30) {
                    ddlDay.options[30] = new Option('30', '30');
                    ddlDay.options[31] = new Option('31', '31');
                }
                else if (ddlDay.options.length == 29) {
                    ddlDay.options[29] = new Option('29', '29');
                    ddlDay.options[30] = new Option('30', '30');
                    ddlDay.options[31] = new Option('31', '31');
                }

                if (MonthValue == "2") {

                    if (ddlDay.options.length == 32) {
                        if ((year % 4) == 0) {
                            ddlDay.options[31] = null;
                            ddlDay.options[30] = null;
                        }
                        else {
                            ddlDay.options[31] = null;
                            ddlDay.options[30] = null;
                            ddlDay.options[29] = null;
                        }
                    }
                    else {
                        if ((year % 4) == 0) {

                            ddlDay.options[30] = null;
                        }
                        else {
                            ddlDay.options[30] = null;
                            ddlDay.options[29] = null;
                        }

                    }
                }

                else if ((MonthValue == "4") || (MonthValue == "6") || (MonthValue == "9") || (MonthValue == "11")) {
                    ddlDay.options[31] = null;
                }
                else {
                    if (ddlDay.options.length != "32")
                        ddlDay.options[31] = new Option('31', '31');
                }

            } catch (ex) { }
        }

        //      
        function chksessiondate() {
            var year = document.getElementById('<%=ddlsessionyear.ClientID%>').value;
            var ddlDay = document.getElementById('<%=ddlsessionday.ClientID%>');
            var MonthVal = document.getElementById('<%=ddlsessionmonth.ClientID%>').value;
            checkData(year, ddlDay, MonthVal);
        }
        function chksessionstartdate() {
            var year = document.getElementById('<%=ddlstartyear.ClientID%>').value;
            var ddlDay = document.getElementById('<%=ddlstartday.ClientID%>');
            var MonthVal = document.getElementById('<%=ddlstartmonth.ClientID%>').value;
            checkData(year, ddlDay, MonthVal);
        }
        function chksessionenddate() {
            var year = document.getElementById('<%=ddlendyear.ClientID%>').value;
            var ddlDay = document.getElementById('<%=ddlendday.ClientID%>');
            var MonthVal = document.getElementById('<%=ddlendmonth.ClientID%>').value;
            checkData(year, ddlDay, MonthVal);
        }



        function textboxMultilineMaxNumber(txt, maxLen) {
            try {
                if (txt.value.length > (maxLen - 1)) return false;
            } catch (e) {
            }
        }
    </script>
    <div class="span-24 prepend-1 top highcont">
        <div class="span-18 top" style="float: right">
            <br class="clear">
            <h3 class="colbright pagetitle" id="H3_1">
                Report Information</h3>
            <form id="fRegister" runat="server">
            <ajax:ScriptManager ID="sm1" runat="server">
            </ajax:ScriptManager>
            <%-- <ajax:UpdatePanel ID="UP1" runat="server">
                <ContentTemplate>--%>
            <div id="dreg" runat="server">
                <table width="100%" runat="server" id="tblAdd" class="Empregwrp">
                    <tr>
                        <td colspan="2" width="51%;">
                            Fields marked with <span style="color: Red">*</span> are mandatory
                            <br />
                        </td>
                        <td style="padding: 1px; border: 1px solid silver; ">
                            <table>
                                <tr>
                                    <td align="center" class="adminusewrp">
                                        <b>For Admin Use</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0; font-size:14px!important;" >
                                        To view registrations, Please
                                        <asp:HyperLink ID="Link" runat="server" NavigateUrl="~/Instructor/ViewInstructor.aspx"
                                            Text="click here"></asp:HyperLink>
                                        to login.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <br />
                            <asp:Label ID="lblmsg" runat="server" Font-Bold="true" ForeColor="Green"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="200px">
                            First Name <span style="color: red">*</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFname" runat="server" Width="200px" onkeypress="return isNumberKey(event);"
                                MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvFirstName" runat="server"
                                ControlToValidate="txtFname" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rgvFname" runat="server" Display="Dynamic" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"
                                ControlToValidate="txtFname" ValidationGroup="register" SetFocusOnError="true"
                                ErrorMessage="Enter proper value "></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Last Name <span style="color: red">*</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtLname" runat="server" Width="200px" onkeypress="return isNumberKey(event);"
                                MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvLastName" runat="server"
                                ControlToValidate="txtLname" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rgvLname" runat="server" Display="Dynamic" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"
                                ControlToValidate="txtLname" ValidationGroup="register" SetFocusOnError="true"
                                ErrorMessage="Enter proper value"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Submitter Email <span style="color: red">*</span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtemail" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtemail"
                                ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionEmail"
                                runat="server" ErrorMessage="Invalid email address" ControlToValidate="txtemail"
                                Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="register"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Submitter Phone <span style="color: red">*</span>
                        </td>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtmob" onkeypress="return istxt(event);" runat="server" Width="200px"
                                MaxLength="12"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvcellPhone" runat="server" ControlToValidate="txtmob"
                                ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionPhone"
                                runat="server" ControlToValidate="txtmob" ErrorMessage="Enter cell phone number (xxx-xxx-xxxx) format"
                                ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" Display="Dynamic"
                                ValidationGroup="register"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                     <tr>
                        <td align="right" width="200px">
                            Client Name <span style="color: red">*</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtclientname" runat="server" Width="200px" onkeypress="return isNumberKey(event);"
                                MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="txtclientname" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"
                                ControlToValidate="txtclientname" ValidationGroup="register" SetFocusOnError="true"
                                ErrorMessage="Enter proper value "></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                       <td align="right">
                           Client State<span style="color: red">*</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlstate">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="CT">CT</asp:ListItem>
                                <asp:ListItem Value="IL">IL</asp:ListItem>
                                <asp:ListItem Value="MA">MA</asp:ListItem>
                                <asp:ListItem Value="NC">NC</asp:ListItem>
                                <asp:ListItem Value="NJ">NJ</asp:ListItem>
                                <asp:ListItem Value="RI">RI</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator6" runat="server" InitialValue="0"
                                ControlToValidate="ddlstate" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                     <tr>
                        <td align="right" width="200px">
                            Client City <span style="color: red">*</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtcity" runat="server" Width="200px" onkeypress="return isNumberKey(event);"
                                MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator4" runat="server"
                                ControlToValidate="txtcity" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"
                                ControlToValidate="txtcity" ValidationGroup="register" SetFocusOnError="true"
                                ErrorMessage="Enter proper value "></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="200px">
                            Topic<span style="color: red"></span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txttopic" runat="server" Width="200px" Height="80px" onkeypress="return isNumberKey(event);"
                                MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator6" runat="server"
                                ControlToValidate="txttopic" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>--%>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" Display="Dynamic" ValidationExpression="^([a-zA-Z]+)([\sa-zA-Z])*$"
                                ControlToValidate="txttopic" ValidationGroup="register" SetFocusOnError="true"
                                ErrorMessage="Enter proper value "></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Event date
                        </td>
                        <td colspan="2" align="left">
                         <asp:DropDownList ID="ddlsessionmonth" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessiondate();">
                                <asp:ListItem Value="0">Month</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlsessionday" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessiondate();">
                                <asp:ListItem Value="0">Day</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlsessionyear" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessiondate();">
                                <asp:ListItem Value="0">Year</asp:ListItem>
                            </asp:DropDownList>
                             <asp:Label ID="Label1" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="200px">
                            Accomplishments<span style="color: red"></span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtaccomplishments" runat="server" Width="200px" Height="80px" 
                                MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator7" runat="server"
                                ControlToValidate="txtaccomplishments" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                           --%> 
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="200px">
                            Issues<span style="color: red"></span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtissues" runat="server" Width="200px" Height="80px" 
                               Rows="1000" TextMode="MultiLine"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator8" runat="server"
                                ControlToValidate="txtissues" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                         --%>  
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="200px">
                            Needed Improvement<span style="color: red"></span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtimprovement" runat="server" Width="200px" Height="80px"  
                                MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator9" runat="server"
                                ControlToValidate="txtimprovement" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                          --%>  
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="200px">
                            Testimonials<span style="color: red"></span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txttestimonials" runat="server" Width="200px" Height="80px" 
                                MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator10" runat="server"
                                ControlToValidate="txttestimonials" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                            --%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="200px">
                            Other Comments<span style="color: red"></span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtcomments" runat="server" Width="200px" Height="80px" 
                                MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                          <%--  <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator11" runat="server"
                                ControlToValidate="txtcomments" ErrorMessage="*" Display="Dynamic" ValidationGroup="register"></asp:RequiredFieldValidator>
                           --%> 
                        </td>
                    </tr>
                  
                     <tr>
                        <td align="right">
                            Session Start Date
                        </td>
                        <td colspan="2" align="left">
                         <asp:DropDownList ID="ddlstartmonth" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessionstartdate();">
                                <asp:ListItem Value="0">Month</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlstartday" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessionstartdate();">
                                <asp:ListItem Value="0">Day</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlstartyear" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessionstartdate();">
                                <asp:ListItem Value="0">Year</asp:ListItem>
                            </asp:DropDownList>
                             <asp:Label ID="Label2" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Session Start Time
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlstarthour" runat="server">
                                <asp:ListItem Value="-1">Hour</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlstartminute" runat="server">
                                <asp:ListItem Value="-1">Minute</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                   <tr>
                        <td align="right">
                            Session End Date
                        </td>
                        <td colspan="2" align="left">
                         <asp:DropDownList ID="ddlendmonth" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessionenddate();">
                                <asp:ListItem Value="0">Month</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlendday" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessionenddate();">
                                <asp:ListItem Value="0">Day</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlendyear" runat="server" AppendDataBoundItems="True" onchange="javascript:chksessionenddate();">
                                <asp:ListItem Value="0">Year</asp:ListItem>
                            </asp:DropDownList>
                             <asp:Label ID="Label3" runat="server" Font-Bold="true" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Session End Time
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlendhour" runat="server">
                                <asp:ListItem Value="-1">Hour</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlendminute" runat="server">
                                <asp:ListItem Value="-1">Minute</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                  
                   
                    <tr>
                        <td>
                        </td>
                        <td>
                            <br />
                            <asp:Button ID="btnsubmit" runat="server" Text="Submit" ValidationGroup="register"
                              OnClick="btnsubmit_Click" />&nbsp;
                               <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnResetClick" />
                          
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False"
                                ValidationGroup="register" />
                        </td>
                    </tr>
                </table>
                
            </div>
           
            </form>
        </div>
        <div class="span-4">
            <img src="../images/resources.gif" class="pull-1" alt="QPS Consulting">
            <hr class="space">
            <div style="margin-top: 40px; padding: 7px; border: 1px dotted silver; background: #eaeaea;">
                <img src="../images/check.gif" title="Click here">
                <h3 class="mostwrap">
                    <a class="nounder" href="/Training/Calendar.aspx" title="Click here">Click here</a></h3>
                for our Upcoming Corporate Training Programs
            </div>
            <hr class="space">
            <br class="clear">
            <br class="clear">
        </div>
    </div>
</asp:Content>

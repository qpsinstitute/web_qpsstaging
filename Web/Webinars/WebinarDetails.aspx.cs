using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using System.Text;

namespace QPS.Webinars
{
    public partial class WebinarDetails : System.Web.UI.Page
    {
        private String _InstructorImage;

        public String InstructorImage
        {
            get { return _InstructorImage; }
            set { _InstructorImage = value; }
        }

        private string _InstructorName;

        public string InstructorName
        {
            get { return _InstructorName; }
            set { _InstructorName = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable tblWebinar = Lib.BusinessLogic.WebinarController.GetWebinarByWClause(" and Status = 1 and WebinarID = " + Request.QueryString["WebinarId"].ToString());

            foreach (DataRow dr in tblWebinar.Rows)
            {
                Page.Title = "Webinar - " + dr["Title"].ToString();

                lblWebinarHeader.Text = dr["Title"].ToString();
                lblWebinarDescription.Text = dr["Description"].ToString();
                imgInstructorImage.Src = new System.Configuration.AppSettingsReader().GetValue("InstructorImages", typeof(String)).ToString() + dr["InstructorImage"].ToString();
                imgInstructorImage.Attributes.Add("alt", dr["Instructor"].ToString());

                lblInstructorName.Text = dr["Instructor"].ToString();
                lblWebinarId.Text = Request.QueryString["WebinarId"].ToString();
                lblTrainingDuration.Text = dr["Duration"].ToString() + " Min";

                lblWhyShouldYouAttend.Text = dr["WhyYouShouldAttend"].ToString();
                ltAreaCoverInThisSeminar.Text = dr["AreaCoverInThisSeminar"].ToString();
                lblWhowillbenefit.Text = dr["MoreInfo"].ToString();
                lblInstructorProfile.Text = dr["InstructorProfile"].ToString();

                lblTrainingDate.Text = DateTime.Parse(dr["WebinarOn"].ToString()).ToString("dddd, MMMM dd, yyyy");//+ " EST";hh:mm tt
                lblTrainingTime.Text = DateTime.Parse(dr["WebinarOn"].ToString()).ToString("HH:mm") + " - " + DateTime.Parse(dr["WebinarOn"].ToString()).AddMinutes(Double.Parse(dr["Duration"].ToString())).ToString("HH:mm") + " EST"; //hh:mm tt

                InstructorImage = new System.Configuration.AppSettingsReader().GetValue("InstructorImages", typeof(String)).ToString() + dr["InstructorImage"].ToString();
                InstructorName = dr["Instructor"].ToString();

                if (DateTime.Parse(dr["WebinarOn"].ToString()) < DateTime.Now)
                    btnsubmit.Enabled = false;
                else
                    btnsubmit.Enabled = true;

                DataTable tblWebniarOption = Lib.BusinessLogic.WebinarController.GetWebinarOptionByWClause(" and WebinarOptions.WebinarID = " + Request.QueryString["WebinarId"].ToString());

                String str = "";
                int cnt = 1;
                foreach (DataRow dropt in tblWebniarOption.Rows)
                {
                    str += "<tr>";
                    str += "<td align=\"left\" valign=\"top\" style=\"width: 18px; padding: 0; padding-left: 10px;\" colspan=\"2\">";
                    str += "<input type=\"checkbox\" value=\"" + dropt["WebinarOptionID"] + "\" name=\"add_product_id\" id=\"add_product_id_" + cnt++ + "\">";
                    str += "<b style=\"color: #D02422; font-size: 12px;\">$" + dropt["Cost"].ToString() + "</b> &nbsp; ";
                    str += "<span style=\"font-size: 11px;color: #5E5E5E; padding-left: 3px;\">" + dropt["Description"].ToString() + "</span>";
                    str += "</td>";
                    str += "</tr>";
                }

                ltWebniarOption.Text = str;
            }


        }
        protected void addCalender_click(object sender, EventArgs e)
        {
            DataTable tblWebinar = Lib.BusinessLogic.WebinarController.GetWebinarByWClause(" and Status = 1 and WebinarID = " + Request.QueryString["WebinarId"].ToString());

            foreach (DataRow dr in tblWebinar.Rows)
            {
                Response.ContentType = "text/v-calendar";
                Response.AddHeader("content-disposition", "attachment; filename=" + "CoWebinar_1" + ".ics");

                DateTime dtstart = DateTime.Parse(dr["WebinarOn"].ToString());
                DateTime dtend = dtstart.AddMinutes(Int32.Parse(dr["Duration"].ToString()));

                Response.Write("BEGIN:VCALENDAR" + Environment.NewLine);
                Response.Write("PRODID: -//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN" + Environment.NewLine);

                Response.Write("VERSION:2.0" + Environment.NewLine);
                Response.Write("METHOD:PUBLISH" + Environment.NewLine);

                Response.Write("X-MS-OLK-FORCEINSPECTOROPEN:TRUE" + Environment.NewLine);
                Response.Write("BEGIN:VEVENT" + Environment.NewLine);

                Response.Write("CLASS:PUBLIC" + Environment.NewLine);
                Response.Write("CREATED:" + dtstart.ToString("s") + Environment.NewLine);

                StringBuilder sb = new StringBuilder();
                sb.Append("Training Topic: " + dr["Title"].ToString());
                sb.AppendLine("Instructor: " + dr["Instructor"].ToString());

                String navLink = new System.Configuration.AppSettingsReader().GetValue("WebinarNavigateURL", typeof(string)).ToString();

                String desc = "Training Topic: " + dr["Title"].ToString() + "\\n Instructor: " + dr["Instructor"].ToString() + "\\n Date: " + DateTime.Parse(dr["WebinarOn"].ToString()).ToString("dddd, dd MMMM yyyy") + "\\n Time: " + DateTime.Parse(dr["WebinarOn"].ToString()).ToString("HH:mm tt") + " EST" + "\\n For Registration & Details: " + navLink + Request.QueryString["WebinarId"].ToString();
                Response.Write("DESCRIPTION:" + desc + Environment.NewLine);

                //Response.Write("DESCRIPTION:" + @"Training Topic: Understanding Attribute Acceptance Sampling \n Instructor: Dan O�Leary\n Date: Thursday 19 January 2012 \n Time:01:00 PM EST | 10:00 AM PST \n For Registration & Details: http://qpsinc.com/Webinars/Default.aspx" + Environment.NewLine);

                Response.Write("DTEND:" + dtend.ToString("s") + Environment.NewLine);

                // Response.Write("DTSTAMP:" + dtstart.ToString("s") + Environment.NewLine);
                Response.Write("DTSTART:" + dtstart.ToString("s") + Environment.NewLine);

                if (dr["WebinarOn"] != null)
                    Response.Write("LAST-MODIFIED:" + DateTime.Parse(dr["WebinarOn"].ToString()).ToString("s") + Environment.NewLine);
                else
                    Response.Write("LAST-MODIFIED:" + DateTime.Parse(dr["CreatedDate"].ToString()).ToString("s") + Environment.NewLine);

                Response.Write("LOCATION:Online Webinar Web link and conference call number will be emailed after registration" + Environment.NewLine);

                Response.Write("PRIORITY:5" + Environment.NewLine);
                Response.Write("SEQUENCE:0" + Environment.NewLine);

                Response.Write("SUMMARY;LANGUAGE=en-us:" + "Understanding Attribute Acceptance Sampling" + Environment.NewLine);
                Response.Write("TRANSP:OPAQUE" + Environment.NewLine);
                Random rand = new Random();

                Response.Write(string.Format("UID:{0}-{1}", 1, rand.Next(1000)) + Environment.NewLine);
                // Response.Write(@"X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC -//W3C//DTD HTML 3.2//EN>\n<HTML>\n<HEAD>\n<META NAME='Generator' CONTENT='MS Exchange Server version 08.00.0681.000'>\n<TITLE></TITLE>\n</HEAD>\n<BODY>\n\n\n<P DIR=LTR><SPAN LANG='en-us'></SPAN><SPAN LANG='en-us'></SPAN><SPAN LANG='en-us'><FONT SIZE=2 FACE='Arial'>" + "Dss" + "</FONT></SPAN><SPAN LANG='en-us'></SPAN><SPAN LANG='en-us'></SPAN><SPAN LANG='en-us'></SPAN></P>\n\n</BODY>\n</HTML>" + Environment.NewLine);

                Response.Write("SUMMARY:" + sb + Environment.NewLine + "");
                Response.Write("X-MICROSOFT-CDO-BUSYSTATUS:BUSY" + Environment.NewLine);

                Response.Write("X-MICROSOFT-CDO-IMPORTANCE:1" + Environment.NewLine);
                Response.Write("BEGIN:VALARM" + Environment.NewLine);

                Response.Write("TRIGGER:-PT15M" + Environment.NewLine);
                Response.Write("ACTION:DISPLAY" + Environment.NewLine);

                Response.Write("DESCRIPTION:Reminder" + Environment.NewLine);
                Response.Write("END:VALARM" + Environment.NewLine);

                Response.Write("END:VEVENT" + Environment.NewLine);
                Response.Write("END:VCALENDAR" + Environment.NewLine);
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            tbldata.Attributes["style"] = "display:none;";
            string txtBody = txtMessage.Text.ToString();
            string YourName = txtYourName.Text.Trim();
            string From = txtYourEmail.Text.Trim();
            string ToEmail = txtFriendsEmail.Text.ToString().Trim();
            string Subject = txtYourEmail.Text + " has sent you a link! ";
            string friendsname = txtfriendsname.Text.Trim();
            Hashtable hsDocument = new Hashtable();

            DataTable tblWebinar = Lib.BusinessLogic.WebinarController.GetWebinarByWClause(" and Status = 1 and WebinarID = " + Request.QueryString["WebinarId"].ToString());
            String navLink = new System.Configuration.AppSettingsReader().GetValue("WebinarNavigateURL", typeof(string)).ToString();


            hsDocument.Add("$$FRIENDSNAME$$", friendsname);
            hsDocument.Add("$$YOURNAME$$", YourName);
            hsDocument.Add("$$MESSAGE$$", txtBody);
            hsDocument.Add("$$SeminarTitle$$", tblWebinar.Rows[0]["Title"].ToString());
            hsDocument.Add("$$navlink$$", navLink + Request.QueryString["WebinarId"].ToString());
            bool ans = false;

            txtBody = EmailController.GetBodyFromTemplate(ConfigurationSettings.AppSettings["QpsWebinarTemplate"], hsDocument);
            try
            {
                EmailController.SendMail(From, ToEmail, Subject, txtBody, true);
                ans = true;
            }
            catch (Exception ex)
            {
                ans = false;
            }
            if (ans == true)
            {
                txtMessage.Text = "";
                txtYourName.Text = "";
                txtYourEmail.Text = "";
                txtfriendsname.Text = "";
                txtFriendsEmail.Text = "";
                tblthanks.Attributes["style"] = "display:block;";
            }
            else
            {
            }
        }
    }
}


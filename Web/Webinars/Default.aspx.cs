using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using log4net.Config;
using System.Text;

namespace QPS.Webinars
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getWebniarTopits();
        }

        private void getWebniarTopits()
        {
            StringBuilder sb = new StringBuilder();

            DataTable tblWebinar = Lib.BusinessLogic.WebinarController.GetWebinarByWClause(" and Status = 1 and WebinarOn >= " + "'" + DateTime.Now.ToString() + "'");
            
            UpCommingSeminar.DataSource = tblWebinar;
            UpCommingSeminar.DataBind();

            tblWebinar = Lib.BusinessLogic.WebinarController.GetWebinarByWClause(" and Status = 1 and WebinarOn < " + "'" + DateTime.Now.ToString() + "'");
            
            PastSeminar.DataSource = tblWebinar; ;
            PastSeminar.DataBind();
        }
    }
}

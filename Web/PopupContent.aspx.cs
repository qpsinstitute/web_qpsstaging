using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lib.BusinessLogic;
using log4net;
using log4net.Config;
using System.Collections.Generic;
using System.Text;

namespace QPS
{
    public partial class PopupContent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Label tb = (Label)Master.FindControl("lblCourseTitle");

                    string CourseCode="";
                    CourseCode = Request.QueryString["CourseCode"].ToString();

                    //Get The Details for the ID
                    DataTable dt_Details = Lib.BusinessLogic.eLearnCategoryController.SelectAllCourseDetailsByCourseCode("WHERE CourseCode= '" + CourseCode + "'");

                    tb.Text = dt_Details.Rows[0]["CourseTitle"].ToString();


                    
                    rptCourseDetails.DataSource = dt_Details;
                    rptCourseDetails.DataBind();
                    foreach (RepeaterItem item in rptCourseDetails.Items)
                    {
                        if (dt_Details.Rows[0]["CourseBenifit"].ToString() != "")
                        {
                            item.FindControl("divcoursebenefit").Visible = true;
                        }
                        if (dt_Details.Rows[0]["Prerequisites"].ToString() != "")
                        {
                            item.FindControl("divPrerequisite").Visible = true;
                        }
                        if (dt_Details.Rows[0]["CertificationReq"].ToString() != "")
                        {
                            item.FindControl("divCertification").Visible = true;
                        }
                        if (dt_Details.Rows[0]["Duration"].ToString() != "")
                        {
                            item.FindControl("divDuration").Visible = true;
                        }
                        if (dt_Details.Rows[0]["Cost"].ToString() != "")
                        {
                            item.FindControl("divCost").Visible = true;
                        }
                        if (dt_Details.Rows[0]["TopicsCovered"].ToString() != "")
                        {
                            item.FindControl("divTopicCovered").Visible = true;
                        }
                        if (dt_Details.Rows[0]["MoreInfo"].ToString() != "")
                        {
                            item.FindControl("divMoreInfo").Visible = true;
                        }
                        if (dt_Details.Rows[0]["Instructor"].ToString() != "")
                        {
                            item.FindControl("divInstructor").Visible = true;
                        }
                        if (dt_Details.Rows[0]["Attenddetail"].ToString() != "")
                        {
                            item.FindControl("divAttendies").Visible = true;
                        }

                    }

                    //Label b = item.FindControl("lblCourseBenefit") as Label;
                    //b.Visible = false;
                    //Label b1 = item.FindControl("lblcoursebenifitdetail") as Label;
                    //b1.Visible = false;

                }

            }
            catch (Exception ex)
            {
               
            }
        }

    }


}